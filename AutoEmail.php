<?php
if (isset($_SERVER['REMOTE_ADDR'])) die('Permission denied.');

/*
Crontab
* 7 * * 3,5 /usr/bin/php /online/erp.izin.co.id/www/AutoEmail.php
*/

require_once "system/CrcConfig.php";
require_once "system/appsConfig.php";
require_once "system/db_connect.php";

require_once "vendor/autoload.php";

use PHPMailer\PHPMailer\Exception as MailerExeption;
use PHPMailer\PHPMailer\PHPMailer;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;

$filename = 'Lead_Report';
$savefileto = dirname(__FILE__) . '/excell/' . $filename . '.xls';

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
header('Cache-Control: max-age=0');

// NotifLead_i
// AMBIL USER DENGAN PRIV SALES (U/ LEAD) DAN MANAGER
// NAMA, EMAIL
$cLgnDet = "SELECT `id`, `uid`, `nama_asli`, `priv` FROM izin_sys_lgndetail WHERE `priv` = 4 OR `priv` = 2";
$rcLgnDet = $dbs->getQuery($cLgnDet);

// BATAS WAKTU 1 TAHUN KEBELAKANG DATANYA
$notif_date_from = date('Y-m-d', strtotime('now - 360 days'));
$notif_date_to = date('Y-m-d', strtotime('now'));

// DATE NOW
date_default_timezone_set(TIMESET);
$dt = new DateTime('now', new DateTimezone(TIMESET));
$date_now = $dt->format('Y-m-d H:i:s');

// LOOP PER ORANG UNTUK PRODUKSI FILE DAN KIRIM EMAIL
// KALAU MANAGER SELURUH LEAD
// KALAU SALES HANYA YANG BAGIANNYA SAJA
while ($LgnDet = $dbs->getAssoc($rcLgnDet)) {
	//VIEW NOTIFICATION LIST
	$cLeadReport = "SELECT `lead`.`client_id`, `lead`.`client_name`, `lead`.`client_wa`, `lead`.`client_email`, `lead`.`tgl_update`, ";
	$cLeadReport .= "`lead`.`status`, `lead`.`status_nm`, `lead`.`sales_id` FROM `izin_apps_lead` AS `lead` ";
	$cLeadReport .= "WHERE `lead`.`status` <> 'CL' ";
	$cLeadReport .= "AND `lead`.`status` <> 'LO' ";

	if ($LgnDet["priv"] == 2  || $LgnDet["priv"] == 1 || $LgnDet["priv"] == 0) {
		if (isset($notif_date_from) && isset($notif_date_to)) {
			$cLeadReport .= "AND str_to_date(`lead`.`tgl_update`, '%Y-%m-%d') BETWEEN '" . $notif_date_from . "' AND '" . $notif_date_to . "' ";
		}
	} else if ($LgnDet["priv"] == 4) {
		$cLeadReport .= "AND `lead`.`sales_id` = '" . $LgnDet["id"] . "' ";
		if (isset($notif_date_from) && isset($notif_date_to)) {
			$cLeadReport .= "AND str_to_date(`lead`.`tgl_update`, '%Y-%m-%d') BETWEEN '" . $notif_date_from . "' AND '" . $notif_date_to . "' ";
		}
	}
	$cLeadReport .= "ORDER BY `lead`.`tgl_update` DESC";
	$rcLeadReport = $dbs->getQuery($cLeadReport);
	// End NotifLead_i


	// Notification_i
	$spreadsheet = new Spreadsheet();
	$writer = IOFactory::createWriter($spreadsheet, 'Xls');

	$spreadsheet->getProperties()
		->setCreator("Bontot")
		->setLastModifiedBy("Bontot")
		->setTitle("Lead Report")
		->setSubject("Lead Report")
		->setDescription("Lead Report List for Office 2007 XLSX, generated using PHP classes.")
		->setKeywords("Lead Report Permit izin")
		->setCategory("Lead");

	$spreadsheet->setActiveSheetIndex(0);
	$activesheet = $spreadsheet->getActiveSheet();

	$PageSetup = $activesheet->getPageMargins();
	$PageSetup->setTop(0.95);
	$PageSetup->setRight(0.75);
	$PageSetup->setLeft(0.75);
	$PageSetup->setBottom(0.95);

	$activesheet->getPageSetup()
		->setOrientation(PageSetup::ORIENTATION_PORTRAIT)
		->setPaperSize(PageSetup::PAPERSIZE_A4)
		->setFitToWidth(0)
		->setFitToHeight(0);

	$activesheet->getDefaultRowDimension()->setRowHeight(27);
	$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
	$spreadsheet->getDefaultStyle()->getFont()->setSize(9);
	$spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
	$spreadsheet->getDefaultStyle()->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);

	$activesheet->getPageSetup()->setHorizontalCentered(true);
	$activesheet->setShowGridlines(false);
	$activesheet->getStyle('A1:G1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
	$activesheet->getStyle('B:C')->getAlignment()->setHorizontal('center');
	$activesheet->getStyle('E:G')->getAlignment()->setHorizontal('center');
	$activesheet->getRowDimension('1')->setRowHeight(25);


	$activesheet->getColumnDimension('A')->setWidth(5);
	$activesheet->setCellValue('A1', 'No')->getStyle('A1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('B')->setWidth(12);
	$activesheet->setCellValue('B1', 'Last Update')->getStyle('B1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('C')->setWidth(10);
	$activesheet->setCellValue('C1', 'Client ID')->getStyle('C1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('D')->setWidth(22);
	$activesheet->setCellValue('D1', 'Client Name')->getStyle('D1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('E')->setWidth(15);
	$activesheet->setCellValue('E1', 'WA')->getStyle('E1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('F')->setWidth(20);
	$activesheet->setCellValue('F1', 'Email')->getStyle('F1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('G')->setWidth(9);
	$activesheet->setCellValue('G1', 'Status')->getStyle('G1')->getFont()->setBold(true);

	$nourut = 0;
	$r = 2;
	$activesheet->getRowDimension($r)->setRowHeight(3);

	while ($LeadReport = $dbs->getAssoc($rcLeadReport)) {
		$nourut++;
		$r++;
		if ($r % 28 == 0) {
			$activesheet->setCellValue('A' . $r, 'Last Update')->getStyle('E' . $r)->getFont()->setBold(true);
			$activesheet->setCellValue('B' . $r, 'Last Update')->getStyle('E' . $r)->getFont()->setBold(true);
			$activesheet->setCellValue('C' . $r, 'Client ID')->getStyle('A' . $r)->getFont()->setBold(true);
			$activesheet->setCellValue('D' . $r, 'Client Name')->getStyle('B' . $r)->getFont()->setBold(true);
			$activesheet->setCellValue('E' . $r, 'WA')->getStyle('C' . $r)->getFont()->setBold(true);
			$activesheet->setCellValue('F' . $r, 'Email')->getStyle('D' . $r)->getFont()->setBold(true);
			$activesheet->setCellValue('G' . $r, 'Status')->getStyle('F' . $r)->getFont()->setBold(true);
			$r++;
			$activesheet->getRowDimension($r)->setRowHeight(3);
		} else {

			$activesheet->setCellValue('A' . $r, $nourut);
			$activesheet->setCellValue('B' . $r, date("d-m-Y", strtotime($LeadReport['tgl_update'])));
			$activesheet->setCellValue('C' . $r, $LeadReport['client_id']);
			$activesheet->setCellValue('D' . $r, $LeadReport['client_name']);
			$activesheet->setCellValue('E' . $r, $LeadReport['client_wa']);
			$activesheet->setCellValue('F' . $r, $LeadReport['client_email']);
			$activesheet->setCellValue('G' . $r, $LeadReport['status_nm']);
		}
	}

	$styleArray = array(
		'borders' => array(
			'allBorders' => array(
				'borderStyle' => Border::BORDER_THIN,
				'color' => array('argb' => 'FFCCCCCC'),
			),
		),
	);

	$activesheet->getStyle('A1:G' . $r)->applyFromArray($styleArray);
	$writer->save($savefileto);
	// End of Notification_i

	// create a log channel
	$logger = new \Monolog\Logger('default');
	$logger->pushHandler(new \Monolog\Handler\RotatingFileHandler('/log/izinapps.log'));

	// Send_Mail
	$Email_Address = $LgnDet["uid"];
	$Email_Name = $LgnDet["nama_asli"];
	$Email_Subject = "IZIN.co.id autogenerated Lead Report";
	$Email_Body = "<b>Halo " . $Email_Name . "</b>,<br/></br>Terlampir Attachment Lead Report untuk masa periode " . date('d M Y', strtotime($notif_date_from)) . " sampai dengan " . date('d M Y', strtotime($notif_date_to)) . ".";

	$mail = new PHPMailer(true);

	try {
		$mail->IsSMTP();
		$mail->SMTPDebug = 0;
		$mail->Host = "smtp.gmail.com";
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "ssl";
		$mail->Port = 465;
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);
		$mail->IsHTML(true);
		$mail->Username = "info@izin.co.id";
		$mail->Password = "zaq!xsw@";
		$mail->SetFrom("info@izin.co.id", "Customer Service izin.co.id");
		$mail->AddAddress($Email_Address, $Email_Name);
		$mail->Subject = $Email_Subject;
		$mail->Body = $Email_Body;
		$mail->AltBody = strip_tags($Email_Body);
		$mail->addAttachment($savefileto);
		$mail->send();
		$logger->withName('emailblas')->success('Email sent successfully.', ['to' => $email, 'subject' => $subject, 'body' => strip_tags($body)]);
		echo $date_now . " " . $filename . " terkirim ke: " . $Email_Address . PHP_EOL;
	} catch (MailerExeption $e) {
		$logger->withName('emailblas')->error($e->getMessage(), ['to' => $email, 'subject' => $subject, 'body' => strip_tags($body)]);
		echo $date_now . " error: " . $e->errorMessage() . PHP_EOL; //Pretty error messages from PHPMailer
	} catch (\Exception $e) {
		$logger->withName('emailblas')->error($e->getMessage(), ['to' => $email, 'subject' => $subject, 'body' => strip_tags($body)]);
		echo $date_now . " error: " . $e->getMessage() . PHP_EOL; //Boring error messages from anything else!
	}
	// End of Send_Mail

} //while ( $LgnDet = $dbs->getAssoc($rcLgnDet) ){
