<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'] . '/system/CrcConfig.php';

class Welcome extends CI_Controller
{
	protected $fileExtension = '.png';

	public function index()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('id', '', 'required');
		$this->form_validation->set_rules('correction', '', 'alpha');
		$this->form_validation->set_rules('signature', '', '');

		if ($this->form_validation->run() == FALSE) {
			$result = $this->Agreement_model->getId();
			if (empty($result)) {
				show_error('Agreement was not found.', 404);
			}

			if ($result['client_sign'] === "Y") {
				show_error('Agreement has been signed.', 404);
			}

			$clientSign = $this->is_signed($result['id'], $result['client_sign']);
			$izinSign = $this->is_signed($result['id'], $result['izin_sign']);

			if (!empty($clientSign)) {
				$check = $this->read_sign($clientSign['id'] . $clientSign['file_ext']);
				if ($check) {
					$clientSign['base64_img'] = 'data:image/png;base64,' . base64_encode($check);
				}
			}

			if (!empty($izinSign)) {
				$check = $this->read_sign($izinSign['id'] . $izinSign['file_ext']);
				if ($check) {
					$izinSign['base64_img'] = 'data:image/png;base64,' . base64_encode($check);
				}
			}

			$this->load->view('agreement', [
				'data' => $result,
				'id' => $result['id'],
				'clientSign' => $clientSign,
				'izinSign' => $izinSign,
				'title' => 'Agreement ' . $result['inv_num']
			]);
		} else {
			$id = $this->input->post('id');
			$clientPic = $this->input->post('client_pic');
			$invNo = $this->input->post('inv_num');
			$correction = $this->input->post('correction');
			$signatureData = $this->input->post('signature');
			$company = $this->input->post('company');
			$responseTime = date('Y-m-d H:i:s');

			if ($correction === '') {
				$base64Image = explode(",", $signatureData)[1];

				if ($this->isBase64Encoded($base64Image)) {
					$fileName = md5(uniqid(microtime()));
					$saveImage = $this->save_sign($fileName . $this->fileExtension, $base64Image);

					if ($saveImage != FALSE) {
						$this->db->insert('apps_agreementsign', [
							'id' => $fileName,
							'file_ext' => $this->fileExtension,
							'file_size' => $saveImage['file_size'],
							'id_agreement' => $id,
							'signed_date' => $responseTime
						]);

						if ($this->db->affected_rows()) {
							$action = $this->Agreement_model->clientSignSubmit($fileName, $id, $invNo);
							if ($action) {
								// notify admin
								if ($company === 'PT INVESTASI INDO ASIA') {
									$message = "Dear Admin, <br><br> Customer $clientPic has been responded and signed the agreement on INV: $invNo.";
								} else {
									$message = "Dear Admin, <br><br> Customer $clientPic telah memberikan response dengan melakukan tanda tangan pada INV: $invNo.";
								}
								$this->notifyEmail($message);
								$this->notifyWA(strip_tags($message));
							}
						}
					}
				} else {
					show_error('Invalid Signature Data.');
				}
			} else {
				$action = $this->Agreement_model->clientRevisionSubmit($id);
				if ($action) {
					// notify admin
					$message = "Dear Admin, <br><br> Customer $clientPic telah memberikan response dengan mengajukan revisi pada INV: $invNo.";
					$this->notifyEmail($message);
					$this->notifyWA(strip_tags($message));
				}
			}

			redirect(current_url() . '?id=' . $id);
		}
	}

	private function isBase64Encoded(string $str): bool
	{
		if (base64_encode(base64_decode($str, true)) === $str) {
			return true;
		} else {
			return false;
		}
	}

	private function init_flysystem()
	{
		// The internal adapter
		$adapter = new \League\Flysystem\Local\LocalFilesystemAdapter(
			// Determine root directory
			$_SERVER['UNGGAH'] . '/Signature',
			// Customize how visibility is converted to unix permissions
			\League\Flysystem\UnixVisibility\PortableVisibilityConverter::fromArray([
				'file' => [
					'public' => 0640,
					'private' => 0604,
				],
				'dir' => [
					'public' => 0740,
					'private' => 7604,
				]
			]),
			// Write flags
			LOCK_EX,
			// How to deal with links, either DISALLOW_LINKS or SKIP_LINKS
			// Disallowing them causes exceptions when encountered
			\League\Flysystem\Local\LocalFilesystemAdapter::DISALLOW_LINKS
		);
		// The FilesystemOperator
		return new \League\Flysystem\Filesystem($adapter);
	}

	/**
	 * Save signed image to file
	 *
	 * @param string $fileName
	 * @param string $base64Image
	 * @return mixed
	 */
	private function save_sign(string $fileName, string $base64Image)
	{
		$filesystem = $this->init_flysystem();

		try {
			$decodedImage = base64_decode($base64Image);
			$filesystem->write($fileName, $decodedImage);
		} catch (\League\Flysystem\FilesystemException | \League\Flysystem\UnableToReadFile $e) {
			log_message('error', $e->getMessage());
			return false;
		}

		// Retrieving metadata
		return [
			'file_name' => $fileName,
			'file_size' => $filesystem->fileSize($fileName),
			'mime_type' => $filesystem->mimeType($fileName)
		];
	}

	/**
	 * Check if signed file is exist
	 *
	 * @param string $fileName
	 * @return mixed
	 */
	private function read_sign(string $fileName)
	{
		$filesystem = $this->init_flysystem();

		try {
			return $filesystem->read($fileName);
		} catch (\League\Flysystem\FilesystemException | \League\Flysystem\UnableToReadFile $e) {
			log_message('error', $e->getMessage());
			return null;
		}
	}

	/**
	 * Check if signed
	 * Get the information of signature file & signature date
	 *
	 * @param string $agreementId
	 * @param string|null $id
	 * @return mixed
	 */
	private function is_signed(string $agreementId, $id = NULL)
	{
		if (empty($id))
			return NULL;

		$check = $this->db->where('id_agreement', $agreementId)->where('id', $id)->get('apps_agreementsign', 1);
		if ($check->num_rows() == 0)
			return NULL;

		return $check->row_array();
	}

	/**
	 * Send email notification to admin
	 *
	 * @param string $message
	 * @return void
	 */
	private function notifyEmail(string $message)
	{
		//Create an instance; passing `true` enables exceptions
		$mail = new \PHPMailer\PHPMailer\PHPMailer(true);

		try {
			//Server settings
			$mail->SMTPDebug = \PHPMailer\PHPMailer\SMTP::DEBUG_OFF;
			$mail->isSMTP();
			$mail->Host = EMAIL_HOST;
			$mail->SMTPAuth = true;
			$mail->Username = EMAIL_USERNAME;
			$mail->Password = EMAIL_PASSWORD;
			$mail->SMTPSecure = EMAIL_SMTPSECURE;
			$mail->Port = EMAIL_PORT;
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			//Recipients
			$mail->setFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
			$mail->addAddress('cs@izin.co.id', EMAIL_SETFROM_NAME);
			$mail->addCC(EMAIL_CC);
			$mail->addBCC('dev@curalib.com', 'Curalib Dev');

			//Content
			$mail->isHTML(true);
			$mail->Subject = 'Agreement Response';
			$mail->Body = $message;
			$mail->AltBody = strip_tags($message);

			$mail->send();
		} catch (\PHPMailer\PHPMailer\Exception $e) {
			log_message("error", "Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
		}

		return;
	}

	/**
	 * Send whatsapp notification to admin
	 *
	 * @param string $message
	 * @return void
	 */
	private function notifyWA(string $message = 'Hi Ded')
	{
		$client = new \GuzzleHttp\Client([
			'base_uri' => 'https://solo.wablas.com',
			'verify' => false,
			'headers' => [
				'Authorization' => 'U8FO7CN5zbVFqLm9loIagw8PDSKddWtCEMRnPbKnUSuduOhQxwvh2fd9qwzG8AaX',
				'Accept' => 'application/json',
			]
		]);

		$res = $client->request('POST', '/api/send-message', [
			'http_errors' => false,
			'form_params' => [
				'phone' => WA_ADMIN, // no hp admin
				'message' => $message
			]
		]);

		$statusCode = $res->getStatusCode();
		$statusMessage = $res->getReasonPhrase();
		$body = $res->getBody();
		if ($statusCode <> 200) {
			log_message('error', 'WABLAS error');
		}

		if (strpos($res->getHeader('Content-Type')[0], 'application/json') !== false) {
			$body = json_decode($body);
		}

		return;
	}

	/**
	 * Save pdf
	 *
	 * @return void
	 */
	private function save_pdf()
	{
		$result = $this->Agreement_model->getId();
		if (empty($result)) {
			show_error('Agreement was not found.', 404);
		}

		$clientSign = $this->is_signed($result['id'], $result['client_sign']);
		$izinSign = $this->is_signed($result['id'], $result['izin_sign']);

		if (!empty($clientSign)) {
			$check = $this->read_sign($clientSign['id'] . $clientSign['file_ext']);
			if ($check) {
				$clientSign['base64_img'] = 'data:image/png;base64,' . base64_encode($check);
			}
		}

		if (!empty($izinSign)) {
			$check = $this->read_sign($izinSign['id'] . $izinSign['file_ext']);
			if ($check) {
				$izinSign['base64_img'] = 'data:image/png;base64,' . base64_encode($check);
			}
		}

		// instantiate and use the dompdf class
		$options = new \Dompdf\Options();
		$dompdf = new \Dompdf\Dompdf($options);
		$dompdf->loadHtml(
			$this->load->view('agreement_pdf', [
				'data' => $result,
				'id' => $result['id'],
				'clientSign' => $clientSign,
				'izinSign' => $izinSign
			], true)
		);
		$dompdf->setPaper('A4');
		$dompdf->render();

		$fileName = $result['inv_num'] . 'agreement.pdf';
		$dompdf->stream($fileName);
	}
}
