<?php

class Agreement_model extends CI_Model
{
	protected $table = "apps_agreement";

	public function getId()
	{
		if (empty($_GET["id"])) {
			return [];
		}

		$now = date("Y-m-d");
		$query = $this->db->select("a.*, b.total_due")
			->from($this->table . " as a")
			->join("apps_invdet as b", "b.id = a.inv_id", "inner")
			->where("a.id", $_GET["id"])
			->where("a.discard", false)
			->where("DATEDIFF('$now', CAST(a.created_at AS DATE)) <=", 3) // active link only three days
			->limit(1)
			->get();

		return $query->row_array();
	}

	public function clientSignSubmit(string $signatureId, string $agreementId, string $invNo)
	{
		$this->db->where("id", $agreementId)
			->where("discard", 0)
			->update($this->table, ["client_sign" => $signatureId]);

		if ($this->db->affected_rows()) {
			// update agree_status on invdet
			$this->db->update('apps_invdet', ['agree_status' => 1], "inv_num = '$invNo'");
			return true;
		} else {
			return false;
		}
	}

	public function clientRevisionSubmit(string $agreementId)
	{
		$this->db->where("id", $agreementId)
			->where("discard", 0)
			->update($this->table, ["correction_request" => date("Y-m-d")]);

		return $this->db->affected_rows();
	}
}
