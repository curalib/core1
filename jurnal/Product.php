<?php

require_once "MyJurnal.php";
require_once "Company.php";

class Product extends MyJurnal
{
    private function sell_account_name(string $kode) : string
    {
        $accountName = 'Pendapatan';

        // Pendapatan Custome Package
        $custom = ['CUSTOM', 'AHU', 'API_NIK', 'APIINIK', 'BDP', 'BRK', 'GBP', 'HQM', 'KITAS', 'KP', 'LKPM', 'NPWP', 'OSS', 'PA', 'PIRT', 'PKP', 'SINIB', 'TDUP'];
        // Pendapatan Merek
        $merek = ['HKIC', 'HKIM', 'PHKI'];
        // Pendapatan PT/CV
        $pt_cv = ['CVF', 'CVFVL', 'CVFVP', 'E4.0', 'FF', 'FL', 'KOPF', 'PKM', 'PMA', 'PMAAPI', 'PPF', 'PTE', 'PTEP', 'PTES', 'PTF', 'PTFIUJK1', 'PTFVL', 'PTFVLAPI', 'PTFVP', 'PTFVPAPI', 'PTL', 'PTP', 'PTS', 'PTSB', 'PTSP', 'TPT', 'YYS'];

        if (in_array($kode, $custom)) $accountName = 'Pendapatan Custome Package';
        if (in_array($kode, $merek)) $accountName = 'Pendapatan Merek';
        if (in_array($kode, $pt_cv)) $accountName = 'Pendapatan PT/CV';

        return $accountName;
    }

    private function buy_account_name(string $kode) : string
    {
        $accountName = 'Pendapatan';

        // Beban Custom Package
        $custom = ['CUSTOM', 'AHU', 'API_NIK', 'APIINIK', 'BDP', 'BRK', 'GBP', 'HQM', 'KITAS', 'KP', 'LKPM', 'NPWP', 'OSS', 'PA', 'PIRT', 'PKP', 'SINIB', 'TDUP'];
        // Beban Merek
        $merek = ['HKIC', 'HKIM', 'PHKI'];
        // Beban PT/CV
        $pt_cv = ['CVF', 'CVFVL', 'CVFVP', 'E4.0', 'FF', 'FL', 'KOPF', 'PKM', 'PMA', 'PMAAPI', 'PPF', 'PTE', 'PTEP', 'PTES', 'PTF', 'PTFIUJK1', 'PTFVL', 'PTFVLAPI', 'PTFVP', 'PTFVPAPI', 'PTL', 'PTP', 'PTS', 'PTSB', 'PTSP', 'TPT', 'YYS'];

        if (in_array($kode, $custom)) $accountName = 'Beban Custom Package';
        if (in_array($kode, $merek)) $accountName = 'Beban Merek';
        if (in_array($kode, $pt_cv)) $accountName = 'Beban PT/CV';

        return $accountName;
    }

    public function init() // init package
    {
        $conn = $this->Connection();

        $sql = "SELECT a.package_id as kode, a.package_name as nama, (SELECT price FROM izin_apps_package WHERE kode = a.package_id) AS price, (SELECT descs FROM izin_apps_package WHERE kode = a.package_id) AS descs ";
        $sql.= "FROM izin_apps_lead a ";
        $sql.= "GROUP BY a.package_id, a.package_name ";

        $result = $conn->query($sql);

        while ($row = $result->fetch_assoc())
        {
            $product = [];
            $product['unit_name'] = 'Buah';
            $product['product_code'] = $row['kode'];
            $product['custom_id'] = $row['kode'];
            $product['name'] = $row['nama'];
            $product['description'] = strip_tags($row['descs']);
            $product['is_bought'] = false;
            $product['track_inventory'] = false;
            $product['is_sold'] = true;
            $product['sell_account_name'] = $this->sell_account_name($row['kode']);
            if ($row['price'] <> 0) $product['sell_price_per_unit'] = $row['price'];
            //$product['buy_account_name'] = $this->buy_account_name($row['kode']);

			$company = new Company();
			$companies = $company->getAll();
			while ($com = $companies->fetch_assoc())
			{
				$this->setTenant($com['account_number']);

                $isExist = $this->is_exist($row['kode']); // dibuang jika sudah di insert ke jurnal
                if (! $isExist)
                {
                    $response = $this->do_request('POST', '/public/jurnal/api/v1/products', ['product' => $product]);
                    $this->log_jurnal('product', $product, $response);
                }
			}
        }

        return;
    }
}
