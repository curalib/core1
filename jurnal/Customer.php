<?php

require_once "MyJurnal.php";

class Customer extends MyJurnal
{
    public function get(?int $id)
    {
        $uri = (empty($id)) ? '/public/jurnal/api/v1/customers' : '/public/jurnal/api/v1/customers/' . $id;

        return $this->do_request('GET', $uri);
    }

    public function batch_insert(array $datas)
    {
        foreach ($datas as $company => $data)
        {
            $this->setTenant($company);

            foreach (array_chunk($data, 100) as $customerss)
            {
                $customers = [];
                for ($i=0; $i < count($customerss); $i++)
                {
                    $kode = $customerss[$i]['customer']['custom_id'];
                    $isExist = $this->is_exist($kode); // dibuang jika sudah di insert ke jurnal
                    if (! $isExist)
                    {
                        $customers[] = $customerss[$i];
                    }
                }

                if (count($customers) > 0)
                {
                    $response = $this->do_request('POST', '/public/jurnal/api/v1/customers/batch_create', ['customers' => $customers]);
                    $this->log_jurnal('customers', $customers, $response);
                }
            }
        }
    }
}
