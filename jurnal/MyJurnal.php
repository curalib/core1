<?php

use Carbon\Carbon;
use Psr\Http\Message\ResponseInterface;

class MyJurnal
{
    protected $requestId;

    protected $statusCode;

    protected $responseOk = false;

    protected $responseMessages = "";

    protected $id;

    protected $apiKey = "";

    protected $clientId = "";

    protected $clientSecret = "";

    protected $baseUri = "https://api.mekari.com";

    private $HOST = "localhost";
    private $USER = "izinapps";
    private $PASSWD = "&)xq1YGX]n(LDYg";
    private $DBNAME = "izin_1";

    protected function setTenant(string $accountNo)
    {
        $conn = $this->Connection();
        $sql = "SELECT * FROM izin_master_company WHERE izin_master_company.jurnal_apikey <> '' AND izin_master_company.account_number = '$accountNo' LIMIT 1";
        $result = $conn->query($sql)->fetch_assoc();

        if (! empty($result)) {
            $this->id = $result['id'];
            $this->apiKey = $result['jurnal_apikey'];
            $this->clientId = $result['jurnal_client_id'];
            $this->clientSecret = $result['jurnal_client_secret'];
        }
    }

    /**
     * Generate authentication headers based on method and path
     */
    protected function generate_headers($method, $pathWithQueryParam)
    {
        $datetime = Carbon::now()->toRfc7231String();
        $request_line = "{$method} {$pathWithQueryParam} HTTP/1.1";
        $payload = implode("\n", ["date: {$datetime}", $request_line]);
        $digest = hash_hmac('sha256', $payload, $this->clientSecret, true);
        $signature = base64_encode($digest);

        return [
            'Accept' => 'application/json',
            'Content-Type'  => 'application/json',
            'Date' => $datetime,
            'Authorization' => "hmac username=\"{$this->clientId}\", algorithm=\"hmac-sha256\", headers=\"date request-line\", signature=\"{$signature}\""
        ];
    }

    private function generateSignature($method, $path) {
        $dateString = gmdate('D, d M Y H:i:s') . ' GMT';
        $requestLine = $method . ' ' . $path . ' HTTP/1.1';
        $dataToSign = 'date: ' . $dateString . "\n" . $requestLine;
        $signature = base64_encode(hash_hmac('sha256', $dataToSign, $this->clientSecret, true));

        return [
            'signature' => $signature,
            'date' => $dateString
        ];
    }

    public function Connection()
    {
        $connect = new mysqli($this->HOST, $this->USER, $this->PASSWD, $this->DBNAME);
        if ($connect->connect_error) {
            die("Connection failed: " . $connect->connect_error);
        } else {
            return $connect;
        }
    }

    protected function do_request(string $method, string $uri, array $data = [])
    {
        $this->requestId = uniqid();
        $this->responseOk = false;
        $this->statusCode = null;

        $signatureData = $this->generateSignature($method, $uri);

        $headers    = [
            //'X-Idempotency-Key' => '1234'
            'Authorization' => 'hmac username="' . $this->clientId . '", algorithm="hmac-sha256", headers="date request-line", signature="' . $signatureData['signature'] . '"',
            'Date' => $signatureData['date'],
            'Accept' => 'application/json'
        ];

        $client = new GuzzleHttp\Client(['base_uri' => $this->baseUri]);
        $response = $client->request($method, $uri, [
            'http_errors' => false,
            'headers' => $headers,
            //'headers'   => array_merge($this->generate_headers($method, $uri), $headers),
            //'verify' => false,
            //'delay' => 250, // 1/4 s
            'json' => $data
        ]);

        return $this->set_response($response);
    }

    private function set_response(ResponseInterface $response)
    {
        $body = [];
        if (strpos($response->getHeader('content-type')[0], 'application/json') !== false) {
            $body = json_decode($response->getBody(), true);
        }

        $statusCode = $response->getStatusCode();
        $this->statusCode = $statusCode;
        $this->responseMessages = $response->getReasonPhrase();
        if (substr((string)$statusCode, 0, 2) == '20') {
            $this->responseOk = true;
            $data = ['success' => true, 'data' => $body];
        } else {
            if ($this->statusCode === 401) // 401 Unauthorized
            {
                //$this->responseMessages = $body['message'];
                $this->responseMessages = 'Invalid authentication credentials';
            } elseif ($this->statusCode === 403) // 403 Forbidden
            {
                $this->responseMessages = $body['message'];
            } elseif ($this->statusCode === 406) // 406 Not Acceptable
            {
                $this->responseMessages = $body['errors'];
            }

            $data = ['success' => false, 'message' => $this->responseMessages, 'data' => $body];
        }

        return json_encode($data);
    }

    protected function log_jurnal(string $names, array $request, string $response)
    {
        if ($names === 'customers') {
            $batch = true;
            $tableName = 'izin_apps_client';
            $key = 'display_name';
            $name = 'customer';
        } elseif ($names === 'customer') {
            $batch = false;
            $tableName = 'izin_apps_client';
            $key = 'display_name';
            $name = 'customer';
        } elseif ($names === 'sales_invoice') {
            $batch = false;
            $tableName = 'izin_apps_invdet';
            $key = 'transaction_no';
            $name = 'sales_invoice';
        } elseif ($names === 'receive_payment') {
            $batch = false;
            $tableName = 'izin_apps_invbayar';
            $key = 'custom_id';
            $name = 'receive_payment';
        } elseif ($names === 'products') {
            $batch = false;
            $tableName = 'izin_apps_package';
            $key = 'product_code';
            $name = 'product';
        } elseif ($names === 'product') {
            $batch = false;
            $tableName = 'izin_apps_package';
            $key = 'product_code';
            $name = 'product';
        } else {
            return;
        }

        // request
        $this->save_reqres('request', json_encode([$names => $request]), 200);

        $arrResponse = json_decode($response, true);
        if ($batch === false) {
            // response
            $this->save_reqres('response', json_encode($arrResponse['data']), $this->statusCode);

            if ($arrResponse['success']) {
                $data = $arrResponse['data'][$name];
                $izinId = $request[$key];
                $jurnalId = $data['id'];

                $this->save_id($tableName, $izinId, $jurnalId);
            }
        } else {
            for ($i = 0; $i < count($arrResponse['data'][$names]); $i++) {
                $data = $arrResponse['data'][$names][$i][$name];

                // response batch
                $this->save_reqres('response', json_encode($data), $data['status']);

                if ($data['status'] === 201) // created
                {
                    $izinId = $request[$i][$name][$key];
                    $jurnalId = $data['content']['id'];

                    $this->save_id($tableName, $izinId, $jurnalId);
                }
            }
        }
    }

    private function save_id(string $tableName, ?string $izinId, ?int $jurnalId)
    {
        $conn = $this->Connection();

        $sql = "INSERT INTO jurnal_id (table_name, ref_id, jurnal_id, company_id) ";
        $sql .= "VALUES ('$tableName', '$izinId', '$jurnalId', '" . $this->id . "');";
        $conn->query($sql);
    }

    private function save_reqres(string $code, string $json, ?int $statusCode)
    {
        $conn = $this->Connection();

        $sql = "INSERT INTO log_jurnal (id, code, `status`, `json`, company_id) ";
        $sql .= "VALUES ('" . $this->requestId . "', '$code', '" . $statusCode . "', '$json', '" . $this->id . "');";
        $conn->query($sql);
    }

    protected function is_exist($id): bool
    {
        $conn = $this->Connection();

        $sql = "SELECT * FROM jurnal_id WHERE company_id = '" . $this->id . "' AND ref_id = '$id' LIMIT 1";
        $result = $conn->query($sql)->fetch_assoc();

        return (! empty($result)) ? TRUE : FALSE;
    }
}
