<?php

require_once "MyJurnal.php";
require_once "Customer.php";

class Invoice extends MyJurnal
{
    public function delInvoice($invoiceId = null)
    {
        if (empty($invoiceId))
        {
            // delete all
            $conn = $this->Connection();
            $sql = "SELECT * FROM jurnal_id WHERE `table_name` = 'izin_apps_invdet' ";
            $result = $conn->query($sql);

            while ($row = $result->fetch_assoc())
            {
                $sql = "SELECT * FROM izin_apps_invdet WHERE inv_num = '" . $row['ref_id'] . "' AND bank_acc <> '' LIMIT 1 ";
                $inv = $conn->query($sql)->fetch_assoc();
                if (! empty($inv))
                {
                    $this->setTenant($inv['bank_acc']);

                    $this->do_request('DELETE', '/public/jurnal/api/v1/sales_invoices/' . $row['jurnal_id']);
                    if ($this->responseOk)
                    {
                        $this->remove_sync($row['ref_id']);
                    }
                }
            }
        }
        else
        {
            // delete by id
            $conn = $this->Connection();

            $sql = "SELECT * FROM izin_apps_invdet WHERE id = '$invoiceId' AND bank_acc <> '' LIMIT 1 ";
            $result = $conn->query($sql)->fetch_assoc();

            if (! empty($result))
            {
                $this->setTenant($result['bank_acc']);

                $this->do_request('DELETE', '/public/jurnal/api/v1/sales_invoices/' . $invoiceId);
                if ($this->responseOk)
                {
                    $this->remove_sync($result['inv_num']);
                }
            }
        }

        return $this->responseMessages;
    }

    public function addInvoice(string $invoiceId)
    {
        $conn = $this->Connection();

        $sql = "SELECT izin_apps_invdet.id AS idinv, izin_apps_invdet.*, izin_apps_lead.*, izin_apps_package.* ";
        $sql.= "FROM izin_apps_invdet ";
        $sql.= "INNER JOIN izin_apps_lead ON izin_apps_invdet.id_lead = izin_apps_lead.id_lead ";
        $sql.= "INNER JOIN izin_apps_package ON izin_apps_package.kode = izin_apps_lead.package_id ";
        $sql.= "WHERE izin_apps_invdet.bank_acc <> '' AND izin_apps_invdet.tipe <> 'PREVIEW' ";
        $sql.= "AND izin_apps_invdet.id = '$invoiceId' LIMIT 1";

        $result = $conn->query($sql);

        $customers = [];
        $invoices = [];
        while ($row = $result->fetch_assoc())
        {
            $salesPerson = $row['sales_name'];
            $bankAccount = $row['bank_acc'];

            // is @jurnal exist?
            $this->setTenant($bankAccount);
            $isJurnalExist = $this->is_jurnal_exist($row['inv_num'], $row['idinv'], $row['paid']);
            if ($isJurnalExist) {
                continue;
            }

            /** Customers */
            $customer = [];
            $customer['custom_id'] = $row['client_id'];
            $customer['display_name'] = $row['client_id'];
            $customer['first_name'] = $row['client_name'];
            $customer['mobile'] = $row['client_wa'];
            $emailErr = '';
            $email = $row['client_email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailErr = 'Invalid email format';
            }
            if ($emailErr === '') $customer['email'] = $email;
            $customer['source'] = 'api';
            $customers[$bankAccount][] = [
                'customer' => $customer
            ];

            /** Invoices */
            $invoice = [];
            $invoice['transaction_date'] = ($row['issue_date'] < '2019-11-10') ? '2019-11-10' : $row['issue_date'];
            $invoice['transaction_lines_attributes'][] = [
                'quantity' => 1,
                'rate' => $row['total_due'],
                'product_code' => $row['package_id']
            ];
            $invoice['is_shipped'] = false;
            $invoice['due_date'] = ($row['due_date'] < '2019-11-10') ? '2019-11-10' : $row['due_date'];

            $invoice['person_name'] = $row['client_id'];
            $invoice['tags'] = [$salesPerson];
            $invoice['email'] = $row['client_email'];
            $invoice['transaction_no'] = $row['inv_num'];
            $invoice['source'] = $row['source'];
            $invoice['custom_id'] = $row['idinv'];
            $invoice['paid'] = false;

            if ($row['paid'] === "Y")
            {
                $invoice['paid'] = true;
            }

            $invoices[$bankAccount][] = $invoice;

        }

        $customers = array_map("unserialize", array_unique(array_map("serialize", $customers)));
        $customer = new Customer();
        $customer->batch_insert($customers);
        sleep(2);
        $this->insert($invoices);

        return $this->responseMessages;
    }

    public function insert(array $datas)
    {
        foreach ($datas as $company => $data)
        {
            $this->setTenant($company);

            for ($i=0; $i < count($data); $i++)
            {
                $invoice = $data[$i];

                $isPaid = $invoice['paid'];
                unset($invoice['paid']);
                $response = $this->do_request('POST', '/public/jurnal/api/v1/sales_invoices', ['sales_invoice' => $invoice]);
                $this->log_jurnal('sales_invoice', $invoice, $response);

                if ($this->responseOk)
                {
                    if ($isPaid)
                    {
                        $this->setPaid($invoice['transaction_no']);
                    }

                    $this->update_sync($invoice['custom_id']);
                }
                else
                {
                    $arrResponse = json_decode($response, true);
                    $this->responseMessages = $invoice['transaction_no'] . ' ' . $arrResponse['message'];
                }
            }
        }
    }

    private function setPaid(string $invoiceNo)
    {
        $conn = $this->Connection();
        $sql = "SELECT izin_apps_invdet.*, izin_apps_lead.sales_name ";
        $sql.= "FROM izin_apps_invdet ";
        $sql.= "INNER JOIN izin_apps_lead ON izin_apps_invdet.id_lead = izin_apps_lead.id_lead ";
        $sql.= "WHERE izin_apps_invdet.inv_num = '$invoiceNo' LIMIT 1";
        $result = $conn->query($sql);
        $row = $result->fetch_assoc();

        $verifiedBy = $row['verified_by'];
        $verifiedByVa = ($verifiedBy === 'System Virtual account BCA');

        /** Payments Receipts */
        $payment = [];
        $payment['transaction_date'] = ($row['tglbayar'] < '2019-11-10') ? '2019-11-10' : $row['tglbayar'];
        $payment['records_attributes'][] = [
            'transaction_no' => $row['inv_num'],
            'amount' => $row['total_payment']
        ];
        $payment['payment_method_name'] = 'Bank Transfer';
        $payment['deposit_to_name'] = $this->get_deposit_company($verifiedByVa);
        $payment['custom_id'] = $row['inv_num'];
        $payment['tags'] = [$row['sales_name']];

        $response = $this->do_request('POST', '/public/jurnal/api/v1/receive_payments', ['receive_payment' => $payment]);
        $this->log_jurnal('receive_payment', $payment, $response);

        if (! $this->responseOk)
        {
            if ($this->statusCode === 422) // 422 Unprocessable Entity
            {
                $arrResponse = json_decode($response, true);
                $this->responseMessages = $row['inv_num'] . ' ' . $arrResponse['records'];
            }
        }
    }

    private function get_deposit_company(bool $byVA)
    {
        $depositName = 'Bank Account';

        if ($this->apiKey === '88007cbf6cccebe0cdea9c26c4d1e044') // Wahana Rahmat Nusa
        {
            $depositName = ($byVA) ? 'Bank BCA 2020' : 'Bank BCA 3888';
        }
        elseif ($this->apiKey === 'f4b5802bd86d72731a630bf6b9b660be') // Ijin Usaha
        {
            $depositName = 'Bank BCA 8282';
        }
        elseif ($this->apiKey === 'c6e728a7b9b0660ae134344e332581a3') // Wahana Ijin Nusantara
        {
            $depositName = 'Bank BCA';
        }
        else // Mitra Solusi Hukum, Sahabat Usaha Anda, Wahana Izin Indonesia, Investasi Indo Asia
        {
            $depositName = 'Rekening Bank';
        }

        return $depositName;
    }

    private function update_sync(string $invoiceId)
    {
        $conn = $this->Connection();
        $sql = "UPDATE izin_apps_invdet SET izin_apps_invdet.syncstatus_jurnal = '2' WHERE izin_apps_invdet.id = '$invoiceId' ";
        $conn->query($sql);
        $conn->close();
    }

    private function remove_sync(string $invoiceNo)
    {
        $conn = $this->Connection();
        $sql1 = "UPDATE izin_apps_invdet SET izin_apps_invdet.syncstatus_jurnal = '0' WHERE izin_apps_invdet.inv_num = '$invoiceNo' ";
        $conn->query($sql1);

        $sql2 = "DELETE FROM jurnal_id WHERE jurnal_id.`table_name` IN ('izin_apps_invdet','izin_apps_invbayar') AND jurnal_id.ref_id = '$invoiceNo' AND jurnal_id.company_id = '" . $this->id . "' ";
        $conn->query($sql2);
        $conn->close();
    }

    public function getInvoice(string $id)
    {
        $conn = $this->Connection();

        $sql = "SELECT izin_apps_invdet.id as idinv, izin_apps_invdet.*,izin_apps_lead.*,izin_apps_package.*,izin_apps_packagework.* ";
        $sql.= "FROM izin_1.izin_apps_invdet ";
        $sql.= "inner join izin_1.izin_apps_lead on izin_1.izin_apps_invdet.id_lead = izin_1.izin_apps_lead.id_lead ";
        $sql.= "inner join izin_1.izin_apps_package on izin_1.izin_apps_package.kode = izin_1.izin_apps_lead.package_id ";
        $sql.= "inner join izin_1.izin_apps_packagework on izin_1.izin_apps_invdet.id_lead = izin_1.izin_apps_packagework.id_lead ";
        $sql.= "where izin_1.izin_apps_invdet.id = '$id' ";

        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    public function updateDoneAgreement(string $id)
    {
        $conn = $this->Connection();
        $sql = "UPDATE izin_1.izin_apps_invdet set izin_1.izin_apps_invdet.agree_status = '1' where izin_1.izin_apps_invdet.id = '$id' ";

        if ($conn->query($sql))
        {
            return "Record updated successfully ".$id;
        }
        else
        {
            return "Error updating record: $id -". $conn->error;
        }
    }

    public function saveReminder($kode, $tanggal, $email)
    {
        $conn = $this->Connection();
        $sql = "INSERT INTO `izin_1`.`reminder` (`kode`, `tanggal`, `email`) VALUES ('$kode', '$tanggal', '$email');";
        if ($conn->query($sql))
        {
            return "Record updated successfully ".$kode;
        }
        else
        {
            return "Error updating record: $kode -". $conn->error;
        }
    }

    protected function is_jurnal_exist(string $invoiceNo, string $invoiceId, string $erpPaid) : bool
    {
        $response = $this->do_request("GET", "/public/jurnal/api/v1/sales_invoices/" . $invoiceNo);
        $this->log_jurnal("sales_invoice", ["transaction_no" => $invoiceNo], $response);

        if ($this->responseOk)
        {
            $arrResponse = json_decode($response, true);
            // Jika yg sudah terposting di jurnal namun belum memiliki payment
            if ($arrResponse["data"]["sales_invoice"]["has_payments"] == false && $erpPaid == "Y") {
                // posting payment ke jurnal
                $this->setPaid($invoiceNo);
            }

            // set sync jurnal to 2 and insert jurnal id
            $this->update_sync($invoiceId);

            return true;
        }
        else
        {
            // set sync jurnal to 0 and delete jurnal id
            $this->remove_sync($invoiceNo);
            return false;
        }
    }
}
