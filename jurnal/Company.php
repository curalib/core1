<?php

require_once "MyJurnal.php";

class Company extends MyJurnal
{
    /**
     * Get a company by account number BCA
     *
     * @param string $no BCA Account No.
     * @return array|null
     */
    public function getByAccount(string $no) : ?array
    {
        $conn = $this->Connection();

        $sql = "SELECT * FROM izin_master_company WHERE izin_master_company.account_number = '$no' LIMIT 1";
        $result = $conn->query($sql);

        return $result->fetch_assoc();
    }

    public function getAll()
    {
        $conn = $this->Connection();

        $sql = "SELECT * FROM izin_master_company WHERE jurnal_apikey <> '' ";
        return $conn->query($sql);
    }
}