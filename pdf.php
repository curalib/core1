<?php
require_once("system/appsConfig.php");
require_once("system/CrcConfig.php");
require_once("system/function.php");
require_once("system/db_connect.php");

ini_set("memory_limit", "-1");
ini_set('implicit_flush', true);
ini_set('output_buffering','off');
ini_set('zlib.output_compression', 0);
ini_set('session.use_strict_mode', false);
ini_set('max_execution_time', 600);
set_time_limit(600);
date_default_timezone_set(TIMESET);

if(SESS_DIR != 1) { session_save_path(SESS_DIR); }
if ( DBC_PREF != "" ) { $tblp = DBC_PREF."_"; }else{ $tblp = ""; }
list($LoginErrs,$LoginErrMess) = explode("|",ChkEnv());

ManSession("Start");

# Check IP Brute Force
$chkBruteForceIP = "select id from ".$tblp."sys_ip_ban_list where ip = '".$ip_addr."'";
$rchkBruteForceIP = $dbs->getArr($chkBruteForceIP);
if ( $rchkBruteForceIP['id'] ){
	if ( IP_BLOCK === true ) {
		# Check White List
		if ( IP_WHITE === true ){
			$ChWhtLst = "select id from ".$tblp."sys_ip_white_list where ip_addr = '".$ip_addr."'";
			$rChWhtLst = $dbs->getArr($ChWhtLst);
			if ( empty($rChWhtLst['id']) ) { $LoginErrs = "IP_LOCK"; }
		}else{
			$LoginErrs = "IP_LOCK";
		}
	}
}

# Check Domain
$PassUrl = CheckUrl(USE_DOMAIN_CHECK,$dom_array,"");
if ( $PassUrl === false ) {
	$LoginErrs = "HTML_HIJACKING";
	$alasan = "HTML hijacking attempt detected.";
	$inBan = "insert into ".$tblp."sys_ip_ban_list set
			  id = '".uniqid(md5(microtime()))."',
			  ip = '".$ip_addr."',
			  tgl = now(),
			  adding_by = 'SYSTEM',
			  alasan = '".$alasan."'";
	$dbs->getQuery($inBan);
}

if(isset($_SESSION['loginID'])) {
	#Check
	$inC = "select id, userid, ip_addr, lact_action from ".$tblp."sys_login where login_id = '".$_SESSION['loginID']."'";
	$rinC = $dbs->getArr($inC);
	if($_SESSION["loginUser"] != $rinC['userid']) {
		$LoginErrs = "WRG_SESS_UID";
	}
	if(!$LoginErrs) {
		if(IP_BASED === true) {
			if($ip_addr != $rinC['ip_addr']) {
				$LoginErrs = "WRG_SESS_IP";
			}
		}
	}
	if(!$LoginErrs) {
		if(SESS_TIME_OUT === true) {
			if(CheckIdle($rinC['lact_action'],IDLE) === false) {
				$LoginErrs = "WRG_TIME_OUT";
			}
		}
	}
} else {
	$LoginErrs = "WRG_SESS_ID";
}

if(!$LoginErrs) {

	$x = Purefy($_GET['x']);
	if ( $x == "payment" ){
		$y = Purefy($_GET['y']);
		$cFl = "select nmfile, nmfile_asli from ".$tblp."apps_invbayar where id = '".$y."'";
		$rcFl = $dbs->getArr($cFl);
		$fileDir = "../../".UPDIR."/Payment/";
		$file = $fileDir . $rcFl['nmfile'];
		$finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mimetype = finfo_file($finfo, $file);
        finfo_close($finfo);

		switch( $mimetype ){
			case "application/pdf":
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="'.$rcFl['nmfile_asli']);
				header('Content-Length: ' . filesize($file));
				@readfile($file);
			break;

			case "image/jpeg":
				echo '
					<style>
        				* {
            				margin: 0;
            				padding: 0;
        				}
        				.imgbox {
            				display: grid;
            				height: 100%;
        				}
        				.center-fit {
            				max-width: 100%;
            				max-height: 100vh;
            				margin: auto;
        				}
					</style>
					';
				echo '<div class="imgbox">';
				echo "<img class=\"center-fit\" src='data:image/jpg;base64,".base64_encode(ShowImage($fileDir, $rcFl['nmfile']))."' alt='Payment'>";
				echo '</div>';
			break;

			case "image/png":
				echo '
					<style>
        				* {
            				margin: 0;
            				padding: 0;
        				}
        				.imgbox {
            				display: grid;
            				height: 100%;
        				}
        				.center-fit {
            				max-width: 100%;
            				max-height: 100vh;
            				margin: auto;
        				}
					</style>
					';
				echo '<div class="imgbox">';
				echo "<img class=\"center-fit\" src='data:image/jpg;base64,".base64_encode(ShowImage($fileDir, $rcFl['nmfile']))."' alt='Payment'>";
				echo '</div>';
			break;

			default:
				header('Content-Type: application/octet-stream');
				header('Content-Length: '.filesize($file));
				header('Content-Disposition: attachment; filename="'.$rcFl['nmfile_asli'].'"');
				header('Content-Transfer-Encoding: binary');

				$filesrc = @fopen($file,"rb");
				if ($filesrc) {
	  				while(!feof($filesrc)) {
	    				print(fread($filesrc, 1024*8));
	    				flush();
	    				if (connection_status()!=0) {
	      					@fclose($filesrc);
	      					die();
	    				}
	  				}
	  				@fclose($filesrc);
				}
				echo "<a href=\"javascript:window.open('','_self').close();\">close</a>";
			break;
		}
	}else{
		if($_SESSION['agreement_pdf'])
		{
			$dir = "../../".UPDIR. "/Agreement";
			$pdffile = $dir."/".$_SESSION['agreement_pdf'];
			$pdffilename = preg_replace("/\W/","_",$_SESSION['agreement_pdf']);
			if ( is_file($pdffile) ){
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="'.$_SESSION['agreement_pdf'].'.pdf"');
				header('Content-Length: ' . filesize($pdffile));
				@readfile($pdffile);
			}else{
				echo "AGREEMENT NOT FOUND!";
			}
		}
		elseif($_SESSION["LeadChStat"])
		{
			$cInvSkr = "select inv_num, name from ".$tblp."apps_invdet where id_lead = '".$_SESSION["LeadChStat"]."'";
			$rcInvSkr = $dbs->getArr($cInvSkr);

			$dir = "../../".UPDIR. "/Invoices";
			$pdffile = $dir."/".$rcInvSkr['inv_num'].".pdf";
			$pdffilename = preg_replace("/\W/","_",$rcInvSkr['name']);
			if ( is_file($pdffile) ){
				header('Content-type: application/pdf');
				header('Content-Disposition: inline; filename="'.$pdffilename.'.pdf"');
				header('Content-Length: ' . filesize($pdffile));
				@readfile($pdffile);
			}else{
				echo "INVOICE NOT FOUND!";
			}
		}
		else
		{
			echo "FILE NOT FOUND!";
		}
	}

}else{
	echo "WHAT A MESS! ";
}
