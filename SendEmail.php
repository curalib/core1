<?php

date_default_timezone_set('Asia/Jakarta');

require_once "system/CrcConfig.php";
require_once "vendor/autoload.php";

use PHPMailer\PHPMailer\Exception as MailerExeption;
use PHPMailer\PHPMailer\PHPMailer;

class SendEmail
{
    private $HOST = DBC_MHOST;
    private $USER = DBC_MUSER;
    private $PASSWD = DBC_MPWD;
    private $DBNAME = DBC_MSDB;

    private function Connection()
    {
        $connect =  new mysqli($this->HOST, $this->USER, $this->PASSWD, $this->DBNAME);
        if ($connect->connect_error) {
            die("Connection failed: " . $connect->connect_error);
        } else {
            return $connect;
        }
    }

    private function getData()
    {
        $conn = $this->Connection();
        $sql = "SELECT * FROM izin_1.reminder";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    private function getDataTrkFileByUploaderId($id)
    {
        $conn = $this->Connection();
        $sql = "SELECT izin_1.izin_apps_trkfile.* FROM izin_1.izin_apps_trkfile where izin_1.izin_apps_trkfile.trkfile_uploader_id = '" . $id . "';";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    private function getDataTrkListByID($id)
    {
        $conn = $this->Connection();
        $sql = "SELECT izin_1.izin_apps_trklist.*,izin_1.izin_apps_lead.client_email FROM izin_1.izin_apps_trklist inner join izin_1.izin_apps_lead on izin_1.izin_apps_lead.id_lead = izin_1.izin_apps_trklist.lead_id where izin_1.izin_apps_trklist.id_trklist = '" . $id . "';";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    public function EmailHaki(string $email, string $choice, string $id_trklist)
    {
        $trklist = $this->getDataTrkListByID($id_trklist);
        $merek = $trklist['reminder_haki_merek'];
        $kelas = $trklist['reminder_haki_kelas'];
        $link = $trklist['reminder_haki_link'];

        $uploaderId = $id_trklist . '_attachment_haki_' . $choice;
        $file = $this->getDataTrkFileByUploaderId($uploaderId);

        switch ((string) $choice) {
            case '0':
                $this->sendHakiPengajuan($email, $merek, $kelas, $file);
                break;

            case '1':
                $this->sendLinkProgress($email, $merek, $kelas, $link);
                break;

            case '2':
                $this->sendPublikasi($email, $merek, $kelas, $file);
                break;

            case '3':
                $this->sendPemeriksaan($email, $merek, $kelas);
                break;

            case '4':
                $this->sendHakiSelesai($email, $merek, $file);
                break;

            default:
                # code...
                break;
        }
    }

    public function EmailTax(string $email, string $nama, string $choice, string $id_trklist)
    {
        $trklist = $this->getDataTrkListByID($id_trklist);
        $date = $trklist['reminder_tax_' . $choice];

        $uploaderId = $id_trklist . '_attachment_tax_' . $choice;
        $file = $this->getDataTrkFileByUploaderId($uploaderId);

        if (empty($file))
            return;

        switch ((string) $choice) {
            case '0':
                $this->sendTaxEFIN($email, $nama, $file);
                break;

            case '1':
                $this->sendTaxSPPKP($email, $nama, $file);
                break;

            case '2':
                $this->sendTaxSKPP55($email, $nama, $file);
                break;

            case '3':
                $this->sendTaxLPB($email, $nama, $date, $file);
                break;

            case '4':
                $this->sendTaxLPT($email, $nama, $date, $file);
                break;

            case '5':
                $this->sendTaxPB($email, $nama, $date, $file);
                break;

            default:
                # code...
                break;
        }
    }

    public function sendHakiPengajuan(string $email, string $merek, int $kelas, array $file)
    {
        $subject = "Update Status Pendaftaran Merek $merek Kelas $kelas - Pengajuan Permohonan";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Sehubungan dengan dilakukannya pendaftaran atas Merek $merek Kelas $kelas, kami ingin memberitahukan Anda bahwa status pendaftaran merek tersebut saat ini sedang dalam proses Pengajuan Permohonan.</p>";
        $body .= "<p>Berikut kami lampirkan berkas laporan status pendaftaran merek tersebut. Apabila ada perkembangan lebih lanjut mengenai merek yang didaftarkan tersebut akan kami sampaikan kepada anda.</p>";
        $body .= "<p>Tahapan Pendaftaran Merek:</p>";
        $body .= "<p>Tahap Pra Permohonan:</p>";
        $body .= "<ol>";
        $body .= "<li>Pengecekan ketersediaan dan kemungkinan dapat terdaftarnya Merek dan Barang/Jasa dimaksud.</li>";
        $body .= "<li>Persiapan persyaratan yang diperlukan.</li>";
        $body .= "</ol>";
        $body .= "<p>Tahap Permohonan:</p>";
        $body .= "<ol>";
        $body .= "<li>Pengajuan permohonan pendaftaran Merek.</li>";
        $body .= "<li>Pemeriksaan formalitas oleh DJKI.</li>";
        $body .= "<li>Pengumuman selama 2 bulan di Berita Resmi Merek (Publikasi).
        (Ada masa tunggu seandainya ada oposisi/keberatan dari masyarakat umum)</li>";
        $body .= "<li>Pemeriksaan Substantif oleh DJKI (Substantif 1 oleh Tim Pemeriksa dan substantif 2 oleh Ketua Tim Pemeriksa/Kasubdit).</li>";
        $body .= "<li>Persetujuan oleh Direktur Merek.</li>";
        $body .= "<li>Pemberian Nomor Pendaftaran Merek.</li>";
        $body .= "<li>Pengiriman Sertifikat Merek.</li>";
        $body .= "</ol>";
        $body .= "<p>Seluruh tahap berlangsung selama 12-24 bulan. Estimasi waktu terbaik.</p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Haki/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendLinkProgress(string $email, string $merek, string $kelas, string $url)
    {
        $subject = "Update Status Pendaftaran Merek $merek Kelas $kelas - Link Progress";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Sehubungan dengan dilakukannya pendaftaran atas Merek $merek Kelas $kelas, berikut kami lampirkan link yang dapat anda gunakan pula guna memantau progress pendaftaran merek anda:</p>";
        $body .= "<p>$merek <a href='$url' alt='$merek'>di sini</a></p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $this->kirim($email, $subject, $body);
    }

    public function sendPublikasi(string $email, string $merek, int $kelas, array $file)
    {
        $subject = "Update Status Pendaftaran Merek $merek Kelas $kelas - Publikasi ";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Sehubungan dengan dilakukannya pendaftaran atas Merek $merek Kelas $kelas, kami ingin memberitahukan Anda bahwa status pendaftaran merek tersebut saat ini sedang dalam proses Publikasi.</p>";
        $body .= "<p>Berikut kami lampirkan Berita Resmi Merek (BRM) yang diterbitkan oleh Direktorat Jendral Kekayaan Intelektual (DJKI). Pengumuman dalam BRM ini akan berlangsung selama 2 (dua) bulan yang merupakan masa tunggu seandainya ada oposisi/keberatan dari masyarakat umum. </p>";
        $body .= "<p>Apabila proses berjalan dengan lancar, selanjutnya proses akan dilanjutkan dengan pemeriksaan substantif oleh DJKI (Substantif 1 oleh Tim Pemeriksa dan substantif 2 oleh Ketua Tim Pemeriksa/Kasubdit). Jika ada update lebih lanjut akan kami kembali informasikan kepada Anda.</p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Haki/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendPemeriksaan(string $email, string $merek, string $kelas)
    {
        $subject = "Update Status Pendaftaran Merek $merek Kelas $kelas - Pemeriksaan Substantif";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Sehubungan dengan dilakukannya pendaftaran atas Merek $merek Kelas $kelas, kami ingin memberitahukan Anda bahwa status pendaftaran merek tersebut saat ini sedang dalam proses  Pemeriksaan Substantif. </p>";
        $body .= "<p>Sebagai gambaran untuk tahapan selanjutnya apabila proses pemeriksaan substantif ini tidak ada kendala maka akan dilanjutkan dengan Persetujuan dan Penerbitan Sertifikat Merek. Jika ada update lebih lanjut akan kami kembali informasikan kepada Anda. </p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $this->kirim($email, $subject, $body);
    }

    public function sendHakiSelesai(string $email, string $merek, array $file)
    {
        $subject = "Update Sertifikat Merek $merek - Selesai";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Melalui notifikasi ini, Kami informasikan bahwa Sertifikat untuk $merek telah selesai dan sudah bisa diunduh.  Mohon untuk melakukan pengecekan kembali terhadap sertifikat yang telah terbit, apabila terdapat penyesuaian atau kesalahan data mohon untuk menginformasikan kepada kami maksimal 2 (dua) hari kerja setelah email ini diterima.</p>";
        $body .= "<p>Untuk mengunduh soft file dokumen izin Anda, silakan klik <a href='https://tracking.izin.co.id'>di sini</a> dan masukkan Kode Tracking yang sudah dikirimkan di email sebelumnya beserta password Anda.</p>";
        $body .= "<p>Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id</p>";
        $body .= "<p>Silakan klik di sini untuk memberikan review Anda: <a href='http://bit.ly/ReviewIZIN'>http://bit.ly/ReviewIZIN</a></p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Haki/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendTaxEFIN(string $email, string $nama, array $file)
    {
        $subject = "Update EFIN " . $nama;

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Melalui notifikasi ini, Kami informasikan bahwa EFIN (Electronic Filing Identification Number) atas nama <b>$nama</b> telah selesai dan sudah bisa diunduh.</p>";
        $body .= "<p>Selanjutnya silahkan melakukan registrasi dengan langkah berikut:</p>";
        $body .= "<p>Buka laman <a href='https://djponline.pajak.go.id/account/login'>https://djponline.pajak.go.id/account/login</a></p>";
        $body .= "<ol>";
        $body .= "<li>Klik 'Daftar Disini'.</li>";
        $body .= "<li>Isikan NPWP, EFIN, dan kode keamanan (sesuai huruf pada gambar berwarna merah) lalu klik 'Submit'.</li>";
        $body .= "<li>Pastikan alamat email dan nomor telepon yang terdaftar adalah email dan nomor yang aktif dan benar. Jika alamat email dan nomor yang tertera masih salah atau ingin Saudara ganti, silahkan ganti dengan alamat email dan nomor yang benar (kolom alamat email dan nomor telepon dapat Saudara ubah).</li>";
        $body .= "<li>Setelah alamat email dan nomor telepon terisi dengan benar, silakan membuat kata sandi (password) untuk mengakses DJP Online. Kata sandi dapat berupa huruf, angka, atau kombinasi dengan minimal 6 digit. Konfirmasi kata sandi dengan mengetikkan kembali kata sandi yang Saudara buat. Ketikkan kode keamanan (sesuai gambar berwarna merah) lalu klik 'Submit'.</li>";
        $body .= "<li>Periksa kotak masuk (inbox) pada alamat email yang Saudara daftarkan dalam DJP Online (langkah no.3).</li>";
        $body .= "<li>Saudara akan menerima email aktivasi akun dari DJP Online, buka lalu klik tombol 'AKTIFKAN AKUN'.</li>";
        $body .= "<li>Registrasi selesai.</li>";
        $body .= "</ol>";
        $body .= "<p>Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan <a href='https://izin.co.id'>IZIN.co.id</a>. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan <a href='https://izin.co.id'>IZIN.co.id</a>.</p>";
        $body .= "<p>Silakan klik di sini untuk memberikan review Anda: <a href='http://bit.ly/ReviewIZIN'>http://bit.ly/ReviewIZIN</a>.</p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Tax/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendTaxSPPKP(string $email, string $nama, array $file)
    {
        $subject = "Update SPPKP " . $nama;

        $body = "<p>Kepada Yth,</p>";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Melalui notifikasi ini, Kami informasikan bahwa SPPKP (Surat Pengukuhan Pengusaha Kena Pajak) untuk <b>$nama</b> telah selesai dan sudah bisa diunduh.</p>";
        $body .= "<p>Adapun nantinya hardcopy SPPKP ini akan kami kirimkan ke alamat perusahaan Anda. Mohon infokan detail alamat dan kontak penerimanya melalui WhatsApp Customer Service kami di +62 822-9998-0011.</p>";
        $body .= "<p>Selanjutnya, jika pihak Anda mendapatkan info dari KPP perihal jadwal survei KPP, harap informasikan kepada kami. Adapun survei KPP biasanya akan dihubungi sekitar 10 - 14 hari kerja setelah SPPKP tersebut terbit. Apabila dalam kurun waktu tersebut Anda belum mendapatkan info dari KPP perihal jadwal surveinya, harap informasikan pula kepada kami melalui email ini atau melalui WA ke <i>legal consultant</i> Anda. Adapun survei KPP biasanya akan ditanyakan seputar profil perusahaan anda seperti bidang usaha, karyawan, penjualan, bruto, lokasi usaha, dsb.</p>";
        $body .= "<p>Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan <a href='https://izin.co.id'>IZIN.co.id</a>. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan <a href='https://izin.co.id'>IZIN.co.id</a>.</p>";
        $body .= "<p>Silakan klik di sini untuk memberikan review Anda: <a href='http://bit.ly/ReviewIZIN'>http://bit.ly/ReviewIZIN</a>.</p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Tax/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendTaxSKPP55(string $email, string $nama, array $file)
    {
        $subject = "Update SK PP 55 $nama";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Melalui notifikasi ini, Kami informasikan bahwa SK PP 55 (Surat Keterangan Peraturan Pemerintah Nomor 55) untuk <b>$nama</b> telah selesai dan sudah bisa diunduh.</p>";
        $body .= "<p>Untuk mengunduh soft file dokumen izin Anda, silakan klik <a href='https://tracking.izin.co.id'>di sini</a> dan masukkan Kode Tracking yang sudah dikirimkan di email sebelumnya beserta password Anda.</p>";
        $body .= "<p>Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id</p>";
        $body .= "<p>Silakan klik di sini untuk memberikan review Anda: <a href='http://bit.ly/ReviewIZIN'>http://bit.ly/ReviewIZIN</a></p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Tax/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendTaxLPB(string $email, string $nama, string $date, array $file)
    {
        $subject = "Update Laporan Pajak Bulanan " . $nama;

        $bulan = date("M", strtotime($date));
        $tahun = date("Y", strtotime($date));

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Berikut adalah Laporan Pajak Bulanan (SPT Masa) untuk periode $bulan $tahun.</p>";
        $body .= "<p>Terima kasih atas data yang sudah disampaikan melalui tim kami.</p>";
        $body .= "<p>Kami mohon informasi dokumen sebagai berikut untuk pelaporan pajak bulan berikutnya: </p>";
        $body .= "<ol>";
        $body .= "<li>List Karyawan beserta gaji & status karyawan (menikah/tidak menikah/laki-laki/perempuan)</li>";
        $body .= "<li>Omset di bulan berjalan</li>";
        $body .= "<li>Informasi jika ada penggunaan jasa</li>";
        $body .= "<li>Informasi jika ada sewa tempat</li>";
        $body .= "</ol>";
        $body .= "<p>Dokumen diatas agar dapat disampaikan kepada kami sebelum tanggal 10 bulan berjalan berikutnya. </p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Tax/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendTaxLPT(string $email, string $nama, string $date, array $file)
    {
        $subject = "Update Laporan Pajak Tahunan " . $nama;

        $tahun = substr($date, 0, 4);

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Berikut adalah Laporan Pajak Tahunan untuk periode tahun $tahun.</p>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Tax/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function sendTaxPB(string $email, string $nama, string $date, array $file)
    {
        $month = date('F', strtotime($date));

        $subject = "Update Pembukuan Bulanan $month $nama";

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Berikut adalah Laporan Pembukuan Bulanan $month <b>$nama</b>. Terima kasih atas data yang sudah disampaikan melalui tim kami.</p>";
        $body .= "<p>Kami mohon informasi dokumen sebagai berikut untuk pembuatan pembukuan di bulan berikutnya : </p>";
        $body .= "<ol>";
        $body .= "<li>Bank Statement (Rekening Koran) Perusahaan</li>";
        $body .= "<li>Buku Kas (Catatan Transaksi uang keluar dan masuk)</li>";
        $body .= "<li>Laporan Penjualan Bulanan</li>";
        $body .= "<li>Laporan Pembelian Bulanan</li>";
        $body .= "<li>Daftar Aset dan Nilai Aset</li>";
        $body .= "<li>Invoice-invoice</li>";
        $body .= "</ol>";
        $body .= "<p>Terima kasih karena telah menggunakan jasa kami dan kami akan berusaha memberikan layanan yang terbaik untuk anda.</p>";
        $body .= "<p>Dokumen diatas agar dapat disampaikan kepada kami sebelum tanggal 5 bulan berjalan berikutnya. </p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $attachment = array(
            "path" => "../../" . UPDIR . "/Attachment_Tax/" . $file['trkfile_name_uniq'],
            "name" => $file['trkfile_name_ori']
        );

        $this->kirim($email, $subject, $body, $attachment);
    }

    public function EmailAkta(string $email, string $companyName, string $date)
    {
        $subject = "Pengingat Berakhirnya Masa Jabatan Direksi dan Komisaris $companyName";

        $str_date = date('d F Y', strtotime($date));

        $body = "<p>Kepada Yth,</p> ";
        $body .= "<p><b>Salam dari IZIN.co.id!</b></p>";
        $body .= "<p>Terima kasih telah senantiasa mempercayakan pengurusan legalitas bersama IZIN.co.id.</p>";
        $body .= "<p>Melalui notifikasi ini, kami informasikan bahwa berdasarkan Akta dan Pengesahan Kemenkumham PT, MASA JABATAN Direksi dan Komisaris Perusahaan Anda akan segera BERAKHIR berdasarkan Akta dan Pengesahan Kemenkumham pada tanggal $str_date.</p>";
        $body .= "<p>Berdasarkan UU Perseroan Terbatas No. 40 tahun 2007, masa jabatan Direksi Komisaris PT yang sudah habis dapat diperpanjang melalui Rapat Umum Pemegang Saham (RUPS) atau dilakukannya Perubahan/Perpanjangan Akta PT.</p>";
        $body .= "<p>IZIN dapat membantu Anda dengan memberikan PENAWARAN SPESIAL berupa VOUCHER SENILAI IDR 500.000 yang dapat Anda gunakan untuk melakukan perubahan/perpanjangan masa jabatan Direksi dan Komisaris PT Anda.</p>";
        $body .= "<p>Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.</p>";
        $body .= "<br>";
        $body .= "<p>Salam Hangat,</p>";
        $body .= "<h6>Tim IZIN.co.id</h6>";

        $this->kirim($email, $subject, $body);
    }

    private function kirim(string $email, string $subject, string $body, array $attachment = [])
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
        $date_now = $dt->format('Y-m-d H:i:s');
        $mail = new PHPMailer(true);

        try {
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = "smtp.gmail.com";
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "ssl";
            $mail->Port = 465;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->IsHTML(true);
            $mail->Username = "info@izin.co.id";
            $mail->Password = "zaq!xsw@";
            $mail->SetFrom("info@izin.co.id", "Customer Service izin.co.id");
            $mail->AddAddress($email);
            $mail->AddCC("team@izin.co.id");
            $mail->addBCC("dev@curalib.com");
            // $mail->AddAddress("tedijammz@gmail.com");
            $mail->Subject = $subject;
            $mail->Body = $body;
            // attachment
            if (!empty($attachment)) {
                $mail->addAttachment($attachment['path'], $attachment['name']);
            }
            $mail->send();
            echo $date_now . "Email sent successfully to " . $email . PHP_EOL;
        } catch (MailerExeption $e) {
            echo $date_now . " error: " . $e->errorMessage() . PHP_EOL; //Pretty error messages from PHPMailer
        } catch (\Exception $e) {
            echo $date_now . " error: " . $e->getMessage() . PHP_EOL; //Boring error messages from anything else!
        }
    }
}
