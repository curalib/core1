<?php
# Apps Setting
# ------------
define("APPS_TITLE", "ERP IZIN");
define("APPS_TITLE_EXPLAIN", "Portal ERP izin.co.id");
define("APPS_OWNER", "Application Owner");
define("APPS_ADDR", "Alamat Izin");
define("APPS_ICON", "templates/Default/images/ico.png");
define("ROOT_DIR", __DIR__);

# Security Setting
# ----------------

# Domain Check
# Prevent HTML Hijacking.
# Strongly recomended to be activated
define("USE_DOMAIN_CHECK", false);

# Check Last Logout.
# Activate it if you have notorious users who don't like to logout
define("CH_LOGOUT", false);

# Limiting Login Attempt.
# Prevent brute force attacks.
# Strongly recomended to be activated
define("LIMIT_LGN_ATTEMPTS", false);

# IP Block
# Prevent brute force attacks.
# Strongly recomended to be activated
define("IP_BLOCK", false);

# IP White List
# Be very carefull using this.
define("IP_WHITE", false);

# Ip Based Session
# Prevent session hijacking.
# Strongly recomended to be activated
define("IP_BASED", false);

# Session Time Out.
# Reducing unauthorized access.
define("SESS_TIME_OUT", false);

# Captcha
# Reducing Brute force attacks.
# Strongly recomended to be activated
# ---------------------
#define("CPTCH", true);
define("CPTCH", false);

# Advanced Application Config
# ------------------
##require "system/CrcConfig.php";
require "system/CrcConfig.php";
require "system/appsConfig.php";

# Run
# -----
require_once "system/barep.php";

?>