<?php

define('BASEURL', 'https://erp.izin.co.id/');
define("ROOT_DIR", __DIR__);
require "system/CrcConfig.php";
require "system/appsConfig.php";
require_once "system/db_connect.php";
require 'system/function.php';
require 'language/default.php';

if (DBC_PREF != "") {
    $tblp = DBC_PREF . "_";
} else {
    $tblp = "";
}

$_SESSION = [];
$_SESSION["ROOT_DIR"] = ROOT_DIR;

$count = $dbs->getQuery("SELECT COUNT(id) AS count  FROM izin_apps_invdet WHERE tipe = 'PAID' AND paid = 'Y'");
echo 'TOTAL: ' . $dbs->getAssoc($count)['count'] . PHP_EOL;

$current = 1;
//$invoices = $dbs->getQuery("SELECT * FROM izin_apps_invdet WHERE tipe = 'PAID' AND paid = 'Y' and inv_num in ('2112-062') ORDER BY inv_num DESC"); // example
$invoices = $dbs->getQuery("SELECT * FROM izin_apps_invdet WHERE tipe = 'PAID' AND paid = 'Y' ORDER BY inv_num DESC");
while ($invoice = $dbs->getAssoc($invoices)) {
    echo ($current++) . ': ' . $invoice['inv_num'] . ' ';
    $_SESSION["LeadChStat"] = $invoice['id_lead'];
    #Create Paid Invoice
    $InvMark = '<img src="' . convert_img_to_base64(ROOT_DIR . '/templates/Default/sysicons/Paid_i.png') . '">';

    $dtinv = "select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rdtinv = $dbs->getArr($dtinv);

    $invnum = $rdtinv['inv_num'];
    $IssueDt = $rdtinv['issue_date'];
    $DueDt = $rdtinv['due_date'];
    $PayDt = $rdtinv['tglbayar'];
    $IssueDtShow = c_date($IssueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
    $DueDtShow = c_date($DueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
    $PayDtShow = c_date($PayDt, "shortdate", "long", $list_month, $list_month_short, "Y");
    $invname = $rdtinv['name'];
    $invpic = $rdtinv['pic'];
    $invmail = $rdtinv['email'];
    $invphnum = $rdtinv['phone_number'];

    $citminv = "Select item, price, no_urut from " . $tblp . "apps_invitem where id_inv = '" . $rdtinv['id'] . "' order by no_urut asc";
    $rcitminv = $dbs->getQuery($citminv);
    while ($cit = $dbs->getAssoc($rcitminv)) {
        ${"invitm0" . $cit['no_urut']} = $cit['item'];
        ${"invprc0" . $cit['no_urut']} = preg_replace("/\D/", "", $cit['price']);
        if (!empty(${"invprc0" . $cit['no_urut']})) {
            ${"prc0" . $cit['no_urut'] . "show"} = 'IDR ' . c_curr(${"invprc0" . $cit['no_urut']}, ",", ".", $min_sign = "mark") . ',-';
        } else {
            ${"prc0" . $cit['no_urut'] . "show"} = '';
        }
    }

    $ammdue = $rdtinv['total_due'];

    $BnkAcc = $rdtinv['bank_acc'];
    $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
    $BAD_NmBank = $BnkAccDet[0];
    $BAD_NmPT = $BnkAccDet[1];
    $BAD_NoRek = $BnkAccDet[2];
    $BAD_SwfCode = $BnkAccDet[3];
    if (VABANK[$BnkAcc] === "00000") {
        $va_num = NULL;
    } else {
        $va_num = VABANK[$BnkAcc] . str_replace("-", "", $invnum);
    }

    $vaNumber = $va_num;
    $root_dir = ROOT_DIR;

    $template = $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php";
    if (is_file($template)) {
        $judul_invoice = "Invoice";
        #Load Invoice HTML
        require $template;

        try {
            require "system/CreatePDF.php";
            $dompdf->loadHtml($invhtml);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $pdfoutput = $dompdf->output();
            $dir = "../../" . UPDIR . "/Invoices";
            if (is_dir($dir)) {
                file_put_contents($dir . "/" . $invnum . '.pdf', $pdfoutput);
                $createpdf = "OK";
            } else {
                $createpdf = "SHUT";
            }
        } catch (Exception $e) {
            $createpdf = "SHUT";
        }
    } else {
        $createpdf = "SHUT";
    }

    echo $createpdf . PHP_EOL;
}
