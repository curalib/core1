# Main Flow
- Web Server: Apache, Nginx
- Config:
    - system/appsConfig.php (domainCheck, appConfig)
    - system/CrcConfig.php (DB, Env, Const)
- Code Flow:
    - index.php
    - system/barep.php
    - system/CheckLogin.php
      - Generate Menu from DB (use `tgl` field on URL)
- Composer: DomPDF
- API Accurate
  - Generate new token:
    - Go to: https://erp.izin.co.id/api/bca/test/
    - Login
    - Choose "PT MITRA SOLUSI HUKUM"
    - Get `access_token` from query string (UUID)
- API Virtual Account BCA
  - api/application/controllers/api/Va.php
  - Issue when generating PDF

# PHP Built-in Interactive Shell
## Important Notes
1. `$_SESSION` is not available.

## Steps
1. `php -a`
2. Run:
```
define('BASEURL', 'https://erp.izin.co.id/');
define("ROOT_DIR", __DIR__);
require "system/CrcConfig.php";
require "system/appsConfig.php";
require_once "system/db_connect.php";
require 'system/function.php';
require 'language/default.php';

if ( DBC_PREF != "" ) {
    $tblp = DBC_PREF."_";
} else {
    $tblp = "";
}

$_SESSION = [];
$_SESSION["ROOT_DIR"] = ROOT_DIR;
```
3. Misc:
```
$_SESSION["LeadChStat"] = '5a2d28dbe5bb18dc5f29e5981cab12ff';

$vaNumber = VABANK[$BnkAcc] . str_replace("-", "", $invnum);
```

# PDF Related Files
1. system/CreatePDF.php (DOMPDF Initialization `$dompdf`)
2. Modules/CRM/file/invva.php (depreceated, moved to InvoiceHTML.php)
3. Modules/CRM/mod/Lead_i.php
4. Modules/CRM/file/InvoiceHTML.php
5. Modules/CRM/file/InvoiceHTMLPaid.php (depreceated, moved to InvoiceHTML.php)
6. Modules/Transaction/mod/TrInv_i.php

# Email Configuration
1. reminder.php
2. SendEmail.php
3. api/index.php
4. system/CrcConfig.php