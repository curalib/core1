<?php
require_once "system/appsConfig.php";
if ( SESS_DIR != 1 ){ session_save_path(SESS_DIR); }
if ( session_status() == PHP_SESSION_NONE ){ session_start(); }
//session_start();
$cptCode = $_SESSION["CPT_CODE"];
$cptFont = "fonts/Happy_Sans.ttf";

$cptImg = imagecreatetruecolor(CPTCH_WD, CPTCH_HT);
imagealphablending($cptImg,false);
$cptCol=imagecolorallocatealpha($cptImg,255,255,255,127);
imagefilledrectangle($cptImg,0,0,CPTCH_WD,CPTCH_HT,$cptCol);
imagealphablending($cptImg,true);
$cptColor = imagecolorallocate($cptImg, 187, 187, 187);
imagettftext($cptImg, CPT_FS, 0, 5, 22, $cptColor, $cptFont, $cptCode);
header('Content-type: image/png');
imagealphablending($cptImg,false);
imagesavealpha($cptImg,true);
imagepng($cptImg);
imagedestroy($cptImg);
?>