<?php

date_default_timezone_set('Asia/Jakarta');

require_once "system/CrcConfig.php";
require_once "vendor/autoload.php";

use PHPMailer\PHPMailer\Exception as MailerExeption;
use PHPMailer\PHPMailer\PHPMailer;

class Reminder
{
    private $HOST = DBC_MHOST;
    private $USER = DBC_MUSER;
    private $PASSWD = DBC_MPWD;
    private $DBNAME = DBC_MSDB;

    private function Connection()
    {
        $connect =  new mysqli($this->HOST, $this->USER, $this->PASSWD, $this->DBNAME);
        if ($connect->connect_error) {
            die("Connection failed: " . $connect->connect_error);
        } else {
            return $connect;
        }
    }

    /**
     * Get data reminder of Agreement & Ultah
     * Depreceated, get data from each table it self.
     *
     * @return mixed
     */
    private function getData()
    {
        $conn = $this->Connection();
        $sql = "SELECT * FROM izin_1.reminder";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    /**
     * Get data Ultah; Company & Director
     *
     * @param string $monthDate
     * @return mixed
     */
    private function dataUltah(string $monthDate)
    {
        $conn = $this->Connection();
        $sql = "SELECT izin_1.izin_apps_trklist.*, izin_1.izin_apps_lead.client_email, izin_1.izin_apps_lead.client_name, (SELECT bank_acc FROM izin_1.izin_apps_invdet WHERE id_lead = izin_1.izin_apps_trklist.lead_id) as bank_acc ";
        $sql .= "FROM izin_1.izin_apps_trklist ";
        $sql .= "INNER JOIN izin_1.izin_apps_lead on izin_1.izin_apps_lead.id_lead = izin_1.izin_apps_trklist.lead_id ";
        $sql .= "WHERE izin_1.izin_apps_trklist.ultah_perusahaan LIKE '%-{$monthDate}' ";
        $sql .= "OR izin_1.izin_apps_trklist.ultah_direktur LIKE '%-{$monthDate}' ";

        return $conn->query($sql);
    }

    /**
     * Get data Tax
     *
     * @param string $monthDate
     * @return mixed
     */
    private function dataReminderTax(string $monthDate)
    {
        // skip EFIN, SPPKP, SK PP 55, & PAJAK TAHUNAN

        $conn = $this->Connection();
        $sql = "SELECT tracking.*, lead.client_email
            FROM izin_apps_trklist AS tracking
            INNER JOIN izin_apps_lead AS lead ON lead.id_lead = tracking.lead_id
            WHERE (tracking.reminder_tax_3 IS NOT NULL && tracking.reminder_tax_3 LIKE '%-{$monthDate}')
                OR (tracking.reminder_tax_5 IS NOT NULL && tracking.reminder_tax_5 LIKE '%-{$monthDate}');";

        return $conn->query($sql);
    }

    /**
     * Get data Akta 5 tahunan
     *
     * @return mixed
     */
    private function dataReminderAkta()
    {
        $curr_date = date('Y-m-d');

        $conn = $this->Connection();
        $sql = "SELECT tracking.*, lead.client_email
            FROM izin_apps_trklist AS tracking
            INNER JOIN izin_apps_lead AS lead ON lead.id_lead = tracking.lead_id
            WHERE (tracking.reminder_akta IS NOT NULL && tracking.reminder_akta = '$curr_date');";

        return $conn->query($sql);
    }

    /**
     * Get data Agreement
     * Reminder client after three days issued
     *
     * @return mixed
     */
    private function dataAgreement()
    {
        $curr_date = date('Y-m-d');

        $conn = $this->Connection();
        $sql = "SELECT * from izin_apps_agreement WHERE `discard` = 0 and `client_sign` IS NULL AND DATEDIFF('$curr_date', CAST(created_at AS DATE)) > 3";

        return $conn->query($sql);
    }

    private function dataLKPM()
    {
        // Get the month number of the date
        $month = date("n", strtotime(date("Y-m-d")));

        // Divide that month number by 3 and round up using ceil.
        $yearQuarter = (int)ceil($month / 3);

        $date = new \DateTime();
        switch ($yearQuarter) {
            case 1:
                $date->setDate(date('Y'), 3, 20);
                break;
            case 2:
                $date->setDate(date('Y'), 6, 20);
                break;
            case 3:
                $date->setDate(date('Y'), 9, 20);
                break;
            case 4:
                $date->setDate(date('Y'), 12, 20);
                break;
        }

        // check if date is today()
        if ($date->format('Y-m-d') === date('Y-m-d')) {
            return 'It works!';
        }

        return;
    }

    public function main()
    {
        $curr_monthdate = date('m-d');

        // Reminder LKPM
        //$dataReminderLkpm = $this->dataLKPM();

        // Reminder Birthday
        $dataUltah = $this->dataUltah($curr_monthdate);
        if ($dataUltah->num_rows > 0) {
            $_i = 0;
            while ($row = $dataUltah->fetch_assoc()) {
                $BnkAcc = $row['bank_acc'];
                $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
                $BAD_NmPT = $BnkAccDet[1];

                if (substr($row['ultah_perusahaan'], -5) == $curr_monthdate) {

                    if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
                        $body = "Dear " . $row['client_name'] . ",
                            <br><br>
                            Greetings from Invest in Asia!
                            <br><br>
                            Congratulation of company establishment!
                            <br><br>
                            Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
                            <br><br><br><br>
                            Warm regards,<br>
                            <strong>Invest In Asia team</strong>";

                        $this->sendReminder($row['client_email'], "An Anniversary for you", $body);
                    } else {
                        $body = '<div style="max-width:640px;margin:auto;">';
                        $body .= '<img src="cid:annivcompany" style="padding-bottom:0px;margin-bottom:0px" width="100%" height="100%">';
                        $body .= '<img src="cid:footermail" style="padding-bottom:0px;margin-bottom:0px" width="100%" height="100%"></div>';
                        $imgs = [
                            'annivcompany' => ['assets/images/annivcompany.jpg', 'annivcompany', 'annivcompany.jpg'],
                            'footermail' => ['assets/images/footermail.jpg', 'footermail', 'footermail.jpg'],
                        ];

                        $this->sendReminder($row['client_email'], "An Anniversary Gift for you from IZIN.co.id", $body, $imgs);
                    }

                    $_i++;
                }

                if (substr($row['ultah_direktur'], -5) == $curr_monthdate) {
                    if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
                        // coming soon
                    } else {
                        $body2 = '<div style="max-width:640px;margin:auto;">';
                        $body2 .= '<img src="cid:birthdaydirektur" style="padding-bottom:0px;margin-bottom:0px" width="100%" height="100%">';
                        $body2 .= '<img src="cid:footermail" style="padding-bottom:0px;margin-bottom:0px" width="100%" height="100%"></div>';
                        $imgs = [
                            'annivcompany' => ['assets/images/birthdaydirektur.jpg', 'birthdaydirektur', 'birthdaydirektur.jpg'],
                            'footermail' => ['assets/images/footermail.jpg', 'footermail', 'footermail.jpg'],
                        ];

                        $this->sendReminder($row['client_email'], "A birthday Gift for you from IZIN.co.id", $body2, $imgs);
                    }

                    $_i++;
                }
            }

            echo "Total reminder_ultah: {$_i} records." . PHP_EOL;
        } else {
            echo "No ultah reminder records found." . PHP_EOL;
        }

        // Reminder Tax
        $dataReminderTax = $this->dataReminderTax($curr_monthdate);
        if ($dataReminderTax->num_rows > 0) {
            echo "Total reminder_tax: {$dataReminderTax->num_rows} records." . PHP_EOL;
            foreach ($dataReminderTax->fetch_all(MYSQLI_ASSOC) as $row) {
                foreach (TAX as $index => $value) {
                    $key = 'reminder_tax_' . $index;
                    if (!empty($row[$key]) && stripos($row[$key], $curr_monthdate) !== false) {
                        require_once "SendEmail.php";
                        $sendEmail = new SendEmail();
                        $sendEmail->EmailTax($row['client_email'], $row['client_name'], $index, $row['id_trklist']);
                    }
                }
            }
        } else {
            echo "No tax reminder records found." . PHP_EOL;
        }

        // Reminder Akta
        $dataReminderAkta = $this->dataReminderAkta();
        if ($dataReminderAkta->num_rows > 0) {
            echo "Total reminder_akta: {$dataReminderAkta->num_rows} records." . PHP_EOL;

            foreach ($dataReminderAkta->fetch_all(MYSQLI_ASSOC) as $row) {
                require_once "SendEmail.php";
                $sendEmail = new SendEmail();
                $sendEmail->EmailAkta($row['client_email'], $row['company_name'], $row['reminder_akta']);
            }
        }

        // Reminder Agreement
        $dataAgreement = $this->dataAgreement();
        if ($dataAgreement->num_rows > 0) {
            echo "Total reminder_agreement: {$dataAgreement->num_rows} records." . PHP_EOL;
            $agreements = [];
            while ($row = $dataAgreement->fetch_assoc()) {
                $agreements[] = [$row['inv_num'], $row['package_name'], $row['client_pic'], $row['client_company_name']];
            }

            // Email to Admin
            if (!empty($agreements)) {
                $body = '<b>Dear Team,</b> <br><br> Berikut daftar agreement yang belum mendapat response klien; <br>';
                $body .= '<ol>';
                for ($i = 0; $i < count($agreements); $i++) {
                    $body .= '<li>' . $agreements[$i][0] . ' ' . $agreements[$i][3] . ' - ' . $agreements[$i][2] . '  </li>';
                }
                $body .= '';
                $body .= '</ol>';

                $this->sendReminder('cs@izin.co.id', "Reminder Response Agreement", $body);
            }
        } else {
            echo "No agreement reminder records found." . PHP_EOL;
        }
    }

    private function sendReminder(string $to, string $subject, string $body, array $imgs = [])
    {
        $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
        $date_now = $dt->format('Y-m-d H:i:s');
        $mail = new PHPMailer(true);

        try {
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;
            $mail->Host = "smtp.gmail.com";
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "ssl";
            $mail->Port = 465;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->IsHTML(true);
            $mail->Username = "info@izin.co.id";
            $mail->Password = "zaq!xsw@";
            $mail->SetFrom("info@izin.co.id", "Customer Service izin.co.id");
            $mail->AddAddress($to);
            if ($to <> "team@izin.co.id") {
                $mail->AddCC("team@izin.co.id");
            }
            $mail->addBCC("dev@curalib.com");
            $mail->Subject = $subject;
            $mail->Body = $body;
            $mail->AltBody = strip_tags($body);
            if (!empty($imgs)) {
                foreach ($imgs as $key => $value) {
                    $mail->AddEmbeddedImage($value[0], $value[1], $value[2]);
                }
            }
            $mail->send();
            echo $date_now . " Email sent successfully to " . $to . PHP_EOL;
        } catch (MailerExeption $e) {
            echo $date_now . " error: " . $e->errorMessage() . PHP_EOL; //Pretty error messages from PHPMailer
        } catch (\Exception $e) {
            echo $date_now . " error: " . $e->getMessage() . PHP_EOL; //Boring error messages from anything else!
        }
    }
}

$reminder = new Reminder();
$reminder->main();
