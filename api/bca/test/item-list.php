<?php

include 'conf.php';

$accessToken = $_GET['access_token'];
$session = $_GET['session'];

// Parameter
$parameter = [];
$parameter['_ts'] = gmdate('Y-m-d\TH:i:s\Z');
$parameter['session'] = $session;
$parameter['fields'] = "id,no,name";


// Create Signature
ksort($parameter);
$parameter = array_map('trim', $parameter);

$data = '';
foreach ( $parameter as $nama => $nilai ) {
    if ($nilai == '') {
        continue;
    }

    if ($data != '') {
        $data .= '&';
    }
    $data .= rawurlencode($nama) . '=' . rawurlencode($nilai);
}
$hash = hash_hmac('sha256', $data, $signatureSecret, true );
$signature = base64_encode($hash);
$parameter['sign'] = $signature;

// Connect API
$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => "Authorization: Bearer " . $accessToken . "\r\n",
        'content' => http_build_query($parameter),
        'ignore_errors' => true,
    )
);

$context  = stream_context_create($opts);
$result = file_get_contents('https://public.accurate.id/accurate/api/customer/list.do', false, $context);

echo $result;

?>
