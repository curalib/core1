<?php

include 'conf.php';

$accessToken = $_GET['access_token'];

// Parameter
$parameter = [];
$parameter['_ts'] = gmdate('Y-m-d\TH:i:s\Z');
$parameter['id'] = $_GET['id'];

// Create Signature
ksort($parameter);
$parameter = array_map('trim', $parameter);

$data = '';
foreach ( $parameter as $nama => $nilai ) {
    if ($nilai == '') {
        continue;
    }

    if ($data != '') {
        $data .= '&';
    }
    $data .= rawurlencode($nama) . '=' . rawurlencode($nilai);
}
$hash = hash_hmac('sha256', $data, $signatureSecret, true );
$signature = base64_encode($hash);
$parameter['sign'] = $signature;

// Connect API
$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => "Authorization: Bearer " . $accessToken . "\r\n",
        'content' => http_build_query($parameter),
        'ignore_errors' => true,
    )
);

$context  = stream_context_create($opts);
$response = file_get_contents('https://account.accurate.id/api/open-db.do', false, $context);

// Output
$session = json_decode($response)->{'session'};
$itemListUrl = "item-list.php?access_token=$accessToken&session=$session";
echo gmdate('Y-m-d\TH:i:s\Z');
echo "====";
echo $signature;
echo "<div>Connected with session $session</div>";
echo "<a href=\"$itemListUrl\">Read Item data</a>";

?>
