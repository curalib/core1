<?php

include 'conf.php';

$accessToken = $_GET['access_token'];

// Parameter
$parameter = [];
$parameter['_ts'] = gmdate('Y-m-d\TH:i:s\Z');

// Create Signature
ksort($parameter);
$parameter = array_map('trim', $parameter);

$data = '';
foreach ( $parameter as $nama => $nilai ) {
    if ($nilai == '') {
        continue;
    }

    if ($data != '') {
        $data .= '&';
    }
    $data .= rawurlencode($nama) . '=' . rawurlencode($nilai);
}
$hash = hash_hmac('sha256', $data, $signatureSecret, true );
$signature = base64_encode($hash);
$parameter['sign'] = $signature;

// Connect API
$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => "Authorization: Bearer " . $accessToken . "\r\n",
        'content' => http_build_query($parameter),
        'ignore_errors' => true,
    )
);

$context  = stream_context_create($opts);
$response = file_get_contents('https://account.accurate.id/api/db-list.do', false, $context);

// Output
$databaseList = json_decode($response)->{'d'};

if (count($databaseList) > 0) {
    echo "<div>Select database</div>";
    foreach ($databaseList as $database) {
        $id = $database->{'id'};
        $alias = $database->{'alias'};
        $openDbUrl = "open-db.php?access_token=$accessToken&id=$id";
        echo "<li><a href=\"$openDbUrl\">$alias</a></li>";
    }
} else {
    echo "You do not have any database, please create database from https://accurate.id";
}


?>
