<?php

$header = json_encode([
  'typ' => 'JWT',
  'alg' => 'HS256'
]);

// Create token payload as a JSON string
$payload = json_encode([
  'CompanyCode' => '80777',
]);

$base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

// Encode Payload to Base64Url String
$base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
$signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', true);
$base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

$jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;
echo $jwt.'  ';

if ($jwt == 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJDb21wYW55Q29kZSI6IjgwNzc3In0.u_Mhx4oy3EtsB92jxPg_qmbJDmFnxIYHmI08dDyrZSQ')
{
  // code...
    $response = array(
       'status' => '200',
       'message' => 'Login Success'
     );
     //http_response_code(200);
     header('Content-Type: application/json');
     echo json_encode($response);
}

?>
