<?php
class Api_public_area_tv_model extends CI_Model {

    private $table = 'Public_TV';
    private $primary = 'id_public_tv';

    function create($data){
        return $this->db->insert($this->table,$data);
    }
    function read($id = null,$id_tv = null){
    	if ($id_tv != null) {
    		$this->db->where('id_public_tv',$id_tv);
    	}
    	if($id != null){
    	    $this->db->where('id_hotel',$id);
    	}
        return $this->db->get($this->table)->result();
    }
    function getAdds($id_publictv,$id_hotel = null,$isactive){
    	if(!is_null($id_hotel)){
    		$this->db->where('Public_TV.id_hotel',$id_hotel);
    		$this->db->join('Public_TV','Public_TV.id_public_tv = DB_Ads.id_public_tv','inner');
    	}
    	$this->db->where('DB_Ads.id_public_tv',$id_publictv);
    	if (!is_null($isactive)) {
    		$this->db->where('DB_Ads.Active',$isactive);
    	}
        return $this->db->get('DB_Ads')->result();
    }
    function getAdsBypotion($position,$id){
    	$this->db->where('QEY',$position);
    	$this->db->where('id_public_tv',$id);
    	return $this->db->get('DB_Ads')->result();
    }
    
    function GetAdsLooping($position,$id,$active){
    	$this->db->where('QEY',$position);
    	$this->db->where('id_public_tv',$id);
    	$this->db->where('DB_Ads.Active',$active);
    	return $this->db->get('DB_Ads')->result();
    }
    function updateAds($data,$id){
       return $this->db->update("DB_Ads",$data,['Id_ads' => $id]);

    }


}
?>