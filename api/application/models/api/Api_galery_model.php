<?php
class Api_galery_model extends CI_Model {

    var $table = 'galery_hotel';
    var $primary = 'Id_galery_hotel';

    function getGalery($id = null){
        if($id != null){
            return $this->db->get_where($this->table,['Id_galery_hotel' => $id])->result_array();    
        }else{
            $this->db->join('hotel','hotel.Id_hotel = galery_hotel.Id_hotel','INNER');
            return $this->db->get($this->table)->result_array();    
        }
        
    }

    function insertGalery($data){
        return $this->db->insert($this->table,$data);
    }

    function updateGalery($data,$id){
        return $this->db->update($this->table,$data,['Id_galery_hotel ' => $id]);
    }

    function deleteGalery($id){
        return $this->db->delete($this->table,['Id_galery_hotel ' => $id]);
    }
}
?>