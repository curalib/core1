<?php
class Api_food_model extends CI_Model {

    var $table = 'food';
    var $primary = 'Id_food';

    function getFood($id = null,$id_hotel = null){
    	if($id != null){
    	   // $this->db->where('food_category.Id_hotel',$id_hotel);
    		return $this->db->get_where($this->table,['Id_food' => $id])->result_array();
    	}else{
    	    $this->db->where('food_category.Id_hotel',$id_hotel);
            $this->db->join('food_category','food_category.Id_food_category = food.Id_food_category','INNER');
            // $this->db->join('hotel','hotel.Id_hotel = food_category.Id_hotel','INNER');
    		return $this->db->get($this->table)->result();	
    	}
    }
    
    function getFoodById_category($id_food_category = null){
        $this->db->join('food_category','food_category.Id_food_category = food.Id_food_category','INNER');
        //$this->db->join('hotel','hotel.Id_hotel = food_category.Id_hotel','INNER');
        $this->db->where('food_category.Id_food_category',$id_food_category);
    	return $this->db->get($this->table)->result_array();
    }

    function insertFood($data){
        return $this->db->insert($this->table,$data);
    }

    function updateFood($data,$id){
        return $this->db->update($this->table,$data,['Id_food' => $id]);
    }

    function deleteFood($id){
        return $this->db->delete($this->table,['Id_food' => $id]);
    }
    
    function OrderFood($data){
        return $this->db->insert("order_food",$data);
    }

    function create_activity($data){
        $this->db->insert("activity",$data);   
    }

    function getIdUser($id_room){
        $this->db->where('room.Id_room',$id_room);
        $this->db->where('reservasi.flag_checkout',0);
        $this->db->join('room','room.Id_room = reservasi.Id_room','inner');
        $this->db->join('user','user.Id_user = reservasi.Id_user','inner');
        $this->db->join('room_type','room_type.Id_room_type = room.Id_room_type','inner');
        return $this->db->get('reservasi')->row();
    }

    function getOrderFood($id_hotel,$id_user){
        $this->db->where('room_type.Id_hotel',$id_hotel);
        $roomtype = $this->db->get('room_type')->result();
        $arr = [];
        if ($roomtype) {
            foreach ($roomtype as $key => $rt) {
                $this->db->where('Id_room_type',$rt->Id_room_type);
                $room = $this->db->get('room')->result();
                if ($room) {
                    foreach ($room as $key => $r) {
                        $this->db->where('reservasi.Id_user',$id_user);
                        $this->db->where('reservasi.flag_checkout',0);
                        $this->db->where('room.status',0);
                        $this->db->where('order_food.id_room',$r->Id_room);
                        $this->db->join('food','food.Id_food = order_food.id_food','INNER');
                        $this->db->join('room','room.Id_room = order_food.id_room','INNER');
                        $this->db->join('reservasi','reservasi.Id_room = order_food.id_room','INNER');
                        $this->db->join('user','user.Id_user = reservasi.Id_user','INNER');
                        $orders = $this->db->get('order_food')->result();
                        if ($orders) {
                            array_push($arr, $orders);
                        }
                    }
                }
            }
        }
        return $arr;
    }
    
}
?>