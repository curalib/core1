<?php
class Api_userGroup_model extends CI_Model {

    var $table = 'user_group';
    var $primary = 'Id_user_group';

    function getUserGroup($id = null){
    	if($id != null){
    		return $this->db->get_where($this->table,['Id_user_group' => $id])->result_array();
    	}else{
    		return $this->db->get($this->table)->result();	
    	}
        
    }

    function insertUserGroup($data){
        return $this->db->insert($this->table,$data);
    }

    function updateUserGroup($data,$id){
        return $this->db->update($this->table,$data,['Id_user_group' => $id]);
    }

    function deleteUserGroup($id){
        return $this->db->delete($this->table,['Id_user_group' => $id]);
    }
}
?>