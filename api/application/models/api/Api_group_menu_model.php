<?php
class Api_group_menu_model extends CI_Model {

    var $table = 'user_group_menu';
    var $primary = 'Id_user_group_menu';

    function getGroupMenu($id = null,$idGroup = null){
        if($id != null){
            $this->db->where('Id_user_group_menu',$id);
        }
        if ($idGroup != null) {
            $this->db->where('jabatan_hotel.Id_jabatan',$idGroup);
        }
        $this->db->join('menu','menu.Id_menu = user_group_menu.Id_menu','INNER');
        $this->db->join('jabatan_hotel','jabatan_hotel.Id_jabatan = user_group_menu.Id_user_group','INNER');
        return $this->db->get($this->table)->result();    
        
        
    }

    function MenuBygroup($id_group){
        $tmp = $this->db->get('menu')->result();
        if ($tmp) {
            foreach ($tmp as $key => $item) {
                $tmp[$key]->checked = $this->CekMenuGroup($id_group,$item->Id_menu);
            }
        }
        return $tmp;

    }
    function SaveGroupMenu($data){
        $tmpData = $this->getGroupMenu(null,$data['id_group']);
        if ($tmpData) {
            foreach ($tmpData as $key => $item) {
                $this->deleteGroupMenu($item->Id_user_group_menu);
            }
        }
        foreach ($data['menu'] as $key => $value) {
            $arr = array(
                'Id_menu' => $value,
                'Id_user_group' => $data['id_group']
            );
            $this->insertGroupMenu($arr);
        }
    }


    function CekMenuGroup($id_group,$id_menu){
        $this->db->where('Id_menu',$id_menu);
        $this->db->where('Id_user_group',$id_group);
        $this->db->select('COUNT(Id_user_group_menu) as jml');
        $tmp = $this->db->get('user_group_menu')->row();
        return $tmp->jml != 0;
    }

    function insertGroupMenu($data){
        return $this->db->insert($this->table,$data);
    }

    function updateGroupMenu($data,$id){
        return $this->db->update($this->table,$data,['Id_user_group_menu' => $id]);
    }

    function deleteGroupMenu($id){
        return $this->db->delete($this->table,['Id_user_group_menu' => $id]);
    }
}
?>