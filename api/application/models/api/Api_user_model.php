<?php
class Api_user_model extends CI_Model {

    private $table = 'user';
    private $primary = 'Id_user';

    function create($data){
        return $this->db->insert($this->table,$data);
    }
    function read($filter){
    	extract($filter);
    	if ($id != null) {
    		$this->db->where('user.Id_user',$id);
    	}
    	if ($id_hotel != null) {
    		$this->db->where('user_hotel.Id_hotel',$id_hotel);
    	}
    	if ($id_jabatan != null) {
    		$this->db->where('jabatan_hotel.Id_jabatan',$id_jabatan);
    	}
    	if ($id_divisi != null) {
    		$this->db->where('jabatan_hotel.Id_divisi',$id_divisi);
    	}
    	if ($isGuest != null) {
    		$this->db->where('user.Is_guest',$isGuest);
    	}
    	if ($id_kelurahan != null) {
    		$this->db->where('user.Id_kelurahan',$id_kelurahan);
    	}
    	$this->db->select('user.*,kelurahan.kelurahan,user_hotel.*,jabatan_hotel.*,divisi_hotel.*,hotel.Name_hotel	');
    	$this->db->join('kelurahan','kelurahan.Id_kelurahan = user.Id_kelurahan','inner');
    	$this->db->join('user_hotel','user_hotel.Id_user = user.Id_user','inner');
    	$this->db->join('hotel','hotel.Id_hotel = user_hotel.Id_hotel','inner');
    	$this->db->join('jabatan_hotel','jabatan_hotel.Id_jabatan = user_hotel.Id_jabatan','inner');
    	$this->db->join('divisi_hotel','divisi_hotel.Id_divisi = jabatan_hotel.Id_divisi','inner');
        return $this->db->get($this->table)->result();
    }
    
    function delete($id){
        $this->db->delete($this->table,['Id_user' => $id]);
        $this->db->delete('user_hotel',['Id_user' => $id]);
    }

    function readById($id = null){
        if($id != null){
            $this->db->where('user.Id_user',$id);
            $this->db->select('user.*,kelurahan.kelurahan,user_hotel.*,jabatan_hotel.*,divisi_hotel.*,hotel.Name_hotel	');
        	$this->db->join('kelurahan','kelurahan.Id_kelurahan = user.Id_kelurahan','inner');
        	$this->db->join('user_hotel','user_hotel.Id_user = user.Id_user','inner');
        	$this->db->join('hotel','hotel.Id_hotel = user_hotel.Id_hotel','inner');
        	$this->db->join('jabatan_hotel','jabatan_hotel.Id_jabatan = user_hotel.Id_jabatan','inner');
        	$this->db->join('divisi_hotel','divisi_hotel.Id_divisi = jabatan_hotel.Id_divisi','inner');
            return $this->db->get($this->table)->result();
        }else{
            return $this->db->get($this->table)->result();
        }
    }

    function getActivity($id){
        $this->db->where('Id_hotel',$id);
        return $this->db->get('activity')->result();
    }

}
?>