<?php
class Api_divisi_hotel_model extends CI_Model {

    private $table = 'divisi_hotel';
    private $primary = 'Id_divisi';

    function create($data){
        return $this->db->insert($this->table,$data);
    }
    function read($id = null,$id_hotel = null){
        if ($id != null) {
            $this->db->where('divisi_hotel.Id_divisi',$id);
        }
        log_message('error', "id_hotel = ".$id_hotel);
        if ($id_hotel != null) {
            $this->db->where('divisi_hotel.Id_hotel',$id_hotel);
        }
        $this->db->join('hotel','hotel.Id_hotel = divisi_hotel.Id_hotel','inner');
        return $this->db->get($this->table)->result();
    }

    function delete($id = null){
        return $this->db->delete($this->table,['Id_divisi' => $id]);
    }

    function edit($data,$id = null){
        return $this->db->update($this->table,$data,['Id_divisi ' => $id]);
    }



}
?>