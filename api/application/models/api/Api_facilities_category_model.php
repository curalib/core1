<?php
class Api_facilities_category_model extends CI_Model {

    var $table = 'facilities_category';
    var $primary = 'Id_facilities_category';

    function getFacilitiesCategory($id_hotel){
        $this->db->where("facilities_category.Id_hotel",$id_hotel);
        $this->db->join('hotel','facilities_category.Id_hotel = hotel.Id_hotel','inner');
        $tmp = $this->db->get($this->table)->result();
        if($tmp){
            foreach($tmp as $key => $item){
                $tmp[$key]->fasilitas = $this->getFacilities($item->Id_facilities_category);
            }
        }
        return $tmp;
    }
    
    function getFacilities($id){
        $this->db->where("hotel_facilities.Is_hotel",1);
        $this->db->where('Id_facilities_category',$id);
        return $this->db->get('hotel_facilities')->result();   
    }

    function insertFacilitiesCategory($data){
        return $this->db->insert($this->table,$data);
    }

    function updateFacilitiesCategory($data,$id){
        return $this->db->update($this->table,$data,['Id_facilities_category' => $id]);
    }

    function deleteFacilitiesCategory($id){
        return $this->db->delete($this->table,['Id_facilities_category' => $id]);
    }
    function getFacilitiesCategoryById($id = null){
        if($id == null){
            return $this->db->get($this->table)->result();
        }else{
            return $this->db->get_where($this->table,['Id_facilities_category' => $id])->result(); 
        }
    }
}
?>