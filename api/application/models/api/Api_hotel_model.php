<?php
class Api_hotel_model extends CI_Model {

    var $table = 'hotel';
    var $primary = 'Id_hotel';

    function getHotel($id = null){
        if($id == null){
            return $this->db->get($this->table)->result();
        }else{
            $this->db->where('Id_hotel',$id);
            $tmp = $this->db->get($this->table)->result();
            if($tmp){
                foreach($tmp as $key => $value){
                    $tmp[$key]->Galery = $this->getGalery($value->Id_hotel);
                    $tmp[$key]->Facilities = $this->getFacilities($value->Id_hotel);
                    $tmp[$key]->User = $this->getUser($value->Id_hotel);
                    $tmp[$key]->Room = $this->getRoomType($value->Id_hotel);
                    $tmp[$key]->Food = $this->getFood($value->Id_hotel);
                }
            }
            return $tmp;
        }
    }

    function getHotelById($id){
        if($id == null){
            return $this->db->get($this->table)->result();
        }else{
            $this->db->where('Id_hotel',$id);
            return $this->db->get($this->table)->result();
        }
    }

    function getFood($id_hotel){
        $this->db->join('food','food.Id_food_category = food_category.Id_food_category','inner')  ;
        $this->db->where('food_category.Id_hotel',$id_hotel);
        $return = $this->db->get('food_category')->result();
        return count($return);
    }

    function getGalery($id_hotel){
        $this->db->where('Id_hotel',$id_hotel);
        $return = $this->db->get('galery_hotel')->result();
        return $return;
    }

    function getUser($id_hotel){
        $this->db->join('user_hotel','user_hotel.Id_user = user.Id_user','INNER');
        $this->db->join('hotel','hotel.Id_hotel = user_hotel.Id_hotel','INNER');
        $this->db->where('hotel.Id_hotel',$id_hotel);
        $return = $this->db->get('user')->result();
        return count($return);
    }

    function getFacilities($id_hotel){
        $this->db->join('hotel_facilities','hotel_facilities.Id_facilities_category = facilities_category.Id_facilities_category','inner')  ;
        $this->db->where('facilities_category.Id_hotel',$id_hotel);
        $return = $this->db->get('facilities_category')->result();
        return count($return);
    }

    function getRoomType($id_hotel){
        $this->db->join('room','room.Id_room_type = room_type.Id_room_type','inner')  ;
        $this->db->where('room_type.Id_hotel',$id_hotel);
        $return = $this->db->get('room_type')->result();
        return count($return);
    }

    function insertHotel($data){
        return $this->db->insert($this->table,$data);
    }

    function updatehHotel($data,$id){
        return $this->db->update($this->table,$data,['Id_hotel' => $id]);
    }

    function deleteHotel($id){
        return $this->db->delete($this->table,['Id_hotel' => $id]);
    }
    function getTotalRoom($id){
        $this->db->select("COUNT(room.Id_room) as totalRoom");
        $this->db->join('room','room.Id_room_type = room_type.Id_room_type','inner');
        $this->db->where('room_type.Id_hotel',$id);
        $return = $this->db->get('room_type')->row();
        return $return ? $return->totalRoom : 0;
    }
    function getTotalRoomUsed($id){
        $this->db->select("COUNT(room.Id_room) as totalRoom");
        $this->db->join('room','room.Id_room_type = room_type.Id_room_type','inner');
        $this->db->join('reservasi','reservasi.Id_room = room.Id_room','inner');
        $this->db->where('room_type.Id_hotel',$id);
        $this->db->where('room.Status',1);
        $this->db->where('reservasi.Tgl_masuk',date('Y-m-d'));
        $return = $this->db->get('room_type')->row();
        return $return ? $return->totalRoom : 0;
    }

    function setHotelPublicTv($data){
        return $this->db->insert("Public_TV",$data);
    }
    function getListHotel(){
        return $this->db->get($this->table)->result();
    }

    function getTotalHotel(){
        $this->db->select("count(Id_hotel) as jml");
        $tmp = $this->db->get('hotel')->row();
        return $tmp->jml;
    }
    function HotelList(){
        $query = $this->db->query("SELECT hotel.*, (SELECT user.Username FROM user INNER JOIN user_hotel ON user_hotel.Id_user = user.Id_user WHERE user_hotel.Id_hotel = hotel.Id_hotel AND user_hotel.Is_pemilik = 1) as person FROM `hotel`");
        return $query->result();
    }
    function getHotelCode(){
        $code = uniqid('QNESYS_',false);
        $this->db->where('Id_hotel',$code);
        $tmp = $this->db->get('hotel')->row();
        if ($tmp) {
            $this->getHotelCode();
        }else{
            return $code;
        }
    }

}
