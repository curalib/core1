<?php
class Api_login_model extends CI_Model {

    var $table = 'user';
    var $primary = 'Id_user';

    function GetLogin($is_guest = null,$username = null){
    	if($is_guest == 0){
            $this->db->where("user.Email",$username);
            $this->db->where("user.Is_guest",$is_guest);
    		return $this->db->get($this->table)->row();
    	}else if($is_guest == 1){
            $this->db->join('user_hotel','user_hotel.Id_user = user.Id_user','INNER');
            // $this->db->join('user_group','user_group.Id_user_group = user.Id_user_group','INNER');
            $this->db->join('hotel','hotel.Id_hotel = user_hotel.Id_hotel','INNER');
            $this->db->join('jabatan_hotel','jabatan_hotel.Id_jabatan = user_hotel.Id_jabatan','INNER');
            $this->db->join('divisi_hotel','divisi_hotel.Id_divisi = jabatan_hotel.Id_divisi','INNER');
            $this->db->where("user.Email",$username);
            $this->db->where("user.Is_guest",$is_guest);
            return $this->db->get($this->table)->row();
        }
    }

    function insertLoginLog($data){
        return $this->db->insert('login_log',$data);
    }

    function GetIsguest($username){
        $this->db->where('user.Email',$username);
        return $this->db->get('user')->row();
    }
    function getAdminQnesys($is_guest = null,$username = null){
        $this->db->where("user.Email",$username);
        $this->db->where("user.Is_guest",$is_guest);
        return $this->db->get($this->table)->row();
    }
}
?>