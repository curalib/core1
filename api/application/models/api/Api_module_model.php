<?php
class Api_module_model extends CI_Model {

    private $table = 'module';
    private $primary = 'Id_module';

    function create($data){
        return $this->db->insert($this->table,$data);
    }
    function createAction($data){
        return $this->db->insert('action_module',$data);
    }
    function read($id_role = null){
        $tmp = $this->db->get($this->table)->result();
        if ($tmp) {
            foreach ($tmp as $key => $item) {
                $tmp[$key]->EditModule = false;
                $tmp[$key]->ShowActionForm = false;
                $tmp[$key]->ACTION = $this->getActionByModule($item->Id_module,$id_role);
            }
        }
        return $tmp;
    }

    function delete($id = null){
        return $this->db->delete($this->table,['Id_divisi' => $id]);
    }

    function deleteAction($id = null){
        return $this->db->delete("action_module",['Id_action' => $id]);
    }

    function edit($data,$id = null){
        return $this->db->update($this->table,$data,['Id_divisi ' => $id]);
    }

    function getActionByModule($id_module = null,$id_group = null){
        $this->db->where('Id_module',$id_module);
        $tmp = $this->db->get('action_module')->result();
        if (!is_null($id_group)) {
            foreach ($tmp as $key => $item) {
                $tmp[$key]->checked = $this->checkActionGroup($id_group,$item->Id_action);
            }
        }
        return $tmp;
    }
    function checkActionGroup($id_group,$id_action){
        $this->db->where('Id_user_group',$id_group);
        $this->db->where('Id_action',$id_action);
        $this->db->select('COUNT(Id_user_group_module) as jml');
        $tmp = $this->db->get('user_group_module')->row();
        return $tmp->jml != 0;
    }
    function getGroupAction($id_group){
        $this->db->where('Id_user_group',$id_group);
        return $this->db->get('user_group_module')->result();
    }

    function deleteUserGroupAction($idRole){
        return $this->db->delete("user_group_module",['Id_user_group_module' => $idRole]);
    }
    function CreateRoleAction($data){
        $this->db->insert("user_group_module",$data);
    }
    function SaveRoleAction($data){
        $oldRoleAction = $this->getGroupAction($data['Id_user_group']);
        foreach ($oldRoleAction as $key => $item) {
            $this->deleteUserGroupAction($item->Id_user_group_module);
        }
        foreach ($data['action'] as $key => $value) {
            $add = array(
                "Id_user_group"   =>  $data['Id_user_group'],
                "Id_action" =>  $value
            );
            $this->CreateRoleAction($add);
        }
    }



}
?>