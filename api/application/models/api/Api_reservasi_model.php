<?php
class Api_reservasi_model extends CI_Model {

    var $table = 'reservasi';
    var $primary = 'Id_reservasi';

    function getReservasi($id = null){
        if($id != null){
            return $this->db->get_where($this->table,['Id_room' => $id])->result_array();    
        }else{
            return $this->db->get($this->table)->result_array();    
        }
        
    }
}
?>