<?php
class Api_food_category_model extends CI_Model {

    var $table = 'food_category';
    var $primary = 'Id_food_category';

    function getFoodCategory($id = null){
        $this->db->select('food_category.Id_food_category,food_category.Food_category,food_category.Pic as Picture,food_category.Id_hotel');
        $this->db->where("food_category.Id_hotel",$id);
        // $this->db->join('hotel','hotel.Id_hotel = food_category.Id_hotel','inner');
        $tmp = $this->db->get($this->table)->result();
        if($tmp){
            foreach($tmp as $key => $item){
                $tmp[$key]->Food = $this->getFood($item->Id_food_category);
            }
        }
        return $tmp;
    }

    function getFood($id){
        $this->db->where('Id_food_category',$id);
        return $this->db->get('food')->result();   
    }

    function getFoodCategoryById($id = null){
        if($id == null){
            return $this->db->get($this->table)->result_array();
        }else{
            return $this->db->get_where($this->table,['Id_food_category' => $id])->result_array();
        }
    }

    function insertFoodCategory($data){
        return $this->db->insert($this->table,$data);
    }

    function updateFoodCategory($data,$id){
        return $this->db->update($this->table,$data,['Id_food_category' => $id]);
    }

    function deleteFoodCategory($id){
        return $this->db->delete($this->table,['Id_food_category' => $id]);
    }
}
?>