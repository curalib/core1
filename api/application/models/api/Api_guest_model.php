<?php
class Api_guest_model extends CI_Model {

    function getRoom($id_hotel){
        $this->db->where('room_type.Id_hotel',$id_hotel);
        $this->db->join('room_type','room_type.Id_room_type = room.Id_room_type','INNER');
        $tmp = $this->db->get("room")->result();
        $data = null;
        if ($tmp) {
            foreach ($tmp as $key => $item) {
                $reservasi = $this->getReservasi($item->Id_room);
                if ($reservasi) {
                    foreach ($reservasi as $key => $reserv) {
                        $data[] = $this->getUser($reserv->Id_user);
                    }
                }
            }
        }
        return $data;
    }
    function getReservasi($id_room){
        $this->db->where('reservasi.Id_room',$id_room);
        $this->db->group_by('reservasi.Id_user');
        return $this->db->get("reservasi")->result();
    }
    function getUser($id_user){
        $this->db->where('Id_user',$id_user);
        return $this->db->get("user")->row();
    }
}
?>