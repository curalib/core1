<?php
class Api_user_group_model extends CI_Model {

    private $table = 'user_group';
    private $primary = 'Id_user_group';

    function create($data){
        return $this->db->insert($this->table,$data);
    }
    function read(){
        return $this->db->get($this->table)->result();
    }


}
?>