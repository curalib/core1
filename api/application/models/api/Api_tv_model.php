<?php
class Api_tv_model extends CI_Model {

    var $table = 'tv';
    var $primary = 'Id_tv';
    
    function read(){
        $tmp = $this->db->get('tv_category')->result();
        if($tmp){
            foreach($tmp as $key => $item){
                $tmp[$key]->TV = $this->getTv($item->Id_tv_category);
            }
        }
        return $tmp;
    }

    function getAdsBypotion($position,$id){
        $this->db->where('ID',$position);
        $this->db->where('Id_hotel',$id);
        return $this->db->get('Setting_TV')->result();
    }

    function getAds($id_hotel = bull){
        $this->db->where('Id_hotel',$id_hotel);
        return $this->db->get('Setting_TV')->result();
    }    

    function deleteAds($id){
       return $this->db->delete('Setting_TV',['ID_SETTING' => $id]);
    }
    
    function getById($id = null){
        $this->db->join('tv_category','tv_category.Id_tv_category = tv.Id_tv_category','INNER');
        if($id == null){
            return $this->db->get($this->table)->result();
        }else{
            $this->db->where('Id_tv',$id);
            return $this->db->get($this->table)->result();
        }
    }
    
    function getTv($id){
        $this->db->where('Id_tv_category',$id);
        return $this->db->get($this->table)->result();
    }
    
    function getTvList($id){
        $this->db->where('Id_tv_category',$id);
        return $this->db->get('tv')->result();   
    }

    function insertTv($data){
        return $this->db->insert($this->table,$data);
    }

    function updateTv($data,$id){
        return $this->db->update($this->table,$data,['Id_tv' => $id]);
    }

    function deleteTv($id){
        return $this->db->delete($this->table,['Id_tv' => $id]);
    }
    
    function getSetting($id,$Id_hotel){
	   $this->db->where('ID',$id);
	   $this->db->where('Id_hotel',$Id_hotel);
	   return $this->db->get('Setting_TV')->row();
    }
    function updateSetting($data,$id){
        return $this->db->update("Setting_TV",$data,['ID' => $id]);
    }
}
?>