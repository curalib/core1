<?php
class Api_menu_model extends CI_Model {

    var $table = 'menu';
    var $primary = 'Id_menu';

    function getMenu($id = null){
    	if($id != null){
    		return $this->db->get_where($this->table,['Id_menu' => $id])->result_array();
    	}else{
    		return $this->db->get($this->table)->result_array();
    	}

    }
    function getByMainMenu($is_main_menu){
        return $this->db->get_where($this->table,['is_main_menu' => $is_main_menu])->result();
    }

    function insertMenu($data){
        return $this->db->insert($this->table,$data);
    }

    function updateMenu($data,$id){
        return $this->db->update($this->table,$data,['Id_menu' => $id]);
    }

    function deleteMenu($id){
        return $this->db->delete($this->table,['Id_menu' => $id]);
    }


    function getGroupMenu($idGroup, $is_main_menu){
        $this->db->where('user_group_menu.id_user_group',$idGroup);
        $this->db->where('menu.is_main_menu',$is_main_menu);
        $this->db->order_by('menu.Menu','ASC');
        $this->db->join('menu','menu.Id_menu = user_group_menu.Id_menu','inner');
        return $this->db->get('user_group_menu')->result();
    }
    function getMenuQnesys($id_user_group,$is_main_menu){
        $qnesysDB = $this->load->database('qnesys',true);
        $qnesysDB->select("*");
        $qnesysDB->where('user_group_menu.id_user_group',$id_user_group);
        $qnesysDB->where('menu.is_main_menu',$is_main_menu);
        $qnesysDB->order_by('menu.Menu','ASC');
        $qnesysDB->join('menu','menu.Id_menu = user_group_menu.Id_menu','inner');
        return $qnesysDB->get('user_group_menu')->result();
    }
}
