<?php
class Api_around_model extends CI_Model {

    var $table = 'around_hotel';
    var $primary = 'Id_around';

    function getAround($id = null){
    	if($id != null){
    		return $this->db->get_where($this->table,['Id_hotel' => $id])->result_array();
    	}else{
            // $this->db->join('hotel','hotel.Id_hotel = around_hotel.Id_hotel','INNER');
    		return $this->db->get($this->table)->result();	
    	}
        
    }

    function insertAround($data){
        return $this->db->insert($this->table,$data);
    }

    function updateAround($data,$id){
        return $this->db->update($this->table,$data,['Id_around' => $id]);
    }

    function deleteAround($id){
        return $this->db->delete($this->table,['Id_around' => $id]);
    }
    
    function getAroundById($id){
        if($id == null){
            return $this->db->get($this->table)->result();
        }else{
            return $this->db->get_where($this->table,['Id_around' => $id])->result(); 
        }
    }
}
?>