<?php
class Api_jabatan_model extends CI_Model {

    private $table = 'jabatan_hotel';
    private $primary = 'Id_jabatan';

    function create($data){
        return $this->db->insert($this->table,$data);
    }
    function read($id = null){
        if ($id != null) {
            $this->db->where('hotel.Id_hotel',$id);
        }
        $this->db->join('divisi_hotel','divisi_hotel.Id_divisi = jabatan_hotel.Id_divisi','inner');
        $this->db->join('hotel','hotel.Id_hotel = divisi_hotel.Id_hotel','inner');
        return $this->db->get($this->table)->result();
    }

    function delete($id = null){
        return $this->db->delete($this->table,['Id_jabatan' => $id]);
    }

    function edit($data,$id = null){
        return $this->db->update($this->table,$data,['Id_jabatan ' => $id]);
    }
    function byDivisi($id){
        $this->db->where('Id_divisi',$id);
        return $this->db->get($this->table)->result();
    }



}
?>