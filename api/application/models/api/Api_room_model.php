<?php
class Api_room_model extends CI_Model {

    var $table = 'room';
    var $primary = 'Id_room';

    function getRoom($id = null){
        $this->db->join('room_type','room_type.Id_room_type = room.Id_room_type','INNER');
        $this->db->join('hotel_facilities','hotel_facilities.Id_hotel = room_type.Id_hotel','INNER');
        $this->db->join('reservasi','reservasi.Id_room = room.Id_room','INNER');
    	if($id != null){
    		return $this->db->get_where($this->table,['room_type.Id_room_type' => $id])->result_array();
    	}else{
    		return $this->db->get($this->table)->result_array();	
    	}
    }

    
    function getRoomById($id){
        $this->db->where("Id_room",$id);
        return $this->db->get($this->table)->result();
    }

    function insertRoom($data){
        return $this->db->insert($this->table,$data);
    }

    function create_activity($data){
        return $this->db->insert('activity',$data);   
    }

    function updateRoom($data,$id){
        return $this->db->update($this->table,$data,['Id_room' => $id]);
    }

    function deleteRoom($id){
        return $this->db->delete($this->table,['Id_room' => $id]);
    }
    function SaveRoomFasilitas($data){
        $this->db->insert("hotel_facilities",$data);
    }   
    function GetRoomByType($id){
        $this->db->where('Id_room_type',$id);
        return $this->db->get($this->table)->result();
    }
    function GetRoomGuest($idRoom){
        $this->db->where('room.Id_room',$idRoom);
        $this->db->where('reservasi.flag_checkout',0);
        $this->db->join('reservasi','reservasi.Id_room = room.Id_room','inner');
        $this->db->join('user','user.Id_user = reservasi.Id_user','inner');
        return $this->db->get($this->table)->row();
    }
    function CheckoutReservasi($data,$id_reservasi){
        return $this->db->update('reservasi',$data,['Id_reservasi' => $id_reservasi]);
    }
    function GetRoomByHotel($id_hotel){
        $this->db->where('room_type.Id_hotel',$id_hotel);
        $this->db->join("room_type","room_type.Id_room_type = room.Id_room_type","inner");
        return $this->db->get($this->table)->result();
    }
    function getSettingLogo($id_hotel,$type){
        $this->db->where('Id_hotel',$id_hotel);
        $this->db->where('ID',$type);
        return $this->db->get('Setting_TV')->row();
    }
}
?>