<?php
class Api_galery_room_model extends CI_Model {

    var $table = 'galery_room';
    var $primary = 'Id_galery_room';

    function getGaleryRoom($id = null){
        if($id != null){
            return $this->db->get_where($this->table,['Id_galery_room' => $id])->result_array();    
        }else{
            $this->db->join('hotel','hotel.Id_hotel = galery_room.Id_hotel','INNER');
            return $this->db->get($this->table)->result_array();    
        }
        
    }

    function insertGaleryRoom($data){
        return $this->db->insert($this->table,$data);
    }

    function updateGaleryRoom($data,$id){
        return $this->db->update($this->table,$data,['Id_galery_room ' => $id]);
    }

    function deleteGaleryRoom($id){
        return $this->db->delete($this->table,['Id_galery_room ' => $id]);
    }
}
?>