<?php
class Api_tv_category_model extends CI_Model {

    var $table = 'tv_category';
    var $primary = 'Id_tv_category';

    function getTvCategory(){
        $tmp = $this->db->get($this->table)->result();
        if($tmp){
            foreach($tmp as $key => $item){
                $tmp[$key]->fasilitas = $this->getTv($item->Id_tv_category);
            }
        }
        return $tmp;
    }

    function insertTvCategory($data){
        return $this->db->insert($this->table,$data);
    }

    function updateTvCategory($data,$id){
        return $this->db->update($this->table,$data,['Id_tv_category' => $id]);
    }

    function deleteTvCategory($id){
        return $this->db->delete($this->table,['Id_tv_category' => $id]);
    }

    function getTvCategoryAll($id = null){
        if($id == null){
            return $this->db->get($this->table)->result_array();
        }else{
            return $this->db->get_where($this->table,['Id_tv_category' => $id])->result_array();
        }
    }
}
?>