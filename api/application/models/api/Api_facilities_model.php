<?php
class Api_facilities_model extends CI_Model {

    var $table = 'hotel_facilities';
    var $primary = 'Id_hotel_Facilities';

    function getFacilities($id_hotel = null){
        $this->db->select('hotel_facilities.*,facilities_category.Category');
        $this->db->join('facilities_category','facilities_category.Id_facilities_category = hotel_facilities.Id_facilities_category','INNER');
        $this->db->join('hotel','hotel.Id_hotel = facilities_category.Id_hotel','inner');
        if($id_hotel != null){
            $this->db->where('hotel.Id_hotel',$id_hotel);
            return $this->db->get($this->table)->result_array();
        }else{
            $this->db->where('Is_hotel',1);
            return $this->db->get($this->table)->result_array();    
        }
        
    }

    function insertFacilities($data = null){
        if(!is_null($data)){
            $data = $this->db->insert($this->table,$data);
            // if(is_array($data)){
            //     foreach($data as $key => $value){
            //         $data[$key]->Picture = base_url()."assets/img".$value->Picture;
            //     }   
            // }
            return $data;
        }else{
            return 0;
        }
    }

    function updateFacilities($data,$id){
        return $this->db->update($this->table,$data,['Id_hotel_Facilities' => $id]);
    }

    function deleteFacilities($id){
        return $this->db->delete($this->table,['Id_hotel_Facilities' => $id]);
    }

    function getfacilitiesById($id = null){
        if($id == null){
            return $this->db->get($this->table)->result();
        }else{
            return $this->db->get_where($this->table,['Id_hotel_Facilities' => $id])->result(); 
        }
    }
}
?>