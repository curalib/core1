<?php
class Api_room_type_model extends CI_Model {

    var $table = 'room_type';
    var $primary = 'Id_room_type';

    function getRoomType($id_hotel){
        $this->db->where("room_type.Id_hotel",$id_hotel);
        $this->db->join('hotel','hotel.Id_hotel = room_type.Id_hotel','INNER');
        $tmp = $this->db->get($this->table)->result();
        if($tmp){
            foreach($tmp as $key => $item){
                $tmp[$key]->Room = $this->getRoom($item->Id_room_type);
            }
        }
        return $tmp;
    }

    function getRoom($id){
        $this->db->where('Id_room_type',$id);
        $this->db->order_by('Status',"ASC");
        $return = $this->db->get('room')->result();   
        if($return){
            foreach($return as $key => $value){
                if($value->Status == 1) {
                    $return[$key]->Reservasi = $this->getReservasi($value->Id_room);
                }
            }
            foreach ($return as $key => $rt) {
                $return[$key]->Facilitas = $this->getFasilitasRoom($rt->Id_room);
                $return[$key]->GALERY = $this->getGaleryRoom($rt->Id_room);
            }
            
        }
        return $return; 
    }
    function getGaleryRoom($id_room){
        $this->db->where('Id_room',$id_room);
        return $this->db->get('galery_room')->result();
    }

    function getFasilitasRoom($id_room){
        $this->db->where('hotel_facilities.Is_hotel',0);
        $this->db->where('hotel_facilities.Id_facilities_category',$id_room);
        return $this->db->get('hotel_facilities')->result();
    }

    function getReservasi($id){
        $this->db->where('Id_room',$id);
        $this->db->where('flag_checkout',0);
        return $this->db->get('reservasi')->result();      
    }

    function getRoomTypeById($id){
        $this->db->where('Id_room_type',$id);
        return $this->db->get($this->table)->result();
    }

    function insertRoomType($data){
        return $this->db->insert($this->table,$data);
    }

    function updateRoomType($data,$id){
        return $this->db->update($this->table,$data,['Id_room_type' => $id]);
    }

    function deleteRoomType($id){
        return $this->db->delete($this->table,['Id_room_type' => $id]);
    }
    function getRoomTypeByHotel($id){
        $this->db->where('Id_hotel',$id);
        return $this->db->get($this->table)->result();
    }
}
?>