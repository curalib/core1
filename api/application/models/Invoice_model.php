<?php
class Invoice_model extends CI_Model {

  function getInvoiceByID($id = null){
    $this->db->select("izin_apps_invdet.id as idinv,izin_apps_invdet.*,izin_apps_lead.*,izin_apps_package.*");
    $this->db->join('izin_apps_lead','izin_apps_invdet.id_lead = izin_apps_lead.id_lead','inner');
    $this->db->join('izin_apps_package','izin_apps_package.kode = izin_apps_lead.package_id','inner');
    $this->db->where('izin_apps_invdet.va_number',$id);
    return $this->db->get('izin_apps_invdet')->row();
  }

  public function savelog($data){
        return $this->db->insert("log_bca",$data);
  }

  public function updateInv($data,$id){
    return $this->db->update("izin_apps_invdet",$data,['id' => $id]);
  }

  public function invItem($idinv){
    $this->db->select('item');
    $this->db->where('id_inv',$idinv);
    $tmp =  $this->db->get('izin_apps_invitem')->row();
    if($tmp){
      return $tmp->item;
    }else{
      return "item tidak di temukan";
    }
  }

  public function itemitem($id){
    $this->db->select('item, price, no_urut');
    $this->db->where('id_inv',$id);
    $this->db->order_by('no_urut','ASC');
    return $this->db->get('izin_apps_invitem')->result();
  }

  public function invCc(){
    $this->db->select('nama_asli,uid');
    $this->db->where('priv',2);
    return $this->db->get('izin_sys_lgndetail')->result();
  }

}
?>
