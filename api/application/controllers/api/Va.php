<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Va extends REST_Controller
{
  private $clientIDWAHANA = "11791";
  private $clientIDIJIN = "11794";
  private $clientSecret = "BCA01";
  private $clientHukum = "13379";
  private $Apikey = "BCA01";
  private $ApiSecret = "BCA01";
  function __construct()
  {
    parent::__construct();
    $this->load->model('Invoice_model', 'inv');
  }
  public function bills_post()
  {
    $token = explode(" ", $this->input->get_request_header('Authorization'));
    log_message('error', $this->input->get_request_header('Authorization'));
    if (sizeof($token) > 0) {
      if ($this->is_base64($token[1])) {
        $tmp = explode("#", base64_decode($token[1]));
        if ($tmp[0] == $this->clientIDIJIN || $tmp[0] == $this->clientIDWAHANA || $tmp[0] == $this->clientHukum) {
          if ($_SERVER["CONTENT_TYPE"] != "application/json") {
            $this->response([
              'error' => true,
              'message' => "content type is not valid"
            ], REST_Controller::HTTP_OK);
            exit;
          } else {
            $post = json_decode(file_get_contents('php://input'));
            $insert['code_log'] = "bills";
            $insert['lognya'] = json_encode($post);
            $this->inv->savelog($insert);

            $InquiryReason = [
              "Indonesian"  =>  "Sukses",
              "English"  =>  "Success",
            ];
            $InquiryStatus = "00";
            $CustomerName = "";
            $TotalAmount = "";
            $SubCompany = "";

            if (empty($post->CustomerNumber)) {
              $InquiryReason = [
                "Indonesian"  =>  "CustomerNumber Kosong",
                "English"  =>  "CustomerNumber Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->RequestID)) {
              $InquiryReason = [
                "Indonesian"  =>  "RequestID Kosong",
                "English"  =>  "RequestID Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->ChannelType)) {
              $InquiryReason = [
                "Indonesian"  =>  "ChannelType Kosong",
                "English"  =>  "ChannelType Empty",
              ];
              $InquiryStatus = "01";
            }
            if (!$this->validateDate($post->TransactionDate)) {
              $InquiryReason = [
                "Indonesian"  =>  "TransactionDate Tidak Sesuai",
                "English"  =>  "TransactionDate Not suitable",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->TransactionDate)) {
              $InquiryReason = [
                "Indonesian"  =>  "TransactionDate Kosong",
                "English"  =>  "TransactionDate Empty",
              ];
              $InquiryStatus = "01";
            }
            if ($post->CompanyCode != $tmp[0]) {
              $InquiryReason = [
                "Indonesian"  =>  "CompanyCode Tidak Sesuai",
                "English"  =>  "CompanyCode Not suitable",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->CompanyCode)) {
              $InquiryReason = [
                "Indonesian"  =>  "CompanyCode Kosong",
                "English"  =>  "CompanyCode Empty",
              ];
              $InquiryStatus = "01";
            }
            if (!empty($post->CompanyCode) && !empty($post->CustomerNumber)) {
              $invoice = $this->inv->getInvoiceByID($post->CompanyCode . $post->CustomerNumber);
              if ($invoice) {
                if ($invoice->va_status != 1) {
                  $CustomerName = $invoice->client_name;
                  $TotalAmount = $invoice->total_due . ".00";
                  $SubCompany = "00000";
                } else {
                  $InquiryReason = [
                    "Indonesian"  =>  "Bill Sudah Selesai",
                    "English"  =>  "Bill Already Done",
                  ];
                  $InquiryStatus = "01";
                }
              } else {
                $InquiryReason = [
                  "Indonesian"  =>  "Customer Tidak Ditemukan",
                  "English"  =>  "Customer Not Found",
                ];
                $InquiryStatus = "01";
              }
            }
            // if($post->CustomerNumber == "1907005")
            $datar = [
              'CompanyCode' => $post->CompanyCode,
              'CustomerNumber' => $post->CustomerNumber,
              'RequestID' => $post->RequestID,
              'InquiryStatus' => $InquiryStatus,
              'InquiryReason' => $InquiryReason,
              'CustomerName'  => $CustomerName,
              'CurrencyCode'  =>  "IDR",
              'TotalAmount' =>  $TotalAmount,
              'SubCompany'  =>  $SubCompany,
              'DetailBills' => [],
              'FreeTexts' =>  [],
              'AdditionalData'  =>  ""
            ];
            $insert['code_log'] = "Rbills";
            $insert['lognya'] = json_encode($datar);
            $this->inv->savelog($insert);
            $this->response($datar, REST_Controller::HTTP_OK);
            exit;
          }
        } else {
          $this->response([
            'error' => true,
            'message' => "ClientID is not valid"
          ], REST_Controller::HTTP_OK);
          exit;
        }
      } else {
        $this->response([
          'error' => true,
          'message' => "Base64 is not valid"
        ], REST_Controller::HTTP_OK);
        exit;
      }
    } else {
      $this->response([
        'error' => true,
        'message' => "Authorization is not valid"
      ], REST_Controller::HTTP_OK);
      exit;
    }
  }

  function is_base64($str)
  {
    if (base64_decode($str) !== false) {
      return true;
    } else {
      return false;
    }
  }

  public function payments_post()
  {
    $token = explode(" ", $this->input->get_request_header('Authorization'));
    log_message('error', $this->input->get_request_header('Authorization'));
    if (sizeof($token) > 0) {
      if ($this->is_base64($token[1])) {
        $tmp = explode("#", base64_decode($token[1]));
        if ($tmp[0] == $this->clientIDIJIN || $tmp[0] == $this->clientIDWAHANA || $tmp[0] == $this->clientHukum) {
          if ($_SERVER["CONTENT_TYPE"] != "application/json") {
            $this->response([
              'error' => true,
              'message' => "content type is not valid"
            ], REST_Controller::HTTP_OK);
            exit;
          } else {
            $post = json_decode(file_get_contents('php://input'));
            $insert['code_log'] = "payments";
            $insert['lognya'] = json_encode($post);
            $this->inv->savelog($insert);

            $InquiryReason = [
              "Indonesian"  =>  "Sukses",
              "English"  =>  "Success",
            ];
            $InquiryStatus = "00";
            $CustomerName = "";
            $TotalAmount = "";
            $SubCompany = "";
            if (empty($post->CustomerNumber)) {
              $InquiryReason = [
                "Indonesian"  =>  "CustomerNumber Kosong",
                "English"  =>  "CustomerNumber Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->RequestID)) {
              $InquiryReason = [
                "Indonesian"  =>  "RequestID Kosong",
                "English"  =>  "RequestID Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->ChannelType)) {
              $InquiryReason = [
                "Indonesian"  =>  "ChannelType Kosong",
                "English"  =>  "ChannelType Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->CustomerName)) {
              $InquiryReason = [
                "Indonesian"  =>  "CustomerName Kosong",
                "English"  =>  "CustomerName Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->CurrencyCode)) {
              $InquiryReason = [
                "Indonesian"  =>  "CurrencyCode Kosong",
                "English"  =>  "CurrencyCode Empty",
              ];
              $InquiryStatus = "01";
            }
            $tmpPaid = (float) $post->PaidAmount;
            $tmptotal = (float) $post->TotalAmount;
            if ($tmpPaid < $tmptotal) {
              $InquiryReason = [
                "Indonesian"  =>  "PaidAmount tidak boleh lebih kecil dari TotalAmount",
                "English"  =>  "PaidAmount cannot be less than TotalAmount",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->PaidAmount)) {
              $InquiryReason = [
                "Indonesian"  =>  "PaidAmount Kosong",
                "English"  =>  "PaidAmount Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->TotalAmount)) {
              $InquiryReason = [
                "Indonesian"  =>  "TotalAmount Kosong",
                "English"  =>  "TotalAmount Empty",
              ];
              $InquiryStatus = "01";
            }
            if (!$this->validateDate($post->TransactionDate)) {
              $InquiryReason = [
                "Indonesian"  =>  "TransactionDate Tidak Sesuai",
                "English"  =>  "TransactionDate Not suitable",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->TransactionDate)) {
              $InquiryReason = [
                "Indonesian"  =>  "TransactionDate Kosong",
                "English"  =>  "TransactionDate Empty",
              ];
              $InquiryStatus = "01";
            }
            if (!$this->CheckValidFlagAdvice($post->FlagAdvice)) {
              $InquiryReason = [
                "Indonesian"  =>  "FlagAdvice Tidak Sesuai",
                "English"  =>  "FlagAdvice Not suitable",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->FlagAdvice)) {
              $InquiryReason = [
                "Indonesian"  =>  "FlagAdvice Kosong",
                "English"  =>  "FlagAdvice Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->Reference)) {
              $InquiryReason = [
                "Indonesian"  =>  "Reference Kosong",
                "English"  =>  "Reference Empty",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->SubCompany)) {
              $InquiryReason = [
                "Indonesian"  =>  "SubCompany Kosong",
                "English"  =>  "SubCompany Empty",
              ];
              $InquiryStatus = "01";
            }
            if ($post->CompanyCode != $tmp[0]) {
              $InquiryReason = [
                "Indonesian"  =>  "CompanyCode Tidak Sesuai",
                "English"  =>  "CompanyCode Not suitable",
              ];
              $InquiryStatus = "01";
            }
            if (empty($post->CompanyCode)) {
              $InquiryReason = [
                "Indonesian"  =>  "CompanyCode Kosong",
                "English"  =>  "CompanyCode Empty",
              ];
              $InquiryStatus = "01";
            }
            if ((!empty($post->CompanyCode) && !empty($post->CustomerNumber)) && $InquiryStatus == "00") {
              $invoice = $this->inv->getInvoiceByID($post->CompanyCode . $post->CustomerNumber);
              if ($invoice) {
                if ($post->TotalAmount != $invoice->total_due . ".00") {
                  $InquiryReason = [
                    "Indonesian"  =>  "TotalAmount Tidak Sesuai",
                    "English"  =>  "TotalAmount Not suitable",
                  ];
                  $InquiryStatus = "01";
                } else {
                  if ($invoice->va_status != 1) {
                    $CustomerName = $invoice->client_name;
                    $TotalAmount = $invoice->total_due . ".00";
                    $SubCompany = "00000";
                    $dataUpdate['va_status'] = 1;
                    $this->inv->updateInv($dataUpdate, $invoice->idinv);
                  } else {
                    $InquiryReason = [
                      "Indonesian"  =>  "Bill Sudah Selesai",
                      "English"  =>  "Bill Already Done",
                    ];
                    $InquiryStatus = "01";
                  }
                }
              } else {
                $InquiryReason = [
                  "Indonesian"  =>  "Customer Tidak Ditemukan",
                  "English"  =>  "Customer Not Found",
                ];
                $InquiryStatus = "01";
              }
            }
            $this->response([
              'CompanyCode' => $post->CompanyCode,
              'CustomerNumber' => $post->CustomerNumber,
              'RequestID' => $post->RequestID,
              'PaymentFlagStatus' => $InquiryStatus,
              'PaymentFlagReason' => $InquiryReason,
              'CustomerName'  => $CustomerName,
              'CurrencyCode'  =>  $post->CurrencyCode,
              'PaidAmount'  =>  $post->PaidAmount,
              'TotalAmount' =>  $post->TotalAmount,
              'TransactionDate' =>  $post->TransactionDate,
              'DetailBills' => [],
              'FreeTexts' =>  [],
              'AdditionalData'  =>  ""
            ], REST_Controller::HTTP_OK);
            exit;
          }
        } else {
          $this->response([
            'error' => true,
            'message' => "ClientID is not valid"
          ], REST_Controller::HTTP_OK);
          exit;
        }
      } else {
        $this->response([
          'error' => true,
          'message' => "Base64 is not valid"
        ], REST_Controller::HTTP_OK);
        exit;
      }
    } else {
      $this->response([
        'error' => true,
        'message' => "Authorization is not valid"
      ], REST_Controller::HTTP_OK);
      exit;
    }
  }


  public function validateDate($date)
  {
    $tmp1 = explode(" ", $date);
    if (sizeof($tmp1) != 2) {
      return false;
    } else {
      $tmpdate = explode("/", $tmp1[0]);
      $tmptime = explode(":", $tmp1[1]);
      if (sizeof($tmpdate) == 3 && sizeof($tmptime) == 3) {
        if ($this->validDate($tmpdate[2] . "-" . $tmpdate[1] . "-" . $tmpdate[0])) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
    // print_r(sizeof($tmp1));
  }

  function validDate($date, $format = 'Y-m-d')
  {
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns true for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
  }
  public function CheckValidFlagAdvice($flag)
  {
    if ($flag == "Y" || $flag == "N") {
      return true;
    } else {
      return false;
    }
  }
}
