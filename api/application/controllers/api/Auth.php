<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
class Auth extends REST_Controller {

      private $clientIDWAHANA = "11791";
      private $clientIDIJIN = "11794";
      private $clientWahanaIjin = "12280";
      private $clientHukum = "13379";
      private $clientSecret = "BCA01";
      private $Apikey = "BCA01";
      private $ApiSecret = "BCA01";


    function __construct()
    {
        parent::__construct();
        $this->load->model('Invoice_model','inv');
    }

    //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzY0NzI2NjksImlzcyI6ImxvY2FsaG9zdCIsImV4cCI6MTU4NzI3MjY2OSwidXNlcklkIjoidXNlcjAxIn0.wSlZasRSdj3z8u3BysXYtZ9suBQyCtTbVmKkzLCZfHQ
    public function index_post(){
      $token = explode(" ",$this->input->get_request_header('Authorization'));
      log_message('error', $this->input->get_request_header('Authorization'));
      $insert['code_log'] = "auth";
      $insert['lognya'] = $this->input->get_request_header('Authorization');
      $this->inv->savelog($insert);
      if(sizeof($token) > 0){
        if($this->is_base64($token[1])){
          $tmp = explode(":",base64_decode($token[1]));
          if($tmp[0] == $this->clientIDIJIN || $tmp[0] == $this->clientIDWAHANA || $tmp[0] == $this->clientWahanaIjin  || $tmp[0] == $this->clientHukum){
              $this->response([
                  'access_token' => base64_encode($tmp[0]."#".date('Y-m-d H:i:s')),
                  'token_type' => 'bearer',
                  'expires_in' => 3600,
                  'scope' => "resource.WRITE resource.READ"
              ], REST_Controller::HTTP_OK);
              exit;
          }else{
              $this->response([
                  'error' => true,
                  'message'=>"ClientID is not valid"
              ], REST_Controller::HTTP_OK);
              exit;
          }
        }else{
              $this->response([
                  'error' => true,
                  'message'=>"Base64 is not valid"
              ], REST_Controller::HTTP_OK);
              exit;
        }
      }else{
          $this->response([
              'error' => true,
              'message'=>"Authorization is not valid"
          ], REST_Controller::HTTP_OK);
          exit;
      }
    }
    public function token_post(){
      $token = explode(" ", $this->input->get_request_header('Authorization'));
      log_message('error', $this->input->get_request_header('Authorization'));
      $this->response([
          'access_token' => base64_encode(date('Y-m-d')),
          'token_type' => 'bearer',
          'expires_in' => 3600,
          'scope' => "resource.WRITE resource.READ"
      ], REST_Controller::HTTP_OK);
      exit;
    }

    function is_base64($str){
      if ( base64_encode(base64_decode($str, true)) === $str){
         return true;
      } else {
         return false;
      }
    }

}
