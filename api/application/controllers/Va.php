<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

require "./../system/phpmailer/Exception.php";
require "./../system/phpmailer/PHPMailer.php";
require "./../system/phpmailer/SMTP.php";

use PhpMailer\PhpMailer\Exception;

use PhpMailer\PhpMailer\PHPMailer;
use Restserver\Libraries\REST_Controller;

class Va extends REST_Controller
{
    private $clientIDWAHANA = "11791";
    private $clientIDIJIN = "11794";
    private $clientWahanaIjin = "12280";
    private $clientSecret = "BCA01";
    private $Apikey = "BCA01";
    private $ApiSecret = "BCA01";
    private $clientHukum = "13379";
    private $invnumber = "00000";
    private $invitem = "0000";
    private $totaldue = "0000";
    private $tglbayar = "0000";
    private $vanumber = "0000";
    private $channeltype = "1604";
    private $invname = "0000";
    private $invpic = "0000";
    private $invmail = "0000";
    private $invphnum = "0000";
    private $invid = "0000";
    private $bankacc = "6560-907-778";

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Invoice_model', 'inv');
    }

    public function bills_post()
    {
        $token = explode(" ", $this->input->get_request_header('Authorization'));
        log_message('error', $this->input->get_request_header('Authorization'));

        if (sizeof($token) > 0) {
            if ($this->is_base64($token[sizeof($token) - 1])) {
                $tmp = explode("#", base64_decode($token[sizeof($token) - 1]));

                //print_r($token[sizeof($token) -1]);exit;
                if ($tmp[0] == $this->clientIDIJIN || $tmp[0] == $this->clientIDWAHANA || $tmp[0] == $this->clientWahanaIjin  || $tmp[0] == $this->clientHukum) {
                    if ($_SERVER["CONTENT_TYPE"] != "application/json") {
                        $this->response([
                            'error' => true,
                            'message' => "content type is not valid"
                        ], REST_Controller::HTTP_OK);
                        exit;
                    } else {
                        $post = json_decode(file_get_contents('php://input'));
                        $insert['code_log'] = "bills";
                        $insert['lognya'] = json_encode($post);
                        $this->inv->savelog($insert);

                        $InquiryReason = [
                            "Indonesian"  =>  "Sukses",
                            "English"  =>  "Success",
                        ];
                        $InquiryStatus = "00";
                        $CustomerName = "";
                        $TotalAmount = "";
                        $SubCompany = "";

                        if (empty($post->CustomerNumber)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CustomerNumber Kosong",
                                "English"  =>  "CustomerNumber Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->RequestID)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "RequestID Kosong",
                                "English"  =>  "RequestID Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->ChannelType)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "ChannelType Kosong",
                                "English"  =>  "ChannelType Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (!$this->validateDate($post->TransactionDate)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "TransactionDate Tidak Sesuai",
                                "English"  =>  "TransactionDate Not suitable",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->TransactionDate)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "TransactionDate Kosong",
                                "English"  =>  "TransactionDate Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if ($post->CompanyCode != $tmp[0]) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CompanyCode Tidak Sesuai",
                                "English"  =>  "CompanyCode Not suitable",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->CompanyCode)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CompanyCode Kosong",
                                "English"  =>  "CompanyCode Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (!empty($post->CompanyCode) && !empty($post->CustomerNumber)) {
                            $invoice = $this->inv->getInvoiceByID($post->CompanyCode . $post->CustomerNumber);
                            if ($invoice) {
                                if ($invoice->va_status != 1) {
                                    $CustomerName = $invoice->client_name;
                                    $TotalAmount = $invoice->total_due . ".00";
                                    $SubCompany = "00000";
                                } else {
                                    $InquiryReason = [
                                        "Indonesian"  =>  "Bill Sudah Selesai",
                                        "English"  =>  "Bill Already Done",
                                    ];
                                    $InquiryStatus = "01";
                                }
                            } else {
                                $InquiryReason = [
                                    "Indonesian"  =>  "Customer Tidak Ditemukan",
                                    "English"  =>  "Customer Not Found",
                                ];
                                $InquiryStatus = "01";
                            }
                        }
                        // if($post->CustomerNumber == "1907005")
                        $this->response([
                            'CompanyCode' => $post->CompanyCode,
                            'CustomerNumber' => $post->CustomerNumber,
                            'RequestID' => $post->RequestID,
                            'InquiryStatus' => $InquiryStatus,
                            'InquiryReason' => $InquiryReason,
                            'CustomerName'  => $CustomerName,
                            'CurrencyCode'  =>  "IDR",
                            'TotalAmount' =>  $TotalAmount,
                            'SubCompany'  =>  $SubCompany,
                            'DetailBills' => [],
                            'FreeTexts' =>  [],
                            'AdditionalData'  =>  ""
                        ], REST_Controller::HTTP_OK);
                        exit;
                    }
                } else {
                    $this->response([
                        'error' => true,
                        'message' => "ClientID is not valid"
                    ], REST_Controller::HTTP_OK);
                    exit;
                }
            } else {
                $this->response([
                    'error' => true,
                    'message' => "Base64 is not valid"
                ], REST_Controller::HTTP_OK);
                exit;
            }
        } else {
            $this->response([
                'error' => true,
                'message' => "Authorization is not valid"
            ], REST_Controller::HTTP_OK);
            exit;
        }
    }

    function is_base64($str)
    {
        if (base64_decode($str) !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function payments_post()
    {
        $token = explode(" ", $this->input->get_request_header('Authorization'));
        log_message('error', $this->input->get_request_header('Authorization'));
        if (sizeof($token) > 0) {
            if ($this->is_base64($token[sizeof($token) - 1])) {
                $tmp = explode("#", base64_decode($token[sizeof($token) - 1]));
                if ($tmp[0] == $this->clientIDIJIN || $tmp[0] == $this->clientIDWAHANA || $tmp[0] == $this->clientWahanaIjin  || $tmp[0] == $this->clientHukum) {
                    if ($_SERVER["CONTENT_TYPE"] != "application/json") {
                        $this->response([
                            'error' => true,
                            'message' => "content type is not valid"
                        ], REST_Controller::HTTP_OK);
                        exit;
                    } else {
                        $post = json_decode(file_get_contents('php://input'));
                        $insert['code_log'] = "payments";
                        $insert['lognya'] = json_encode($post);
                        $this->inv->savelog($insert);

                        $InquiryReason = [
                            "Indonesian"  =>  "Sukses",
                            "English"  =>  "Success",
                        ];
                        $InquiryStatus = "00";
                        $CustomerName = "";
                        $TotalAmount = "";
                        $SubCompany = "";
                        if (empty($post->CustomerNumber)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CustomerNumber Kosong",
                                "English"  =>  "CustomerNumber Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->RequestID)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "RequestID Kosong",
                                "English"  =>  "RequestID Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->ChannelType)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "ChannelType Kosong",
                                "English"  =>  "ChannelType Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->CustomerName)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CustomerName Kosong",
                                "English"  =>  "CustomerName Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->CurrencyCode)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CurrencyCode Kosong",
                                "English"  =>  "CurrencyCode Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        $tmpPaid = (float) $post->PaidAmount;
                        $tmptotal = (float) $post->TotalAmount;
                        if ($tmpPaid < $tmptotal) {
                            $InquiryReason = [
                                "Indonesian"  =>  "PaidAmount tidak boleh lebih kecil dari TotalAmount",
                                "English"  =>  "PaidAmount cannot be less than TotalAmount",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->PaidAmount)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "PaidAmount Kosong",
                                "English"  =>  "PaidAmount Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->TotalAmount)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "TotalAmount Kosong",
                                "English"  =>  "TotalAmount Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (!$this->validateDate($post->TransactionDate)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "TransactionDate Tidak Sesuai",
                                "English"  =>  "TransactionDate Not suitable",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->TransactionDate)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "TransactionDate Kosong",
                                "English"  =>  "TransactionDate Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (!$this->CheckValidFlagAdvice($post->FlagAdvice)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "FlagAdvice Tidak Sesuai",
                                "English"  =>  "FlagAdvice Not suitable",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->FlagAdvice)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "FlagAdvice Kosong",
                                "English"  =>  "FlagAdvice Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->Reference)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "Reference Kosong",
                                "English"  =>  "Reference Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->SubCompany)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "SubCompany Kosong",
                                "English"  =>  "SubCompany Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if ($post->CompanyCode != $tmp[0]) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CompanyCode Tidak Sesuai",
                                "English"  =>  "CompanyCode Not suitable",
                            ];
                            $InquiryStatus = "01";
                        }
                        if (empty($post->CompanyCode)) {
                            $InquiryReason = [
                                "Indonesian"  =>  "CompanyCode Kosong",
                                "English"  =>  "CompanyCode Empty",
                            ];
                            $InquiryStatus = "01";
                        }
                        if ((!empty($post->CompanyCode) && !empty($post->CustomerNumber)) && $InquiryStatus == "00") {
                            $invoice = $this->inv->getInvoiceByID($post->CompanyCode . $post->CustomerNumber);
                            if ($invoice) {
                                if ($post->TotalAmount != $invoice->total_due . ".00") {
                                    $InquiryReason = [
                                        "Indonesian"  =>  "TotalAmount Tidak Sesuai",
                                        "English"  =>  "TotalAmount Not suitable",
                                    ];
                                    $InquiryStatus = "01";
                                } else {
                                    if ($invoice->va_status != 1) {
                                        $CustomerName = $invoice->client_name;
                                        $TotalAmount = $invoice->total_due . ".00";
                                        $SubCompany = "00000";
                                        $dataUpdate['va_status'] = 1;
                                        $dataUpdate['tglbayar'] = $this->tanggalformat($post->TransactionDate);
                                        $dataUpdate['tipe'] = 'PAID';
                                        $dataUpdate['paid'] = 'Y';
                                        $dataUpdate['verified_by_id'] = 'Y';
                                        $dataUpdate['verified_by'] = 'System Virtual account BCA';
                                        $dataUpdate['verified_date'] = $this->tanggalformat($post->TransactionDate);
                                        $dataUpdate['total_payment'] = $invoice->total_due;
                                        $this->invnumber = $invoice->inv_num;
                                        $this->invitem = $this->inv->invItem($invoice->idinv);
                                        $this->totaldue = $invoice->total_due;
                                        $this->tglbayar = $this->tanggal($post->TransactionDate);
                                        $this->vanumber = $post->CompanyCode . $post->CustomerNumber;
                                        $this->channeltype = VACHANNEL[$post->ChannelType];
                                        $this->invname = $invoice->name;
                                        $this->invpic = $invoice->pic;
                                        $this->invmail = $invoice->email;
                                        $this->invphnum = $invoice->phone_number;
                                        $this->invid = $invoice->idinv;
                                        $this->bankacc = $invoice->bank_acc;

                                        $this->inv->updateInv($dataUpdate, $invoice->idinv);
                                        $this->createpdf();
                                    } else {
                                        $InquiryReason = [
                                            "Indonesian"  =>  "Bill Sudah Selesai",
                                            "English"  =>  "Bill Already Done",
                                        ];
                                        $InquiryStatus = "01";
                                    }
                                }
                            } else {
                                $InquiryReason = [
                                    "Indonesian"  =>  "Customer Tidak Ditemukan",
                                    "English"  =>  "Customer Not Found",
                                ];
                                $InquiryStatus = "01";
                            }
                        }
                        $this->response([
                            'CompanyCode' => $post->CompanyCode,
                            'CustomerNumber' => $post->CustomerNumber,
                            'RequestID' => $post->RequestID,
                            'PaymentFlagStatus' => $InquiryStatus,
                            'PaymentFlagReason' => $InquiryReason,
                            'CustomerName'  => $CustomerName,
                            'CurrencyCode'  =>  $post->CurrencyCode,
                            'PaidAmount'  =>  $post->PaidAmount,
                            'TotalAmount' =>  $post->TotalAmount,
                            'TransactionDate' =>  $post->TransactionDate,
                            'DetailBills' => [],
                            'FreeTexts' =>  [],
                            'AdditionalData'  =>  ""
                        ], REST_Controller::HTTP_OK);
                        exit;
                    }
                } else {
                    $this->response([
                        'error' => true,
                        'message' => "ClientID is not valid"
                    ], REST_Controller::HTTP_OK);
                    exit;
                }
            } else {
                $this->response([
                    'error' => true,
                    'message' => "Base64 is not valid"
                ], REST_Controller::HTTP_OK);
                exit;
            }
        } else {
            $this->response([
                'error' => true,
                'message' => "Authorization is not valid"
            ], REST_Controller::HTTP_OK);
            exit;
        }
    }


    public function validateDate($date)
    {
        $tmp1 = explode(" ", $date);
        if (sizeof($tmp1) != 2) {
            return false;
        } else {
            $tmpdate = explode("/", $tmp1[0]);
            $tmptime = explode(":", $tmp1[1]);
            if (sizeof($tmpdate) == 3 && sizeof($tmptime) == 3) {
                if ($this->validDate($tmpdate[2] . "-" . $tmpdate[1] . "-" . $tmpdate[0])) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        // print_r(sizeof($tmp1));
    }

    function validDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns true for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
    public function CheckValidFlagAdvice($flag)
    {
        if ($flag == "Y" || $flag == "N") {
            return true;
        } else {
            return false;
        }
    }





    function IDR($amount)
    {
        $angka_format = number_format($amount, 2, ",", ".");
        return $angka_format;
    }
    function tanggalformat($a)
    {
        $p1 = explode(" ", $a);
        $p2 = explode("/", $p1[0]);
        return $p2[2] . "-" . $p2[1] . "-" . $p2[0] . " " . $p1[1];
    }
    function tanggal($var = '')
    {
        $tgl = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $pecah1 = explode(" ", $var);
        $pecah = explode("/", $pecah1[0]);
        return $pecah[2] . " " . $tgl[$pecah[1] - 1] . " " . $pecah[0] . " " . $pecah1[1];
    }

    function sendmailnotif()
    {
        $Email_Subject = "URGENT  Payment verification INVOICE No. {$this->invnumber}";
        $Email_Body = "
      Dear Finance Team,
      <br><br>
      Mohon dapat dilakukan verifikasi pembayaran terhadap INVOICE No. {$this->invnumber}
      yang telah dibayarkan oleh klien (pembayaran dilakukan menggunakan virtual account) dengan detil sebagai berikut :
        <br><br>
        Nama Paket : {$this->invitem}<br>
        Nominal pembayaran : IDR " . $this->IDR($this->totaldue) . ",-<br>
        Vitual Account  : {$this->vanumber} <br>
        ChannelType  : {$this->channeltype} <br>
        Tanggal : {$this->tglbayar}<br>
        <br><br><br><br>
        Salam,
        <strong>IZIN.co.id</strong>
        <br><br>
        <hr>
        <em>This is an automated message please do not reply directly to this email/whatsapp. For further
        information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";


        $mail = new PHPMailer();
        try {
            $mail->IsSMTP();
            $mail->SMTPDebug = EMAIL_SMTPDEBUG;
            $mail->Host = EMAIL_HOST;
            $mail->SMTPAuth = EMAIL_SMTPAUTH;
            $mail->SMTPSecure = EMAIL_SMTPSECURE;
            $mail->Port = EMAIL_PORT;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->IsHTML(EMAIL_ISHTML);
            $mail->Username = EMAIL_USERNAME;
            $mail->Password = EMAIL_PASSWORD;
            $mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
            foreach (FIN_USR as $FinUsrEm => $FinUsrNm) {
                $mail->AddAddress($FinUsrEm, $FinUsrNm);
            }
            $ccemail = $this->inv->invCc();
            foreach ($ccemail as $cce => $ccn) {
                $mail->AddCC($ccn->uid, $ccn->nama_asli);
            }

            $mail->Subject = $Email_Subject;
            $mail->Body = $Email_Body;
            $mail->addAttachment("./../../../unggah/Invoices/{$this->invnumber}.pdf");
            if ($mail->send()) {
                echo 'Email sent successfully to ';
                log_message('error', 'Email sent successfully to ');
            } else {
                echo  'Email not sent!';
                log_message('error', 'Email not sent!');
            }
        } catch (Exception $e) {
            echo $e->errorMessage();
            log_message('error', $e->getMessage());
        } catch (Exception $e) {
            echo $e->getMessage();
            log_message('error', $e->getMessage());
        }

        unset($mail);
    }

    function createpdf()
    {
        $root_dir = './..';

        require_once $root_dir . "/vendor/autoload.php";

        require_once $root_dir . "/system/CrcConfig.php";

        require_once $root_dir . "/system/function.php";

        if (is_file($root_dir . "/Modules/CRM/file/InvoiceHTML.php")) {
            $invnum = $this->invnumber;
            $PayDtShow = $this->tglbayar;
            $ammdue = $this->totaldue;
            $invname = $this->invname;
            $invpic = $this->invpic;
            $invmail = $this->invmail;
            $invphnum = $this->invphnum;
            $citminv = $this->inv->itemitem($this->invid);
            foreach ($citminv as $key => $value) {
                ${"invitm0" . $value->no_urut} = $value->item;
                ${"invprc0" . $value->no_urut} = preg_replace("/\D/", "", $value->price);
                if (!empty(${"invprc0" . $value->no_urut})) {
                    ${"prc0" . $value->no_urut . "show"} = 'IDR ' . c_curr(${"invprc0" . $value->no_urut}, ",", ".", $min_sign = "mark") . ',-';
                } else {
                    ${"prc0" . $value->no_urut . "show"} = '';
                }
            }
            $BnkAcc = $this->bankacc;
            $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
            $BAD_NmBank = $BnkAccDet[0];
            $BAD_NmPT = $BnkAccDet[1];
            $BAD_NoRek = $BnkAccDet[2];
            $BAD_SwfCode = $BnkAccDet[3];
            $vaNumber = $this->vanumber;

            #Load Invoice HTML
            $judul_invoice = "Invoice";
            $InvMark = '<img src="' . convert_img_to_base64($root_dir . '/templates/Default/sysicons/Paid_i.png') . '">';

            require $root_dir . "/Modules/CRM/file/InvoiceHTML.php";
            try {
                $dompdf = new Dompdf\Dompdf();
                $dompdf->loadHtml($invhtml);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $pdfoutput = $dompdf->output();
                $dir = "./../../../unggah/Invoices";
                if (is_dir($dir)) {
                    file_put_contents($dir . "/" . $invnum . '.pdf', $pdfoutput);
                    $createpdf = "OK";
                    log_message('error', "ok");
                    // echo "ok";
                } else {
                    $createpdf = "SHUT";
                    log_message('error', "SHUT");
                }
            } catch (Exception $e) {
                // print_r($e);
                $createpdf = "SHUT";
                log_message('error', $e->errorMessage());
            }
        } else {
            $createpdf = "SHUT";
            log_message('error', "SHUT");
            // echo "g";
        }
        if ($createpdf == "OK") {
            // send email PAID

            /* -----------
								SEND EMAIL HERE
								-------------- */
            $Email_Subject = '[IZIN.CO.ID] Pembayaran Invoice Nomor ' . $invnum;
            $Email_Body = '
				Dear Client,
				<br><br>
				Terima kasih atas pembayaran yang telah dilakukan. Pembayaran telah kami terima dengan baik. Untuk selanjutnya kami akan segera memproses pendirian perusahaan/pembuatan perijinan perusahaan anda.
				<br><br>
				Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk anda.
				<br><br><br><br>
				Salam,
				<strong>IZIN.co.id</strong>
				<br><br>
				<hr>
				<em>This is an automated message please do not reply directly to this email/whatsapp. For further
				information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>
							';
            $mail = new PHPMailer();
            try {

                $mail->IsSMTP();
                $mail->SMTPDebug = EMAIL_SMTPDEBUG;
                $mail->Host = EMAIL_HOST;
                $mail->SMTPAuth = EMAIL_SMTPAUTH;
                $mail->SMTPSecure = EMAIL_SMTPSECURE;
                $mail->Port = EMAIL_PORT;
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
                $mail->isHTML(EMAIL_ISHTML);
                $mail->Username = EMAIL_USERNAME;
                $mail->Password = EMAIL_PASSWORD;
                $mail->setFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
                $mail->addAddress($invmail, $invpic);
                $mail->addCC(EMAIL_CC);
                $mail->addBCC("dev@curalib.com");
                $mail->Subject = $Email_Subject;
                $mail->Body = $Email_Body;
                //$mail->AltBody = $Email_Alt_Body;
                $mail->addAttachment("./../../../unggah/Invoices/{$this->invnumber}.pdf");
                if ($mail->send()) {
                    $MailSendStatus = 'Email sent successfully to ';
                    log_message('error', "Email sent successfully to");
                } else {
                    $MailSendStatus = 'Email not sent!';
                    log_message('error', "Email not sent!");
                }
            } catch (Exception $e) {
                $MailSendErr = $e->errorMessage();
                log_message('error', $e->errorMessage());
            } catch (Exception $e) {
                $MailSendErr = $e->getMessage();
                log_message('error', $e->errorMessage());
            }
            unset($mail);
        } else {
            $this->sendmailnotif();
        }
    }
}
