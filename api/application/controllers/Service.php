<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Service extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('api/Api_public_area_tv_model','publicTv');
        $this->load->model('api/api_tv_model','tv');
        $this->load->model('api/api_hotel_model','hotel');
        $this->load->model('api/api_room_model','room');
        $this->load->helper('firebase');
    }

    private function UpdateTimeTvAds($id_tv,$time){
        $data = array(
                "curent_time"   =>  $time
            );
        $this->tv->updateSetting($data,$id_tv);
    }
    public function servicePublicArea(){
        // $data = $this->publicTv->read();
        // $arrKeys = ["MIDDLEIKLAN","LEFTIKLAN","BOTTOMIKLAN"];
        // $res = [];
        // foreach($data as $key => $tv){
        //     $arr = [];
        //     foreach($arrKeys as $key => $arrKey){
        //         $tmp = $this->publicTv->GetAdsLooping($arrKey,$tv->id_public_tv,0);
        //         if($tmp){
        //             array_push($arr,$tmp[0]);
        //             $update = array(
        //                 "Active"    =>  1
        //                 );
        //             $this->publicTv->updateAds($update,$tmp[0]->Id_ads);
        //         }else{
        //             $tmpUpdates = $this->publicTv->GetAdsLooping($arrKey,$tv->id_public_tv,1);
        //             foreach($tmpUpdates as $key => $tmpUpdate){                    
        //                 $update = array(
        //                     "Active"    =>  0
        //                     );
        //                 $this->publicTv->updateAds($update,$tmpUpdate->Id_ads);
        //             }
        //         }
        //     }
            
        //     $arrPush =  array (
        //             "category"  => "tv",
        //             "type"  =>  "costumer_name",
        //             "value" =>  $arr
        //         );
        //     SendFirebaseFCM($tv->token,$arrPush);
        // }
        //iklan di saat nonton tv
        $tvLists = $this->hotel->getListHotel();
            $arrKeys = ["LEFTIKLAN","BOTTOMIKLAN"];
            foreach($tvLists as $key => $tvList){
                    $stringAdss = [];
                    foreach($arrKeys as $key => $arrKey){
                        $tmp = $this->tv->getSetting($arrKey,$tvList->Id_hotel);
                        if($tmp)
                        {
                            if($tmp->waktu != $tmp->curent_time){
                                $this->UpdateTimeTvAds($arrKey,$tmp->curent_time + 5000);
                            }else{
                                array_push($stringAdss,$tmp->Value);
                                $this->UpdateTimeTvAds($arrKey,0);
                            }
                        }else{
                                array_push($stringAdss,"kosong");
                        }
                    }
                    $str = implode("#",$stringAdss)."#60000";
                    $arr =  array (
                            "category"  => "tv",
                            "type"  =>  "iklan_tv",
                            "value" =>  $str
                        );
                    if($stringAdss > 0){
                        $data = $this->room->GetRoomByHotel($tvList->Id_hotel);
                         if($data){
                            foreach($data as $key => $item){
                                //echo $item->KeyFirebase."==============";
                                echo SendFirebaseFCM($item->KeyFirebase,$arr);
                            }
                         }
                    }
                    log_message('error',$str);
        }
    }



}
