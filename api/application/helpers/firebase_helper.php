<?php
/**
 * @author   Tedi Susanto
 */
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('SendFirebaseFCM'))
{
  function SendFirebaseFCM($token , $data){
            /**
                $data = $data/message
                
            **/
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array (
                    'to' => $token,
                    'notification' =>  array(
                        "body" => "great match!",
                        "title" => "Portugal vs. Denmark",
                        "content_available" => true,
                        "priority" => "high"
                        ),
                    'data' => $data
            );
            $fields = json_encode ($fields);

            $headers = array (
                    'Authorization: key=' . "AAAAs7iIafI:APA91bHq_NS4ktL02XrERn6GWPO_Axgk87gb_mpBhP7ymAyST1v2Z5D6QDCciHADd-VSGhF7gckVWGakf7bF-0z9YL_km7R5HbG1MmaiIj2qXDRgYUVUIdMmkYAP_C8vXODiMBmi7uZj",
                    'Content-Type: application/json'
            );

            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, $url );
            curl_setopt ( $ch, CURLOPT_POST, true );
            curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

            $result = curl_exec ( $ch );
            curl_close ( $ch );
            return $result;
    }
}


if(! function_exists('TopicFirebase')){
    function TopicFirebase($topic,$data){
        // API access key from Google API's Console
        $API_ACCESS_KEY = 'AAAAs7iIafI:APA91bHq_NS4ktL02XrERn6GWPO_Axgk87gb_mpBhP7ymAyST1v2Z5D6QDCciHADd-VSGhF7gckVWGakf7bF-0z9YL_km7R5HbG1MmaiIj2qXDRgYUVUIdMmkYAP_C8vXODiMBmi7uZj';
        $fields = array
            (
                'to'  => '/topics/'.$topic,
                'notification'          => array(
                        "body"  =>  "Ads Change",
                        "title" =>  "Ads Change",
                        "content_available" =>  true,
                        "priority"  =>  "high",
                        "timeToLive"    =>  10
                    ),
                'data'  =>  $data
            );
        
        $headers = array
        (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        log_message('error', "FIREBASE_".$result);
        echo $result;
        curl_close( $ch );
    }
}