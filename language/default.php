<?php
#----------------------------
# SYSTEM
# ---------------------------

$lang_0 = "Super User";
$lang_1 = "BOD";
$lang_2 = "Manager";
$lang_3 = "Administrator";
$lang_4 = "Enter";
$lang_5 = "Try Again";
$lang_6 = "User ID";
$lang_7 = "Password";
$lang_8 = "Code";
$lang_9 = "Serial Number";
$lang_10 = "Key";
$lang_11 = "Install";
$lang_12 = "Update";
$lang_13 = "Delete Item";
$lang_14 = "Batal";
$lang_15 = "Tutup";
$lang_16 = "Hasil";
$lang_17 = "Edit Item";
$lang_19 = "Online";
$lang_20 = "Disconnect";
$lang_21 = "Blokir";
$lang_22 = "Buka Blokir";
$lang_23 = "Halaman Login";
$lang_24 = "Reset";
$lang_18 = "Nama";
$lang_30 = "Email Address";
$lang_31 = "Login";
$lang_32 = "Enter";
$lang_33 = "Finance";
$lang_97 = "Akses tidak diijinkan!";
$lang_98 = "Halaman Tidak Ditemukan!";
$lang_99 = "Main Menu";

$lang_100 = "Account";
$lang_200 = "Tambah Akun";
$lang_201 = "Save New User";
$lang_202 = "Form incompleted.";
$lang_203 = "Password did not match.";
$lang_204 = "New user created successfully!.";
$lang_205 = "User ID sudah terpakai.";
$lang_206 = "Tingkat";
$lang_207 = "Ulangi";
$lang_208 = "Password Lama";
$lang_209 = "Password Baru";
$lang_210 = "User ID terlalu pendek.";
$lang_211 = "Password terlalu pendek.";
$lang_212 = "Password successfully changed!";
$lang_213 = "Akun berhasil di-update!";
$lang_214 = "Invalid Email Addres";
$lang_215 = "Email address already taken";
$lang_216 = "You can not create this kind of account";
$lang_217 = "Save Data";
$lang_218 = "Selected item(s) will be deleted. Continue?";
$lang_219 = "Item successfully deleted!";
$lang_220 = "Please select at least one item to continue.";
$lang_221 = "System";
$lang_222 = "Invalid Phone Number";

$lang_300 = "Daftar Akun";
$lang_301 = "Daftar Akun";
$lang_302 = "Edit Account";
$lang_303 = "Akun akan dihapus. Lanjutkan?";
$lang_304 = "Akun berhasil dihapus";
$lang_305 = "Hapus Akun Gagal!";
$lang_306 = "Akun berhasil diputuskan!";
$lang_307 = "Akun gagal diputuskan!";
$lang_308 = "Akun berhasil diblokir!";
$lang_309 = "Akun gagal diblokir!";
$lang_310 = "Buka blokir berhasil!";
$lang_311 = "Buka blokir gagal!";
$lang_400 = "My Profile";
$lang_401 = "Akun Saya";
$lang_402 = "Update Akun Saya";
$lang_403 = "Reset Password Saya";
$lang_404 = "Save Change";
$lang_405 = "Cancel";
$lang_406 = "Account successfully changed!";

$lang_500 = "Menu Access Rights";

$lang_600 = "Untuk menambahkan akun baru, pilih <strong>" . $lang_500 . "</strong> yang akan dimiliki oleh akun tersebut.<br>Bila dalam " . $lang_500 . " sudah ada tanda <strong>'check'</strong> maka artinya menu tersebut <strong>wajib</strong> untuk dimiliki oleh setiap akun.";
$lang_601 = "<ul>
				<li>Bila <strong>TIDAK INGIN MENGGANTI</strong> password, <strong>KOSONGKAN</strong> kolom isian password dan ulangi.</li>
				<li>UserID tidak bisa diganti.</li>
				<li>Lakukan perubahan sesuai dengan kebutuhan dan klik tombol Simpan Perubahan untuk menyimpan.</li>
				<li>Untuk kembali ke daftar akun, klik tombol batal. Tombol batal ini tidak melakukan proses penyimpanan, oleh karena itu pastikan untuk selalu menyimpan perubahan sebelum kembali ke halaman daftar akun.</li>
			 </ul>";

$lang_818 = "Attachment";
$lang_805 = "Add";

$lang_995 = "Edit";
$lang_996 = "Add";
$lang_997 = "Listing";
$lang_998 = "Are you sure to Logout from the system?";
$lang_999 = "Logout";


$InstallErrs = array(
	"INS_SN_FAILED" => "General Error!",
	"INS_WRG_SN" => "SN/Key tidak valid",
	"INS_SN_OCCUPIED" => "SN/Key tidak valid",
	"INS_GO_AHEAD_BROW" => "Instalasi Selesai"
);

$InstallErrsNote = array(
	"INS_SN_FAILED" => "Mohon untuk mengubungi teknisi kami.",
	"INS_WRG_SN" => "Mohon untuk mengulangi mengisi SN dan Key kembali.",
	"INS_SN_OCCUPIED" => "Mohon untuk menghubungi teknisi kami.",
	"INS_GO_AHEAD_BROW" => "Klik untuk menampilkan halaman login."
);

$LoginErrsNote = array(
	"WRONG_DOM" => "Domain tidak valid.",
	"WRONG_CAPT" => "Captcha tidak valid.",
	"CAPT_ERROR" => "Sistem captcha gagal!",
	"WRONG_ENV" => "Kesalahan Environment!",
	"NO_CREDS" => "Masukkan User ID dan Password.",
	"ACC_BAN" => "Akun anda sedang di-blok.",
	"ACC_LOGOUT_LOCK" => "Anda tidak logout pada sesi terakhir. Mohon untuk menunggu selama: ",
	"BAD_LGN" => "Login salah.",
	"ACC_LOCK" => "Brute Force Terdeteksi! Mohon untuk menunggu selama: ",
	"IP_LOCK" => "Brute Force!",
	"WRG_SESS_UID" => "Login tidak valid!",
	"WRG_SESS_IP" => "IP Berganti! Login tidak valid!",
	"WRG_SESS_ID" => "Login diputuskan!",
	"WRG_TIME_OUT" => "Time out. Tidak ada aktifitas selama " . IDLE . " detik",
	"WRONG_CAPT" => "Kode captcha salah",
	"HTML_HIJACKING" => "What are you trying to do?",
	"IL_COPY" => "Kesalahan Registrasi",
	"WRG_OS" => "Kesalahan Sistem Operasi"

);

$LoginErrsPesan = array(
	"PS_AJAX_FS_NOT_FOUND" => "File tidak ditemukan",
	"PS_AJAX_CON_ERR" => "Session Invalid",
	"PS_WRONG_ENV" => "Please check your configuration.",
	"PS_FR_INS" => "Instalasi Aplikasi. Pastikan server telah terkoneksi dengan internet.",
	"PS_WRG_OS" => "Aplikasi hanya mendukung sistem operasi windows",
	"PS_IL_COPY" => "Mohon untuk menginstall ulang dan melakukan registrasi aplikasi"
);

$FileManPesan = array(
	"FL_IM_SIZE_ERROR" => "Incorrect image size setting",
	"FL_IM_SUCCESS" => "Image uploaded",
	"FL_UPLOAD_SUCCESS" => "File uploaded",
	"FL_UPLOAD_FAILED" => "Upload file failed",
	"FL_MAXSIZE_EXCEED" => "Filesize exceed maximum limit size",
	"FL_PROCESS_ERRS" => "Incorrect in choosing file process",
	"FL_TYPE_ERRS" => "File type not allowed",
	"FL_FILE_NF" => "File not found",
	"FL_FUNC_NF" => "Missing PHP file info function",
	"FL_DIR_NF" => "Missing upload directory"

);

# Error MySQL
define("ERR_1", "Koneksi database gagal!");
define("ERR_2", "Kesalahan query!");
define("ERR_3", "Tipe koneksi tidak disupport!");
define("ERR_4", "Kesalahan tipe query!");

# Months
$list_month = array(
	"01" => "January", "02" => "February", "03" => "March", "04" => "April", "05" => "May", "06" => "June", "07" => "July",
	"08" => "August", "09" => "September", "10" => "October", "11" => "November", "12" => "December"
);

$list_month_short = array(
	"01" => "Jan", "02" => "Feb", "03" => "Mar", "04" => "Apr", "05" => "Mei", "06" => "Jun", "07" => "Jul",
	"08" => "Agu", "09" => "Sep", "10" => "Okt", "11" => "Nov", "12" => "Des"
);

# ---------------------------


#----------------------------
# MODULES
# ---------------------------

/* ----------
DASHBOARD
------------- */
$lang_1000 = "Dashboard";


/*----
DARWIS
----- */
$lang_1500 = "Tracking";
$lang_1510 = "Data Tracking";
$lang_1520 = "Finished Permit";

//$lang_1540 = "Track Activity";
$lang_1540 = "Permit Today";

/*------------
CRM
-------------- */
$lang_2000 = "CRM";
$lang_2001 = "Database";
$lang_2002 = "Leads";
$lang_2003 = "Activity";
$lang_2004 = "Client";
$lang_2005 = "Search Exisiting Client";
$lang_2006 = "Client Full Name";
$lang_2007 = "Birth Date";
$lang_2008 = "Nomor Whatsapp";
$lang_2009 = "Client ID";
$lang_2010 = "Email";
$lang_2011 = "Source";
$lang_2012 = "Use This Client";
$lang_2013 = "Add New Client";
$lang_2014 = "Product Interested";
$lang_2015 = "Prefered Location";
$lang_2016 = "Client Not Found! | Please select one first.";
$lang_2017 = "Suggested Company Name";
$lang_2018 = "Company Major Fuction";
$lang_2019 = "Company Address";
$lang_2020 = "KBLI";
$lang_2021 = "Province";
$lang_2022 = "City";
$lang_2023 = "Main KBLI";
$lang_2024 = "Company Size";
$lang_2025 = "Company Mail";
$lang_2026 = "Note & Special Instructions";
$lang_2027 = "Incomplete Form";
$lang_2028 = "Add Lead Failed!";
$lang_2029 = "Lead added successfully";
$lang_2030 = "Search";
$lang_2031 = "Daterange";
$lang_2032 = "Export to Excel";
$lang_2033 = "Update Status";
$lang_2034 = "Note / Reason";
$lang_2035 = "Back to Listing";
$lang_2036 = "Lead status updated!";
$lang_2037 = "Edit";
$lang_2038 = "Update Status";
$lang_2039 = "File Title";
$lang_2040 = "File Description";
$lang_2041 = "File";
$lang_2042 = "Upload File";
$lang_2043 = "Update Data";
$lang_2044 = "Lead updated";
$lang_2045 = "Update Lead failed. ";
$lang_2046 = "Upload Failed ";
$lang_2047 = "Client Password";
$lang_2048 = "Save New Lead";
$lang_2049 = "Discard and Choose another client";
$lang_2050 = "Discard and Choose client";
$lang_2051 = "Save Client Detail";
$lang_2052 = "Save Sales Detail";
$lang_2053 = "Save Company Detail";
$lang_2054 = "Chairman Full Name";
$lang_2055 = "Position";
$lang_2056 = "Save BOC & BOD";
$lang_2057 = "Client";
$lang_2058 = "Listing";
$lang_2059 = "Lead Detail";
$lang_2060 = "Locale";
$lang_2061 = "List Renewal";
$lang_2062 = "Save Sales Renewal Detail";

/* --------
TRANSACTION
----------- */
$lang_3000 = "Transaction";
$lang_3001 = "Invoices";
$lang_3002 = "Summary";
$lang_3003 = "Agreement";
$lang_3004 = "Save Agreement";
$lang_3005 = "Send Agreement";
$lang_3006 = "Sign Agreement";
$lang_3007 = "Incomplete Form";
$lang_3008 = "Add Agreement Failed!";
$lang_3009 = "Agreement added successfully and has been sent.";
$lang_3010 = "Agreement already exists.";
$lang_3011 = "Cancel Agreement";
$lang_3012 = "Agreement added successfully but failed to send email notification.";
$lang_3013 = "Agreement correction has reached the limit.";
$lang_3014 = "Resend Agreement";
$lang_3015 = "Resend agreement success.";
$lang_3016 = "Resend agreement failed.";
$lang_3017 = "The agreement was not found.";
$lang_3018 = "The agreement file has expired. Please cancel and reissue.";
$lang_3019 = "Verify Agreement";
$lang_3020 = "Manual upload agreement?";
$lang_3021 = "Do you want to save the Agreement?";
$lang_3022 = "Do you want to cancel the Agreement?";
$lang_3023 = "Are you sure you want to sign right now?";
$lang_3024 = "Sent to Me";
$lang_3025 = "This agreement need correction!";

/* ---------
TRACKING
------------ */
//$lang_4000 = "Tracking";
//$lang_4001 = "Data Tracking";
$lang_50010 = "Tracking";
$lang_50011 = "<strong>Tracking</strong>  Administrasi servis dan administrasi pembayaran servis. Termasuk penaganan komplain.";
//$lang_50100 = "Data Tracking";
$lang_50100 = "Tracking List";
$lang_50101 = "Tracking Data";
$lang_50109 = "Add Tracking";
$lang_50110 = "New Tracking";
$lang_50111 = "Search ID";
$lang_50113 = "Track Status";
$lang_50121 = "Close";
$lang_50122 = "Save changes";
$lang_50125 = "New tracking inserted";
$lang_50126 = "Add canceled, lead exists in tracking ";

$lang_50130 = "Tracking Item";
$lang_50132 = "Item Update";
$lang_50133 = "Stakeholder";
$lang_50134 = "Update";
//$lang_50135 = "Update canceled, please choose an item and up";
$lang_50135 = "Update canceled, make sure you choose an item and fill in update note";
$lang_50136 = "Tracking updated";
$lang_50137 = "Update canceled, item status can not be changed it is done";

$lang_50140 = "Timeline";
//$lang_50101 = "Closing";
$lang_50150 = "Elapsed";


$lang_50201 = "Tracking ID";
$lang_50202 = "Client Name";
$lang_50203 = "Product Name";
$lang_50204 = "Company Name";
$lang_50300 = "Finished Permit";
//$lang_50400 = "Track Activity";
$lang_50400 = "Permit Today";

$lang_51000 = "Notification";

/* -------
REPORT
---------- */
$lang_5000 = "Report";
$lang_5001 = "Leads";
$lang_5002 = "Invoice";
$lang_5003 = "Tracking";



/* ---------
SETTING
------------ */
$lang_6000 = "Setting";
$lang_6001 = "Packages";
$lang_6002 = "KBLI";
$lang_6003 = "Virtual Office Location";
$lang_6004 = "User";
$lang_6005 = "Invoices";
$lang_6006 = "Holiday";
$lang_6010 = "Work Item";
$lang_6011 = "Package";
$lang_6012 = "Pricing";
$lang_6013 = "Code";
$lang_6014 = "Name";
$lang_6015 = "Description";
$lang_6016 = "Duration";
$lang_6017 = "CountDown";
$lang_6018 = "Steps";
$lang_6019 = "Paralel";
$lang_6020 = "Adding Working Item Failed!|Form was incompleted!";
$lang_6021 = "Adding Working Item Succeeded!";
$lang_6022 = "Adding Working Item Failed!|Working Item Code already taken!";
$lang_6023 = "Sequence";
$lang_6024 = "Paralel";

$lang_6025 = "Code";
$lang_6026 = "Name";
$lang_6027 = "Description";
$lang_6028 = "Adding Package Failed!|Form was incompleted!";
$lang_6029 = "Adding Package Failed!|Package Code already taken!";
$lang_6030 = "Adding Package Succeeded!";
$lang_6031 = "Time Est.";

$lang_6032 = "Full Name";
$lang_6033 = "Email Address";
$lang_6034 = "Position";
$lang_6035 = "Company";
$lang_6036 = "Password";
$lang_6037 = "Retype Password";
$lang_6038 = "Sales";
$lang_6039 = "Last Login";
$lang_6040 = "Avatar";
$lang_6041 = "Avatar Uploaded";

$lang_6042 = "Virtual Office Location";
$lang_6043 = "Add New Data";
$lang_6044 = "Published";
$lang_6045 = "Change Published Status";

$lang_6046 = "KBLI Number";
$lang_6047 = "Title";
$lang_6048 = "Description";
$lang_6049 = "Create Json File";
$lang_6050 = "VO Successfully added";
$lang_6051 = "VO status changed";

$lang_6052 = "Fixed Holiday";
$lang_6053 = "Not Fixed Holiday";
$lang_6054 = "Holiday Date";
$lang_6055 = "Description";
$lang_6056 = "Month";
$lang_6057 = "Date";
$lang_6058 = "Add Holiday Failed!. Incomplete form";
$lang_6059 = "Holiday Added!";


$lang_11000 = "Notification";
