<?php
/*
====================================================================================
DATABASE CONNECTION
You can use this class as you wish and share as you wish
under GNU General Public License (GPL).
Please keep this author intact.

author: Barep (ggembulls@gmail.com)

* Input Parameters(in sequences):
  + MySQL Host
  + MySQL User
  + MySQL Password
  + MySQL DB Name
  + MySQL Port
  + Engine Type
  Eg: $db_conn = new dbc("Host","User","Password","DB Name","Port","PDO");

* Engine Type Support:
  + MySQLi
  + PDO (recomended)

* 2 types of query,
  + Basic => Old fashioned query.
    Eg : $db_conn->getQuery($sql_statement);
  + Exec => Using prepared statement.
    Eg: $db_conn->getQuery($sql_statement,"Exec",$params);

* Output types
  + Multi rows:
  	+ Do a query:
  		- With prepare statement: $exec = $db_conn->getQuery($sql_statement,"Exec",$params);
		- Old fashioned query: $exec = $dbs->getQuery($sql_statement);
  	+ Do Output:
  		- Return all result in associative array. Loop with foreach function (getAll):
		  foreach( $xx = $db_conn->getAll($exec) as $yy ){ ... }
  		- Return all result in associative array. Loop with while function (getAssoc):
    	  while( $yy = $db_conn->getAssoc($exec) ){ ... }
  + Single row:
  	+ Do Output:
		- With prepare statement: $db_conn->getArr($sql_statement,$params);
		- Old fashioned query: $db_conn->getArr($sql_staement);

* Use Constants:
  Use this to generating custom error messages instead true error messages.
  + ENVIRONMENT -> Predefined application environtment
  + ERR_1 -> Database connection error.
  + ERR_2 -> Query error.
  + ERR_3 -> Database engine error.
  + ERR_4 -> Query type error.
====================================================================================
*/

//function connection ()
//function getQuery($dbc_query,$tipeQ="Basic",$pbind="")
//function getAll($resource_sql)
//function getAssoc($resource_sql)
//function getArr($resource_sql,$pbind=array())
//function connection ()


class dbc
{
    var $dbc_mhost;
    var $dbc_muser;
    var $dbc_mpwd;
    var $dbc_msdb;
    var $dbc_msport;
    var $dbc_mstype;

    //	function __construct($dbc_mhost="127.0.0.1",$dbc_muser="drwizz",$dbc_mpwd="pim",$dbc_msdb="",$dbc_msport=3306,$dbc_mstype="PDO") {
    function __construct($dbc_mhost = "127.0.0.1", $dbc_muser = "root", $dbc_mpwd = "", $dbc_msdb = "", $dbc_msport = 3306, $dbc_mstype = "PDO")
    {
        $this->dbc_mhost = $dbc_mhost;
        $this->dbc_muser = $dbc_muser;
        $this->dbc_mpwd = $dbc_mpwd;
        $this->dbc_msdb = $dbc_msdb;
        $this->dbc_msport = $dbc_msport;
        $this->dbc_mstype = $dbc_mstype;
    }

    function connection()
    {
        if ($this->dbc_mstype == "MySQLi") {
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            try {
                $this->dbh = new mysqli($this->dbc_mhost, $this->dbc_muser, $this->dbc_mpwd, $this->dbc_msdb, $this->dbc_msport);
            } catch (Exception $e) {
                if (strtolower(ENVIRONMENT) == "development") {
                    return $e->getMessage();
                } else {
                    return ERR_1;
                }
            }
        } elseif ($this->dbc_mstype == "PDO") {
            try {
                $this->dbh = new PDO(
                    'mysql:host=' . $this->dbc_mhost . ';dbname=' . $this->dbc_msdb . ';port=' . $this->dbc_msport . '',
                    $this->dbc_muser,
                    $this->dbc_mpwd,
                    array(
                        PDO::ATTR_EMULATE_PREPARES => false,
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    )
                );
            } catch (PDOException $ex) {
                if (strtolower(ENVIRONMENT) == "development") {
                    return $ex->getMessage();
                } else {
                    return ERR_1;
                }
            }
        } else {
            return ERR_3;
        }
    }

    function getQuery($dbc_query, $tipeQ = "Basic", $pbind = [])
    {
        /* MySQLi */
        if ($this->dbc_mstype == "MySQLi") {
            try {
                if ($tipeQ == "Basic") {
                    return $Hsl = $this->dbh->query($dbc_query);
                } elseif ($tipeQ == "Exec") {
                    $this->Prep = $this->dbh->prepare($dbc_query);
                    if ($pbind) {
                        $prtype = '';
                        $Params = array();
                        foreach ($pbind as $itm) {
                            list($ky, $vl) = explode("|", $itm);
                            $prtype .= $ky;
                            array_push($Params, $vl);
                        }
                        array_unshift($Params, $prtype);
                        $tmp = array();
                        foreach ($Params as $pkey => $pvalue) $tmp[$pkey] = &$Params[$pkey];
                        call_user_func_array(array($this->Prep, 'bind_param'), $Params);
                    }
                    $this->Prep->execute();
                    return $Hsl = $this->Prep->get_result();
                } else {
                    echo ERR_4;
                    exit(0);
                }
            } catch (Exception $ex) {
                $this->SQL_Error = "Error querying SQL: " . mysqli_error($this->dbh);
                if (strtolower(ENVIRONMENT) == "development") {
                    echo "Error querying SQL: " . mysqli_error($this->dbh);
                } else {
                    // echo ERR_2;
                    echo "Error querying SQL: " . $ex->getMessage();
                }
                exit(0);
            }
            /* --- */

            /* PDO */
        } elseif ($this->dbc_mstype == "PDO") {
            try {
                if ($tipeQ == "Basic") {
                    return $Hsl = $this->dbh->query($dbc_query);
                } elseif ($tipeQ == "Exec") {
                    $pcc = 0;
                    $this->Prep = $this->dbh->prepare($dbc_query);
                    // echo json_encode($dbc_query) . PHP_EOL . PHP_EOL;
                    foreach ($pbind as $itm) {
                        list($ky, $vl) = explode("|", $itm);
                        $pcc++;
                        switch ($ky) {
                            case "n":
                                $this->Prep->bindValue($pcc, null, PDO::PARAM_NULL);
                                break;
                            case "s":
                                $this->Prep->bindValue($pcc, $vl, PDO::PARAM_STR);
                                break;
                            case "i":
                                $this->Prep->bindValue($pcc, $vl, PDO::PARAM_INT);
                                break;
                            case "d":
                                $this->Prep->bindValue($pcc, $vl);
                                break;
                        }
                    }

                    $this->Prep->execute();
                    return $this->Prep;
                } else {
                    echo ERR_4;
                    exit(0);
                }
            } catch (PDOException $ex) {
                $this->SQL_Error = "Error querying SQL: " . $ex->getMessage();
                if (strtolower(ENVIRONMENT) == "development") {
                    echo "Error querying SQL: " . $ex->getMessage();
                } else {
                    // echo ERR_2;

                    echo "Error querying SQL: " . $ex->getMessage();
                }
                exit(0);
            }
        }
        /* --- */
    }


    function getAll($resource_sql)
    {
        if ($this->dbc_mstype == "MySQLi") {
            return $resource_sql->fetch_all(MYSQLI_ASSOC);
        }
        if ($this->dbc_mstype == "PDO") {
            return $resource_sql->fetchAll(PDO::FETCH_ASSOC);
        }
    }

    function getAssoc($resource_sql)
    {
        if ($this->dbc_mstype == "MySQLi") {
            return $resource_sql->fetch_assoc();
        }
        if ($this->dbc_mstype == "PDO") {
            return $resource_sql->fetch(PDO::FETCH_ASSOC);
        }
    }

    function getArr($resource_sql, $pbind = array())
    {
        $ResultArr = $this->getQuery($resource_sql, "Exec", $pbind);

        if ($this->dbc_mstype == "MySQLi") {
            return $ResultArr->fetch_assoc();
        }
        if ($this->dbc_mstype == "PDO") {
            return $this->Prep->fetch(PDO::FETCH_ASSOC);
        }
    }
}

/*
Intiating Class
Change as you need
*/

$dbs = new dbc(DBC_MHOST, DBC_MUSER, DBC_MPWD, DBC_MSDB, DBC_MSPORT, DBC_MSTYPE);
$cons = $dbs->connection();

/*
Get and show error generated
*/

if ($cons) {
    echo $cons;
    exit(0);
}
