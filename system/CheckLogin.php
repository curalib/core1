<?php
# Check IP Brute Force
$chkBruteForceIP = "select id from " . $tblp . "sys_ip_ban_list where ip = '" . $ip_addr . "'";
$rchkBruteForceIP = $dbs->getArr($chkBruteForceIP);
if ($rchkBruteForceIP['id']) {
    if (IP_BLOCK === true) {
        # Check White List
        if (IP_WHITE === true) {
            $ChWhtLst = "select id from " . $tblp . "sys_ip_white_list where ip_addr = '" . $ip_addr . "'";
            $rChWhtLst = $dbs->getArr($ChWhtLst);
            if (empty($rChWhtLst['id'])) {
                $LoginErrs = "IP_LOCK";
            }
        } else {
            $LoginErrs = "IP_LOCK";
        }
    }
}

# Check Domain
$PassUrl = CheckUrl(USE_DOMAIN_CHECK, $dom_array, "");
if ($PassUrl === false) {
    $LoginErrs = "HTML_HIJACKING";
    $alasan = "HTML hijacking attempt detected.";
    $inBan = "insert into " . $tblp . "sys_ip_ban_list set
              id = '" . uniqid(md5(microtime())) . "',
              ip = '" . $ip_addr . "',
              tgl = now(),
              adding_by = 'SYSTEM',
              alasan = '" . $alasan . "'";
    $dbs->getQuery($inBan);
}

if (isset($_SESSION['loginID'])) {
    #Check
    $inC = "select id, userid, ip_addr, lact_action from " . $tblp . "sys_login where login_id = '" . $_SESSION['loginID'] . "'";
    $rinC = $dbs->getArr($inC);
    if ($_SESSION["loginUser"] != $rinC['userid']) {
        $LoginErrs = "WRG_SESS_UID";
    }
    if (!$LoginErrs) {
        if (IP_BASED === true) {
            if ($ip_addr != $rinC['ip_addr']) {
                $LoginErrs = "WRG_SESS_IP";
            }
        }
    }
    if (!$LoginErrs) {
        if (SESS_TIME_OUT === true) {
            if (CheckIdle($rinC['lact_action'], IDLE) === false) {
                $LoginErrs = "WRG_TIME_OUT";
            }
        }
    }
} else {
    $LoginErrs = "WRG_SESS_ID";
}

if (!$LoginErrs) {
    #Renew Confidential
    $cLgnDet = "select priv, nama_asli, jabatan, avatar from " . $tblp . "sys_lgndetail where id_login = '" . $rinC['id'] . "'";
    $rcLgnDet = $dbs->getArr($cLgnDet);
    $_SESSION["lgnPriv"] = $rcLgnDet['priv'];
    $_SESSION["lgnNama"] = $rcLgnDet['nama_asli'];
    $_SESSION["Jbtn"] = $rcLgnDet['jabatan'];
    $_SESSION["Avatar"] = $rcLgnDet['avatar'];

    $x = (isset($_GET['x']) ? Purefy($_GET['x']) : null);
    $y = (isset($_GET['y']) ? Purefy($_GET['y']) : null);
    $z = (isset($_GET['z']) ? Purefy($_GET['z']) : null);

    if (BASEURL == "http://core1.test/") {
        list($x, $y, $z) = explode('/', str_replace('.html', '', trim($_SERVER['REQUEST_URI'], '/')));
    }

    if ($x == 'pdf') {
        require 'pdf.php';
        die;
    }

    if ($x == 'download') {
        $cFd = "SELECT trkfile_name_ori, trkfile_name_uniq FROM " . $tblp . "apps_trkfile WHERE id_trkfile = '" . $z . "'";
        $rcFd = $dbs->getArr($cFd);

        if (!$rcFd) {
            echo 'Not Found!';
            die;
        }

        $filename = "../../" . UPDIR . "/{$y}/" . $rcFd['trkfile_name_uniq'];
        if (!empty($filename) && file_exists($filename)) {
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($filename));
            header('Content-Disposition: attachment; filename="' . $rcFd['trkfile_name_ori'] . '"');
            header('Content-Transfer-Encoding: binary');

            $file = @fopen($filename, "rb");
            if ($file) {
                while (!feof($file)) {
                    print(fread($file, 1024 * 8));
                    flush();
                    if (connection_status() != 0) {
                        @fclose($file);
                        die();
                    }
                }
                @fclose($file);
            }
        } else {
            echo 'Not Found!';
            die;
        }
    }

    $atu_idit = false;
    if (($x != $_SESSION["xx"]) && $x != '') {
        $_SESSION["xx"] = $x;
        $atu_idit = true;
    }

    if (empty($x)) {
        $cmodule = "Dashboard";
        $cmod = "DBoard_i.php";

        require "system/HitDaysClass.php";

        require "Modules/" . $cmodule . "/mod/" . $cmod;
        $cpage = "Dashboard.php";
    } else {
        $Keys = ExpandKeys($x);
        if ($Keys == 0 or empty($Keys)) {
            $cmodule = "System";
            $cmod = "";
            $cpage = "PageNotFound.php";
            $_SESSION["MM_ID"] = "";
        } else {
            $cmdet = "select id, id_mainmenu, id_parent, tipe_menu, tipe_konten, module, page, mods, nama, subnama, icon, navbar, tingkat, tgl from " . $tblp . "sys_menu where tgl = ? and publish = 'Y'";
            $bind = array("s|" . $Keys);
            $rcmdet = $dbs->getArr($cmdet, $bind);

            if ($_SESSION["lgnPriv"] === 0) {
                $rcAccRght['id'] = "OK";
            } else {
                $cAccRght = "select id from " . $tblp . "sys_menuaccess where id_logintbl = '" . $_SESSION["tblID"] . "' and menus_id = '" . $rcmdet['id'] . "'";
                $rcAccRght = $dbs->getArr($cAccRght);
            }

            if (empty($rcAccRght['id'])) {
                $cmodule = "System";
                $cmod = "";
                $cpage = "NotAuthorized.php";
            } else {
                $cpage = $rcmdet['page'] . ".php";
                $cmod = $rcmdet['mods'] . ".php";
                $cmodule = $rcmdet['module'];

                if (!is_file("Modules/" . $cmodule . "/mod/" . $cmod) or $rcmdet['mods'] == '') {
                    $cmodule = "System";
                    $cpage = "PageNotFound.php";
                    $cmod = "";
                } else {
                    if (!isset($_SESSION["butts_Update"])) {
                        $_SESSION["butts_Update"] = md5(uniqids(microtime()));
                    }
                    if (!isset($_SESSION["butts_Enter"])) {
                        $_SESSION["butts_Enter"] = md5(uniqids(microtime()));
                    }
                    require "system/FileManClass.php";
                    require "system/HitDaysClass.php";
                    require "system/PagingClass2.php";
                    require "Modules/" . $cmodule . "/mod/" . $cmod;
                }
            }
        }
    }

    if ($atu_idit == true) {
        unset($_SESSION["Edit_ID"]); # Session edit account.
        foreach ($_SESSION as $ses_keys => $ses_vals) {
            if (strpos($ses_keys, 'currpage') !== false) {
                unset($_SESSION[$ses_keys]);
            }
        }
        unset($_SESSION["LeadSearch"]);
        unset($_SESSION["LeadSearchSales"]);
        unset($_SESSION["LeadSearchDateRange"]);
        unset($_SESSION["LeadProdSales"]);
        unset($_SESSION["LeadSearchStatus"]);
        unset($_SESSION["InvSearch"]);
        unset($_SESSION["InvSearchIssuedDateRange"]);
        unset($_SESSION["InvSearchDueDateRange"]);
        unset($_SESSION["InvSearchVerifiedDateRange"]);
        unset($_SESSION["InvSearchStatus"]);
        unset($_SESSION["TrackingSearch"]);

        unset($_SESSION["AddItm"]);
        unset($_SESSION["ClietType"]);
        unset($_SESSION["LeadDets"]);
        unset($_SESSION["LeadChStat"]);
        unset($_SESSION["FixHoliday"]);
        unset($_SESSION["Curr_Year"]);
        unset($_SESSION["InvSearch"]);
        unset($_SESSION["InvDtFr"]);
        unset($_SESSION["InvDtTo"]);
    }

    # Update Action
    $upLastAct = "update " . $tblp . "sys_login set lact_action = '" . microtime_float() . "' where login_id = '" . $_SESSION['loginID'] . "'";
    $dbs->getQuery($upLastAct);

    # Button Keys
    if (!isset($_SESSION["butts_Add"])) {
        $_SESSION["butts_Add"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Edit"])) {
        $_SESSION["butts_Edit"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Del"])) {
        $_SESSION["butts_Del"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Save"])) {
        $_SESSION["butts_Save"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Unlink"])) {
        $_SESSION["butts_Unlink"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Bann"])) {
        $_SESSION["butts_Bann"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_UnBann"])) {
        $_SESSION["butts_UnBann"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["buttkey"])) {
        $_SESSION["buttkey"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Enter"])) {
        $_SESSION["butts_Enter"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["Frm_ID"])) {
        $_SESSION["Frm_ID"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["Open_Dashboard"])) {
        $_SESSION["Open_Dashboard"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["Restart_Dashboard"])) {
        $_SESSION["Restart_Dashboard"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Update"])) {
        $_SESSION["butts_Update"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Src"])) {
        $_SESSION["butts_Src"] = md5(uniqids(microtime()));
    }
    if (!isset($_SESSION["butts_Reset"])) {
        $_SESSION["butts_Reset"] = md5(uniqids(microtime()));
    }

    # Show The Page
    require_once "system/FileManClass.php";
    require_once "system/PagingClass2.php";
    require_once "system/HitDaysClass.php";
    require_once "system/GrantMenu.php";
    require_once "page/" . INNER_PAGE;
}
