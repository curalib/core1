<?php
# User Menu Access
$mmshow = '';
$UsrAccs = '';
if ($_SESSION["lgnPriv"] == 0) {
	$UsrAccs = 1;
} else {
	$cMenuAccs = "select menus_id from " . $tblp . "sys_menuaccess where id_logintbl = '" . $_SESSION["tblID"] . "'";
	$rcMenuAccs = $dbs->getQuery($cMenuAccs);
	$UsrAccs = "(";
	while ($rMA = $dbs->getAssoc($rcMenuAccs)) {
		$mac++;
		if ($mac == 1) {
			$UsrAccs .= "id = '" . $rMA['menus_id'] . "' ";
		} else {
			$UsrAccs .= "or id = '" . $rMA['menus_id'] . "'";
		}
	}
	if (empty($mac)) {
		$UsrAccs = "id = ''";
	}
	$UsrAccs .= ')';
}

$cUsrMainMenu = "SELECT id, nama, subnama, icon, tipe_menu, tipe_konten, mandatory, module, page, mods, tgl FROM " . $tblp . "sys_menu WHERE `id_mainmenu` != 'SYSTEM'  AND `id_parent` IS NULL AND `publish` = 'Y' AND tingkat = 1 AND " . $UsrAccs . " order by no_urut asc";
$rcUsrMainMenu = $dbs->getQuery($cUsrMainMenu);
while ($mm_det = $dbs->getAssoc($rcUsrMainMenu)) {
	$mmshow .= '<li class="sidebar-menu-item ">';
	switch ($mm_det['tipe_menu']) {
		case "ITEM":
			switch ($mm_det['tipe_konten']) {
				case "APP":
					$mmshow .= '<a class="sidebar-menu-button"  href="' . BASEURL . SplitKeys($mm_det['tgl']) . '/0/0.html">';
					$mmshow .= '<i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">' . $mm_det["icon"] . '</i>';
					$mmshow .= '<span class="sidebar-menu-text">' . ${"lang_" . $mm_det["nama"]} . '</span>';
					$mmshow .= '</a>';
					break;

				case "LINK":
					# Ini kalu menu itu adalah link.
					break;
			}
			break;

		case "MENU":
			$mmshow .= '<a class="sidebar-menu-button" data-toggle="collapse" href="#' . $mm_det["module"] . '">';
			$mmshow .= '<i class="sidebar-menu-icon sidebar-menu-icon--left material-icons">' . $mm_det["icon"] . '</i>';
			$mmshow .= '<span class="sidebar-menu-text">' . ${"lang_" . $mm_det["nama"]} . '</span>';
			$mmshow .= '<span class="ml-auto sidebar-menu-toggle-icon"></span>';
			$mmshow .= '</a>';
			$mmshow .= '<ul class="sidebar-submenu collapse" id="' . $mm_det["module"] . '">';
			// Cari Turunan, Menu Tingkat 2;
			$cUsrMenuLv2 = "select id, nama, subnama, icon, tipe_menu, tipe_konten, mandatory, module, page, mods, tgl from " . $tblp . "sys_menu where `id_parent` = '" . $mm_det["id"] . "' and `publish` = 'Y' and tingkat = 2 and " . $UsrAccs . " order by no_urut asc";
			$rcUsrMenuLv2 = $dbs->getQuery($cUsrMenuLv2);
			while ($mmlv2_det = $dbs->getAssoc($rcUsrMenuLv2)) {
				$mmshow .= '<li class="sidebar-menu-item">';
				switch ($mmlv2_det['tipe_menu']) {
					case "ITEM":
						switch ($mmlv2_det['tipe_konten']) {
							case "APP":
								$mmshow .= '<a class="sidebar-menu-button" href="' . BASEURL . SplitKeys($mmlv2_det['tgl']) . '/0/0.html">';
								$mmshow .= '<span class="sidebar-menu-text">' . ${"lang_" . $mmlv2_det["nama"]} . '</span>';
								$mmshow .= '</a>';
								break;

							case "LINK":
								# Ini kalu menu itu adalah link.
								break;
						}
						break;

					case "MENU":
						$mmshow .= '<a class="sidebar-menu-button" data-toggle="collapse" href="#tgl' . $mmlv2_det["nama"] . '">';
						$mmshow .= '<span class="sidebar-menu-text">' . ${"lang_" . $mmlv2_det["nama"]} . '</span>';
						$mmshow .= '<span class="ml-auto sidebar-menu-toggle-icon"></span>';
						$mmshow .= '</a>';
						$mmshow .= '<ul class="sidebar-submenu collapse" id="tgl' . $mmlv2_det["nama"] . '">';
						$cUsrMenuLv3 = "select id, nama, subnama, icon, tipe_menu, tipe_konten, mandatory, module, page, mods, tgl from " . $tblp . "sys_menu where `id_parent` = '" . $mmlv2_det["id"] . "' and `publish` = 'Y' and tingkat = 3 and " . $UsrAccs . " order by no_urut asc";
						$rcUsrMenuLv3 = $dbs->getQuery($cUsrMenuLv3);
						while ($mmlv3_det = $dbs->getAssoc($rcUsrMenuLv3)) {
							$mmshow .= '<li class="sidebar-menu-item">';
							switch ($mmlv3_det['tipe_menu']) {
								case "ITEM":
									switch ($mmlv3_det['tipe_konten']) {
										case "APP":
											$mmshow .= '<a class="sidebar-menu-button" href="' . BASEURL . SplitKeys($mmlv3_det['tgl']) . '/0/0.html">';
											$mmshow .= '<span class="sidebar-menu-text">' . ${"lang_" . $mmlv3_det["nama"]} . '</span>';
											$mmshow .= '</a>';
											break;

										case "LINK":
											# Ini kalu menu itu adalah link.
											break;
									}
									break;

								case "MENU":
									# Cari Menu level 4;
									break;
							}
							$mmshow .= '</li>';
						}
						$mmshow .= '</ul>';
						break;
				}
				$mmshow .= '</li>';
			}
			$mmshow .= '</ul>';
			break;
	}
	$mmshow .= '</li>';
}


# Get System Menu
# -------------------------------------------------
$sysshow = '';
$csysmenu = "select id, nama, subnama, icon, tipe_menu, tipe_konten, module, page, mods, links, target, tgl from " . $tblp . "sys_menu where publish = 'Y' and id_mainmenu = 'SYSTEM' order by no_urut asc";
$rcsysmenu = $dbs->getQuery($csysmenu);
while ($rcsys = $dbs->getAssoc($rcsysmenu)) {
	if ($rcsys['tipe_menu'] == "ITEM") {
		$rcsysc++;
		//if ( $rcsysc == 1 ){
		if ($rcsys['page'] != 'SysLogoutPg') {
			$sysshow .= '<a class="dropdown-item" href="' . BASEURL . SplitKeys($rcsys['tgl']) . '/0/0.html">' . ${"lang_" . $rcsys["nama"]} . '</a>';
		} else {
			$sysshow .= '<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="' . BASEURL . SplitKeys($rcsys['tgl']) . '/0/0.html" onClick="return konfirmasi(\'' . $lang_998 . '\')">'
				. ${"lang_" . $rcsys["nama"]}
				. '</a>';
		}
	}
}
