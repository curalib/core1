<?php

require "system/phpmailer/PHPMailer.php";
require "system/phpmailer/Exception.php";
require "system/phpmailer/SMTP.php";

use PhpMailer\PhpMailer\PHPMailer;
use PhpMailer\PhpMailer\Exception as PHPMailerException;

$mail = new PHPMailer();
