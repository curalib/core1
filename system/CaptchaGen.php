<?php
$cptSrc = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789abcdefghijkmnpqrstuvwxyz";
$cptCode = substr(str_shuffle($cptSrc), 0, CPTCH_LENG);

if ( SESS_DIR != 1 ){ session_save_path(SESS_DIR); }
if (session_status() === PHP_SESSION_NONE) {
	session_start();
}

$_SESSION["CPT_CODE"] = $cptCode;
?>