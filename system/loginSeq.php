<?php
if ($logbutt == $lang_4) {
    # Check IP Brute Force
    $chkBruteForceIP = "select id from " . $tblp . "sys_ip_ban_list where ip = '" . $ip_addr . "'";
    $rchkBruteForceIP = $dbs->getArr($chkBruteForceIP);
    if ($rchkBruteForceIP['id']) {
        if (IP_BLOCK === true) {
            # Check White List
            if (IP_WHITE === true) {
                $ChWhtLst = "select id from " . $tblp . "sys_ip_white_list where ip_addr = '" . $ip_addr . "'";
                $rChWhtLst = $dbs->getArr($ChWhtLst);
                if (empty($rChWhtLst['id'])) {
                    $LoginErrs = "IP_LOCK";
                }
            } else {
                $LoginErrs = "IP_LOCK";
            }
        }
    }

    # Check Domain
    if (!$LoginErrs) {
        $PassUrl = CheckUrl(USE_DOMAIN_CHECK, $dom_array, "");
        if ($PassUrl === false) {
            $LoginErrs = "HTML_HIJACKING";
            $alasan = "HTML hijacking attempt detected.";
            $inBan = "insert into " . $tblp . "sys_ip_ban_list set
                                          id = '" . uniqid(md5(microtime())) . "',
                                          ip = '" . $ip_addr . "',
                                          tgl = now(),
                                          adding_by = 'SYSTEM',
                                          alasan = '" . $alasan . "'";
            $dbs->getQuery($inBan);
        }
    }

    # Check Captcha
    if (!$LoginErrs) {
        if (CPTCH === true) {
            $inCCode = Purefy($_POST['CCode']);
            if (!empty($inCCode)) {
                if ($_SESSION["CPT_CODE"] != $inCCode) {
                    $LoginErrs = "WRONG_CAPT";
                }
            } else {
                $LoginErrs = "WRONG_CAPT";
            }
        }
    }

    if (!$LoginErrs) {
        //Kalau tidak ada kesalahan dari cek2 di atas
        $inUid = Purefy($_POST['fuid']);
        $inPwd = Purefy($_POST['fpwd']);
        $LoginErrs = '';
        if (empty($inUid) or empty($inPwd)) {
            //Bila userid dan password terkirim TIDAK tersedia
            $LoginErrs = "NO_CREDS";
        } else {
            //Bila userid dan password terkirim tersedia
            #PassCheck
            $PassCheck = "select pwd from " . $tblp . "sys_login where userid = ? ";
            $bind = array("s|" . md5($inUid));
            $rPassCheck = $dbs->getArr($PassCheck, $bind);

            //check password based on user id
            if (password_verify($inPwd, $rPassCheck['pwd'])) {
                //Password database sama dengan input
                //AMBIL DATA
                $LoginCheck = "select id, login_id, bann, wrong_p, idle, lact, lact_login, locking from " . $tblp . "sys_login where userid = ? ";
                $bind = array("s|" . md5($inUid));
                $rLC = $dbs->getArr($LoginCheck, $bind);
            }

            if (isset($rLC['id'])) {
                //LOGIN OK UserID benar password benar
                if ($rLC['bann'] == 1) {
                    //KALAU AKUN DI BANNED
                    $LoginErrs = "ACC_BAN";
                } else {
                    # Check Logout
                    if (CH_LOGOUT === true) {
                        if (!empty($rLC['login_id'])) {
                            $upD = "update " . $tblp . "sys_login set login_id = '', idle = '" . CHECK_LOGOUT . "', lact = '" . microtime_float() . "' where userid = ? ";
                            $bind = array("s|" . md5($inUid));
                            $dbs->getQuery($upD, "Exec", $bind);
                            $LoginErrs = "ACC_LOGOUT_LOCK";
                            $remainlock = TimeRemain(microtime_float(), CHECK_LOGOUT);
                            $LoginErrsMess = formatPeriod($remainlock);
                        } elseif ($rLC['idle'] > 0) {
                            if (CheckIdle($rLC['lact'], $rLC['idle']) === true) {
                                $nxtidle = $rLC['idle'] + LOGOUT_INCREASING;
                                $up = "update " . $tblp . "sys_login set idle = '" . $nxtidle . "', lact = '" . microtime_float() . "' where userid = ?";
                                $bind = array("s|" . md5($inUid));
                                $dbs->getQuery($up, "Exec", $bind);
                                $LoginErrs = "ACC_LOGOUT_LOCK";
                                $remainlock = TimeRemain(microtime_float(), $nxtidle);
                                $LoginErrsMess = formatPeriod($remainlock);
                            } else {
                                $up = "update " . $tblp . "sys_login set idle = '', lact = '' where userid = ?";
                                $bind = array("s|" . md5($inUid));
                                $dbs->getQuery($up, "Exec", $bind);
                            }
                        }
                    } //if ( CH_LOGOUT === true ){
                }

                if (!$LoginErrs) {
                    if (LIMIT_LGN_ATTEMPTS === true) {
                        if ((CheckIdle($rLC['lact_login'], $rLC['locking']) === true) && $rLC['wrong_p'] > WRONG_PASS) {
                            $LoginErrs = "ACC_LOCK";
                            $remainlock = TimeRemain($rLC['lact_login'], $rLC['locking']);
                            $LoginErrsMess = formatPeriod($remainlock);
                        }
                    }
                }

                if (!$LoginErrs) {
                    //LOGIN OK bisa masuk ke dashboard
                    ManSession("Create");

                    #Get Credentials
                    $_SESSION["ROOT_DIR"] = ROOT_DIR;

                    $loginID = md5(uniqid(microtime()));
                    $up = "update " . $tblp . "sys_login set wrong_p = 0, login_id = '" . $loginID . "', curr_login = now(), idle = 0, locking = 0, lact = 0, ip_addr = '" . $ip_addr . "', lact_action = '" . microtime_float() . "', lact_login = 0 where userid = ? ";
                    $bind = array("s|" . md5($inUid));
                    $dbs->getQuery($up, "Exec", $bind);

                    $cLgnDet = "select id, uid, priv, nama_asli, karyawan, jabatan, avatar from " . $tblp . "sys_lgndetail where id_login = '" . $rLC['id'] . "'";
                    $rcLgnDet = $dbs->getArr($cLgnDet);

                    $_SESSION["loginID"] = $loginID; # Session Login
                    $_SESSION["loginUser"] = md5($inUid); # md5 userid
                    $_SESSION["lgnUid"] = $rcLgnDet['uid']; # userid
                    $_SESSION["tblID"] = $rLC['id']; # id table sys_login
                    $_SESSION["lgnPriv"] = $rcLgnDet['priv']; # Privelege
                    $_SESSION["lgnNama"] = $rcLgnDet['nama_asli']; # Nama asli
                    $_SESSION["Jbtn"] = $rcLgnDet['jabatan'];
                    $_SESSION["Avatar"] = $rcLgnDet['avatar'];
                    $_SESSION["lgnDetID"] = $rcLgnDet['id']; # Login Detail ID, sales-id

                    // Bikin Menu2 nya
                    require_once "system/GrantMenu.php";

                    # Show Page
                    # Hard Code !!
                    $cmodule = "Dashboard";
                    $cmod = "DBoard_i.php";
                    $cpage = "Dashboard.php";

                    if (!is_file("Modules/" . $cmodule . "/mod/" . $cmod)) {
                        //Kalau file tidak di temukan di module atau mod nya kosong
                        //INGAT MOD TIDAK BOLEH KOSONG
                        $cmodule = "System";
                        $cpage = "PageNotFound.php";
                        $cmod = "";
                    } else {
                        // Lakukan Perintah menggunakan halaman dari folder mod di module
                        // File yang berakhiran *_i.php
                        require "system/FileManClass.php";
                        require "system/HitDaysClass.php";
                        require_once "system/PagingClass2.php";
                        require "Modules/" . $cmodule . "/mod/" . $cmod;
                    }

                    if (!isset($_SESSION["butts_Enter"])) {
                        $_SESSION["butts_Enter"] = md5(uniqids(microtime()));
                    }

                    if (!isset($_SESSION["butts_Update"])) {
                        $_SESSION["butts_Update"] = md5(uniqids(microtime()));
                    }

                    require_once "page/" . INNER_PAGE;
                }
            } else {
                //LOGIN Not OK UserID benar tapi password salah
                # Check IP Addr Hit
                $endHit = mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y"));
                $startHit = bcsub($endHit, IP_CHECK_PERIOD, 2);
                $hitungIP = "select count(*) as jum from " . $tblp . "sys_brute_force_ip where ip_addr = '" . $ip_addr . "' and (UNIX_TIMESTAMP(wkt) >= '" . $startHit . "' and UNIX_TIMESTAMP(wkt) <= '" . $endHit . "')";
                $rhitungIP = $dbs->getArr($hitungIP);
                if (($rhitungIP['jum'] + 1) >= IP_BANN) {
                    $checkBan = "select id from " . $tblp . "sys_ip_ban_list where ip = '" . $ip_addr . "'";
                    $rcheckBan = $dbs->getArr($checkBan);
                    if (!$rcheckBan['id']) {
                        $alasan = ($rhitungIP['jum'] + 1) . " Login attempts in " . IP_CHECK_PERIOD . " seconds.";
                        $inBan = "insert into " . $tblp . "sys_ip_ban_list set
                                                                  id = '" . uniqid(md5(microtime())) . "',
                                                                  ip = '" . $ip_addr . "',
                                                                  tgl = now(),
                                                                  adding_by = 'SYSTEM',
                                                                  alasan = '" . $alasan . "'";
                        $dbs->getQuery($inBan);
                    }

                    if (IP_BLOCK === true) {
                        $LoginErrs = "IP_LOCK";
                    }
                }

                if (!$LoginErrs) {
                    $inIP = "insert into " . $tblp . "sys_brute_force_ip set id = '" . uniqid(md5(microtime())) . "', uid = '" . $inUid . "', ip_addr = '" . $ip_addr . "', ip_addr_note = '" . $ip_note . "', wkt = now()";
                    $dbs->getQuery($inIP);

                    $cNL = "select id, locking, lact_login, wrong_p from " . $tblp . "sys_login where userid = ?";
                    $bind = array("s|" . md5($inUid));
                    $rcNL = $dbs->getArr($cNL, $bind);
                    //
                    if ($rcNL['id']) {
                        $lgn_attempt = $rcNL['wrong_p'] + 1;

                        if ($lgn_attempt <= WRONG_PASS) {
                            if (CheckIdle($rLC['lact_login'], $rLC['locking']) === false) {
                                if (LOCK_RESET === true) {
                                    $lgn_attempt = 1;
                                }
                            }
                        }

                        if (LIMIT_LGN_ATTEMPTS === true) {
                            if ($lgn_attempt < WRONG_PASS) {
                                $upDb = "update " . $tblp . "sys_login set wrong_p = '" . $lgn_attempt . "', lact_login = '" . microtime_float() . "' where userid = ?";
                                $LoginErrs = "BAD_LGN";
                            } elseif ($lgn_attempt == WRONG_PASS) {
                                $timewait = TEMP_LOCK;
                                $upDb = "update " . $tblp . "sys_login set wrong_p = '" . $lgn_attempt . "', lact_login = '" . microtime_float() . "', locking = '" . $timewait . "' where userid = ? ";

                                $LoginErrs = "ACC_LOCK";
                                $remainlock = TimeRemain(microtime_float(), $timewait);
                                $LoginErrsMess = formatPeriod($remainlock);
                            } elseif ($lgn_attempt > WRONG_PASS) {
                                $timewait = $rcNL['locking'] + LOCK_INCREASING;
                                $upDb = "update " . $tblp . "sys_login set wrong_p = '" . $lgn_attempt . "', lact_login = '" . microtime_float() . "', locking = '" . $timewait . "' where userid = ? ";
                                $LoginErrs = "ACC_LOCK";
                                $remainlock = TimeRemain(microtime_float(), $timewait);
                                $LoginErrsMess = formatPeriod($remainlock);
                            }
                            $bind = array("s|" . md5($inUid));
                            $dbs->getQuery($upDb, "Exec", $bind);
                        } else {
                            //echo " SAMPAI SINI 2 ";
                            //exit(0);
                            $LoginErrs = "BAD_LGN";
                        }
                    } else {
                        if (LIMIT_LGN_ATTEMPTS === true) {
                            $UidNgawur = "select id, lact_login, wrong_p, locking from " . $tblp . "sys_brute_force where uid = ?";
                            $bind = array("s|" . $inUid);
                            $rUidNgawur = $dbs->getArr($UidNgawur, $bind);
                            if ($rUidNgawur['id']) {
                                $lgn_attempt = $rUidNgawur['wrong_p'] + 1;
                                if ($lgn_attempt < WRONG_PASS) {
                                    $upDb = "update " . $tblp . "sys_brute_force set wrong_p = '" . $lgn_attempt . "', lact_login = '" . microtime_float() . "' where uid = ? ";

                                    $LoginErrs = "BAD_LGN";
                                } elseif ($lgn_attempt == WRONG_PASS) {
                                    $timewait = TEMP_LOCK;
                                    $upDb = "update " . $tblp . "sys_brute_force set wrong_p = '" . $lgn_attempt . "', lact_login = '" . microtime_float() . "', locking = '" . $timewait . "' where uid = ? ";
                                    $LoginErrs = "ACC_LOCK";
                                    $remainlock = TimeRemain(microtime_float(), $timewait);
                                    $LoginErrsMess = formatPeriod($remainlock);
                                } elseif ($lgn_attempt > WRONG_PASS) {
                                    $timewait = $rUidNgawur['locking'] + LOCK_INCREASING;
                                    $upDb = "update " . $tblp . "sys_brute_force set wrong_p = '" . $lgn_attempt . "', lact_login = '" . microtime_float() . "', locking = '" . $timewait . "' where uid = ? ";
                                    $LoginErrs = "ACC_LOCK";
                                    $remainlock = TimeRemain(microtime_float(), $timewait);
                                    $LoginErrsMess = formatPeriod($remainlock);
                                }
                                $bind = array("s|" . $inUid);
                                $dbs->getQuery($upDb, "Exec", $bind);
                            } else {
                                $upDb = "insert into " . $tblp . "sys_brute_force set id = '" . md5(uniqid(microtime())) . "', uid = ?, wrong_p = 1";
                                $bind = array("s|" . $inUid);
                                $dbs->getQuery($upDb, "Exec", $bind);
                                $LoginErrs = "BAD_LGN";
                            }
                        } else {

                            $LoginErrs = "BAD_LGN";
                        } //293	}else{
                    } //253	}else{
                } //205	if ( !$LoginErrs ){
            } //170	}else{			if ( isset($rLC['id']) ){
        } //56	} else {
    } //50	if ( !$LoginErrs ) {
} //02	if ( $logbutt == $lang_4 ){
