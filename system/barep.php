<?php
ini_set("memory_limit", "-1");
ini_set('implicit_flush', true);
ini_set('output_buffering', 'off');
ini_set('zlib.output_compression', 0);
ini_set('session.use_strict_mode', false);
ini_set('max_execution_time', 0);
set_time_limit(0);
date_default_timezone_set(TIMESET);

# Language and Function Library
require "language/default.php";
require_once "system/function.php";
require_once "system/is_email.php";

# Reset Error;
$LoginErrsMess = "";
$LoginErrs = "";

# Set Params.
if (SESS_DIR != 1) {
    session_save_path(SESS_DIR);
}

# Get Current IP
list($ip_addr, $ip_note) = explode("|", getIP());

# Set Mysql Table Prefix
if (DBC_PREF != "") {
    $tblp = DBC_PREF . "_";
} else {
    $tblp = "";
}

# Connecting to MySQL
require "system/db_connect.php";

# Check if IP is listed on brute force database.
# If yes, show error => IP_LOCK
$chkBruteForceIP = "select id from " . $tblp . "sys_ip_ban_list where ip = '" . $ip_addr . "' limit 1";
$rchkBruteForceIP = $dbs->getArr($chkBruteForceIP);
if (is_array($rchkBruteForceIP) && $rchkBruteForceIP['id'] && IP_BLOCK === true) {
    # Check White List
    if (IP_WHITE === true) {
        $ChWhtLst = "select id from " . $tblp . "sys_ip_white_list where ip_addr = '" . $ip_addr . "'";
        $rChWhtLst = $dbs->getArr($ChWhtLst);
        if (empty($rChWhtLst['id'])) {
            $LoginErrs = "IP_LOCK";
        }
    } else {
        $LoginErrs = "IP_LOCK";
    }
}

# Continue with process
if ($LoginErrs) {
    # This will show error on login page if
    # IP is listed on brute force database
    //ManSession("Destroy");
    require_once "page/" . LOGINPAGE;
} else {
    # Check Environtment.
    # Environtment can be setted on CrcConfig.php
    list($LoginErrs, $LoginErrMess) = explode("|", ChkEnv());
    if ($LoginErrs) {
        # If wrong environtment setted
        //ManSession("Destroy");
        require_once "page/" . LOGINPAGE;
    } else {
        if (isset($_POST['Login'])) {
            $logbutt = Purefy($_POST['Login']);
        } else {
            $logbutt = "";
        }

        if ($logbutt == $lang_5 or $logbutt == $lang_23) {
            ManSession("ReStart");
            require "system/CaptchaGen.php";
            require_once "page/" . LOGINPAGE;
        } elseif ($logbutt == $lang_4) {
            ManSession("Start");
            require "system/loginSeq.php";

            if ($LoginErrs) {
                ManSession("ReStart");
                require "system/CaptchaGen.php";
                require_once "page/" . LOGINPAGE;
            }
        } elseif (empty($logbutt)) {
            ManSession("Start");
            if (isset($_SESSION['loginID'])) {
                if (isset($_POST['task'])) {
                    $task = Purefy($_POST['task']);
                } else {
                    $task = '';
                }

                require_once "system/CheckLogin.php";
                if ($LoginErrs) {
                    if (CLEAR_SESSION === true) {
                        $clearLoginId = "update " . $tblp . "sys_login set login_id = '' where userid = '" . $_SESSION["loginUser"] . "'";
                        $dbs->getQuery($clearLoginId);
                    }

                    ManSession("ReStart");
                    require_once "page/" . LOGINPAGE;
                }
            } else {
                ManSession("ReStart");
                require "system/CaptchaGen.php";
                require_once "page/" . LOGINPAGE;
            }
        }
    }
}
