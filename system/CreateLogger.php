<?php
require_once("vendor/autoload.php");

date_default_timezone_set('Asia/Jakarta');

// create a log channel
$logger = new \Monolog\Logger('default');
$logger->pushHandler(new \Monolog\Handler\RotatingFileHandler('./log/izinapps.log'));