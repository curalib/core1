<?php
require "CrcConfig.php";
require "appsConfig.php";

ini_set("memory_limit", "-1");
ini_set('output_buffering','off');
ini_set('zlib.output_compression', false);
ini_set('session.use_strict_mode', false);
set_time_limit(0);
date_default_timezone_set(TIMESET);

ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
error_reporting(0);

require "../language/default.php";
require_once "function.php";
require_once "is_email.php";

header("Content-type: text/plain");
header('Cache-Control: no-cache');

if(SESS_DIR != 1){ session_save_path("../".SESS_DIR); }
if ( DBC_PREF != "" ) { $tblp = DBC_PREF."_"; }else{ $tblp = ""; }

ManSession("Start");
if(isset($_SESSION['loginID'])) {
	require "db_connect.php";
	require "FileManClass.php";
	$pg = Purefy($_POST['pg']);
	$amod = Purefy($_POST['amod']);
	if ( is_file("../Modules/".$amod."/file/".$pg.".php") ){
		require "../Modules/".$amod."/file/".$pg.".php";
	} else {
		echo $LoginErrsPesan["PS_AJAX_FS_NOT_FOUND"]." ".$pg." = ".$amod." == ".BASEURL;
	}
} else {
	echo $LoginErrsPesan["PS_AJAX_CON_ERR"];
}

?>
