<?php
# Handling the page request
$task = Purefy($_POST['task']);
if ( $task == $_SESSION["buttkey"]."prev" ){
	$pagenum = Purefy($_POST['pagenum']);
	$pageid = Purefy($_POST['pageid']);
	$sessname = "currpage".$pageid;
	$_SESSION[$sessname] = $pagenum;
}


Class Paging
{
	var $per_page;
	var $shown_link;
	var $query;
	var $max_item;
	var $koneksi;

	// Get New Params
	function __construct($koneksi,$per_page=20, $shown_link=5,$max_item=-1,$pageid="default")
	{
		$this->per_page = $per_page; // Max record shown per page
		$this->shown_link = $shown_link; // Max page shown
		$this->max_item = $max_item; // Maximum item could be displayed. Set to -1 (minus one) for unlimited item.
		$this->pageid = $pageid;
		$this->koneksi = $koneksi;
	}

	// Query
	function query($query)
	{
		// Find ammount of total record.
		if ( !strstr($query,"distinct") ){
			$sql_count = preg_replace("/^select (.*) from (.*)/si","select count(*) as jumlah from \\2",$query);
//$sql_count = preg_replace("/^select (.) from (.) $/si","select count(*) as jumlah from \\2",$query);
		}else{
			$sql_count = preg_replace("/(.*) group (.*)/si", "\\1",$query);
			$sql_count = preg_replace("/(.*) distinct (.*) from (.*)/si","select count(distinct \\2) as jumlah from \\3",$sql_count);
		}
//echo $sql_count;
//exit;

		$result_count = $this->koneksi->getArr($sql_count);
		$this->record = $result_count['jumlah'];
//echo "sampe ".$result_count['jumlah'];
//exit;

		if ( $this->per_page == -1 ){ $this->per_page = $this->record; }

		// Limiting Record if Any
		if ( $this->max_item > -1 and $this->record > $this->max_item ) { $this->record = $this->max_item; }

		// See if amount of record exceeds max record shown
		if ( $this->record > $this->per_page ) {

			// Determine max paging link availabe according to amount of record
			$this->max_paging = ceil($this->record/$this->per_page);

			// Find current page number
			$currpagename = "currpage".$this->pageid;
			$this->s_page = $_SESSION[$currpagename];
			if ( empty( $this->s_page) ) {
				$this->s_page = 1;
			}elseif ( $this->s_page > $this->max_paging ) {
				$this->s_page = $this->max_paging;
			}

			// Determine startrecord
			$this->start_record = ( $this->per_page * $this->s_page ) - $this->per_page;

			// New SQL query
			$sql_new = preg_replace("/^select (.*)/si","select \\1 limit $this->start_record,$this->per_page",$query);
//echo $sql_new;
//exit;

		}else{
			if ( $this->max_item > -1 ) {
				$sql_new = preg_replace("/^select (.*)/si","select \\1 limit $this->max_item",$query);
			}else{
				$sql_new = $query;
			}

		}

		$result_new = $this->koneksi->getQuery($sql_new);
		if ( !$result_new) {
			throw new Exception('Query Error!');
		}else{
			$this->result_new=$result_new;
		}
	}


	// Query
	function queryTracking($query1, $query2)
	{
		// Find ammount of total record.
		$result_count = $this->koneksi->getArr($query2);
		$this->record = $result_count['jumlah'];
//echo "sampe ".$result_count['jumlah'];
//exit;

		if ( $this->per_page == -1 ){ $this->per_page = $this->record; }

		// Limiting Record if Any
		if ( $this->max_item > -1 and $this->record > $this->max_item ) { $this->record = $this->max_item; }

		// See if amount of record exceeds max record shown
		if ( $this->record > $this->per_page ) {

			// Determine max paging link availabe according to amount of record
			$this->max_paging = ceil($this->record/$this->per_page);

			// Find current page number
			$currpagename = "currpage".$this->pageid;
			$this->s_page = $_SESSION[$currpagename];
			if ( empty( $this->s_page) ) {
				$this->s_page = 1;
			}elseif ( $this->s_page > $this->max_paging ) {
				$this->s_page = $this->max_paging;
			}

			// Determine startrecord
			$this->start_record = ( $this->per_page * $this->s_page ) - $this->per_page;



			// New SQL query
			//$sql_new = preg_replace("/^select (.*)/si","select \\1 limit $this->start_record,$this->per_page",$query);
			$sql_new = $query1."LIMIT ".$this->start_record.",".$this->per_page." ";

//echo "sampe ".$sql_new;
//exit;

		}else{
			if ( $this->max_item > -1 ) {
				//$sql_new = preg_replace("/^select (.*)/si","select \\1 limit $this->max_item",$query);
				$sql_new = $query1."LIMIT ".$this->max_item." ";
			}else{
				$sql_new = $query1;
			}

		}
//echo $sql_new;
//exit;


		$result_new = $this->koneksi->getQuery($sql_new);
		if ( !$result_new) {
			throw new Exception('Query Error!');
		}else{
			$this->result_new=$result_new;
		}
	}




	// Return New SQl Query Assoc
	function result_assoc(){
		return $this->koneksi->getAssoc($this->result_new);
	}

	// Show current displayed data
	function data_info()
	{
		// See if amount of record exceeds max record shown
		if ( $this->record > $this->per_page ) {

			// Check range of current page
			$this->start_page = ceil($this->s_page/$this->shown_link);
			$this->end_page = ceil($this->s_page/$this->shown_link) * $this->shown_link;

			// Find current shown records and total records
			$first_curr_record = ($this->s_page * $this->per_page) - ($this->per_page - 1);
			$this->for_number = $first_curr_record - 1;
			if ( $this->s_page * $this->per_page >= $this->record ) {
				$second_curr_record = $this->record;
			}else{
				$second_curr_record = $this->s_page * $this->per_page;
			}

			// Passing it up !!!!
			$print_curr_data = "Data no " . $first_curr_record . " - " . $second_curr_record . " from ". $this->record . " data";

		} else {
			if ( $this->record == 0 ) {
				$print_curr_data = "No Data Available";
			}else{
				$print_curr_data = "Data no 1 - " . $this->record . " from " . $this->record . " data";
			}
		}

		return $print_curr_data;

	}

	function show_num()
	{
		$number = $this->for_number += 1;
		return $number;
	}

	//function print_page()
	function print_page()
	{

		// See if amount of record exceeds max record shown
		if ( $this->record > $this->per_page ) {

			// Print The damn paging link

			if ( $this->start_page > 1 and $this->s_page > $this->shown_link ) {
				$start_count = $this->end_page - $this->shown_link + 1;
            }else{
            	$start_count = $this->start_page;
            }

			$num_page_prev = $this->end_page - $this->shown_link;
			$num_page_next = $this->end_page + 1;

			if ( $this->end_page <= $this->max_paging ) {
				$num_page_mid = $this->end_page;
			} else {
				$num_page_mid = $this->max_paging;
			}

			$print_page = '<ul class="pagination justify-content-center ">';
			if ( $this->s_page > 1 ){
				$this->form_print_page .= "<form action=\"\" method=\"post\" name=\"".$this->pageid."_paging_first\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"task\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pagenum\" value=\"1\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pageid\" value=\"".$this->pageid."\">";
				$this->form_print_page .= "</form>";
				$print_page .= '<li class="page-item">'."<a class=\"page-link\" href=\"javascript:submitbutton('".$_SESSION["buttkey"]."prev"."','".$this->pageid."_paging_first');\" aria-label=\"Previous\">".'<span aria-hidden="true" class="material-icons">first_page</span> <span class="sr-only">First</span> </a> </li>';

				$this->form_print_page .= "<form action=\"\" method=\"post\" name=\"".$this->pageid."_paging_prev\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"task\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pagenum\" value=\"".$num_page_prev."\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pageid\" value=\"".$this->pageid."\">";
				$this->form_print_page .= "</form>";
				$print_page .= '<li class="page-item">'."<a class=\"page-link\" href=\"javascript:submitbutton('".$_SESSION["buttkey"]."prev"."','".$this->pageid."_paging_prev');\" aria-label=\"Previous\">".'<span aria-hidden="true" class="material-icons">chevron_left</span> <span class="sr-only">Prev</span> </a> </li>';

			}else{
				$print_page .= '<li class="page-item disabled"> <a class="page-link" href="#" aria-label="Previous"> <span aria-hidden="true" class="material-icons">first_page</span> <span class="sr-only">First</span> </a> </li>';
				$print_page .= '<li class="page-item disabled"> <a class="page-link" href="#" aria-label="Previous"> <span aria-hidden="true" class="material-icons">chevron_left</span> <span class="sr-only">Prev</span> </a> </li>';
			}

			for ($i=$start_count;$i<=$num_page_mid;$i++) {
				if ( $i != $this->s_page ) {
					$this->form_print_page .= "<form action=\"\" method=\"post\" name=\"".$this->pageid."_paging_".$i."\">";
					$this->form_print_page .= "<input type=\"hidden\" name=\"task\">";
					$this->form_print_page .= "<input type=\"hidden\" name=\"pagenum\" value=\"".$i."\">";
					$this->form_print_page .= "<input type=\"hidden\" name=\"pageid\" value=\"".$this->pageid."\">";
					$this->form_print_page .= "</form>";
					$print_page .= '<li class="page-item">'."<a class=\"page-link\" href=\"javascript:submitbutton('".$_SESSION["buttkey"]."prev"."','".$this->pageid."_paging_".$i."');\">".'<span>'.$i.'</span> </a> </li>';
				}else{
					$print_page .= '<li class="page-item active"> <a class="page-link" href="#" aria-label="'.$i.'"> <span>'.$i.'</span> </a> </li>';
				}

			}

			if ( $this->s_page < $this->max_paging ) {

				$this->form_print_page .= "<form action=\"\" method=\"post\" name=\"".$this->pageid."_paging_next\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"task\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pagenum\" value=\"".$num_page_next."\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pageid\" value=\"".$this->pageid."\">";
				$this->form_print_page .= "</form>";

				$print_page .= '<li class="page-item">'."<a class=\"page-link\" href=\"javascript:submitbutton('".$_SESSION["buttkey"]."prev"."','".$this->pageid."_paging_next');\" aria-label=\"Next\">".'<span aria-hidden="true" class="material-icons">chevron_right</span></a></li>';

				$this->form_print_page .= "<form action=\"\" method=\"post\" name=\"".$this->pageid."_paging_end\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"task\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pagenum\" value=\"".$this->max_paging."\">";
				$this->form_print_page .= "<input type=\"hidden\" name=\"pageid\" value=\"".$this->pageid."\">";
				$this->form_print_page .= "</form>";

    			$print_page .= '<li class="page-item">'."<a class=\"page-link\" href=\"javascript:submitbutton('".$_SESSION["buttkey"]."prev"."','".$this->pageid."_paging_end');\" aria-label=\"Next\">".'<span class="sr-only">Last</span> <span aria-hidden="true" class="material-icons">last_page</span> </a> </li>';
			}else{
				$print_page  .= '<li class="page-item disabled"> <a class="page-link" href="#" aria-label="Next"> <span class="sr-only">Next</span> <span aria-hidden="true" class="material-icons">chevron_right</span> </a> </li>
    <li class="page-item disabled"> <a class="page-link" href="#" aria-label="Next"> <span class="sr-only">Last</span> <span aria-hidden="true" class="material-icons">last_page</span> </a> </li>';
			}

			$print_page .= '</ul>';
			return $print_page;
		}
	}

	function form_print_page()
	{
		return $this->form_print_page;
	}

	function curr_page()
	{
		if ( empty( $this->s_page) ) {
			$this->s_page = 1;
		}
		return $this->s_page;
	}

	function max_page()
	{
		if ( $this->max_paging > 0 ) {
			$max_pages = $this->max_paging;
		}else{
			$max_pages = 1;
		}
		return $max_pages;
	}
}
