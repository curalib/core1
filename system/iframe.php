<?php
require "CrcConfig.php";
require "appsConfig.php";
ini_set("memory_limit", "-1");
ini_set('implicit_flush', true);
ini_set('output_buffering','on');
ini_set('zlib.output_compression', 0);
ini_set('session.use_strict_mode', false);
set_time_limit(0);
date_default_timezone_set(TIMESET);

require "../language/default.php";
require_once "function.php";

if ( SESS_DIR != 1 ){ session_save_path("../".SESS_DIR); }

require "ChkCopy.php";
if ( DBC_PREF != "" ) { $tblp = DBC_PREF."_"; }else{ $tblp = $sch->SQLPrev()."_"; }
if ( $LoginErrs == "FR_INS" ) {
	ManSession("Destroy");
	require_once "../page/".AJAX_ERR;
	exit;
}elseif ( $LoginErrs == "IL_COPY" or $LoginErrs == "WRONG_ENV" or $LoginErrs == "WRG_OS" ){
	ManSession("Destroy");
	require_once "../page/".AJAX_ERR;
	exit;
}
ManSession("Start");
if ( isset($_SESSION['loginID']) ){
	echo '<script src="'.BASEURL.'js/jquery-1.11.1.js"></script>';
	echo '<script src="'.BASEURL.'js/pus.js"></script>';
	require "db_connect.php";
	require "FileManClass.php";
	$ifmodule = Purefy($_GET['im']);
	$iffile = Purefy($_GET['if']);
	$ifact = Purefy($_GET['ic']);
	$ifcss = Purefy($_GET['is']);
	
	if ( is_file("../css/".$ifcss.".css") ){
		echo '<link rel="stylesheet" href="'.BASEURL.'../css/'.$ifcss.'.css"/>';
		if ( is_file("../Modules/".$ifmodule."/file/".$iffile.".php") ){
			require "../Modules/".$ifmodule."/file/".$iffile.".php";
			if ( is_file("../Modules/".$ifmodule."/file/".$ifact.".php") ){
				require "../Modules/".$ifmodule."/file/".$ifact.".php";
			}else{
				echo $LoginErrsPesan["PS_AJAX_FS_NOT_FOUND"]." ".$pg;
			}
		}else{
			echo $LoginErrsPesan["PS_AJAX_FS_NOT_FOUND"]." ".$pg;
		}
	}else{
		echo $LoginErrsPesan["PS_AJAX_FS_NOT_FOUND"]." ".$pg;
	}
	

}else{
	echo $LoginErrsPesan["PS_AJAX_CON_ERR"];
}
?>