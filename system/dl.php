<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE|E_WARNING));

require "CrcConfig.php";
require "appsConfig.php";
require "../language/default.php";
require_once "function.php";
//include("system/db_connect.php");
if ( DBC_PREF != "" ) { $tblp = DBC_PREF."_"; }else{ $tblp = $sch->SQLPrev()."_"; }
require "db_connect.php";
require "FileManClass.php";

	$fids = Purefy($_POST['fids']);
	$fdir = Purefy($_POST['fdir']);

	if(!empty($fids))
	{
		if ($fdir == 'userfile')
		{
			$cGetFilename = "SELECT `filename`, `filename_orig`, `filesize` FROM `track_users_file` WHERE `id` = '".$fids."'";
			$rcGetFilename = $dbs->getArr($cGetFilename);

			$filename = "../../../".UPDIR_2."/".$fdir."/".$rcGetFilename['filename'];
			$saveasname = $rcGetFilename['filename_orig'];
			$filesize = $rcGetFilename['filesize'];
		}
		else
		{
			$cGetFilename = "SELECT `trkfile_name_uniq`, `trkfile_size`, `trkfile_name_ori` FROM `izin_apps_trkfile` WHERE `id_trkfile` = '".$fids."'";
			$rcGetFilename = $dbs->getArr($cGetFilename);

			$filename = "../../../".UPDIR."/".$fdir."/".$rcGetFilename['trkfile_name_uniq'];
			$saveasname = $rcGetFilename['trkfile_name_ori'];
			$filesize = $rcGetFilename['trkfile_size'];

			#Update unduh
			$ccu = "SELECT `trkfile_downloaded` FROM `izin_apps_trkfile` WHERE `id_trkfile` = '".$fids."'";
			$rccu = $dbs->getArr($ccu);
			$nextccu = $rccu['trkfile_downloaded']+1;
			$ncu = "UPDATE `izin_apps_trkfile` SET `trkfile_downloaded` = '".$nextccu."' WHERE `id_trkfile` = '".$fids."'";
			$dbs->getQuery($ncu);
		}

		if (!empty($filename) || file_exists($filename))
		{
			header('Content-Type: application/octet-stream; charset=utf-8');
			header('Content-Disposition: Attachment; filename="'.$saveasname.'"');
			header('Content-Length: '.$filesize);
			header('Content-Transfer-Encoding: binary');
			$download_rate = 20.5;
			$file = @fopen($filename,"rb");
			if ($file) {
				while(!feof($file)) {
						// send the current file part to the browser
					print(fread($file, 1024*$download_rate));
						// flush the content to the browser
						flush();
						// sleep one second
						//sleep(1);
						if (connection_status()!=0) {
						@fclose($file);
						die();
					}
				}
				@fclose($file);
			}

		}
		else
		{
			echo "No file";
		}
	}
?>
