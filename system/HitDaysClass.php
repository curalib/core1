<?php
/*--------------------
Busness Days and Duration Days
Barep ggembulls@gmail.com

You can use this class and make modification as you need.
Please keep this header intact.

# Required
	+ Fix Holiday Table name [table_prefix]_master_fixholiday 
	  must contain at least tgl (date), purblish (varchar (1) default: "Y".
	+ Year Holiday Table name _master_yearholiday
	  must contain at least tgl (date), purblish (varchar (1) default: "Y".

# Params Input,
	Construct:
	+ connect_db class
	+ table prefix
	
	DayRange:
	+ Start Date. Format: dd-mm-yyyy HH:ii:ss
	+ End Date. Format: dd-mm-yyyy HH:ii:ss OR just type "now" for current time.
	+ Result Type. True: full time elapsed. False: short time elapsed
	
# Params Output
	+ Result() -> array([Class_Message], [Duration], [Business_Day]);
	
	Class_Message:
	[INVALID_DATE_RANGE] = Start Date > End Date.
	[DATE_RANGE_CALC_SUCCESS] = Successfully Executed
	
# Examples
	+ Create instance
	  $HitDays = new CountDays([db_class],[table_prefix]);
	+ Count Date
	  $HitDays->DayRange([Start_Date],[End_Date)->Result();
*/

class CountDays{

	var $startDay;
	var $endDay;
	
	function __construct($dbx,$tblp){
		$this->dbs = $dbx;
		$this->tblp = $tblp;
		$this->Errs = '';
		return $this;
	}
	
	function DayRange($startDay,$endDay="now",$full=false){
		
		if ( trim(strtolower($endDay)) == "now" ){
			$endDay = date("Y")."-".date("m")."-".date("d")." ".date("H").":".date("i").":".date("s");
			$endTime = time();
		}else{
			$endDay = $this->convertDates($endDay);
			list($bgtgl,$bgjam) = explode(" ",$endDay);
			list($ly,$lm,$ld) = explode("-",$bgtgl);
			list($lh,$lmm,$ls) = explode(":",$bgjam);
			$endTime = mktime($lh,$lmm,$ls,intval($lm),intval($ld),$ly);	
		}
		
		$startDay = $this->convertDates($startDay);
		list($bgtgl,$bgjam) = explode(" ",$startDay);
		list($ly,$lm,$ld) = explode("-",$bgtgl);
		list($lh,$lmm,$ls) = explode(":",$bgjam);
		$startTime = mktime($lh,$lmm,$ls,intval($lm),intval($ld),$ly);
		if ( $startTime > $endTime ){
			$this->Errs = "INVALID_DATE_RANGE";
		}else{
			/* 
			Penyesuaian 
			+ 7 jam
			*/
			$startTime += 25200;
			/* -------- */
			$this->Durations = $this->formatSisaWaktu('@'.$startTime,'@'.$endTime, $full);
		
			# Count Business Day
			$startyear = date('Y', $startTime);
			$endyear = date('Y', $endTime);
			$arrHolidays = array();
			$arrFixHolidays = array();
		
			$FixHols = "select extract(DAY from tgl) as tanggal, extract(MONTH from tgl) as bulan from ".$this->tblp."master_fixholiday order by month (tgl), day (tgl) asc";
			$rFixHols = $this->dbs->getQuery($FixHols);
			while( $rfh = $this->dbs->getAssoc($rFixHols)){
				array_push($arrFixHolidays, date("Y")."-".sprintf("%02d",$rfh['bulan'])."-".sprintf("%02d",$rfh['tanggal']));
			}
		
			for( $y=$startyear;$y<=$endyear;$y++){
				foreach ( $arrFixHolidays as $afh ){
					array_push($arrHolidays, $afh."-".sprintf("%02d",$rfh['bulan'])."-".sprintf("%02d",$rfh['tanggal']));
				}
			}
		
			$VarHols = "select tgl from ".$this->tblp."master_yearholiday where YEAR(tgl) >= ".$startyear." and YEAR(tgl) <= ".$endyear." order by tgl asc";
			$rVarHols = $this->dbs->getQuery($VarHols);
			while( $ryh = $this->dbs->getAssoc($rVarHols)){
				array_push($arrHolidays, $ryh['tgl']);
			}
		
			$this->BusinessDay = intval($this->getWorkingDays($startDay,$endDay,$arrHolidays));

$this->BusinessDayOnly = $this->BusinessDay;
 
			if ( $this->BusinessDay > 1 ){ $this->BusinessDay .= " days"; }else{ $this->BusinessDay .= " day"; }
			
			$this->Errs = "DATE_RANGE_CALC_SUCCESS";
			
		}
		return $this;
	}
	
	function Result(){
		return array($this->Errs, $this->Durations, $this->BusinessDay);
	}
function Result_OnlyNumber(){
	return array($this->Errs, $this->BusinessDayOnly);
}
	
	private function getWorkingDays($startDate,$endDate,$holidays){
	    $endDate = strtotime($endDate);
	    $startDate = strtotime($startDate);
	
	    $days = ($endDate - $startDate) / 86400 + 1;
	    $no_full_weeks = floor($days / 7);
	    $no_remaining_days = fmod($days, 7);
	    $the_first_day_of_week = date("N", $startDate);
	    $the_last_day_of_week = date("N", $endDate);

	    if ($the_first_day_of_week <= $the_last_day_of_week) {
	        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
	        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
	    } else {
	        if ($the_first_day_of_week == 7) {
	            $no_remaining_days--;
	            if ($the_last_day_of_week == 6) {
	                $no_remaining_days--;
	            }
	        } else {
	            $no_remaining_days -= 2;
	        }
	    }
	
		$workingDays = $no_full_weeks * 5;
	    if ($no_remaining_days > 0 ) { $workingDays += $no_remaining_days; }
	
	    foreach($holidays as $holiday){
	        $time_stamp=strtotime($holiday);
	        if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
	            $workingDays--;
	    }

	   	return $workingDays;
	}
	
	private function formatSisaWaktu($datetime, $endtime, $full = false) {
		# Note:
		#input('2013-05-01 00:22:35', '2013-05-01 00:23:45', false);
    	
		$now = new DateTime($endtime);
    		$ago = new DateTime($datetime);
    		$diff = $now->diff($ago);

    		$diff->w = floor($diff->d / 7);
    		$diff->d -= $diff->w * 7;
		

    		$string = array(
    	    		'y' => 'year',
    	    		'm' => 'month',
    	    		'w' => 'week',
    	    		'd' => 'day',
    	    		'h' => 'hour',
    	    		'i' => 'minute',
    	    		's' => 'second',
    		);
    		foreach ($string as $k => &$v) {
    	    		if ($diff->$k) {
    	        		$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
    	    		} else {
    	        		unset($string[$k]);
    	    		}
    		}

    		if (!$full) $string = array_slice($string, 0, 1);
    		return $string ? implode(', ', $string) . ' ago' : 'Just Now';
	}
	
	private function convertDates($date_to_be_process){
		list($Btgl,$BJam) = explode(" ",$date_to_be_process);
		list($tgl_01,$tgl_02,$tgl_03) = explode("-",$Btgl);
		$formated_date = $tgl_03."-".$tgl_02."-".$tgl_01;
		if ( $BJam ){ $formated_date .= " ".$BJam; }

		return $formated_date;
	}
} 

$HitDays = new CountDays($dbs,$tblp);
?>
