<?php
/*--------------------
File Manager Class
Barep, ggembulls@gmail.com

You can use this class and make modification as you need.
Please keep this header intact.

# Required
	+ GD2 Library
	+ finfo function

# Params Input,
	Construct:
	+ Array allowed mime file upload
	+ Array allower mime image file (Limited to JPG and PNG)

	File:
	+ ([form_file_name],[max_file_size], [target_directory]);
	+ [form_file_name]: field file form name
	+ [max_file_size]: maximum allowed file size
	+ [target_directory]: directory for new file to be saved

	Images:
	+ [desired_width], [desired_height], [type_process]
	+ [desired_width]: new width size
	+ [desired_height]: new height size
	+ set this desired 0 for automatically adjusted
	+ [type_process]: shrink -> shrink images with new desired width and/or height
					  crop -> crop images with new desired width and/or height
					  ori -> upload images as is.

	File Upload:
	+ No Param.

# Params Output
	Result() -> array([Class_Message], [File_Saved_Name], [File_Size], [Original_File_Name]);
	ResultIm() -> array([Class_Message], [File_Saved_Name], [Thumb_Name_File]);
	Class_Message:
	+ "FL_IM_SIZE_ERROR" = Insuficient size of images
	+ "FL_IM_SUCCESS" = Images process and upload succeeded
	+ "FL_UPLOAD_SUCCESS" = File upload succeeded,
	+ "FL_UPLOAD_FAILED" = Upload file failed,
	+ "FL_MAXSIZE_EXCEED" = Uploaded file size exceeded maximum file size,
	+ "FL_IM_PROCESS_ERRS" = Image process not supported,
	+ "FL_TYPE_ERRS" = File mime type not allowed,
	+ "FL_FILE_NF" = Uploaded file not found,
	+ "FL_FUNC_NF" = finfo function not installed,
	+ "FL_DIR_NF" => Target directory not found


# Examples
	+ Create instance
		$pm = new PicMan($Array_List_Allowed_File_Mime_Type, $Array_List_Allowed_Image_Mime_Type);
	+ Upload Images Without Thumbnail
		$pm->File([form_file_name],[max_file_size],[destination_directory])
		   ->Images("[desired_width]","[desired_height]","[type_process]")
		   ->Result();
	+ Upload Images With Thumbnail
		$pm->File([form_file_name],[max_file_size],[destination_directory])
		   ->Images("[desired_width]","[desired_height]","[type_process]")
		   ->Thumbs("[desired_width]","[desired_height]","[type_process]")
		   ->ResultIm();
	+ Upload File
		$pm->File([form_file_name],[max_file_size],[destination_directory])
		   ->FileUpload()
		   ->Result();

------------------- */

class PicMan
{

    var $imfile;
    var $maxfilesize;
    var $savedir;
    var $processtype;
    var $maxlength;
    var $savetype;

    var $fta;
    var $ita;
    var $Errs = "";
    var $file_name = "";
    var $file_name_ori = "";
    var $file_size;
    var $FileSaved;
    var $ThumbFileSaved;
    var $savefile;
    var $savefilethumbs;

    public function __construct($fta, $ita)
    {
        $this->fta = $fta;
        $this->ita = $ita;
    }

    private function Upload()
    {
        if ($this->file_name == "") {
            $file_name = preg_replace("/[^A-z0-9,.]/si", "_", $_FILES[$this->imfile]['name']);
            $file_name = md5(uniqid(microtime())) . $file_name;
            $this->file_name = $file_name;
        }
        $this->savefile = $_SESSION["ROOT_DIR"] . "/" . $this->savedir . "/" . $this->file_name;
        $this->savefilethumbs = $_SESSION["ROOT_DIR"] . "/" . $this->savedir . "/cp_" . $this->file_name;

        if (!move_uploaded_file($_FILES[$this->imfile]['tmp_name'], $this->savefile)) {
            $this->Errs = "FL_UPLOAD_FAILED";
        }

        # Copy file for thumbnail
        copy($this->savefile, $this->savefilethumbs);

        return $this;
    }

    public function Result()
    {
        if (is_file($this->savefilethumbs ?? '')) {
            unlink($this->savefilethumbs);
        }
        return array($this->Errs, $this->FileSaved, $this->file_size, $this->file_name_ori);
    }

    public function ResultIm()
    {
        if (is_file($this->savefilethumbs ?? '')) {
            unlink($this->savefilethumbs);
        }
        return array($this->Errs, $this->FileSaved, $this->ThumbFileSaved);
    }

    public function FileUpload($AllowedMimeType = '')
    {
        if (empty($this->Errs)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $_FILES[$this->imfile]['tmp_name']);
            finfo_close($finfo);
            $tf = "NOT_OK";

            if (empty($AllowedMimeType)) {
                $MimeCheck = $this->fta;
            } else {
                $MimeCheck = $AllowedMimeType;
            }
            foreach ($MimeCheck as $uft) {
                if ($uft == $mime) {
                    $tf = "OK";
                    break;
                }
            }


            if ($tf == "OK") {
                $this->Upload();
                if (empty($this->Errs)) {
                    $this->Errs = "FL_UPLOAD_SUCCESS";
                    $this->FileSaved = $this->file_name;
                }
            } else {
                $this->Errs = "FL_TYPE_ERRS";
            }
        }

        return $this;
    }


    private function ImShrink($maxwlength, $maxhlength, $mime, $copytipe = "NT")
    {
        $foto_prop = getimagesize($this->savefile);
        $currwidth = $foto_prop[0];
        $currheight = $foto_prop[1];

        if (!empty($maxwlength) and empty($maxhlength)) {
            if ($currwidth < $maxwlength) {
                $this->Errs = "FL_IM_SIZE_ERROR";
            } else {
                $percent = ($maxwlength * 100) / $currwidth;
                $nwidth = $maxwlength;
                $nheight = round(($percent * $currheight) / 100);
            }
        } elseif (empty($maxwlength) and !empty($maxhlength)) {
            if ($currheight < $maxhlength) {
                $this->Errs = "FL_IM_SIZE_ERROR";
            } else {
                $percent = ($maxhlength * 100) / $currheight;
                $nheight = $maxhlength;
                $nwidth = round(($percent * $currwidth) / 100);
            }
        } elseif (!empty($maxwlength) and !empty($maxhlength)) {
            if (($currwidth < $maxwlength) or ($currheight < $maxhlength)) {
                $this->Errs = "FL_IM_SIZE_ERROR";
            } else {
                $nwidth = $maxwlength;
                $nheight = $maxhlength;
            }
        } else {
            $this->Errs = "FL_IM_PROCESS_ERRS";
        }

        if (empty($this->Errs) or ($copytipe == "T" and $this->Errs == "FL_IM_SUCCESS")) {
            if ($copytipe == "NT") {
                $fd = @fopen($this->savefile, "r");
                $image_string = fread($fd, filesize($this->savefile));
                fclose($fd);
                $im = ImageCreateFromString($image_string);
                $newim_size = imagecreatetruecolor($nwidth, $nheight);

                if ($mime == "image/jpeg") {

                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagejpeg($newim_size, $this->savefile, 99);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->FileSaved = $this->file_name;
                } elseif ($mime == "image/png") {

                    imagealphablending($newim_size, false);
                    imagesavealpha($newim_size, true);
                    $transparent = imagecolorallocatealpha($newim_size, 255, 255, 255, 127);
                    imagefilledrectangle($newim_size, 0, 0, $nwidth, $nheight, $transparent);
                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagepng($newim_size, $this->savefile, 9);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->FileSaved = $this->file_name;
                } else {
                    $this->Errs = "FL_TYPE_ERRS";
                    if (is_file($this->savefile ?? '')) {
                        unlink($this->savefile);
                    }
                    if (is_file($this->savefilethumbs ?? '')) {
                        unlink($this->savefilethumbs);
                    }
                }

                # Copy file for thubnail
                copy($this->savefile, $this->savefilethumbs);
            } elseif ($copytipe == "T") {

                $fd = @fopen($this->savefile, "r");
                $image_string = fread($fd, filesize($this->savefile));
                fclose($fd);

                $im = ImageCreateFromString($image_string);
                $newim_size = imagecreatetruecolor($nwidth, $nheight);

                $thumb_file = $_SESSION["ROOT_DIR"] . "/" . $this->savedir . "/th_" . $this->file_name;

                if ($this->mime == "image/jpeg") {

                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagejpeg($newim_size, $thumb_file, 99);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->ThumbFileSaved = "th_" . $this->file_name;
                } elseif ($this->mime == "image/png") {

                    imagealphablending($newim_size, false);
                    imagesavealpha($newim_size, true);
                    $transparent = imagecolorallocatealpha($newim_size, 255, 255, 255, 127);
                    imagefilledrectangle($newim_size, 0, 0, $nwidth, $nheight, $transparent);
                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagepng($newim_size, $thumb_file, 9);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->ThumbFileSaved = "th_" . $this->file_name;
                } else {
                    $this->Errs = "FL_TYPE_ERRS";
                    if (is_file($this->savefile ?? '')) {
                        unlink($this->savefile);
                    }
                    if (is_file($this->savefilethumbs ?? '')) {
                        unlink($this->savefilethumbs);
                    }
                }
            }
        } else {
            if (is_file($this->savefile ?? '')) {
                unlink($this->savefile);
            }
            if (is_file($this->savefilethumbs ?? '')) {
                unlink($this->savefilethumbs);
            }
        }
    }

    private function ImCrop($maxwlength, $maxhlength, $mime, $copytipe = "NT")
    {
        $foto_prop = getimagesize($this->savefile);
        $currwidth = $foto_prop[0];
        $currheight = $foto_prop[1];

        if (($currwidth < $maxwlength) or ($currheight < $maxhlength)) {
            $this->Errs = "FL_IM_SIZE_ERROR";
        } else {
            $percent = ($maxwlength * 100) / $currwidth;
            $nwidth = $maxwlength;
            $nheight = round(($percent * $currheight) / 100);

            if ($nheight < $maxhlength) {
                $percent = ($maxhlength * 100) / $currheight;
                $nheight = $maxhlength;
                $nwidth = round(($percent * $currwidth) / 100);
            }

            if ($maxwlength == $maxhlength) {
                if ($nwidth > $nheight) {
                    $cr_y = 0;
                    $cr_x = ($nwidth - $nheight) / 2;
                    $cr_source_x = $maxwlength;
                    $cr_source_y = $maxhlength;
                } elseif ($nwidth < $nheight) {
                    $cr_y = ($nheight - $nwidth) / 2;
                    $cr_x = 0;
                    $cr_source_x = $maxwlength;
                    $cr_source_y = $maxhlength;
                } else {
                    $cr_y = 0;
                    $cr_x = 0;
                    $cr_source_x = $maxwlength;
                    $cr_source_y = $maxhlength;
                }
            } else {
                if ($nwidth == $maxwlength) {
                    $cr_y = ($nheight / 2) - ($maxhlength / 2);
                    $cr_x = 0;
                    $cr_source_x = $maxwlength;
                    $cr_source_y = $maxhlength;
                } elseif ($nheight == $maxhlength) {
                    $cr_y = 0;
                    $cr_x = ($nwidth / 2) - ($maxwlength / 2);
                    $cr_source_x = $maxwlength;
                    $cr_source_y = $maxhlength;
                } else {
                    $cr_y = 0;
                    $cr_x = 0;
                    $cr_source_x = $maxwlength;
                    $cr_source_y = $maxhlength;
                }
            }
        }


        if (empty($this->Errs) or ($copytipe == "T" and $this->Errs == "FL_IM_SUCCESS")) {
            if ($copytipe == "NT") {
                if ($mime == "image/jpeg") {

                    $fd = @fopen($this->savefile, "r");
                    $image_string = fread($fd, filesize($this->savefile));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($nwidth, $nheight);

                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagejpeg($newim_size, $this->savefile);
                    imagedestroy($newim_size);

                    $fd = @fopen($this->savefile, "r");
                    $image_string = fread($fd, filesize($this->savefile));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($maxwlength, $maxhlength);

                    imagecopyresampled($newim_size, $im, 0, 0, $cr_x, $cr_y, $maxwlength, $maxhlength, $cr_source_x, $cr_source_y);
                    imagejpeg($newim_size, $this->savefile, 99);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->FileSaved = $this->file_name;
                } elseif ($mime == "image/png") {

                    $fd = @fopen($this->savefile, "r");
                    $image_string = fread($fd, filesize($this->savefile));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($nwidth, $nheight);

                    imagealphablending($newim_size, false);
                    imagesavealpha($newim_size, true);
                    $transparent = imagecolorallocatealpha($newim_size, 255, 255, 255, 127);
                    imagefilledrectangle($newim_size, 0, 0, $nwidth, $nheight, $transparent);
                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagepng($newim_size, $this->savefile, 9);
                    imagedestroy($newim_size);

                    $fd = @fopen($this->savefile, "r");
                    $image_string = fread($fd, filesize($this->savefile));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($maxwlength, $maxhlength);

                    imagealphablending($newim_size, false);
                    imagesavealpha($newim_size, true);
                    $transparent = imagecolorallocatealpha($newim_size, 255, 255, 255, 127);
                    imagefilledrectangle($newim_size, 0, 0, $nwidth, $nheight, $transparent);
                    imagecopyresampled($newim_size, $im, 0, 0, $cr_x, $cr_y, $maxwlength, $maxhlength, $cr_source_x, $cr_source_y);
                    imagepng($newim_size, $this->savefile, 9);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->FileSaved = $this->file_name;
                } else {
                    $this->Errs = "FL_TYPE_ERRS";
                    if (is_file($this->savefile ?? '')) {
                        unlink($this->savefile);
                    }
                    if (is_file($this->savefilethumbs ?? '')) {
                        unlink($this->savefilethumbs);
                    }
                }

                # Copy file for thubnail
                copy($this->savefile, $this->savefilethumbs);
            } elseif ($copytipe == "T") {
                $thumb_file = $_SESSION["ROOT_DIR"] . "/" . $this->savedir . "/th_" . $this->file_name;
                if ($mime == "image/jpeg") {

                    $fd = @fopen($this->savefilethumbs, "r");
                    $image_string = fread($fd, filesize($this->savefilethumbs));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($nwidth, $nheight);

                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagejpeg($newim_size, $thumb_file);
                    imagedestroy($newim_size);

                    $fd = @fopen($thumb_file, "r");
                    $image_string = fread($fd, filesize($thumb_file));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($maxwlength, $maxhlength);

                    imagecopyresampled($newim_size, $im, 0, 0, $cr_x, $cr_y, $maxwlength, $maxhlength, $cr_source_x, $cr_source_y);
                    imagejpeg($newim_size, $thumb_file, 99);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->ThumbFileSaved = "th_" . $this->file_name;
                } elseif ($mime == "image/png") {

                    $fd = @fopen($this->savefilethumbs, "r");
                    $image_string = fread($fd, filesize($this->savefilethumbs));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($nwidth, $nheight);

                    imagealphablending($newim_size, false);
                    imagesavealpha($newim_size, true);
                    $transparent = imagecolorallocatealpha($newim_size, 255, 255, 255, 127);
                    imagefilledrectangle($newim_size, 0, 0, $nwidth, $nheight, $transparent);
                    imagecopyresampled($newim_size, $im, 0, 0, 0, 0, $nwidth, $nheight, $currwidth, $currheight);
                    imagepng($newim_size, $thumb_file, 9);
                    imagedestroy($newim_size);

                    $fd = @fopen($thumb_file, "r");
                    $image_string = fread($fd, filesize($thumb_file));
                    fclose($fd);
                    $im = ImageCreateFromString($image_string);
                    $newim_size = imagecreatetruecolor($maxwlength, $maxhlength);

                    imagealphablending($newim_size, false);
                    imagesavealpha($newim_size, true);
                    $transparent = imagecolorallocatealpha($newim_size, 255, 255, 255, 127);
                    imagefilledrectangle($newim_size, 0, 0, $nwidth, $nheight, $transparent);
                    imagecopyresampled($newim_size, $im, 0, 0, $cr_x, $cr_y, $maxwlength, $maxhlength, $cr_source_x, $cr_source_y);
                    imagepng($newim_size, $thumb_file, 9);
                    imagedestroy($newim_size);

                    $this->Errs = "FL_IM_SUCCESS";
                    $this->ThumbFileSaved = "th_" . $this->file_name;
                } else {
                    $this->Errs = "FL_TYPE_ERRS";
                    if (is_file($this->savefile ?? '')) {
                        unlink($this->savefile);
                    }
                    if (is_file($this->savefilethumbs ?? '')) {
                        unlink($this->savefilethumbs);
                    }
                }
            }
        } else {
            if (is_file($this->savefile ?? '')) {
                unlink($this->savefile);
            }
            if (is_file($this->savefilethumbs ?? '')) {
                unlink($this->savefilethumbs);
            }
        }
    }

    function File($imfile = "", $maxfilesize = "1024", $savedir = "temp", $filename = "")
    {
        $this->Errs = "";
        if (!is_dir($_SESSION["ROOT_DIR"] . "/" . $savedir) or !isset($_SESSION["ROOT_DIR"])) {
            $this->Errs = "FL_DIR_NF";
        } else {
            if (empty($imfile) or empty($_FILES[$imfile]['name'])) {
                $this->Errs = "FL_FILE_NF";
            } else {
                if ($_FILES[$imfile]['size'] > ($maxfilesize * 1024)) {
                    $this->Errs = "FL_MAXSIZE_EXCEED";
                } else {
                    if (!function_exists('finfo_file')) {
                        $this->Errs = "FL_FUNC_NF";
                    } else {
                        $this->imfile = $imfile;
                        $this->savedir = $savedir;
                        $this->file_size = $_FILES[$imfile]['size'];
                        $this->file_name_ori = $_FILES[$imfile]['name'];
                        $this->file_name = $filename;
                    }
                }
            }
        }
        return $this;
    }

    function Images($maxwlength = "150", $maxhlength = "150", $savetype = "crop")
    {
        if (empty($this->Errs)) {

            $maxwlength = preg_replace("/\D/", "", $maxwlength);
            $maxhlength = preg_replace("/\D/", "", $maxhlength);
            $savetype = preg_replace("/\W/", "", $savetype);

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $_FILES[$this->imfile]['tmp_name']);
            finfo_close($finfo);
            $tf = "NOT_OK";

            foreach ($this->ita as $uft) {
                if ($uft == $mime) {
                    $tf = "OK";
                    break;
                }
            }

            if ($tf == "OK") {
                $this->Upload();

                if (empty($this->Errs)) {
                    # For Thumbnail purposes
                    $this->mime = $mime;
                    # ---------------------------------

                    if ($savetype == "shrink") {
                        $this->ImShrink($maxwlength, $maxhlength, $mime, "NT");
                    } elseif ($savetype == "crop") {
                        $this->ImCrop($maxwlength, $maxhlength, $mime, "NT");
                    } elseif ($savetype == "ori") {
                        $this->Errs = "FL_IM_SUCCESS";
                        $this->FileSaved = $this->file_name;
                    } else {
                        $this->Errs = "FL_IM_PROCESS_ERRS";
                        if (is_file($this->savefile ?? '')) {
                            unlink($this->savefile);
                        }
                    }
                }
            } else {
                $this->Errs = "FL_TYPE_ERRS";
            }
        }
        return $this;
    }

    function Thumbs($tmaxwlength = "150", $tmaxhlength = "150", $tsavetype = "shrink")
    {
        if ($this->Errs == "FL_IM_SUCCESS") {
            $tmaxwlength = preg_replace("/\D/", "", $tmaxwlength);
            $tmaxhlength = preg_replace("/\D/", "", $tmaxhlength);
            $tsavetype = preg_replace("/\W/", "", $tsavetype);

            $foto_prop = getimagesize($this->savefilethumbs);
            $currwidth = $foto_prop[0];
            $currheight = $foto_prop[1];
            if ($tmaxwlength > $currwidth || $tmaxhlength > $currheight) {
                $this->Errs = "FL_IM_THUMB_PROCESS_ERRS";
                if (is_file($this->savefile ?? '')) {
                    unlink($this->savefile);
                }
            } elseif ($tsavetype == "shrink") {
                $this->ImShrink($tmaxwlength, $tmaxhlength, $this->mime, "T");
            } elseif ($tsavetype == "crop") {
                $this->ImCrop($tmaxwlength, $tmaxhlength, $this->mime, "T");
            } else {
                $this->Errs = "FL_IM_THUMB_PROCESS_ERRS";
                if (is_file($this->savefile ?? '')) {
                    unlink($this->savefile);
                }
            }
        }

        return $this;
    }
}

$pm = new PicMan($upfiletype, $upimtype);
