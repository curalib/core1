<?php
# -----------------------------------------------
# Advanced Setting.
# Change it on your own risk.
# Make sure you knew what are you doing or else
# The Apps may crash
# -----------------------------------------------



# System Menu Icon
# Toggle to show icon menu or not
# Strongly recomended to be activated
define("SYSMNICON", false);


# Themes
define("THEMES", "Default");

# Timeset
define("TIMESET", "Asia/Jakarta");

# Base URL
if (!defined('BASEURL')) {
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
    define("BASEURL", $protocol . '://' . $_SERVER['HTTP_HOST'] . "/");
}

# Session Directory
# Change to 1 (number one) to use default server.
define("SESS_DIR", '1');

# Domain Check.
# Listing allowed domain
$dom_array = array(
    // "localhost", # Default vals
    // "127.0.0.1", # Devault vals
    "erp.izin.co.id",
    "core1.test",
);

# Check Logout Options
# All parameters are in seconds
define("CHECK_LOGOUT", 300);
define("LOGOUT_INCREASING", 120);
define("CLEAR_SESSION", true);

# Limiting Login Attempt Options
# All parameters are in seconds
define("WRONG_PASS", 3);
define("TEMP_LOCK", 300);
define("LOCK_INCREASING", 120);
define("LOCK_RESET", true);

# Ip Banned Options
define("IP_BANN", 10);
define("IP_CHECK_PERIOD", 300);

# Captcha Options
define("CPTCH_LENG", 5);
define("CPTCH_WD", 75);
define("CPTCH_HT", 27);
define("CPT_FS", 18);

# Session Time Out Options
define("IDLE", 100);

# Pass & UID
define("UIDL", 35);
define("PWDL", 20);
define("MIN_PWD", 3);
define("MIN_UID", 3);

# Paging Parameter
define("PAGE_ITEM", 100);
define("PAGE_LIST", 20);

# File Size
# Dalam Kb
# Default: 1024 Kb (1 Mb)
#define("FILESIZES", 5120);
define("ATTACHMENTFILESIZES", 10240);
define("FILESIZES", 30720);

# Warna
# Hanya untuk menyatukan penulisan warna
$Warna = array(
    "Hitam", "Putih", "Merah", "Hijau", "Oranye", "Perak", "Kuning", "Abu-abu", "Biru", "Merah Muda", "Cokelat", "Ungu"
);

# File Tipe
# Accepted file tipe:
# doc, docx, xls, xlsx, ppt, pptx, pdf, jpg, png, vsd, odt, odp, ods
$upfiletype = array(
    "application/vnd.ms-excel",
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    "image/jpeg",
    "image/png",
    "application/pdf",
    "application/x-rar",
    "application/x-rar-compressed",
    "application/zip",
    "application/x-zip-compressed",
    "multipart/x-zip",
    "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    "application/vnd.ms-powerpoint",
    "application/vnd.ms-office",
    "application/msword",
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    "application/vnd.oasis.opendocument.text",
    "application/vnd.oasis.opendocument.spreadsheet",
    "application/vnd.oasis.opendocument.presentation"
);

$upimtype = array(
    "image/jpeg",
    "image/png"
);

# Setting for number form fields
$formSize = 20;
$formMaxVal = "999999999999999.99";
$formMaxValMarkUp = "99999999.99";
