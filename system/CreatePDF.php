<?php

require_once 'vendor/autoload.php';
//$dompdf = new \Dompdf\Dompdf(["isRemoteEnabled" => true]);
$tmp = sys_get_temp_dir();

$dompdf = new \Dompdf\Dompdf([
    'logOutputFile' => '',
    // authorize DomPdf to download fonts and other Internet assets
    'isRemoteEnabled' => true,
    // all directories must exist and not end with /
    'fontDir' => $tmp,
    'fontCache' => $tmp,
    'tempDir' => $tmp,
    'chroot' => $tmp,
]);
