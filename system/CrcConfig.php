<?php

if (!defined('BASEURL')) {
    # Base URL
    $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http';
    if (isset($_SERVER['HTTP_HOST'])) {
        define("BASEURL", $protocol . '://' . $_SERVER['HTTP_HOST'] . "/");
    } else {
        define('BASEURL', 'http://core1.test/');
    }
}

defined("PAYURL") or define("PAYURL", "https://pay.izin.co.id");
defined("CLIENTURL") or define("CLIENTURL", "https://tracking.izin.co.id");

/* --- Crucial Setting --- */
define('DBC_MHOST', "localhost"); # Db Host
define('DBC_MUSER', "izinapps"); # Db User
define('DBC_MPWD', "&)xq1YGX]n(LDYg"); # Db Pass
define('DBC_MSDB', "izin_1"); # Db Name
define('DBC_MSPORT', 3306); # Db Port
define("DBC_MSTYPE", "PDO"); # PDO (recomended) or MySQLi
define("DBC_PREF", "izin"); # Prefix. Alphanumerik

# Environtment
# Set Application Environtment.
# Development, Testing or Production.
// define("ENVIRONMENT", "Development");
defined('ENVIRONMENT') or define('ENVIRONMENT', 'Production');

# DON'T CHANGE IT WHEN IT'VE BEEN SET!!
define("LOGINPAGE", "LoginIndex.php");
define("INNER_PAGE", "Inner.php");
define("COPYRPAGE", "Copy.php");
define("INSPAGE", "Ins.php");
define("AJAX_ERR", "ErrAjax.php");

# Upload directory.
define("UPDIR", "unggah");
define("UPDIR_2", "client.izin.co.id/writable/uploads");

# Privilege
$priv = array(
    1 => 1, #Super User
    2 => 2, #Administrator
    3 => 3, #User
);

# Password Hash
$passhashoptions = [
    'cost' => 12,
    //'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
];

# Number
$comma_separator = ".";
$comma_separator_user = ".";

# Activate WA Blast
define("WA_BLAST", true);
defined("WA_BLAST_HOST") or define("WA_BLAST_HOST", "https://solo.wablas.com");
defined("WA_BLAST_AUTH") or define("WA_BLAST_AUTH", "U8FO7CN5zbVFqLm9loIagw8PDSKddWtCEMRnPbKnUSuduOhQxwvh2fd9qwzG8AaX.wvOzxaCO");
/* ------------------------------------------ */

$UserPos = array(
    0 => 0, # Super User
    1 => 1, # BOD
    2 => 2, # Manager
    3 => 3, #Administrator
    4 => 6038, # Sales
    5 => 33 # Finance
);

define("AVATAR_WD", 250);

$CompanyNums = array(
    "izin" => "PT. Izin",
    "ijin" => "PT. Ijin"
);

define("SRC_ITEM", array(
    "WA" => "Whatsapp",
    "EM" => "Email",
    "CC" => "Call Center",
    "RO" => "Referral Virtual Office",
    "RU" => "Referral Unionspace",
    "IN" => "Instagram",
    "FB" => "Facebook",
    "WI" => "Walk In",
    "LC" => "Live Chat",
    "TT" => "TIK TOK",
    "CB" => "Chatbot",
    "AG" => "Agent",
    "RC" => "Referral Client",
    "SE" => "SEM (Email)",
    "SW" => "SEM (Whatsapp)",
    "BL" => "Blog"
));

define("LEAD_STATUS", array(
    'AC' => 'About to Contact',
    'WR' => 'Waiting for Response',
    'IN' => 'Interacted',
    'PS' => 'Proposal Sent',
    'IS' => 'Invoice Sent',
    'CL' => 'Closed',
    'LO' => 'Lost'
));

define("INDUSTRY", array(
    "D" => "Perdagangan",
    "I" => "Industri",
    "P" => "Properti",
    "J" => "Jasa",
    "K" => "Kontraktor",
    "IT" => "IT",
    "KS" => "Konsultan",
    "B" => "Perbengkelan",
    "PAR" => "Pariwisata",
    "KES" => "Kesehatan",
    "EDU" => "Edukasi",
    "AGR" => "Agrikultur",
    "IKN" => "Perikanan",
    "ESDM" => "ESDM",
    "TRA" => "Transportasi",
    "IKL" => "Periklanan"
));

define("JENIS_PER", array(
    "K" => "Kecil",
    "M" => "Menengah",
    "B" => "Besar"
));

define("POS_PENGURUS", array(
    "DIRUT" => "President Director",
    "DIR" => "Director",
    //"KOMUT" => "President Commissioner",
    //"KOM" => "Commissioner",
    "PER_AKTIF" => "Persero Aktif"
    //"PER_PASIF" => "Persero Pasif"
));

# Status Badge
define("BADGE_STATUS", array(
    "0" => "New",
    "1" => "On Going",
    "2" => "Waiting",
    "3" => "Done",
));

define("KAT_KBLI", array(
    "01111-03263" => "Kategori A",
    "05101-09900" => "Kategori B",
    "10110-33200" => "Kategori C",
    "35101-35302" => "Kategori D",
    "36001-39000" => "Kategori E",
    "41011-43909" => "Kategori F",
    "45101-47999" => "Kategori G",
    "49110-53202" => "Kategori H",
    "55111-56306" => "Kategori I",
    "58110-63990" => "Kategori J",
    "64110-66300" => "Kategori K",
    "68110-68200" => "Kategori L",
    "69101-75000" => "Kategori M",
    "77100-82990" => "Kategori N",
    "84111-84300" => "Kategori O",
    "85111-85500" => "Kategori P",
    "86101-88902" => "Kategori Q",
    "90001-93299" => "Kategori R",
    "94110-96999" => "Kategori S",
    "97000-98200" => "Kategori T",
    "99000" => "Kategori U"
));


define("REASON_TITLE", array(
    "all" => "Any",
    "1" => "Dokumen Selesai",
    "2" => "Menunggu update dokumen NPWP klien",
    "3" => "Kendala sistem OSS",
    "4" => "Kendala sistem PTSP",
    "5" => "Klien belum melengkapi dokumen",
    "6" => "Kendala pengiriman NPWP/SKT dari KPP",
    "7" => "Menunggu email & password perusahaan dari klien",
    "8" => "Others",
));

/* --- Email Setting --- */
if (BASEURL == "http://core1.test/") {
    define("EMAIL_HOST", "sandbox.smtp.mailtrap.io");
    define("EMAIL_SMTPDEBUG", 0);
    define("EMAIL_SMTPAUTH", true);
    define("EMAIL_SMTPSECURE", "tls");
    define("EMAIL_PORT", 587);
    define("EMAIL_USERNAME", "2c74a339fe6d14");
    define("EMAIL_PASSWORD", "b7d361eb86f2f5");
    define("EMAIL_SETFROM_MAIL", "info@izin.co.id");
    define("EMAIL_SETFROM_NAME", "Customer Service izin.co.id");
    define("EMAIL_CC", "team@izin.co.id");
    define("EMAIL_ISHTML", true);
    define("WA_ADMIN", '6281932156898');
} else {
    define("EMAIL_HOST", "smtp.gmail.com");
    define("EMAIL_SMTPDEBUG", 0);
    define("EMAIL_SMTPAUTH", true);
    define("EMAIL_SMTPSECURE", "ssl");
    define("EMAIL_PORT", 465);
    define("EMAIL_USERNAME", "info@izin.co.id");
    define("EMAIL_PASSWORD", "zaq!xsw@");
    define("EMAIL_SETFROM_MAIL", "info@izin.co.id");
    define("EMAIL_SETFROM_NAME", "Customer Service izin.co.id");
    define("EMAIL_CC", "team@izin.co.id");
    define("EMAIL_ISHTML", true);
    define("WA_ADMIN", '6282299980011');
}

/* For Invoicing System */
define("BANK_ACC", array(
    "6560-243-888" => "Bank Central Asia|PT WAHANA RAHMAT NUSA|6560-243-888|CENAIDJA|Active|",
    "6560-828-282" => "Bank Central Asia|PT IJIN USAHA INDONESIA|6560-828-282|CENAIDJA|Inactive|", // sudah ada spaze
    "6560-907-778" => "Bank Central Asia|PT WAHANA IJIN NUSANTARA|6560-907-778|CENAIDJA|Inactive|BUSINESS PARK KEBON JERUK", // deleted
    "6560-821-199" => "Bank Central Asia|PT MITRA SOLUSI HUKUM|6560-821-199|CENAIDJA|Inactive|BUSINESS PARK KEBON JERUK", // deleted
    "6561-912-022" => "Bank Central Asia|PT WAHANA IZIN INDONESIA|6561-912-022|CENAIDJA|Active|",
    "6561-582-022" => "Bank Central Asia|PT SAHABAT USAHA ANDA|6561-582-022|CENAIDJA|Active|",
    "6561-252-022" => "Bank Central Asia|PT INVESTASI INDO ASIA|6561-252-022|CENAIDJA|Active|KCP Kebon Jeruk Raya",
));

define("VABANK", array(
    "6560-243-888" => "11791",
    "6560-828-282" => "11794",
    "6560-907-778" => "12280",
    "6560-821-199" => "13379",
    "6561-912-022" => "00000",
    "6561-582-022" => "00000",
    "6561-252-022" => "00000",
));

define("BANK_TO_COMPANY", [
    "6560-243-888" => [
        'name' => 'PT WAHANA RAHMAT NUSA',
        'tax_id' => '03.163.024.7-086.000',
        'address' => 'Kencana Tower, Level Mezzanine, Jl Raya Meruya Ilir No 88 Business Park Kebon Jeruk RT/RW 001/005, Kec. Kembangan, Kel. Meruya Utara, Kota Administrasi Jakarta Barat, DKI Jakarta, 11620',
    ],
    "6560-828-282" => [
        'name' => 'PT IJIN USAHA INDONESIA',
        'tax_id' => '86.081.497.9-067.000',
        'address' => 'Menara Rajawali Level 7-1, Jl. Dr. Ide Anak Gde Agung Lot. 5.1 Kawasan Mega Kuningan Kel. Kuningan Timur Kec. Setiabudi, Kota Administrasi Jakarta Selatan, DKI Jakarta 12950',
    ],
    "6560-907-778" => [
        'name' => 'PT WAHANA IJIN NUSANTARA',
        'tax_id' => '93.508.017.6-063.000',
        'address' => 'Centennial Tower, Lantai 29, Kav. 24-25 Unit D-F, Jl. Jenderal Gatot Subroto No.27 RT/RW.002/002, Kel. Karet Semanggi, Kec. Setiabudi, Kota Administrasi Jakarta Selatan, DKI Jakarta 12930',
    ],
    "6560-821-199" => [
        'name' => 'PT MITRA SOLUSI HUKUM',
        'tax_id' => '94.394.359.7-063.000',
        'address' => 'Centennial Tower, Lantai 29, Kav. 24-25 Unit D-F, Jl. Jenderal Gatot Subroto No.27 RT/RW.002/002, Kel. Karet Semanggi, Kec. Setiabudi, Kota Administrasi Jakarta Selatan, DKI Jakarta 12930',
    ],
    "6561-912-022" => [
        'name' => 'PT WAHANA IZIN INDONESIA',
        'tax_id' => '62.481.586.6-451.000',
        'address' => 'MyRepublic Plaza, Wing A, Zona 6, Green Office Sampora, Kecamatan Cisauk, Kabupaten Tangerang, Banten 15345',
    ],
    "6561-582-022" => [
        'name' => 'PT SAHABAT USAHA ANDA',
        'tax_id' => '62.701.189.3-451.000',
        'address' => 'MyRepublic Plaza, Wing A, Zona 6, Green Office Sampora, Kecamatan Cisauk, Kabupaten Tangerang, Banten 15345',
    ],
    "6561-252-022" => [
        'name' => 'PT INVESTASI INDO ASIA',
        'tax_id' => '62.845.310.2-012.000',
        'address' => 'District 8, Treasury Tower Lantai 6 Unit F, Senayan, Kebayoran Baru, Kota Administrasi Jakarta Selatan, DKI Jakarta',
    ],
]);

define("INVNUM", 63);

define("FIN_USR", array(
    /*	"verena@izin.co.id" => "Verena",*/
    "ria@izin.co.id" => "Ria",
    // "wulan@izin.co.id" => "Wulan", // diganti fitria
    "fitria@izin.co.id" => "Fitria",
));

define("HAKI", array(
    "0" => "Pengajuan Permohonan",
    "1" => "Link Progress",
    "2" => "Publikasi",
    "3" => "Pemeriksaan Substantif",
    "4" => "Sertifikat Selesai"
));

define("TAX", array(
    "0" => "EFIN", // no reminder, use sendNow
    "1" => "SPPKP", // no reminder, use sendNow
    "2" => "SK PP 55", // no reminder, use sendNow
    "3" => "Laporan Pajak Bulanan",
    "4" => "Laporan Pajak Tahunan", // no reminder, use sendNow
    "5" => "Pembukuan Bulanan"
));

define("BAST", array(
    "0" => "Attachment 1"
));

# Activate Xendit
define("XND_PYMT", true);
