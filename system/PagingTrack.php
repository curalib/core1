<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <?php
                    $trckMenuShow = '';
                    for ($mm = $rcmdet['tingkat']; $mm >= 1; $mm--) {
                        if ($mm == $rcmdet['tingkat']) {
                            $MenuNama[$mm] = ${"lang_" . $rcmdet['nama']};
                            $MenuLink[$mm] = SplitKeys($rcmdet['tgl']);
                            $MenuTipe[$mm] = $rcmdet['tipe_menu'];
                            $MenuIdParent[$mm] = $rcmdet['id_parent'];
                        } else {
                            $cNextMenu = "select nama, tgl, id_parent, tipe_menu from " . $tblp . "sys_menu where id = '" . $MenuIdParent[$mm + 1] . "'";
                            $rcNextMenu = $dbs->getArr($cNextMenu);
                            $MenuNama[$mm] = ${"lang_" . $rcNextMenu['nama']};
                            $MenuLink[$mm] = SplitKeys($rcNextMenu['tgl']);
                            $MenuTipe[$mm] = $rcNextMenu['tipe_menu'];
                            $MenuIdParent[$mm] = $rcNextMenu['id_parent'];
                        }
                    }

                    # Paging For Add/Listing if available.
                    $ma = $rcmdet['tingkat'];
                    if ($rcmdet['navbar'] == 'Y') {
                        $ma++;
                        if (!isset($_SESSION["AddItm"])) {
                            if (!isset($_SESSION["Edit_ID"])) {
                                $MenuNama[$ma] = $lang_997 . " " . ${"lang_" . $rcmdet['nama']};
                                $MenuTipe[$ma] = "ITEM";
                                $Butts = "
									<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
									<input type=\"hidden\" name=\"task\">
									</form>
									" .
                                    ButtonsCommon("commonbuttons", THEMES, "Add_i", $lang_996 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Add"], "Listing" . $rcmdet['nama'], "", "right");
                            } else {
                                $MenuNama[$ma] = $lang_995 . " " . ${"lang_" . $rcmdet['nama']};
                                $MenuTipe[$ma] = "ITEM";
                                $Butts = "
									<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
									<input type=\"hidden\" name=\"task\">
									</form>
									" .
                                    ButtonsCommon("commonbuttons", THEMES, "listing", $lang_997 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right") .
                                    ButtonsCommon("commonbuttons", THEMES, "Add_i", $lang_996 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Add"], "Listing" . $rcmdet['nama'], "", "right");
                            }
                        } else {
                            $MenuNama[$ma] = $lang_996 . " " . ${"lang_" . $rcmdet['nama']};
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", $lang_997 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right");
                        }
                    } elseif ($rcmdet['navbar'] == "K") {
                        $ma++;
                        if (!isset($_SESSION["FixHoliday"])) {
                            $MenuNama[$ma] = $lang_6053;
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Fixed_Holiday\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", $lang_6052, $lang_6052 . "_" . $_SESSION["butts_Reset"], "Fixed_Holiday", "", "right");
                        } else {
                            $MenuNama[$ma] = $lang_6052;
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Fixed_Holiday\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", $lang_6053, $lang_6053 . "_" . $_SESSION["butts_Reset"], "Fixed_Holiday", "", "right");
                        }
                    } elseif ($rcmdet['navbar'] == "A") {
                        $ma++;
                        if (isset($_SESSION["LeadDets"]) || isset($_SESSION["RenewalLeadDets"])) {
                            $MenuNama[$ma] = $lang_2059;
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"DtList\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", "Database Listing", $lang_2059 . "_" . $_SESSION["butts_Reset"], "DtList", "", "right");
                        } else {
                            $MenuNama[$ma] = $lang_2058;
                            $MenuTipe[$ma] = "ITEM";
                        }
                    } elseif ($rcmdet['navbar'] == 'L') {
                        $ma++;
                        if (isset($_SESSION["AddItm"])) {
                            $MenuNama[$ma] = $lang_996 . " " . ${"lang_" . $rcmdet['nama']};
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", $lang_997 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right");
                        } elseif (isset($_SESSION["LeadDets"])) {
                            $MenuNama[$ma] = $lang_2037 . " " . ${"lang_" . $rcmdet['nama']};
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", $lang_997 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right") . " " .
                                ButtonsCommon("commonbuttons", THEMES, "Add_i", $lang_996 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Add"], "Listing" . $rcmdet['nama'], "", "right");
                        } elseif (isset($_SESSION["LeadChStat"])) {
                            $MenuNama[$ma] = $lang_2038 . " " . ${"lang_" . $rcmdet['nama']};
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", $lang_997 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right");
                        } else {
                            $MenuNama[$ma] = $lang_997 . " " . ${"lang_" . $rcmdet['nama']};
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>
								" .
                                ButtonsCommon("commonbuttons", THEMES, "Add_i", $lang_996 . " " . ${"lang_" . $rcmdet['nama']}, $rcmdet['nama'] . "_" . $_SESSION["butts_Add"], "Listing" . $rcmdet['nama'], "", "right");
                        }
                    } elseif ($rcmdet['navbar'] == 'T') {
                        if (isset($_SESSION["Edit_ID"])) {
                            $MenuNama[$ma] = "Detail " . ${"lang_" . $rcmdet['nama']};
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>
								" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", "Back to Listing", $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right");
                        }

                        if (isset($_SESSION["AGREEMENT"])) {
                            $MenuNama[$ma] = "Form Agreement";
                            $MenuTipe[$ma] = "ITEM";
                            $Butts = "
								<form action=\"\" method=\"post\" name=\"Listing" . $rcmdet['nama'] . "\">
								<input type=\"hidden\" name=\"task\">
								</form>
								" .
                                ButtonsCommon("commonbuttons", THEMES, "listing", "Back to Listing", $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"], "Listing" . $rcmdet['nama'], "", "right");
                        }
                    }

                    for ($ms = 1; $ms <= $ma; $ms++) {
                        if ($ms == $ma) {
                            $MenuTrckCss = "active";
                        } else {
                            $MenuTrckCss = '';
                        }
                        if ($MenuTipe[$ms] == "MENU") {
                            $trckMenuShow .= '<li class="breadcrumb-item ' . $MenuTrckCss . '">' . $MenuNama[$ms] . '</li>';
                        } elseif ($MenuTipe[$ms] == "ITEM" and $ms != $ma) {
                            $trckMenuShow .= '<li class="breadcrumb-item ' . $MenuTrckCss . '"><a href="' . BASEURL . $MenuLink[$ms] . '/0/0.html">' . $MenuNama[$ms] . '</a></li>';
                        } elseif ($MenuTipe[$ms] == "ITEM" and $ms == $ma) {
                            $trckMenuShow .= '<li class="breadcrumb-item ' . $MenuTrckCss . '">' . $MenuNama[$ms] . '</li>';
                        }
                    }
                    echo $trckMenuShow;
                    ?>
                </ol>
            </nav>
            <h1 class="m-0"><?php echo $MenuNama[$ma] ?></h1>
        </div>
        <?php echo $Butts ?>
    </div>
</div>