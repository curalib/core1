<?php

//function ChPassUid($in,$minlen)

//function uniqids($ins){
//function UrlExist($url)
//function ManSession($acts)
//function CheckUrl($using,$dom_array,$steps)
//function CheckIdle($dbtime,$IdleLimit)
//function TimeRemain($dbtime,$AddingTime)
//function microtime_float()
//function formatPeriod($duration)
//function formatSisaWaktu($sisa)
//function SplitKeys($ItemKeys)
//function ExpandKeys($ItemKeys)
//function ButtonsDashboard($cssname="commonbuttons",$buttthemes="Default",$icons="",$name="",$act="",$forms="",$konfirmasi="",$float="left")
//function ButtonsCommon($cssname="commonbuttons",$buttthemes="Default",$icons="",$name="",$act="",$forms="",$konfirmasi="",$float="left")
//function ButtonsPKBAdd($cssname="commonbuttons",$buttthemes="Default",$icons="",$name="",$act="",$forms="",$konfirmasi="",$float="left")
//function ButtonsPaging($cssname="pagingbuttonsactive",$buttthemes="Default",$icons="",$name="",$act="",$forms="",$konfirmasi="",$float="left")
//function SaveNum($num)
//function c_curr($num,$comma_separator,$comma_separator_user,$min_sign="mark")
//function deldir($dir)
//function convertDate($date_to_be_process)
//function c_date ($in_date,$co_date,$co_month,$list_bln,$list_bln_short)
//function getIP()
//function Purefy($str)
// echo "s";
function WaBlast($tlp, $pesan)
{
    if (strtolower(ENVIRONMENT) == "development") {
        $tlp = '6281932156898';
    }

    $returned_result = array();
    if (empty($tlp) || empty($pesan)) {
        $returned_result[0] = false;
        $returned_result[1] = '{"status": false,"message":"PIC contact number is empty!"}';
    } else {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: " . WA_BLAST_AUTH));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query([
            'phone' => $tlp,
            'message' => $pesan
        ]));
        curl_setopt($curl, CURLOPT_URL, WA_BLAST_HOST . "/api/send-message");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($curl);
        curl_close($curl);

        $returned_result[0] = true;
        $returned_result[1] = $result;
    }

    return $returned_result;
}

function WA_message(array $results)
{
    require_once "system/CreateLogger.php";

    $data = json_decode($results[1], true);

    $status = $data['status'];
    $statusMessage = $data['message'];

    if ($status) {
        $quota = $data['data']['quota'];
        $dataMessages = $data['data']['messages'];

        $successPhones = [];
        for ($i = 0; $i < count($dataMessages); $i++) {
            $id = $dataMessages[$i]['id'];
            $phone = $dataMessages[$i]['phone'];
            $status = $dataMessages[$i]['status'];
            $logger->withName('wablas')->info($statusMessage, ['quota' => $quota, 'phone' => $phone, 'id' => $id, 'status' => $status]);

            if ($status == "pending") {
                $successPhones[] = $phone;
            }
        }

        if (count($successPhones) > 0) $message = "WA message sent (to: " . implode(', ', $successPhones) . ")";
    } else {
        $message = "Fail sending WA message (problem: $statusMessage)";
        $logger->withName('wablas')->error($statusMessage);
    }

    return $message;
}


function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
{
    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);
    while ($current <= $last) {
        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }
    return $dates;
}


function show_badge($int_status, $arr_badge)
{
    switch ($int_status) {
        case 0:
            //New
            $return_show_badge = '<span class="badge badge-warning">' . $arr_badge[$int_status] . '</span>';
            break;
        case 1:
            //On Going
            $return_show_badge = '<span class="badge badge-info">' . $arr_badge[$int_status] . '</span>';
            break;
        case 2:
            //waiting
            $return_show_badge = '<span class="badge badge-secondary">' . $arr_badge[$int_status] . '</span>';
            break;
        case 3:
            //done
            $return_show_badge = '<span class="badge badge-success">' . $arr_badge[$int_status] . '</span>';
            break;
        default:
            $return_show_badge = '<span class="badge badge-light">' . $arr_badge[$int_status] . '</span>';
    }

    return $return_show_badge;
}


function ShowImage($filedir, $imtoshow)
{
    //$filedir= UPDIR."/avatar/";
    if (is_file($filedir . $imtoshow)) {
        $filename = $filedir . $imtoshow;
    } else {
        $filename = $filedir . "no_pic.png";
    }
    $im = file_get_contents($filename);
    return $im;
}

function ChkEnv()
{
    $LoginErrMess = '';
    $LoginErrs = '';
    if (strtolower(ENVIRONMENT) == "development") {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL & ~(E_STRICT | E_NOTICE | E_WARNING));
    } elseif (strtolower(ENVIRONMENT) == "testing") {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL & ~(E_STRICT | E_NOTICE | E_WARNING));
    } elseif (strtolower(ENVIRONMENT) == "production") {
        ini_set('display_errors', 0);
        ini_set('display_startup_errors', 0);
        error_reporting(0);
    } else {
        $LoginErrs = "WRONG_ENV";
        $LoginErrsMess = "PS_WRONG_ENV";
    }

    return $LoginErrs . "|" . $LoginErrMess;
}


function ChPassUid($in, $minlen)
{
    if (strlen($in) < $minlen) {
        return false;
    } else {
        return true;
    }
}

function uniqids($ins)
{
    $rtn = uniqid($ins . rand(10000, 99999));
    return $rtn;
}

function UrlExist($url)
{
    $headersCheck = @get_headers($url);
    if (strpos($headersCheck[0], '200') === false) {
        return false;
    } else {
        return true;
    }
}

function ManSession($acts)
{
    if ($acts == "Destroy") {
        if (session_status() == PHP_SESSION_NONE) {
        } else {
            $_SESSION = array();
            session_unset();

            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
            }

            session_destroy();
        }
    }

    if ($acts == "Start") {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    if ($acts == "ReStart") {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        } else {
            $_SESSION = array();
            session_unset();
            session_destroy();
            session_start();
        }
    }
}

function CheckUrl($using, $dom_array, $steps)
{
    if ($using == true) {
        $passit = false;
        if ($steps == "Begin") {
            $CURR_URL = isset($_SESSION["loginID"]) ? $_SERVER['HTTP_REFERER'] : $_SERVER['HTTP_HOST'];
        } else {
            $CURR_URL = $_SERVER['HTTP_REFERER'];
        }
        $dCheck = preg_replace("/[http[s]*:\/\/]*[www\.]*(.+)/si", "\\1", $CURR_URL);
        $dCheck = preg_split("/\//", $dCheck);
        foreach ($dom_array as $da) {
            if ($da == $dCheck[0]) {
                $passit = true;
                break;
            }
        }
    } else {
        $passit = true;
    }
    return $passit;
}

function CheckIdle($dbtime, $IdleLimit)
{
    list($usec, $sec) = explode(" ", microtime());
    $currTimeAct = ((float)$usec + (float)$sec);

    if ($IdleLimit == 0) {
        return true;
    } elseif ($dbtime > 0) {
        $SubsTime = intval(bcsub($currTimeAct, $dbtime, 2));
        if ($SubsTime > $IdleLimit) {
            return false; # Idle Exceed;
        } else {
            return true; # Idle Meets
        }
    }
}

function TimeRemain($dbtime, $AddingTime)
{
    list($usec, $sec) = explode(" ", microtime());
    $currTimeAct = ((float)$usec + (float)$sec);
    return bcsub(bcadd($dbtime, $AddingTime, 2), $currTimeAct,    2);
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}

function formatPeriod($duration)
{
    $sec = intval($duration);
    $hours = (int) ($sec / 60 / 60);
    $minutes = (int) ($sec / 60) - $hours * 60;
    $seconds = (int) $sec - $hours * 60 * 60 - $minutes * 60;
    return ($hours == 0 ? "00" : $hours) . ":" . ($minutes == 0 ? "00" : ($minutes < 10 ? "0" . $minutes : $minutes)) . ":" . ($seconds == 0 ? "00" : ($seconds < 10 ? "0" . $seconds : $seconds));
}

function formatSisaWaktu($sisa)
{
    $days = bcdiv($sisa, 86400, 0);
    $hour = bcdiv(bcsub($sisa, bcmul($days, 86400, 0), 0), 3600, 0);
    $menit = bcdiv(bcsub($sisa, bcadd(bcmul($days, 86400, 0), bcmul($hour, 3600, 0), 0), 0), 60, 0);
    if ($menit > 2) {
        $balikan = $menit . " Minutes ";
    }
    if ($hour > 0) {
        $balikan = $hour . " hours ";
    }
    if ($days > 0) {
        $balikan = $days . " Days ";
    }
    if (empty($balikan)) $balikan = "A moment ";
    return $balikan . " ago";
}

function SplitKeys($ItemKeys)
{
    list($bg_tgl, $bg_jam) = explode(" ", $ItemKeys ?? '');
    $bg_tgl = preg_replace("/\D/", "", $bg_tgl ?? '');
    $bg_jam = preg_replace("/\D/", "", $bg_jam ?? '');
    return $bg_tgl . "" . $bg_jam;
}

function ExpandKeys($ItemKeys)
{

    $ItemKeys = preg_replace('/\D/', '', $ItemKeys);
    if (empty($ItemKeys)) {
        return 0;
    } else {
        $thn = substr($ItemKeys, 0, 4);
        $bln = substr($ItemKeys, 4, 2);
        $tgl = substr($ItemKeys, 6, 2);
        $jam = substr($ItemKeys, 8, 2);
        $men = substr($ItemKeys, 10, 2);
        $det = substr($ItemKeys, 12, 2);
        return $thn . "-" . $bln . "-" . $tgl . " " . $jam . ":" . $men . ":" . $det;
    }
}

function ButtonsDashboard($cssname = "commonbuttons", $buttthemes = "Default", $icons = "", $name = "", $act = "", $forms = "", $konfirmasi = "", $float = "left")
{
    $butts = "
	<div id=\"" . $cssname . "\" style=\"float:" . $float . ";\">
	<div class=\"icon\">";
    if (empty($konfirmasi)) {
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\">";
    } else {
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\" onClick=\"return konfirmasi('" . $konfirmasi . "')\">";
    }
    $butts .= "<div style=\"text-align:center;\"><img src=\"" . BASEURL . "templates/" . $buttthemes . "/sysicons/" . $icons . ".png\"  alt=\"" . $name . "\" align=\"middle\" border=\"0\" /></div><div style=\"padding-top:3px; text-align: center; \">&nbsp;" . $name . "</div></a></div></div>";

    return $butts;
}

function ButtonsCommon($cssname = "commonbuttons", $buttthemes = "Default", $icons = "", $name = "", $act = "", $forms = "", $konfirmasi = "", $float = "left")
{
    $panjangchar = strlen($name);
    if ($panjangchar > 0) {
        for ($i = 0; $i <= $panjangchar; $i++) {
            if ($i == 0) {
                $lebar = 35;
            } else {
                if ($i > 0 and $i < 5) {
                    $lebar += 9;
                } else {
                    if (ctype_upper($name[$i])) {
                        $lebar += 8;
                    } else {
                        $lebar += 6;
                    }
                }
            }
        }
    } else {
        $lebar = 28;
    }
    $butts = "";
    $butts .= "<div id=\"" . $cssname . "\" style=\"float:" . $float . "; width:" . $lebar . "px;\">";
    $butts .= "\r\n";
    $butts .= "												";
    $butts .= "<div class=\"icon\" style=\"width:" . $lebar . "px;\">";
    $butts .= "\r\n";
    /*
	$butts = "
	<div id=\"".$cssname."\" style=\"float:".$float."; width:".$lebar."px;\">
	<div class=\"icon\" style=\"width:".$lebar."px;\">";
    */
    if (empty($konfirmasi)) {
        $butts .= "													";
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\">";
        $butts .= "\r\n";
    } else {
        $butts .= "													";
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\" onClick=\"return konfirmasi('" . $konfirmasi . "')\">";
        $butts .= "\r\n";
    }
    $butts .= "														";
    $butts .= "<div style=\"float:left;\"><img src=\"" . BASEURL . "templates/" . $buttthemes . "/sysicons/" . $icons . ".png\"  alt=\"" . $name . "\" align=\"middle\" border=\"0\" width=\"14\" height=\"14\"/></div>";
    $butts .= "\r\n";
    $butts .= "														";
    $butts .= "<div style=\"padding-top:3px; \">&nbsp;" . $name . "</div>";
    $butts .= "\r\n";
    $butts .= "													";
    $butts .= "</a>";
    $butts .= "\r\n";
    $butts .= "												";
    $butts .= "</div>";
    $butts .= "\r\n";
    $butts .= "											";
    $butts .= "</div>";
    $butts .= "\r\n";
    //	$butts .= "												";
    //	$butts .= "<div style=\"padding-top:3px; \">&nbsp;".$name."</div></a></div></div>";
    //	$butts .= "\r\n";

    return $butts;
}

function ButtonsPKBAdd($cssname = "commonbuttons", $buttthemes = "Default", $icons = "", $name = "", $act = "", $forms = "", $konfirmasi = "", $float = "left")
{
    $panjangchar = strlen($name);
    if ($panjangchar > 0) {
        for ($i = 0; $i <= $panjangchar; $i++) {
            if ($i == 0) {
                $lebar = 35;
            } else {
                if ($i > 0 and $i < 5) {
                    $lebar += 7;
                } else {
                    if (ctype_upper($name[$i])) {
                        $lebar += 7;
                    } else {
                        $lebar += 5;
                    }
                }
            }
        }
    } else {
        $lebar = 28;
    }
    $butts = "
	<div id=\"" . $cssname . "\" style=\"float:" . $float . "; width:" . $lebar . "px;\">
	<div class=\"icon\" style=\"width:" . $lebar . "px;\">";
    if (empty($konfirmasi)) {
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\">";
    } else {
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\" onClick=\"return konfirmasi('" . $konfirmasi . "')\">";
    }
    $butts .= "<div style=\"float:left;\"><img src=\"" . BASEURL . "templates/" . $buttthemes . "/sysicons/" . $icons . ".png\"  alt=\"" . $name . "\" align=\"middle\" border=\"0\" /></div><div style=\"padding-top:3px; \">&nbsp;" . $name . "</div></a></div></div>";

    return $butts;
}

function ButtonsPaging($cssname = "pagingbuttonsactive", $buttthemes = "Default", $icons = "", $name = "", $act = "", $forms = "", $konfirmasi = "", $float = "left")
{
    $panjangchar = strlen($name);
    for ($i = 0; $i <= $panjangchar; $i++) {
        if ($i == 0) {
            $lebar = 22;
        } else {
            if ($i > 0 and $i < 5) {
                $lebar += 4;
            } else {
                if (ctype_upper($name[$i])) {
                    $lebar += 0;
                } else {
                    $lebar += 0;
                }
            }
        }
    }
    $butts = "
	<div id=\"" . $cssname . "\" style=\"float:" . $float . "; width:" . $lebar . "px;\">
	<div class=\"icon\" style=\"width:" . $lebar . "px;\">";
    if (empty($konfirmasi)) {
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\">";
    } else {
        $butts .= "<a href =\"javascript:submitbutton('" . $act . "','" . $forms . "');\" onClick=\"return konfirmasi('" . $konfirmasi . "')\">";
    }
    if (!empty($icons)) {
        $butts .= "<div style=\"float:left;\"><img src=\"" . BASEURL . "templates/" . $buttthemes . "/sysicons/" . $icons . ".png\"  alt=\"" . $name . "\" align=\"middle\" border=\"0\" width=\"14\" height=\"14\"/></div>";
    }
    $butts .= "<div style=\"padding-top:3px; text-align:left;\">&nbsp;" . $name . "</div>";
    $butts .= "</a>";
    $butts .= "</div></div>";

    return $butts;
}

function SaveNum($num)
{
    $num = preg_replace("/\,/", "", $num);
    return $num;
}

function c_curr($num, $comma_separator, $comma_separator_user, $min_sign = "mark")
{
    if ($num != '') {
        if (preg_match("/-/si", $num)) {
            $num = preg_replace("/^-/si", "", $num);
            $minmark = "Y";
        } else {
            $minmark = "N";
        }

        $snum = preg_replace("/^(.*)\\$comma_separator\d{1,}$/si", "\\1", $num);
        $snum = preg_replace("/\D/si", "", $snum);

        if (preg_match("/\\$comma_separator/", $num)) {
            $snum_coma = preg_replace("/^(.*)\\$comma_separator(.*)$/si", "\\2", $num);
        }

        if (strlen($snum) > 3) {
            $fnum = intval(strlen($snum) / 3);
            if ((strlen($snum) % 3) == 0) {
                $fnum--;
            }

            for ($i = 1; $i <= $fnum; $i++) {
                if ($i == 1) {
                    $snums = preg_replace("/(.+)(\d{3}$)/", "\\2", $snum);
                    $snumx = preg_replace("/(.+)(\d{3}$)/", "\\1", $snum);
                } else {
                    $snums = preg_replace("/(.+)(\d{3}$)/", "\\2", $snumx);
                    $snumx = preg_replace("/(.+)(\d{3}$)/", "\\1", $snumx);
                }

                @$snumsr = "," . $snums  . $snumsr;
            }
            $snumr = $snumx . $snumsr;
        } else {
            $snumr = $snum;
        }

        if (!empty($snum_coma) && !preg_match("/^0{1,}$/", $snum_coma)) {
            $snum_coma = preg_replace("/0{1,}$/", "", $snum_coma);
            $snumr = $snumr . $comma_separator_user . $snum_coma;
        }
    }
    if ($num != '') {
        if ($minmark == "Y") {
            switch ($min_sign) {
                case "curve":
                    $snumr = "(" . $snumr . ")";
                    break;
                case "mark":
                    $snumr = "-" . $snumr;
                    break;
            }
        }
        return $snumr;
    } else {
        if ($num != "") {
            return 0;
        } else {
            return 0;
        }
    }
}

function deldir($dir)
{
    if (is_dir($dir)) {
        $current_dir = opendir($dir);
        while ($entryname = readdir($current_dir)) {
            if (is_dir("$dir/$entryname") and ($entryname != "." and $entryname != "..")) {
                deldir("${dir}/${entryname}");
            } elseif ($entryname != "." and $entryname != "..") {
                //chmod("${dir}/${entryname}", 0600);
                unlink("${dir}/${entryname}");
            }
        }
        closedir($current_dir);
        rmdir(${dir});
    }
}

function convertDate($date_to_be_process, $separator = "-")
{
    $formated_date = "";
    if ($date_to_be_process and ($date_to_be_process != "0000-00-00" or $date_to_be_process != "0000-00-00 00:00:00")) {

        list($Btgl, $BJam) = explode(" ", $date_to_be_process);
        list($tgl_01, $tgl_02, $tgl_03) = explode($separator, $Btgl);
        $formated_date = $tgl_03 . "-" . $tgl_02 . "-" . $tgl_01;
        if ($BJam) {
            $formated_date .= " " . $BJam;
        }
    }

    return $formated_date;
}

function c_date($in_date, $co_date, $co_month, $list_bln, $list_bln_short, $yearIncluded = "Y")
{
    if ($in_date == "0000-00-00" or $in_date == "0000-00-00 00:00:00") {
        $result_date = "";
    } else {
        if ($yearIncluded == "Y") {
            if ($co_date == "longdate") {
                list($bg_tgl, $bg_jam) = explode(" ", $in_date ?? '');
                list($dyear, $dmonth, $dtgl) = explode("-", $bg_tgl ?? '');
                if ($co_month == "long") {
                    $result_date = $dtgl . " " . $list_bln[$dmonth] . " " . $dyear . " " . $bg_jam;
                }
                if ($co_month == "short") {
                    $result_date = $dtgl . " " . $list_bln_short[$dmonth] . " " . $dyear . " " . $bg_jam;
                }
            }

            if ($co_date == "shortdate") {
                list($bg_tgl, $bg_jam) = explode(" ", $in_date ?? '') + ['', ''];
                list($dyear, $dmonth, $dtgl) = explode("-", $bg_tgl ?? '');
                if ($co_month == "long") {
                    $result_date = $dtgl . " " . $list_bln[$dmonth] . " " . $dyear;
                }
                if ($co_month == "short") {
                    $result_date = $dtgl . " " . $list_bln_short[$dmonth] . " " . $dyear;
                }
            }
        } else {
            if ($co_date == "longdate") {
                list($bg_tgl, $bg_jam) = explode(" ", $in_date ?? '');
                list($dyear, $dmonth, $dtgl) = explode("-", $bg_tgl ?? '');
                if ($co_month == "long") {
                    $result_date = $dtgl . " " . $list_bln[$dmonth] . " " . $bg_jam;
                }
                if ($co_month == "short") {
                    $result_date = $dtgl . " " . $list_bln_short[$dmonth] . " " . $bg_jam;
                }
            }

            if ($co_date == "shortdate") {
                list($bg_tgl, $bg_jam) = explode(" ", $in_date ?? '');
                list($dyear, $dmonth, $dtgl) = explode("-", $bg_tgl ?? '');
                if ($co_month == "long") {
                    $result_date = $dtgl . " " . $list_bln[$dmonth];
                }
                if ($co_month == "short") {
                    $result_date = $dtgl . " " . $list_bln_short[$dmonth];
                }
            }
        }
    }
    return $result_date;
}

function getIP()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'] . "|Shared";
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'] . "|Proxy";
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'] . "|Proxy";
    else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'] . "|Shared";
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'] . "|Proxy";
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'] . "|Proxy";
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'] . "|Real";
    else
        $ipaddress = '0.0.0.0|Spoofing';
    return $ipaddress;
}

function Purefy($str)
{
    $str = trim($str ?? '');
    if (!is_string($str) || strlen($str) < 1) {
        return "";
    } else {
        # Strip HTML tags.
        /* Allow only as mentioned */
        $str = strip_tags($str, '<p><br /><br><strong><em><ul><ol><li>');
        $str = str_replace('', '', $str);
        $str = preg_replace("/<p[^>]*>/is", '<p>', $str);

        /* More stripped just in case they are not filtered */
        $str = preg_replace('#<(/*\s*)(alert|vbscript|javascript|applet|basefont|base|behavior|bgsound|blink|body|embed|expression|form|frameset|frame|head|html|ilayer|iframe|input|layer|link|meta|object|plaintext|style|script|textarea|title|xml|xss|lowsrc)([^>]*)>#is', "&lt;\\1\\2\\3&gt;", $str);

        $str = preg_replace('#(alert|cmd|passthru|eval|exec|system|fopen|fsockopen|file|file_get_contents|readfile|unlink)(\s*)\((.*?)\)#si', "\\1\\2(\\3)", $str);



        ## More Cleaning House!
        /* Clean hex */
        $str = preg_replace("/x1a/", '', $str);
        $str = preg_replace("/x00/", '', $str);
        $str = preg_replace('/([\x00-\x08][\x0b-\x0c][\x0e-\x20])/', '', $str);

        /* Clean dir travelsal */
        $str = preg_replace("|\.\./|", '', $str);

        /* Clean MySQL comment */
        $str = preg_replace("/--/", ' - ', $str);

        /* Clean B64 Encoded */
        $str = preg_replace("/%3A%2F%2F/", '', $str);

        /* Clean Null Chars */
        $str = preg_replace('/\0+/', '', $str);
        $str = preg_replace('/(\\\\0)+/', '', $str);



        $str = preg_replace('#(&\#*\w+)[\x00-\x20]+;#u', "\\1;", $str);
        $str = preg_replace('#(&\#x*)([0-9A-F]+);*#iu', "\\1\\2;", $str);
        if (preg_match_all("/<(.+?)>/si", $str, $matches)) {
            for ($i = 0; $i < count($matches['0']); $i++) {
                $str = str_replace(
                    $matches['1'][$i],
                    html_entity_decode($matches['1'][$i], ENT_COMPAT, $charset),
                    $str
                );
            }
        }


        /* Convert tabs to space */
        $str = preg_replace("#\t+#", " ", $str);

        /* Makes PHP tags safe
		   XML "<?xml" tags will be replaced too.
		   but who cares. Only terorist use XML :)
		   Stripping out!!!
		*/
        $str = str_replace(array("<?php", "<?PHP", "<?", "?>"),  array("&lt;?php", "&lt;?PHP", "&lt;?", "?&gt;"), $str);

        /* Extra precaution */
        $bad = array(
            'document.cookie'    => '',
            'document.write'    => '',
            'window.location'    => '',
            "javascript\s*:"    => '',
            "Redirect\s+302"    => '',
            '<!--'            => '&lt;!--',
            '-->'            => '--&gt;'
        );

        $str = preg_replace("/\\\/s", "", $str);
        $str = preg_replace("/'/s", "&#039", $str);
        $str = preg_replace("/\"/s", "&quot;", $str);

        return $str;
    }
}

/*------- DARWIS SUBJECT TO DELETE ------------- */
function get_a_business_day_date($start_date, $days, $endof_workinghour = 17, $holidays = NULL)
{
    //Go to next day if after 5 pm (5 pm = 17)
    if (!is_null($endof_workinghour) && date(G, $start_date) >= $endof_workinghour) {
        $start_date = strtotime($start_date . " + 1 day"); //Tomorrow
    } else {
        $start_date = strtotime($start_date); //Today
    }
    //$date = strtotime($start_date);
    $date = $start_date;
    for ($i = 1; $i <= intval($days); $i++) { //Loops each day count
        //First, find the next available weekday because this might be a weekend/holiday
        while (date('N', $date) >= 6 || in_array(date('Y-m-d', $date), $holidays)) {
            $date = strtotime(date('Y-m-d', $date) . ' +1 day');
        }
        //Now that we know we have a business day, add 1 day to it
        $date = strtotime(date('Y-m-d', $date) . ' +1 day');
        //If this day that was previously added falls on a weekend/holiday, then find the next business day
        while (date('N', $date) >= 6 || in_array(date('Y-m-d', $date), $holidays)) {
            $date = strtotime(date('Y-m-d', $date) . ' +1 day');
        }
    }
    return date('Y-m-d', $date);
}

function get_duration_days($start_date, $endof_workinghour = 17, $holidays = NULL, $duration_days = NULL)
{
    $count_duration = 0;
    //Go to next day if after 5 pm (5 pm = 17)
    if (!is_null($endof_workinghour) && date(G, $start_date) >= $endof_workinghour) {
        //$start_date = strtotime($start_date." + 1 day"); //Tomorrow
        $start_date = strtotime($start_date); //Today
        //$start_date = strtotime($start_date." + 1 day"); //Tomorrow
        $count_duration++;
    } else {
        $count_duration++;
        //$count_duration = $count_duration+1;
        $start_date = strtotime($start_date); //Today
        //$start_date = strtotime($start_date." + 1 day"); //Tomorrow
    }

    $enddate = date("Y-m-d H:i:s");

    //$enddate = '2019-03-22 11:47:00';

    $end_date = strtotime($enddate); //Today

    $sdate = intval(date('Ymd', $start_date)); //Today
    $edate = intval(date('Ymd', $end_date)); //Today

    //echo $sdate;
    //exit (0);

    if ($sdate != $edate) {
        // kalau start date < end date ... hitung berdasarkan hari
        if ($sdate < $edate) {

            $date = $start_date;
            while ($start_date < $end_date) {
                //First, find the next available weekday because this might be a weekend/holiday
                while (date('N', $date) >= 6 || in_array(date('Y-m-d', $date), $holidays)) {
                    $date = strtotime(date('Y-m-d', $date) . ' +1 day');
                }
                //Now that we know we have a business day, add 1 day to it
                $count_duration++;
                $date = strtotime(date('Y-m-d', $date) . ' +1 day');
                //If this day that was previously added falls on a weekend/holiday, then find the next business day
                while (date('N', $date) >= 6 || in_array(date('Y-m-d', $date), $holidays)) {
                    $date = strtotime(date('Y-m-d', $date) . ' +1 day');
                }
                $start_date = strtotime(date('Y-m-d', $date) . ' +1 day');
            }

            $count_duration--;

            if ($count_duration == 1) {
                $Returned_Result = "a day ago";
            } else {
                if (!is_null($duration_days) || !$duration_days == NULL) {
                    if ($count_duration > $duration_days) {
                        //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                        $Returned_Result = "<div class='text-danger'>" . $count_duration . " days ago</div>";
                    } else {
                        $Returned_Result = $count_duration . " days ago";
                    }
                } else {
                    $Returned_Result = $count_duration . " days ago";
                }
            }
        } else {
            //if($sdate<$edate) {
            $Returned_Result = "Error!";
        }
    } else {
        //$strStart = '2013-06-19 18:25';
        //$strEnd   = '06/19/13 21:47';
        $dteStart = new DateTime(date("Y-m-d H:i:s", $start_date));
        $dteEnd   = new DateTime(date("Y-m-d H:i:s", $end_date));
        $dteDiff  = $dteStart->diff($dteEnd);
        $result_H = $dteDiff->format("%h");

        if ($result_H > 0) {
            if ($result_H == 1) {
                $Returned_Result = "an hour ago";
            } else {
                $Returned_Result = $result_H . " hours ago";
            }
        } else {
            $result_m = $dteDiff->format("%i");
            if ($result_m <= 1) {
                $Returned_Result = "a minute ago";
            } else {
                $Returned_Result = $result_m . " minutes ago";
            }
        }
    }

    return $Returned_Result;
}

function get_duration_days_note($start_date)
{
    $start_date = strtotime($start_date); //Today
    $enddate = date("Y-m-d H:i:s");
    $end_date = strtotime($enddate); //Today
    $show_duration_note = "days ago";
    // kalau start date = end date ... tampilkan " minutes ago"
    if (intval(date('Ymd', $start_date)) == intval(date('Ymd', $end_date))) {
        $dteStart = new DateTime(date("Y-m-d H:i:s", $start_date));
        $dteEnd   = new DateTime(date("Y-m-d H:i:s", $end_date));
        $dteDiff  = $dteStart->diff($dteEnd);
        //print $dteDiff->format("%H:%I:%S");
        $result_H = intval($dteDiff->format("%h"));
        //echo $result_H;
        //exit(0);
        if ($result_H > 0) {
            // tampilkan hasil hitung berdasarkan jam
            $show_duration_note = "hours ago";
            if ($result_H == 1)
                $show_duration_note = "an hour ago";
        } else {
            // tampilkan hasil hitung berdasarkan menit
            $show_duration_note = "minutes ago";
        }
    }
    // kalau (yyyymmdd) start date < (yyyymmdd) end date ... tampilkan " days ago"
    return $show_duration_note;
}

/*
2019
Pasti
01-01 	1 jan 	Tahun Baru Masehi 2019
05-01 	1 mei 	Hari Buruh Internasional
06-01		1 jun 	Hari Lahir Pancasila
08-17		17 agu 	Hari Kemerdekaan Republik Indonesia
12-25		25 des 	Hari Raya Natal

Tidak pasti
02-05		5 feb 	Tahun Baru Imlek 2570 Kongzili
03-07		7 mar 	Hari Raya Nyepi Tahun Baru Saka 1941
04-03 	3 apr 	Isra' Mi'raj Nabi Muhammad SAW
04-19		19 apr 	Wafat Isa Al Masih / ?
05-19		19 mei 	Hari Raya Waisak 2563
05-30		30 mei 	Kenaikan Isa Al Masih
06-05		5,6 jun 	Hari Raya Idul Fitri 1440H
06-06		5,6 jun 	Hari Raya Idul Fitri 1440H
06-03		3,4,7 jun Cuti Bersama Hari Raya Idul Fitri 1440H
06-04		3,4,7 jun Cuti Bersama Hari Raya Idul Fitri 1440H
06-07		3,4,7 jun Cuti Bersama Hari Raya Idul Fitri 1440H
08-11		11 agu 	Hari Raya Idul Adha 1440H
09-01		1 sep 	Tahun Baru Islam 1441 Hijriah
11-09		9 nov 	Maulid Nabi Muhammad SAW
12-24		24 des 	Cuti Bersama Hari Raya Natal

2020
Pasti
1 jan 	Tahun Baru Masehi 2020
1 mei 	Hari Buruh Internasional
1 jun 	Hari Lahir Pancasila
17 agu 	Hari Kemerdekaan Republik Indonesia
25 des 	Hari Raya Natal

Tidak pasti
01-25		25 jan 	Tahun Baru Imlek 2571 Kongzili
03-25		25 mar 	Hari Raya Nyepi Tahun Baru Saka 1942
03-22		22 mar 	Isra' Mi'raj Nabi Muhammad SAW
04-10		10 apr 	Wafat Isa Al Masih / ?
05-07		7 mei 	Hari Raya Waisak 2564
05-21		21 mei 	Kenaikan Isa Al Masih
06-24		24,25 jun 	Hari Raya Idul Fitri 1440H
06-25		24,25 jun 	Hari Raya Idul Fitri 1440H
06-03		?,?,? jun Cuti Bersama Hari Raya Idul Fitri 1441H
07-31		31 jul 	Hari Raya Idul Adha 1441H
08-20		20 agu 	Tahun Baru Islam 1442 Hijriah
10-29		29 okt 	Maulid Nabi Muhammad SAW
12-26		2? des 	Cuti Bersama Hari Raya Natal


fixed holidays
A business calendar for any year always includes a number of fixed holidays. They could be date-specific holidays,
such as the New Year Day on January 1, or the Independence Day on July 4. These holidays are specified with the attributes month and day.
Some fixed holidays are day-of-the-week-specific, such as the Thanksgiving Day falling on the fourth Thursday of November.
The date varies depending on the year, and hence these holidays are specified with the attributes month, dow and occurrence.

Year-specific holidays
Year-specific holidays are always qualified for the specified year.
The above example shows that the Company Annual Event takes place on a different date each year, and in the year 2011,
it falls on November 18. The year-specific holidays can also be day-specific, as explained in the case of Fixed holidays.
Note: Some year-specific holidays can be half-day holidays. Refer to Half-day holidays.

Calendar of Holidays and Religious Observances
unofficial observances by date
Unofficial holidays, awareness days, and other observances

*/

/*
function get_indonesian_holidays($year, $dbx) {
    $holiday_array[]=array();
    $cHoliday = "SELECT * FROM `izin_sys_workingday` WHERE `wd_year`='".$year."'";
    $rcHoliday = $dbx->getQuery($cHoliday);
        while($Holiday = $dbx->getAssoc($rcHoliday)) {
            // add 0 to month 1 digit
            if(strlen($Holiday['wd_month']) == 1){$bulan = "0".$Holiday['wd_month'];} else {$bulan = $Holiday['wd_month'];}
            $haystack = $Holiday["wd_holiday"];
            $needle   = ",";
            if( strpos( $haystack, $needle ) !== false) {
                $the_day = explode(',',$Holiday["wd_holiday"]);
                foreach ($the_day as $value) {
                    //"2019-01-01"
                    // add 0 to date 1 digit
                    if(strlen($value) == 1){$tanggal = "0".$value;} else {$tanggal = $value;}
                    $holiday_array[] = $Holiday['wd_year']."-".$bulan."-".$tanggal;
                }
            } else {
                // add 0 to date 1 digit
                if(strlen($haystack) == 1){$tanggal = "0".$haystack;} else {$tanggal = $haystack;}
                $holiday_array[] = $Holiday['wd_year']."-".$bulan."-".$tanggal;
            }
        }
    return $holiday_array;
}
*/

//function get_indonesian_holidays($year, $dbx) {
function get_indonesian_holidays($dbx)
{
    $holiday_array = array();
    $year = date("Y");
    $cHoliday = "SELECT * FROM `izin_master_holiday` WHERE `hday_year` >= " . intval($year) . "";
    // cek kalau hasil kosong, skip function, return show empty
    $rcHoliday = $dbx->getQuery($cHoliday);
    //$Holiday = $dbx->getAssoc($rcHoliday);
    while ($Holiday = $dbx->getAssoc($rcHoliday)) {
        $year = $Holiday['hday_year'];
        if (!empty($Holiday['hday_fix'])) {
            //not working trim
            //$hday_fix = trim($Holiday['hday_fix']);
            $hday_fix = $Holiday['hday_fix'];
            if (strpos($hday_fix, ",") !== false) {
                $hdayfix = explode(',', $hday_fix);
                foreach ($hdayfix as $value) {
                    //"2019-01-01"
                    $holiday_array[] = $year . "-" . $value;
                }
            } else {
                $holiday_array[] = $year . "-" . $hday_fix;
            }
        }
        if (!empty($Holiday['hday_no_fix'])) {
            //not working trim
            //$hday_no_fix = trim($Holiday['hday_no_fix'], " ");
            $hday_no_fix = $Holiday['hday_no_fix'];
            if (strpos($hday_no_fix, ",") !== false) {
                $hdaynofix = explode(',', $hday_no_fix);
                foreach ($hdaynofix as $value) {
                    //"2019-01-01"
                    $holiday_array[] = $year . "-" . $value;
                }
            } else {
                $holiday_array[] = $year . "-" . $hday_no_fix;
            }
        }
    }
    //print_r($holiday_array);
    //exit(0);
    return $holiday_array;
}

if (!function_exists('convert_img_to_base64')) {
    function convert_img_to_base64($path)
    {
        $mime_type = mime_content_type($path);

        return "data:$mime_type;base64," . base64_encode(
            file_get_contents($path)
        );
    }
}

function human_filesize($bytes, $decimals = 2)
{
    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor > 0) $sz = 'KMGT';
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
}

function create_user(string $email)
{
    try {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => CLIENTURL . "/api/user",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query([
                "email" => $email
            ]),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $content = curl_exec($curl);

        // Check the return value of curl_exec(), too
        if ($content === false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }

        // Check HTTP return code, too; might be something else than 200
        $httpReturnCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    } catch (Exception $e) {

        trigger_error(
            sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(),
                $e->getMessage()
            ),
            E_USER_ERROR
        );
    } finally {
        // Close curl handle unless it failed to initialize
        if (is_resource($curl)) {
            curl_close($curl);
        }
    }

    return $httpReturnCode;
}

function reset_password(string $email)
{
    try {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => CLIENTURL . "/api/user_reset",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query([
                "email" => $email
            ]),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $content = curl_exec($curl);

        // Check the return value of curl_exec(), too
        if ($content === false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }

        // Check HTTP return code, too; might be something else than 200
        $httpReturnCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    } catch (Exception $e) {

        trigger_error(
            sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(),
                $e->getMessage()
            ),
            E_USER_ERROR
        );
    } finally {
        // Close curl handle unless it failed to initialize
        if (is_resource($curl)) {
            curl_close($curl);
        }
    }

    return $httpReturnCode;
}

function delete_userfile(array $data)
{
    try {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => CLIENTURL . "/api/userfile/delete",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query([
                "file_id" => $data['file_id'],
                "tracking_id" => $data['tracking_id'],
                "lead_id" => $data['lead_id'],
                "email" => $data['email']
            ]),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $content = curl_exec($curl);

        // Check the return value of curl_exec(), too
        if ($content === false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }

        // Check HTTP return code, too; might be something else than 200
        $httpReturnCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    } catch (Exception $e) {

        trigger_error(
            sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(),
                $e->getMessage()
            ),
            E_USER_ERROR
        );
    } finally {
        // Close curl handle unless it failed to initialize
        if (is_resource($curl)) {
            curl_close($curl);
        }
    }

    return $httpReturnCode;
}
