
                    <div class="container-fluid page__container">
                        <div class="page__heading d-flex align-items-center">
                            <div class="flex">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item"><a href="<?php echo $bchref?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101 ?></a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Leads Report<?php //echo $lang_50100 ?></li>
                                    </ol>
                                </nav>
                            </div>

                        </div>
                    </div>




                    <div class="container-fluid page__container">
                      <?php
                      if(isset($_SESSION["AlertMess"]) OR !empty($_SESSION["AlertMess"])){
                          echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
                              echo '<i class="material-icons mr-3">error_outline</i>';
                              echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
                          echo '</div>';
                          //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                          unset($_SESSION["AlertMess"]);
                      }
                      ?>
<!--
                        <div class="card-group">
                            <div class="card card-body text-center">

                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0"> <i class="material-icons icon-muted icon-30pt">perm_identity</i> Total Lead</div>
                                    <div class="text-amount ml-auto"><?php echo $Count_AllLeads ?></div>
                                </div>
                            </div>
                            <div class="card card-body text-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0"><i class="material-icons icon-muted icon-30pt">shopping_basket</i>Total Lead Deal</div>
                                    <div class="text-amount ml-auto"><?php echo $Count_CloseLeads ?></div>
                                </div>
                            </div>
                            <div class="card card-body text-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0"><i class="material-icons  icon-muted icon-30pt">mood_bad</i> Total Lost</div>
                                    <div class="text-amount ml-auto"><?php echo $Count_LostLeads ?></div>
                                </div>
                            </div>
                        </div>
-->



<div class="row">
    <div class="col-lg-12">
        <div class="card">


            <div class="card-header">

                <form class="form-inline" action="" method="post" name="SearchLeadBy" id ="SearchLeadBy" enctype="multipart/form-data">
                    <input type="hidden" name="task" />
                    <input type="hidden" name="lchart_range" value="<?php echo $lead_date_range ?>" />

                    <label class="sr-only" for="lead_status">Status</label><!--  data-toggle="select" form-control" style="max-width: 150px!;" -->
                    <select name="lead_status" id="lead_status" class="custom-select mb-2 mr-sm-2 mb-sm-0 mt-2" style="max-width: 150px!;">
                        <option value="all">Status</option>
                        <?php
                        foreach(LEAD_STATUS as $key => $value) {
                            echo '<option value="'.$key.'"';
                            echo $key == $lead_status ? ' selected> ' : '> ';
                            echo $value.'</option>';
                        }
                        ?>
                    </select>

                    <label class="sr-only" for="lead_sales">Sales</label>
                    <select name="lead_sales" id="lead_sales" class="custom-select mb-2 mr-sm-2 mb-sm-0 mt-2">
                        <option value="all">Sales</option>
                        <?php
                        while($LeadSalesData = $dbs->getAssoc($rcLeadSalesData) ) {
                            echo '<option value="'.$LeadSalesData['id'].'"';
                            echo $LeadSalesData['id'] == $lead_sales ? ' selected> ' : '> ';
                            echo $LeadSalesData['nama_asli'].'</option>';
                        }
                        ?>
                    </select>

                    <label class="sr-only" for="lead_package">Packages</label>
                    <select name="lead_package" id="lead_package" class="custom-select mb-2 mr-sm-2 mb-sm-0 mt-2">
                        <option value="all">Packages</option>
                        <?php
                        while($LeadPackagesData = $dbs->getAssoc($rcLeadPackagesData)) {
                            echo '<option value="'.$LeadPackagesData['kode'].'"';
                            echo $LeadPackagesData['kode'] == $lead_package ? ' selected> ' : '> ';
                            echo $LeadPackagesData['nama'].'</option>';
                        }
                        ?>
                    </select>
                    <label class="sr-only" for="lead_location">Prefered Location</label>
                    <select name="lead_location" id="lead_location" class="custom-select mb-1 mr-sm-1 mb-sm-0 mt-2">
                        <option value="all"> Location</option>
                        <?php
                        while($LeadLocationData = $dbs->getAssoc($rcLeadLocationData)) {
                            echo '<option value="'.$LeadPackagesData['id'].'"';
                            echo $LeadLocationData['id'] == $lead_location ? ' selected> ' : '> ';
                            echo $LeadLocationData['lokasi'].'</option>';
                        }
                        ?>
                    </select>

                    <label class="sr-only" for="lead_transaction">Transaction</label>
                    <select name="lead_transaction" id="lead_transaction" class="custom-select mb-1 mr-sm-1 mb-sm-0 mt-2">
                        <option value="all">Transaction</option>
                        <option value="n"<?php echo $lead_transaction == 'n' ? ' selected' : '';?>>New</option>
                        <option value="r"<?php echo $lead_transaction == 'r' ? ' selected' : '';?>>Repeat</option>
                        <?php
                        /*
                        while($LeadTransactionData = $dbs->getAssoc($rcLeadTransactionData)) {
                            echo '<option value="'.$LeadTransactionData['id'].'"';
                            echo $LeadTransactionData['id'] == $sales_id ? ' selected> ' : '> ';
                            echo $LeadTransactionData['lokasi'].'</option>';
                        }
                        */
                        ?>
                    </select>


                    <input id="lchart_range" name="lchart_range" value="" type="text" class="form-control flatpickr mt-2" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">
                    <!--
                    <div id="recent_orders_date" data-toggle="flatpickr" data-flatpickr-wrap="true" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y">
                        <a href="javascript:void(0)" class="link-date" data-toggle>13/03/2018 to 20/03/2018</a>
                        <input class="flatpickr-hidden-input" type="hidden" value="13/03/2018 to 20/03/2018" data-input>

                        <?php
                        //echo '<input id="lchart_range" name="lchart_range" value="" type="text" class="form-control flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
                        ?>

                    </div>
                  -->



                <div class="" style="margin-left:10px;">
                  <label class="sr-only" for="lead_transaction">Source</label>
                  <select name="lead_source" id="lead_source" class="custom-select mb-1 mr-sm-1 mb-sm-0 mt-2">
                      <option value="all">All Source</option>
                      <?php foreach (SRC_ITEM as $key => $src): ?>
                        <option value="<?= $key;?>"<?php echo $lead_source == $key ? ' selected' : '';?> ><?= $src;?></option>
                      <?php endforeach; ?>
                      <?php
                      /*
                      while($LeadTransactionData = $dbs->getAssoc($rcLeadTransactionData)) {
                          echo '<option value="'.$LeadTransactionData['id'].'"';
                          echo $LeadTransactionData['id'] == $sales_id ? ' selected> ' : '> ';
                          echo $LeadTransactionData['lokasi'].'</option>';
                      }
                      */
                      ?>
                  </select>
                </div>
                    <!--<div style="margin-top:-40px" class="ml-auto">-->
                        <!--
                        <a href="#" class="btn btn-warning">Search </a>
                        <a href="#" class="btn btn-success">Export Excel </a>
                      -->
                        <button type="button" class="btn btn-warning btn-search ml-2 mt-2" id="search-lead" data-toggle="tooltip" data-placement="top" title="Search Lead"><?php //echo $lang_50121?>Search</button>
                        <input type="submit" class="btn btn-success btn-excell ml-2 mt-2" name="export-excell" data-toggle="tooltip" data-placement="top" title="Export to Excell"  value="Export Excell"><?php //echo $lang_50121?>
                    <!--</div>-->
                </form>
                <!--
                <form action="" method="post" name="ExportToXLS" id ="ExportToXLS">
                    <input type="hidden" name="task" />
                </form>
              -->




  <!--
                  <div class="d-flex align-items-center text-center mb-4">

                      <div class="ml-auto">
                              <form action="" method="post" name="UpdateLeadStatus" id ="UpdateLeadStatus" enctype="multipart/form-data">
                                  <input type="hidden" name="task" />
                                  <input type="hidden" name="lchart_range" value="<?php echo $lead_date_range ?>" />
                                  <select name="lead_status" id="lead_status" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                      <option value="All">Status</option>
                                      <?php
                                      foreach(LEAD_STATUS as $key => $value) {
                                          echo '<option value="'.$key.'"';
                                          echo $lead_status == $key ? ' selected> ' : '> ';
                                          echo $value.'</option>';

                                          //echo '<option value="'.$key.'">'.$value.'</option>';
                                      }
                                      ?>
                                  </select>
                              </form>
                      </div>




                      <div class="ml-auto">
                              <form action="" method="post" name="UpdateLeadSales" id ="UpdateLeadSales" enctype="multipart/form-data">
                                  <input type="hidden" name="task" />
                                  <input type="hidden" name="lchart_range" value="<?php echo $lead_date_range ?>" />
                                  <select name="lead_sales" id="lead_sales" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                      <option value="All">Sales</option>
                                      <?php
                                      while($LeadSalesData = $dbs->getAssoc($rcLeadSalesData)) {
                                          echo '<option value="'.$LeadSalesData['id'].'"';
                                          echo $LeadSalesData['id'] == $sales_id ? ' selected> ' : '> ';
                                          echo $LeadSalesData['nama_asli'].'</option>';

                                          //echo '<option value="'.$key.'">'.$value.'</option>';
                                      }
                                      ?>
                                  </select>
                              </form>
                      </div>


                      <div class="ml-auto">
                              <form action="" method="post" name="UpdateLeadPackages" id ="UpdateLeadPackages" enctype="multipart/form-data">
                                  <input type="hidden" name="task" />
                                  <input type="hidden" name="lchart_range" value="<?php echo $lead_date_range ?>" />
                                  <select name="lead_packages" id="lead_packages" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                      <option value="All">Packages</option>
                                      <?php
                                      foreach(LEAD_STATUS as $key => $value) {
                                          echo '<option value="'.$key.'"';
                                          echo $lead_status == $key ? ' selected> ' : '> ';
                                          echo $value.'</option>';

                                          //echo '<option value="'.$key.'">'.$value.'</option>';
                                      }
                                      ?>
                                  </select>
                              </form>
                      </div>

                      <div class="ml-auto">
                              <form action="" method="post" name="UpdateLeadLocation" id ="UpdateLeadLocation" enctype="multipart/form-data">
                                  <input type="hidden" name="task" />
                                  <input type="hidden" name="lchart_range" value="<?php echo $lead_date_range ?>" />
                                  <select name="lead_location" id="lead_location" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                      <option value="All">Location</option>
                                      <?php
                                      foreach(LEAD_STATUS as $key => $value) {
                                          echo '<option value="'.$key.'"';
                                          echo $lead_status == $key ? ' selected> ' : '> ';
                                          echo $value.'</option>';

                                          //echo '<option value="'.$key.'">'.$value.'</option>';
                                      }
                                      ?>
                                  </select>
                              </form>
                      </div>


                      <div class="ml-auto">
                          <div class="" style="font-size: 8px!;">
                              <form action="" method="post" name="UpdateLeadTransaction" id ="UpdateLeadTransaction" enctype="multipart/form-data">
                                  <input type="hidden" name="task" />
                                  <input type="hidden" name="lchart_range" value="<?php echo $lead_date_range ?>" />
                                  <select name="lead_transaction" id="lead_transaction" data-toggle="select" class="" style="min-width: 200px!;">
                                      <option value="All">Transaction</option>
                                      <?php
                                      foreach(LEAD_STATUS as $key => $value) {
                                          echo '<option value="'.$key.'"';
                                          echo $lead_status == $key ? ' selected> ' : '> ';
                                          echo $value.'</option>';

                                          //echo '<option value="'.$key.'">'.$value.'</option>';
                                      }
                                      ?>
                                  </select>
                              </form>
                          </div>
                      </div>











                      <div class="ml-auto">
                          <form action="" method="post" name="UpdateDateRange" id ="UpdateDateRange" enctype="multipart/form-data">
                              <input type="hidden" name="task" />
                              <input type="hidden" name="lead_status" value="<?php echo $lead_status ?>" />
                              <?php
                              echo '<input id="lchart_range" name="lchart_range" value="" type="text" class="form-control flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
                              ?>
                          </form>
                      </div>

                      <div class="ml-auto">
                          <button type="button" class="btn btn-warning btn-close" id="dialog-message-close" data-toggle="tooltip" data-placement="top" title="Tutup dialog ini"><?php //echo $lang_50121?>Search</button>
                          <button type="button" class="btn btn-success btn-close" id="dialog-message-close" data-toggle="tooltip" data-placement="top" title="Tutup dialog ini"><?php //echo $lang_50121?>Export Excel</button>
                      </div>

                  </div>
  -->






            </div>


            <div class="card">
                <div class="card-header card-header-large bg-white">
                    <small class="text-muted"><?php echo $rl->data_info(); ?></small>
                </div>
            </div>






                                                  <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                      <table class="table mb-0 thead-border-top-0">
                                                          <thead>
                                                              <tr>
                                                                  <th style="width: 5%;">Client ID</th>
                                                                  <th style="width: 15%;">Name</th>
                                                                  <th style="width: 20%;">Company</th>
                                                                  <th style="width: 10%;">Transaction</th>
                                                                  <th style="width: 10%;">Packages</th>
                                                                  <th style="width: 10%;">Location</th>
                                                                  <th style="width: 10%;text-align:center;">Status</th>
                                                                  <th style="width: 10%;text-align:center;">Status Reason</th>
                                                                  <th style="width: 10%;text-align:center;">Source</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody class="list" id="staff">
              <!--
                                                              <tr class="selected">
                                                                  <td>
                                                                      <div class="custom-control custom-checkbox">
                                                                          <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_1">
                                                                          <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                                                                      </div>
                                                                  </td>
                                                                  <td>2019</td>
                                                                  <td>January</td>
                                                                  <td>52</td>
                                                                  <td>Rp. 600.000.000 </td>
                                                                  <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                              </tr>
              -->
                                                              <?php
                                                              //while($LeadMainData = $dbs->getAssoc($rcLeadMainData)) {
                                                              while($LeadMainData = $rl->result_assoc()) {
                                                              ?>
                                                              <tr>
              <!--
                                                              <tr class="selected">-->
              <!--                                                <pre><?php print_r($LeadMainData); ?></pre> -->
                                                                  <td align="left"><?php echo $LeadMainData["client_id"] ?></td>
                                                                  <td align="left"><?php echo $LeadMainData["client_name"] ?></td>
                                                                  <td align="left"><?php echo $LeadMainData["company_name"] ?></td>
                                                                  <td align="center">
                                                                    <?php
                                                                    if(!isset($lead_repeat_array[$LeadMainData["client_id"]])) {
                                                                        echo "New";
                                                                    } else {
                                                                        echo "Repeat";
                                                                    }

                                                                    //$lead_repeat_array//$dt = DateTime::createFromFormat('!m', $InvData["InvMonth"]); echo $dt->format('F');
                                                                    ?>
                                                                  </td>
                                                                  <td align="left"><?php echo $LeadMainData["package_name"] ?></td>
                                                                  <td align="left"><?php echo $LeadMainData["prefloc_name"] ?></td>
                                                                  <td align="center"><?php echo $LeadMainData["status_nm"] ?></td>
                                                                  <td align="left"><?php echo $LeadMainData["status_reason"] ?></td>
                                                                  <td align="left"><?php echo $LeadMainData["source"] ?></td>
                                                              </tr>
                                                              <?php
                                                              }
                                                              ?>
                                                          </tbody>
                                                      </table>
                                                  </div>





            <!--
            <div class="card-body">
            </div>
            -->
            <div class="card-body text-right">
<!--                15 <span class="text-muted">of 1,430</span> <a href="#" class="text-muted-light"><i class="material-icons ml-1">arrow_forward</i></a>
-->
                <?php
                echo $rl->print_page(NULL);
                echo $rl->form_print_page();
                ?>

            </div>



        </div>
    </div>
</div>




<!--
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center text-center mb-4">

                                            <div class="border-right pr-4 mr-4">
                                                <div class="mb-0">Total Lead</div>
                                                <div class="text-amount"><?php //echo $Count_AllLeads ?></div>
                                            </div>

                                            <div class="ml-auto">
                                                <form action="" method="post" name="UpdateDateRange" id ="UpdateDateRange" enctype="multipart/form-data">
                                                    <input type="hidden" name="task" />
                                                    <input type="hidden" name="lead_status" value="<?php //echo $lead_status ?>" />
                                                    <?php
                                                    //echo '<input id="lchart_range" name="lchart_range" value="" type="text" class="form-control flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
                                                    ?>
                                                </form>
                                            </div>

                                            <div class="ml-auto">
                                                    <form action="" method="post" name="UpdateLeadStatus" id ="UpdateLeadStatus" enctype="multipart/form-data">
                                                        <input type="hidden" name="task" />
                                                        <input type="hidden" name="lchart_range" value="<?php //echo $lead_date_range ?>" />
                                                        <select name="lead_status" id="lead_status" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                                          <option value="All"> All Leads Data </option>
                                                          <?php
                                                          /*
                                                          foreach(LEAD_STATUS as $key => $value) {
                                                              echo '<option value="'.$key.'"';
                                                              echo $lead_status == $key ? ' selected> ' : '> ';
                                                              echo $value.'</option>';

                                                              //echo '<option value="'.$key.'">'.$value.'</option>';
                                                          }
                                                          */
                                                          ?>
                                                        </select>
                                                    </form>
                                            </div>

                                        </div>
                                        <div class="chart" style="height: 295px;">
                                            <canvas id="viewsChart">
                                                <span style="font-size: 1rem;"><strong>Views</strong> area chart goes here.</span>
                                            </canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

-->




                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header card-header-large bg-white d-flex align-items-center">
                                        <div class="flatpickr-wrapper flex">
                                            <div id="recent_orders_date" data-toggle="flatpickr" data-flatpickr-wrap="true" data-flatpickr-static="true" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y">
                                                <h4 class="card-header__title">Top Product interested </h4>
                                                <input class="d-none" type="hidden" value="13/03/2018 to 20/03/2018" data-input>
                                            </div>
                                        </div>
                                        <i class="material-icons icon-muted">help_outline</i>
                                    </div>
                                    <div class="card-body py-0">
                                        <div class="list-group list-group-small list-group-flush">
                                          <?php
                                            //while($ViewTrack = $dbs->getAssoc($rcViewTrack)) {
                                            while($TopData = $dbs->getAssoc($rcTopData)) {
                                          ?>
                                            <div class="list-group-item d-flex align-items-center px-0">
                                                <div class="mr-3 flex"><?php echo $TopData["package_name"] ?></div>
                                                <div class="mr-3 text-dark-gray"><?php echo $TopData["JumlahLeads"] ?></div>
                                            </div>
                                          <?php
                                          }
                                          ?>

                                        </div>
                                    </div>

                                </div>
                            </div>

                <div class="col-lg">

                    <div class="card">
<!--
                        <div class="card-header card-header-tabs-basic nav justify-content-center" role="tablist">
                            <div data-toggle="chart" data-target="#locationDoughnutChart" data-update='{"data":{
  "labels": ["WhatsApp", "United Kingdom", "Germany", "India"],
  "datasets": [{"label": "Traffic", "data":[25,25,15,35]}]
}}'>
                                <a href="#" class="active" data-toggle="tab" role="tab" aria-selected="true">
                                    Source Leads
                                </a>
                            </div>
                        </div>
-->
                        <!--<div class="card-body d-flex align-items-center justify-content-center" style="background-color: red; height: 210px;padding-top: 16px;">-->
                        <div class="card-body d-flex align-items-center justify-content-center" style="height: 250px;">
                            <div class="row">
                                <div class="col-7">
                                  <!--<small><strong>Source Leads</strong></small>-->
                                  <small><strong>Status</strong></small>
                                    <div class="chart" style="height: calc(210px - 1.25rem * 2); padding-top: 20px;">
                                        <canvas id="locationDoughnutChart" data-chart-legend="#locationDoughnutChartLegend">
                                            <span style="font-size: 1rem;" class="text-muted"><strong>Location</strong> doughnut chart goes here.</span>
                                        </canvas>
                                    </div>
                                </div>
                                <!--<div class="col-5"  style="background-color: yellow; padding-top: 1px;">-->
                                <div class="col-5"  style="margin-top: 0px;">
                                    <div id="locationDoughnutChartLegend" class="chart-legend chart-legend--vertical" style="padding-top: 0px;"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                        </div>






                    </div>




                    <!-- SCRIPT untuk LINE CHART (Tidak perlu yang lain??) -->
                    		    <!-- Moment.js -->
                      <script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment.min.js"></script>
                    	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment-range.js"></script>
                    	<script>
                        (function() {
                            'use strict';
                            window['moment-range'].extendMoment(moment);
                        })()
                    	</script>
                    <!-- SCRIPT untuk LINE CHART (Tidak perlu yang lain??) -->


                    <!-- SCRIPT untuk SOURCE LEAD CHART (Tidak perlu yang lain??) -->

                    	<!-- Global Settings --> <!-- cHART SOURCE LEAD -->
                    	<script src="<?php echo BASEURL ?>assets/js/izin_js/settings.js"></script>
                    	<!-- Chart.js -->  <!-- cHART SOURCE LEAD -->
                    	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-Chart.min.js"></script>
                    	<!-- App Charts JS -->  <!-- cHART SOURCE LEAD -->
                    	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-charts.js"></script>






                    <!-- Chart Samples -->
                    <script>
                        (function() {
                            'use strict';

                            Charts.init()

/*
                            var EarningsTraffic = function(id, type = 'line', options = {}) {
                                options = Chart.helpers.merge({
                                    elements: {
                                        line: {
                                            fill: 'start',
                                            backgroundColor: settings.charts.colors.area
                                        }
                                    }
                                }, options)

                                var data = {
                                    labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                                    datasets: [{
                                        label: "Traffic",
                                        data: [10, 2, 5, 15, 10, 12, 15, 25, 22, 30, 25, 40]
                                    }]
                                }

                                Charts.create(id, type, options, data)
                            }

                            var Products = function(id, type = 'line', options = {}, data) {
                                options = Chart.helpers.merge({
                                    elements: {
                                        line: {
                                            fill: 'start',
                                            backgroundColor: settings.charts.colors.area,
                                            tension: 0,
                                            borderWidth: 1
                                        },
                                        point: {
                                            pointStyle: 'circle',
                                            radius: 5,
                                            hoverRadius: 5,
                                            backgroundColor: settings.colors.white,
                                            borderColor: settings.colors.primary[700],
                                            borderWidth: 2
                                        }
                                    },
                                    scales: {
                                        yAxes: [{
                                            display: false
                                        }],
                                        xAxes: [{
                                            display: false
                                        }]
                                    }
                                }, options)

                                data = data || {
                                    labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                                    datasets: [{
                                        label: "Earnings",
                                        data: [400, 200, 450, 460, 220, 380, 800]
                                    }]
                                }

                                Charts.create(id, type, options, data)
                            }

                            var Courses = function(id, type = 'line', options = {}) {
                                options = Chart.helpers.merge({
                                    elements: {
                                        line: {
                                            borderColor: settings.colors.success[700],
                                            backgroundColor: settings.hexToRGB(settings.colors.success[100], 0.5)
                                        },
                                        point: {
                                            borderColor: settings.colors.success[700]
                                        }
                                    }
                                }, options)

                                Products(id, type, options)
                            }
*/



                            var LocationDoughnut = function(id, type = 'doughnut', options = {}) {
                                options = Chart.helpers.merge({
                                    tooltips: {
                                        callbacks: {
                                            title: function(a, e) {
                                                return e.labels[a[0].index]
                                            },
                                            label: function(a, e) {
                                                var t = "";
                                                return t += '<span class="popover-body-value">' + e.datasets[0].data[a.index] + "%</span>"
                                            }
                                        }
                                    }
                                }, options)
                    //!!!!!CHANGE THESE BITS
                                var data = {
                                    //labels: ["WhatsApp", "Email", "Call Center", "Referral vOffice", "Referral Unionspace", "Instagram", "Facebook", "Walk In"],
                                    labels: [<?php echo $labels_data ?>],
                                    datasets: [{
                                        /*<?php //echo 'data: ['.intval($WA).', '.intval($EM).', '.intval($CC).', '.intval($RO).', '.intval($RU).', '.intval($IN).', '.intval($FB).', '.intval($WI).'],' ?>
                                        */
                                        data: [<?php echo $pecentage_data ?>],
                                        //backgroundColor: ['rgb(255,0,0)', 'rgb(65,105,225)', 'rgb(173,216,230)', 'rgb(255,69,0)', 'rgb(219,112,147)', 'rgb(127,255,212)', 'rgb(154,205,50)', 'rgb(240,230,140)', 'rgb(219,112,147)'],
                                        backgroundColor: [<?php echo $bgcolor_data ?>],
                                        hoverBorderColor: "dark" == settings.charts.colorScheme ? settings.colors.gray[800] : settings.colors.white
                                    }]
                                }
                                /*
                                var data = {
                                    labels: ["WhatsApp", "Email", "Call Center", "Referral vOffice", "Referral Unionspace", "Instagram", "Facebook", "Walk In"],
                                    datasets: [{
                                        data: [20, 20, 5, 3, 5, 10, 5, 5,],
                                        backgroundColor: [settings.colors.success[400], settings.colors.danger[400], settings.colors.primary[500], settings.colors.gray[300], settings.colors.gray[300], settings.colors.primary[500], settings.colors.primary[500], settings.colors.primary[500]],
                                        hoverBorderColor: "dark" == settings.charts.colorScheme ? settings.colors.gray[800] : settings.colors.white
                                    }]
                                }
                                */
                                Charts.create(id, type, options, data)
                            }

                            ///////////////////
                            // Create Charts //
                            ///////////////////

//                            EarningsTraffic('#earningsTrafficChart')
//                            Products('#productsChart')
//                            Courses('#coursesChart')
                              LocationDoughnut('#locationDoughnutChart')

                        })()
                    </script>






                    <!-- Chart Samples -->
                    <script>
/*
                        (function() {
                            'use strict';

                            Charts.init()

                            var Views = function(id, type = 'line', options = {}) {
                                options = Chart.helpers.merge({
                                    elements: {
                                        line: {
                                            fill: 'start',
                                            backgroundColor: settings.charts.colors.area,
                                            tension: 0,
                                            borderWidth: 1
                                        },
                                        point: {
                                            pointStyle: 'circle',
                                            radius: 5,
                                            hoverRadius: 5,
                                            backgroundColor: settings.colors.white,
                                            borderColor: settings.colors.primary[700],
                                            borderWidth: 2
                                        }
                                    },
                                    scales: {
                                        xAxes: [{
                                            gridLines: {
                                                display: false
                                            },
                                            type: 'time',
                                            time: {
                                                unit: 'day'
                                            }
                                        }]
                                    },
                                    tooltips: {
                                        callbacks: {
                                            label: function(a, e) {
                                                var t = e.datasets[a.datasetIndex].label || "",
                                                    o = a.yLabel,
                                                    r = "";
                                                return 1 < e.datasets.length && (r += '<span class="popover-body-label mr-auto">' + t + "</span>"), r += '<span class="popover-body-value">' + o + " leads</span>"
                                            }
                                        }
                                    }
                                }, options)

//                                var data = []


/*
                                // Create a date range for the last 7 days
                                var start = moment().subtract(7, 'days').format('YYYY-MM-DD') // 7 days ago
                                var end = moment().format('YYYY-MM-DD') // today
                                var range = moment.range(start, end)

                                // Create the graph data
                                // Iterate the date range and assign a random value for each day
                                for (let moment of range.by('days')) {
                                    data.push({
                                        y: Math.floor(Math.random() * 300) + 10,
                                        x: moment.toDate()
                                    })
                                }
*/


//!!!!!CHANGE THESE BITS
        var data = [];

// dapatkan jumlah leads per RANGE, tampilkan di bagian jumlah LEADS
        <?php
        if(isset($lead_date_to) && !empty($lead_date_to)) {
            echo "     ";
            echo "var end = moment('".$lead_date_to."').format('YYYY-MM-DD'); ";
            echo "\r\n";
        } else {
            echo "var end = moment().format('YYYY-MM-DD'); ";
        }

        if(isset($lead_date_from) && !empty($lead_date_from)) {
            echo "     ";
            echo "var start = moment('".$lead_date_from."').format('YYYY-MM-DD'); ";
            echo "\r\n";
        } else {
          // Create a date range for the last 7 days
            echo "var start = moment().subtract(7, 'days').format('YYYY-MM-DD'); ";
        }
        ?>

        var range = moment.range(start, end);

        // pass PHP variable declared above to JavaScript variable
        var dates_data = <?php echo $dates_to_js ?>;
        var leads_data = <?php echo $leads_to_js ?>;
//alert(dates_data[0]);
        var point_value = 0;
// dapatkan jumlah leads per days, masukkan ke array data[]
        // Create the graph data
        // Iterate the date range and assign a random value for each day
        for (let moment of range.by('days')) {

            if(dates_data.includes(moment.format('YYYY-MM-DD')) ) {
                point_value = leads_data[dates_data.indexOf(moment.format('YYYY-MM-DD'))]

                data.push({
                    //ambil data dari array jumlah lead per tanggal input di sini
                    y: point_value,
                    x: moment.toDate()
                })
            }
            //alert(moment.format('YYYY-MM-DD'));
        }




                                var data = {
                                    datasets: [{
                                        label: "All Views",
                                        data
                                    }]
                                }

                                Charts.create(id, type, options, data)
                            }

                            ///////////////////
                            // Create Charts //
                            ///////////////////
                            Views('#viewsChart')

                        })()
*/
                    </script>



                          <script>

                    //      $(document).on("focusout","#cobain",function() {
                              // change focusout
                              //alert("hello");
                              //if($('#lchart_range').val() != ''){
                              //   $('#form').submit();
                    /*
                              if($('#lchart_range').length > 20){
                                 alert("hello");
                              } else {
                                event.preventDefault();
                              }
                    */
                    //          if($('#lchart_range').length < 20)
                    //          event.preventDefault();
                    //          else
                    //            alert("hello");

                    //      });

                          $(document).ready(function() {

                            var now_date = moment().format("DD/MM/YYYY");
                            var amonth_ago = moment().subtract(30, 'days').format("DD/MM/YYYY");

                            //$flatpickr("#lchart_range", {
                            $("#lchart_range").flatpickr({
                                mode: "range",
                                altInput: true,
                                altFormat: "d/m/Y",
                                dateFormat: "d/m/Y",
                                allowInput: false,
                                <?php
                                if(isset($lead_date_fromS) && !empty($lead_date_fromS)) {
                                    echo 'defaultDate: ["'.$lead_date_fromS.'", "'.$lead_date_toS.'"],';
                                } else {
                                    echo 'defaultDate: [amonth_ago, now_date],';
                                }
                                // onChange onClose onValueUpdate
                                ?>
                                /*
                                onClose: function(selectedDates, dateStr, instance) {
                                    //$(this).updateLineChart("UpdateDateRange");
                                    $(this).updateLineChart("SearchLeadBy");
                                }
                                */
                                //event.preventDefault();
                            });

                    /*
                            var picker = $flatpickr(".flatpickr");
                            picker.byID("lchart_range").config.onChange = function(dateobj, datestr){
                                //console.info(dateobj, datestr);
                                alert("hello "+dateStr);
                            }
                    */

/*
                            $("#ra_range").flatpickr({
                                mode: "range",
                                altInput: true,
                                altFormat: "d/m/Y",
                                dateFormat: "d/m/Y",
                                allowInput: false,
                                <?php
/*
                                if(isset($date_fromS) && !empty($date_fromS)) {
                                    echo 'defaultDate: ["'.$date_fromS.'", "'.$date_toS.'"],';
                                } else {
                                    echo 'defaultDate: [amonth_ago, now_date],';
                                }
                                // onChange onClose onValueUpdate
*/
                                ?>
                                onClose: function(selectedDates, dateStr, instance) {
                                    $(this).updateLineChart("UpdateDateRange");
                                }
                            });
*/

                  //if(empty($("#lchart_range")) || (!empty($("#lchart_range")) && $("#lchart_range").length < 12)) {
    //                if(empty($("#lchart_range"))) {
    //                  event.preventDefault();
    //              }
    //              $(this).updateLineChart();
    //alert("hello");
                              $.fn.updateLineChart = function(form_to_send) {
                                  var act_upLineChart = form_to_send+"_<?php echo $_SESSION["butts_Update"] ?>";
//alert("hello");
                                  submitbutton(act_upLineChart,form_to_send);
                              }

                              $("#lead_status").on("change",function() {
                                        $(this).updateLineChart("UpdateLeadStatus");
                                        //

                                        //alert("hello");
                                        //if($('#lchart_range').val() != ''){
                                        //   $('#form').submit();
                              /*
                                        if($('#lchart_range').length > 20){
                                           alert("hello");
                                        } else {
                                          event.preventDefault();
                                        }
                              */
                              //          if($('#lchart_range').length < 20)
                              //          event.preventDefault();
                              //          else
                              //            alert("hello");

                              });

                              $("[data-time-format]").each(function() {
                                  var el = $( this );
                                  switch(el.attr("data-time-format")) {
                                      case "time-ago":
                                        var timeValue = el.attr("data-time-value");
                                        var strTimeAgo = moment(timeValue).fromNow();
                                        el.text(strTimeAgo);
                                      break;
                                  }
                              });//$( "[data-time-format]" ).each(function() {

                              $(".btn-search").on("click", function(element){
                                  $(this).updateLineChart("SearchLeadBy");
                              });

                              $(".btn-excell").on("click", function(element){
                                  //var act_button = "ExportToXLS_<?php //echo $_SESSION["butts_Enter"] ?>";
                                  //var form_to_send = "ExportToXLS";
                                  //submitbutton(act_button,form_to_send);
                                  //$(this).updateLineChart("ExportToXLS");
                                  $(this).updateLineChart("SearchLeadBy");
                              });



                          });
                          </script>
