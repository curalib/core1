                  <div class="container-fluid page__container">
                        <div class="page__heading d-flex align-items-center">
                            <div class="flex">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item"><a href="<?php echo $bchref?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101 ?></a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Invoice Report<?php //echo $lang_50100 ?></li>
                                    </ol>
                                </nav>
                            </div>

                        </div>
                  </div>
                  <div class="container-fluid page__container">
                    <?php
                    if(isset($_SESSION["AlertMess"]) OR !empty($_SESSION["AlertMess"])){
                        echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
                            echo '<i class="material-icons mr-3">error_outline</i>';
                            echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
                        echo '</div>';
                        //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                        unset($_SESSION["AlertMess"]);
                    }
                    ?>

<!--
                        <div class="card-group">
                            <div class="card card-body text-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0"> Total Invoice</div>
                                    <div class="text-amount ml-auto">142</div>
                                </div>
                            </div>
                            <div class="card card-body text-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0">Izin</div>
                                    <div class="text-amount ml-auto">120</div>
                                </div>
                            </div>
                            <div class="card card-body text-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0"> Ijin</div>
                                    <div class="text-amount ml-auto">0</div>
                                </div>
                            </div>
                            <div class="card card-body text-center">
                                <div class="d-flex flex-row align-items-center">
                                    <div class="card-header__title m-0">vOffice</div>
                                    <div class="text-amount ml-auto">0</div>
                                </div>
                            </div>
                        </div>
-->


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center text-center mb-4">
                                            <div class="border-right pr-4 mr-4">
                                                <div class="mb-0">Invoice </div>
                                                <div class="text-amount"><?php echo $Count_AllInvs ?></div>
                                            </div>

                                            <div class="ml-auto">
                                                <form action="" method="post" name="UpdateDateRange" id ="UpdateDateRange" enctype="multipart/form-data">
                                                    <input type="hidden" name="task" />
                                                    <?php
                                                    echo '<input id="lchart_range" name="lchart_range" value="" type="text" class="form-control flatpickr" data-toggle="flatpickr">';
                                                    ?>
                                                </form>

                                                <!--<a href="#" class="btn btn-warning">Filter Company :</a>-->
                                            </div>



                            <div class="ml-auto">
                                <!--<div class="form-group"> -->
                                    <form action="" method="post" name="UpdateInvStatus" id ="UpdateInvStatus" enctype="multipart/form-data">
                                        <input type="hidden" name="task" />
                                        <input type="hidden" name="lchart_range" value="<?php echo $inv_date_range ?>" />
                                        <select name="inv_status" id="inv_status" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                          <!--   <option selected value="">By Leads</option> name="select01" id="select01" data-toggle="select" class="form-control"      -->
                                          <option value="SENT" <?php echo $inv_status == 'SENT' ? 'selected> ' : '> ';?> Waiting </option>
                                          <option value="PAID" <?php echo $inv_status == 'PAID' ? ' selected> ' : '> ';?> Paid </option>

                                        </select>
                                    </form>
                                <!--</div>-->
                            </div>




                                        </div>
                                        <div class="chart" style="height: 295px;">
                                            <canvas id="viewsChart">
                                                <span style="font-size: 1rem;"><strong>Views</strong> area chart goes here.</span>
                                            </canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="card card-form">
                            <div class="row no-gutters">
                                <div class="col-lg-12 card-form__body">

                                    <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                        <table class="table mb-0 thead-border-top-0">
                                            <thead>
                                                <tr>
                                                    <th style="width: 18px;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
                                                            <label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
                                                        </div>
                                                    </th>
<!--
                                                    <th style="width: 100px;">Year</th>
                                                    <th style="width: 100px;">Month</th>
                                                    <th style="width: 37px;">Invoice</th>
                                                    <th style="width: 237px;">Revenue</th>
                                                    <th style="width: 24px;"></th>
-->
                                                    <th style="width: 20%;text-align:center">Year</th>
                                                    <th style="width: 20%;text-align:center">Month</th>
                                                    <th style="width: 20%;text-align:center">Invoice</th>
                                                    <th style="width: 20%;text-align:center;">Revenue</th>
                                                    <th style="width: 20%;"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="list" id="staff">
<!--
                                                <tr class="selected">
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_1">
                                                            <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                                                        </div>
                                                    </td>
                                                    <td>2019</td>
                                                    <td>January</td>
                                                    <td>52</td>
                                                    <td>Rp. 600.000.000 </td>
                                                    <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                </tr>
-->
                                                <?php
                                                //while($ViewTrack = $dbs->getAssoc($rcViewTrack)) {
                                                while($InvData = $dbs->getAssoc($rcInvData)) {
                                                ?>
                                                <tr>
<!--
                                                <tr class="selected">
-->
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_1">
                                                            <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                                                        </div>
                                                    </td>
                                                    <td align="center"><?php echo $InvData["InvYear"] ?></td>
                                                    <td align="center"><?php $dt = DateTime::createFromFormat('!m', $InvData["InvMonth"]); echo $dt->format('F'); ?></td>
                                                    <td align="center"><?php echo $InvData["TotalRows"] ?></td>
                                                    <td align="right">Rp. <?php echo number_format($InvData["TotalDue"],0,",","."); ?></td>
                                                    <td align="center"><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>

                     </div>








          <!-- SCRIPT untuk LINE CHART (Tidak perlu yang lain??) -->
          		    <!-- Moment.js -->
            <script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment.min.js"></script>
          	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment-range.js"></script>
          	<script>
              (function() {
                  'use strict';
                  window['moment-range'].extendMoment(moment);
              })()
          	</script>
          <!-- SCRIPT untuk LINE CHART (Tidak perlu yang lain??) -->


          <!-- SCRIPT untuk SOURCE LEAD CHART (Tidak perlu yang lain??) -->

          	<!-- Global Settings --> <!-- cHART SOURCE LEAD -->
          	<script src="<?php echo BASEURL ?>assets/js/izin_js/settings.js"></script>
          	<!-- Chart.js -->  <!-- cHART SOURCE LEAD -->
          	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-Chart.min.js"></script>
          	<!-- App Charts JS -->  <!-- cHART SOURCE LEAD -->
          	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-charts.js"></script>




                         <!-- Chart Samples -->


                         <script>
                             (function() {
                                 'use strict';

                                 Charts.init()

                                 var Views = function(id, type = 'line', options = {}) {
                                     options = Chart.helpers.merge({
                                         elements: {
                                             line: {
                                                 fill: 'start',
                                                 backgroundColor: settings.charts.colors.area,
                                                 tension: 0,
                                                 borderWidth: 1
                                             },
                                             point: {
                                                 pointStyle: 'circle',
                                                 radius: 5,
                                                 hoverRadius: 5,
                                                 backgroundColor: settings.colors.white,
                                                 borderColor: settings.colors.primary[700],
                                                 borderWidth: 2
                                             }
                                         },
                                         scales: {
                                             xAxes: [{
                                                 gridLines: {
                                                     display: false
                                                 },
                                                 type: 'time',
                                                 time: {
                                                     unit: 'day'
                                                 }
                                             }]
                                         },
                                         tooltips: {
                                             callbacks: {
                                                 label: function(a, e) {
                                                     var t = e.datasets[a.datasetIndex].label || "",
                                                         o = a.yLabel,
                                                         r = "";
                                                     return 1 < e.datasets.length && (r += '<span class="popover-body-label mr-auto">' + t + "</span>"), r += '<span class="popover-body-value">' + o + " invoice</span>"
                                                 }
                                             }
                                         }
                                     }, options)



/*
                                     var data = []
                                     // Create a date range for the last 7 days
                                     var start = moment().subtract(7, 'days').format('YYYY-MM-DD') // 7 days ago
                                     var end = moment().format('YYYY-MM-DD') // today
                                     var range = moment.range(start, end)
                                     // Create the graph data
                                     // Iterate the date range and assign a random value for each day
                                     for (let moment of range.by('days')) {
                                         data.push({
                                             y: Math.floor(Math.random() * 300) + 10,
                                             x: moment.toDate()
                                         })
                                     }
*/


//!!!!!CHANGE THESE BITS
        var data = [];

// dapatkan jumlah leads per RANGE, tampilkan di bagian jumlah LEADS
        <?php
        if(isset($inv_date_to) && !empty($inv_date_to)) {
            echo "     ";
            echo "var end = moment('".$inv_date_to."').format('YYYY-MM-DD'); ";
            echo "\r\n";
        } else {
            echo "var end = moment().format('YYYY-MM-DD'); ";
        }

        if(isset($inv_date_from) && !empty($inv_date_from)) {
            echo "     ";
            echo "var start = moment('".$inv_date_from."').format('YYYY-MM-DD'); ";
            echo "\r\n";
        } else {
          // Create a date range for the last 7 days
            echo "var start = moment().subtract(90, 'days').format('YYYY-MM-DD'); ";
        }
        ?>

        var range = moment.range(start, end);

        // pass PHP variable declared above to JavaScript variable
        var dates_data = <?php echo $dates_to_js ?>;
        var invoices_data = <?php echo $invoices_to_js ?>;
//alert(dates_data[0]);
        var point_value = 0;
// dapatkan jumlah leads per days, masukkan ke array data[]
        // Create the graph data
        // Iterate the date range and assign a random value for each day
        for (let moment of range.by('days')) {

            if(dates_data.includes(moment.format('YYYY-MM-DD')) ) {
                point_value = invoices_data[dates_data.indexOf(moment.format('YYYY-MM-DD'))]

                data.push({
                    //ambil data dari array jumlah lead per tanggal input di sini
                    y: point_value,
                    x: moment.toDate()
                })
            }
            //alert(moment.format('YYYY-MM-DD'));
        }





                                     var data = {
                                         datasets: [{
                                             label: "All Views",
                                             data
                                         }]
                                     }

                                     Charts.create(id, type, options, data)
                                 }

                                 ///////////////////
                                 // Create Charts //
                                 ///////////////////
                                 Views('#viewsChart')

                             })()
                         </script>



                         <script>
                         $(document).ready(function() {

                             var now_date = moment().format("DD/MM/YYYY");
                             var amonth_ago = moment().subtract(30, 'days').format("DD/MM/YYYY");

                             $("#lchart_range").flatpickr({
                                 mode: "range",
                                 altInput: true,
                                 altFormat: "d/m/Y",
                                 dateFormat: "d/m/Y",
                                 allowInput: false,
                                 <?php
                                 if(isset($inv_date_fromS) && !empty($inv_date_fromS)) {
                                     echo 'defaultDate: ["'.$inv_date_fromS.'", "'.$inv_date_toS.'"],';
                                 } else {
                                     echo 'defaultDate: [amonth_ago, now_date],';
                                 }
                                 ?>

                                 onClose: function(selectedDates, dateStr, instance) {
                                     $(this).updateLineChart("UpdateDateRange");
                                 }
                             });

                             $.fn.updateLineChart = function(form_to_send) {
                                 var act_upLineChart = form_to_send+"_<?php echo $_SESSION["butts_Update"] ?>";
                                 submitbutton(act_upLineChart,form_to_send);
                             }


                             $("#inv_status").on("change",function() {
                                 $(this).updateLineChart("UpdateInvStatus");
                             });



                         });
                         </script>
