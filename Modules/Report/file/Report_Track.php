<div class="container-fluid page__container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="<?php echo $bchref ?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101
                                                                                                                            ?></a></li>
                    <li class="breadcrumb-item active" aria-current="page">Tracking Report<?php //echo $lang_50100
                                                                                            ?></li>
                </ol>
            </nav>
        </div>

    </div>
</div>




<div class="container-fluid page__container">
    <?php
    if (isset($_SESSION["AlertMess"]) or !empty($_SESSION["AlertMess"])) {
        echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
        echo '<i class="material-icons mr-3">error_outline</i>';
        echo '<div class="text-body"><strong>Alert - </strong> ' . $_SESSION["AlertMess"] . '.</div>';
        echo '</div>';
        //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
        unset($_SESSION["AlertMess"]);
    }
    ?>


    <div class="card-group">
        <div class="card card-body text-center">

            <div class="d-flex flex-row align-items-center">
                <div class="card-header__title m-0"> <i class="material-icons icon-muted icon-30pt">perm_identity</i><?php echo $track_type == 'TR' ? 'Tracking Running' : 'Finished Permit'; ?></div>
                <div class="text-amount ml-auto"><?php echo $Count_AllTrack ?></div>
            </div>
        </div>
        <div class="card card-body text-center">
            <div class="d-flex flex-row align-items-center">
                <div class="card-header__title m-0"><i class="material-icons icon-muted icon-30pt">network_check</i> Accurate</div>
                <div class="text-amount ml-auto"><?php echo $Count_AccTrack ?></div>
            </div>
        </div>
        <div class="card card-body text-center">
            <div class="d-flex flex-row align-items-center">
                <div class="card-header__title m-0"><i class="material-icons  icon-muted icon-30pt">mood_bad</i> Timeline over</div>
                <div class="text-amount ml-auto"><?php echo $Count_OvrTrack ?></div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center text-center mb-4">
                        <div class="border-right pr-4 mr-4">
                            <div class="mb-0">Tracking </div>
                            <div class="text-amount"><?php echo $Count_AllTrack ?></div>
                        </div>

                        <div class="ml-auto">
                            <!--<a href="#" class="btn btn-primary">Filter Date :</a>-->
                            <form action="" method="post" name="UpdateLineChart" id="UpdateLineChart" enctype="multipart/form-data">
                                <input type="hidden" name="task" />
                                <input type="hidden" name="track_type" value="<?php echo $track_type ?>" />
                                <?php
                                echo '<input id="lchart_range" name="lchart_range" value="" type="text" class="flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
                                ?>
                            </form>
                        </div>


                        <div class="ml-auto">
                            <!--<div class="form-group"> -->
                            <form action="" method="post" name="UpdateTrackStatus" id="UpdateTrackStatus" enctype="multipart/form-data">
                                <input type="hidden" name="task" />
                                <input type="hidden" name="lchart_range" value="<?php echo $track_date_range ?>" />
                                <select name="track_type" id="track_type" data-toggle="select" class="form-control" style="min-width: 200px!;">
                                    <!--   <option selected value="">By Leads</option> name="select01" id="select01" data-toggle="select" class="form-control"      -->
                                    <option value="TR" <?php echo $track_type == 'TR' ? 'selected> ' : '> '; ?>Tracking Running</option>
                                    <option value="FP" <?php echo $track_type == 'FP' ? ' selected> ' : '> '; ?>Finished Permit</option>

                                </select>
                            </form>
                            <!--</div>-->
                        </div>



                    </div>
                    <div class="chart" style="height: 295px;">
                        <canvas id="viewsChart">
                            <span style="font-size: 1rem;"><strong>Views</strong> area chart goes here.</span>
                        </canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>



<!-- SCRIPT untuk LINE CHART (Tidak perlu yang lain??) -->
<!-- Moment.js -->
<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment.min.js"></script>
<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment-range.js"></script>
<script>
    (function() {
        'use strict';
        window['moment-range'].extendMoment(moment);
    })()
</script>
<!-- SCRIPT untuk LINE CHART (Tidak perlu yang lain??) -->


<!-- SCRIPT untuk SOURCE LEAD CHART (Tidak perlu yang lain??) -->

<!-- Global Settings -->
<!-- cHART SOURCE LEAD -->
<script src="<?php echo BASEURL ?>assets/js/izin_js/settings.js"></script>
<!-- Chart.js -->
<!-- cHART SOURCE LEAD -->
<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-Chart.min.js"></script>
<!-- App Charts JS -->
<!-- cHART SOURCE LEAD -->
<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-charts.js"></script>







<!-- Chart Samples -->
<script>
    (function() {
        'use strict';

        Charts.init()

        var Views = function(id, type = 'line', options = {}) {
            options = Chart.helpers.merge({
                elements: {
                    line: {
                        fill: 'start',
                        backgroundColor: settings.charts.colors.area,
                        tension: 0,
                        borderWidth: 1
                    },
                    point: {
                        pointStyle: 'circle',
                        radius: 5,
                        hoverRadius: 5,
                        backgroundColor: settings.colors.white,
                        borderColor: settings.colors.primary[700],
                        borderWidth: 2
                    }
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        type: 'time',
                        time: {
                            unit: 'day'
                        }
                    }]
                },
                tooltips: {
                    callbacks: {
                        label: function(a, e) {
                            var t = e.datasets[a.datasetIndex].label || "",
                                o = a.yLabel,
                                r = "";
                            return 1 < e.datasets.length && (r += '<span class="popover-body-label mr-auto">' + t + "</span>"), r += '<span class="popover-body-value">' + o + " tracks</span>"
                        }
                    }
                }
            }, options)

            /*
                                                  var data = []

                                                  // Create a date range for the last 7 days
                                                  var start = moment().subtract(7, 'days').format('YYYY-MM-DD') // 7 days ago
                                                  var end = moment().format('YYYY-MM-DD') // today
                                                  var range = moment.range(start, end)

                                                  // Create the graph data
                                                  // Iterate the date range and assign a random value for each day
                                                  for (let moment of range.by('days')) {
                                                      data.push({
                                                          y: Math.floor(Math.random() * 300) + 10,
                                                          x: moment.toDate()
                                                      })
                                                  }
            */


            //!!!!!CHANGE THESE BITS
            var data = [];

            // dapatkan jumlah leads per RANGE, tampilkan di bagian jumlah LEADS
            <?php
            if (isset($track_date_to) && !empty($track_date_to)) {
                echo "     ";
                echo "var end = moment('" . $track_date_to . "').format('YYYY-MM-DD'); ";
                echo "\r\n";
            } else {
                echo "var end = moment().format('YYYY-MM-DD'); ";
            }

            if (isset($track_date_from) && !empty($track_date_from)) {
                echo "     ";
                echo "var start = moment('" . $track_date_from . "').format('YYYY-MM-DD'); ";
                echo "\r\n";
            } else {
                // Create a date range for the last 7 days
                echo "var start = moment().subtract(7, 'days').format('YYYY-MM-DD'); ";
            }
            ?>

            var range = moment.range(start, end);

            // pass PHP variable declared above to JavaScript variable
            var dates_data = <?php echo $dates_to_js ?>;
            var tracks_data = <?php echo $tracks_to_js ?>;
            //alert(dates_data[0]);
            var point_value = 0;
            // dapatkan jumlah leads per days, masukkan ke array data[]
            // Create the graph data
            // Iterate the date range and assign a random value for each day
            for (let moment of range.by('days')) {

                if (dates_data.includes(moment.format('YYYY-MM-DD'))) {
                    point_value = tracks_data[dates_data.indexOf(moment.format('YYYY-MM-DD'))]

                    data.push({
                        //ambil data dari array jumlah lead per tanggal input di sini
                        y: point_value,
                        x: moment.toDate()
                    })
                }
                //alert(moment.format('YYYY-MM-DD'));
            }






            var data = {
                datasets: [{
                    label: "All Tracks",
                    data
                }]
            }

            Charts.create(id, type, options, data)
        }

        ///////////////////
        // Create Charts //
        ///////////////////
        Views('#viewsChart')

    })()
</script>






<!-- Chart Samples -->
<!--
                          <script>
                              (function() {
                                  'use strict';

                                  Charts.init()

                                  var EarningsTraffic = function(id, type = 'line', options = {}) {
                                      options = Chart.helpers.merge({
                                          elements: {
                                              line: {
                                                  fill: 'start',
                                                  backgroundColor: settings.charts.colors.area
                                              }
                                          }
                                      }, options)

                                      var data = {
                                          labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                                          datasets: [{
                                              label: "Traffic",
                                              data: [10, 2, 5, 15, 10, 12, 15, 25, 22, 30, 25, 40]
                                          }]
                                      }

                                      Charts.create(id, type, options, data)
                                  }

                                  var Products = function(id, type = 'line', options = {}, data) {
                                      options = Chart.helpers.merge({
                                          elements: {
                                              line: {
                                                  fill: 'start',
                                                  backgroundColor: settings.charts.colors.area,
                                                  tension: 0,
                                                  borderWidth: 1
                                              },
                                              point: {
                                                  pointStyle: 'circle',
                                                  radius: 5,
                                                  hoverRadius: 5,
                                                  backgroundColor: settings.colors.white,
                                                  borderColor: settings.colors.primary[700],
                                                  borderWidth: 2
                                              }
                                          },
                                          scales: {
                                              yAxes: [{
                                                  display: false
                                              }],
                                              xAxes: [{
                                                  display: false
                                              }]
                                          }
                                      }, options)

                                      data = data || {
                                          labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                                          datasets: [{
                                              label: "Earnings",
                                              data: [400, 200, 450, 460, 220, 380, 800]
                                          }]
                                      }

                                      Charts.create(id, type, options, data)
                                  }

                                  var Courses = function(id, type = 'line', options = {}) {
                                      options = Chart.helpers.merge({
                                          elements: {
                                              line: {
                                                  borderColor: settings.colors.success[700],
                                                  backgroundColor: settings.hexToRGB(settings.colors.success[100], 0.5)
                                              },
                                              point: {
                                                  borderColor: settings.colors.success[700]
                                              }
                                          }
                                      }, options)

                                      Products(id, type, options)
                                  }

                                  var LocationDoughnut = function(id, type = 'doughnut', options = {}) {
                                      options = Chart.helpers.merge({
                                          tooltips: {
                                              callbacks: {
                                                  title: function(a, e) {
                                                      return e.labels[a[0].index]
                                                  },
                                                  label: function(a, e) {
                                                      var t = "";
                                                      return t += '<span class="popover-body-value">' + e.datasets[0].data[a.index] + "%</span>"
                                                  }
                                              }
                                          }
                                      }, options)

                                      var data = {
                                          labels: ["WhatsApp", "Email", "Call Center", "Referral vOffice", "Referral Unionspace", "Instagram", "Facebook", "Walk In"],
                                          datasets: [{
                                              data: [20, 20, 5, 3, 5, 10, 5, 5,],
                                              backgroundColor: [settings.colors.success[400], settings.colors.danger[400], settings.colors.primary[500], settings.colors.gray[300], settings.colors.gray[300], settings.colors.primary[500], settings.colors.primary[500], settings.colors.primary[500]],
                                              hoverBorderColor: "dark" == settings.charts.colorScheme ? settings.colors.gray[800] : settings.colors.white
                                          }]
                                      }

                                      Charts.create(id, type, options, data)
                                  }

                                  ///////////////////
                                  // Create Charts //
                                  ///////////////////

                                  EarningsTraffic('#earningsTrafficChart')
                                  LocationDoughnut('#locationDoughnutChart')
                                  Products('#productsChart')
                                  Courses('#coursesChart')

                              })()
                          </script>
-->


<script>
    $(document).ready(function() {

        var now_date = moment().format("DD/MM/YYYY");
        var amonth_ago = moment().subtract(30, 'days').format("DD/MM/YYYY");

        $("#lchart_range").flatpickr({
            mode: "range",
            altInput: true,
            altFormat: "d/m/Y",
            dateFormat: "d/m/Y",
            allowInput: false,
            <?php
            if (isset($track_date_fromS) && !empty($track_date_fromS)) {
                echo 'defaultDate: ["' . $track_date_fromS . '", "' . $track_date_toS . '"],';
            } else {
                echo 'defaultDate: [amonth_ago, now_date],';
            }
            // onChange onClose onValueUpdate
            ?>
            onClose: function(selectedDates, dateStr, instance) {
                //alert("hello "+dateStr);
                //$(this).updateLineChart();
                $(this).updateLineChart("UpdateLineChart");
            }
            //event.preventDefault();
        });

        $.fn.updateLineChart = function(form_to_send) {
            var act_upLineChart = form_to_send + "_<?php echo $_SESSION["butts_Update"] ?>";
            submitbutton(act_upLineChart, form_to_send);
        }

        $("#track_type").on("change", function() {
            $(this).updateLineChart("UpdateTrackStatus");
        });



    });
</script>