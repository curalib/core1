<?php


if(isset($_SESSION["ReportPage"])) {
 unset($_SESSION["ReportPage"]);
}

//BREADCRUMB
$cBrdCrumb = "SELECT tgl FROM ".$tblp."sys_menu WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL.$bcsplitKeys."/0/0.html";

// TOTAL LEADS, CLOSE, LOST DATA
//-------------------------------------------------
//Total Leads
/*
$cAllLeads = "SELECT COUNT(`leads`.`id_lead`) AS `count_leads` FROM `izin_apps_lead` AS `leads`  ";
//$cAllLeads .= "WHERE str_to_date(`leads`.`tgl_update`, '%Y-%m-%d') = '".$date_now."' ";
//$cAllLeads .= "WHERE str_to_date(`leads`.`tgl_update`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."' ";
$cAllLeads .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";
if(isset($lead_status) && !empty($lead_status) && $lead_status != "All") {
    $cAllLeads .= "AND `leads`.`status` = '".$lead_status."' ";
}
$rcAllLeads = $dbs->getQuery($cAllLeads);
$AllLeads = $dbs->getAssoc($rcAllLeads);
$Count_AllLeads = $AllLeads["count_leads"];

//Leads Close / Deal
$cCloseLeads = "SELECT COUNT(`leads`.`id_lead`) AS `count_close_leads` FROM `izin_apps_lead` AS `leads`  ";
$cCloseLeads .= "WHERE str_to_date(`leads`.`tgl_update`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";
$cCloseLeads .= "AND `leads`.`status` = 'CL' ";
$rcCloseLeads = $dbs->getQuery($cCloseLeads);
$CloseLeads = $dbs->getAssoc($rcCloseLeads);
$Count_CloseLeads = $CloseLeads["count_close_leads"];

//Leads Lost
$cLostLeads = "SELECT COUNT(`leads`.`id_lead`) AS `count_lost_leads` FROM `izin_apps_lead` AS `leads`  ";
$cLostLeads .= "WHERE str_to_date(`leads`.`tgl_update`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";
$cLostLeads .= "AND `leads`.`status` = 'LO' ";
$rcLostLeads = $dbs->getQuery($cLostLeads);
$LostLeads = $dbs->getAssoc($rcLostLeads);
$Count_LostLeads = $LostLeads["count_lost_leads"];
*/


// LINE CHART DATA
//-------------------------------------------------
/*
$LC_dates = array();
$LC_leads = array();

$cLineChartData = "SELECT `leads`.`tgl_input`, COUNT(`leads`.`tgl_input`) AS JumlahLead FROM `izin_apps_lead` AS `leads`  ";
$cLineChartData .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";
if(isset($lead_status) && !empty($lead_status) && $lead_status != "All") {
    $cLineChartData .= "AND `leads`.`status` = '".$lead_status."' ";
}
$cLineChartData .= "GROUP BY str_to_date(`tgl_input`, '%Y-%m-%d') ";
$rcLineChartData = $dbs->getQuery($cLineChartData);
while($LineChartData = $dbs->getAssoc($rcLineChartData)) {
    $LC_leads[] = $LineChartData["JumlahLead"] ;
    $LC_dates[] = date("Y-m-d",strtotime($LineChartData["tgl_input"])) ;//
}
$leads_sum = array_sum($LC_leads);
$dates_to_js = json_encode($LC_dates);
$leads_to_js = json_encode($LC_leads);
*/



// SEARCH LEAD BY
//-------------------------------------------------

// STATUS

// SALES
$cLeadSalesData = "SELECT `sales`.`id`, `sales`.`nama_asli` FROM `izin_sys_lgndetail` AS `sales` ";
//$cLeadSalesData .= "WHERE `sales`.`priv` = 4 ";
$cLeadSalesData .= "WHERE `sales`.`jabatan` = 'Sales' AND `sales`.`jabatan` = 'Manager' ";
$rcLeadSalesData = $dbs->getQuery($cLeadSalesData);

// PACKAGES
$cLeadPackagesData = "SELECT `packages`.`kode`, `packages`.`nama` FROM `izin_apps_package` AS `packages` ";
$cLeadPackagesData .= "GROUP BY `packages`.`kode` ";
$cLeadPackagesData .= "ORDER BY `packages`.`kode` ASC";
$rcLeadPackagesData = $dbs->getQuery($cLeadPackagesData);

// LOCATION
$cLeadLocationData = "SELECT `location`.`id`, `location`.`lokasi` FROM `izin_master_volocation` AS `location` ";
$cLeadLocationData .= "WHERE `location`.`publish` = 'Y' ";
$cLeadLocationData .= "ORDER BY `location`.`lokasi` ASC";
$rcLeadLocationData = $dbs->getQuery($cLeadLocationData);


// TRANSACTION
/*
$cLeadTransactionData = "SELECT `location`.`id`, `location`.`lokasi` FROM `izin_master_volocation` AS `lead` ";
$cLeadTransactionData .= "WHERE `location`.`publish` = 'Y' ";
$cLeadTransactionData .= "ORDER BY `location`.`lokasi` ASC";
$rcLeadTransactionData = $dbs->getQuery($cLeadTransactionData);

SELECT client_id, client_name, COUNT(*) AS cnt
FROM izin_apps_lead
GROUP BY client_id
HAVING cnt > 1
*/


$cLeadMainData = "SELECT `leads`.`id_lead`, `leads`.`client_wa`, `leads`.`client_email`, `leads`.`sales_name`, `leads`.`client_id`, `leads`.`client_name`, `leads`.`company_name`, `leads`.`package_name`, `leads`.`prefloc_name`, `leads`.`status_nm` ";
$cLeadMainData .= "FROM `izin_apps_lead` AS `leads` ";
$cLeadMainData .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";

if(isset($lead_status) && !empty($lead_status) && $lead_status!='all') {
    $cLeadMainData .= "AND `status` = '".$lead_status."' ";
}
if(isset($lead_sales) && !empty($lead_sales) && $lead_sales!='all') {
    $cLeadMainData .= "AND `sales_id` = '".$lead_sales."' ";
}
if(isset($lead_package) && !empty($lead_package) && $lead_package!='all') {
    $cLeadMainData .= "AND `package_id` = '".$lead_package."' ";
}
if(isset($lead_location) && !empty($lead_location) && $lead_location!='all') {
    $cLeadMainData .= "AND `prefloc_id` = '".$lead_location."' ";
}
if(isset($lead_transaction) && !empty($lead_transaction) && $lead_transaction!='all') {

    if($lead_transaction=='n') {
        $cLeadMainData .= "AND `client_id` NOT IN ";
    } else {
        //if($lead_transaction=='r')
        $cLeadMainData .= "AND `client_id` IN ";
    }
    //$cLeadMainData .= "(SELECT client_id, client_name, COUNT(*) AS cnt ";
    $cLeadMainData .= "(SELECT client_id ";
    $cLeadMainData .= "FROM izin_apps_lead ";
    $cLeadMainData .= "GROUP BY client_id ";
    //$cLeadMainData .= "HAVING cnt > 1) ";
    $cLeadMainData .= "HAVING COUNT(*) > 1) ";
}
//$rcLeadMainData = $dbs->getQuery($cLeadMainData);


$cLeadMainData2 = "SELECT COUNT(`leads`.`client_id`) AS jumlah ";
$cLeadMainData2 .= "FROM `izin_apps_lead` AS `leads` ";
$cLeadMainData2 .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";

if(isset($lead_status) && !empty($lead_status) && $lead_status!='all') {
    $cLeadMainData2 .= "AND `status` = '".$lead_status."' ";
}
if(isset($lead_sales) && !empty($lead_sales) && $lead_sales!='all') {
    $cLeadMainData2 .= "AND `sales_id` = '".$lead_sales."' ";
}
if(isset($lead_package) && !empty($lead_package) && $lead_package!='all') {
    $cLeadMainData2 .= "AND `package_id` = '".$lead_package."' ";
}
if(isset($lead_location) && !empty($lead_location) && $lead_location!='all') {
    $cLeadMainData2 .= "AND `prefloc_id` = '".$lead_location."' ";
}

if(isset($lead_transaction) && !empty($lead_transaction) && $lead_transaction!='all') {

    if($lead_transaction=='n') {
        $cLeadMainData2 .= "AND `client_id` NOT IN ";
    } else {
        //if($lead_transaction=='r')
        $cLeadMainData2 .= "AND `client_id` IN ";
    }
    //$cLeadMainData .= "(SELECT client_id, client_name, COUNT(*) AS cnt ";
    $cLeadMainData2 .= "(SELECT client_id ";
    $cLeadMainData2 .= "FROM izin_apps_lead ";
    $cLeadMainData2 .= "GROUP BY client_id ";
    $cLeadMainData2 .= "HAVING COUNT(*) > 1) ";
}

//if(isset($task) && $task == "ExportToXLS_".$_SESSION["butts_Enter"]) {
if(isset($_POST['export-excell'])) {

//echo "helloo";
//exit;



    $rcLeadMainData = $dbs->getQuery($cLeadMainData);


} else {

//  echo "helloo 2";
//  exit;

    $rl = new paging ($dbs,PAGE_ITEM,PAGE_LIST,-1,"LstFinished");
    $rl->queryTracking($cLeadMainData,$cLeadMainData2);
}


//echo "helloo 3";
//exit;















//echo $cLeadMainData;
//exit;



$lead_repeat_array = array();

$cLeadRepeatData = "SELECT client_id, client_name, COUNT(*) AS cnt ";
$cLeadRepeatData .= "FROM izin_apps_lead ";
$cLeadRepeatData .= "GROUP BY client_id ";
$cLeadRepeatData .= "HAVING cnt > 1 ";
$rcLeadRepeatData = $dbs->getQuery($cLeadRepeatData);
while($LeadRepeatData = $dbs->getAssoc($rcLeadRepeatData)) {

    $lead_repeat_array[$LeadRepeatData["client_id"]] = $LeadRepeatData["client_name"];

    //$lead_repeat_array[$LeadRepeatData["client_id"]] ++;
    //if(!isset($lead_status_value_array[$SourcePercentage["status"]])) {
    //  $lead_status_value_array[$SourcePercentage["status"]] = $SourcePercentage["status_nm"];
    //}
    //$TOTL++;

} //while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)){









// LEAD SOURCE PERCENTAGE
//-------------------------------------------------
//percentage data
$TOTL = 0;
$lead_status_count_array = array();
$lead_status_value_array = array();
//$src_item_count_array = array_fill(0, COUNT(SRC_ITEM), 0);
//$src_item_count_array = array_fill(0, COUNT(LEAD_STATUS), 0);

//echo SRC_ITEM;
//echo count(SRC_ITEM);
//exit;

/*
$cSourcePercentage = "SELECT `leads`.`source_id`, `leads`.`source` FROM `izin_apps_lead` AS `leads` ";
$cSourcePercentage .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";
//$cSourcePercentage .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '2019-02-01' AND '2019-07-01' ";
if(isset($lead_status) && !empty($lead_status) && $lead_status != "all") {
    $cSourcePercentage .= "AND `leads`.`status` = '".$lead_status."' ";
}
*/


$cSourcePercentage = "SELECT `leads`.`status`, `leads`.`status_nm` FROM `izin_apps_lead` AS `leads` ";
$cSourcePercentage .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";
//$cSourcePercentage .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '2019-02-01' AND '2019-07-01' ";
//if(isset($lead_status) && !empty($lead_status) && $lead_status != "all") {
//    $cSourcePercentage .= "AND `leads`.`status` = '".$lead_status."' ";
//}


if(isset($lead_status) && !empty($lead_status) && $lead_status!='all') {
    $cSourcePercentage .= "AND `status` = '".$lead_status."' ";
}
if(isset($lead_sales) && !empty($lead_sales) && $lead_sales!='all') {
    $cSourcePercentage .= "AND `sales_id` = '".$lead_sales."' ";
}
if(isset($lead_package) && !empty($lead_package) && $lead_package!='all') {
    $cSourcePercentage .= "AND `package_id` = '".$lead_package."' ";
}
if(isset($lead_location) && !empty($lead_location) && $lead_location!='all') {
    $cSourcePercentage .= "AND `prefloc_id` = '".$lead_location."' ";
}

if(isset($lead_transaction) && !empty($lead_transaction) && $lead_transaction!='all') {

    if($lead_transaction=='n') {
        $cSourcePercentage .= "AND `client_id` NOT IN ";
    } else {
        //if($lead_transaction=='r')
        $cSourcePercentage .= "AND `client_id` IN ";
    }
    $cSourcePercentage .= "(SELECT client_id ";
    //$cSourcePercentage .= "(SELECT client_id, client_name, COUNT(*) AS cnt ";
    $cSourcePercentage .= "FROM izin_apps_lead ";
    $cSourcePercentage .= "GROUP BY client_id ";
    $cSourcePercentage .= "HAVING  COUNT(*) > 1) ";
}



//echo $cSourcePercentage;
//exit;

$rcSourcePercentage = $dbs->getQuery($cSourcePercentage);

while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)) {

    $lead_status_count_array[$SourcePercentage["status"]]++;
    if(!isset($lead_status_value_array[$SourcePercentage["status"]])) {
      $lead_status_value_array[$SourcePercentage["status"]] = $SourcePercentage["status_nm"];
    }

    $TOTL++;
} //while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)){
//percentage
$src_item_percentage_array = array();
foreach($lead_status_count_array as $key => $value) {
    $src_item_percentage_array[$key] = round(($value/$TOTL)*100, 2);
}
$pecentage_data = implode(",",$src_item_percentage_array);

//bgcolor data
$i = 0;
//$sizeofarray = sizeof(SRC_ITEM);
$sizeofarray = sizeof($lead_status_count_array);
$bgcolor_data_rgb = array();
while($i < $sizeofarray) {
    $red_color = mt_rand(0, 255);
    $green_color = mt_rand(0, 255);
    $blue_color = mt_rand(0, 255);
    $bgcolor_data_rgb[] = "rgb(".$red_color.",".$green_color.",".$blue_color.")";
    $i++;
}
$bgcolor_data_raw = implode("', '",$bgcolor_data_rgb);
$bgcolor_data = "'".$bgcolor_data_raw."'";

//labels data
//$labels_data_raw = implode("', '",SRC_ITEM);
$labels_data_raw = implode("', '",$lead_status_value_array);
$labels_data = "'".$labels_data_raw."'";
//$labels_data = json_encode(SRC_ITEM);
//{"WA":"Whatsapp","EM":"Email","CC":"Call Center","RO":"Referral vOffice","RU":"Referral Unionspace","IN":"Instagram","FB":"Facebook","WI":"Walk In","LC":"Live Chat"}


// TOP PRODUCT INTERESTED DATA
//-------------------------------------------------
$cTopData = "SELECT COUNT(`leads`.`id_lead`) AS `JumlahLeads`, `leads`.`package_name` FROM `izin_apps_lead` AS `leads`  ";
$cTopData .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '".$lead_date_from."' AND '".$lead_date_to."' ";

if(isset($lead_status) && !empty($lead_status) && $lead_status!='all') {
    $cTopData .= "AND `status` = '".$lead_status."' ";
}
if(isset($lead_sales) && !empty($lead_sales) && $lead_sales!='all') {
    $cTopData .= "AND `sales_id` = '".$lead_sales."' ";
}
if(isset($lead_package) && !empty($lead_package) && $lead_package!='all') {
    $cTopData .= "AND `package_id` = '".$lead_package."' ";
}
if(isset($lead_location) && !empty($lead_location) && $lead_location!='all') {
    $cTopData .= "AND `prefloc_id` = '".$lead_location."' ";
}

if(isset($lead_transaction) && !empty($lead_transaction) && $lead_transaction!='all') {

    if($lead_transaction=='n') {
        $cTopData .= "AND `client_id` NOT IN ";
    } else {
        //if($lead_transaction=='r')
        $cTopData .= "AND `client_id` IN ";
    }
    $cTopData .= "(SELECT client_id ";
    $cTopData .= "FROM izin_apps_lead ";
    $cTopData .= "GROUP BY client_id ";
    $cTopData .= "HAVING COUNT(*) > 1) ";
}

$cTopData .= "GROUP BY `leads`.`package_name` ";
//$cTopData .= "ORDER BY `leads`.`package_name` ASC";
$cTopData .= "ORDER BY `JumlahLeads` DESC";
$rcTopData = $dbs->getQuery($cTopData);



  /******************************************
   * End of Report Leads
   ******************************************/



?>
