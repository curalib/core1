<?php

require "system/CreateExcel.php";

	$filename = 'Report Leads';

	$spreadsheet->getProperties()
    	->setCreator("Bontot")
    	->setLastModifiedBy("Bontot")
    	->setTitle("Report Leads")
    	->setSubject("Report Leads")
    	->setDescription("Report Leads List for Office 2007 XLSX, generated using PHP classes.")
    	->setKeywords("Report Leads izin")
    	->setCategory("Report");

	$spreadsheet->setActiveSheetIndex(0);
	$activesheet = $spreadsheet->getActiveSheet();

	$PageSetup = $activesheet->getPageMargins();
	$PageSetup->setTop(0.95);
	$PageSetup->setRight(0.65);
	$PageSetup->setLeft(0.65);
	$PageSetup->setBottom(0.95);

	$activesheet->getPageSetup()
		->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT)
		->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
		->setFitToWidth(0)
		->setFitToHeight(0);

	$activesheet->getDefaultRowDimension()->setRowHeight(27);
	$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
	$spreadsheet->getDefaultStyle()->getFont()->setSize(9);
	$spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
	$spreadsheet->getDefaultStyle()->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    $activesheet->getPageSetup()->setHorizontalCentered(true);
	$activesheet->setShowGridlines(false);
/*
    $activesheet->mergeCells('C2:D2');
	$activesheet->getStyle('C2:D2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $activesheet->setCellValue('C2', 'Finished Permit')->getStyle('A2')->getFont()->setBold(true);
    $activesheet->getStyle('C2')->getFont()->setSize(16);
    $activesheet->getStyle('C2')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
*/

	//$activesheet->getStyle('A4:F4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
	//$activesheet->getStyle('E:F')->getAlignment()->setHorizontal('center');
	$activesheet->getStyle('D')->getAlignment()->setHorizontal('center');
	$activesheet->getRowDimension('1')->setRowHeight(25);


	$activesheet->getColumnDimension('A')->setWidth(5);
        $activesheet->setCellValue('A1', 'NO')->getStyle('A1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('B')->setWidth(9);
	$activesheet->setCellValue('B1', 'ID')->getStyle('B1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('C')->setWidth(18);
	$activesheet->setCellValue('C1', 'CLIENT NAME')->getStyle('C1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('D')->setWidth(21);
	$activesheet->setCellValue('D1', 'COMPANY')->getStyle('D1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('E')->setWidth(21);
        $activesheet->setCellValue('E1', 'WHATSAPP/TELP')->getStyle('E1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('F')->setWidth(41);
        $activesheet->setCellValue('F1', 'EMAIL')->getStyle('F1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('G')->setWidth(15);
	$activesheet->setCellValue('G1', 'TRANSACTION')->getStyle('G1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('H')->setWidth(20);
	$activesheet->setCellValue('H1', 'PACKAGES')->getStyle('H1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('I')->setWidth(22);
	$activesheet->setCellValue('I1', 'LOCATION')->getStyle('I1')->getFont()->setBold(true);

        $activesheet->getColumnDimension('J')->setWidth(21);
        $activesheet->setCellValue('J1', 'PRICE')->getStyle('J1')->getFont()->setBold(true);

        $activesheet->getColumnDimension('K')->setWidth(21);
        $activesheet->setCellValue('K1', 'DISCOUNT')->getStyle('K1')->getFont()->setBold(true);

        $activesheet->getColumnDimension('L')->setWidth(21);
        $activesheet->setCellValue('L1', 'TOTAL AMOUNT')->getStyle('L1')->getFont()->setBold(true);

        $activesheet->getColumnDimension('M')->setWidth(21);
        $activesheet->setCellValue('M1', 'DATE PAID')->getStyle('M1')->getFont()->setBold(true);

        $activesheet->getColumnDimension('N')->setWidth(21);
        $activesheet->setCellValue('N1', 'SALES NAME')->getStyle('N1')->getFont()->setBold(true);

        $activesheet->getColumnDimension('O')->setWidth(41);
        $activesheet->setCellValue('O1', 'BANK ACCOUNT')->getStyle('O1')->getFont()->setBold(true);

	$activesheet->getColumnDimension('P')->setWidth(12);
	$activesheet->setCellValue('P1', 'STATUS')->getStyle('P1')->getFont()->setBold(true);
		$activesheet->getColumnDimension('Q')->setWidth(12);
		$activesheet->setCellValue('Q1', 'STATUS REASON')->getStyle('P1')->getFont()->setBold(true);
			$activesheet->getColumnDimension('R')->setWidth(12);
			$activesheet->setCellValue('R1', 'STATUS REASON')->getStyle('R1')->getFont()->setBold(true);
	$r = 2;
	$activesheet->getRowDimension($r)->setRowHeight(3);
//	$activesheet->getRowDimension($r)->setRowHeight(300);

	$num = 0;

	while($LeadMainData = $dbs->getAssoc($rcLeadMainData)) {
		$r++;
		$num++;
		$activesheet->setCellValue('A'.$r, $num);
		$activesheet->setCellValue('B'.$r, $LeadMainData["client_id"]);
		$activesheet->setCellValue('C'.$r, $LeadMainData["client_name"]);
		$activesheet->setCellValue('D'.$r, $LeadMainData["company_name"]);
		$activesheet->getCell('E'.$r)->setValueExplicit($LeadMainData["client_wa"], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
		$activesheet->setCellValue('F'.$r, $LeadMainData["client_email"]);
		if(!isset($lead_repeat_array[$LeadMainData["client_id"]])) {
				$Transaction = "New";
		} else {
				$Transaction = "Repeat";
		}
		$activesheet->setCellValue('G'.$r, $Transaction);
		$activesheet->setCellValue('H'.$r, $LeadMainData["package_name"]);
		$activesheet->setCellValue('I'.$r, $LeadMainData["prefloc_name"]);
		$activesheet->setCellValue('N'.$r, $LeadMainData["sales_name"]);
		$activesheet->setCellValue('P'.$r, $LeadMainData["status_nm"]);
		$activesheet->setCellValue('Q'.$r, $LeadMainData["status_reason"]);
		$activesheet->setCellValue('R'.$r, $LeadMainData["source"]);

		# Get invoice Detail
		$invdet = "select id, tglbayar, total_due, bank_acc from ".$tblp."apps_invdet where id_lead = '".$LeadMainData['id_lead']."'";
		$rinvdet = $dbs->getArr($invdet);

		if ( empty($rinvdet['total_due']) ){ $rinvdet['total_due'] = 0; }

		$activesheet->getStyle('L'.$r)->getNumberFormat()->setFormatCode('#,##0');
		$activesheet->setCellValue('L'.$r, $rinvdet["total_due"]);

                $activesheet->setCellValue('M'.$r, c_date ($rinvdet["tglbayar"],"shortdate","short",$list_month,$list_month_short,"Y"));

		$detbank = explode("|", BANK_ACC[$rinvdet['bank_acc']]);
		$detbankshow = $detbank[1]."\n".$rinvdet['bank_acc'];
		$activesheet->setCellValue('O'.$r, $detbankshow);

		$cInvDet2 = "select price from ".$tblp."apps_invitem where id_inv = '".$rinvdet['id']."' and no_urut = 1";
		$rcInvDet2 = $dbs->getArr($cInvDet2);

		/*if ( empty($rcInvDet2['price']) ){

                	$cInvDet2 = "select price from ".$tblp."apps_invitem where id_inv = '".$rinvdet['id']."' and no_urut = 2";
                	$rcInvDet2 = $dbs->getArr($cInvDet2);
		}*/

		$cInvDet3 = "select price from ".$tblp."apps_invitem where id_inv = '".$rinvdet['id']."' and no_urut = 5";
                $rcInvDet3 = $dbs->getArr($cInvDet3);

		$activesheet->getStyle('J'.$r)->getNumberFormat()->setFormatCode('#,##0');
                $activesheet->setCellValue('J'.$r, $rcInvDet2["price"]);

		$activesheet->getStyle('K'.$r)->getNumberFormat()->setFormatCode('#,##0');
                $activesheet->setCellValue('K'.$r, $rcInvDet3["price"]);

	}

/*
    while($r <= 100) {
		$r++;
if($r % 28 == 0){
	$activesheet->setCellValue('A'.$r, 'ID')->getStyle('A'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('B'.$r, 'Client Name')->getStyle('B'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('C'.$r, 'Product')->getStyle('C'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('D'.$r, 'Company Name')->getStyle('D'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('E'.$r, 'Start Date')->getStyle('E'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('F'.$r, 'Close Date')->getStyle('F'.$r)->getFont()->setBold(true);
	$r++;
	$activesheet->getRowDimension($r)->setRowHeight(3);
} else {
		$activesheet->setCellValue('A'.$r, "hello");
		$activesheet->setCellValue('B'.$r, "world");
		$activesheet->setCellValue('C'.$r, "world");
		$activesheet->setCellValue('D'.$r, "world");
		$activesheet->setCellValue('E'.$r, "world");
		$activesheet->setCellValue('F'.$r, "world");
}
    }
*/


	$styleArray = array(
    	'borders' => array(
        	'allBorders' => array(
            	'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            	'color' => array('argb' => 'FFCCCCCC'),
        	),
    	),
	);

	$activesheet->getStyle('A1:R'.$r)->applyFromArray($styleArray);
//    $activesheet->getStyle('A2:F'.$r)->getAlignment()->setWrapText(true);

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$filename.'.Xls"');
	header('Cache-Control: max-age=0');



	$writer->save('php://output');



/*
require_once("vendor/autoload.php");
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		$writer = new Xls($spreadsheet);

		$filename = 'simple';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
*/


?>
