<?php

require "system/CreateExcel.php";

	$filename = 'Report Leads';

	$spreadsheet->getProperties()
    	->setCreator("Bontot")
    	->setLastModifiedBy("Bontot")
    	->setTitle("Report Leads")
    	->setSubject("Report Leads")
    	->setDescription("Report Leads List for Office 2007 XLSX, generated using PHP classes.")
    	->setKeywords("Report Leads izin")
    	->setCategory("Report");

	$spreadsheet->setActiveSheetIndex(0);
	$activesheet = $spreadsheet->getActiveSheet();

	$PageSetup = $activesheet->getPageMargins();
	$PageSetup->setTop(0.95);
	$PageSetup->setRight(0.65);
	$PageSetup->setLeft(0.65);
	$PageSetup->setBottom(0.95);

	$activesheet->getPageSetup()
		->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT)
		->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
		->setFitToWidth(0)
		->setFitToHeight(0);

	$activesheet->getDefaultRowDimension()->setRowHeight(27);
	$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
	$spreadsheet->getDefaultStyle()->getFont()->setSize(9);
	$spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
	$spreadsheet->getDefaultStyle()->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

    $activesheet->getPageSetup()->setHorizontalCentered(true);
	$activesheet->setShowGridlines(false);
/*
    $activesheet->mergeCells('C2:D2');
	$activesheet->getStyle('C2:D2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $activesheet->setCellValue('C2', 'Finished Permit')->getStyle('A2')->getFont()->setBold(true);
    $activesheet->getStyle('C2')->getFont()->setSize(16);
    $activesheet->getStyle('C2')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
*/

	//$activesheet->getStyle('A4:F4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
	//$activesheet->getStyle('E:F')->getAlignment()->setHorizontal('center');
	$activesheet->getStyle('D')->getAlignment()->setHorizontal('center');
	$activesheet->getRowDimension('1')->setRowHeight(25);


	$activesheet->getColumnDimension('A')->setWidth(9);
	$activesheet->setCellValue('A1', 'ID')->getStyle('A1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('B')->setWidth(18);
	$activesheet->setCellValue('B1', 'Client Name')->getStyle('B1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('C')->setWidth(21);
	$activesheet->setCellValue('C1', 'Company')->getStyle('C1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('D')->setWidth(11);
	$activesheet->setCellValue('D1', 'Transaction')->getStyle('D1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('E')->setWidth(20);
	$activesheet->setCellValue('E1', 'Packages')->getStyle('E1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('F')->setWidth(12);
	$activesheet->setCellValue('F1', 'Location')->getStyle('F1')->getFont()->setBold(true);
	$activesheet->getColumnDimension('F')->setWidth(12);
	$activesheet->setCellValue('G1', 'Status')->getStyle('G1')->getFont()->setBold(true);

	$r = 2;
	$activesheet->getRowDimension($r)->setRowHeight(3);
//	$activesheet->getRowDimension($r)->setRowHeight(300);

	while($LeadMainData = $dbs->getAssoc($rcLeadMainData)) {
		$r++;
		$activesheet->setCellValue('A'.$r, $LeadMainData["client_id"]);
		$activesheet->setCellValue('B'.$r, $LeadMainData["client_name"]);
		$activesheet->setCellValue('C'.$r, $LeadMainData["company_name"]);
		if(!isset($lead_repeat_array[$LeadMainData["client_id"]])) {
				$Transaction = "New";
		} else {
				$Transaction = "Repeat";
		}
		$activesheet->setCellValue('D'.$r, $Transaction);
		$activesheet->setCellValue('E'.$r, $LeadMainData["package_name"]);
		$activesheet->setCellValue('F'.$r, $LeadMainData["prefloc_name"]);
		$activesheet->setCellValue('G'.$r, $LeadMainData["status_nm"]);
	}

/*
    while($r <= 100) {
		$r++;
if($r % 28 == 0){
	$activesheet->setCellValue('A'.$r, 'ID')->getStyle('A'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('B'.$r, 'Client Name')->getStyle('B'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('C'.$r, 'Product')->getStyle('C'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('D'.$r, 'Company Name')->getStyle('D'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('E'.$r, 'Start Date')->getStyle('E'.$r)->getFont()->setBold(true);
	$activesheet->setCellValue('F'.$r, 'Close Date')->getStyle('F'.$r)->getFont()->setBold(true);
	$r++;
	$activesheet->getRowDimension($r)->setRowHeight(3);
} else {
		$activesheet->setCellValue('A'.$r, "hello");
		$activesheet->setCellValue('B'.$r, "world");
		$activesheet->setCellValue('C'.$r, "world");
		$activesheet->setCellValue('D'.$r, "world");
		$activesheet->setCellValue('E'.$r, "world");
		$activesheet->setCellValue('F'.$r, "world");
}
    }
*/


	$styleArray = array(
    	'borders' => array(
        	'allBorders' => array(
            	'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            	'color' => array('argb' => 'FFCCCCCC'),
        	),
    	),
	);

	$activesheet->getStyle('A1:G'.$r)->applyFromArray($styleArray);
//    $activesheet->getStyle('A2:F'.$r)->getAlignment()->setWrapText(true);

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$filename.'.Xls"');
	header('Cache-Control: max-age=0');



	$writer->save('php://output');



/*
require_once("vendor/autoload.php");
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'Hello World !');

		$writer = new Xls($spreadsheet);

		$filename = 'simple';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xls"');
		header('Cache-Control: max-age=0');
		$writer->save('php://output');
*/


?>
