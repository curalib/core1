<?php


if(isset($_SESSION["ReportPage"])) {
    unset($_SESSION["ReportPage"]);
}
//if(isset($_SESSION["showtrackingid"])) {
// unset($_SESSION["showtrackingid"]);
//}


//BREADCRUMB
$cBrdCrumb = "SELECT tgl FROM ".$tblp."sys_menu WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL.$bcsplitKeys."/0/0.html";

// TOTAL COUNT INVOICE
//-------------------------------------------------
$cAllInvs = "SELECT COUNT(*) AS `count_invoice` FROM `izin_apps_invdet`  ";
//$cAllInvs .= "WHERE str_to_date(`tglbayar`, '%Y-%m-%d') BETWEEN '".$inv_date_from."' AND '".$inv_date_to."' ";
$cAllInvs .= "WHERE str_to_date(`tgl_create`, '%Y-%m-%d') BETWEEN '".$inv_date_from."' AND '".$inv_date_to."' ";
if(isset($inv_status) && !empty($inv_status)   ) {
    $cAllInvs .= "AND `tipe` = '".$inv_status."' ";
}
//$cAllInvs .= "AND (`verified_by` IS NOT NULL AND `verified_by` <> '') ";
$rcAllInvs = $dbs->getQuery($cAllInvs);
$AllInvs = $dbs->getAssoc($rcAllInvs);
$Count_AllInvs = $AllInvs["count_invoice"];



// LINE CHART DATA
//-------------------------------------------------
$LC_dates = array();
$LC_invoices = array();

//$cLineChartData = "SELECT `tglbayar`, COUNT(`tglbayar`) AS `JumlahInvoices` FROM `izin_apps_invdet`  ";
$cLineChartData = "SELECT `tgl_create`, COUNT(`tgl_create`) AS `JumlahInvoices` FROM `izin_apps_invdet`  ";
//$cLineChartData .= "WHERE str_to_date(`tglbayar`, '%Y-%m-%d') BETWEEN '".$inv_date_from."' AND '".$inv_date_to."' ";
$cLineChartData .= "WHERE str_to_date(`tgl_create`, '%Y-%m-%d') BETWEEN '".$inv_date_from."' AND '".$inv_date_to."' ";

if(isset($inv_status) && !empty($inv_status)) {
    $cLineChartData .= "AND `tipe` = '".$inv_status."' ";
}
//$cLineChartData .= "AND `sending` = 'Y' ";

//$cLineChartData .= "GROUP BY str_to_date(`tglbayar`, '%Y-%m-%d') ";
$cLineChartData .= "GROUP BY str_to_date(`tgl_create`, '%Y-%m-%d') ";
$rcLineChartData = $dbs->getQuery($cLineChartData);
while($LineChartData = $dbs->getAssoc($rcLineChartData)) {
    $LC_invoices[] = $LineChartData["JumlahInvoices"] ;
    $LC_dates[] = date("Y-m-d",strtotime($LineChartData["tgl_create"])) ;//
}

$leads_sum = array_sum($LC_leads);

$dates_to_js = json_encode($LC_dates);
$invoices_to_js = json_encode($LC_invoices);



// INVOICE DATA
//-------------------------------------------------

//$cInvData = "SELECT SUM(`total_due`) AS `TotalDue`, YEAR(`tglbayar`) AS `InvYear`, MONTH(`tglbayar`) AS `InvMonth`, Count(*) As `TotalRows` FROM `izin_apps_invdet` ";
$cInvData = "SELECT SUM(`total_due`) AS `TotalDue`, YEAR(`tgl_create`) AS `InvYear`, MONTH(`tgl_create`) AS `InvMonth`, Count(*) As `TotalRows` FROM `izin_apps_invdet` ";
//$cInvData .= "WHERE str_to_date(`tglbayar`, '%Y-%m-%d') BETWEEN '".$inv_date_from."' AND '".$inv_date_to."' ";
$cInvData .= "WHERE str_to_date(`tgl_create`, '%Y-%m-%d') BETWEEN '".$inv_date_from."' AND '".$inv_date_to."' ";

if(isset($inv_status) && !empty($inv_status)) {
    $cInvData .= "AND `tipe` = '".$inv_status."' ";
}
//$cInvData .= "AND `sending` = 'Y' ";

//$cInvData .= "AND (`verified_by` IS NOT NULL AND `verified_by` <> '') ";
//$cInvData .= "GROUP BY  YEAR(`tglbayar`), MONTH(`tglbayar`) ";
$cInvData .= "GROUP BY  YEAR(`tgl_create`), MONTH(`tgl_create`) ";
//$cInvData .= "ORDER BY  YEAR(`tglbayar`), MONTH(`tglbayar`) ASC";
$rcInvData = $dbs->getQuery($cInvData);

  /******************************************
   * End of Report Invoice
   ******************************************/
//echo $date_fromS;
//exit;



?>
