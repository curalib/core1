<?php


if(isset($_SESSION["ReportPage"])) {
 unset($_SESSION["ReportPage"]);
}
//if(isset($_SESSION["showtrackingid"])) {
// unset($_SESSION["showtrackingid"]);
//}


//BREADCRUMB
$cBrdCrumb = "SELECT tgl FROM ".$tblp."sys_menu WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL.$bcsplitKeys."/0/0.html";

// TOTAL CURRENT TRACKING
//-------------------------------------------------
//Total Tracking Close
$cAllTrack = "SELECT COUNT(`trklist`.`id_trklist`) AS `count_tracking` FROM `izin_apps_trklist` AS `trklist`  ";
$cAllTrack .= "WHERE str_to_date(`trklist`.`tgl_start`, '%Y-%m-%d') BETWEEN '".$track_date_from."' AND '".$track_date_to."' ";
if(isset($track_type) && $track_type == "TR") {
    $cAllTrack .= "AND `trklist`.`trk_status` <> 3 ";
} else {
    //finished permit
    $cAllTrack .= "AND `trklist`.`trk_status` = 3 ";
}
//$cAllLeads .= "WHERE `leads`.`status` IN ('LO','CL') ";
//echo $cAllLeads;
//exit;
$rcAllTrack = $dbs->getQuery($cAllTrack);
$AllTrack = $dbs->getAssoc($rcAllTrack);
$Count_AllTrack = $AllTrack["count_tracking"];



// LINE CHART DATA
//-------------------------------------------------


//LOAD EXPECTED TIME
$arr_item_working_days = array();
//$cWorkDays = "SELECT `works_id`, `kode`, `wkt` FROM `izin_apps_works` ";
$cWorkDays = "SELECT `kode`, `wkt` FROM `izin_apps_works` WHERE `cntdown` = 'Y' ";
$rcWorkDays = $dbs->getQuery($cWorkDays);
while($WorkDays = $dbs->getAssoc($rcWorkDays)) {
    $arr_item_working_days[$WorkDays["kode"]] = intval($WorkDays["wkt"]);
}

//$cAllTrack = "SELECT `trkitm`.`trklist_id`, `trkitm`.`works_id`, `trkitm`.`tgl_update_itm`, `trkitm`.`tgl_close_itm`, `packagework`.`wkt` ";
$cAllTrack = "SELECT `trkitm`.`trklist_id`, `trkitm`.`works_id`, `trkitm`.`tgl_update_itm` AS `start_date`, ";
$cAllTrack .= "`trkitm`.`tgl_close_itm` AS `close_date`, `works`.`kode`, `works`.`wkt` FROM `izin_apps_trklist` AS `trklist`  ";
//$cAllTrack .= "`trkitm`.`tgl_close_itm` AS `close_date`, `packagework`.`kode`, `packagework`.`wkt` AS `time_limit` FROM `izin_apps_trklist` AS `trklist`  ";
//$cAllTrack .= "MAX(`trkitm`.`tgl_close_itm`) AS `close_date`, `packagework`.`wkt` FROM `izin_apps_trklist` AS `trklist`  ";
$cAllTrack .= "INNER JOIN `izin_apps_trkitm` AS `trkitm` ON `trklist`.`id_trklist` = `trkitm`.`trklist_id` ";
//$cAllTrack .= "INNER JOIN `izin_apps_packagework` AS `packagework` ON `trklist`.`lead_id` = `packagework`.`id_lead` ";
$cAllTrack .= "RIGHT JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cAllTrack .= "WHERE str_to_date(`trklist`.`tgl_start`, '%Y-%m-%d') BETWEEN '".$track_date_from."' AND '".$track_date_to."' ";
if(isset($track_type) && $track_type == "TR") {
    //$cAllTrack .= "WHERE str_to_date(`trklist`.`tgl_start`, '%Y-%m-%d') BETWEEN '".$track_date_from."' AND '".$track_date_to."' ";
    $cAllTrack .= "AND `trklist`.`trk_status` <> 3 ";
} else {
    //finished permit or on variable error by any chance
    //$cAllTrack .= "WHERE str_to_date(`trklist`.`tgl_close`, '%Y-%m-%d') BETWEEN '".$track_date_from."' AND '".$track_date_to."' ";
    $cAllTrack .= "AND `trklist`.`trk_status` = 3 ";
}
//$cAllTrack .= "AND `works`.`cntdown` = 'Y' ";
$rcAllTrack = $dbs->getQuery($cAllTrack);

$arr_tracking_line = array();
$arr_works_start_date = array();
$arr_works_close_date = array();
$arr_tracking_line_key = "";

while($AllTrack = $dbs->getAssoc($rcAllTrack)) {
    $arr_tracking_line_key = $AllTrack["trklist_id"]."-".$AllTrack["kode"];

    if(isset($arr_tracking_line[$arr_tracking_line_key]) &&
         (!is_null($arr_tracking_line[$arr_tracking_line_key]) && !empty($arr_tracking_line[$arr_tracking_line_key]) )) {
        $arr_t_line = explode(",",$arr_tracking_line[$arr_tracking_line_key]);

        if(isset($AllTrack["start_date"]) && !is_null($AllTrack["start_date"]) && !empty($AllTrack["start_date"])) {
            if(isset($arr_t_line[1]) && !is_null($arr_t_line[1]) && !empty($arr_t_line[1])) {
                $sdate_db = new DateTime($AllTrack["start_date"]);
                $sdate_arr = new DateTime($arr_t_line[1]);
                if($arr_t_line[0] == $AllTrack["kode"] && $sdate_db < $sdate_arr) {
                //if($arr_t_line[0] == $AllTrack["works_id"] ) {
                    $start_date = $AllTrack["start_date"];
                } else {
                    $start_date = $arr_t_line[1];
                }
            } else {
                $start_date = $AllTrack["start_date"];
            }
        } else {
            $start_date = $arr_t_line[1];
        }

        if(isset($AllTrack["close_date"]) && !is_null($AllTrack["close_date"]) && !empty($AllTrack["close_date"])) {
            if(isset($arr_t_line[2]) && !is_null($arr_t_line[2]) && !empty($arr_t_line[2])) {
                $cdate_db = new DateTime($AllTrack["close_date"]);
                $cdate_arr = new DateTime($arr_t_line[2]);
                //if($arr_t_line[0] == $AllTrack["kode"] && $AllTrack["close_date"] > $arr_t_line[2]) {
                if($arr_t_line[0] == $AllTrack["kode"] && $cdate_db > $cdate_arr) {
                    $close_date = $AllTrack["close_date"];
                } else {
                    $close_date = $arr_t_line[2];
                }
            } else {
                $close_date = $AllTrack["close_date"];
            }
        } else {
            $close_date = $arr_t_line[2];
        }



    } else {
        if(isset($AllTrack["start_date"]) && !is_null($AllTrack["start_date"]) && !empty($AllTrack["start_date"])) {
            $start_date = $AllTrack["start_date"];
        } else {
            $start_date = "";
        }
        if(isset($AllTrack["close_date"]) && !is_null($AllTrack["close_date"]) && !empty($AllTrack["close_date"])) {
            $close_date = $AllTrack["close_date"];
        } else {
            $close_date = "";
        }
    }
    $arr_tracking_line_string = $AllTrack["kode"].",".$start_date.",".$close_date.",".$AllTrack["wkt"];
    $arr_tracking_line[$arr_tracking_line_key] = $arr_tracking_line_string;
}

$before_trklistid = "";
$arr_sum_activity_days = array();
$arr_sum_expected_days = array();
ksort($arr_tracking_line);
foreach($arr_tracking_line as $key => $value) {
    //$arr_sum_expected_days[$key];
    $arr_key = explode("-",$key);
    if($arr_key[0] != $before_trklistid) {
      $before_trklistid = $arr_key[0];
    }
    $arr_value = explode(",",$value);
    if(is_null($arr_value[2]) || empty($arr_value[2])) {
      //close date
      $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
      $date_now = $dt->format('Y-m-d H:i:s');
      $close_date = $date_now;
    } else {
      $close_date = $arr_value[2];
    }

    $LastActivityInDays = $HitDays->DayRange(convertDate($arr_value[1]), convertDate($close_date), false)->Result_OnlyNumber();
    $arr_sum_activity_days[$before_trklistid] = $arr_sum_activity_days[$before_trklistid] + $LastActivityInDays[1];
    $arr_sum_expected_days[$before_trklistid] = $arr_sum_expected_days[$before_trklistid] + intval($arr_value[3]);

//    echo "tid: ".$arr_key[0]." kode: ".$arr_value[0]." ad: ".$arr_value[1]." ed: ".$close_date."<BR/>";
    //echo "tid: ".$arr_key[0]." ad: ".$value." ed: ".$arr_sum_expected_days[$key];
    //echo "tid: ".$arr_key[0]." ad: ".$arr_sum_activity_days[$before_trklistid]." ed: ".$arr_sum_expected_days[$before_trklistid]."<BR/>";
}
//exit;
$arr_AccTrack = array();
$arr_OvrTrack = array();
$LC_dates = array();
$LC_tracks = array();

foreach($arr_sum_activity_days as $key => $value) {

    $cStartDate = "SELECT `tgl_start` FROM `izin_apps_trklist` WHERE `id_trklist` = '".$key."' ";
    $rcStartDate = $dbs->getQuery($cStartDate);
    $StartDateAssoc = $dbs->getAssoc($rcStartDate);
    $StartDate = $StartDateAssoc["tgl_start"];

    $cCountTracks = "SELECT COUNT(`tgl_start`) AS `count_tr` FROM `izin_apps_trklist` WHERE `tgl_start` = '".$StartDate."' ";
    $rcCountTracks = $dbs->getQuery($cCountTracks);
    $CountTracksAssoc = $dbs->getAssoc($rcCountTracks);
    $CountTracks = $CountTracksAssoc["count_tr"];

    $LC_tracks[] = $CountTracks ;
    $LC_dates[] = date("Y-m-d",strtotime($StartDate)) ;//

    if($value <= $arr_sum_expected_days[$key]) {
        //bandingkan dengan jumlah hari kerja yang di packagework
        //kalau kurang atau pas masukkan ke ACCURATE (trklist_id)
        $arr_AccTrack[$key] = $StartDate.",".intval($arr_sum_expected_days[$key]) - intval($value);

//        $LC_invoices[] = $LineChartData["JumlahInvoices"] ;
//        $LC_dates[] = date("Y-m-d",strtotime($LineChartData["tgl_start"])) ;//


    } else {
        //bandingkan dengan jumlah hari kerja yang di packagework
        //kalau lebih masukkan ke TIMELINE OVER (trklist_id)
        $arr_OvrTrack[$key] = $StartDate.",".intval($value) - intval($arr_sum_expected_days[$key]);
    }
}



// ACCURATE TRACKING
//-------------------------------------------------
$Count_AccTrack = count($arr_AccTrack);

// TIMELINE OVER TRACKING
//-------------------------------------------------
$Count_OvrTrack = count($arr_OvrTrack);



$dates_to_js = json_encode($LC_dates);
$tracks_to_js = json_encode($LC_tracks);



  /******************************************
   * End of Permit Today
   ******************************************/



?>
