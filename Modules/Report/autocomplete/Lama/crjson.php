<?php
/*
Function Create json file
This function for autocomplete purposes
*/
function CrJsonFile($jsonfile,$dbcon,$tblp){
	$jsonArr = "var datasp = [";
	$JsonPrep = "select id, kode_sp, nama, ket_sp, lst_price, sat_sp  from ".$tblp."apps_sp_nama order by nama, ket_sp, kode_sp asc";
	$rJsonPrep = $dbcon->getQuery($JsonPrep);
	while( $jp = $dbcon->getAssoc($rJsonPrep)){
		$j++;
		$jp['nama'] = preg_replace("/\,/","",$jp['nama']);
		$jp['ket_sp'] = preg_replace("/[^a-zA-Z0-9 :]/","",$jp['ket_sp']);
		$labels = $jp['kode_sp'];
		if ( !empty($jp['nama']) ){ $labels .= " - ".$jp['nama']; }
		if ( !empty($jp['ket_sp']) ){ $labels .= " - ".$jp['ket_sp']; }
		
		if ( empty($jp['nama']) and empty($jp['ket_sp']) ){ 
			$namapart = "N/A"; 
		}elseif( !empty($jp['nama']) and empty($jp['ket_sp']) ){
			$namapart = $jp['nama'];
		}elseif( empty($jp['nama']) and !empty($jp['ket_sp']) ){
			$namapart = $jp['ket_sp'];
		}elseif( !empty($jp['nama']) and !empty($jp['ket_sp']) ){
			$namapart = $jp['nama']." - ".$jp['ket_sp'];
		}
		
		if ( $j == 1 ){ 
			$jsonArr .= "{ value: '".$jp['id']."', label: '".$labels."', HrgPart: '".c_curr($jp['lst_price'],$comma_separator,$comma_separator_user,$min_sign="mark")."', SatPart: '".$jp['sat_sp']."', namapart: '".$namapart."', kodepart: '".$jp['kode_sp']."' }"; 
		}else{
			$jsonArr .= ",{ value: '".$jp['id']."', label: '".$labels."', HrgPart: '".c_curr($jp['lst_price'],$comma_separator,$comma_separator_user,$min_sign="mark")."', SatPart: '".$jp['sat_sp']."', namapart: '".$namapart."', kodepart: '".$jp['kode_sp']."' }"; 
		}
	}
	$jsonArr .= "]";
			
	$handle = fopen($jsonfile, 'w');
	fwrite($handle, $jsonArr);
	fclose($handle);
}


/* ---------------------------------------------------- */
?>