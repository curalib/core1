<?php

$HitDays = new CountDays($dbs,$tblp);

if(isset($_POST['task'])) {
    $task = Purefy($_POST['task']);

    /******************************************
     * Tracking List
     ******************************************/

    if($task == "SearchLeadBy_".$_SESSION["butts_Update"]) {
        $lead_date_range = Purefy($_POST['lchart_range']);
        $lead_date_fromS = substr($lead_date_range, 0, 10);
        $lead_date_toS = substr($lead_date_range, -10);
        $lead_date_range = $lead_date_fromS." to ".$lead_date_toS;
        $lead_date_to = str_replace('/', '-', $lead_date_toS);
        $lead_date_to = date("Y-m-d", strtotime($lead_date_to));
        $lead_date_from = str_replace('/', '-', $lead_date_fromS);
        $lead_date_from = date("Y-m-d", strtotime($lead_date_from));

        $lead_status = Purefy($_POST['lead_status']);
        $lead_sales = Purefy($_POST['lead_sales']);
        $lead_package = Purefy($_POST['lead_package']);
        $lead_location = Purefy($_POST['lead_location']);
        $lead_transaction = Purefy($_POST['lead_transaction']);
        $lead_source = Purefy($_POST['lead_source']);

	$_SESSION["Report_lead_status"] = $lead_status;
	$_SESSION["Report_lead_sales"] = $lead_sales;
	$_SESSION["Report_lead_package"] = $lead_package;
	$_SESSION["Report_lead_location"] = $lead_location;
  $_SESSION["Report_lead_transaction"] = $lead_transaction;
	$_SESSION["lead_source"] = $lead_source;
	$_SESSION["Report_lead_date_range"] = $lead_date_range;



        require "Modules/Report/file/ReportLeads_i.php";

        if(isset($_POST['export-excell'])) {
            require "Modules/Report/file/ReportLeads_XLS.php";
        }
    }

    /******************************************
     * Paging buttons
     ******************************************/
    if($task == $_SESSION["buttkey"]."prev") {
        $lead_status = $_SESSION["Report_lead_status"];
        $lead_sales = $_SESSION["Report_lead_sales"];
        $lead_package = $_SESSION["Report_lead_package"];
        $lead_location = $_SESSION["Report_lead_location"];
        $lead_transaction = $_SESSION["Report_lead_transaction"];
        $lead_date_range = $_SESSION["Report_lead_date_range"];
        $lead_date_fromS = substr($lead_date_range, 0, 10);
        $lead_date_toS = substr($lead_date_range, -10);
        $lead_date_range = $lead_date_fromS." to ".$lead_date_toS;
        $lead_date_to = str_replace('/', '-', $lead_date_toS);
        $lead_date_to = date("Y-m-d", strtotime($lead_date_to));
        $lead_date_from = str_replace('/', '-', $lead_date_fromS);
        $lead_date_from = date("Y-m-d", strtotime($lead_date_from));

        //PAGING
        require "Modules/Report/file/ReportLeads_i.php";
    }
    /******************************************
     * End of Paging buttons
     ******************************************/


/*
    if($task == "ExportToXLS_".$_SESSION["butts_Enter"]) {
    //if($task == "ExportToXLS_".$_SESSION["butts_Update"]) {
        $lead_date_range = Purefy($_POST['lchart_range']);
        $lead_date_fromS = substr($lead_date_range, 0, 10);
        $lead_date_toS = substr($lead_date_range, -10);
        $lead_date_range = $lead_date_fromS." to ".$lead_date_toS;
        $lead_date_to = str_replace('/', '-', $lead_date_toS);
        $lead_date_to = date("Y-m-d", strtotime($lead_date_to));
        $lead_date_from = str_replace('/', '-', $lead_date_fromS);
        $lead_date_from = date("Y-m-d", strtotime($lead_date_from));

        $lead_status = Purefy($_POST['lead_status']);
        $lead_sales = Purefy($_POST['lead_sales']);
        $lead_package = Purefy($_POST['lead_package']);
        $lead_location = Purefy($_POST['lead_location']);
        $lead_transaction = Purefy($_POST['lead_transaction']);

echo $lead_date_range.", ".$lead_status.", ".$lead_sales.", ".$lead_package.", ".$lead_location.", ".$lead_transaction;
exit;

        require "Modules/Report/file/ReportLeads_i.php";
        require "Modules/Report/file/ReportLeads_XLS.php";
    }
*/



/*
    if($task == "UpdateDateRange_".$_SESSION["butts_Update"]) {

      $lead_date_range = Purefy($_POST['lchart_range']);
      $lead_date_fromS = substr($lead_date_range, 0, 10);
      $lead_date_toS = substr($lead_date_range, -10);
      //$Use_DateRange = true;
      //$Kode_Kantor = "izin";
      //$date_fromS = date('d/m/Y',strtotime('2019-04-04 - 67 days'));
      //$date_toS = date('d/m/Y',strtotime('2019-04-04'));
      //$date_fromS = date('d/m/Y',strtotime('now - 67 days'));
      //$date_toS = date('d/m/Y',strtotime('now'));

      $lead_date_range = $lead_date_fromS." to ".$lead_date_toS;

      //$date_range = $date_from." to ".$date_to;
      $lead_date_to = str_replace('/', '-', $lead_date_toS);
      $lead_date_to = date("Y-m-d", strtotime($lead_date_to));
      $lead_date_from = str_replace('/', '-', $lead_date_fromS);
      $lead_date_from = date("Y-m-d", strtotime($lead_date_from));

      $lead_status = Purefy($_POST['lead_status']);

        require "Modules/Report/file/ReportLeads_i.php";
    }

    if($task == "UpdateLeadStatus_".$_SESSION["butts_Update"]) {

      $lead_date_range = Purefy($_POST['lchart_range']);
      $lead_date_fromS = substr($lead_date_range, 0, 10);
      $lead_date_toS = substr($lead_date_range, -10);
      //$Use_DateRange = true;
      //$Kode_Kantor = "izin";
      //$date_fromS = date('d/m/Y',strtotime('2019-04-04 - 67 days'));
      //$date_toS = date('d/m/Y',strtotime('2019-04-04'));
      //$date_fromS = date('d/m/Y',strtotime('now - 67 days'));
      //$date_toS = date('d/m/Y',strtotime('now'));

      $lead_date_range = $lead_date_fromS." to ".$lead_date_toS;

      //$date_range = $date_from." to ".$date_to;
      $lead_date_to = str_replace('/', '-', $lead_date_toS);
      $lead_date_to = date("Y-m-d", strtotime($lead_date_to));
      $lead_date_from = str_replace('/', '-', $lead_date_fromS);
      $lead_date_from = date("Y-m-d", strtotime($lead_date_from));

      $lead_status = Purefy($_POST['lead_status']);

        require "Modules/Report/file/ReportLeads_i.php";
    }
*/
/*
    if($task == "ResetSearch_".$_SESSION["butts_Enter"]) {
        //RESET SEARCH
        require "Modules/Report/file/ReportLeads_i.php";
    }

    if($task == $_SESSION["buttkey"]."prev") {
        //PAGING
        require "Modules/Report/file/ReportLeads_i.php";
    }
*/

    /******************************************
     * End of Add Tracking
     ******************************************/


} else {
//    $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
//    $date_now = $dt->format('Y-m-d H:i:s');
//    $date_now = "2019-05-08";
//$date_range = Purefy($_POST['lchart_range']);
//$date_fromS = substr($date_range, 0, 10);
//$date_toS = substr($date_range, -10);
//$Use_DateRange = true;
//$Kode_Kantor = "izin";
//$date_fromS = date('d/m/Y',strtotime('2019-04-04 - 67 days'));
//$date_toS = date('d/m/Y',strtotime('2019-04-04'));

    $lead_date_fromS = date('d/m/Y',strtotime('now - 30 days'));
    $lead_date_toS = date('d/m/Y',strtotime('now'));
    $lead_date_range = $lead_date_fromS." to ".$lead_date_toS;
    //$date_range = $date_from." to ".$date_to;
    $lead_date_to = str_replace('/', '-', $lead_date_toS);
    $lead_date_to = date("Y-m-d", strtotime($lead_date_to));
    $lead_date_from = str_replace('/', '-', $lead_date_fromS);
    $lead_date_from = date("Y-m-d", strtotime($lead_date_from));
    //$lead_status = "all";

    $_SESSION["Report_lead_status"] = "all";
    $_SESSION["Report_lead_sales"] = "all";
    $_SESSION["Report_lead_package"] = "all";
    $_SESSION["Report_lead_location"] = "all";
    $_SESSION["Report_lead_transaction"] = "all";
    $_SESSION["Report_lead_date_range"] = $lead_date_range;


    require "Modules/Report/file/ReportLeads_i.php";
}
