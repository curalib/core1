<?php



$HitDays = new CountDays($dbs, $tblp);

if (isset($_POST['task'])) {
  $task = Purefy($_POST['task']);

  /******************************************
   * Tracking List
   ******************************************/
  if ($task == "UpdateLineChart_" . $_SESSION["butts_Update"]) {

    $track_date_range = Purefy($_POST['lchart_range']);
    $track_date_fromS = substr($track_date_range, 0, 10);
    $track_date_toS = substr($track_date_range, -10);
    $track_date_range = $track_date_fromS . " to " . $track_date_toS;
    //$date_range = $date_from." to ".$date_to;
    $track_date_to = str_replace('/', '-', $track_date_toS);
    $track_date_to = date("Y-m-d", strtotime($track_date_to));
    $track_date_from = str_replace('/', '-', $track_date_fromS);
    $track_date_from = date("Y-m-d", strtotime($track_date_from));

    $track_type = Purefy($_POST['track_type']);


    require "Modules/Report/file/ReportTrack_i.php";
  }

  if ($task == "UpdateTrackStatus_" . $_SESSION["butts_Update"]) {
    $track_date_range = Purefy($_POST['lchart_range']);
    $track_date_fromS = substr($track_date_range, 0, 10);
    $track_date_toS = substr($track_date_range, -10);
    $track_date_range = $track_date_fromS . " to " . $track_date_toS;
    $track_date_to = str_replace('/', '-', $track_date_toS);
    $track_date_to = date("Y-m-d", strtotime($track_date_to));
    $track_date_from = str_replace('/', '-', $track_date_fromS);
    $track_date_from = date("Y-m-d", strtotime($track_date_from));

    $track_type = Purefy($_POST['track_type']);


    require "Modules/Report/file/ReportTrack_i.php";
  }


  if ($task == "ResetSearch_" . $_SESSION["butts_Enter"]) {
    //RESET SEARCH
    require "Modules/Report/file/ReportLeads_i.php";
  }

  if ($task == $_SESSION["buttkey"] . "prev") {
    //PAGING
    require "Modules/Report/file/ReportTrack_i.php";
  }


  /******************************************
   * End of Add Tracking
   ******************************************/
} else {

  //echo "sampe sini";
  //exit;

  //    $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
  //    $date_now = $dt->format('Y-m-d H:i:s');
  //    $date_now = "2019-05-08";

  //$date_range = Purefy($_POST['lchart_range']);
  //$date_fromS = substr($date_range, 0, 10);
  //$date_toS = substr($date_range, -10);
  //$Use_DateRange = true;
  //$Kode_Kantor = "izin";
  //$date_fromS = date('d/m/Y',strtotime('2019-04-04 - 67 days'));
  //$date_toS = date('d/m/Y',strtotime('2019-04-04'));
  $track_date_fromS = date('d/m/Y', strtotime('now - 30 days'));
  $track_date_toS = date('d/m/Y', strtotime('now'));

  $track_date_range = $track_date_fromS . " to " . $track_date_toS;


  //$date_range = $date_from." to ".$date_to;
  $track_date_to = str_replace('/', '-', $track_date_toS);
  $track_date_to = date("Y-m-d", strtotime($track_date_to));
  $track_date_from = str_replace('/', '-', $track_date_fromS);
  $track_date_from = date("Y-m-d", strtotime($track_date_from));

  $track_type = "TR";

  //echo "sampe sini juga";
  //exit;

  require "Modules/Report/file/ReportTrack_i.php";
}
