
<?php
$cBrdCrumb = "SELECT tgl FROM ".$tblp."sys_menu WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL.$bcsplitKeys."/0/0.html";
?>

                <div class="container-fluid page__heading-container">
                    <div class="page__heading d-flex align-items-center">
                        <div class="flex">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb mb-0">

                                    <li class="breadcrumb-item"><a href="<?php echo $bchref?>">Home</a></li>
                                    <li class="breadcrumb-item">Setting</li>
<!--
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item"><a href="#">Setting</a></li>
-->
                                    <li class="breadcrumb-item active" aria-current="page">Packages</li>
                                </ol>
                            </nav>

                        </div>
                        <a href="add-paket.php" class="btn btn-warning ml-3">Add Packages</a>
                    </div>
                </div>



                    <div class="container-fluid page__container">
                        <div class="card card-form" >
                            <div class="row no-gutters">
                                <div class="col-lg-12 card-form__body">


                                    <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                        <table class="table mb-0 thead-border-top-0">
                                            <thead>
                                                <tr>

                                                    <th style="width: 18px;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
                                                            <label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
                                                        </div>
                                                    </th>

                                                    <th style="width: 100px;">Packages</th>

                                                    <th style="width: 37px;">Company</th>

                                                    <th style="width: 237px;">Price</th>

                                                    <th style="width: 51px;">Status</th>
                                                    <th style="width: 24px;"></th>
                                                </tr>
                                            </thead>
                                            <tbody class="list" id="staff">

                                                <tr class="selected">

                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_1">
                                                            <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                                                        </div>
                                                    </td>

                                                    <td>

                                                    PT Full Services

                                                    </td>


                                                    <td>Izin.co.id</td>
                                                    <td>IDR 12,000,000</td>
                                                    <td><span class="badge badge-success">DONE</span></td>
                                                    <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
