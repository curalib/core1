<?php
require "system/PagingTrack.php";
require "page/Alert.php";
if ( !isset($_SESSION["FixHoliday"]) ){
	$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/NotFixedHoliday.php"; 
}else{
	$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/FixedHoliday.php"; 
}
echo '<div class="container-fluid page__container">';
if ( is_file($fl) ) { require $fl; }else{ require $_SESSION["ROOT_DIR"]."/Modules/System/page/PageNotFound.php"; }
echo '</div>';

	function time_elapsed_string($datetime, $endtime, $full = false) {
    $now = new DateTime($endtime);
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        //'s' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}
?>