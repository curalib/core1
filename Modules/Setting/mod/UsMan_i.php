<?php
if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Add"] ){
	$_SESSION["AddItm"] = "GasPol";
	unset($_SESSION["Edit_ID"]);
}

if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Reset"] ){
	unset($_SESSION["AddItm"]);
	unset($_SESSION["Edit_ID"]);
}

if ( $task == "NewUsr_".$_SESSION["butts_Save"] ){
	$FullName = Purefy($_POST['FullName']);
	$nUID = Purefy($_POST['nUID']);
	$usr_pos = Purefy($_POST['usr_pos']);
	$UBm = Purefy($_POST['UBm']);
	$PBm = Purefy($_POST['PBm']);

	if ( empty($FullName) || empty($nUID) || empty($usr_pos) || empty($UBm) || empty($PBm) ){
		$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_202;
	}else{
		if ( $UBm != $PBm ){
			$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_203;
		}else{
			if ( !is_email($nUID) ){
				$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_214;
			}else{
				if ( $usr_pos == 0 or ($usr_pos < $_SESSION['lgnPriv']) ){
					$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_216;
				}else{
					$cUser = "select id from ".$tblp."sys_login where userid = '".md5($nUID)."'";
					$rcUser = $dbs->getArr($cUser);
					if ( !empty($rcUser['id']) ){
						$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_215;
					}else{
						$id_login = md5(uniqids(microtime()));

						$inLgnDet = "insert into ".$tblp."sys_lgndetail set
									 id = ?,
									 id_login = ?,
									 uid = ?,
									 priv = ?,
									 nama_asli = ?,
									 jabatan  = ?,
									 ass_man = ?";
						$bind = array(
							"s|".md5(uniqids(microtime())),
							"s|".$id_login,
							"s|".$nUID,
							"i|".$usr_pos,
							"s|".$FullName,
							"s|".${"lang_".$UserPos[$usr_pos]},
							"s|".$PilMans
						);

						$dbs->getQuery($inLgnDet,"Exec",$bind);

						$pwdhash = password_hash($UBm, PASSWORD_BCRYPT, $passhashoptions);
						$inLgn = "insert into ".$tblp."sys_login set
								  id = ?,
								  userid = ?,
								  pwd = ?";
						$bind = array(
									"s|".$id_login,
									"s|".md5($nUID),
									"s|".$pwdhash
								);
						$dbs->getQuery($inLgn,"Exec",$bind);


						$cLstMenu = "Select id, mandatory, tipe_menu from ".$tblp."sys_menu where publish = 'Y' and tingkat = 1";
						$rcLstMenu = $dbs->getQuery($cLstMenu);
						while ( $lstm = $dbs->getAssoc($rcLstMenu) ){
							$mnexist = false;
							if ( $lstm['tipe_menu'] == "MENU" ){
								$cLstMenu2 = "select id, mandatory from ".$tblp."sys_menu where publish = 'Y' and id_parent = '".$lstm['id']."' and tingkat = 2";
								$rcLstMenu2 = $dbs->getQuery($cLstMenu2);
								while ( $lstm2 = $dbs->getAssoc($rcLstMenu2) ){
									if ( ($lstm2['mandatory'] == "Y") or (Purefy($_POST[$lstm2['id']]) == 1) ) {
										$inMenuAcc = "insert into ".$tblp."sys_menuaccess set
													  id = ?, id_logintbl = ?, menus_id = ?";
										$bind = array(
											"s|".md5(uniqids(microtime())),
											"s|".$id_login,
											"s|".$lstm2['id']
										);
										$dbs->getQuery($inMenuAcc,"Exec",$bind);
										$mnexist = true;
									}
								}

								if ( $mnexist === true ){
									$inMenuAcc = "insert into ".$tblp."sys_menuaccess set
											  	  id = ?, id_logintbl = ?, menus_id = ?";
									$bind = array(
										"s|".md5(uniqids(microtime())),
										"s|".$id_login,
										"s|".$lstm['id']
									);
									$dbs->getQuery($inMenuAcc,"Exec",$bind);
								}

							}else{
								if ( ($lstm['mandatory'] == "Y") or (Purefy($_POST[$lstm['id']]) == 1) ) {
									$inMenuAcc = "insert into ".$tblp."sys_menuaccess set
												  id = ?, id_logintbl = ?, menus_id = ?";
									$bind = array(
										"s|".md5(uniqids(microtime())),
										"s|".$id_login,
										"s|".$lstm['id']
									);
									$dbs->getQuery($inMenuAcc,"Exec",$bind);
								}
							}
						}

						$AlertMess = $lang_204;
						$AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_204;

						$Hasil = $pm->File("UpAv",FILESIZES,"../../".UPDIR."/avatar")->Images(AVATAR_WD,0,"shrink")->Result();
						if ( $Hasil[0] == "FL_IM_SUCCESS" and $Hasil[1] != '' ){

							$upAv = "update ".$tblp."sys_lgndetail set avatar = '".$Hasil[1]."' where id_login = '".$id_login."'";
							$dbs->getQuery($upAv);
							$AlertMess = $AlertMess."**".'alert-success#done_all#AVATAR SUCCESS!#'.$lang_6041;

						}else{
							$AlertMess = $AlertMess."**".'alert-warning#warning#AVATAR WARNING!#'.$FileManPesan[$Hasil[0]];
						}

						unset($_SESSION["AddItm"]);
					}
				}
			}
		}
	}
}

if ( $task == "Usr_".$_SESSION["butts_Del"] ){
	$LstU = "select id from ".$tblp."sys_lgndetail where priv > ".$_SESSION["lgnPriv"];
	$rLstU = $dbs->getQuery($LstU);
	$deltrue = "N";
	while ( $ru = $dbs->getAssoc($rLstU)){
		if ( Purefy($_POST['chkbox_'.$ru['id']]) == 1 ){
			$cD = "select avatar, uid, id_login from ".$tblp."sys_lgndetail where id = '".$ru['id']."'";
			$rcD = $dbs->getArr($cD);
			# Unlik avatar
			$f_av = $_SESSION["ROOT_DIR"]."/"."../../".UPDIR."/avatar/".$rcD['avatar'];
			if ( is_file($f_av) ){ unlink($f_av);  }

			$delLgn = "delete from ".$tblp."sys_lgndetail where id = '".$ru['id']."'";
			$dbs->getQuery($delLgn);

			$delAcc = "delete from ".$tblp."sys_login where userid = '".md5($rcD['uid'])."'";
			$dbs->getQuery($delAcc);

			$delMenu = "delete from ".$tblp."sys_menuaccess where id_logintbl = '".$rcD['id_login']."'";
			$dbs->getQuery($delMenu);

			$deltrue = "Y";
		}
	}
	if ( $deltrue == "Y" ) { $AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_219; }else{ $AlertMess = 'alert-info#info#NO ITEM SELECTED!#'.$lang_220; }
}

if ( $task == "Usr_".$_SESSION["butts_Edit"] ){
	$LstU = "select id from ".$tblp."sys_lgndetail where priv > ".$_SESSION["lgnPriv"];
	$rLstU = $dbs->getQuery($LstU);
	$deltrue = "N";
	while ( $ru = $dbs->getAssoc($rLstU)){
		if ( Purefy($_POST['chkbox_'.$ru['id']]) == 1 ){
			$_SESSION["Edit_ID"] = $ru['id'];
			$deltrue = "Y";
		}
	}
	if ( $deltrue == "N" ) { $AlertMess = 'alert-info#info#NO ITEM SELECTED!#'.$lang_220; }
}

if ( $task == "UserCut".$_SESSION["butts_Reset"] ){
	$cutid = Purefy($_POST['cutid']);
	if ( $cutid ){
		$up = "update ".$tblp."sys_login set login_id = '' where id = '".$cutid."'";
		$dbs->getQuery($up);
		$AlertMess = 'alert-success#done_all#SUCCESS!#User successfully disconnected';
	}
}

if ( $task == "UserUnBann".$_SESSION["butts_Reset"] ){
	$cutid = Purefy($_POST['cutid']);
	if ( $cutid ){
		$up = "update ".$tblp."sys_login set bann = '' where id = '".$cutid."'";
		$dbs->getQuery($up);
		$AlertMess = 'alert-success#done_all#SUCCESS!#User successfully Un-Banned';
	}
}

if ( $task == "UserBann".$_SESSION["butts_Reset"] ){
	$cutid = Purefy($_POST['cutid']);
	if ( $cutid ){
		$up = "update ".$tblp."sys_login set bann = 1, login_id = '' where id = '".$cutid."'";
		$dbs->getQuery($up);
		$AlertMess = 'alert-success#done_all#SUCCESS!#User successfully banned';
	}
}

if ( $task == "EditUsr_".$_SESSION["butts_Save"] ){
	$FullName = Purefy($_POST['FullName']);
	$usr_pos = Purefy($_POST['usr_pos']);
	$UBm = Purefy($_POST['UBm']);
	$PBm = Purefy($_POST['PBm']);

	if ( empty($FullName) || empty($usr_pos) ){
		$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_202;
	}else{
		if ( $usr_pos == 0 or ($usr_pos < $_SESSION['lgnPriv']) ){
			$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_216;
		}else{
			$inLgnDet = "update ".$tblp."sys_lgndetail set
					 	priv = ?,
						nama_asli = ?,
						jabatan  = ?,
						ass_man = ?
						where id = ?";
			$bind = array(
				"i|".$usr_pos,
				"s|".$FullName,
				"s|".${"lang_".$UserPos[$usr_pos]},
				"s|".$PilMans,
				"s|".$_SESSION["Edit_ID"]
			);

			$dbs->getQuery($inLgnDet,"Exec",$bind);

			$cidlgn = "select id_login from ".$tblp."sys_lgndetail where id = '".$_SESSION["Edit_ID"]."'";
			$rcidlgn = $dbs->getArr($cidlgn);

			$delUsrAccs = "delete from ".$tblp."sys_menuaccess where id_logintbl = '".$rcidlgn['id_login']."'";
			$dbs->getQuery($delUsrAccs);

			$cLstMenu = "Select id, mandatory, tipe_menu from ".$tblp."sys_menu where publish = 'Y' and tingkat = 1";
			$rcLstMenu = $dbs->getQuery($cLstMenu);
			while ( $lstm = $dbs->getAssoc($rcLstMenu) ){
				$mnexist = false;
				if ( $lstm['tipe_menu'] == "MENU" ){
					$cLstMenu2 = "select id, mandatory from ".$tblp."sys_menu where publish = 'Y' and id_parent = '".$lstm['id']."' and tingkat = 2";
					$rcLstMenu2 = $dbs->getQuery($cLstMenu2);
					while ( $lstm2 = $dbs->getAssoc($rcLstMenu2) ){
						if ( ($lstm2['mandatory'] == "Y") or (Purefy($_POST[$lstm2['id']]) == 1) ) {
							$inMenuAcc = "insert into ".$tblp."sys_menuaccess set
										  id = ?, id_logintbl = ?, menus_id = ?";
							$bind = array(
								"s|".md5(uniqids(microtime())),
								"s|".$rcidlgn['id_login'],
								"s|".$lstm2['id']
							);
							$dbs->getQuery($inMenuAcc,"Exec",$bind);
							$mnexist = true;
						}
					}

					if ( $mnexist === true ){
						$inMenuAcc = "insert into ".$tblp."sys_menuaccess set
								  	  id = ?, id_logintbl = ?, menus_id = ?";
						$bind = array(
							"s|".md5(uniqids(microtime())),
							"s|".$rcidlgn['id_login'],
							"s|".$lstm['id']
						);
						$dbs->getQuery($inMenuAcc,"Exec",$bind);
					}

				}else{
					if ( ($lstm['mandatory'] == "Y") or (Purefy($_POST[$lstm['id']]) == 1) ) {
						$inMenuAcc = "insert into ".$tblp."sys_menuaccess set
									  id = ?, id_logintbl = ?, menus_id = ?";
						$bind = array(
							"s|".md5(uniqids(microtime())),
							"s|".$rcidlgn['id_login'],
							"s|".$lstm['id']
						);
						$dbs->getQuery($inMenuAcc,"Exec",$bind);
					}
				}
			}

			$AlertMess .= $lang_406;

			if ( !empty($UBm) ){
				if ( $UBm != $PBm ){
					$AlertMess = 'alert-danger#remove_circle#ERROR!#Change Password Failed!.<br>'.$lang_203;
				}else{
					$pwdhash = password_hash($UBm, PASSWORD_BCRYPT, $passhashoptions);
					$inLgn = "update ".$tblp."sys_login set pwd = ? where id = ?";
					$bind = array(
								"s|".$pwdhash,
								"s|".$rcidlgn['id_login']
							);
					$dbs->getQuery($inLgn,"Exec",$bind);
					$AlertMess .= "<br>".$lang_212;
				}
			}

			if ( Purefy($_POST['DA']) == 1 ){
				$cLastAv = "Select avatar from ".$tblp."sys_lgndetail where id = '".$_SESSION["Edit_ID"]."'";
				$rcLastAv = $dbs->getArr($cLastAv);
				$ftd = $_SESSION["ROOT_DIR"]."/"."../../".UPDIR."/avatar/".$rcLastAv['avatar'];
				if ( is_file($ftd) ){ unlink($ftd); }
				$upAv = "update ".$tblp."sys_lgndetail set avatar = '' where id = '".$_SESSION["Edit_ID"]."'";
				$dbs->getQuery($upAv);
				$AlertMess .= '<br>Avatar deleted!';
			}

			$AlertMess = 'alert-success#done_all#SUCCESS!#'.$AlertMess;

			$Hasil = $pm->File("UpAv",FILESIZES,"../../".UPDIR."/avatar")->Images(AVATAR_WD,0,"shrink")->Result();
			if ( $Hasil[0] == "FL_IM_SUCCESS" and $Hasil[1] != '' ){

				$cLastAv = "Select avatar from ".$tblp."sys_lgndetail where id = '".$_SESSION["Edit_ID"]."'";
				$rcLastAv = $dbs->getArr($cLastAv);
				$ftd = $_SESSION["ROOT_DIR"]."/"."../../".UPDIR."/avatar/".$rcLastAv['avatar'];
				if ( is_file($ftd) ){ unlink($ftd); }

				$upAv = "update ".$tblp."sys_lgndetail set avatar = '".$Hasil[1]."' where id = '".$_SESSION["Edit_ID"]."'";
				$dbs->getQuery($upAv);
				$AlertMess = $AlertMess."**".'alert-success#done_all#AVATAR SUCCESS!#'.$lang_6041;

			}else{
				$AlertMess = $AlertMess."**".'alert-warning#warning#AVATAR WARNING!#'.$FileManPesan[$Hasil[0]];
			}

			unset($_SESSION["Edit_ID"]);
		}
	}
}
?>
