<?php
if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Add"] ){
	$_SESSION["AddItm"] = "GasPol";
	unset($_SESSION["Edit_ID"]);
}

if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Reset"] ){
	unset($_SESSION["AddItm"]);
	unset($_SESSION["Edit_ID"]);
}

if ( $task == "pkgEdit_".$_SESSION["butts_Reset"] ){
	unset($_SESSION["Edit_ID"]);
	$AlertMess = 'alert-warning#warning#WARNING!#Update Package Canceled.';
}

if ( $task == "pkg_".$_SESSION["butts_Save"] ){
	$pkg_code = Purefy($_POST['pkg_code']);
	$pkg_name = Purefy($_POST['pkg_name']);
	$pkg_desc = Purefy(nl2br($_POST['pkg_desc']));
	$pkg_works = '';
	$scWi = $dbs->getQuery("Select id, kode from ".$tblp."apps_works order by urutan asc");
	while( $rscwi = $dbs->getAssoc($scWi)){
		if ( Purefy($_POST[$rscwi['id']]) == 1 ) { $pkg_works .= $rscwi['id'].":";  }
	}
	$pkg_works = preg_replace("/(.*)\:$/","\\1",$pkg_works);
	
	#Custom Package
	if ( Purefy($_POST['Custom'] == 1 ) ){
		$pkg_works .= 'Custom';
	}
	
	
	//echo $pkg_works;
	
	if ( empty($pkg_code) || empty($pkg_name) || empty($pkg_works) ){
		//$AlertMess = $lang_6028;
		$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_6028;
	}else{
		$cKode = "Select id from ".$tblp."apps_package where kode = '".$pkg_code."'";
		$rcKode = $dbs->getArr($cKode);
		if ( !empty($rcKode['id']) ){
			//$AlertMess = $lang_6029;
			$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_6029;
		}else{
			$cNu = "select no_urut from ".$tblp."apps_package order by no_urut desc limit 1";
			$rcNu = $dbs->getArr($cNu);
			$NxtNu = bcadd($rcNu['no_urut'],1,0);
			
			$in = "insert into ".$tblp."apps_package set id = ?, nama = ?, kode = ?, publish = ?, no_urut = ?, works = ?, descs = ?";
			$idPackage = md5(uniqids(microtime()));
			$bind = array(
						"s|".$idPackage, 
						"s|".$pkg_name, 
						"s|".$pkg_code, 
						"s|Y", 
						"i|".$NxtNu, 
						"s|".$pkg_works, 
						"s|".$pkg_desc
					);
			$dbs->getQuery($in,"Exec",$bind);
			
			#Untuk drwis
			$IDWorks = explode(":",$pkg_works);
			foreach( $IDWorks as $IDW ){
				$inHub = "insert into ".$tblp."apps_pkgitm set id_pkgitm = ?, package_id = ?, mstitem_id = ?";
				$bind = array("s|".uniqids(md5(microtime())), "s|".$idPackage, "s|".$IDW);
				$dbs->getQuery($inHub,"Exec",$bind);
			}
			
			# Create Json File for autocomplete
			require_once("Modules/".$cmodule."/autocomplete/crjson.php");
			CrJsonFile("Modules/".$cmodule."/autocomplete/JS_File_Package.json",$dbs,$tblp);
			
			$AlertMess = $lang_6030;
			$AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_6030;
			unset($_SESSION["AddItm"]);
			
		}
	}
}

if ( $task == "pack_".$_SESSION["butts_Edit"] ){
	
	$cLstWiR = "Select id from ".$tblp."apps_package order by no_urut asc";
	$rcLstWiR = $dbs->getQuery($cLstWiR);
	
	while ( $wir = $dbs->getAssoc($rcLstWiR) ){
		if ( Purefy($_POST[$wir['id']]) == 1 ){
			$_SESSION["Edit_ID"] = $wir['id'];
		}
	}
}

if ( $task == "Setting_".$_SESSION["SaveSetting"] ){
	$cLstWiR = "Select * from ".$tblp."apps_package";
	$rcLstWiR = $dbs->getQuery($cLstWiR);
	
	while ( $wir = $dbs->getAssoc($rcLstWiR) ){
		if ( Purefy($_POST[$wir['id']]) == 1 ){
			$_SESSION["Edit_ID"] = $wir['id'];
		}
		$cLstWiRUpOne = "update ".$tblp."apps_package SET welcome_email = 0 where id='".$wir['id']."'";
		$dbs->getQuery($cLstWiRUpOne);
	}
	if(sizeof($_POST['welcome']) > 0){
		$artmp = array_map(function($num){return "'".$num."'";},$_POST['welcome']);
		$kondisi = implode(',',$artmp);

		$cLstWiRsd = "update ".$tblp."apps_package SET welcome_email = 1 where id in ($kondisi)";
		$rcLstWiRsd = $dbs->getQuery($cLstWiRsd);
	}
}

if ( $task == "pkgEdit_".$_SESSION["butts_Save"] ){
	$pkg_code = Purefy($_POST['pkg_code']);
	$pkg_name = Purefy($_POST['pkg_name']);
	$pkg_desc = Purefy(nl2br($_POST['pkg_desc']));
	$pkg_works = '';
	$scWi = $dbs->getQuery("Select id, kode from ".$tblp."apps_works order by urutan asc");
	while( $rscwi = $dbs->getAssoc($scWi)){
		if ( Purefy($_POST[$rscwi['id']]) == 1 ) { $pkg_works .= $rscwi['id'].":";  }
	}
	$pkg_works = preg_replace("/(.*)\:$/","\\1",$pkg_works);
	
	#Custom Package
	if ( Purefy($_POST['Custom'] == 1 ) ){
		$pkg_works .= 'Custom';
	}
	
	if ( empty($pkg_name) || empty($pkg_works) ){
		$AlertMess = 'alert-danger#remove_circle#ERROR!#Update Package Failed!|Form was incompleted!';
	}else{
		$in = "update ".$tblp."apps_package set nama = ?, works = ?, descs = ? where id = ?";
			$idPackage = md5(uniqids(microtime()));
			$bind = array( 
						"s|".$pkg_name, 
						"s|".$pkg_works, 
						"s|".$pkg_desc, 
						"s|".$_SESSION["Edit_ID"]
					);
			$dbs->getQuery($in,"Exec",$bind);
			
			#Untuk drwis
			$delPackItem = "delete from ".$tblp."apps_pkgitm where package_id = '".$_SESSION["Edit_ID"]."'";
			$dbs->getQuery($delPackItem);
			
			$IDWorks = explode(":",$pkg_works);
			foreach( $IDWorks as $IDW ){
				$inHub = "insert into ".$tblp."apps_pkgitm set id_pkgitm = ?, package_id = ?, mstitem_id = ?";
				$bind = array("s|".uniqids(md5(microtime())), "s|".$_SESSION["Edit_ID"], "s|".$IDW);
				$dbs->getQuery($inHub,"Exec",$bind);
			}
			
			# Create Json File for autocomplete
			require_once("Modules/".$cmodule."/autocomplete/crjson.php");
			CrJsonFile("Modules/".$cmodule."/autocomplete/JS_File_Package.json",$dbs,$tblp);
			
			$AlertMess = $lang_6030;
			$AlertMess = 'alert-success#done_all#SUCCESS!#Update Package Succeeded!';
			unset($_SESSION["Edit_ID"]);
	}
}

if ( $task == "pack_".$_SESSION["butts_Del"] ){
	$deltrue = "N";
	$cLstWiR = "Select id from ".$tblp."apps_package order by no_urut asc";
	$rcLstWiR = $dbs->getQuery($cLstWiR);
	while ( $wir = $dbs->getAssoc($rcLstWiR) ){
		if ( Purefy($_POST[$wir['id']]) == 1 ){
			$del = "delete from ".$tblp."apps_package where id = '".$wir['id']."'";
			$dbs->getQuery($del);
			
			$del2 = "delete from ".$tblp."apps_pkgitm where package_id = '".$wir['id']."'";
			$dbs->getQuery($del2);
			$deltrue = "Y";
		}
	}
	
	# Create Json File for autocomplete
	require_once("Modules/".$cmodule."/autocomplete/crjson.php");
	CrJsonFile("Modules/".$cmodule."/autocomplete/JS_File_Package.json",$dbs,$tblp);
	if ( $deltrue == "Y" ) { $AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_219; }else{ $AlertMess = 'alert-info#info#NOTHING TO DELETE!#'.$lang_220; }
 }
?>