<?php
if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Add"] ){
	$_SESSION["AddItm"] = "GasPol";
}

if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Reset"] ){
	unset($_SESSION["AddItm"]);
}

if ( $task == "wi_".$_SESSION["butts_Save"] ){
	$wi_code = Purefy($_POST['wi_code']);
	$wi_name = Purefy($_POST['wi_name']);
	$wi_desc = Purefy(nl2br($_POST['wi_desc']));
	$wi_dur = preg_replace("/\D/","",Purefy($_POST['wi_dur']));
	$wi_seq = preg_replace("/\D/","",Purefy($_POST['wi_seq']));
	$wi_Paralel = Purefy($_POST['wi_Paralel']);
	$wi_CntDown = Purefy($_POST['wi_CntDown']);
	if ( empty($wi_code) || empty($wi_name) || empty($wi_dur) || empty($wi_CntDown) || empty($wi_Paralel) || empty($wi_seq) ){
		$AlertMess = $lang_6020;
	}else{
		$cKode = "Select id from ".$tblp."apps_works where kode = '".$wi_code."'";
		$rcKode = $dbs->getArr($cKode);
		if ( !empty($rcKode['id']) ){
			$AlertMess = $lang_6022;
		}else{
			$in = "insert into ".$tblp."apps_works set 
				   id = ?, 
				   urutan = ?,
				   paralel = ?,
				   nama = ?, 
				   kode = ?,
				   wkt = ?, 
				   cntdown = ?, 
				   descs = ?";
			$bind = array("s|".md5(uniqids(microtime())), "i|".$wi_seq, "s|".$wi_Paralel, "s|".$wi_name, "s|".$wi_code, "i|".$wi_dur, "i|".$wi_CntDown, "s|".$wi_desc);
			$dbs->getQuery($in,"Exec",$bind); 
			$AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_6021;
			unset($_SESSION["AddItm"]);	
		}
	}
}


if ( $task == "wi_".$_SESSION["butts_Del"] ){
	$deltrue = "N";
	$cLstWiR = "Select id from ".$tblp."apps_works order by urutan asc";
	$rcLstWiR = $dbs->getQuery($cLstWiR);
	while ( $wir = $dbs->getAssoc($rcLstWiR) ){
		if ( Purefy($_POST[$wir['id']]) == 1 ){
			$del = "delete from ".$tblp."apps_works where id = '".$wir['id']."'";
			$dbs->getQuery($del);
			
			# Updating Packages
			$packUp = "delete from ".$tblp."apps_pkgitm where mstitem_id = '".$wir['id']."'";
			$dbs->getQuery($packUp);
			
			$deltrue = "Y";
		}
	}
	
	if ( $deltrue == "Y" ) { $AlertMess = 'alert-success#done_all#SUCCESS !#'.$lang_219; }else{ $AlertMess = 'alert-warning#warning#NOTHING TO DELETE!#'.$lang_220; }
	
 }
?>