<?php
if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Add"] ){
	$_SESSION["AddItm"] = "GasPol";
}

if ( $task == $rcmdet['nama']."_".$_SESSION["butts_Reset"] ){
	unset($_SESSION["AddItm"]);
}

if ( $task == "VOLoc_".$_SESSION["butts_Add"] ){
	$voloc = Purefy($_POST['voloc']);
	if ( !empty($voloc) ){
		$in = "insert into ".$tblp."master_volocation set 
			   id = ?, 
			   lokasi = ?";
		$bind = array("s|".md5(uniqids(microtime())), "s|".$voloc);
		$dbs->getQuery($in,"Exec",$bind);
		unset($_SESSION["AddItm"]);
	}
	# Create Json File for autocomplete
	require_once("Modules/".$cmodule."/autocomplete/crjson.php");
	CrJsonFileLocation("Modules/".$cmodule."/autocomplete/JS_File_Location.json",$dbs,$tblp);
	$AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_6050;
}

if ( $task == "Vo_".$_SESSION["butts_Del"] ){
	$deltrue = "N";
	$cLvo = "Select id from ".$tblp."master_volocation";
	$rcLvo = $dbs->getQuery($cLvo);
	while ( $tlvo = $dbs->getAssoc($rcLvo)){
		if ( Purefy($_POST[$tlvo['id']]) == 1 ){
			$del = "delete from ".$tblp."master_volocation where id = '".$tlvo['id']."'";
			$dbs->getQuery($del);
			$deltrue = "Y";
		}
	}
	
	if ( $deltrue == "Y" ) { $AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_219; }else{ $AlertMess = 'alert-info#info#NOTHING TO DELETE!#'.$lang_220; }
	
	# Create Json File for autocomplete
	require_once("Modules/".$cmodule."/autocomplete/crjson.php");
	CrJsonFileLocation("Modules/".$cmodule."/autocomplete/JS_File_Location.json",$dbs,$tblp);
}

if ( $task == "Vo_".$_SESSION["butts_Update"] ){
	$cLvo = "Select id from ".$tblp."master_volocation";
	$rcLvo = $dbs->getQuery($cLvo);
	while ( $tlvo = $dbs->getAssoc($rcLvo)){
		if ( Purefy($_POST[$tlvo['id']]) == 1 ){
			$cCrr = "select publish from ".$tblp."master_volocation where id = '".$tlvo['id']."'";
			$rcCrr = $dbs->getArr($cCrr);
			switch( $rcCrr['publish'] ){
				case "Y" : $nxtpub = "update ".$tblp."master_volocation set publish = 'N' where id = '".$tlvo['id']."'"; break;
				case "N" : $nxtpub = "update ".$tblp."master_volocation set publish = 'Y' where id = '".$tlvo['id']."'"; break;
			}
		
			if ( $nxtpub ){ $dbs->getQuery($nxtpub); }
			
		}
	}
	
	# Create Json File for autocomplete
	require_once("Modules/".$cmodule."/autocomplete/crjson.php");
	CrJsonFileLocation("Modules/".$cmodule."/autocomplete/JS_File_Location.json",$dbs,$tblp);
	
	$AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_6051;
}
?>
