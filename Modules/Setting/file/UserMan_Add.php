<form action="" method="post" name="AddNewUsr" id="AddNewUsr" class="dropzone" enctype="multipart/form-data">
<input type="hidden" name="task">
<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
            
            <div class="row">
                <div class="col">
					<div class="form-group">
						<label for="FullName"><?php echo $lang_6032 ?>*</label>
						<input id="FullName" name="FullName" type="text" class="form-control" placeholder="<?php echo $lang_6032 ?> ..." >
					</div>
				</div>
                
                <div class="col">
                    <div class="form-group">
						<label for="nUID"><?php echo $lang_6033 ?>*</label>
						<input id="nUID" name="nUID" type="text" class="form-control" placeholder="<?php echo $lang_6033 ?> ..." >
                        <div id='username_availability_result' class="PwdStats"></div>
					</div>  
				</div>    
            </div> 
            <div class="row">
                <div class="col">
					<div class="form-group">
						<label for="usr_pos"><?php echo $lang_6034 ?>*</label>
						<select id="usr_pos" name="usr_pos" class="form-control" style="width:150px;">
								<option value=""> -- </option>
                                <?php
									foreach( $UserPos as $UP_Code => $UP_Nm ){
										if ( $_SESSION["lgnPriv"] < $UP_Code ) {
											echo '<option value="'.$UP_Code.'">'.${"lang_".$UP_Nm}.'</option>';
										}
									}
								?>
                            </select>
					</div>
				</div>
                <div class="col">
					<div class="form-group">
                        
                        <label for="UpAv"><?php echo $lang_6040 ?></label>
                        
                        <div class="custom-file">
                        	<label for="UpAv" class="custom-file-label"><?php echo $lang_6040 ?></label>
                            <input id="UpAv" name="UpAv" type="file" class="custom-file-input" />
                        </div>
                        
                        <script>
						$(".custom-file-input").on("change", function() {
 							 var fileName = $(this).val().split("\\").pop();
							 var pjg = fileName.length;
							 var ambil = 47;
							 var mulai = pjg - ambil;
							 if ( pjg > ambil ){
							 	var shortText = '...' + jQuery.trim(fileName).substring(mulai);
							 }else{
							 	var shortText = fileName;
							 }
  							$(this).siblings(".custom-file-label").addClass("selected").html(shortText);
						});
						</script>

                    </div>
				</div>   
            </div>
            
            <script type="text/javascript" src="<?php echo BASEURL ?>assets/js/jquery.passstrength.js"></script>
            <script type='text/javascript'>
				$(document).ready(function(){
					var bind = function() {
			    		$('#PBm').passStrengthify({
								element: $('#out'),
								minimum: <?php echo MIN_PWD ?>,
								levels : ['Sangat Lemah', 'Sangat Lemah', 'Sangat Lemah', 'Lemah','Sedang', 'Baik', 'Sangat Baik'],
						        colours : ['gray', '#ad0000', '#ad0000', '#ec1212','#f9bd1a', '#9fdf17', '#209c31'],
								security:-1
						});
				  	};
					bind();

					var min_chars = <?php echo MIN_UID ?>;
					//var empty_field = '<span class=\"Small_Error_Mess\"><?php echo $lang_210 ?></span>';
					var characters_error = '<span class=\"Small_Error_Mess\"><?php echo $lang_210 ?></span>';
					var checking_html = '<img src="<?php echo BASEURL ?>templates/<?php echo THEMES ?>/images/loading.gif" /> Checking...';
					$('#nUID').keyup(function(){
						if($('#nUID').val().length==0){
							$('#username_availability_result').html(empty_field);
						} else if ($('#nUID').val().length < min_chars){
							$('#username_availability_result').html(characters_error);
						} else {
							$('#username_availability_result').html(checking_html);
							check_availability();
						}
					});
						
					function check_availability(){
						var username = $('#nUID').val().toLowerCase();
						var pg = 'chUser';
						var amodul = '<?php echo $cmodule ?>';

						$.ajax({
							url: '<?php echo BASEURL ?>system/ajax.php',
							type: 'POST',
							dataType: 'html',
							data: {unm:username, pg:pg, amod:amodul},
							success: function(result){
								if(result == 1){
									$('#username_availability_result').html('<img src="<?php echo BASEURL ?>templates/<?php echo THEMES ?>/sysicons/Ok_i.png" class="JvIcon" />'+' '+' <span class="Small_Ok_Mess"><b>' +username + ' </b>available</span>');
								} else if (result == 0) {
									$('#username_availability_result').html('<img src="<?php echo BASEURL ?>templates/<?php echo THEMES ?>/sysicons/No_i.png" class="JvIcon" />'+' '+' <span class="Small_Error_Mess"><b>' +username + ' </b>already taken</span>');
								}else if (result == 2 ){
									$('#username_availability_result').html('<img src="<?php echo BASEURL ?>templates/<?php echo THEMES ?>/sysicons/No_i.png" class="JvIcon" />'+' '+' <span class="Small_Error_Mess"><b>' +username + ' </b>invalid email address</span>');
								}
							},
   							//error: function(xhr, status, error) {
							//	alert("xhr: "+xhr.responseText);
   							//},
						});	
						
					}
						
					$('#UBm').keyup(function(){
						if ( $('#UBm').val() == '' || $('#PBm').val() == '' ) {
							$('#passwd_compare').html('');
						}else{
							if( ($('#UBm').val() === $('#PBm').val() ) ){
								$('#passwd_compare').html('<img src="<?php echo BASEURL ?>templates/<?php echo THEMES ?>/sysicons/Ok_i.png" class="JvIcon" />'+' '+'<span class="Small_Ok_Mess">Match</span>');
							}else{
								$('#passwd_compare').html('<img src="<?php echo BASEURL ?>templates/<?php echo THEMES ?>/sysicons/No_i.png" class="JvIcon" />'+' '+'<span class="Small_Error_Mess">Not Match</span>');
						}
					}
				});
			});
			</script>
            <div class="row">
                <div class="col">
					<div class="form-group">
						<label for="PBm"><?php echo $lang_6036 ?>*</label>
						<input id="PBm" name="PBm" type="password" class="form-control" placeholder="*****" style="width:150px;" >
                        <span id='out' class="PwdStats"></span>
					</div>
				</div>
                
                <div class="col">
                    <div class="form-group">
						<label for="UBm"><?php echo $lang_6037 ?>*</label>
						<input id="UBm" name="UBm" type="password" class="form-control" placeholder="*****" style="width:150px;" >
                        <span id='passwd_compare' class="PwdStatsMatch"></span>
					</div>  
				</div>    
            </div>
		</div>
	</div>
</div>

<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
			
            <div class="row">
            	<div class="col">
                	<span class="navbar-brand"><?php echo $lang_500 ?></span>
                    <div class="col-sm flex">&nbsp;</div>
                    <div class="col-sm flex">&nbsp;</div>
            	</div>
            </div>
            <div class="row">
            	<?php
					$cTotMenu = "select count(*) as jum from ".$tblp."sys_menu 
								  where publish = 'Y' and tingkat = 1";
					$rcTotMenu = $dbs->getArr($cTotMenu);
					$median = bcdiv($rcTotMenu['jum'],2,0);
					$u = 1;
					
					$cLstMenu = "Select id, nama, tipe_menu, tipe_konten, mandatory from ".$tblp."sys_menu where publish = 'Y' and tingkat = 1 order by no_urut asc";
					$rcLstMenu = $dbs->getQuery($cLstMenu);
					while ( $lstm = $dbs->getAssoc($rcLstMenu) ){
						if ( $u <= $median ){
							if ( $u == 1 ) { $ushow .= '<div class="col"><div class="form-group">'; }
							
							if ( $lstm['tipe_menu'] == "MENU" ){ 
									$nmmenu = strtoupper(${"lang_".$lstm['nama']}); 
									$ushow .= '<div class="col-sm-auto flex"><h6>'.$nmmenu.'</h6></div>';
									
									$cLstMenu2 = "Select id, nama, tipe_menu, tipe_konten, mandatory from ".$tblp."sys_menu 
												  where publish = 'Y' and tingkat = 2 and id_parent = '".$lstm['id']."' order by no_urut asc";
									$rcLstMenu2 = $dbs->getQuery($cLstMenu2);
									while ( $lstm2 = $dbs->getAssoc($rcLstMenu2) ){
										if ( $lstm2['tipe_menu'] == "MENU" ){ 
											$nmmenu = strtoupper(${"lang_".$lstm2['nama']}); 
											$ushow .= 	'<div class="col-sm-auto flex"><h6>'.$nmmenu.'</h6></div>';
										}else{
											$nmmenu = ${"lang_".$lstm2['nama']};
											if ( $lstm2['mandatory'] == 'Y' ) { $disa = "disabled checked"; }else{ $disa = ''; } 
											$ushow .= '	<div class="custom-control custom-checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
														<input type="checkbox" class="custom-control-input" id="'.$lstm2['id'].'" name="'.$lstm2['id'].'" value="1" '.$disa.'>
										  				<label class="custom-control-label" for="'.$lstm2['id'].'"><span>'.$nmmenu.'</span></label>
    	                			      				</div>';
										}
									}
									
									$ushow .= '<div class="col-sm flex">&nbsp;</div>';
							}else{
								$nmmenu = strtoupper(${"lang_".$lstm['nama']});
								if ( $lstm['mandatory'] == 'Y' ) {  $disa = "disabled checked"; }else{ $disa = ''; } 
									$ushow .= '	<div class="custom-control custom-checkbox col-sm flex">
										  		<input type="checkbox" class="custom-control-input" id="'.$lstm['id'].'" name="'.$lstm['id'].'" value="1" '.$disa.'>
										  		<label class="custom-control-label" for="'.$lstm['id'].'"><span><h6>'.$nmmenu.'</h6></span></label>
    	                			     		 </div>
										  		<div class="col-sm flex">&nbsp;</div>';
							}
							
							if ( ($u == $median) ) { $ushow .= '</div></div>'; }
						}
						
						if ( $u == $median ) { $u = 1; } else{ $u++; }
					}
					
					echo $ushow;
				?>
                
            </div>
            
        </div>
	</div>
</div>
</form>
<div class="text-right mb-5">
    <?php 
		echo ButtonsCommon("commonbuttons",THEMES,"Save_i",$lang_201,"NewUsr_".$_SESSION["butts_Save"],"AddNewUsr","","right");
	?>
</div>
