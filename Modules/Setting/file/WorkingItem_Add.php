<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
			<form action="" method="post" name="AddNewWI" id="AddNewWI">
            <input type="hidden" name="task">
            <div class="row">
				<div class="col">
					<div class="form-group">
						<label for="wi_seq"><?php echo $lang_6023 ?>*</label>
						<input id="wi_seq" name="wi_seq" type="text" class="form-control" placeholder="<?php echo $lang_6023 ?> ..." style="width:100px;" >
					</div>
				</div>
                <div class="col">
					<div class="form-group">
						<label for="wi_code"><?php echo $lang_6013 ?> *</label>
						<input id="wi_code" name="wi_code" type="text" class="form-control" placeholder="<?php echo $lang_6013 ?> ..." style="width:100px;" >
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="wi_name"><?php echo $lang_6014 ?> *</label>
						<input id="wi_name" name="wi_name" type="text" class="form-control" placeholder="<?php echo $lang_6014 ?> ..." >
					</div>
				</div>
			</div>

            <div class="form-group">
				<label for="desc"><?php echo $lang_6015 ?></label>
					<textarea id="wi_desc" name="wi_desc" rows="4" class="form-control" placeholder="<?php echo $lang_6015 ?> ..."></textarea>
			</div>

            <div class="row">
				<div class="col">
					<div class="form-group">
						<label for="wi_dur"><?php echo $lang_6016 ?> *</label>
						<input id="wi_dur" name="wi_dur" type="text" class="form-control" placeholder="<?php echo $lang_6016 ?> ..." style="width:100px;" >
					</div>
				</div>
                <div class="col">
					<div class="form-group">
						<label for="wi_CntDown"><?php echo $lang_6017 ?> *</label>
							<select id="wi_CntDown" name="wi_CntDown" class="form-control" style="width:150px;">
								<option value=""> -- </option>
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                            </select>
					</div>
				</div>
				<div class="col">
					<div class="form-group">
						<label for="wi_Paralel"><?php echo $lang_6024 ?> *</label>
							<select id="wi_Paralel" name="wi_Paralel" class="form-control" style="width:150px;">
								<option value=""> -- </option>
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                            </select>
					</div>
				</div>
			</div>
            </form>
          
        </div>
    </div>
</div>
<div class="text-right mb-5">
    <?php 
		echo ButtonsCommon("commonbuttons",THEMES,"Save_i",$lang_217,"wi_".$_SESSION["butts_Save"],"AddNewWI","","right");
	?>
</div>