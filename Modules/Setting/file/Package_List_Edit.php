<?php
$cDPack = "select * from ".$tblp."apps_package where id = '".$_SESSION["Edit_ID"]."'";
$rcDPack = $dbs->getArr($cDPack);
?>
<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
        	<form action="" method="post" name="AddNewPkg" id="AddNewPkg">
            <input type="hidden" name="task">
            <div class="row">
                <div class="col">
					<div class="form-group">
						<label for="pkg_code"><?php echo $lang_6025 ?>*</label>
						<input id="pkg_code" name="pkg_code" type="text" class="form-control" placeholder="<?php echo $lang_6025 ?> ..." style="width:100px;" value="<?php echo $rcDPack['kode'] ?>" disabled>
					</div>
				</div>
                
                <div class="col">
                    <div class="form-group">
						<label for="pkg_name"><?php echo $lang_6026 ?>*</label>
						<input id="pkg_name" name="pkg_name" type="text" class="form-control" placeholder="<?php echo $lang_6026 ?> ..." value="<?php echo $rcDPack['nama'] ?>">
					</div>  
				</div>    
            </div>
            <div class="form-group">
				<label for="pkg_desc"><?php echo $lang_6027 ?></label>
					<textarea id="pkg_desc" name="pkg_desc" rows="4" class="form-control" placeholder="<?php echo $lang_6027 ?> ..."><?php echo $rcDPack['descs'] ?></textarea>
			</div>
            <div class="form-group">
            	<label><?php echo $lang_6010 ?>*</label>
                <?php
					$wiArr = array();
					$cWorkPack = "select mstitem_id from ".$tblp."apps_pkgitm where package_id = '".$_SESSION["Edit_ID"]."'";
					$rcWorkPack = $dbs->getQuery($cWorkPack);
					while($wp = $dbs->getAssoc($rcWorkPack)){
						array_push($wiArr,$wp['mstitem_id']);
					}
					
					$cLWi = "Select id, kode, nama from ".$tblp."apps_works order by urutan asc";
					$rcLWi = $dbs->getQuery($cLWi);
					
					while ( $cwi = $dbs->getAssoc($rcLWi) ){
						if (in_array($cwi['id'], $wiArr)) { $wi_sel = "checked"; }else{ $wi_sel = ""; }
						echo '<div class="custom-control custom-checkbox">
							  <input type="checkbox" class="custom-control-input" id="'.$cwi['id'].'" name="'.$cwi['id'].'" value="1" '.$wi_sel.'>
							  <label class="custom-control-label" for="'.$cwi['id'].'"><span>'.$cwi['kode']." - ".$cwi['nama'].'</span></label>
    	        			  </div>';
					}
					
					if (in_array('Custom', $wiArr)) { $wi_sel = "checked"; }else{ $wi_sel = ""; }
					echo '<div class="custom-control custom-checkbox">
						  <input type="checkbox" class="custom-control-input" id="Custom" name="Custom" value="1" '.$wi_sel.'>
						  <label class="custom-control-label" for="Custom"><span>CUSTOM - Custom</span></label>
    	        		  </div>';
				?>
			</div>
            </form>
        </div>
    </div>
</div>
<div class="text-right mb-5">
	<?php
		echo ButtonsCommon("commonbuttons",THEMES,"Back_i","Cancel","pkgEdit_".$_SESSION["butts_Reset"],"AddNewPkg","","right");
		echo ButtonsCommon("commonbuttons",THEMES,"Save_i","Save Edit","pkgEdit_".$_SESSION["butts_Save"],"AddNewPkg","","right");
	?>
</div>