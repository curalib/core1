<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
        	
        <script>
			function generateDays(sel) {
    			var year = new Date().getFullYear();
				var month = document.getElementById('FixHolMonth').value
    			if(year == "" || month == "") {
					document.getElementById('MDay').innerHTML = "";
					//document.getElementById('MDay2').innerHTML = "";
       				document.getElementById('MDay').disabled = true;
					//document.getElementById('MDay2').disabled = true;
    			} else {
        			var days = new Date(year, month, 0).getDate();
        			document.getElementById('MDay').disabled = false;
        			document.getElementById('MDay').innerHTML = "";
					//document.getElementById('MDay2').disabled = false;
        			//document.getElementById('MDay2').innerHTML = "";
        			for (i = 1; i <= days; i++) {
            			document.getElementById('MDay').innerHTML += "<option value='"+i+"'>"+i+"</option>";
						//document.getElementById('MDay2').innerHTML += "<option value='"+i+"'>"+i+"</option>";
        			} 
    			}
			}
		</script>
            
        	<form action="" method="post" name="AddNewVoLoc" id="AddNewVoLoc">
            <input type="hidden" name="task">
             <div class="row">
				<div class="col-sm-auto">
            		<div class="form-group">
                    	<label for="FixHolMonth"><?php echo $lang_6056 ?></label>
                    		<select id="FixHolMonth" name="FixHolMonth" onchange="generateDays()" required class="custom-select" >
        						<option value="">--</option>
                    			<?php
								foreach($list_month as $lmsnum => $lmsname){
									echo '<option value="'.$lmsnum.'">'.$lmsname.'</option>';
								}
								?>
    					</select>
                    </div>
                </div>
                <div class="col-sm-auto">
                   	<div class="form-group">
                    	<label for="MDay"><?php echo $lang_6057 ?></label>
               			<select id="MDay" name="MDay" required disabled class="custom-select"></select>
               		</div>
               	</div>
                
                <div class="colp-sm-auto">
                	<div class="form-group">
                		<label for="HolidayDesc"><?php echo $lang_6055 ?></label>
            			<input id="HolidayDesc" name="HolidayDesc" type="text" class="form-control" placeholder="<?php echo $lang_6055 ?>" style="width:450px;">
                	</div>
                </div>
            </div>
            </form>
            <div class="text-right mb-5">
    			<?php 
					echo 
					ButtonsCommon("commonbuttons",THEMES,"Add_i",$lang_6043,"FixHoliday_".$_SESSION["butts_Add"],"AddNewVoLoc","","left")
					;
				?>
			</div>
            
        </div>
	</div>
</div>
<div class="card">
	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 18px;">
						<!--div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
							<label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
    	                </div-->
					</th>
					<th style="width: 175px;"><?php echo strtoupper($lang_6054) ?></th>
					<th style=""><?php echo strtoupper($lang_6055) ?></th>
				 </tr>
			</thead>
            <tbody>
             <?php
				echo '
					<form action="" name="LstHol" method="post" id="LstHol">
					<input type="hidden" name="task">
				';
				$lvo = "Select id, tgl, ket from ".$tblp."master_fixholiday where publish = 'Y' order by tgl asc";
				$rlvo = $dbs->getQuery($lvo);
				while( $vol = $dbs->getAssoc($rlvo)){
					$volc++;
					if ( $volc%2 == 0 ) { $wilsclass = "selected"; }else{ $wilsclass = ""; }
					echo '
						<tr class="'.$wilsclass.'">
							<td style="width: 18px;">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input js-toggle-check-all" id="'.$vol['id'].'" name="'.$vol['id'].'" value="1">
									<label class="custom-control-label" for="'.$vol['id'].'"><span class="text-hide">&nbsp;</span></label>
    	    	           		</div>
							</td>
							<td style="width: 175px;">'.c_date($vol['tgl'],"longdate","long",$list_month,$list_month_short,"N").'</td>
							<td>'.$vol['ket'].'</td>
						</tr>
					';
					}
					echo '</form>';
				?>
					
            </tbody>
		</table>
            
            
	</div>
</div>

<div class="table-responsive border-bottom">
	<div class="text-right mb-5">
    <?php 
		echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"FHol_".$_SESSION["butts_Del"],"LstHol",$lang_218,"right");
	?>
	</div>
</div>