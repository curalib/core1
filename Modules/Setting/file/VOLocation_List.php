<div class="card">
	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 18px;">
						<!--div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
							<label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
    	                </div-->
					</th>
					<th style="width: 100%;"><?php echo strtoupper($lang_6042) ?></th>
					<th style="width: 75px;"><?php echo strtoupper($lang_6044) ?></th>
				 </tr>
			</thead>
            <tbody>
            <?php
				echo '
				<form action="" name="VoLst" method="post" id="VoLst">
				<input type="hidden" name="task">';
				$lvo = "Select id, lokasi, publish from ".$tblp."master_volocation";
				$rlvo = $dbs->getQuery($lvo);
				while( $vol = $dbs->getAssoc($rlvo)){
					$volc++;
					if ( $volc%2 == 0 ) { $wilsclass = "selected"; }else{ $wilsclass = ""; }
					echo '
						<tr class="'.$wilsclass.'">
							<td style="width: 18px;">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input js-toggle-check-all" id="'.$vol['id'].'" name="'.$vol['id'].'" value="1">
									<label class="custom-control-label" for="'.$vol['id'].'"><span class="text-hide">&nbsp;</span></label>
    	    	           		</div>
							</td>
							<td style="width: 100%;">'.$vol['lokasi'].'</td>
							<td style="width: 75px;">'.$vol['publish'].'</td>
						</tr>
					';
				}
				?>
            </tbody>
    	</table>
	</div>
</div>
<div class="text-right mb-5">
    <?php 
		echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"Vo_".$_SESSION["butts_Del"],"VoLst",$lang_218,"right");
		echo ButtonsCommon("commonbuttons",THEMES,"Reset_i",$lang_6045,"Vo_".$_SESSION["butts_Update"],"VoLst","","right");
	?>
</div>