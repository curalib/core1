<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
        	<form action="" method="post" name="AddNewVoLoc" id="AddNewVoLoc">
            <input type="hidden" name="task">
            <div class="form-group">
             	<label for="voloc"><?php echo $lang_6042 ?></label>
				<input id="voloc" name="voloc" type="text" class="form-control">
             </div>

            <div class="text-right mb-5">
    			<?php
					echo
					ButtonsCommon("commonbuttons",THEMES,"Add_i",$lang_6043,"VOLoc_".$_SESSION["butts_Add"],"AddNewVoLoc","","left")
					;
				?>
			</div>

        </div>
	</div>
</div>
