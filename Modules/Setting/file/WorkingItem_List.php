<div class="card">
	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 18px;">
						<!--div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
							<label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
    	                </div-->
					</th>
					<th style="width: 30px;"><?php echo strtoupper($lang_6023) ?></th>
					<th style="width: 75px;"><?php echo strtoupper($lang_6013) ?></th>
					<th style="width: 150px;"><?php echo strtoupper($lang_6014) ?></th>
					<th style="width: 318px;"><?php echo strtoupper($lang_6015) ?></th>
					<th style="width: 150px; text-align: center;"><?php echo strtoupper($lang_6016) ?></th>
					<th style="width: 120px;"><?php echo strtoupper($lang_6017) ?></th>
					<!--th style="width: 24px;"></th-->
				 </tr>
			</thead>
    	    <tbody>
    	    	<?php
				echo '
				<form action="" name="wiDel" method="post" id="wiDel">
				<input type="hidden" name="task">';
				$cLstWi = "Select id, nama, kode, wkt, cntdown, descs, urutan from ".$tblp."apps_works order by urutan asc";
				$rcLstWi = $dbs->getQuery($cLstWi);
				while ( $lwi = $dbs->getAssoc($rcLstWi) ){
				$lwic++;
				if ( $lwic%2 == 0 ) { $wilsclass = "selected"; }else{ $wilsclass = ""; }
				echo '
				<tr class="'.$wilsclass.'">
					<td style="width: 18px;">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input js-toggle-check-all" id="'.$lwi['id'].'" name="'.$lwi['id'].'" value="1">
							<label class="custom-control-label" for="'.$lwi['id'].'"><span class="text-hide">&nbsp;</span></label>
    	    	           </div>
					</td>
					<td style="width: 30px; text-align: center;">'.$lwi['urutan'].'</td>
					<td style="width: 75px;">'.$lwi['kode'].'</td>
					<td style="width: 150px;">'.$lwi['nama'].'</td>
					<td style="width: 318px;">'.$lwi['descs'].'</td>
					<td style="width: 150px; text-align: center;">'.$lwi['wkt'].'</td>
					<td style="width: 120px; text-align: center;">'.$lwi['cntdown'].'</td>
    	        </tr>
				';
				}
				echo '</form>';
				?>
    		</tbody>
    	</table>
	</div>
</div>
<div class="text-right mb-5">
    <?php 
		//echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"wi_".$_SESSION["butts_Del"],"wiDel",$lang_218,"right");
	?>
</div>
