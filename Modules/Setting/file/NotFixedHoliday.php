<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
        	<form action="" method="post" name="AddNewVoLoc" id="AddNewVoLoc">
            <input type="hidden" name="task">
             <div class="row">
				<div class="col">
            		<div class="form-group">
						<label for="HolidayDate"><?php echo $lang_6054 ?></label>
            <input id="HolidayDate" name="HolidayDate" type="text" class="form-control" placeholder="Select date ..." value="<?php echo date("d/m/Y",strtotime( '-1 days' ))." to ".date("d/m/Y") ?>" data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y">
					</div>
        		</div>
                <div class="col">
                	<label for="HolidayDesc"><?php echo $lang_6055 ?></label>
            		<input id="HolidayDesc" name="HolidayDesc" type="text" class="form-control" placeholder="<?php echo $lang_6055 ?>" >
                </div>
            </div>
            </form>
            <div class="text-right mb-5">
    			<?php 
					echo 
					ButtonsCommon("commonbuttons",THEMES,"Add_i",$lang_6043,"NotFixHoliday_".$_SESSION["butts_Add"],"AddNewVoLoc","","left")
					;
				?>
			</div>
            
        </div>
	</div>
</div>

<div class="table-responsive border-bottom">
	<div class="text-left mb-3">
    	<script>
			$(document).ready(function() {
  				$('#FixHolMonth').on('change', function() {
					if ( $('#FixHolMonth').find(":selected").val() ) {
     					document.forms['ChooseYear'].submit();
  					}
				});
			});
		</script>
    	<form action="" method="post" name="ChooseYear">
        <input type="hidden" name="task" value="<?php echo $_SESSION["buttkey"]."ChYr" ?>">
		<select id="FixHolMonth" name="FixHolMonth" onchange="generateDays()" required class="custom-select" style="width:180px;" >
        	<option value="">Choose Year</option>
            <?php
				if ( !isset($_SESSION["Curr_Year"]) ){ $_SESSION["Curr_Year"] = date("Y"); }
				
				$cSY = "Select YEAR(tgl) as starty from ".$tblp."master_yearholiday where publish = 'Y' order by tgl asc limit 1";
				$rcSY = $dbs->getArr($cSY);
				
				$cEY = "Select YEAR(tgl) as endy from ".$tblp."master_yearholiday where publish = 'Y' order by tgl desc limit 1";
				$rcEY = $dbs->getArr($cEY);
				
				for( $yy=$rcSY['starty'];$yy<=$rcEY['endy'];$yy++){
					if ( $yy == date("Y") ){ $yy_add = "(Current Year)"; }else{ $yy_add = ""; }
					if ( $_SESSION["Curr_Year"] == $yy ) { $yy_sel = "selected"; }else{ $yy_sel = ""; }
					echo '<option value="'.$yy.'" '.$yy_sel.'>'.$yy.' '.$yy_add.'</option>';
				}
			?>            			
    	</select> 
        </form>
    </div>
</div>

<div class="card">
	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 18px;">
						<!--div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
							<label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
    	                </div-->
					</th>
					<th style="width: 175px;"><?php echo strtoupper($lang_6054) ?></th>
					<th style=""><?php echo strtoupper($lang_6055) ?></th>
				 </tr>
			</thead>
            <tbody>
             <?php
				echo '
					<form action="" name="LstHol" method="post" id="LstHol">
					<input type="hidden" name="task">
				';
				$lvo = "Select id, tgl, ket from ".$tblp."master_yearholiday where publish = 'Y' and YEAR(tgl) = ".$_SESSION["Curr_Year"]." order by tgl asc";
				$rlvo = $dbs->getQuery($lvo);
				while( $vol = $dbs->getAssoc($rlvo)){
					$volc++;
					if ( $volc%2 == 0 ) { $wilsclass = "selected"; }else{ $wilsclass = ""; }
					echo '
						<tr class="'.$wilsclass.'">
							<td style="width: 18px;">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input js-toggle-check-all" id="'.$vol['id'].'" name="'.$vol['id'].'" value="1">
									<label class="custom-control-label" for="'.$vol['id'].'"><span class="text-hide">&nbsp;</span></label>
    	    	           		</div>
							</td>
							<td style="width: 175px;">'.c_date($vol['tgl'],"longdate","short",$list_month,$list_month_short).'</td>
							<td>'.$vol['ket'].'</td>
						</tr>
					';
					}
					echo '</form>';
				?>
					
            </tbody>
		</table>
            
            
	</div>
</div>

<div class="table-responsive border-bottom">
	<div class="text-right mb-5">
    <?php 
		echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"NFHol_".$_SESSION["butts_Del"],"LstHol",$lang_218,"right");
	?>
	</div>
</div>