<div class="card card-form">
	<div class="row no-gutters">
    	<div class="col-lg-12 card-form__body card-body">
        	<form action="" method="post" name="AddNewPkg" id="AddNewPkg">
            <input type="hidden" name="task">
            <div class="row">
                <div class="col">
					<div class="form-group">
						<label for="pkg_code"><?php echo $lang_6025 ?>*</label>
						<input id="pkg_code" name="pkg_code" type="text" class="form-control" placeholder="<?php echo $lang_6025 ?> ..." style="width:100px;" >
					</div>
				</div>
                
                <div class="col">
                    <div class="form-group">
						<label for="pkg_name"><?php echo $lang_6026 ?>*</label>
						<input id="pkg_name" name="pkg_name" type="text" class="form-control" placeholder="<?php echo $lang_6026 ?> ..." >
					</div>  
				</div>    
            </div>
            <div class="form-group">
				<label for="pkg_desc"><?php echo $lang_6027 ?></label>
					<textarea id="pkg_desc" name="pkg_desc" rows="4" class="form-control" placeholder="<?php echo $lang_6027 ?> ..."></textarea>
			</div>
            <div class="form-group">
            	<label><?php echo $lang_6010 ?>*</label>
                <?php
					$cLWi = "Select id, kode, nama from ".$tblp."apps_works order by urutan asc";
					$rcLWi = $dbs->getQuery($cLWi);
					$wiArr = array();
					while ( $cwi = $dbs->getAssoc($rcLWi) ){
						echo '<div class="custom-control custom-checkbox">
							  <input type="checkbox" class="custom-control-input" id="'.$cwi['id'].'" name="'.$cwi['id'].'" value="1">
							  <label class="custom-control-label" for="'.$cwi['id'].'"><span>'.$cwi['kode']." - ".$cwi['nama'].'</span></label>
    	        			  </div>';
					}
					echo '<div class="custom-control custom-checkbox">
						  <input type="checkbox" class="custom-control-input" id="Custom" name="Custom" value="1">
						  <label class="custom-control-label" for="Custom"><span>CUSTOM - Custom</span></label>
    	        		  </div>';
				?>
			</div>
            </form>
        </div>
    </div>
</div>
<div class="text-right mb-5">
	<?php
	echo ButtonsCommon("commonbuttons",THEMES,"Save_i",$lang_217,"pkg_".$_SESSION["butts_Save"],"AddNewPkg","","right");
	?>
</div>