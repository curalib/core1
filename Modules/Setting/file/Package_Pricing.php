<div class="card">
	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 30px;"><?php echo strtoupper($lang_6025) ?></th>
					<th style="width: 75px;"><?php echo strtoupper($lang_6026) ?></th>
					<th style="width: 170px;"><?php echo strtoupper($lang_6027) ?></th>
					<th style="width: 318px;"><?php echo strtoupper($lang_6010) ?></th>
					<th style="width: 50px;"><?php echo strtoupper("Basic Price") ?></th>
				</tr>
			</thead>
			<tbody class="list" id="staff">
				<?php
				echo '
				<form action="" name="PackPrice" method="post" id="PackPrice">
				<input type="hidden" name="task">';
				$cLstWi = "Select id, nama, kode, works, descs, price from " . $tblp . "apps_package where publish = 'Y' order by no_urut asc";
				$rcLstWi = $dbs->getQuery($cLstWi);
				while ($lwi = $dbs->getAssoc($rcLstWi)) {
					$lwic++;
					if ($lwic % 2 == 0) {
						$wilsclass = "selected";
					} else {
						$wilsclass = "";
					}

					$worklist = array();
					$cworklist = "select mstitem_id from " . $tblp . "apps_pkgitm left join " . $tblp . "apps_works on (mstitem_id = id) where package_id = '" . $lwi['id'] . "' order by urutan asc";
					$rcworklist = $dbs->getQuery($cworklist);
					while ($lworklist = $dbs->getAssoc($rcworklist)) {
						array_push($worklist, $lworklist['mstitem_id']);
					}

					//$worklist = explode(":",$lwi['works']);
					$estTime = 0;
					$workshow = '';
					$biggest = 0;

					foreach ($worklist as $wl) {
						$cNmWi = "select nama, kode, wkt, cntdown, paralel from " . $tblp . "apps_works where id = '" . $wl . "'";
						$rcNmWi = $dbs->getArr($cNmWi);

						# Hitung Estimated Days
						if ($rcNmWi['cntdown'] == "Y") {
							$counted = "Counted";
							if ($rcNmWi['paralel'] == "N") {
								$estTime = bcadd($estTime, $rcNmWi['wkt'], 0);
							} else {
								if ($biggest < $rcNmWi['wkt']) {
									$biggest = $rcNmWi['wkt'];
								}
							}
						} else {
							$counted = "Not Counted";
						}
						# ---------------------------
						//$workshow .= $rcNmWi['kode']." - ".$rcNmWi['nama']." (".$rcNmWi['wkt']." Work Days - ".$counted.")<br>";
						//$workshow .= $rcNmWi['kode']." - ".$rcNmWi['nama']." (".$counted.")<br>";
						$workshow .= $rcNmWi['kode'] . " - " . $rcNmWi['nama'] . "<br>";
					}


					# Add Biggest Paralel Works to Estimated Time
					$estTime = bcadd($estTime, $biggest, 0);

					echo '
					<tr class="' . $wilsclass . '">
						<td style="width: 30px;">' . $lwi['kode'] . '</td>
						<td style="width: 75px;">' . $lwi['nama'] . '</td>
						<td style="width: 170px;">' . $lwi['descs'] . '</td>
						<td style="width: 318px;">' . $workshow . '</td>
						<!--td style="width: 50px; text-align: center;">' . $estTime . '</td-->
						' . "<script>jQuery(function($){ $('#" . $lwi['id'] . "').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});});</script>" . '
						<td style="width: 150px; text-align: right;">
							<input id="' . $lwi['id'] . '" name="' . $lwi['id'] . '" type="text" class="form-control" placeholder="Price..." style="width:130px; text-align:right" value=' . $lwi['price'] . ' >
						</td>
    	    	    </tr>
					';
				}
				echo '</form>';
				?>
			</tbody>
		</table>
	</div>
</div>
<div class="text-right mb-5">
	<?php
	echo ButtonsCommon("commonbuttons", THEMES, "Save_i", "Update Price", "packprice_" . $_SESSION["butts_Edit"], "PackPrice", '', "right");
	?>
</div>