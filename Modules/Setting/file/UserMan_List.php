<div class="card">
	<div class="table-responsive border-bottom">

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th style="width: 2%;">&nbsp;</th>
					<th style="width: 26%; flex-shrink: 0"><?php echo $lang_6032 ?></th>
					<th style="width: 20%;"><?php echo $lang_6034 ?></th>
					<th style="width: 20%;"><?php echo $lang_6033 ?></th>
					<th style="width: 20%;"><?php echo $lang_6039 ?></th>
					<th style="width: 5%;">Banning</th>
					<th style="width: 7%;">Login</th>
				</tr>
			</thead>
			<tbody class="list" id="staff">
				<?php
				echo '
			<form action="" name="UserDel" method="post" id="UserDel">
			<input type="hidden" name="task">
		';
				$cUserDetail = "select " . $tblp . "sys_lgndetail.id as ida, nama_asli, jabatan, uid, last_login, avatar, " . $tblp . "sys_login.login_id, " . $tblp . "sys_login.bann, " . $tblp . "sys_login.id as idl from " . $tblp . "sys_lgndetail left join " . $tblp . "sys_login on " . $tblp . "sys_login.id = " . $tblp . "sys_lgndetail.id_login where " . $tblp . "sys_lgndetail.priv > " . $_SESSION["lgnPriv"] . "";
				$rcUserDetail = $dbs->getQuery($cUserDetail);
				while ($userDetail = $dbs->getAssoc($rcUserDetail)) {
					$usrc++;
					$usrShowFrm .= '<form action="" name="UserCut_' . $usrc . '" method="post" id="UserCut_' . $usrc . '">
					 		<input type="hidden" name="task">
							<input type="hidden" name="cutid" value="' . $userDetail['idl'] . '">
					 		</form>';
					if ($userDetail['login_id']) {
						$lgnicon = "<a href=\"javascript:submitbutton('UserCut" . $_SESSION["butts_Reset"] . "','UserCut_" . $usrc . "');\">" . '<img src="' . BASEURL . 'templates/Default/sysicons/Green_i.png" class="JvIcon"></a>';
					} else {
						$lgnicon = '<img src="' . BASEURL . 'templates/Default/sysicons/Red_i.png" class="JvIcon">';
					}

					if (empty($userDetail['bann'])) {
						$banicon = "<a href=\"javascript:submitbutton('UserBann" . $_SESSION["butts_Reset"] . "','UserCut_" . $usrc . "');\">" . '<img src="' . BASEURL . 'templates/Default/sysicons/UBan_i.png" class="JvIcon"></a>';
					} else {
						$banicon = "<a href=\"javascript:submitbutton('UserUnBann" . $_SESSION["butts_Reset"] . "','UserCut_" . $usrc . "');\">" . '<img src="' . BASEURL . 'templates/Default/sysicons/UUnBan_i.png" class="JvIcon"></a>';
					}

					$usrShow .= '<tr>';
					$usrShow .= '<td style="">';
					$usrShow .= '<div class="custom-control custom-checkbox">';
					$usrShow .= '<input id="chkbox_' . $userDetail['ida'] . '" name="chkbox_' . $userDetail['ida'] . '" value="1" type="checkbox" class="custom-control-input js-check-selected-row">';
					$usrShow .= '<label class="custom-control-label" for="chkbox_' . $userDetail['ida'] . '"><span class="text-hide">Check</span></label>';
					$usrShow .= '</div>';
					$usrShow .= '</td>';

					$usrShow .= '<td style="">
							<div class="media align-items-center">
                            	<div class="avatar avatar-xs mr-2">'
						. "<img src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $userDetail["avatar"])) . "' class='avatar-img rounded-circle' alt='Avatar'>" .
						'</div>
                             	<div class="media-body">
									<span class="js-lists-values-employee-name">' . $userDetail['nama_asli'] . '</span>
								</div>
                             </div>
						 </td>
                         <td style="">' . $userDetail['jabatan'] . '</td>
                         <td style="">' . $userDetail['uid'] . '</td>
                         <td style="">' . c_date($userDetail['last_login'], "longdate", "long", $list_month, $list_month_short) . '</td>';
					$usrShow .= '</td>';
					$usrShow .= '<td style="text-align:center;">' . $banicon . '</td>';
					$usrShow .= '<td style="text-align:center;">' . $lgnicon . '</td>';
					$usrShow .= '</tr>';
				}
				echo $usrShow;
				echo '</form>';
				echo $usrShowFrm;
				?>

			</tbody>
		</table>
	</div>
</div>
<div class="text-right mb-5">
	<?php
	//echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"Usr_".$_SESSION["butts_Del"],"UserDel",$lang_218,"right");
	echo ButtonsCommon("commonbuttons", THEMES, "Edit_i", $lang_17, "Usr_" . $_SESSION["butts_Edit"], "UserDel", "", "right");
	?>
</div>