<div class="row">
	<div class="col-md-12">
		<div class="text-right mb-5">
			<?php
			//echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"pack_".$_SESSION["butts_Del"],"PackDel",$lang_218,"right");
			echo ButtonsCommon("commonbuttons", THEMES, "Ok_i", "Save Setting", "Setting_" . $_SESSION["SaveSetting"], "PackDel", '', "right");
			?>
		</div>
	</div>
</div>

<div class="card">

	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 18px;">&nbsp;

					</th>
					<th style="width: 30px;"><?php echo strtoupper($lang_6025) ?></th>
					<th style="width: 75px;"><?php echo strtoupper($lang_6026) ?></th>
					<th style="width: 170px;"><?php echo strtoupper($lang_6027) ?></th>
					<th style="width: 318px;"><?php echo strtoupper($lang_6010) ?></th>
					<th style="width: 50px;"><?php echo strtoupper($lang_6031) ?></th>
					<th style="width: 50px;">Welcome Email</th>
				</tr>
			</thead>
			<tbody class="list" id="staff">
				<?php
				echo '
				<form action="" name="PackDel" method="post" id="PackDel">
				<input type="hidden" name="task">';
				$cLstWi = "Select id, nama, kode, works, descs,welcome_email from " . $tblp . "apps_package where publish = 'Y' order by no_urut asc";
				$rcLstWi = $dbs->getQuery($cLstWi);
				while ($lwi = $dbs->getAssoc($rcLstWi)) {
					$lwic++;
					if ($lwic % 2 == 0) {
						$wilsclass = "selected";
					} else {
						$wilsclass = "";
					}

					$worklist = array();
					$cworklist = "select mstitem_id from " . $tblp . "apps_pkgitm left join " . $tblp . "apps_works on (mstitem_id = id) where package_id = '" . $lwi['id'] . "' order by urutan asc";
					$rcworklist = $dbs->getQuery($cworklist);
					while ($lworklist = $dbs->getAssoc($rcworklist)) {
						array_push($worklist, $lworklist['mstitem_id']);
					}

					//$worklist = explode(":",$lwi['works']);
					$estTime = 0;
					$workshow = '';
					$biggest = 0;
					$welcomec = $lwi['welcome_email'] == 1 ? '<input type="checkbox" checked="true"   id="welcome' . $lwi['id'] . '" name="welcome[]" value="' . $lwi['id'] . '">' : '<input type="checkbox"   id="welcome' . $lwi['id'] . '" name="welcome[]" value="' . $lwi['id'] . '">';
					foreach ($worklist as $wl) {
						$cNmWi = "select nama, kode, wkt, cntdown, paralel from " . $tblp . "apps_works where id = '" . $wl . "'";
						$rcNmWi = $dbs->getArr($cNmWi);

						# Hitung Estimated Days
						if ($rcNmWi['cntdown'] == "Y") {
							$counted = "Counted";
							if ($rcNmWi['paralel'] == "N") {
								$estTime = bcadd($estTime, $rcNmWi['wkt'], 0);
							} else {
								if ($biggest < $rcNmWi['wkt']) {
									$biggest = $rcNmWi['wkt'];
								}
							}
						} else {
							$counted = "Not Counted";
						}
						# ---------------------------
						//$workshow .= $rcNmWi['kode']." - ".$rcNmWi['nama']." (".$rcNmWi['wkt']." Work Days - ".$counted.")<br>";
						//$workshow .= $rcNmWi['kode']." - ".$rcNmWi['nama']." (".$counted.")<br>";
						$workshow .= $rcNmWi['kode'] . " - " . $rcNmWi['nama'] . "<br>";
					}


					# Add Biggest Paralel Works to Estimated Time
					$estTime = bcadd($estTime, $biggest, 0);

					echo '
					<tr class="' . $wilsclass . '">
    	    	    	<td style="width: 18px;">
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input js-toggle-check-all" id="' . $lwi['id'] . '" name="' . $lwi['id'] . '" value="1">
								<label class="custom-control-label" for="' . $lwi['id'] . '"><span class="text-hide">&nbsp;</span></label>
    	    	            </div>
						</td>
						<td style="width: 30px; text-align: center;">' . $lwi['kode'] . '</td>
						<td style="width: 75px;">' . $lwi['nama'] . '</td>
						<td style="width: 170px;">' . $lwi['descs'] . '</td>
						<td style="width: 318px;">' . $workshow . '</td>
						<td style="width: 50px; text-align: center;">' . $estTime . '</td>
						<td style="width: 50px; text-align: center;">
								' . $welcomec . '
    	    	            </td>
    	    	    </tr>
					';
				}
				echo '</form>';
				?>
			</tbody>
		</table>
	</div>
</div>
<div class="text-right mb-5">
	<?php
	//echo ButtonsCommon("commonbuttons",THEMES,"Del_i",$lang_13,"pack_".$_SESSION["butts_Del"],"PackDel",$lang_218,"right");
	echo ButtonsCommon("commonbuttons", THEMES, "Edit_i", "Edit Package", "pack_" . $_SESSION["butts_Edit"], "PackDel", '', "right");
	?>
</div>