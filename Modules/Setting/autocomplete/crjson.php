<?php
/*
This fucntion creates json file for package.
Create every add/del and edit.
*/
function CrJsonFile($jsonfile,$dbcon,$tblp){
	$jsonArr = "var datapackage = [";
	$JsonPrep = "select id, nama from ".$tblp."apps_package";
	$rJsonPrep = $dbcon->getQuery($JsonPrep);
	while( $jp = $dbcon->getAssoc($rJsonPrep)){
		$j++;
		$jp['nama'] = preg_replace("/\,/","",$jp['nama']);
		if ( $j == 1 ){ 
			$jsonArr .= "{ value: '".$jp['id']."', label: '".$jp['nama']."' }"; 
		}else{
			$jsonArr .= ",{ value: '".$jp['id']."', label: '".$jp['nama']."' }"; 
		}
	}
	$jsonArr .= "]";
	
	$handle = fopen($jsonfile, 'w');
	fwrite($handle, $jsonArr);
	fclose($handle);
}

/*
This fucntion creates json file for location.
Create every add/del and edit.
*/
function CrJsonFileLocation($jsonfile,$dbcon,$tblp){
	$jsonArr = "var datalocation = [";
	$JsonPrep = "select id, lokasi from ".$tblp."master_volocation where publish = 'Y'";
	$rJsonPrep = $dbcon->getQuery($JsonPrep);
	while( $jp = $dbcon->getAssoc($rJsonPrep)){
		$j++;
		$jp['lokasi'] = preg_replace("/\,/","",$jp['lokasi']);
		if ( $j == 1 ){ 
			$jsonArr .= "{ value: '".$jp['id']."', label: '".$jp['lokasi']."' }"; 
		}else{
			$jsonArr .= ",{ value: '".$jp['id']."', label: '".$jp['lokasi']."' }"; 
		}
	}
	$jsonArr .= "]";
	
	$handle = fopen($jsonfile, 'w');
	fwrite($handle, $jsonArr);
	fclose($handle);
}

// Create json for KBLI
function CrJsonFileKBLI($jsonfile,$dbcon,$tblp){
	$jsonArr = "var dataKBLI = [";
	$JsonPrep = "select id, kode, judul from ".$tblp."master_kbli order by kode asc";
	$rJsonPrep = $dbcon->getQuery($JsonPrep);
	while( $jp = $dbcon->getAssoc($rJsonPrep)){
		$j++;
		$jp['judul'] = preg_replace("/\,/","",$jp['judul']);
		$labels = $jp['kode']." - ".$jp['judul'];
		
		if ( $j == 1 ){ 
			$jsonArr .= "{ value: '".$jp['kode']."', label: '".$labels."' }"; 
		}else{
			$jsonArr .= ",{ value: '".$jp['kode']."', label: '".$labels."' }"; 
		}
	}
	$jsonArr .= "]";
	
	$handle = fopen($jsonfile, 'w');
	fwrite($handle, $jsonArr);
	fclose($handle);
}

/* ---------------------------------------------------- */
?>