
<!--===========DATA TRACKING=========================================================-->
<?php
//require "system/PagingTrack.php";
//require "page/Alert.php";
//echo '<div class="container-fluid page__container">';

/*
if ( isset($_SESSION["AddItm"]) ){
	$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Leads_Add.php";
}else{
	if ( isset($_SESSION["LeadDets"]) ){
		$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Leads_Dets.php";
	}elseif(isset($_SESSION["LeadChStat"]) ){
		$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Leads_Stats.php";
	}else{
		$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Leads_List.php";
	}
}
*/

if (isset($_SESSION["TrackingPage"])) {

    switch($_SESSION["TrackingPage"]) {
        case "AddTrack_pg":
            $fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/AddTrack_pg.php";
            //code to be executed if n=label1;
            break;
        case "Track_pg":
            $fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Track_pg.php";
            break;
        case "Timeline_pg":
            $fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Timeline_pg.php";
            break;
        default:
            //code to be executed if n is different from all labels;
    }

} else {
    //show list
    $fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/Track_List.php";
}

if ( is_file($fl) ) { require $fl; }else{ require $_SESSION["ROOT_DIR"]."/Modules/System/page/PageNotFound.php"; }
//echo '</div>';
?>
<!--===========END of DATA TRACKING=========================================================-->
