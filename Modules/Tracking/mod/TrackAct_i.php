<?php

$HitDays = new CountDays($dbs,$tblp);


if(isset($_POST['task'])) {
    $task = Purefy($_POST['task']);

    if($task == "SettingTracking_".$_SESSION["SaveSettingTracking"]){
        echo "<pre>";
        print_r($_POST);
        echo "</pre>";
    }

   /******************************************
    * Search Button
    ******************************************/
    if($task == "UpdateRecentActivity_".$_SESSION["butts_Update"]) {

        //if(isset($_POST['react_range']) && (!empty($_POST['react_range']) && $_POST['react_range'] != "Select date range ...")) {
        if(isset($_POST['react_range']) && !empty($_POST['react_range'])) {
            $filter_date = Purefy($_POST['react_range']);
            $date_fromS = substr($filter_date, 0, 10);
            $date_toS = substr($filter_date, -10);
            $date_range = $date_fromS." to ".$date_toS;
/*
echo "from: ".$date_from;
echo "to ".$date_to;
echo "rang: ".$date_range;
exit;
*/
            $date_to = str_replace('/', '-', $date_toS);
            $date_to = date("Y-m-d", strtotime($date_to));
            $date_from = str_replace('/', '-', $date_fromS);
            $date_from = date("Y-m-d", strtotime($date_from));
        }
        if(isset($_POST['track_status']) && !empty($_POST['track_status'])) {
            $track_status = Purefy($_POST['track_status']);
        } else {
            $track_status = 0;
        }


        require "Modules/Tracking/file/TrackActivity_i.php";
//        $_SESSION["ActivityPage"] = "TrackActivity_pg";

   }

    if($task == "Filter_By_".$_SESSION["butts_Update"]) {
        $track_status = Purefy($_POST['track_status']);
        if(isset($_POST['filter_by_name']) && !empty($_POST['filter_by_name'])) {
            $filter_by_name = Purefy($_POST['filter_by_name']);
        }
//echo $track_status;
//exit;
        if(isset($_POST['filter_date']) && (!empty($_POST['filter_date']) && $_POST['filter_date'] != "Select date range ...")) {
            $filter_date = Purefy($_POST['filter_date']);
            $date_fromS = substr($filter_date, 0, 10);
            $date_toS = substr($filter_date, -10);
            $date_range = $date_fromS." to ".$date_toS;
            /*
                 //$date_from = date('d/m/Y',strtotime('2019-04-04 - 27 days'));
                 //$date_to = date('d/m/Y',strtotime('2019-04-04'));
                 $date_from = date('d/m/Y',strtotime(date('d-m-Y').' - 30 days'));
                 $date_to = date('d/m/Y',strtotime(date('d-m-Y').' + 10 days'));
            */
            $date_to = str_replace('/', '-', $date_toS);
            $date_to = date("Y-m-d", strtotime($date_to));
            $date_from = str_replace('/', '-', $date_fromS);
            $date_from = date("Y-m-d", strtotime($date_from));
        }
        require "Modules/Tracking/file/TrackActivity_i.php";
//        $_SESSION["ActivityPage"] = "TrackActivity_pg";

   }

   if($task == "PermitDoneItem_".$_SESSION["butts_Update"]) {
       $ra_date_range = Purefy($_POST['ra_range']);
       $ra_date_fromS = substr($ra_date_range, 0, 10);
       $ra_date_toS = substr($ra_date_range, -10);
       //$ra_date_range = $ra_date_fromS." to ".$ra_date_toS;
       $ra_date_to = str_replace('/', '-', $ra_date_toS);
       $ra_date_to = date("Y-m-d", strtotime($ra_date_to));
       $ra_date_from = str_replace('/', '-', $ra_date_fromS);
       $ra_date_from = date("Y-m-d", strtotime($ra_date_from));

       $_SESSION["PermitTodayDateRange"] = $ra_date_range;

        $works_id_d = Purefy($_POST['selPermitDoneItem']);
        $works_id_w = Purefy($_POST['selPermitWaitItem']);
        $works_id_o = Purefy($_POST['selPermitOngoingItem']);
        $DoneTab = true;
        $WaitTab = false;
        $OngoingTab = false;
        $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
        $date_now = $dt->format('Y-m-d');
//        $date_now = '2019-05-08';
        //$works_id = '8c2ec23b4905f12a418bdf79e4366eab';//akta
        //$works_id = 'All Item';//akta
        require "Modules/Tracking/file/TrackActivity_i.php";
    }
    if($task == "PermitWaitingItem_".$_SESSION["butts_Update"]) {
        $ra_date_range = Purefy($_POST['ra_range']);
        $ra_date_fromS = substr($ra_date_range, 0, 10);
        $ra_date_toS = substr($ra_date_range, -10);
        //$ra_date_range = $ra_date_fromS." to ".$ra_date_toS;
        $ra_date_to = str_replace('/', '-', $ra_date_toS);
        $ra_date_to = date("Y-m-d", strtotime($ra_date_to));
        $ra_date_from = str_replace('/', '-', $ra_date_fromS);
        $ra_date_from = date("Y-m-d", strtotime($ra_date_from));

        $_SESSION["PermitTodayDateRange"] = $ra_date_range;

        $works_id_d = Purefy($_POST['selPermitDoneItem']);
        $works_id_w = Purefy($_POST['selPermitWaitItem']);
        $works_id_o = Purefy($_POST['selPermitOngoingItem']);
        $DoneTab = false;
        $WaitTab = true;
        $OngoingTab = false;
        $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
        $date_now = $dt->format('Y-m-d');

        require "Modules/Tracking/file/TrackActivity_i.php";
    }
    if($task == "PermitOngoingItem_".$_SESSION["butts_Update"]) {
        $ra_date_range = Purefy($_POST['ra_range']);
        $ra_date_fromS = substr($ra_date_range, 0, 10);
        $ra_date_toS = substr($ra_date_range, -10);
        //$ra_date_range = $ra_date_fromS." to ".$ra_date_toS;
        $ra_date_to = str_replace('/', '-', $ra_date_toS);
        $ra_date_to = date("Y-m-d", strtotime($ra_date_to));
        $ra_date_from = str_replace('/', '-', $ra_date_fromS);
        $ra_date_from = date("Y-m-d", strtotime($ra_date_from));

        $_SESSION["PermitTodayDateRange"] = $ra_date_range;

        $works_id_d = Purefy($_POST['selPermitDoneItem']);
        $works_id_w = Purefy($_POST['selPermitWaitItem']);
        $works_id_o = Purefy($_POST['selPermitOngoingItem']);
        $DoneTab = false;
        $WaitTab = false;
        $OngoingTab = true;
        $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
        $date_now = $dt->format('Y-m-d');

        require "Modules/Tracking/file/TrackActivity_i.php";
    }
    if($task == $_SESSION["buttkey"]."prev") {

        $tab_selected = substr($_POST["pageid"],-4);
        if($tab_selected == "Done") {
            $DoneTab = true;
            $WaitTab = false;
            $OngoingTab = false;
        } else if($tab_selected == "Wait") {
            $DoneTab = false;
            $WaitTab = true;
            $OngoingTab = false;
        } else {
            $DoneTab = false;
            $WaitTab = false;
            $OngoingTab = true;
        }
        $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
        $date_now = $dt->format('Y-m-d');
//        $date_now = '2019-05-08';

$ra_date_range = $_SESSION["PermitTodayDateRange"];
//$ra_date_range = Purefy($_POST['ra_range']);
$ra_date_fromS = substr($ra_date_range, 0, 10);
$ra_date_toS = substr($ra_date_range, -10);
//$ra_date_range = $ra_date_fromS." to ".$ra_date_toS;
$ra_date_to = str_replace('/', '-', $ra_date_toS);
$ra_date_to = date("Y-m-d", strtotime($ra_date_to));
$ra_date_from = str_replace('/', '-', $ra_date_fromS);
$ra_date_from = date("Y-m-d", strtotime($ra_date_from));



        //PAGING
        require "Modules/Tracking/file/TrackActivity_i.php";
    }


   /******************************************
    * End of Search Button
    ******************************************/

    /******************************************
     * Reset Search Button
     ******************************************/
    if($task == "ResetSearch_".$_SESSION["butts_Enter"]) {
        require "Modules/Tracking/file/Finished_i.php";
    }
    /******************************************
     * End of Reset Search Button
     ******************************************/

     /******************************************
      * Update Data Range
      ******************************************/
     //SHOW LINE IN DATE RANGE
     if($task == "UpdateDateRange_".$_SESSION["butts_Update"]) {
         $lead_status = Purefy($_POST['lead_status']);

         $ra_date_range = Purefy($_POST['ra_range']);
         $ra_date_fromS = substr($ra_date_range, 0, 10);
         $ra_date_toS = substr($ra_date_range, -10);
         //$ra_date_range = $ra_date_fromS." to ".$ra_date_toS;
         $ra_date_to = str_replace('/', '-', $ra_date_toS);
         $ra_date_to = date("Y-m-d", strtotime($ra_date_to));
         $ra_date_from = str_replace('/', '-', $ra_date_fromS);
         $ra_date_from = date("Y-m-d", strtotime($ra_date_from));

         $_SESSION["PermitTodayDateRange"] = $ra_date_range;

         $works_id_d = Purefy($_POST['selPermitDoneItem']);
         $works_id_w = Purefy($_POST['selPermitWaitItem']);
         $works_id_o = Purefy($_POST['selPermitOngoingItem']);
         $DoneTab = true;
         $WaitTab = false;
         $OngoingTab = false;


         require "Modules/Tracking/file/TrackActivity_i.php";

     }
     /******************************************
      * End of Update Data Range
      ******************************************/


} else {

    /******************************************
     * Finished Permit List
     ******************************************/
/*
     $track_status = "3"; //done

     $date_fromS = date('d/m/Y',strtotime('now - 30 days'));
     $date_toS = date('d/m/Y',strtotime('now'));

     $date_range = $date_fromS." to ".$date_toS;

     $date_to = str_replace('/', '-', $date_toS);
     $date_to = date("Y-m-d", strtotime($date_to));
     $date_from = str_replace('/', '-', $date_fromS);
     $date_from = date("Y-m-d", strtotime($date_from));
*/


$ra_date_fromS = date('d/m/Y',strtotime('now - 7 days'));
//$date_fromS = date('d/m/Y',strtotime('2019-04-04 - 7 days'));
//$date_toS = date('d/m/Y',strtotime('2019-04-04'));
$ra_date_toS = date('d/m/Y',strtotime('now'));

$ra_date_range = $ra_date_fromS." to ".$ra_date_toS;

$ra_date_to = str_replace('/', '-', $ra_date_toS);
$ra_date_to = date("Y-m-d", strtotime($ra_date_to));
$ra_date_from = str_replace('/', '-', $ra_date_fromS);
$ra_date_from = date("Y-m-d", strtotime($ra_date_from));

if(isset($_SESSION["PermitTodayDateRange"]) && !empty($_SESSION["PermitTodayDateRange"])) {
    unset($_SESSION["PermitTodayDateRange"]);
}

$_SESSION["PermitTodayDateRange"] = $ra_date_range;

$works_id_d = "All Item";
$works_id_w = "All Item";
$works_id_o = "All Item";

$DoneTab = true;
$WaitTab = false;
$OngoingTab = false;
$dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
$date_now = $dt->format('Y-m-d');
//$date_now = '2019-05-08';
//$works_id = '8c2ec23b4905f12a418bdf79e4366eab';//akta
//$works_id = 'All Item';//akta


    require "Modules/Tracking/file/TrackActivity_i.php";
    /******************************************
     * End of Finished Permit List
     ******************************************/
}


?>
