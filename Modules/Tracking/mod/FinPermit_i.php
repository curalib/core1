<?php

$HitDays = new CountDays($dbs, $tblp);

if (isset($_POST['task'])) {
    $task = Purefy($_POST['task']);

    /******************************************
     * Export to xls
     ******************************************/
    if ($task == "ExportToXLS_" . $_SESSION["butts_Enter"]) {
        if (isset($_POST['filter_name'])) {
            $_SESSION["TrackingFinishSearch"] = Purefy($_POST['filter_name']);
        }
        if (isset($_POST['filter_date']) && (!empty($_POST['filter_date']) && $_POST['filter_date'] != "Select date range ...")) {
            $_SESSION["TrackingFinishSearchDateRange"] = Purefy($_POST['filter_date']);
        }

        require "Modules/Tracking/file/Finished_i.php";
        require "Modules/Tracking/file/FinishedPermit_XLS.php";
    }
    /******************************************
     * End of Export to xls
     ******************************************/

    /******************************************
     * Search Button
     ******************************************/
    if ($task == "Search_" . $_SESSION["butts_Src"]) {
        if (isset($_POST['filter_name'])) {
            $_SESSION["TrackingFinishSearch"] = Purefy($_POST['filter_name']);
        }
        if (isset($_POST['filter_date']) && (!empty($_POST['filter_date']) && $_POST['filter_date'] != "Select date range ...")) {
            $_SESSION["TrackingFinishSearchDateRange"] = Purefy($_POST['filter_date']);
        }
        require "Modules/Tracking/file/Finished_i.php";
    }
    /******************************************
     * End of Search Button
     ******************************************/

    /******************************************
     * Reset Search Button
     ******************************************/
    if ($task == "Search_" . $_SESSION["butts_Reset"]) {
        unset($_SESSION["TrackingFinishSearch"]);
        unset($_SESSION["TrackingFinishSearchDateRange"]);
        require "Modules/Tracking/file/Finished_i.php";
    }
    /******************************************
     * End of Reset Search Button
     ******************************************/

    /******************************************
     * Restore Finished Permit
     ******************************************/
    if ($task == "TrackView_" . $_SESSION["butts_Enter"]) {
        $_SESSION["showtrackingid"] = Purefy($_POST['tracking_id']);

        //GET DIRUT DETAIL
        //-------------------------------------------------
        $FindDirut = get_find_dirut($dbs, $_SESSION["showtrackingid"]);

        $trkList_id = $FindDirut["id_trklist"];

        $cFindAttachments = "SELECT * FROM `izin_apps_trkfile` WHERE `trkfile_uploader_id` LIKE '{$trkList_id}%'";
        $rcFindAttachments = $dbs->getQuery($cFindAttachments);
        $FindAttachments = $dbs->getAll($rcFindAttachments);

        $lead_id = $FindDirut["id_lead"];
        $dirut_name = $FindDirut["dirut"];
        $dirut_birthdate = $FindDirut["tgl_lhr_dirut"];
        $arr_dirut_name = explode(" ", $dirut_name);
        $fix_dirut_name = implode("", $arr_dirut_name);
        $arr_dirut_birthdate = explode("-", $dirut_birthdate);
        $fix_dirut_birthdate = implode("", $arr_dirut_birthdate);
        $pwdtoshow = $fix_dirut_name . $fix_dirut_birthdate;

        // CLIENT DETAIL
        //-------------------------------------------------
        $ViewClient = get_client_detail($dbs, $_SESSION["showtrackingid"]);
        // CLIENT FILES
        //-------------------------------------------------
        $ViewClientFiles = get_client_files($dbs, $_SESSION["showtrackingid"]);
        // POPULATE TRACKING
        //-------------------------------------------------
        $rcViewTrack = get_track_detail($dbs, $_SESSION["showtrackingid"]);

        $_SESSION["TrackingPage"] = "TrackVO_pg";
    }

    if ($task == "CreatePasswd_" . $_SESSION["butts_Save"]) {

        $Email_Address = Purefy($_POST["client_email"]);
        $response = create_user($Email_Address);

        if (!in_array($response, [200, 201])) {
            $_SESSION["AlertMess"] = "Error!";
        } else {
            $_SESSION["AlertMess"] = "Success! Email has been sent to $Email_Address";
        }

        require "Modules/Tracking/file/Finished_i.php";
    }

    if ($task == "UpdatePasswd_" . $_SESSION["butts_Save"]) {

        $Email_Address = Purefy($_POST["client_email"]);
        $response = reset_password($Email_Address);

        if (!in_array($response, [200, 201])) {
            $_SESSION["AlertMess"] = "Error!";
        } else {
            $_SESSION["AlertMess"] = "Success! Email has been sent to $Email_Address";
        }

        require "Modules/Tracking/file/Finished_i.php";
    }

    if ($task == "DeleteUserFile") {

        $file_id = Purefy($_POST['fids']);
        $tracking_id = Purefy($_POST['tracking_id']);
        $lead_id = Purefy($_POST['lead_id']);
        $email = $_SESSION['lgnUid'];

        $response = delete_userfile(['file_id' => $file_id, 'tracking_id' => $tracking_id, 'lead_id' => $lead_id, 'email' => $email]);

        if (!in_array($response, [200])) {
            $_SESSION["AlertMess"] = "Error!";
        } else {
            $_SESSION["AlertMess"] = "Success! Record file has been removed";
        }

        require "Modules/Tracking/file/Finished_i.php";
    }

    /******************************************
     * End of Restore Finished Permit
     ******************************************/

    /******************************************
     * Restore Finished Permit
     ******************************************/
    if ($task == "RestoreTracking_" . $_SESSION["butts_Enter"]) {
        $tracking_id = Purefy($_POST['tracking_id']);
        $status_restore = 0;
        $cRestoreTrkList = "UPDATE `izin_apps_trklist` ";
        $cRestoreTrkList .= "SET `tgl_close` = NOW(), `trk_status` = " . $status_restore . " ";
        $cRestoreTrkList .= "WHERE `tracking_id` = '" . $tracking_id . "' ";
        $rcRestoreTrkList = $dbs->getQuery($cRestoreTrkList);

        //status gui
        $_SESSION["AlertMess"] .= "Tracking id: " . $tracking_id . " Restored!!"; //Tracking updated

        /*
        //save notif
        $cinsNotif = "INSERT INTO `izin_apps_notification` ";
        $cinsNotif .= "(id_notif, client_id, notif_msg, notif_badge, user_id) ";
        $cinsNotif .= "VALUES ('".md5(uniqids(microtime()))."', '".$_SESSION["tblID"]."', '".$update_status."', '1', '".$_SESSION["tblID"]."') ";
        $rcinsNotif = $dbs->getQuery($cinsNotif);
        */

        require "Modules/Tracking/file/Finished_i.php";
        //$_SESSION["TrackingPage"] = "";
    }

    /******************************************
     * End of Restore Finished Permit
     ******************************************/

    /******************************************
     * Breadcrumb finished list
     ******************************************/
    if ($task == "FinishedList_" . $_SESSION["butts_Enter"]) {
        require "Modules/Tracking/file/Finished_i.php";
    }
    /******************************************
     * End of Breadcrumb finished list
     ******************************************/

    /******************************************
     * Paging buttons
     ******************************************/
    if ($task == $_SESSION["buttkey"] . "prev") {
        //PAGING
        require "Modules/Tracking/file/Finished_i.php";
    }
    /******************************************
     * End of Paging buttons
     ******************************************/
} else {

    /**
     * AKTA SETTING
     */
    if (isset($_POST['SAVESETTINGAKTA'])) {
        $date = $_POST['aktaMulai'];
        $inYear = (int)$_POST['aktaTahun'];
        $newdate = date('Y-m-d', strtotime("+$inYear year", strtotime($date)));
        $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_akta_in = ?, reminder_akta = ? WHERE id_trklist = ?";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "i|$inYear",
            "d|$newdate",
            "s|" . $_POST['idtrack']
        ));

        unset($_SESSION["TrackingPage"]);
    }

    /**
     * HAKI SETTING
     */
    if (isset($_POST['SAVESETTINGHAKI'])) {
        if ($_POST['SAVESETTINGHAKI'] == 1) // link progress
        {
            $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_haki_link = ? WHERE id_trklist = ? ";
            $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
                "s|" . $_POST['reminder_haki_link'],
                "s|" . $_POST['idtrack']
            ));
        } elseif ($_POST['SAVESETTINGHAKI'] == 'header') // header merek & kelas
        {
            $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_haki_merek = ?, reminder_haki_kelas = ? WHERE id_trklist = ? ";
            $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
                "s|" . $_POST['reminder_haki_merek'],
                "i|" . $_POST['reminder_haki_kelas'],
                "s|" . $_POST['idtrack']
            ));
        }

        unset($_SESSION["TrackingPage"]);
    }

    if (isset($_POST['SAVEATTACHMENTHAKI'])) {
        $key = $_POST['SAVEATTACHMENTHAKI'];

        if (isset($_FILES['attachment_haki_' . $key]) && !empty($_FILES['attachment_haki_' . $key]['name'])) {
            $uploadDirectory = "../../" . UPDIR . "/Attachment_Haki";
            $AllowedMime = "";
            $uploadResult = $pm->File("attachment_haki_" . $key, ATTACHMENTFILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
            if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
                $dt = new DateTime('now', new DateTimezone(TIMESET));
                $date_now = $dt->format('Y-m-d H:i:s');

                $id_fileuniq = md5(uniqids(microtime()));
                $filenameuniq = $uploadResult[1];
                $filesize = $uploadResult[2];
                $filenameori = $uploadResult[3];
                $trkfile_uploader_id = $_POST['idtrack'] . "_attachment_haki_" . $key;

                //check for similar FILE name on db.
                $cFindFile = "SELECT * FROM  `izin_apps_trkfile` AS `trklist` WHERE `trkfile_uploader_id` = ? ";
                $rcFindFile = $dbs->getQuery($cFindFile, "Exec", array(
                    "s|$trkfile_uploader_id"
                ));

                //if file exists either replace/not continue with the task.
                if (($rcFindFile instanceof PDOStatement || count($dbs->getAll($rcFindFile)) > 0)) {
                    // rename nama uniq dan filesize di setiap baris trkitm yang sama
                    if ($FindFile = $dbs->getAssoc($rcFindFile)) {
                        $trkFile_id = $FindFile["id_trkfile"];
                        $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                        //hapus file yang ada dari folder unggah
                        try {
                            $exfilename = $uploadDirectory . "/" . $exfilenameuniq;
                            //if(delete_file($filename) === true) {
                            //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                            if (file_exists($exfilename)) {
                                $deleted = unlink($exfilename);
                            }
                        } catch (Exception $e) {
                            $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                        }

                        //update for file details on file db.
                        $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, `trkfile_size` = ?, `trkfile_name_ori` = ? WHERE `id_trkfile` = ? ";
                        $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", array(
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$filenameori",
                            "s|$trkFile_id"
                        ));
                    } else {
                        //insert for file details on file db.
                        $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` (id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name) ";
                        $cUpTrkFile .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";

                        $rcUpTrkFile = $dbs->getQuery($cUpTrkFile, "Exec", array(
                            "s|$id_fileuniq",
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$filenameori",
                            "d|$date_now",
                            "i|0",
                            "s|$trkfile_uploader_id",
                            "s|" . $_SESSION["lgnNama"]
                        ));
                    }
                }
            } else {
                //upload error
                $_SESSION["AlertMess"] = $uploadResult[0];
            }
        }

        unset($_SESSION["TrackingPage"]);
    }

    if (isset($_POST['SAVESENDINGHAKI'])) {
        require_once "SendEmail.php";

        $sendHakiemail = new SendEmail();
        $sendHakiemail->EmailHaki($_POST['email'], $_POST["SAVESENDINGHAKI"], $_POST['idtrack']);

        $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_haki = ? WHERE id_trklist = ? ";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "i|" . $_POST["SAVESENDINGHAKI"],
            "s|" . $_POST['idtrack']
        ));

        unset($_SESSION["TrackingPage"]);
    }

    /**
     * TAX SETTING
     */
    if (isset($_POST['SAVESENDINGTAX'])) {
        require_once "SendEmail.php";

        $sendTaxemail = new SendEmail();
        $sendTaxemail->EmailTax($_POST["email"], $_POST["nama"], $_POST["SAVESENDINGTAX"], $_POST['idtrack']);

        $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_tax = ? WHERE id_trklist = ? ";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "i|" . $_POST["SAVESENDINGTAX"],
            "s|" . $_POST['idtrack']
        ));

        unset($_SESSION["TrackingPage"]);
    }

    if (isset($_POST['SAVEATTACHMENTTAX'])) {
        $key = $_POST['SAVEATTACHMENTTAX'];

        if (isset($_FILES['attachment_tax_' . $key]) && !empty($_FILES['attachment_tax_' . $key]['name'])) {
            $uploadDirectory = "../../" . UPDIR . "/Attachment_Tax";
            $AllowedMime = "";
            $uploadResult = $pm->File("attachment_tax_" . $key, ATTACHMENTFILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
            if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
                $dt = new DateTime('now', new DateTimezone(TIMESET));
                $date_now = $dt->format('Y-m-d H:i:s');

                $id_fileuniq = md5(uniqids(microtime()));
                $filenameuniq = $uploadResult[1];
                $filesize = $uploadResult[2];
                $filenameori = $uploadResult[3];
                $trkfile_uploader_id = $_POST['idtrack'] . "_attachment_tax_" . $key;

                //check for similar FILE name on db.
                $cFindFile = "SELECT * FROM  `izin_apps_trkfile` AS `trklist` WHERE `trkfile_uploader_id` = ? ";
                $rcFindFile = $dbs->getQuery($cFindFile, "Exec", array(
                    "s|$trkfile_uploader_id"
                ));

                //if file exists either replace/not continue with the task.
                if (($rcFindFile instanceof PDOStatement || count($dbs->getAll($rcFindFile)) > 0)) {
                    // rename nama uniq dan filesize di setiap baris trkitm yang sama
                    if ($FindFile = $dbs->getAssoc($rcFindFile)) {
                        $trkFile_id = $FindFile["id_trkfile"];
                        $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                        //hapus file yang ada dari folder unggah
                        try {
                            $exfilename = $uploadDirectory . "/" . $exfilenameuniq;
                            //if(delete_file($filename) === true) {
                            //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                            if (file_exists($exfilename)) {
                                $deleted = unlink($exfilename);
                            }
                        } catch (Exception $e) {
                            $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                        }

                        //update for file details on file db.
                        $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, `trkfile_size` = ?, `trkfile_name_ori` = ? WHERE `id_trkfile` = ? ";
                        $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", array(
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$filenameori",
                            "s|$trkFile_id"
                        ));
                    } else {
                        //insert for file details on file db.
                        $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` (id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name) ";
                        $cUpTrkFile .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";

                        $rcUpTrkFile = $dbs->getQuery($cUpTrkFile, "Exec", array(
                            "s|$id_fileuniq",
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$filenameori",
                            "d|$date_now",
                            "i|0",
                            "s|$trkfile_uploader_id",
                            "s|" . $_SESSION["lgnNama"]
                        ));
                    }
                }
            } else {
                //upload error
                $_SESSION["AlertMess"] = $uploadResult[0];
            }
        }

        unset($_SESSION["TrackingPage"]);
    }

    if (isset($_POST['SAVEREMINDERTAX'])) {
        $key = $_POST['SAVEREMINDERTAX'];
        $value = $_POST['reminder_tax_' . $key];

        if ($key == '4') {
            $value = $_POST['reminder_tax_' . $key] . '-12-31';
        } else {
            $value = $_POST['reminder_tax_' . $key];
        }

        if (isset($value) && !empty($value)) {
            $cUpdTrkListW = "UPDATE izin_1.izin_apps_trklist SET reminder_tax_" . $key . " = ? WHERE id_trklist = ? ";
            $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
                "s|$value",
                "s|" . $_POST['idtrack']
            ));
        }

        unset($_SESSION["TrackingPage"]);
    }

    if (isset($_POST['SAVESETTINGLKPM'])) {
        $cUpdTrkListW = "UPDATE izin_1.izin_apps_trklist SET reminder_lkpm = ? WHERE id_trklist = ? ";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "i|" . $_POST['lkpm'],
            "s|" . $_POST['idtrack']
        ));

        $_SESSION["TrackingPage"] = "TrackVO_pg";
    }

    if (isset($_POST['SAVEATTACHMENTBAST'])) {
        $key = $_POST['SAVEATTACHMENTBAST'];

        if (isset($_FILES['attachment_bast_' . $key]) && !empty($_FILES['attachment_bast_' . $key]['name'])) {
            $uploadDirectory = "../../" . UPDIR . "/Attachment_Bast";
            $AllowedMime = "";
            $uploadResult = $pm->File("attachment_bast_" . $key, ATTACHMENTFILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
            if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
                $dt = new DateTime('now', new DateTimezone(TIMESET));
                $date_now = $dt->format('Y-m-d H:i:s');

                $id_fileuniq = md5(uniqids(microtime()));
                $filenameuniq = $uploadResult[1];
                $filesize = $uploadResult[2];
                $filenameori = $uploadResult[3];
                $trkfile_uploader_id = $_POST['idtrack'] . "_attachment_bast_" . $key;

                //check for similar FILE name on db.
                $cFindFile = "SELECT * FROM  `izin_apps_trkfile` AS `trklist` WHERE `trkfile_uploader_id` = ? ";
                $rcFindFile = $dbs->getQuery($cFindFile, "Exec", array(
                    "s|$trkfile_uploader_id"
                ));

                //if file exists either replace/not continue with the task.
                if (($rcFindFile instanceof PDOStatement || count($dbs->getAll($rcFindFile)) > 0)) {
                    // rename nama uniq dan filesize di setiap baris trkitm yang sama
                    if ($FindFile = $dbs->getAssoc($rcFindFile)) {
                        $trkFile_id = $FindFile["id_trkfile"];
                        $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                        //hapus file yang ada dari folder unggah
                        try {
                            $exfilename = $uploadDirectory . "/" . $exfilenameuniq;
                            //if(delete_file($filename) === true) {
                            //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                            if (file_exists($exfilename)) {
                                $deleted = unlink($exfilename);
                            }
                        } catch (Exception $e) {
                            $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                        }

                        //update for file details on file db.
                        $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, `trkfile_size` = ?, `trkfile_name_ori` = ? WHERE `id_trkfile` = ? ";
                        $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", array(
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$filenameori",
                            "s|$trkFile_id"
                        ));
                    } else {
                        //insert for file details on file db.
                        $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` (id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name) ";
                        $cUpTrkFile .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";

                        $rcUpTrkFile = $dbs->getQuery($cUpTrkFile, "Exec", array(
                            "s|$id_fileuniq",
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$filenameori",
                            "d|$date_now",
                            "i|0",
                            "s|$trkfile_uploader_id",
                            "s|" . $_SESSION["lgnNama"]
                        ));
                    }
                }
            } else {
                //upload error
                $_SESSION["AlertMess"] = $uploadResult[0];
            }
        }

        unset($_SESSION["TrackingPage"]);
    }

    /******************************************
     * Finished Permit List
     ******************************************/
    require "Modules/Tracking/file/Finished_i.php";
    /******************************************
     * End of Finished Permit List
     ******************************************/
}

function get_find_dirut($dbs, $tracking_id)
{
    $cFindDirut = "SELECT `track`.`reminder_tax`,`track`.`reminder_haki`,`track`.`reminder_akta_in`,`track`.`reminder_akta`,`track`.`reminder_lkpm`,`track`.`id_trklist`,`track`.`tracking_id`,`track`.`trk_password`, `lead`.`client_id`,`lead`.`client_email`,`track`.`reminder_tax_0`, `track`.`reminder_tax_1`, `track`.`reminder_tax_2`, `track`.`reminder_tax_3`, `track`.`reminder_tax_4`, `track`.`reminder_tax_5`, ";
    $cFindDirut .= "`track`.`ultah_perusahaan`,`track`.`ultah_direktur`,`track`.`company_name`,`lead`.`id_lead`,`lead`.`dirut`, `lead`.`tgl_lhr_dirut`,  ";
    $cFindDirut .= "`track`.`reminder_haki_merek`, `track`.`reminder_haki_kelas`, `track`.`reminder_haki_link`, ";
    $cFindDirut .= "CASE WHEN EXISTS (SELECT * FROM `track_auth_identities` WHERE `type` = 'email_password' AND `secret` = `lead`.`client_email`) then 1 ELSE 0 END AS isActivated ";
    $cFindDirut .= "FROM `izin_apps_trklist` AS `track` ";
    $cFindDirut .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
    $cFindDirut .= "WHERE `track`.`tracking_id` = ? ";
    $rcFindDirut = $dbs->getQuery($cFindDirut, "Exec", ["s|$tracking_id"]);

    return $dbs->getAssoc($rcFindDirut);
}

function get_client_detail($dbs, $tracking_id)
{
    $cViewClient = "SELECT `trklist`.`company_name`, `lead`.`client_id`, `lead`.`client_email`, `lead`.`client_name`, `trklist`.`tracking_id`, `lead`.`package_name`, `lead`.`package_id`, ";
    $cViewClient .= "CASE WHEN `trklist`.`tracking_id` IN (SELECT tracking_id FROM `track_users_file` WHERE `deleted_at` IS NULL) THEN 'Y' ELSE 'N' END AS ownFile  ";
    $cViewClient .= "FROM `izin_apps_lead` AS `lead` ";
    $cViewClient .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `lead`.`id_lead`=`trklist`.`lead_id` ";
    $cViewClient .= "WHERE `trklist`.`tracking_id` = ? ";
    $rcViewClient = $dbs->getQuery($cViewClient, "Exec", ["s|$tracking_id"]);

    return $dbs->getAssoc($rcViewClient);
}

function get_client_files($dbs, $tracking_id)
{
    $cViewClientFiles = "SELECT * FROM `track_users_file` WHERE `tracking_id` = ? AND `deleted_at` IS NULL ORDER BY created_at DESC ";
    $rcViewClientFiles = $dbs->getQuery($cViewClientFiles, "Exec", ["s|$tracking_id"]);

    return $dbs->getAll($rcViewClientFiles);
}

function get_track_detail($dbs, $tracking_id)
{
    $cViewTrack = "SELECT `track`.`id_trklist`, `track`.`tracking_id`, `track`.`company_name`, `lead`.`client_id`, `lead`.`client_name`, `lead`.`package_name`, ";
    $cViewTrack .= "`item`.`id_trkitm`, `item`.`tgl_update_itm`, `item`.`reason_title`, `item`.`reason_note`, `item`.`trkitm_status`, `item`.`id_trkfile`, `item`.`trkfile_name_uniq`, ";
    $cViewTrack .= "`item`.`kode` FROM `izin_apps_trklist` AS `track` ";
    $cViewTrack .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
    $cViewTrack .= "LEFT JOIN ( ";
    $cViewTrack .= "SELECT `trkitm`.`id_trkitm`, `trkitm`.`trklist_id`, `trkitm`.`tgl_update_itm`, `trkitm`.`reason_title`, `trkitm`.`reason_note`, `trkitm`.`trkitm_status`, `trkfile`.`id_trkfile`, ";
    $cViewTrack .= "`trkfile`.`trkfile_name_uniq`, `trkitm`.`stakeholder_id`, `works`.`id`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
    $cViewTrack .= "LEFT JOIN `izin_apps_trkfile` AS `trkfile` ON `trkitm`.`trkfile_id`=`trkfile`.`id_trkfile` ";
    $cViewTrack .= "INNER JOIN `izin_apps_works` AS `works` ON `works`.`id`=`trkitm`.`works_id` ";
    $cViewTrack .= ") AS `item` ON `item`.`trklist_id` = `track`.`id_trklist` ";
    $cViewTrack .= "WHERE `lead`.`status` = ? AND `track`.`tracking_id` = ? ";
    $cViewTrack .= "ORDER BY `item`.`tgl_update_itm` DESC";

    return $dbs->getQuery($cViewTrack, "Exec", ["s|CL", "s|$tracking_id"]);
}
