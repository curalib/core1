<?php

$HitDays = new CountDays($dbs, $tblp);

function saveReminder($dbs, $kode, $tanggal, $email, $idTrack)
{
    if ($kode == 'ULTAHPERUSAHAAN') {
        $field = 'ultah_perusahaan';
    } elseif ($kode == 'ULTAHDIREKTUR') {
        $field = 'ultah_direktur';
    } else {
        return;
    }

    if (empty($tanggal)) {
        $tanggal = '0000-00-00';
    }

    $cUpdTrkListW = "UPDATE izin_1.izin_apps_trklist SET $field = '$tanggal' WHERE id_trklist = '$idTrack' ";
    if ($dbs->getQuery($cUpdTrkListW)) {
        // check reminder if exist
        $cFind = "SELECT * FROM `izin_1`.`reminder` WHERE kode = '$kode' AND email = '$email' ";
        $rcFind = $dbs->getQuery($cFind);
        $result = $dbs->getAll($rcFind);

        if (($rcFind instanceof PDOStatement && count($result) > 0)) {
            foreach ($result as $val) {
                $sql = "UPDATE `izin_1`.`reminder` SET tanggal = '$tanggal' WHERE id = '" . $val["id"] . "' ";
                $dbs->getQuery($sql);
            }
        } elseif (($rcFind instanceof PDOStatement && count($result) == 0)) {
            $sql = "INSERT INTO `izin_1`.`reminder` (`kode`, `tanggal`, `email`) VALUES ('$kode', '$tanggal', '$email')";
            $dbs->getQuery($sql);
        }
    }
}

if (isset($_POST['SAVESETTING'])) {
    $cUpdTrkListW = "UPDATE izin_apps_trklist SET company_name = ? WHERE id_trklist = ?";
    $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
        "s|" . $_POST['namaPerusahaan'],
        "s|" . $_POST['idtrack']
    ));

    // replicate to leads
    $cUpdLeadtW = "UPDATE izin_apps_lead SET company_name = ? WHERE id_lead = ?";
    $rcUpdLeadW = $dbs->getQuery($cUpdLeadtW, "Exec", array(
        "s|" . $_POST['namaPerusahaan'],
        "s|" . $_POST['idlead']
    ));

    saveReminder($dbs, 'ULTAHPERUSAHAAN', $_POST['ultahPerusahaan'], $_POST['email'], $_POST['idtrack']);
    saveReminder($dbs, 'ULTAHDIREKTUR', $_POST['ultahDirektur'], $_POST['email'], $_POST['idtrack']);

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}
if (isset($_POST['SAVESETTINGAKTA'])) {
    $date = $_POST['aktaMulai'];
    $inYear = (int)$_POST['aktaTahun'];
    $newdate = date('Y-m-d', strtotime("+$inYear year", strtotime($date)));
    $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_akta_in = ?, reminder_akta = ? WHERE id_trklist = ?";
    $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
        "i|$inYear",
        "d|$newdate",
        "s|" . $_POST['idtrack']
    ));

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

/**
 * HAKI SETTING
 */
if (isset($_POST['SAVESETTINGHAKI'])) {
    if ($_POST['SAVESETTINGHAKI'] == 1) // link progress
    {
        $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_haki_link = ? WHERE id_trklist = ? ";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "s|" . $_POST['reminder_haki_link'],
            "s|" . $_POST['idtrack']
        ));
    } elseif ($_POST['SAVESETTINGHAKI'] == 'header') // header merek & kelas
    {
        $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_haki_merek = ?, reminder_haki_kelas = ? WHERE id_trklist = ? ";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "s|" . $_POST['reminder_haki_merek'],
            "i|" . $_POST['reminder_haki_kelas'],
            "s|" . $_POST['idtrack']
        ));
    }

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['SAVEATTACHMENTHAKI'])) {
    $key = $_POST['SAVEATTACHMENTHAKI'];

    if (isset($_FILES['attachment_haki_' . $key]) && !empty($_FILES['attachment_haki_' . $key]['name'])) {
        $uploadDirectory = "../../" . UPDIR . "/Attachment_Haki";
        $AllowedMime = "";
        $uploadResult = $pm->File("attachment_haki_" . $key, ATTACHMENTFILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
        if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
            $dt = new DateTime('now', new DateTimezone(TIMESET));
            $date_now = $dt->format('Y-m-d H:i:s');

            $id_fileuniq = md5(uniqids(microtime()));
            $filenameuniq = $uploadResult[1];
            $filesize = $uploadResult[2];
            $filenameori = $uploadResult[3];
            $trkfile_uploader_id = $_POST['idtrack'] . "_attachment_haki_" . $key;

            //check for similar FILE name on db.
            $cFindFile = "SELECT * FROM  `izin_apps_trkfile` AS `trklist` WHERE `trkfile_uploader_id` = ? ";
            $rcFindFile = $dbs->getQuery($cFindFile, "Exec", array(
                "s|$trkfile_uploader_id"
            ));

            //if file exists either replace/not continue with the task.
            if (($rcFindFile instanceof PDOStatement || count($dbs->getAll($rcFindFile)) > 0)) {
                // rename nama uniq dan filesize di setiap baris trkitm yang sama
                if ($FindFile = $dbs->getAssoc($rcFindFile)) {
                    $trkFile_id = $FindFile["id_trkfile"];
                    $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                    //hapus file yang ada dari folder unggah
                    try {
                        $exfilename = $uploadDirectory . "/" . $exfilenameuniq;
                        //if(delete_file($filename) === true) {
                        //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                        if (file_exists($exfilename)) {
                            $deleted = unlink($exfilename);
                        }
                    } catch (Exception $e) {
                        $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                    }

                    //update for file details on file db.
                    $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, `trkfile_size` = ?, `trkfile_name_ori` = ? WHERE `id_trkfile` = ? ";
                    $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", array(
                        "s|$filenameuniq",
                        "i|$filesize",
                        "s|$filenameori",
                        "s|$trkFile_id"
                    ));
                } else {
                    //insert for file details on file db.
                    $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` (id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name) ";
                    $cUpTrkFile .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";

                    $rcUpTrkFile = $dbs->getQuery($cUpTrkFile, "Exec", array(
                        "s|$id_fileuniq",
                        "s|$filenameuniq",
                        "i|$filesize",
                        "s|$filenameori",
                        "d|$date_now",
                        "i|0",
                        "s|$trkfile_uploader_id",
                        "s|" . $_SESSION["lgnNama"]
                    ));
                }
            }
        } else {
            //upload error
            $_SESSION["AlertMess"] = $uploadResult[0];
        }
    }

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['SAVESENDINGHAKI'])) {
    require_once "SendEmail.php";

    $sendHakiemail = new SendEmail();
    $sendHakiemail->EmailHaki($_POST['email'], $_POST["SAVESENDINGHAKI"], $_POST['idtrack']);

    $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_haki = ? WHERE id_trklist = ? ";
    $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
        "i|" . $_POST["SAVESENDINGHAKI"],
        "s|" . $_POST['idtrack']
    ));

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

/**
 * TAX SETTING
 */
if (isset($_POST['SAVESENDINGTAX'])) {
    require_once "SendEmail.php";

    $sendTaxemail = new SendEmail();
    $sendTaxemail->EmailTax($_POST["email"], $_POST["nama"], $_POST["SAVESENDINGTAX"], $_POST['idtrack']);

    $cUpdTrkListW = "UPDATE izin_apps_trklist SET reminder_tax = ? WHERE id_trklist = ? ";
    $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
        "i|" . $_POST["SAVESENDINGTAX"],
        "s|" . $_POST['idtrack']
    ));

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['SAVEATTACHMENTTAX'])) {
    $key = $_POST['SAVEATTACHMENTTAX'];

    if (isset($_FILES['attachment_tax_' . $key]) && !empty($_FILES['attachment_tax_' . $key]['name'])) {
        $uploadDirectory = "../../" . UPDIR . "/Attachment_Tax";
        $AllowedMime = "";
        $uploadResult = $pm->File("attachment_tax_" . $key, ATTACHMENTFILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
        if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
            $dt = new DateTime('now', new DateTimezone(TIMESET));
            $date_now = $dt->format('Y-m-d H:i:s');

            $id_fileuniq = md5(uniqids(microtime()));
            $filenameuniq = $uploadResult[1];
            $filesize = $uploadResult[2];
            $filenameori = $uploadResult[3];
            $trkfile_uploader_id = $_POST['idtrack'] . "_attachment_tax_" . $key;

            //check for similar FILE name on db.
            $cFindFile = "SELECT * FROM  `izin_apps_trkfile` AS `trklist` WHERE `trkfile_uploader_id` = ? ";
            $rcFindFile = $dbs->getQuery($cFindFile, "Exec", array(
                "s|$trkfile_uploader_id"
            ));

            //if file exists either replace/not continue with the task.
            if (($rcFindFile instanceof PDOStatement || count($dbs->getAll($rcFindFile)) > 0)) {
                // rename nama uniq dan filesize di setiap baris trkitm yang sama
                if ($FindFile = $dbs->getAssoc($rcFindFile)) {
                    $trkFile_id = $FindFile["id_trkfile"];
                    $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                    //hapus file yang ada dari folder unggah
                    try {
                        $exfilename = $uploadDirectory . "/" . $exfilenameuniq;
                        //if(delete_file($filename) === true) {
                        //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                        if (file_exists($exfilename)) {
                            $deleted = unlink($exfilename);
                        }
                    } catch (Exception $e) {
                        $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                    }

                    //update for file details on file db.
                    $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, `trkfile_size` = ?, `trkfile_name_ori` = ? WHERE `id_trkfile` = ? ";
                    $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", array(
                        "s|$filenameuniq",
                        "i|$filesize",
                        "s|$filenameori",
                        "s|$trkFile_id"
                    ));
                } else {
                    //insert for file details on file db.
                    $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` (id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name) ";
                    $cUpTrkFile .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";

                    $rcUpTrkFile = $dbs->getQuery($cUpTrkFile, "Exec", array(
                        "s|$id_fileuniq",
                        "s|$filenameuniq",
                        "i|$filesize",
                        "s|$filenameori",
                        "d|$date_now",
                        "i|0",
                        "s|$trkfile_uploader_id",
                        "s|" . $_SESSION["lgnNama"]
                    ));
                }
            }
        } else {
            //upload error
            $_SESSION["AlertMess"] = $uploadResult[0];
        }
    }

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['SAVEREMINDERTAX'])) {
    $key = $_POST['SAVEREMINDERTAX'];
    $value = $_POST['reminder_tax_' . $key];

    if ($key == '4') {
        $value = $_POST['reminder_tax_' . $key] . '-12-31';
    } else {
        $value = $_POST['reminder_tax_' . $key];
    }

    if (isset($value) && !empty($value)) {
        $cUpdTrkListW = "UPDATE izin_1.izin_apps_trklist SET reminder_tax_" . $key . " = ? WHERE id_trklist = ? ";
        $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
            "s|$value",
            "s|" . $_POST['idtrack']
        ));
    }

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['SAVESETTINGLKPM'])) {
    $cUpdTrkListW = "UPDATE izin_1.izin_apps_trklist SET reminder_lkpm = ? WHERE id_trklist = ? ";
    $rcUpdTrkListW = $dbs->getQuery($cUpdTrkListW, "Exec", array(
        "i|" . $_POST['lkpm'],
        "s|" . $_POST['idtrack']
    ));

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['SAVEATTACHMENTBAST'])) {
    $key = $_POST['SAVEATTACHMENTBAST'];

    if (isset($_FILES['attachment_bast_' . $key]) && !empty($_FILES['attachment_bast_' . $key]['name'])) {
        $uploadDirectory = "../../" . UPDIR . "/Attachment_Bast";
        $AllowedMime = "";
        $uploadResult = $pm->File("attachment_bast_" . $key, ATTACHMENTFILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
        if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
            $dt = new DateTime('now', new DateTimezone(TIMESET));
            $date_now = $dt->format('Y-m-d H:i:s');

            $id_fileuniq = md5(uniqids(microtime()));
            $filenameuniq = $uploadResult[1];
            $filesize = $uploadResult[2];
            $filenameori = $uploadResult[3];
            $trkfile_uploader_id = $_POST['idtrack'] . "_attachment_bast_" . $key;

            //check for similar FILE name on db.
            $cFindFile = "SELECT * FROM  `izin_apps_trkfile` AS `trklist` WHERE `trkfile_uploader_id` = ? ";
            $rcFindFile = $dbs->getQuery($cFindFile, "Exec", array(
                "s|$trkfile_uploader_id"
            ));

            //if file exists either replace/not continue with the task.
            if (($rcFindFile instanceof PDOStatement || count($dbs->getAll($rcFindFile)) > 0)) {
                // rename nama uniq dan filesize di setiap baris trkitm yang sama
                if ($FindFile = $dbs->getAssoc($rcFindFile)) {
                    $trkFile_id = $FindFile["id_trkfile"];
                    $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                    //hapus file yang ada dari folder unggah
                    try {
                        $exfilename = $uploadDirectory . "/" . $exfilenameuniq;
                        //if(delete_file($filename) === true) {
                        //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                        if (file_exists($exfilename)) {
                            $deleted = unlink($exfilename);
                        }
                    } catch (Exception $e) {
                        $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                    }

                    //update for file details on file db.
                    $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, `trkfile_size` = ?, `trkfile_name_ori` = ? WHERE `id_trkfile` = ? ";
                    $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", array(
                        "s|$filenameuniq",
                        "i|$filesize",
                        "s|$filenameori",
                        "s|$trkFile_id"
                    ));
                } else {
                    //insert for file details on file db.
                    $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` (id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name) ";
                    $cUpTrkFile .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?) ";

                    $rcUpTrkFile = $dbs->getQuery($cUpTrkFile, "Exec", array(
                        "s|$id_fileuniq",
                        "s|$filenameuniq",
                        "i|$filesize",
                        "s|$filenameori",
                        "d|$date_now",
                        "i|0",
                        "s|$trkfile_uploader_id",
                        "s|" . $_SESSION["lgnNama"]
                    ));
                }
            }
        } else {
            //upload error
            $_SESSION["AlertMess"] = $uploadResult[0];
        }
    }

    require_once "Modules/Tracking/file/TrackDetail_i.php";
    $_SESSION["TrackingPage"] = "Track_pg";
    return;
}

if (isset($_POST['delete_track'])) {
    $querygetid = "select id_trklist from izin_apps_trklist where tracking_id='" . $_POST['id_list'] . "'";
    $tmp = $dbs->getQuery($querygetid);
    $tmp = $dbs->getAssoc($tmp);

    // delete query
    $qeuryDelete = "delete from izin_apps_trklist where id_trklist='" . $tmp['id_trklist'] . "'";
    $tmpDelete = $dbs->getQuery($qeuryDelete);

    require "Modules/Tracking/file/TrackList_i.php";
}
if (isset($_POST['task'])) {
    $task = Purefy($_POST['task']);

    /******************************************
     * Tracking List
     ******************************************/
    if ($task == "Search_" . $_SESSION["butts_Src"]) {
        //FILTER BY NAME
        if (isset($_POST['filter_name'])) {
            $_SESSION["TrackingSearch"] = Purefy($_POST['filter_name']);
        }
        require "Modules/Tracking/file/TrackList_i.php";
    }
    if ($task == "Search_" . $_SESSION["butts_Reset"]) {
        //RESET SEARCH\
        unset($_SESSION["TrackingSearch"]);
        require "Modules/Tracking/file/TrackList_i.php";
    }

    if ($task == $_SESSION["buttkey"] . "prev") {
        //PAGING
        require "Modules/Tracking/file/TrackList_i.php";
    }

    /******************************************
     * End of Tracking List
     ******************************************/

    /******************************************
     * Tracking
     ******************************************/

    //SHOW TRACKING LINE DETAIL ACTION
    if ($task == "Track_" . $_SESSION["butts_Enter"] . "_Detail") {
        //echo "sampe sini";
        //exit;

        require "Modules/Tracking/file/TrackDetail_i.php";
        $_SESSION["TrackingPage"] = "Track_pg";
    }

    if ($task == "CreatePasswd_" . $_SESSION["butts_Save"]) {

        $Email_Address = Purefy($_POST["client_email"]);
        $response = create_user($Email_Address);

        if (!in_array($response, [200, 201])) {
            $_SESSION["AlertMess"] = "Error!";
        } else {
            $_SESSION["AlertMess"] = "Success! Email has been sent to $Email_Address";
        }

        require "Modules/Tracking/file/TrackDetail_i.php";
        $_SESSION["TrackingPage"] = "Track_pg";
    }

    if ($task == "UpdatePasswd_" . $_SESSION["butts_Save"]) {

        $Email_Address = Purefy($_POST["client_email"]);
        $response = reset_password($Email_Address);

        if (!in_array($response, [200, 201])) {
            $_SESSION["AlertMess"] = "Error!";
        } else {
            $_SESSION["AlertMess"] = "Success! Email has been sent to $Email_Address";
        }

        require "Modules/Tracking/file/TrackDetail_i.php";
        $_SESSION["TrackingPage"] = "Track_pg";
    }

    // SHOW TIMELINE
    //-------------------------------------------------
    if ($task == "ShowTimeline_" . $_SESSION["butts_Enter"]) {

        $_SESSION["TrackingPage"] = "Timeline_pg";

        //BREADCRUMB
        $cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'System' AND `page` = 'Dashboard'";
        $rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
        $BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
        $bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
        $bchref = BASEURL . $bcsplitKeys . "/0/0.html";

        //BREADCRUMB TRACKING
        $linkTarget = "DtTrac_pg";
        $cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'Tracking' AND `page` = '" . $linkTarget . "'";
        $rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
        $BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
        $bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
        $bcDtTrack = BASEURL . $bcsplitKeys . "/0/0.html";

        //BREADCRUMB TRACKING
        $linkTarget = "Track_pg";
        $cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'Tracking' AND `page` = '" . $linkTarget . "'";
        $rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
        $BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
        $bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
        $bcTrack = BASEURL . $bcsplitKeys . "/0/0.html";


        //VIEW TRACKING
        if (isset($_SESSION["showtrackingid"])) {
            $tracking_id = Purefy($_SESSION["showtrackingid"]);
        } else {
            //error or direct access !!!!
            $_SESSION["AlertMess"] = $lang_80457;
        }

        if (!isset($_SESSION["AlertMess"])) {
            // CLIENT DETAIL
            $cViewClient .= "SELECT `trklist`.`company_name`, `lead`.`client_id`, `lead`.`client_name`, `trklist`.`tracking_id`, `lead`.`package_name`, `lead`.`package_id` FROM `izin_apps_lead` AS `lead` ";
            $cViewClient .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `lead`.`id_lead`=`trklist`.`lead_id` ";
            $cViewTrack .= "`item`.`trkfile_name_uniq`, `item`.`stkholder_name`, `item`.`kode_mstitem` FROM `izin_apps_trklist` AS `track` ";
            $cViewClient .= "WHERE `trklist`.`tracking_id` = ? ";
            $rcViewClient = $dbs->getQuery($cViewClient, "Exec", ["s|$tracking_id"]);
            $ViewClient = $dbs->getAssoc($rcViewClient);

            //TIMELINE PER ITEM
            $cLoadTrkItem = "SELECT * FROM `izin_apps_trklist` AS `track` ";
            $cLoadTrkItem .= "INNER JOIN ( ";
            $cLoadTrkItem .= "SELECT `trkitm`.`trklist_id`, `trkitm`.`tgl_update_itm`, `trkitm`.`trkitm_status`, `trkitm`.`works_id`, ";
            $cLoadTrkItem .= "`works`.`urutan`, `works`.`nama`, `works`.`kode`, `works`.`wkt` FROM `izin_apps_trkitm` AS `trkitm` ";
            $cLoadTrkItem .= "INNER JOIN `izin_apps_works` AS `works` ON `works`.`id`=`trkitm`.`works_id` ";
            $cLoadTrkItem .= "INNER JOIN (SELECT `trklist_id`, `works_id`, MAX(`trkitm_status`) AS `maxitmstat` FROM `izin_apps_trkitm` GROUP BY `trklist_id`, `works_id`) AS `maxi` ";
            $cLoadTrkItem .= "ON `maxi`.`trklist_id` = `trkitm`.`trklist_id` AND `maxi`.`works_id` = `trkitm`.`works_id` AND `maxi`.`maxitmstat` = `trkitm`.`trkitm_status` ";
            $cLoadTrkItem .= ") AS `trit` ON `trit`.`trklist_id` = `track`.`id_trklist` ";
            $cLoadTrkItem .= "WHERE `track`.`tracking_id` = ? ";
            $cLoadTrkItem .= "ORDER BY `trit`.`urutan` ASC ";
            $rcLoadTrkItem = $dbs->getQuery($cLoadTrkItem, "Exec", ["s|$tracking_id"]);
        }
    }

    //UPDATE TRACKING BUTTON ACTION
    if ($task == "UpdateTracking_" . $_SESSION["butts_Update"]) {
        $tracking_id = Purefy($_POST['tracking_id']);
        //$upstatus = Purefy($_POST['upstatus']);
        $reason_title = Purefy($_POST['reason_title']);
        $work_id = Purefy($_POST['mstitem']);
        //$stkholder_id = Purefy($_POST['stkholder']);
        $badge_id = Purefy($_POST['trkbadge']);

        //$date_now = new DateTime('now', new DateTimezone(TIMESET))->format('Y-m-d H:i:s');
        //$dt = new DateTime('now', new DateTimezone(TIMESET));
        //$date_now = $dt->format('Y-m-d H:i:s');

        // Syarat Minimum untuk UPdate
        //if($reason_title != "all" && $work_id != "all" && $badge_id != "all") {
        if ($work_id != "all" && $badge_id != "all") {
            //cari badge dengan item yang sama untuk tanggal update terbaru
            // kalau belum ada di db otomatis 0/new
            // kalau item yang sama ada dengan badge 0/new maka badge jadi 1/ongoing otomatis
            // kalau 3/done yang sudah ada dengan item yang sama juga 3/done tidak dapat di timpa kecuali priv 2
            // selain itu save saja badge nya mengikuti yang lama (bukan new/0) / sesuai dengan pilihan dropdown

            //ambil id tracking di tabel tracking
            $cFindTrkListID = "SELECT * FROM `izin_apps_trklist` WHERE `tracking_id` = '" . $tracking_id . "'";
            $rcFindTrkListID = $dbs->getQuery($cFindTrkListID);
            $FindTrkListID = $dbs->getAssoc($rcFindTrkListID);
            $trkList_id = $FindTrkListID["id_trklist"];

            //cari badge terakhir dari item yang sama dan update hasilnya
            $cFindItemBadge = "SELECT `tgl_start_itm`, `trkitm_status` FROM `izin_apps_trkitm` WHERE `trklist_id` = '" . $trkList_id . "' AND `works_id` = '" . $work_id . "' ";
            $cFindItemBadge .= "AND `tgl_update_itm` IN (SELECT MAX(`tgl_update_itm`) FROM `izin_apps_trkitm` WHERE `trklist_id` = '" . $trkList_id . "' AND `works_id` = '" . $work_id . "' GROUP BY `trklist_id`)";
            $rcFindItemBadge = $dbs->getQuery($cFindItemBadge);
            $FindItemBadge = $dbs->getAssoc($rcFindItemBadge);

            //if((isset($FindItemBadge["trkitm_status"]) && !empty($FindItemBadge["trkitm_status"])) || $FindItemBadge["trkitm_status"]) == "0") {
            if (isset($FindItemBadge["trkitm_status"]) && intval($FindItemBadge["trkitm_status"]) >= 0) {
                //sudah ada itemnya....
                if ($badge_id == "3") {
                    // Cek Apa ada file uploadnya.
                    $cFindItemFilesUploaded = "SELECT `trkitm_status`, `trkfile_id` FROM `izin_apps_trkitm` WHERE `trklist_id` = '" . $trkList_id . "' AND `works_id` = '" . $work_id . "' ";
                    $cFindItemFilesUploaded .= "AND `tgl_update_itm` IN (SELECT MAX(`tgl_update_itm`) FROM `izin_apps_trkitm` WHERE `trklist_id` = '" . $trkList_id . "' AND `works_id` = '" . $work_id . "' GROUP BY `trklist_id`)";
                    $rcFindItemFilesUploaded = $dbs->getQuery($cFindItemFilesUploaded);
                    $FindItemFilesUploaded = $dbs->getAssoc($rcFindItemFilesUploaded);

                    // CEK SAMPAI ISFILE true DI SINI

                    //echo "value in db1: ".$FindItemBadge["trkitm_status"];
                    //exit(0);
                    $close_trackitem = false;
                    if (intval($FindItemFilesUploaded["trkitm_status"]) < intval($badge_id) && (!empty($FindItemFilesUploaded["trkfile_id"]))) {
                        // boleh lanjut save update tapi harus ada file di db nya/ file uploaded previously
                        //$set_badge_status = ", '".$badge_id."', '".$tglupdate_itm."' ";
                        //$set_badge_status = ", ".intval($badge_id)." , NOW() ";
                        $dt = new DateTime('now', new DateTimezone(TIMESET));
                        $date_now = $dt->format('Y-m-d H:i:s');

                        $set_badge_status = ", " . intval($badge_id) . " , '" . $date_now . "' ";
                        $field_name = ", trkitm_status, tgl_close_itm ";
                        $close_trackitem = true;
                    } else if (intval($FindItemFilesUploaded["trkitm_status"]) < intval($badge_id)  && !empty($_FILES['izindocsup']['name'])) {
                        // badge lebih kecil tapi ada file  di attachment nya boleh lanjut
                        //$set_badge_status = ", ".intval($badge_id).", NOW() ";
                        $dt = new DateTime('now', new DateTimezone(TIMESET));
                        $date_now = $dt->format('Y-m-d H:i:s');

                        $set_badge_status = ", " . intval($badge_id) . ", '" . $date_now . "' ";
                        $field_name = ", trkitm_status, tgl_close_itm ";
                        $close_trackitem = true;
                    } else if (intval($FindItemFilesUploaded["trkitm_status"]) < intval($badge_id) && (empty($FindItemFilesUploaded["trkfile_id"]))) {
                        // tidak boleh lanjut save update karena belum ada file di db nya/ file has not uploaded previously
                        $_SESSION["AlertMess"] = "No file/attachment has been uploaded, update canceled.";
                    } else if (intval($FindItemFilesUploaded["trkitm_status"]) == intval($badge_id)  && !empty($_FILES['izindocsup']['name'])) {
                        // badge sama (3) tapi ada file  di attachment nya boleh lanjut (buat update gambar)
                        //if(isset($_FILES['izindocsup']) && !empty($_FILES['izindocsup']['name'])) {
                        //$set_badge_status = ", '".$badge_id."', '".$tglupdate_itm."' ";
                        //$set_badge_status = ", ".intval($badge_id).", NOW() ";'".$date_now."'
                        $dt = new DateTime('now', new DateTimezone(TIMESET));
                        $date_now = $dt->format('Y-m-d H:i:s');

                        $set_badge_status = ", " . intval($badge_id) . ", '" . $date_now . "' ";
                        $field_name = ", trkitm_status, tgl_close_itm ";
                    } else if (intval($FindItemFilesUploaded["trkitm_status"]) == intval($badge_id)  && empty($_FILES['izindocsup']['name'])) {
                        // badge sama (3) tapi tidak ada file attachment nya tidak boleh lanjut
                        $_SESSION["AlertMess"] = "Work status already Done, update canceled.";
                    } else {
                        $_SESSION["AlertMess"] = "How on earth did we get here, update canceled.";
                    }
                    //if( intval($FindItemBadge["trkitm_status"]) == 0) {
                } else {
                    //if($badge_id < "3") save saja
                    //$set_badge_status = ", '".$badge_id."' ";
                    $set_badge_status = ", " . intval($badge_id) . " ";
                    $field_name = ", trkitm_status ";
                }
            } else {
                //lom ada itemnya.... item baru status 0 = new
                $itemstatusnow = 0;
                //$itemstatusnow = "0";
                //$set_badge_status = ", '".$itemstatusnow."' ";
                $set_badge_status = ", " . $itemstatusnow . " ";
                $field_name = ", trkitm_status ";
            }
        } else {
            if ($work_id == "all") {
                $_SESSION["AlertMess"] = "Please choose type of work";
                //            } elseif ($reason_title  == "all") {
                //              $_SESSION["AlertMess"] = "Please choose reason title";
            } elseif ($badge_id == "all") {
                $_SESSION["AlertMess"] = "Please choose work status";
            }
        }

        // Bila tidak ada kesalahan awal untuk isian badge, lanjut isi line untuk update baik fields juga values nya
        if (!isset($_SESSION["AlertMess"])) {
            $setphrase = "'" . md5(uniqids(microtime())) . "', '" . $trkList_id . "', '" . $work_id . "'";
            //if(isset($_POST['stkholder']) && $_POST['stkholder']!="all"){
            //$stkholder_id = Purefy($_POST['stkholder']);
            //} else {
            // izin office id
            //$stkholder_id = "88fc001b69111479a709898a95526312";
            $stkholder_id = "";
            //}
            $setphrase .= ", '" . $stkholder_id . "'";

            //cari tanggal start dari db kalau tidak ada masukkan tanggal sekarang
            $cFindItemStart = "SELECT `tgl_start_itm` FROM `izin_apps_trkitm` WHERE `trklist_id` = '" . $trkList_id . "' AND `works_id` = '" . $work_id . "' ";
            $cFindItemStart .= "AND `tgl_update_itm` IN (SELECT MAX(`tgl_update_itm`) FROM `izin_apps_trkitm` GROUP BY `trklist_id`)";
            $rcFindItemStart = $dbs->getQuery($cFindItemStart);
            $FindItemStart = $dbs->getAssoc($rcFindItemStart);
            $itemstartdate = $FindItemStart["tgl_start_itm"];
            //$itemstatus = $FindItemStart["trkitm_status"];

            if (isset($FindItemStart["tgl_start_itm"]) && !empty($FindItemStart["tgl_start_itm"])) {
                // jika item yang sama sudah ada copy tanggal start
                $itemstartdate = "'" . $FindItemStart["tgl_start_itm"] . "'";
            } else {
                // kalau item tidak ada yang sama masukkan tanggal sekarang.....
                //$itemstartdate = "NOW()";
                $dt = new DateTime('now', new DateTimezone(TIMESET));
                $date_now = $dt->format('Y-m-d H:i:s');

                $itemstartdate = "'" . $date_now . "'";
            }

            //$tglupdate_itm = date("Y-m-d H:i:s");
            //$tglupdate_itm = new DateTime('now', new DateTimezone(TIMESET))->format('Y-m-d H:i:s');
            //$dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
            //$tglupdate_itm = $dt->format('F j, Y, g:i a');

            $dt = new DateTime('now', new DateTimezone(TIMESET));
            $date_now = $dt->format('Y-m-d H:i:s');

            $tglupdate_itm = "'" . $date_now . "'";
            //$tglupdate_itm = "NOW()";
            $setphrase .= ", " . $itemstartdate;
            $setphrase .= ", '" . $_SESSION["tblID"] . "'";
            $setphrase .= ", '" . $_SESSION["lgnNama"] . "'";
            //$setphrase .= ", '".$tglupdate_itm."'";
            $setphrase .= ", " . $tglupdate_itm;
            if (isset($_POST['reason_title']) && !empty($_POST['reason_title']) && $_POST['reason_title'] != "All") {
                $update_status = REASON_TITLE[$reason_title];
            } else {
                //$update_status = "No reason title";
                $update_status = "";
            }
            $setphrase .= ", '" . $update_status . "'";
            if (isset($_POST['reason_note']) && !empty($_POST['reason_note'])) {
                $reason_note = Purefy($_POST['reason_note']);
            } else {
                $reason_note = "";
            }
            $setphrase .= ", '" . $reason_note . "'";
            $ins_fields = "id_trkitm, trklist_id, works_id, stakeholder_id, ";
            $ins_fields .= "tgl_start_itm, updater_id, updater_name, tgl_update_itm, reason_title, reason_note";
            $setphrase .= $set_badge_status;
            $ins_fields .= $field_name;

            if (isset($_FILES['izindocsup']) && !empty($_FILES['izindocsup']['name'])) {
                $uploadDirectory = "../../" . UPDIR . "/Tracking_Files";
                //$maxFileSize = 5000000; //5MB
                //$picWidth = 0; //200 dalam px
                //$picHeight = 0; //200 in px
                //$pm = new PicMan($upfiletype,$upimtype);
                //$uploadResult = $pm->File("izindocsup",FILESIZES,"../../".UPDIR."/Tracking_Files")->FileUpload()->Result();
                //$uploadResult = $pm->File("izindocsup",$maxFileSize,$uploadDirectory)->FileUpload()->Result();

                //$uploadResult = $pm->File("izindocsup",$maxFileSize,$uploadDirectory)->Images($picWidth,$picHeight,"ori")->Result();
                //if($uploadResult[0] == "FL_IM_SUCCESS" && $uploadResult[1] != '') {
                //if($uploadResult[0] == "FL_UPLOAD_SUCCESS" && $uploadResult[1] != '') {
                $AllowedMime = "";
                $uploadResult = $pm->File("izindocsup", FILESIZES, $uploadDirectory)->FileUpload($AllowedMime)->Result();
                if ($uploadResult[0] == "FL_UPLOAD_SUCCESS" and !empty($uploadResult[1])) {
                    $id_fileuniq = md5(uniqids(microtime()));
                    $setfilephrase .= "'" . $id_fileuniq . "'";
                    $setfilephrase .= ", '" . $uploadResult[1] . "'";
                    $setfilephrase .= ", '" . $uploadResult[2] . "'";
                    $setfilephrase .= ", '" . $uploadResult[3] . "'";
                    $setfilephrase .= ", '" . $date_now . "'";
                    //$setfilephrase .= ", NOW()";
                    $setfilephrase .= ", 0";
                    $setfilephrase .= ", '" . $_SESSION["tblID"] . "'";
                    $setfilephrase .= ", '" . $_SESSION["lgnNama"] . "'";
                    $insfile_fields .= "id_trkfile, trkfile_name_uniq, trkfile_size, trkfile_name_ori, trkfile_upload_date, trkfile_downloaded, trkfile_uploader_id, trkfile_uploader_name ";
                    $filenameuniq = $uploadResult[1];
                    $filesize = $uploadResult[2];
                    $filenameori = $uploadResult[3];
                } else {
                    //upload error
                    $_SESSION["AlertMess"] = $uploadResult[0];
                }

                //check for similar FILE name on db.
                $cFindFile = "SELECT * FROM  `izin_apps_trklist` AS `trklist` ";
                $cFindFile .= "INNER JOIN ( ";
                $cFindFile .= "SELECT * FROM `izin_apps_trkitm` AS `trkitm` ";
                $cFindFile .= "INNER JOIN `izin_apps_trkfile` AS `trkfile` ON `trkitm`.`trkfile_id`=`trkfile`.`id_trkfile` ";
                $cFindFile .= "WHERE `trkfile`.`trkfile_name_ori` = '" . $filenameori . "'";
                $cFindFile .= ") AS `itmfile` ON `trklist`.`id_trklist`=`itmfile`.`trklist_id` ";
                $cFindFile .= "WHERE `tracking_id` = '" . $tracking_id . "'";
                //echo $cFindFile;
                //exit(0);
                $rcFindFile = $dbs->getQuery($cFindFile);

                //if file exists either replace/not continue with the task.
                if (($rcFindFile instanceof PDOStatement || count($rcFindFile) > 0) && (!isset($_SESSION["AlertMess"]) || empty($_SESSION["AlertMess"]))) {
                    // rename nama uniq dan filesize di setiap baris trkitm yang sama
                    while ($FindFile = $dbs->getAssoc($rcFindFile)) {
                        $trkFile_id = $FindFile["id_trkfile"];
                        $exfilenameuniq = $FindFile["trkfile_name_uniq"];
                        //hapus file yang ada dari folder unggah
                        try {
                            //$exfilename = "../".UPDIR."/".$exfilenameuniq;
                            $exfilename = "../../" . UPDIR . "/Tracking_Files/" . $exfilenameuniq;
                            //if(delete_file($filename) === true) {
                            //$_SESSION["AlertMess"] = $lang_50125;        echo 'File Deleted';
                            if (file_exists($exfilename)) {
                                $deleted = unlink($exfilename);
                            }
                        } catch (Exception $e) {
                            $_SESSION["AlertMess"] = $e->getMessage(); // will print Exception message defined above.
                        }

                        //update for file details on file db.
                        $cUpdTrkFile = "UPDATE `izin_apps_trkfile` SET `trkfile_name_uniq` = ?, trkfile_size= ? ";
                        $cUpdTrkFile .= "WHERE `id_trkfile` = ? ";
                        $rcUpdTrkFile = $dbs->getQuery($cUpdTrkFile, "Exec", [
                            "s|$filenameuniq",
                            "i|$filesize",
                            "s|$trkFile_id"
                        ]);
                    }
                    //save yang baru
                } else {
                    //FILE WITH SIMILAR NAME NOT EXISTS save yang baru
                }


                if (!isset($_SESSION["AlertMess"]) || empty($_SESSION["AlertMess"])) {
                    //insert for file details on file db.
                    $cUpTrkFile = "INSERT INTO `izin_apps_trkfile` ";
                    $cUpTrkFile .= "(" . $insfile_fields . ") ";
                    $cUpTrkFile .= "VALUES (" . $setfilephrase . ") ";
                    $rcUpTrkFile = $dbs->getQuery($cUpTrkFile);

                    //include file id for new file ini trkitem
                    $setphrase .= ", '" . $id_fileuniq . "'";
                    $ins_fields .= ", trkfile_id";
                }
            }

            if (!isset($_SESSION["AlertMess"]) || empty($_SESSION["AlertMess"])) {
                //IF ok then INSERT
                //inserting detail on tracking item db.
                $cUpTrkList = "INSERT INTO `izin_apps_trkitm` ";
                $cUpTrkList .= "(" . $ins_fields . ") ";
                $cUpTrkList .= "VALUES (" . $setphrase . ") ";
                $rcUpTrkList = $dbs->getQuery($cUpTrkList);
                $_SESSION["AlertMess"] = $lang_50136; //Tracking updated

                // dapatkan nomor urutan untuk work yang sekarang
                $cGetUrutan = "SELECT `pkgworks`.`id_work`, `pkgworks`.`kode`,`pkgworks`.`urutan`,`pkgworks`.`id_lead` FROM `izin_apps_packagework` AS `pkgworks` ";
                $cGetUrutan .= "WHERE `pkgworks`.`id_lead` = (";
                $cGetUrutan .= "SELECT `lead`.`id_lead` FROM `izin_apps_trklist` AS `track` ";
                $cGetUrutan .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
                $cGetUrutan .= "WHERE `track`.`tracking_id` = '" . $tracking_id . "') ";
                $cGetUrutan .= "AND `pkgworks`.`id_work` = '" . $work_id . "' ";
                $rcGetUrutan = $dbs->getQuery($cGetUrutan);
                $GetUrutan = $dbs->getAssoc($rcGetUrutan);
                $NoUrutan = $GetUrutan["urutan"];
                $KodeWorks = $GetUrutan["kode"]; // for WA Blast
                $NomorLead = $GetUrutan["id_lead"];

                //IF BADGE DONE GET THE NEXT SEQUENCE ITEM
                if ($badge_id == 3) {
                    $Continue_getNext = false;
                    //DETECTOR UNTUK CUSTOM PAKET
                    // SAVE URUTAN BERIKUTNYA DARI ITEM DAN BERI BADGE ONGOING
                    //------------------------------------------------------------------------------

                    // query works untuk mengetahui count urutan ada berapa item
                    //$cGetCountUrutan = "SELECT COUNT(`id`) AS Urutan_Counter FROM `izin_apps_works` WHERE `urutan` = ".$NoUrutan." ";
                    $cGetCountUrutan = "SELECT COUNT(`id`) AS Urutan_Counter FROM `izin_apps_packagework` AS `pkgworks` WHERE `pkgworks`.`id_lead` = '" . $NomorLead . "' AND `urutan` = " . $NoUrutan . " ";
                    $rcGetCountUrutan = $dbs->getQuery($cGetCountUrutan);
                    $GetCountUrutan = $dbs->getAssoc($rcGetCountUrutan);

                    // kalau urut$cGetUrutanan yang sekarang hanya ada 1, lanjut cari urutan berikutnya
                    if (intval($GetCountUrutan["Urutan_Counter"]) == 1) {
                        // lanjut cari urutan berikutnya
                        $Continue_getNext = true;
                    } else {
                        //kalau urutan lebih dari satu, yg sekarang done, yang satu lagi di cek
                        //tunggu sampai done semua
                        //query trkitm innerjoin works ambil urutan dan status trkitm
                        /*
                        $cCountItemDone = "SELECT COUNT(`trkitm`.`works_id`) AS `Item_Done` FROM `izin_apps_trkitm` AS `trkitm` ";
                        $cCountItemDone .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
                        $cCountItemDone .= "WHERE `trkitm`.`trklist_id` = '".$trkList_id."' ";
                        $cCountItemDone .= "AND `works`.`urutan` = ".$NoUrutan." ";
                        $cCountItemDone .= "AND `trkitm`.`trkitm_status` = 3 ";
                        $rcCountItemDone = $dbs->getQuery($cCountItemDone);
                        $CountItemDone = $dbs->getAssoc($rcCountItemDone);
                        */
                        // yang lama error yang kembali dari finished permit atau yang di update 2 kali item nya maka akan lebih dari dua....
                        //
                        $cCekTwinItemStatus = "SELECT `trkitm`.`works_id`, MAX(`trkitm`.`trkitm_status`) AS `max_status` FROM `izin_apps_trkitm` AS `trkitm` ";
                        $cCekTwinItemStatus .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
                        $cCekTwinItemStatus .= "WHERE `trkitm`.`trklist_id` = '" . $trkList_id . "' ";
                        $cCekTwinItemStatus .= "AND `works`.`urutan` = " . $NoUrutan . " ";
                        $cCekTwinItemStatus .= "AND `trkitm`.`works_id` <> '" . $work_id . "' ";
                        $cCekTwinItemStatus .= "GROUP BY `trkitm`.`works_id` ";
                        $rcCekTwinItemStatus = $dbs->getQuery($cCekTwinItemStatus);

                        /*
                        //CARA 1 HANYA BISA 2 ITEM DENGAN URUTAN YANG SAMA
                        $CekTwinItemStatus = $dbs->getAssoc($rcCekTwinItemStatus);
                        // cek status urutan sama yang lainnya (ingat hanya 2 yang sama tidak lebih)
                        //if (intval($CountItemDone["Item_Done"]) == 2) {
                        if (intval($CekTwinItemStatus["max_status"]) == 3) {
                        //if (intval($CekTwinItemStatus["trkitm_status"]) == 3) {
                            // bila Done dua2 nya, lanjut cari urutan BERIKUTNYA
                            $Continue_getNext = true;
                        }
                        // bila belum Done salah satunya
                        // do nothing
                        */

                        //CARA 2 BANYAK ITEM DENGAN URUTAN YANG SAMA
                        while ($CekTwinItemStatus = $dbs->getAssoc($rcCekTwinItemStatus)) {
                            if (intval($CekTwinItemStatus["max_status"]) == 3) {
                                // bila Done semua nya, lanjut cari urutan BERIKUTNYA
                                $Continue_getNext = true;
                            } else {
                                // bila salah satu ada yang belum done skip proses BERIKUTNYA
                                if ($Continue_getNext == true) {
                                    $Continue_getNext = false;
                                }
                            }
                        }
                    } //if (count($rcGetUrutan) == 1 ) {

                    //echo "<br/>";
                    //echo "continue get next: ";
                    //var_dump($Continue_getNext);
                    //print_r($Continue_getNext);
                    //echo $Continue_getNext;
                    //echo "<br/>";
                    //exit;
                    //kalau $Continue_getNext = true lanjut proses
                    if ($Continue_getNext) {
                        // query untuk mendapatkan urutan berikutnya berdasarkan nomor urutan di order ascending
                        //echo "lewat sini 1";
                        //exit;

                        // hitung jumlah item tersisa pada tracking id yang sama
                        //kalau ada hasil, lanjutkan proses
                        $cGetItemsCount = "SELECT COUNT(`pkgworks`.`id_work`) AS `Count_Item_Left` FROM `izin_apps_packagework` AS `pkgworks` ";
                        $cGetItemsCount .= "WHERE `pkgworks`.`id_lead` = (";
                        $cGetItemsCount .= "SELECT `lead`.`id_lead` FROM `izin_apps_trklist` AS `track` ";
                        $cGetItemsCount .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
                        $cGetItemsCount .= "WHERE `track`.`tracking_id` = '" . $tracking_id . "') ";
                        $cGetItemsCount .= "AND `pkgworks`.`urutan` > " . $NoUrutan . " ";
                        $cGetItemsCount .= "AND `pkgworks`.`kode` <> 'BNRI' "; //skip BNRI
                        //$cGetItemsCount .= "ORDER BY `pkgworks`.`urutan` ASC";
                        $rcGetItemsCount = $dbs->getQuery($cGetItemsCount);
                        $GetItemsCount = $dbs->getAssoc($rcGetItemsCount);
                        $NextItemCount = intval($GetItemsCount["Count_Item_Left"]);

                        // cek kalau semua item berikutnya sudah done apa belum (untuk yang kembali dari finished permit)
                        // kalau belum done, lanjutkan proses
                        $Continue_process = false;

                        if ($NextItemCount > 0) {
                            //CARA 2
                            // Hitung jumlah item di package work
                            $cCountPW_Items = "SELECT COUNT(`pkgworks`.`id_work`) AS `Count_Item_Left` FROM `izin_apps_packagework` AS `pkgworks` ";
                            $cCountPW_Items .= "WHERE `pkgworks`.`id_lead` = (";
                            $cCountPW_Items .= "SELECT `lead`.`id_lead` FROM `izin_apps_trklist` AS `track` ";
                            $cCountPW_Items .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
                            $cCountPW_Items .= "WHERE `track`.`tracking_id` = '" . $tracking_id . "') ";
                            //$cGetPW_Items .= "AND `pkgworks`.`urutan` > ".$NoUrutan." ";
                            $cCountPW_Items .= "AND `pkgworks`.`kode` <> 'BNRI' "; //skip BNRI
                            //$cGetItemsCount .= "ORDER BY `pkgworks`.`urutan` ASC";
                            $rcCountPW_Items = $dbs->getQuery($cCountPW_Items);
                            $CountPW_Items = $dbs->getAssoc($rcCountPW_Items);
                            $CountPWItems = intval($CountPW_Items["Count_Item_Left"]);

                            // Hitung jumlah item di trkitem
                            $cCountT_Items = "SELECT COUNT(`trkitm`.`id_trkitm`) AS `Count_Item_Done` FROM `izin_apps_trkitm` AS `trkitm` ";
                            $cCountT_Items .= "WHERE `trkitm`.`trklist_id` = '" . $trkList_id . "' ";
                            $cCountT_Items .= "AND `trkitm`.`trkitm_status` = 3 ";
                            $cCountT_Items .= "GROUP BY `trkitm`.`works_id` ";
                            $rcCountT_Items = $dbs->getQuery($cCountT_Items);
                            //$CountT_Items = $dbs->getAssoc($rcCountT_Items);
                            //$CountTItems = intval($CountT_Items["Count_Item_Done"]);
                            $CountTItems = 0;
                            while ($CountT_Items = $dbs->getAssoc($rcCountT_Items)) {
                                $CountTItems++;
                            }
                            //kalau sama berarti done semua
                            //kalau tidak sama lanjutkan proses penambahan
                            if ($CountPWItems != $CountTItems) {
                                $Continue_process = true;
                            }
                        }

                        //echo "next item: ".$NextItemCount;
                        //echo "<br/>";
                        //echo "continue: ";
                        //print_r($Continue_process);
                        //var_dump($Continue_process);
                        //exit;


                        //if($NextItemCount>0 && $Continue_process == true) {
                        if ($Continue_process == true) {
                            //if($NextItemCount>0) {

                            //echo "lewat sini save <br/>";

                            // loop item tersedia, masukkan dalam array
                            $item_array = array();
                            $urutan_array = array();
                            $cGetItems = "SELECT `pkgworks`.`id_work`, `pkgworks`.`kode`,`pkgworks`.`urutan` FROM `izin_apps_packagework` AS `pkgworks` ";
                            $cGetItems .= "WHERE `pkgworks`.`id_lead` = (";
                            $cGetItems .= "SELECT `lead`.`id_lead` FROM `izin_apps_trklist` AS `track` ";
                            $cGetItems .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
                            $cGetItems .= "WHERE `track`.`tracking_id` = '" . $tracking_id . "') ";
                            $cGetItems .= "AND `pkgworks`.`urutan` > " . $NoUrutan . " ";
                            //$cGetItems .= "AND `pkgworks`.`kode` <> 'BNRI' "; //skip BNRI
                            $cGetItems .= "ORDER BY `pkgworks`.`urutan` ASC";
                            $rcGetItems = $dbs->getQuery($cGetItems);
                            while ($GetItems = $dbs->getAssoc($rcGetItems)) {
                                $item_array[] = $GetItems['id_work'];
                                $urutan_array[] = $GetItems['urutan'];
                            }

                            // ambil urutan pertama dari array hapus urutan ($urutan_array[0]) dari array
                            $next_item_array = array();

                            /*
                            //CARA 1 HANYA DUA ITEM MAKSIKUM
                            //$next_urutan_array = array();
                            $next_item_array[] = array_shift($item_array);
                            $next_urutan_array[] = array_shift($urutan_array);
                            // bandingkan dengan urutan berikutnya kalau sama berarti ada 2 yg SAMA
                            // seandainya ada dua (maksimal dua)
                            if($next_urutan_array[0] == $urutan_array[0]) {
                                //ambil urutan yang sama
                                $next_item_array[] = array_shift($item_array);
                                //$next_urutan_array[] = array_shift($urutan_array);
                            }
                            */

                            //CARA 2 BISA BANYAK ITEM MAKSIKUM
                            $before_urutan_item = 0;
                            foreach ($urutan_array as $key => $value) {
                                if ($before_urutan_item == 0) {
                                    //isi pertama
                                    $before_urutan_item = $value;
                                    $next_item_array[] = $item_array[$key];
                                } else {
                                    //isi kedua dan seterusnya
                                    if ($before_urutan_item == $value) {
                                        $next_item_array[] = $item_array[$key];
                                    }
                                }
                            }

                            foreach ($next_item_array as $value) {
                                //check if item already on db incase of reload or double submit
                                $cCheckNextExistance = "SELECT count(`id_trkitm`) AS 'NextItmeExists' FROM `izin_apps_trkitm` ";
                                $cCheckNextExistance .= "WHERE `trklist_id` = '" . $trkList_id . "' ";
                                $cCheckNextExistance .= "AND `works_id` = '" . $value . "' ";
                                $rcCheckNextExistance = $dbs->getQuery($cCheckNextExistance);
                                $CheckNextExistance = $dbs->getAssoc($rcCheckNextExistance);
                                if ($CheckNextExistance['NextItmeExists'] == 0) {
                                    // tambah reason title here
                                    $cGetNextKode = "SELECT `kode` FROM `izin_apps_works` ";
                                    $cGetNextKode .= "WHERE `id` = '" . $value . "' ";
                                    $rcGetNextKode = $dbs->getQuery($cGetNextKode);
                                    $GetNextKode = $dbs->getAssoc($rcGetNextKode);

                                    $dt = new DateTime('now + 1 sec', new DateTimezone(TIMESET));
                                    $date_now = $dt->format('Y-m-d H:i:s');

                                    $set_fields = "id_trkitm, trklist_id, works_id, stakeholder_id, tgl_start_itm, updater_id, updater_name, tgl_update_itm, reason_title, trkitm_status";
                                    //$set_values = "'".md5(uniqids(microtime()))."', '".$trkList_id."', '".$value."', '', '".$date_now."', '".$_SESSION["tblID"]."', '".$_SESSION["lgnNama"]."', '".$date_now."', 'Add new ".$GetNextKode['kode']."', 0 " ;
                                    $set_values = "'" . md5(uniqids(microtime())) . "', '" . $trkList_id . "', '" . $value . "', '', '" . $date_now . "', '" . $_SESSION["tblID"] . "', '" . $_SESSION["lgnNama"] . "', '" . $date_now . "', 'On going " . $GetNextKode['kode'] . "', 1 ";
                                    $cNextNewItem = "INSERT INTO `izin_apps_trkitm` (" . $set_fields . ") VALUES (" . $set_values . ") ";
                                    $rcNextNewItem = $dbs->getQuery($cNextNewItem);
                                }
                            }
                        } else {
                            //echo "lewat sini close <br/>";
                            // HITUNG ULANG SEMUA ITEM DENGAN BADGE 3/DONE, BILA SEMUA ITEM DONE CLOSE TRACKING
                            // close tracking if no other item available
                            $status_close = 3;

                            $dt = new DateTime('now', new DateTimezone(TIMESET));
                            $date_now = $dt->format('Y-m-d H:i:s');

                            $cCloseTrkList = "UPDATE `izin_apps_trklist` ";
                            //                            $cCloseTrkList .= "SET `tgl_close` = NOW(), `trk_status` = ".$status_close." ";
                            $cCloseTrkList .= "SET `tgl_close` = '" . $date_now . "', `trk_status` = " . $status_close . " ";
                            $cCloseTrkList .= "WHERE `id_trklist` = '" . $trkList_id . "' ";
                            $rcCloseTrkList = $dbs->getQuery($cCloseTrkList);
                            //status gui
                            $_SESSION["AlertMess"] .= "Tracking id: " . $tracking_id . " Closed!!"; //Tracking updated
                            //status for wablast
                            //$WA_MessageTRK = "Tracking id: ".$tracking_id." Closed!!";//$tracking_id
                        } //if(count($rcGetItems)>0) {
                    } //if ($Continue_getNext) {
                    //echo "disini ";
                    //exit;

                    //kalau false do nothing
                } //if ($badge_id == 3) {



                //--------------------------------------------------------------------------------------------------------
                // BLAST EMAIL n WA BILA BADGE 2/WAITING, WA ke nomor PIC di LEAD
                // BLAST EMAIL n WA BILA BADGE 3/DONE, WA ke nomor PIC di LEAD

                // get email address, phone number, bank_acc
                $cGetLead = "SELECT `lead`.`client_name`, `lead`.`client_email`, `lead`.`client_wa`, `lead`.`package_id`, `trklist`.`company_name`, ";
                $cGetLead .= "(SELECT bank_acc FROM `izin_apps_invdet` as `inv` WHERE `inv`.`id_lead` = `trklist`.`lead_id`) as bank_acc ";
                $cGetLead .= "FROM `izin_apps_trklist` AS `trklist` ";
                $cGetLead .= "INNER JOIN `izin_apps_lead` AS `lead` ON `trklist`.`lead_id` = `lead`.`id_lead` ";
                $cGetLead .= "WHERE `trklist`.`tracking_id` = '" . $tracking_id . "' ";
                $rcGetLead = $dbs->getQuery($cGetLead);
                $GetLead = $dbs->getAssoc($rcGetLead);
                $PhoneNo = $GetLead["client_wa"];
                $Email_Address = $GetLead["client_email"];
                $Email_Name = $GetLead["client_name"];
                $Company_Name = $GetLead["company_name"];
                $PackageID = $GetLead["package_id"];
                $BnkAcc = $GetLead['bank_acc'];
                $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
                $BAD_NmPT = $BnkAccDet[1];

                $sendWA = false;
                $wa_message = "";

                if (intval($badge_id) >= 2) {
                    // untuk menghilangkan proses selanjutnya (apabila client memilih custom akta misalnya) pada template email, kode di SINI
                    if (intval($badge_id) == 2) {
                        $sendEmail = true;
                        require "Modules/Tracking/file/MailNWA_PENDING.php";
                    } else {
                        //if badge == 3 / done
                        switch ($KodeWorks) {
                            case "AKTA - SK":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_Akta.php";
                                break;
                            case "HKIM":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_HKIM.php";
                                break;
                            case "NPWP":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_NPWP.php";
                                break;
                            case "SIUP":
                                if ($PackageID == "CUSTOM") {
                                    $sendEmail = true;
                                    $sendWA = true;
                                    require "Modules/Tracking/file/MailNWA_SIUP_c.php";
                                } else {
                                    $sendEmail = true;
                                    $sendWA = true;
                                    require "Modules/Tracking/file/MailNWA_SIUP.php";
                                }
                                break;
                            case "NIB":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_NIB.php";
                                break;
                            case "API":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_API.php";
                                break;
                            case "KPPA":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_KPPA.php";
                                break;
                            case "KADIN":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_KADIN.php";
                                break;
                            case "APEC":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_APEC.php";
                                break;
                            case "RPTKA&IMTA":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_RPTKA_IMTA.php";
                                break;
                            case "VISA":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_VISA.php";
                                break;
                            case "KITAS":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_KITAS.php";
                                break;
                            case "KITAS_INV":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_KITAS_Investor.php";
                                break;
                            case "STM":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_STM.php";
                                break;
                            case "SKTTS":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_SKTT.php";
                                break;
                            case "SS_OSS":
                                $sendEmail = true;
                                $sendWA = true;
                                require "Modules/Tracking/file/MailNWA_SS_OSS.php";
                                break;
                            case "BNRI":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_BNRI.php";
                                break;
                            case "IPAK":
                            case "IDAK":
                            case "BPOM":
                            case "IKP":
                            case "IAPTK":
                            case "PIRT":
                            case "IFWR":
                            case "I_MBA":
                            case "LPK":
                            case "SHL":
                            case "ILS":
                            case "IRST":
                            case "LPSE":
                            case "TDUP":
                            case "PKRT":
                            case "IUJK":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_IZIN_Khusus.php";
                                break;
                            case "EFIN":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_EFIN.php";
                                break;
                            case "SPTT_T":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_SPTT_T.php";
                                break;
                            case "SPT_M":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_SPT_M.php";
                                break;
                            case "PKP":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_PKP.php";
                                break;
                            case "ACC":
                                $sendEmail = true;
                                require "Modules/Tracking/file/Mail_ACC.php";
                                break;
                            default:
                                $sendEmail = false;
                                $sendWA = false;
                                //code to be executed if n is different from all labels;
                                //incase come from sidebar menu.
                                //show list
                                //$fl = $_SESSION["ROOT_DIR"]."/Modules/".$cmodule."/file/TrackBase_pg.php";
                        } //switch($KodeWorks) {
                    }
                    //blast Email
                    if ($sendEmail) {
                        require "Modules/Tracking/file/Send_Mail.php";
                    }

                    //blast WA
                    if ($sendWA) {
                        $WaBlast_result = WaBlast($PhoneNo, $WA_Message);
                        $wa_message = WA_message($WaBlast_result);
                    }

                    if (!isset($_SESSION["AlertMess"])) {
                        $_SESSION["AlertMess"] = $wa_message;
                    } else {
                        $_SESSION["AlertMess"] .= "<BR/>" . $wa_message;
                    }

                    /*
                    // Tracking close beri pesan email dan wa sekali lagi
                    if(isset($status_close) && $status_close == 3) {


                        //blast Email
                        require "Modules/Tracking/file/MailNWA_CloseT.php";
                        require "Modules/Tracking/file/Send_Mail.php";


                        //blast WA
                        $WA_Message = "Dear Client,

Salam dari IZIN CO ID!

Congratulation! Proses pembuatan Perusahaan anda telah selesai.
Dokumen dapat diunduh melalui link https://tracking.izin.co.id

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62_ _822-9998-0011/cs@izin.co.id_
";
                        //blast WA
                        $WaBlast_result = WaBlast($PhoneNo, $WA_Message);
                        if(!isset($_SESSION["AlertMess"]))
                            $_SESSION["AlertMess"] = $WaBlast_result[1];
                        else
                            $_SESSION["AlertMess"] .= "<BR/>".$WaBlast_result[1];
                    }
*/


                    //user id, user name*, avatar*, nama company, tracking id*, trklist_id, kode work, status traking item, tanggal update item,
                    //save notif
                    $cinsNotif = "INSERT INTO `izin_apps_notification` ";
                    $cinsNotif .= "(id_notif, user_id, user_name, jabatan, avatar, company_name, ";
                    $cinsNotif .= "trklist_id, tracking_id, kode, trkitm_status, tgl_update_itm) ";
                    $cinsNotif .= "VALUES ('" . md5(uniqids(microtime())) . "', '" . $_SESSION["tblID"] . "', '" . $_SESSION["lgnNama"] . "', '" . $_SESSION["Jbtn"] . "', '" . $_SESSION["Avatar"] . "', ";
                    $cinsNotif .= "'" . $Company_Name . "', '" . $trkList_id . "', '" . $tracking_id . "', '" . $KodeWorks . "', '" . $badge_id . "', " . $tglupdate_itm . ") ";
                    $rcinsNotif = $dbs->getQuery($cinsNotif);
                } //if (intval($badge_id) >= 2) {

            } //if(!isset($_SESSION["AlertMess"]) || empty($_SESSION["AlertMess"])) {
        } //if (!isset($_SESSION["AlertMess"])) {

        require "Modules/Tracking/file/TrackDetail_i.php";
        $_SESSION["TrackingPage"] = "Track_pg";
    } //if($task == "UpdateTracking_".$_SESSION["butts_Update"]) {

    if ($task == "SendEmailWA_" . $_SESSION["butts_Enter"]) {
        //$tracking_id = Purefy($_POST['tracking_id']);
        $trkitm_id = Purefy($_POST['trkitm_id']);
        //$KodeWorks = Purefy($_POST['trkitm_id']);

        $cGetTrackID = "SELECT `trkitm`.`trklist_id`, `trkitm`.`works_id`, `trkitm`.`trkitm_status`, `trkitm`.`reason_title`, `trkitm`.`reason_note` FROM `izin_apps_trkitm` AS `trkitm` ";
        $cGetTrackID .= "WHERE `trkitm`.`id_trkitm` = '" . $trkitm_id . "' ";
        $rcGetTrackID = $dbs->getQuery($cGetTrackID);
        $GetTrackID = $dbs->getAssoc($rcGetTrackID);
        $tracking_id = $GetTrackID["trklist_id"];
        $works_id = $GetTrackID["works_id"];
        $update_status = $GetTrackID["reason_title"];
        $reason_note = $GetTrackID["reason_note"];
        $badge_id = $GetTrackID["trkitm_status"];

        $cGetClientData = "SELECT `trklist`.`trk_status`, `trklist`.`trk_password`, `trklist`.`client_id`, `trklist`.`client_name`, `trklist`.`company_name`, ";
        $cGetClientData .= "`trklist`.`pck_nama`, `lead`.`client_email`, `lead`.`client_wa`, `lead`.`package_id`, ";
        $cGetClientData .= "(SELECT bank_acc FROM `izin_apps_invdet` as `inv` WHERE `inv`.`id_lead` = `trklist`.`lead_id`) as bank_acc ";
        $cGetClientData .= "FROM `izin_apps_trklist` AS `trklist` ";
        $cGetClientData .= "INNER JOIN `izin_apps_lead` AS `lead` ON `trklist`.`lead_id` = `lead`.`id_lead` ";
        $cGetClientData .= "WHERE `trklist`.`id_trklist` = '" . $tracking_id . "' ";
        $rcGetClientData = $dbs->getQuery($cGetClientData);
        $GetClientData = $dbs->getAssoc($rcGetClientData);

        $PackageID = $GetClientData["package_id"];
        $ClientId = $GetClientData["client_id"];
        $PackageName = $GetClientData["pck_nama"];

        $trkstatus = $GetClientData["trk_status"];
        $pwdtohash = $GetClientData["trk_password"];

        $Email_Name = $GetClientData["client_name"];
        $Company_Name = $GetClientData["company_name"];
        $PhoneNo = $GetClientData["client_wa"];
        $Email_Address = $GetClientData["client_email"];

        $BnkAcc = $GetClientData['bank_acc'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmPT = $BnkAccDet[1];

        $cGetWorkData = "SELECT `works`.`nama`, `works`.`kode` FROM `izin_apps_works` AS `works` ";
        $cGetWorkData .= "WHERE `works`.`id` = '" . $works_id . "' ";
        $rcGetWorkData = $dbs->getQuery($cGetWorkData);
        $GetWorkData = $dbs->getAssoc($rcGetWorkData);
        $workcode = $GetWorkData["kode"];
        $workname = $GetWorkData["nama"];

        $sendWA = false;
        $wa_message = "";

        if (intval($badge_id) == 2) {
            $sendEmail = true;
            require "Modules/Tracking/file/MailNWA_PENDING.php";
        } else {
            switch ($workcode) {
                case "AKTA - SK":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_Akta.php";
                    break;
                case "HKIM":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_HKIM.php";
                    break;
                case "NPWP":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_NPWP.php";
                    break;
                case "SIUP":
                    if ($PackageID == "CUSTOM") {
                        $sendEmail = true;
                        $sendWA = true;
                        require "Modules/Tracking/file/MailNWA_SIUP_c.php";
                    } else {
                        $sendEmail = true;
                        $sendWA = true;
                        require "Modules/Tracking/file/MailNWA_SIUP.php";
                    }
                    break;
                case "NIB":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_NIB.php";
                    break;
                case "API":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_API.php";
                    break;
                case "KPPA":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_KPPA.php";
                    break;
                case "KADIN":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_KADIN.php";
                    break;
                case "APEC":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_APEC.php";
                    break;
                case "RPTKA&IMTA":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_RPTKA_IMTA.php";
                    break;
                case "VISA":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_VISA.php";
                    break;
                case "KITAS":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_KITAS.php";
                    break;
                case "KITAS_INV":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_KITAS_Investor.php";
                    break;
                case "STM":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_STM.php";
                    break;
                case "SKTTS":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_SKTT.php";
                    break;
                case "SS_OSS":
                    $sendEmail = true;
                    $sendWA = true;
                    require "Modules/Tracking/file/MailNWA_SS_OSS.php";
                    break;
                case "BNRI":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_BNRI.php";
                    break;
                case "IPAK":
                case "IDAK":
                case "BPOM":
                case "IKP":
                case "IAPTK":
                case "PIRT":
                case "IFWR":
                case "I_MBA":
                case "LPK":
                case "SHL":
                case "ILS":
                case "IRST":
                case "LPSE":
                case "TDUP":
                case "PKRT":
                case "IUJK":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_IZIN_Khusus.php";
                    break;
                case "EFIN":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_EFIN.php";
                    break;
                case "SPTT_T":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_SPTT_T.php";
                    break;
                case "SPT_M":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_SPT_M.php";
                    break;
                case "PKP":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_PKP.php";
                    break;
                case "ACC":
                    $sendEmail = true;
                    require "Modules/Tracking/file/Mail_ACC.php";
                    break;
                default:
                    $sendEmail = false;
                    $sendWA = false;
                    //code to be executed if n is different from all labels;
                    //incase come from sidebar menu.
            } //switch($KodeWorks) {
        } //if(intval($badge_id) == 2) {

        //blast Email
        if ($sendEmail) {
            require "Modules/Tracking/file/Send_Mail.php";
        }

        //blast WA
        if ($sendWA) {
            $WaBlast_result = WaBlast($PhoneNo, $WA_Message);
            $wa_message = WA_message($WaBlast_result);
        }

        if (!isset($_SESSION["AlertMess"]))
            $_SESSION["AlertMess"] = $wa_message;
        else
            $_SESSION["AlertMess"] .= "<BR/>" . $wa_message;


        require "Modules/Tracking/file/TrackDetail_i.php";
        $_SESSION["TrackingPage"] = "Track_pg";
    }

    /******************************************
     * End of Tracking
     ******************************************/


    /******************************************
     * Add Tracking
     ******************************************/

    //SHOW ADD TRACKING PAGE
    if ($task == "AddTrackPage_" . $_SESSION["butts_Add"]) {

        require "Modules/Tracking/file/AddTracking_i.php";
        $_SESSION["TrackingPage"] = "AddTrack_pg";
    } //if($task == "AddTrackPage_".$_SESSION["butts_Add"]) {


    //SAVE CHANGES BUTTON ACTION
    if ($task == "SaveChanges_" . $_SESSION["butts_Save"]) {
        $ins_leadid = Purefy($_POST['insert_lead']); //used as id
        //$ins_id_client = Purefy($_POST['ins_id_client']); //hidden
        //$ins_nama_client = Purefy($_POST['ins_nama_client']); //showed in form
        //$ins_pck_id = Purefy($_POST['ins_pck_id']); //hidden no use
        //$ins_pck_nama = Purefy($_POST['ins_pck_nama']); //showed in form
        //$ins_company_name = Purefy($_POST['ins_company_name']); //showed in form

        $cChosenAddTrack = "SELECT `lead`.`id_lead`, `lead`.`client_id`, `lead`.`client_name`, `lead`.`package_id`, `lead`.`package_name`, `inv`.`inv_num`, ";
        $cChosenAddTrack .= "CASE WHEN `inv`.`agree_status` = '0' THEN `inv`.`name` ELSE (SELECT `client_company_name` FROM `izin_apps_agreement` WHERE `inv_id` = `inv`.`id` AND `discard` = 0) END AS `company_name` ";
        $cChosenAddTrack .= "FROM `izin_apps_lead` AS `lead` ";
        $cChosenAddTrack .= "LEFT JOIN `izin_apps_invdet` as `inv` on `inv`.`id_lead` = `lead`.`id_lead` ";
        $cChosenAddTrack .= "WHERE `lead`.`id_lead` = ? ";
        $rcChosenAddTrack = $dbs->getQuery($cChosenAddTrack, "Exec", ["s|$ins_leadid"]);
        $ChosenAddTrack = $dbs->getAssoc($rcChosenAddTrack);

        $ins_id_client = $ChosenAddTrack['client_id']; //hidden
        $ins_nama_client = $ChosenAddTrack['client_name']; //showed in form
        $ins_pck_id = $ChosenAddTrack['package_id']; //hidden no use
        $ins_pck_nama = $ChosenAddTrack['package_name']; //showed in form
        $ins_company_name = $ChosenAddTrack['company_name']; //showed in form

        require "Modules/Tracking/file/AddTracking_i.php";
        $_SESSION["TrackingPage"] = "AddTrack_pg";
    } //if($task == "SaveChanges_".$_SESSION["butts_Save"]) {

    //ADD BUTTON ACTION
    if ($task == "AddTrack_" . $_SESSION["butts_Add"]) {

        /*      if (isset($_SESSION["TrackingPage"])) {
          unset($_SESSION["TrackingPage"]);
      }
      $_SESSION["TrackingPage"] = "AddTrack_pg";
*/


        //BREADCRUMB USER MANAGEMENT
        $linkTarget = "DtTrac_pg";
        $cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'Tracking' AND `page` = '" . $linkTarget . "'";
        $rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
        $BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
        $bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
        $bcDtTrac = BASEURL . $bcsplitKeys . "/0/0.html";

        $ins_leadid = Purefy($_POST['leadID']);
        $ins_clientid = Purefy($_POST['clientID']);
        $ins_clientname = Purefy($_POST['clientName']);
        $ins_legalname = Purefy($_POST['legalName']);
        $ins_packagename = Purefy($_POST['packageName']);
        //$ins_packageid = Purefy($_POST['packageID']);


        //cek kalau lead id tidak ada di tabel tracking
        $cFindTrkList = "SELECT * FROM `izin_apps_trklist` WHERE `lead_id` = '" . $ins_leadid . "" . "'";
        $rcFindTrkList = $dbs->getQuery($cFindTrkList);
        $FindTrkList = $dbs->getAssoc($rcFindTrkList);

        if (!isset($FindTrkList["lead_id"])) {

            // find free number for tracking_id
            //$arrMonth[];
            //$thisYearMonth = (string) date("Ym");
            $thisYearMonth = (string) date("ym");
            //            $cLoadTrkList = "SELECT * FROM ".$tblp."apps_trklist WHERE `tracking_id` LIKE '%".$thisYearMonth."%'";
            //select * from izin_apps_trklist where tracking_id LIKE '%1905%' and MAX(tracking_id)
            //$cLoadTrkList = "SELECT COUNT(`tracking_id`) FROM `izin_apps_trklist` WHERE `tracking_id` LIKE '%".$thisYearMonth."%'";

            $cLoadTrkList = "SELECT * FROM `izin_apps_trklist` ORDER BY `tracking_id` DESC LIMIT 1";
            $rcLoadTrkList = $dbs->getQuery($cLoadTrkList);
            $LoadTrkList = $dbs->getAssoc($rcLoadTrkList);

            $lastYearMonth = substr($LoadTrkList["tracking_id"], 0, -3);

            //echo "now ym: ".$thisYearMonth;
            //echo "<br/>";
            //echo "lst ym: ".$lastYearMonth;
            //exit;
            //if($thisYearMonth > $lastYearMonth) {
            if ($thisYearMonth != $lastYearMonth) {
                $exIDserial = 0;
            } else {
                $exIDserial = intval(substr($LoadTrkList["tracking_id"], 4));
            }


            $trackingid_no = (string) $exIDserial + 1;
            if ($exIDserial < 100) {
                $trackingid = sprintf("%03s", $trackingid_no);
            } else {
                $trackingid = $trackingid_no;
            }
            /*
            while($LoadTrkList = $dbs->getAssoc($rcLoadTrkList)){
                //$exIDserial = (int) substr($LoadTrkList["tracking_id"],6);
                $exIDserial = intval(substr($LoadTrkList["tracking_id"],5));
                $arrMonth[] = (int)$exIDserial;
            }

*/

            //            $trackingid_no = (string) max($arrMonth)+1;
            //            $trackingid = sprintf("%03s", $trackingid_no);
            $trackingid = $thisYearMonth . $trackingid;

            //!!!!!! Pengaman kalau gagal        //  SAVE tracking list
            $ins_id_trklist = md5(uniqids(microtime()));
            $dt = new DateTime('now', new DateTimezone(TIMESET));
            $date_now = $dt->format('Y-m-d H:i:s');

            $cInsTrkList = "INSERT INTO `izin_apps_trklist` (`id_trklist`, `tracking_id`, `trackid_md5`, `lead_id`, `tgl_start`, `tgl_close`, `trk_status`, `trk_password`, `client_id`, `client_name`, `company_name`, `pck_nama`) ";
            $cInsTrkList .= "VALUES ('" . $ins_id_trklist . "', '" . $trackingid . "', MD5('" . $trackingid . "'), '" . $ins_leadid . "', '" . $date_now . "', NULL, 0, NULL, '" . $ins_clientid . "', '" . $ins_clientname . "', '" . $ins_legalname . "', '" . $ins_packagename . "')";
            //$cInsTrkList .= "VALUES ('".$ins_id_trklist."', '".$trackingid."', MD5('".$trackingid."'), '".$ins_leadid."', NOW(), NULL, 0, NULL, '".$ins_clientid."', '".$ins_clientname."', '".$ins_legalname."', '".$ins_packagename."')";
            $rcInsTrkList = $dbs->getQuery($cInsTrkList);

            // baca lead untuk dapatkan id packages
            //$cLoadLead = "SELECT * FROM ".$tblp."apps_lead WHERE `id_lead` = '".$ins_leadid."'";
            //$rcLoadLead = $dbs->getQuery($cLoadLead);
            //$LoadLead = $dbs->getAssoc($rcLoadLead);
            //$package_id = $LoadLead["package_id"];

            // baca pkgitm untuk dapatkan id item WAITING 'ffe8ac087db20323778079b05ed36482'
            //$cLoadPkgitm = "SELECT * FROM ".$tblp."apps_pkgitm WHERE `package_id` = '".$package_id."' AND `mstitem_id` = 'ffe8ac087db20323778079b05ed36482'";
            //$rcLoadPkgitm = $dbs->getQuery($cLoadPkgitm);
            //$LoadPkgitm = $dbs->getAssoc($rcLoadPkgitm);
            $ins_mstitem_id = "ffe8ac087db20323778079b05ed36482";

            //CARI DARI PACKAGEWORK YANG URUTANNYA PALING KECIL UNTUK PT/CV ITU AKTA, UNTUK CUSTOM YANG PALING KECIL
            $cFindItemSS = "SELECT COUNT(`id_work`) AS `Smallest_Sequence` FROM `izin_apps_packagework` WHERE `id_lead` = '" . $ins_leadid . "' ";
            $cFindItemSS .= "AND `urutan` IN (SELECT MIN(`urutan`) FROM `izin_apps_packagework` WHERE `id_lead` = '" . $ins_leadid . "')";
            $rcFindItemSS = $dbs->getQuery($cFindItemSS);
            $FindItemSS = $dbs->getAssoc($rcFindItemSS);

            $cFindItem = "SELECT `kode`, `id_work` FROM `izin_apps_packagework` WHERE `id_lead` = '" . $ins_leadid . "' ";
            $cFindItem .= "AND `urutan` IN (SELECT MIN(`urutan`) FROM `izin_apps_packagework` WHERE `id_lead` = '" . $ins_leadid . "')";
            $rcFindItem = $dbs->getQuery($cFindItem);

            if (intval($FindItemSS["Smallest_Sequence"]) == 1) {
                //cari stakeholder
                //$ins_stkholder_id = "88fc001b69111479a709898a95526312";//izin
                $ins_stkholder_id = ""; //izin

                //$cFindItem = "SELECT `kode`, `id_work` FROM `izin_apps_packagework` WHERE `id_lead` = '".$ins_leadid."' ";
                //$cFindItem .= "AND `urutan` IN (SELECT MIN(`urutan`) FROM `izin_apps_packagework` WHERE `id_lead` = '".$ins_leadid."')";
                //$rcFindItem = $dbs->getQuery($cFindItem);
                $FindItem = $dbs->getAssoc($rcFindItem);
                $dt = new DateTime('now', new DateTimezone(TIMESET));
                $date_now = $dt->format('Y-m-d H:i:s');

                $cInsTrkItem = "INSERT INTO `izin_apps_trkitm` (id_trkitm, trklist_id, works_id, Stakeholder_id, tgl_start_itm, updater_id, updater_name, tgl_update_itm, reason_title, trkitm_status) ";
                //$cInsTrkItem .= "VALUES ('".md5(uniqids(microtime()))."', '".$ins_id_trklist."', '".$FindItem["id_work"]."', '".$ins_stkholder_id."', NOW(), '".$_SESSION["tblID"]."', '".$_SESSION["lgnNama"]."', NOW(), 'Add new ".$FindItem['kode']."', 0)";
                $cInsTrkItem .= "VALUES ('" . md5(uniqids(microtime())) . "', '" . $ins_id_trklist . "', '" . $FindItem["id_work"] . "', '" . $ins_stkholder_id . "', '" . $date_now . "', '" . $_SESSION["tblID"] . "', '" . $_SESSION["lgnNama"] . "', '" . $date_now . "', 'Add New " . $FindItem['kode'] . "', 0)";
                //$cInsTrkItem .= "VALUES ('".md5(uniqids(microtime()))."', '".$ins_id_trklist."', '".$FindItem["id_work"]."', '".$ins_stkholder_id."', '".$date_now."', '".$_SESSION["tblID"]."', '".$_SESSION["lgnNama"]."', '".$date_now."', 'Add New ".$FindItem['kode']."', 1)";
                $rcInsTrkItem = $dbs->getQuery($cInsTrkItem);
                //if($FindItem["kode"] == "AKTA") {
            } else {
                //$cFindItem = "SELECT `kode`, `id_work` FROM `izin_apps_packagework` WHERE `id_lead` = '".$ins_leadid."' ";
                //$cFindItem .= "AND `urutan` IN (SELECT MIN(`urutan`) FROM `izin_apps_packagework` WHERE `id_lead` = '".$ins_leadid."')";
                //$rcFindItem = $dbs->getQuery($cFindItem);
                $dt = new DateTime('now', new DateTimezone(TIMESET));
                $date_now = $dt->format('Y-m-d H:i:s');
                //cari stakeholder
                //$ins_stkholder_id = "88fc001b69111479a709898a95526312"; //izin
                $ins_stkholder_id = ""; //izin

                while ($FindItem = $dbs->getAssoc($rcFindItem)) {
                    $cInsTrkItem = "INSERT INTO `izin_apps_trkitm` (id_trkitm, trklist_id, works_id, Stakeholder_id, tgl_start_itm, updater_id, updater_name, tgl_update_itm, reason_title, trkitm_status) ";
                    //$cInsTrkItem .= "VALUES ('".md5(uniqids(microtime()))."', '".$ins_id_trklist."', '".$FindItem["id_work"]."', '".$ins_stkholder_id."', NOW(), '".$_SESSION["tblID"]."', '".$_SESSION["lgnNama"]."', '".$_SESSION["lgnNama"]."', NOW(), 'Add new ".$FindItem['kode']."', 0)";
                    $cInsTrkItem .= "VALUES ('" . md5(uniqids(microtime())) . "', '" . $ins_id_trklist . "', '" . $FindItem["id_work"] . "', '" . $ins_stkholder_id . "', '" . $date_now . "', '" . $_SESSION["tblID"] . "', '" . $_SESSION["lgnNama"] . "', '" . $date_now . "', 'Add new " . $FindItem['kode'] . "', 0)";
                    //$cInsTrkItem .= "VALUES ('".md5(uniqids(microtime()))."', '".$ins_id_trklist."', '".$FindItem["id_work"]."', '".$ins_stkholder_id."', '".$date_now."', '".$_SESSION["tblID"]."', '".$_SESSION["lgnNama"]."', '".$date_now."', 'On going ".$FindItem['kode']."', 1)";
                    $rcInsTrkItem = $dbs->getQuery($cInsTrkItem);
                }
            }
            //!!!!!! Pengaman kalau gagal        // save tracking item

            $_SESSION["AlertMess"] = $lang_50125; //New tracking inserted
            header("Location: " . $bcDtTrac);
            die();
        } else {
            // Lead already in tracking
            $_SESSION["AlertMess"] = $lang_50126; //Add canceled, lead exists in tracking
            header("Location: " . $bcDtTrac);
            die();
        } //if(empty($FindTrkList)) {} else {

    } //if($task == "AddTrack_".$_SESSION["butts_Add"]) {

    /******************************************
     * End of Add Tracking
     ******************************************/
} else {
    if (isset($_SESSION["TrackingFromNotif"]) && $_SESSION["TrackingFromNotif"] === true) {
        require "Modules/Tracking/file/TrackDetail_i.php";
        unset($_SESSION["TrackingFromNotif"]);
    } else {
        require "Modules/Tracking/file/TrackList_i.php";
    }
}
