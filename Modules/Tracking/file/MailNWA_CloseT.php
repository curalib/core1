<?php

$Email_Subject = "✅  Update process - AKTA ".$Company_Name;

$Email_Body = "<DIV style='max-width: 500px'><P>Dear Client,</P>

<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Dengan demikian seluruh proses perizinan telah selesai.</P>

<P>Dokumen asli dapat diambil di Kantor kami di :<BR/>
<STRONG>Centennial Tower, level 29<BR/>
Jalan Jendral Gatot Subroto Kav 24 - 25, RT.2/RW.2, <BR/>
Karet Semanggi, Setiabudi, Jakarta Selatan, <BR/>
DKI Jakarta 12950</STRONG></P>

<P>Apabila ingin mengambil dokumen, mohon untuk menginformasikan kepada kami agar kami bisa
mempersiapkan dokumennya.</P>

<P>Terima kasih karena telah menggunakan layanan <a href='https://izin.co.id'>IZIN.co.id</a> dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely, <BR/>
<A HREF='https://izin.co.id'>IZIN.co.id</A></P>

<BR/><HR>
<P class='text-muted'><i>Kunjungi https://tracking.izin.co.id dengan memasukan kode Tracking ID melalui website
dan log in password untuk mendownload file legalitas Perusahaan anda.</i></P>
</DIV>
";

$Email_Alt_Body = "
Dear Client,

Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa akta pendirian dan pengesahan Kemenkumham perusahaan anda
telah selesai.

Berikut kami lampirkan scan akta pendirian berikut dengan pengesahan dari Kemenkumham, untuk
fisik akta akan kami serahkan secara keseluruhan setelah semua perizinan selesai dibuat.

Proses selanjutnya adalah pengurusan NPWP Perusahaan.

Terima kasih karena telah menggunakan layanan IZIN.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id

______________________________________________________________________________________

Kunjungi https://tracking.izin.co.id dengan memasukan kode Tracking ID melalui website
dan log in password untuk mendownload file legalitas Perusahaan anda.
";

$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Congratulation! Akta dan Pengesahan Perusahaan anda telah selesai diproses.
Kunjungi https://tracking.izin.co.id untuk melihat status lebih lanjut.

Sincerely,
IZIN.co.id

__________________________________________________________________________

_Kunjungi_ _https://tracking.izin.co.id_ _dengan_ _memasukan_ _kode_ _Tracking_ _ID_ _melalui_ _website_ _dan_ _log_ _in_ _password_ _untuk_ _mendownload_ _file_ _legalitas_ _Perusahaan_ _anda._
";

?>
