<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "VISA";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        We enclose a VISA for $Company_Name in the attachment.
        <br><br>
        The document can be used in the span of 90 days after the issue date, please inform us if the Visa is used and send us a picture of the immigration stamp so we can proceed to the next steps.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! VISA has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Warm regards,
*Invest In Asia team*
";
}