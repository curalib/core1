<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "Investor Work Permit";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        Congratulation, Your KITAS (Kartu Izin Tinggal Terbatas) or Investor Work Permit for $Company_Name have been completed.
        <br><br>
        This permit can be used to work in Indonesia and at valid for 2 year.
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! Investor Work Permit has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Thank You.

Warm regards,
*Invest In Asia team*
";
}