<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "Sertifikat Standar completed";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        We attach your completed company Standard Certificate.
        <br><br>
        Standard certificate is a statement and/or proof of compliance with standards for implementing business activities For companies with medium and high risk business sectors. In order to be effective, you can meet the requirements as listed in the attachment.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! Sertifikat Standart has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Warm regards,
*Invest In Asia team*
";

}