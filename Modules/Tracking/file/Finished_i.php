<?php


if (isset($_SESSION["TrackingPage"])) {
    unset($_SESSION["TrackingPage"]);
}
if (isset($_SESSION["showtrackingid"])) {
    unset($_SESSION["showtrackingid"]);
}


//BREADCRUMB
$cBrdCrumb = "SELECT tgl FROM " . $tblp . "sys_menu WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL . $bcsplitKeys . "/0/0.html";

/*
    //BREADCRUMB TRACKING
    $linkTarget="Track_pg";
    $cBrdCrumb = "SELECT tgl FROM ".$tblp."sys_menu WHERE `module` = 'Tracking' AND `page` = '".$linkTarget."'";
    $rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
    $BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
    $bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
    $bcTrack = BASEURL.$bcsplitKeys."/0/0.html";
    */

$Use_DateRange = false;

//VIEW TRACKING LIST
$chkbox = 0;

$srcque_user = "";
/*
if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
    $srcque_user = " AND `lead`.`sales_id` = '" . $_SESSION['lgnDetID'] . "' ";
}
*/
$srcque_date = "";
if (isset($_SESSION["TrackingFinishSearchDateRange"])) {
    $date_from = substr($_SESSION["TrackingFinishSearchDateRange"], 0, 10);
    $date_to = substr($_SESSION["TrackingFinishSearchDateRange"], -10);
    $date_range = $date_from . " to " . $date_to;
    $date_to = str_replace('/', '-', $date_to);
    $date_to = date("Y-m-d", strtotime($date_to));
    $date_from = str_replace('/', '-', $date_from);
    $date_from = date("Y-m-d", strtotime($date_from));

    $srcque_date = " AND str_to_date(`track`.`tgl_close`, '%Y-%m-%d') BETWEEN '" . $date_from . "' AND '" . $date_to . "' ";
}
if (isset($_SESSION["TrackingFinishSearch"])) {
    $srcque = "`track`.`trk_status` = 3 AND `track`.`client_name` LIKE '%" . $_SESSION["TrackingFinishSearch"] . "%' $srcque_date $srcque_user ";
    $srcque .= "OR `track`.`trk_status` = 3 AND `track`.`company_name` LIKE '%" . $_SESSION["TrackingFinishSearch"] . "%' $srcque_date $srcque_user ";
    $srcque .= "OR `track`.`trk_status` = 3 AND `track`.`tracking_id` LIKE '%" . $_SESSION["TrackingFinishSearch"] . "%' $srcque_date $srcque_user ";
} else {
    $srcque = "`track`.`trk_status` = 3 $srcque_date $srcque_user ";
}

$cViewTrack = "SELECT `track`.`id_trklist`, `track`.`tracking_id`, `track`.`tgl_start`, `track`.`tgl_close`, `track`.`company_name`, ";
$cViewTrack .= "`track`.`client_id`, `track`.`client_name`, `track`.`pck_nama`, `lead`.`sales_name` FROM `izin_apps_trklist` AS `track` ";
//    $cViewTrack .= "`lead`.`client_id`, `lead`.`client_name`, `lead`.`package_name` FROM `izin_apps_trklist` AS `track` ";
$cViewTrack .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
$cViewTrack .= "WHERE $srcque "; // DONE
$cViewTrack .= "ORDER BY `track`.`tgl_close` DESC ";
// echo $cViewTrack;
// exit(0);
//    $rcViewTrack = $dbs->getQuery($cViewTrack);

//  QUERY FOR COUNT ROW
$cViewTrack2 = "SELECT COUNT(`track`.`id_trklist`) AS jumlah FROM `izin_apps_trklist` AS `track` ";
$cViewTrack2 .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
$cViewTrack2 .= "WHERE $srcque ";
//echo $cViewTrack2;
//exit(0);

if (isset($task) && $task == "ExportToXLS_" . $_SESSION["butts_Enter"]) {
    $rcViewTrack = $dbs->getQuery($cViewTrack);
} else {
    $d = new paging($dbs, PAGE_ITEM, PAGE_LIST, -1, "LstFinished");
    $d->queryTracking($cViewTrack, $cViewTrack2);
}
