<?php

require_once 'system/SendEmail.php';

$mail->IsSMTP(); //Tell PHPMailer to use SMTP
$mail->SMTPDebug = EMAIL_SMTPDEBUG; // debugging: 0 = off (for production use) 1 = errors and messages, 2 = messages only
//$mail->Debugoutput = 'html';
$mail->Host = EMAIL_HOST; // "smtp.gmail.com" - Google (Gmail) SMTP server.
$mail->SMTPAuth = EMAIL_SMTPAUTH; // true - authentication enabled
$mail->SMTPSecure = EMAIL_SMTPSECURE; // 'ssl'/'tls' - secure transfer enabled REQUIRED for Gmail
$mail->Port = EMAIL_PORT; // 465 - or 587
$mail->CharSet = "UTF-8";
$mail->SMTPOptions = array(
    'ssl' => array(
    'verify_peer' => false,
    'verify_peer_name' => false,
    'allow_self_signed' => true
    )
); // Disable some SSL checks.
$mail->IsHTML(true); // true -
$mail->Username = EMAIL_USERNAME; //Username to use for SMTP authentication - use full email address for gmail
$mail->Password = EMAIL_PASSWORD; //Password to use for SMTP authentication
$mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME); //Set who the message is to be sent from
$mail->AddCC(EMAIL_CC);
if (isset($imgattach)) {
    foreach ($imgattach as $item) {
        $mail->AddEmbeddedImage($item['path'], $item['contentID']);
    }
}
$mail->AddAddress($Email_Address, $Email_Name); //Set who the message is to be sent to
$mail->Subject = $Email_Subject; //Set the subject line
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
if ($mail->send())
{
    $_SESSION["AlertMess"] = "Email has been sent to client " . $Email_Address;
}
else
{
    $_SESSION["AlertMess"] =  $mail->ErrorInfo;
}