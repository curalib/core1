<?php
$Email_Subject = "Update process - ".$KodeWorks." ".$Company_Name;
$Email_Body = "<P>Dear Client,</P>

<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Kami ingin memberitahu anda bahwa proses perizinan anda tertunda dikarenakan<BR>
".$update_status." : ".$reason_note."</P>

<P>Mohon untuk segera menyelesaikan/memberikan dokumen terkait untuk melanjutkan proses
pembuatan ".$Company_Name.".</P>

<P>Terima kasih karena telah menggunakan layanan <a href='https://izin.co.id'>IZIN.co.id</a> dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely,<br/>
<a href='https://izin.co.id'>IZIN.co.id</a></P>
";
$Email_Alt_Body = "Dear Client,

Salam dari IZIN CO ID!

Kami ingin memberitahu anda bahwa proses perizinan anda tertunda dikarenakan
".$update_status." : ".$reason_note."

Mohon untuk segera menyelesaikan/memberikan dokumen terkait untuk melanjutkan proses
pembuatan perusahaan anda.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id
";
$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Notice, Pending Document

Saat ini proses pembuatan perusahaan anda tertunda dikarenakan ".$update_status."
Untuk informasi lebih lanjut dapat menghubungi kami melalui link https://izin.co.id.

Sincerely,
IZIN.co.id
";
?>
