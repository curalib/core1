<?php
$Email_Subject = "Update process - NPWP ".$Company_Name;
$Email_Body = "<P>Dear Client,</P>

<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Kami ingin menginformasikan bahwa NPWP (Nomor Pokok Wajib Pajak) Perusahaan anda telah
selesai.</P>

<P>Proses selanjutnya adalah pengurusan SIUP (Surat Izin Usaha Perdagangan) & NIB (Nomor Induk
Berusaha).</P>

<P>Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely,<BR/>
<a href='https://izin.co.id'>IZIN.co.id</a></P>
";
$Email_Alt_Body = "Dear Client,

Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa NPWP (Nomor Pokok Wajib Pajak) Perusahaan anda telah
selesai.

Proses selanjutnya adalah pengurusan SIUP (Surat Izin Usaha Perdagangan) & NIB (Nomor Induk
Berusaha).

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id
";
$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Congratulation! NPWP Perusahaan anda telah selesai diproses.
Kunjungi https://tracking.izin.co.id untuk melihat status lebih lanjut.

Sincerely,
IZIN.co.id
";
?>
