<?php
$Email_Subject = "Update process - AKTA ".$Company_Name;
$Email_Body = "
<P>Dear Client,</P>

<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Kami ingin menginformasikan bahwa akta pendirian dan pengesahan Kemenkumham perusahaan anda
telah selesai.</P>

<P>Berikut kami lampirkan scan akta pendirian berikut dengan pengesahan dari Kemenkumham, untuk
fisik akta akan kami serahkan secara keseluruhan setelah semua perizinan selesai dibuat.</P>

<P>Proses selanjutnya adalah pengurusan NPWP Perusahaan.</P>

<P>Terima kasih karena telah menggunakan layanan <a href='izin.co.id'>izin.co.id</a> dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely,<BR/>
<a href='https://izin.co.id'>IZIN.co.id</a></P>
";
$Email_Alt_Body = "
Dear Client,

Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa akta pendirian dan pengesahan Kemenkumham perusahaan anda
telah selesai.

Berikut kami lampirkan scan akta pendirian berikut dengan pengesahan dari Kemenkumham, untuk
fisik akta akan kami serahkan secara keseluruhan setelah semua perizinan selesai dibuat.

Proses selanjutnya adalah pengurusan NPWP Perusahaan.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id
";
$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Congratulation! Akta dan Pengesahan Perusahaan anda telah selesai diproses.
Kunjungi https://tracking.izin.co.id untuk melihat status lebih lanjut.

Sincerely,
IZIN.co.id
";
?>
