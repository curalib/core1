<?php
$Email_Subject = "Update process - SIUP & NIB ".$Company_Name;
$Email_Body = "<P>Dear Client,</P>
<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Kami ingin menginformasikan bahwa SIUP (Surat Izin Usaha Perdagangan) & NIB (Nomor Induk
Berusaha) Perusahaan anda telah selesai.</P>

<P>NIB adalah nomor identitas pelaku usaha dalam pelaksanaan kegiatan usaha dan berfungsi sebagai
TDP (Tanda Daftar Perusahaan). </P>

<P>Berikut kami lampirkan scan SIUP dan NIB yang telah selesai. Proses selanjutnya adalah pengurusan
SKDP Perusahaan.</P>

<P>Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely,<BR/>
<a href='https://izin.co.id'>IZIN.co.id</a></P>
";
$Email_Alt_Body = "Dear Client,

Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa SIUP (Surat Izin Usaha Perdagangan) & NIB (Nomor Induk
Berusaha) Perusahaan anda telah selesai.

NIB adalah nomor identitas pelaku usaha dalam pelaksanaan kegiatan usaha dan berfungsi sebagai
TDP (Tanda Daftar Perusahaan). 

Berikut kami lampirkan scan SIUP dan NIB yang telah selesai. Proses selanjutnya adalah pengurusan
SKDP Perusahaan.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id
";
$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Congratulation! Akta dan Pengesahan Perusahaan anda telah selesai diproses.
Kunjungi https://tracking.izin.co.id untuk melihat status lebih lanjut.

Sincerely,
IZIN.co.id
";
?>
