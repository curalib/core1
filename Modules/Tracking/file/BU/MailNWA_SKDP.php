<?php
$Email_Subject = "Update process - SKDP ".$Company_Name;
$Email_Body = "<P>Dear Client,</P>
<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Kami ingin menginformasikan bahwa SKDP (Surat Keterangan Domisili Perusahaan) anda telah
selesai.</P>

<P>Berikut kami lampirkan scan SKDP ".$Company_Name."</P>

<P>Dengan demikian seluruh proses perizinan telah selesai.</P>

<P>Dokumen asli dapat diambil di Kantor kami di :<BR/>
<STRONG>Centennial Tower, level 29<BR/>
Jalan Jendral Gatot Subroto Kav 24 - 25, RT.2/RW.2, <BR/>
Karet Semanggi, Setiabudi, Jakarta Selatan, <BR/>
DKI Jakarta 12950</STRONG></P>

<P>Apabila ingin mengambil dokumen, mohon untuk menginformasikan kepada kami agar kami bisa
mempersiapkan dokumennya.</P>

<P>Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely,<BR/>
<a href='https://izin.co.id'>IZIN.co.id</a></P>
";
$Email_Alt_Body = "Dear Client,
Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa SKDP (Surat Keterangan Domisili Perusahaan) anda telah
selesai.

Berikut kami lampirkan scan SKDP ".$Company_Name."

Dengan demikian seluruh proses perizinan telah selesai.

Dokumen asli dapat diambil di Kantor kami di :
Centennial Tower, level 29
Jalan Jendral Gatot Subroto Kav 24 - 25, RT.2/RW.2, Karet Semanggi, Setiabudi, Jakarta
Selatan, DKI Jakarta 12950

Apabila ingin mengambil dokumen, mohon untuk menginformasikan kepada kami agar kami bisa
mempersiapkan dokumennya.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id
";
$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Congratulation! SKDP anda telah selesai diproses.
Kunjungi https://tracking.izin.co.id untuk melihat status lebih lanjut.

Sincerely,
IZIN.co.id
";
?>
