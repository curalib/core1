<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "KPPA";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        Congratulation, all your company documents have been completed.
        <br><br>
        We attach your completed company KPPA (Kantor Perwakilan Perusahaan Asing) permit.
        <br><br>
        For further information about document pick-up and delivery, please contact our customer service.
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! KPPA has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Thank You.

Warm regards,
*Invest In Asia team*
";
}