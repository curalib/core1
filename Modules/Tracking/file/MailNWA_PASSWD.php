<?php

$Email_Subject = "✅ Tracking ID dan Password  ".$Company_Name." - IZIN.co.id";

$imgattach = [
  array(
      'path' => "https://erp.izin.co.id/assets/images/tutorial-izin-11.jpg",
      'contentID' => "izin11"
  ),
  array(
      'path' => "https://erp.izin.co.id/assets/images/tutorial-izin-22.jpg",
      'contentID' => "izin22"
  ),
  array(
      'path' => "https://erp.izin.co.id/assets/images/tutorial-izin-33.jpg",
      'contentID' => "izin33"
  ),
  array(
      'path' => "https://erp.izin.co.id/assets/images/tutorial-izin-44.jpg",
      'contentID' => "izin44"
  ),
];

$Email_Body = "<DIV style='max-width: 500px'><P>Kepada Yth,</P>
<P> ".$Company_Name."</P>
<P><STRONG>Salam dari IZIN CO ID!</STRONG></P>

<P>Pembuatan Perusahaan anda saat ini sudah dalam proses. Anda dapat melakukan pengecekan status pembuatan perusahaan anda melalui IZIN Tracker, sistem tracking perizinan kami disini:
</P>
<P>Kunjungi  <a href='tracking.izin.co.id'>tracking.izin.co.id</a> atau klik tombol Track Perizinan di website www.izin.co.id untuk mengakses sistem kami.
</P>
<img style='width:45%;padding:5px;' src='https://erp.izin.co.id/assets/images/tutorial-izin-11.jpg'>
<img style='width:45%;padding:5px;'  src='https://erp.izin.co.id/assets/images/tutorial-izin-22.jpg'>
<img style='width:45%;padding:5px;'  src='https://erp.izin.co.id/assets/images/tutorial-izin-33.jpg'>
<img  style='width:45%;padding:5px;' src='https://erp.izin.co.id/assets/images/tutorial-izin-44.jpg'>
<br>
<P>Berikut Tracking ID dan password anda sebagai berikut :<BR/>

<strong>Tracking ID : ".$tracking_id."<BR/>
Password : ".$pwdtohash." </strong> </P>

<P>Masukan kode Tracking ID melalui website dan log in password untuk mengecek proses perizinan dan mengunduh file legalitas Perusahaan anda. Petunjuk penggunaan kami lampirkan dibawah.</P>

<P>Terima kasih atas kepercayaan Anda menggunakan layanan IZIN.co.id. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.</P>

<P>Salam hangat, <BR/>
<A HREF='https://izin.co.id'>IZIN.co.id</A></P>

<BR/><HR>
<P class='text-muted'><i>Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id</i></P>
</DIV>
";

$Email_Alt_Body = "Dear Client,

Salam dari IZIN CO ID!

Pembuatan Perusahaan anda saat ini sudah dalam proses. Anda dapat melakukan pengecekan status pembuatan perusahaan anda melalui IZIN Tracker, sistem tracking perizinan kami disini:
Kunjungi tracking.izin.co.id <a href='tracking.izin.co.id'>tracking.izin.co.id</a> atau klik tombol Track Perizinan di website www.izin.co.id untuk mengakses sistem kami.
Berikut Tracking ID dan password anda sebagai berikut :

Tracking ID : ".$tracking_id."
Password : ".$pwdtohash."

Kunjungi https://tracking.izin.co.id dengan memasukan kode Tracking ID melalui website
dan log in password untuk mendownload file legalitas Perusahaan anda.

Sincerely,
IZIN.co.id

______________________________________________________________________________________

This is an automated message please do not reply directly to this email. For further
information you can contact us through our whatsapp/email +62 822-9998-0011 / cs@izin.co.id
";

$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Pembuatan Perusahaan anda saat ini sudah dalam proses. Anda dapat melakukan pengecekan status pembuatan perusahaan anda melalui IZIN Tracker, sistem tracking perizinan kami disini:

Kunjungi tracking.izin.co.id atau klik tombol Track Perizinan di website www.izin.co.id untuk mengakses sistem kami.

Berikut Tracking ID dan password anda sebagai berikut :

Tracking ID : ".$tracking_id."
Password : ".$pwdtohash."

Kunjungi https://tracking.izin.co.id dengan memasukan kode Tracking ID melalui website  dan log in password untuk mendownload file legalitas Perusahaan anda.

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62_ _822-9998-0011/cs@izin.co.id_
";

?>
