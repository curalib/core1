<?php


//BREADCRUMB
$cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL . $bcsplitKeys . "/0/0.html";

//BREADCRUMB USER MANAGEMENT
$linkTarget = "DtTrac_pg";
$cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'Tracking' AND `page` = '" . $linkTarget . "'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bcDtTrac = BASEURL . $bcsplitKeys . "/0/0.html";

//BUTTON ADD TRACKING
$actADD = "AddTrack_" . $_SESSION["butts_Add"];
$forms = "AddTrack";
$name = $lang_50109;
$buttAddLink = "";
$buttAddLink .= "													";
$buttAddLink .= "<a href=\"javascript:submitbutton('" . $actADD . "','" . $forms . "');\" id=\"" . $actADD . "\" class=\"btn btn-primary\">" . $name . "</a>";
$buttAddLink .= "\r\n";

$chkbox = 0;

//VIEW LEAD for TRACKING LIST
$cViewAddTrack = "SELECT `lead`.`sales_name`, `lead`.`id_lead`, `lead`.`client_id`, `lead`.`client_name`, `lead`.`package_id`, `lead`.`package_name`, `inv`.`inv_num`, ";
$cViewAddTrack .= "CASE WHEN `inv`.`agree_status` = '0' THEN `inv`.`name` ELSE (SELECT `client_company_name` FROM `izin_apps_agreement` WHERE `inv_id` = `inv`.`id` AND `discard` = 0) END AS `company_name` ";
$cViewAddTrack .= "FROM `izin_apps_lead` AS `lead` ";
$cViewAddTrack .= "LEFT JOIN `izin_apps_invdet` as `inv` on `inv`.`id_lead` = `lead`.`id_lead` ";
$cViewAddTrack .= "WHERE `lead`.`status` = 'CL' AND `inv`.`tipe` = 'PAID' AND `inv`.`paid` = 'Y' ";
$cViewAddTrack .= "AND `inv`.`id` IN (SELECT `inv_id` FROM `izin_apps_agreement` WHERE `discard` = 0) ";
$cViewAddTrack .= "AND `lead`.`id_lead` NOT IN (SELECT `lead_id` FROM `izin_apps_trklist`) ";
if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
    $cViewAddTrack .= "AND `lead`.`sales_id` = '" . $_SESSION['lgnDetID'] . "' ";
}
// By Barep
//$cViewAddTrack .= "GROUP BY `lead`.`client_name` ";
//--------------------------------------------------
$cViewAddTrack .= "ORDER BY `lead`.`client_name` ASC";
$rcViewAddTrack = $dbs->getQuery($cViewAddTrack);
//echo $cViewAddTrack;
//exit(0);

//BUTTON SAVE CHANGES
$act = "SaveChanges_" . $_SESSION["butts_Save"];
$forms = "InsertTrack";
$name = $lang_50122;
$buttInsLink = "";
$buttInsLink .= "													";
$buttInsLink .= "<a href=\"javascript:submitbutton('" . $act . "','" . $forms . "');\" id=\"" . $act . "\" class=\"btn btn-primary\">" . $name . "</a>";
$buttInsLink .= "\r\n";
