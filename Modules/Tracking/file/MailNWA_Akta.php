<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "Deed of Establishment and Letter of Approval from Kemenkumham completed";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        We enclose a completed copy of the Deed of Establishment and Letter of Approval from the Ministry of Law and Human Rights of your company, $Company_Name.
        <br><br>
        The next stage is the registration of your company's NPWP. We will periodically send the completed documents via email.
        <br><br>
        If there is further information, we will convey it to you.
        <br><br>
        Thank You.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! Deed of Establishment and Letter of Approval from Kemenkumham has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Thank You.

Warm regards,
*Invest In Asia team*
";

} else {
    $Email_Subject = "✅ Update Process - Akta dan SK Menkumham  $Company_Name Selesai";
    $Email_Body = "<DIV style='max-width: 500px'><P>Kepada Yth,</P>
        <P>".$Company_Name."</P>
        <P>Salam dari IZIN CO ID!</P>

        <P>Melalui notifikasi ini, Kami informasikan bahwa Akta pendirian dan pengesahan dari Kemenkumham untuk PT ".$Company_Name." telah selesai dan sudah bisa diunduh.</P>

        <P>Untuk mengunduh soft file dokumen izin Anda, silakan klik <a href='https://tracking.izin.co.id'>traking.izin.co.id</a> dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya.</P>

        <P>Proses selanjutnya adalah <b>pengurusan NPWP Perusahaan</b>. Silakan cek proses perizinan di sistem tracking kami, satu-satunya di Indonesia.</P>
        <p>Terima kasih atas kepercayaan Anda menggunakan layanan <A HREF='https://izin.co.id'>IZIN.co.id</A></P>. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.</p>

        <P>Salam Hangat, <BR/>
        <A HREF='https://izin.co.id'>IZIN.co.id</A></P>

        <BR/><HR>
        <P class='text-muted'><i>Pesan ini adalah pesan otomatis - Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
        </i></P>
        </DIV>
        ";

$WA_Message = "Dear Customer,

Salam dari IZIN CO ID.

Kami ingin menginformasikan bahwa akta pendirian dan pengesahan Kemenkumham perusahaan anda telah selesai. Proses selanjutnya adalah pengurusan NPWP Perusahaan.

Kunjungi https://tracking.izin.co.id dengan memasukan kode Tracking ID melalui website dan log in password yang telah dikirimkan pada pesan sebelumnya untuk mendownload file legalitas Perusahaan anda.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan layanan yang terbaik untuk anda.

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62_ _822-9998-0011/cs@izin.co.id_
";

}