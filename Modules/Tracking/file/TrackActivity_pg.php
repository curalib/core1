
<!--===========TRACK ACTIVITY=========================================================-->


                    <div class="container-fluid page__heading-container">
                        <div class="page__heading d-flex align-items-center">
                            <div class="flex">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item"><a href="<?php echo $bchref?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101?></a></li>
                                        <li class="breadcrumb-item"><?php echo $lang_50010?></li>
                                        <li class="breadcrumb-item active" aria-current="page"><?php echo $lang_50400?></li>
                                    </ol>
                                </nav>
                                <h1 class="m-0"><?php echo $lang_50400?></h1>
                            </div>
                        </div>
                    </div>



                    <div class="container-fluid page__container">


                        <?php

                        if(isset($_SESSION["AlertMess"]) OR !empty($_SESSION["AlertMess"])){
                            echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
                                echo '<i class="material-icons mr-3">error_outline</i>';
                                echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
                            echo '</div>';
                            //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                            unset($_SESSION["AlertMess"]);
                        }

                        ?><!-- -->



                         <div class="row">
                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header card-header-large bg-white d-flex align-items-center">
                                        <h4 class="card-header__title flex m-0">Recent Activity</h4>
<!--
                                        <div data-toggle="flatpickr" data-flatpickr-wrap="true" data-flatpickr-static="true" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y">
                                            <a href="javascript:void(0)" class="link-date" data-toggle>13/03/2018 <span class="text-muted mx-1">to</span> 20/03/2018</a>
                                            <input class="d-none" type="hidden" value="13/03/2018 to 20/03/2018" data-input>
                                        </div>
-->

                                        <form action="" method="post" name="UpdateRecentActivity" id ="UpdateRecentActivity" enctype="multipart/form-data">
                                            <input type="hidden" name="task" />
                                            <?php
                                            echo '<input id="react_range" name="react_range" value="" type="text" class="flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
                                            ?>
                                        </form>



                                    </div>
                                    <div class="card-header card-header-tabs-basic nav" role="tablist">
                                        <a href="#activity_all" class="active" data-toggle="tab" role="tab" aria-controls="activity_all" aria-selected="true">All</a>
                                        <a href="#activity_waiting" data-toggle="tab" role="tab" aria-controls="activity_waiting" aria-selected="false">Waiting</a>
                                        <a href="#activity_ongoing" data-toggle="tab" role="tab" aria-controls="activity_ongoing" aria-selected="false">On Going</a>
                                        <a href="#activity_done" data-toggle="tab" role="tab" aria-controls="activity_done" aria-selected="false">Done</a>
                                    </div>
                                    <div class="list-group tab-content list-group-flush">

                                        <div class="tab-pane active show fade" id="activity_all">
                                            <?php
                                            while( $RecActAllTrack = $dbs->getAssoc($rcRecActAllTrack)) {
                                              if($RecActAllTrack['trkitm_status'] != 0) {
                                            ?>

                                            <div class="list-group-item list-group-item-action d-flex align-items-center ">
                                                <div class="flex">
                                                <small class="text-muted">[<?php echo $RecActAllTrack["tgl_update_itm"] ?>] - <?php echo $RecActAllTrack["kode"] ?> : <?php echo $RecActAllTrack["reason_title"] ?></small>
                                                    <div class="d-flex align-items-middle">
                                                        <div class="avatar avatar-xxs mr-1">
                                                            <?php
                                                            //$cFindAvatar = "SELECT `avatar`, `nama`, `email` FROM `izin_apps_client` WHERE `kode` = '".$_SESSION['kodeClient']."' ";
                                                            //$rcFindAvatar = $dbs->getQuery($cFindAvatar);
                                                            //$FindAvatar = $dbs->getAssoc($rcFindAvatar);

                                                            $showthisavatar = $RecActAllTrack["avatar"];
                                                            $usethispath = "../../".UPDIR."/avatar/";
                                                            ?>
                                                            <img src='data:image/jpg;base64,<?php echo base64_encode(ShowImage($usethispath,$showthisavatar)) ?>' class="avatar-img rounded-circle" alt="Avatar">
                                                            <!--<img src="assets/images/256_rsz_1andy-lee-642320-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">width="32" -->
                                                        </div>
                                                        <strong class="text-15pt mr-1"><?php echo $RecActAllTrack["updater_name"] ?></strong><!-- lgndetail id_login nama_asli-->
                                                    </div>
                                                </div>
                                                <!--<span class="badge badge-warning">-->
                                                <?php
                                                    echo show_badge($RecActAllTrack['trkitm_status'], BADGE_STATUS);
                                                ?>
                                              <!--</span>-->
                                            </div>
                                            <?php
                                                  }
                                                }
                                            ?>

                                        </div>






                                        <div class="tab-pane" id="activity_waiting">

                                          <?php
                                          while( $RecActWaitTrack = $dbs->getAssoc($rcRecActWaitTrack)) {
                                          ?>

                                          <div class="list-group-item list-group-item-action d-flex align-items-center ">
                                              <div class="flex">
                                              <small class="text-muted">[<?php echo $RecActWaitTrack["tgl_update_itm"] ?>] - <?php echo $RecActWaitTrack["kode"] ?> : <?php echo $RecActWaitTrack["reason_title"] ?></small>
                                                  <div class="d-flex align-items-middle">
                                                      <div class="avatar avatar-xxs mr-1">
                                                          <?php
                                                          //$cFindAvatar = "SELECT `avatar`, `nama`, `email` FROM `izin_apps_client` WHERE `kode` = '".$_SESSION['kodeClient']."' ";
                                                          //$rcFindAvatar = $dbs->getQuery($cFindAvatar);
                                                          //$FindAvatar = $dbs->getAssoc($rcFindAvatar);

                                                          $showthisavatar = $RecActWaitTrack["avatar"];
                                                          $usethispath = "../../".UPDIR."/avatar/";
                                                          ?>
                                                          <img src='data:image/jpg;base64,<?php echo base64_encode(ShowImage($usethispath,$showthisavatar)) ?>' class="avatar-img rounded-circle" alt="Avatar">
                                                          <!--<img src="assets/images/256_rsz_1andy-lee-642320-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">width="32" -->
                                                      </div>
                                                      <strong class="text-15pt mr-1"><?php echo $RecActWaitTrack["updater_name"] ?></strong><!-- lgndetail id_login nama_asli-->
                                                  </div>
                                              </div>
                                              <!--<span class="badge badge-warning">-->
                                              <?php
                                                  echo show_badge($RecActWaitTrack['trkitm_status'], BADGE_STATUS);
                                              ?>
                                            <!--</span>-->
                                          </div>
                                          <?php
                                              }
                                          ?>

                                        </div>


                                        <div class="tab-pane" id="activity_ongoing">
                                          <?php
                                          while( $RecActOGTrack = $dbs->getAssoc($rcRecActOGTrack)) {
                                          ?>

                                          <div class="list-group-item list-group-item-action d-flex align-items-center ">
                                              <div class="flex">
                                              <small class="text-muted">[<?php echo $RecActOGTrack["tgl_update_itm"] ?>] - <?php echo $RecActOGTrack["kode"] ?> : <?php echo $RecActOGTrack["reason_title"] ?></small>
                                                  <div class="d-flex align-items-middle">
                                                      <div class="avatar avatar-xxs mr-1">
                                                          <?php
                                                          //$cFindAvatar = "SELECT `avatar`, `nama`, `email` FROM `izin_apps_client` WHERE `kode` = '".$_SESSION['kodeClient']."' ";
                                                          //$rcFindAvatar = $dbs->getQuery($cFindAvatar);
                                                          //$FindAvatar = $dbs->getAssoc($rcFindAvatar);

                                                          $showthisavatar = $RecActOGTrack["avatar"];
                                                          $usethispath = "../../".UPDIR."/avatar/";
                                                          ?>
                                                          <img src='data:image/jpg;base64,<?php echo base64_encode(ShowImage($usethispath,$showthisavatar)) ?>' class="avatar-img rounded-circle" alt="Avatar">
                                                          <!--<img src="assets/images/256_rsz_1andy-lee-642320-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">width="32" -->
                                                      </div>
                                                      <strong class="text-15pt mr-1"><?php echo $RecActOGTrack["updater_name"] ?></strong><!-- lgndetail id_login nama_asli-->
                                                  </div>
                                              </div>
                                              <!--<span class="badge badge-warning">-->
                                              <?php
                                                  echo show_badge($RecActOGTrack['trkitm_status'], BADGE_STATUS);
                                              ?>
                                            <!--</span>-->
                                          </div>
                                          <?php
                                              }
                                          ?>
                                        </div>


                                        <div class="tab-pane" id="activity_done">
                                          <?php
                                          while( $RecActDoneTrack = $dbs->getAssoc($rcRecActDoneTrack)) {
                                          ?>

                                          <div class="list-group-item list-group-item-action d-flex align-items-center ">
                                              <div class="flex">
                                              <small class="text-muted">[<?php echo $RecActDoneTrack["tgl_update_itm"] ?>] - <?php echo $RecActDoneTrack["kode"] ?> : <?php echo $RecActDoneTrack["reason_title"] ?></small>
                                                  <div class="d-flex align-items-middle">
                                                      <div class="avatar avatar-xxs mr-1">
                                                          <?php
                                                          //$cFindAvatar = "SELECT `avatar`, `nama`, `email` FROM `izin_apps_client` WHERE `kode` = '".$_SESSION['kodeClient']."' ";
                                                          //$rcFindAvatar = $dbs->getQuery($cFindAvatar);
                                                          //$FindAvatar = $dbs->getAssoc($rcFindAvatar);

                                                          $showthisavatar = $RecActDoneTrack["avatar"];
                                                          $usethispath = "../../".UPDIR."/avatar/";
                                                          ?>
                                                          <img src='data:image/jpg;base64,<?php echo base64_encode(ShowImage($usethispath,$showthisavatar)) ?>' class="avatar-img rounded-circle" alt="Avatar">
                                                          <!--<img src="assets/images/256_rsz_1andy-lee-642320-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">width="32" -->
                                                      </div>
                                                      <strong class="text-15pt mr-1"><?php echo $RecActDoneTrack["updater_name"] ?></strong><!-- lgndetail id_login nama_asli-->
                                                  </div>
                                              </div>
                                              <!--<span class="badge badge-warning">-->
                                              <?php
                                                  echo show_badge($RecActDoneTrack['trkitm_status'], BADGE_STATUS);
                                              ?>
                                            <!--</span>-->
                                          </div>
                                          <?php
                                              }
                                          ?>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                            <div class="card">
                            <div class="card-header card-header-large bg-white">
                                <h4 class="card-header__title">Current Status</h4>
                            </div>
                            <form action="" method="post" name="Filter_By" id ="Filter_By">
                            <div class="card-header">
                                    <input type="hidden" name="task" />
                                    <?php
                                    if(isset($date_range) && !empty($date_range)) {
                                        echo '<input type="hidden" name="filter_date" value="'.$date_range.'" />';
                                    } else {
                                        echo '<input type="hidden" name="filter_date" value="" />';
                                    }
                                    ?>
                                    <div class="row">

                                        <div class="col-sm-auto flex">
                                            <label class="mr-sm-2" for="filter_by_name">Filter by:</label>
                                    <?php
                                    if(isset($filter_by_name) && !empty($filter_by_name)) {
                                        echo '<input name="filter_by_name" id="filter_by_name" value="'.$filter_by_name.'" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Type a name">';
                                    } else {
                                        echo '<input name="filter_by_name" id="filter_by_name" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Type a name">';
                                    }
                                    ?>
                                            <!--<input name="filter_by_name" id="filter_by_name" type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" placeholder="Type a name">-->
                                        </div>

                                        <div class="col-sm-auto flex wiz_ts-25">
                                    <label class="sr-only" for="track_status">Role</label>
                                    <select name="track_status" id="track_status" class="custom-select mb-2 mr-sm-2 mb-sm-0">

                                    <?php
                                    //if(isset($track_status) && !empty($track_status)) {
                                    if(isset($track_status)) {



                                        switch ($track_status) {
                                            case '3':
                                                echo '
                                                  <option value="3" selected>Done</option>
                                                  <option value="0">On Progress</option>
                                                  <option value="2">Waiting</option>
                                                ';
                                                break;
                                            case '0':
                                                echo '
                                                  <option value="3">Done</option>
                                                  <option value="0" selected>On Progress</option>
                                                  <option value="2">Waiting</option>
                                                ';
                                                break;
                                            case '2':
                                                echo '
                                                  <option value="3">Done</option>
                                                  <option value="0">On Progress</option>
                                                  <option value="2" selected>Waiting</option>
                                                ';
                                                break;
                                            default:
                                                echo "how did we get here?";
                                        }



                                    } else {
                                    echo '
                                      <option value="3">Done</option>
                                      <option value="0">On Progress</option>
                                      <option value="2">Waiting</option>
                                    ';
                                    }

                                    ?>

                                    </select>
                                        </div>

                                    </div>


                            </div>
                            </form>

                            <div style="border:10px solid white;" class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                <table class="table mb-0 thead-border-top-0">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th style="width: 150px;">Client</th>
                                            <th style="width: 148px;">Nama PT</th>
                                        </tr>
                                    </thead>
                                    <tbody  class="list" id="staff">

                                      <?php
                                      while( $CurStatTrack = $dbs->getAssoc($rcCurStatTrack)) {
                                      ?>
                                        <tr >
                                            <td><?php echo $CurStatTrack["tracking_id"] ?></td>
                                            <td><?php echo $CurStatTrack["client_name"] ?></td>
                                            <td><?php echo $CurStatTrack["company_name"] ?></td>
                                        </tr>
                                      <?php
                                          }
                                      ?>


                                    </tbody>
                                </table>
                                </div>

                                <div class="card-body text-right">
                                15 <span class="text-muted">of 1,430</span> <a href="#" class="text-muted-light"><i class="material-icons ml-1">arrow_forward</i></a>
                                </div>
                            </div>

                            </div>

                        </div>
                      </div>


                      <script>
                      $(document).ready(function() {

                          var now_date = moment().format("DD/MM/YYYY");
                          var amonth_ago = moment().subtract(10, 'days').format("DD/MM/YYYY");

                          $("#react_range").flatpickr({
                              mode: "range",
                              altInput: true,
                              altFormat: "d/m/Y",
                              dateFormat: "d/m/Y",
                              allowInput: false,
                              <?php
                              if(isset($date_fromS) && !empty($date_fromS)) {
                                  echo 'defaultDate: ["'.$date_fromS.'", "'.$date_toS.'"],';
                              } else {
                                  echo 'defaultDate: [amonth_ago, now_date],';
                              }
                              // onChange onClose onValueUpdate
                              ?>
                              onClose: function(selectedDates, dateStr, instance) {
                                  $(this).updateReAct("UpdateRecentActivity");
                              }
                              //event.preventDefault();
                          });

                          $.fn.updateReAct = function(form_to_send) {
                              var act_upReAct = form_to_send+"_<?php echo $_SESSION["butts_Update"] ?>";
                              submitbutton(act_upReAct,form_to_send);
                          }

                          $("#track_status").on("change", function() {
                              $(this).updateReAct("Filter_By");
                          });
                          $('#filter_by_name').on('keypress', function (e) {
                                  if(e.which === 13){
                                     //Disable textbox to prevent multiple submit
//                                     $(this).attr("disabled", "disabled");
                                     $(this).updateReAct("Filter_By");
                                     //Enable the textbox again if needed.
//                                     $(this).removeAttr("disabled");
                                  }
                          });

                          $("[data-time-format]").each(function() {
                              var el = $( this );
                              switch(el.attr("data-time-format")) {
                                  case "time-ago":
                                    var timeValue = el.attr("data-time-value");
                                    var strTimeAgo = moment(timeValue).fromNow();
                                    el.text(strTimeAgo);
                                  break;
                              }
                          });//$( "[data-time-format]" ).each(function() {
                      });
                      </script>




<!--===========END of TRACK ACTIVIY=========================================================-->
