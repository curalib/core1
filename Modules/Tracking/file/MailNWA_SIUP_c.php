<?php

$Email_Subject = "✅ Update Process - SIUP ".$Company_Name;

$Email_Body = "<DIV style='max-width: 500px'><P>Dear Client,</P>

<P>Salam dari IZIN CO ID!</P>

<P>Kami ingin menginformasikan bahwa SIUP (Surat Izin Usaha Perdagangan) Perusahaan anda telah selesai.
Proses selanjutnya adalah pengurusan NIB (Nomor Induk Berusaha)</P>

<P>Kunjungi <a href='https://tracking.izin.co.id'>https://tracking.izin.co.id</a> dengan memasukan kode Tracking ID melalui website dan log in
password yang telah dikirimkan di email sebelumnya untuk mendownload file legalitas Perusahaan
anda.</P>

<P>Terima kasih karena telah menggunakan layanan <a href='https://izin.co.id'>izin.co.id</a> dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.</P>

<P>Sincerely, <BR/>
<A HREF='https://izin.co.id'>IZIN.co.id</A></P>

<BR/><HR>
<P class='text-muted'><i>This is an automated message please do not reply directly to this email. For further
information you can contact us through our whatsapp/email +62 822-9998-0011 / cs@izin.co.id</i></P>
</DIV>
";

$Email_Alt_Body = "Dear Client,

Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa SIUP (Surat Izin Usaha Perdagangan) Perusahaan anda telah selesai.
Proses selanjutnya adalah pengurusan NIB (Nomor Induk Berusaha)</P>

Kunjungi <a href='https://tracking.izin.co.id'>https://tracking.izin.co.id</a> dengan memasukan kode Tracking ID melalui website dan
log in password yang telah dikirimkan di email sebelumnya untuk mendownload file legalitas
Perusahaan anda.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan
layanan yang terbaik untuk anda.

Sincerely,
IZIN.co.id

______________________________________________________________________________________

This is an automated message please do not reply directly to this email. For further
information you can contact us through our whatsapp/email +62 822-9998-0011 / cs@izin.co.id
";

$WA_Message = "Dear Client,

Salam dari IZIN CO ID!

Kami ingin menginformasikan bahwa SIUP (Surat Izin Usaha Perdagangan) Perusahaan anda telah selesai. Proses selanjutnya adalah pengurusan NIB (Nomor Induk Berusaha).

Kunjungi https://tracking.izin.co.id dengan memasukan kode Tracking ID melalui website dan log in password yang telah dikirimkan di email sebelumnya untuk mendownload file legalitas Perusahaan anda.

Terima kasih karena telah menggunakan layanan izin.co.id dan kami akan berusaha memberikan layanan yang terbaik untuk anda.

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62_ _822-9998-0011/cs@izin.co.id_
";

?>
