<?php

$Email_Subject = "✅ Update Process - SIUP  ".$Company_Name." Selesai";

$Email_Body = "<DIV style='max-width: 500px'>
<P>Kepada Yth,</P>

<P>Salam dari IZIN.co.id!</P>

<P>Melalui notifikasi ini, Kami informasikan bahwa SIUP (Surat Izin Usaha Perdagangan) untuk ".$Company_Name." telah selesai dan sudah bisa diunduh. </P>

<P>Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya.</P>

<P>Terima kasih atas kepercayaan Anda menggunakan layanan IZIN.co.id. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.</P>

<P>Salam Hangat,<BR/>
<A HREF='https://izin.co.id'>Tim IZIN.co.id</A></P>

<BR/><HR>
<P class='text-muted'><i>Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id</i></P>
</DIV>
";

$Email_Alt_Body = "Kepada Yth,

Salam dari IZIN.co.id!

Melalui notifikasi ini, Kami informasikan bahwa SIUP (Surat Izin Usaha Perdagangan) untuk ".$Company_Name." telah selesai dan sudah bisa diunduh.

Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya.


Terima kasih atas kepercayaan Anda menggunakan layanan IZIN.co.id. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.


Salam Hangat,
Tim IZIN.co.id

______________________________________________________________________________________

Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
";

$WA_Message = "Kepada Yth,

Salam dari IZIN.co.id!

Melalui notifikasi ini, Kami informasikan bahwa SIUP (Surat Izin Usaha Perdagangan) untuk ".$Company_Name." telah selesai dan sudah bisa diunduh.

Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya.

Terima kasih atas kepercayaan Anda menggunakan layanan IZIN.co.id. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.

Salam Hangat,
*Tim IZIN.co.id*

__________________________________________________________________________

Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
";

?>
