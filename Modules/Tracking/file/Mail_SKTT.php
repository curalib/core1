<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "SKTT (Surat Keterangan Tempat Tinggal)";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        Congratulation, Your SKTT (Surat Keterangan Tempat Tinggal) for $Company_Name have been completed.
        <br><br>
        This permit can be used to work in Indonesia and at valid for 1 year.
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

}