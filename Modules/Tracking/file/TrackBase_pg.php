<!--===========FINISH PERMIT LIST=========================================================-->

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="<?php echo $bchref ?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101
                                                                                                                            ?></a></li>
                    <li class="breadcrumb-item"><?php echo $lang_50010 ?></li>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo $lang_50300 ?></li>
                </ol>
            </nav>
            <h1 class="m-0"><?php echo $lang_50300 ?></h1>
        </div>
    </div>
</div>

<div class="container-fluid page__container">
    <?php
    if (isset($_SESSION["AlertMess"]) or !empty($_SESSION["AlertMess"])) {
        echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
        echo '<i class="material-icons mr-3">error_outline</i>';
        echo '<div class="text-body"><strong>Alert - </strong> ' . $_SESSION["AlertMess"] . '.</div>';
        echo '</div>';
        //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
        unset($_SESSION["AlertMess"]);
    }
    ?>
    <div class="card">
        <div class="card-header card-header-large bg-white">
            <small class="text-muted"><?php echo $d->data_info(); ?></small>
        </div>
        <div class="card-header">
            <form action="" method="post" name="FinishedTrackSrcFrm" id="FinishedTrackSrcFrm" onkeydown="if(event.keyCode === 13) { return false; }">
                <input type="hidden" name="task" />
                <div class="form-row align-items-center">
                    <div class="form-group col-md-3">
                        <label for="filter_name">Filter by:</label>
                        <input id="filter_name" name="filter_name" type="text" class="form-control" value="<?php echo isset($_SESSION["TrackingFinishSearch"]) ? $_SESSION["TrackingFinishSearch"] : ""  ?>" placeholder="Enter ID or name of client or company">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="filter_date">Range of Close Date</label>
                        <input id="filter_date" name="filter_date" type="text" class="form-control" placeholder="Select date range ..." data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y" value="<?php echo isset($date_range) ? $date_range : ""  ?>">
                    </div>
                    <div class="col-auto" style="margin-top:10px">
                        <?= ButtonsCommon("commonbuttons", THEMES, "Cari_i", $lang_2030, "Search_" . $_SESSION["butts_Src"], "FinishedTrackSrcFrm", "", "right") ?>
                    </div>
                    <?php if ((isset($_SESSION["TrackingFinishSearch"]) && $_SESSION["TrackingFinishSearch"] != '') || isset($date_range)) : ?>
                        <div class="col-auto" style="margin-top:10px">
                            <?= ButtonsCommon("commonbuttons", THEMES, "Reset_i", "Clear Search", "Search_" . $_SESSION["butts_Reset"], "FinishedTrackSrcFrm", "", "right") ?>
                        </div>
                    <?php endif; ?>
                    <div class="col-auto" style="margin-top:10px">
                        <?= ButtonsCommon("commonbuttons", THEMES, "Excell_i", "Export to Excel", "ExportToXLS_" . $_SESSION["butts_Enter"], "FinishedTrackSrcFrm", "", "right") ?>
                    </div>
                </div>
            </form>
        </div>
        <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
            <table class="table mb-0 thead-border-top-0">
                <thead>

                    <tr>
                        <!--
                                                    <th style="width: 18px;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
                                                            <label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
                                                        </div>
                                                    </th>
                                                    <th style="width: 270px;">Nama Instansi</th>
                                                    <th style="width: 148px;">Mobile / WA</th>
                                                    <th style="width: 148px;">Email</th>
                                                    <th style="width: 120px;">Last Update</th>
                                                    <th style="width: 200px;">Status</th>
                                                    <th style="width: 24px;"></th>
                                                  -->

                        <th style="width: 9%;">
                            <div class="wiz_ls-20">ID</div>
                        </th>
                        <th style="width: 16%;">Client</th>

                        <th style="width: 21%;">Product</th>
                        <th style="width: 20%;">Nama PT</th>
                        <th style="width: 10%;">Start Date</th>
                        <th style="width: 10%;">Close Date</th>
                        <th style="width: 8%;">Restore</th>
                        <!--<th style="width: 13%;">Last Activity</th>-->

                    </tr>
                </thead>
                <tbody class="list" id="staff">

                    <?php
                    //while($ViewTrack = $dbs->getAssoc($rcViewTrack)) {
                    while ($ViewTrack = $d->result_assoc()) {
                    ?>
                        <tr>
                            <!--
                                                      <td>
                                                          <div class="custom-control custom-checkbox">
                                                              <input name="selectTrack[]" id="<?php //echo 'chkbox_'.$chkbox
                                                                                                ?>" value="<?php //echo $ViewTrack['id_trklist']
                                                                                                            ?>" type="checkbox" class="custom-control-input js-check-selected-row">
                                                              <label class="custom-control-label" for="<?php //echo 'chkbox_'.$chkbox
                                                                                                        ?>"><span class="text-hide">Check</span></label>
                                                          </div>
                                                      </td>
                                                    -->
                            <!--<td><?php //echo $ViewTrack['client_id']
                                    ?></td>-->
                            <td>
                                <form action="" method="post" name="TrackView_<?php echo $ViewTrack['tracking_id']; ?>" id="TrackView_<?php echo $ViewTrack['tracking_id']; ?>">
                                    <input type="hidden" name="task" />
                                    <input type="hidden" name="tracking_id" value="<?php echo $ViewTrack['tracking_id']; ?>">
                                </form>
                                <div class="media align-items-center wiz_ls-20">
                                    <a href="javascript:submitbutton('TrackView_<?php echo $_SESSION['butts_Enter'] ?>','TrackView_<?php echo $ViewTrack['tracking_id'] ?>');"><?php echo $ViewTrack['tracking_id'] ?></a>

                                </div>
                            </td>
                            <td><?php echo $ViewTrack['client_name'] ?></td>
                            <td><?php echo $ViewTrack['pck_nama'] ?></td>
                            <td><?php echo $ViewTrack['company_name'] ?></td>
                            <td>
                                <small class="text-muted"><?php $start_date = strtotime($ViewTrack['tgl_start']);
                                                            echo date("d-m-Y", $start_date); ?></small>
                            </td>
                            <td>
                                <small class="text-muted"><?php $close_date = strtotime($ViewTrack['tgl_close']);
                                                            echo date("d-m-Y", $close_date); ?></small>
                            </td>

                            <td class="text-center">
                                <form action="" method="post" name="RestoreTracking_<?php echo $ViewTrack['tracking_id'] ?>" id="RestoreTracking_<?php echo $ViewTrack['tracking_id'] ?>">
                                    <input type="hidden" name="task" />
                                    <input type="hidden" name="tracking_id" value="<?php echo $ViewTrack['tracking_id']; ?>">
                                </form>
                                <a href="javascript:submitbutton('RestoreTracking_<?php echo $_SESSION["butts_Enter"] ?>','RestoreTracking_<?php echo $ViewTrack['tracking_id'] ?>');" class="btn btn-success btn-sm"><i class="material-icons icon-14pt">undo</i></a>
                            </td>

                            <!--
                                                      <td>
                                                          <td><?php echo $ViewTrack['kode'] ?></td>
                                                          <small class="text-muted"><span data-time-format="time-ago" data-time-value="<?php echo $ViewTrack['tgl_update_itm'] ?>"></span></small>
                                                      </td>
                                                      <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                    -->
                        </tr>
                    <?php
                        //$chkbox++;
                    }
                    ?>








                    <!--
                                                <tr class="selected">
                                                    <td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input js-check-selected-row"  id="customCheck1_1">
                                                            <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                                                        </div>
                                                    </td>

                                                    <td>12</td>
                                                    <td>
                                                        <div class="media align-items-center">
                                                            <a href="lead-info.php">Budi</a>
                                                            <a href="#" class="rating-link"></a>
                                                        </div>
                                                    </td>
                                                    <td>PT Budi Sejahtera</td>
                                                    <td>087172871827</td>
                                                    <td>budi@budisejahera.com</td>
                                                    <td><small class="text-muted">3 days ago</small></td>
                                                    <td>Renewal</td>

                                                    <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>

                                                </tr>
                                                -->

                </tbody>
            </table>
        </div>
        <!--
                                    <div class="card-body text-right">
                                        15 <span class="text-muted">of 1,430</span> <a href="#" class="text-muted-light"><i class="material-icons ml-1">arrow_forward</i></a>
                                    </div>
-->
    </div>
    <?php
    echo $d->print_page(NULL);
    echo $d->form_print_page();
    ?>

</div>

<script>
    $(document).ready(function() {
        $.fn.updateLineChart = function() {
            var act_upLineChart = "Search_<?php echo $_SESSION["butts_Reset"] ?>";
            var forms = "SearchByRange";
            submitbutton("Search_<?php echo $_SESSION["butts_Reset"] ?>", "SearchByRange");
        }

        $("[data-time-format]").each(function() {
            var el = $(this);
            switch (el.attr("data-time-format")) {
                case "time-ago":
                    var timeValue = el.attr("data-time-value");
                    var strTimeAgo = moment(timeValue).fromNow();
                    el.text(strTimeAgo);
                    break;
            }
        });
    });
</script>

<!--===========END of FINISH PERMIT LIST==================================================-->