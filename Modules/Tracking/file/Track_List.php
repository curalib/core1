<style media="screen">
    .trash-icon:hover {
        color: red;
    }
</style>
<!--===========DATA TRACKING=========================================================-->

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="<?php echo $bchref ?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101
                                                                                                                            ?></a></li>
                    <li class="breadcrumb-item"><?php echo $lang_50010 ?></li>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo $lang_50100 ?></li>
                </ol>
            </nav>
            <h1 class="m-0"><?php echo $lang_50100 ?></h1>
        </div>

        <?php
        if ($allowAdd) {

        ?>
            <form action="" method="post" name="Add_new_track" id="Add_new_track">
                <input type="hidden" name="task" />
            </form>
            <a href="javascript:submitbutton('AddTrackPage_<?php echo $_SESSION["butts_Add"] ?>','Add_new_track');" class="btn btn-success ml-3"><?php echo $lang_50110 ?></a>
        <?php
        }
        //echo $buttAddLink;
        ?>
    </div>
</div>


<div class="container-fluid page__container">
    <?php
    if (isset($_SESSION["AlertMess"]) or !empty($_SESSION["AlertMess"])) {
        echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
        echo '<i class="material-icons mr-3">error_outline</i>';
        echo '<div class="text-body"><strong>Alert - </strong> ' . $_SESSION["AlertMess"] . '.</div>';
        echo '</div>';
        //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
        unset($_SESSION["AlertMess"]);
    }
    ?>
    <div class="card">
        <div class="card-header card-header-large bg-white">
            <small class="text-muted"><?php echo $d->data_info(); ?></small>
        </div>
        <div class="card-header">
            <form action="" method="post" name="TrackSrcFrm" id="TrackSrcFrm" onkeydown="if(event.keyCode === 13) { return false; }">
                <input type="hidden" name="task" />
                <div class="form-row align-items-center">
                    <div class="form-group col-md-3">
                        <label for="filter_name">Filter by:</label>
                        <input id="filter_name" name="filter_name" type="text" class="form-control" value="<?php echo isset($_SESSION["TrackingSearch"]) ? $_SESSION["TrackingSearch"] : ""  ?>" placeholder="Enter ID or name of client or company">
                    </div>
                    <div class="col-auto" style="margin-top:10px">
                        <?= ButtonsCommon("commonbuttons", THEMES, "Cari_i", $lang_2030, "Search_" . $_SESSION["butts_Src"], "TrackSrcFrm", "", "right") ?>
                    </div>
                    <?php if (isset($_SESSION["TrackingSearch"]) && $_SESSION["TrackingSearch"] != '') : ?>
                        <div class="col-auto" style="margin-top:10px">
                            <?= ButtonsCommon("commonbuttons", THEMES, "Reset_i", "Clear Search", "Search_" . $_SESSION["butts_Reset"], "TrackSrcFrm", "", "right") ?>
                        </div>
                    <?php endif; ?>
            </form>
        </div>
    </div>
    <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
        <table class="table mb-0 thead-border-top-0">
            <thead>
                <tr>
                    <!--
                                                    <th style="width: 4%;">
                                                        <div class="custom-control custom-checkbox">
                                                            <input id="customCheckAll" type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff">
                                                            <label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
                                                        </div>
                                                    </th>
-->
                    <th style="width: 9%;">
                        <div class="wiz_ls-20">ID</div>
                    </th>
                    <th style="width: 14%;">Client</th>
                    <th style="width: 22%;">Product</th>
                    <th style="width: 22%;">Nama PT</th>
                    <th style="width: 12%;">Start Date</th>
                    <th style="width: 12%;">Last Update</th>
                    <th style="width: 9%;">Last Activity</th>
                    <!--th style="width: 4%;">Action</th-->
                </tr>
            </thead>
            <tbody class="list" id="staff">
                <?php
                //while($ViewTrack = $dbs->getAssoc($rcViewTrack)) {
                while ($ViewTrack = $d->result_assoc()) {
                ?>
                    <tr>
                        <!--
                                                  <td>
                                                      <div class="custom-control custom-checkbox">
                                                          <input name="selectTrack[]" id="<?php //echo 'chkbox_'.$chkbox
                                                                                            ?>" value="<?php //echo $ViewTrack['id_trklist']
                                                                                                        ?>" type="checkbox" class="custom-control-input js-check-selected-row">
                                                          <label class="custom-control-label" for="<?php //echo 'chkbox_'.$chkbox
                                                                                                    ?>"><span class="text-hide">Check</span></label>
                                                      </div>
                                                  </td>
-->
                        <!--<td><?php //echo $ViewTrack['client_id']
                                ?></td>-->
                        <td>
                            <form action="" method="post" name="View_<?php echo $ViewTrack['tracking_id']; ?>" id="View_<?php echo $ViewTrack['tracking_id']; ?>">
                                <input type="hidden" name="task" />
                                <input type="hidden" name="tracking_id" value="<?php echo $ViewTrack['tracking_id']; ?>">
                            </form>
                            <div class="media align-items-center wiz_ls-20">
                                <!--<a href ="javascript:submitbutton('<?php //echo $_SESSION['butts_Enter']
                                                                        ?>nTrack','View_<?php //echo $ViewTrack['tracking_id']
                                                                                        ?>');"><?php //echo $ViewTrack['tracking_id']
                                                                                                ?></a>-->
                                <a href="javascript:submitbutton('Track_<?php echo $_SESSION["butts_Enter"] ?>_Detail','View_<?php echo $ViewTrack['tracking_id'] ?>');"><?php echo $ViewTrack['tracking_id'] ?></a>
                            </div>
                        </td>
                        <td><?php echo $ViewTrack['client_name'] ?></td>
                        <td><?php echo $ViewTrack['package_name'] ?></td>
                        <td><?php echo $ViewTrack['company_name'] ?></td>
                        <td>
                            <small class="text-muted"><?php $start_date = strtotime($ViewTrack['tgl_start']);
                                                        echo date("d-m-Y", $start_date); ?></small>
                        </td>
                        <td><?php echo $ViewTrack['kode'] ?></td>
                        <td>
                            <!--<small class="text-muted"><span data-time-format="time-ago" data-time-value="<?php //echo $ViewTrack['tgl_update_itm']
                                                                                                                ?>"></span></small>-->
                            <small class="text-muted">
                                <?php
                                $LastActivityInDays = $HitDays->DayRange(convertDate($ViewTrack['tgl_update_itm']), "now", false)->Result();
                                echo $LastActivityInDays[2];
                                ?>
                            </small>
                        </td>

                        <!--td>
                                <form method="post">
                                    <input type="hidden" name="id_list" value="<?= $ViewTrack['tracking_id']; ?>">
                                    <button name="delete_track" type="submit" class="btn " class="text-muted"><i class="material-icons trash-icon">delete_forever</i></a>
                                </form>
                            </td-->


                    </tr>
                <?php
                    $chkbox++;
                }
                ?>
            </tbody>
        </table>
    </div>
    <!--
  <div class="">
    <?php
    //for ($i=1; $i<=$pages ; $i++){
    ?>
    <a href="?halaman=<?php //echo $i;
                        ?>"><?php //echo $i;
                            ?></a>

    <?php //}
    ?>

  </div>
-->


    <!--
                                    <div class="card-body text-right">
                                        15 <span class="text-muted">of 1,430</span> <a href="#" class="text-muted-light"><i class="material-icons ml-1">arrow_forward</i></a>
                                    </div>
-->



</div>
<?php
echo $d->print_page(NULL);
echo $d->form_print_page();
?>
</div> <!-- <div class="container-fluid page__container"> -->

<script>
    $(document).ready(function() {
        // time formatter
        $("[data-time-format]").each(function() {
            var el = $(this);
            switch (el.attr("data-time-format")) {
                case "time-ago":
                    var timeValue = el.attr("data-time-value");
                    //        var timeValue = now();
                    var strTimeAgo = moment(timeValue).fromNow();
                    el.text(strTimeAgo);
                    break;
            }
        }); //$( "[data-time-format]" ).each(function() {

        $('#customCheckAll').click(function(event) {
            $(':checkbox').prop('checked', this.checked);
        });

    });
</script>