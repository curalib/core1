<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "RPTKA & IMTA";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        We enclose a RPTKA (Rencana Penggunaan Tenaga Kerja Asing) & IMTA (Izin Menggunakan Tenaga Kerja Asing) $Company_Name in the attachment.
        <br><br>
        This permit can be used to work in Indonesia and at valid for 1 year.
        <br><br>
        The next stage is the Visa registration. We will periodically send the completed documents via email.
        <br><br>
        If there is further information, we will convey it to you.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! RPTKA & IMTA has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Thank You.

Warm regards,
*Invest In Asia team*
";
}