<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "Permit Process On Hold ".$Company_Name;
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        We want to inform you that the process of your legal documents now unfortunately must be on hold because<br>
        ".(isset($reason_note) ? $update_status." : ".$reason_note: $update_status)."
        <br><br>
        Please complete the necessary documents so we can proceed with your company legal documents.
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br>
        Thank You.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";
} else {
    $Email_Subject = "Pending Document  ".$Company_Name;
    $Email_Body = "<DIV style='max-width: 500px'><P>Dear Client,</P>
        <P>Salam dari IZIN CO ID!</P>

        <P>Kami ingin memberitahu anda bahwa proses perizinan anda tertunda dikarenakan<BR>
        ".(isset($reason_note) ? $update_status." : ".$reason_note: $update_status)."</P>

        <P>Mohon untuk segera menyelesaikan/memberikan dokumen terkait untuk melanjutkan proses
        pembuatan perusahaan anda.</P>

        <P>Terima kasih karena telah menggunakan layanan <a href='https://izin.co.id'>IZIN.co.id</a> dan kami akan berusaha memberikan
        layanan yang terbaik untuk anda.</P>

        <P>Sincerely, <BR/>
        <A HREF='https://izin.co.id'>IZIN.co.id</A></P>

        <BR/><HR>
        <P class='text-muted'><i>This is an automated message please do not reply directly to this email. For further
        information you can contact us through our whatsapp/email +62 822-9998-0011 / cs@izin.co.id</i></P>
        </DIV>
        ";

$WA_Message = "Dear Client,

Salam dari IZIN CO ID.

Notice: Pending Document

Saat ini proses pembuatan perusahaan anda tertunda dikarenakan ".(isset($reason_note) ? $update_status." : ".$reason_note: $update_status)."

Untuk informasi lebih lanjut dapat menghubungi kami melalui whatsapp di +62 822-9998-0011 atau cs@izin.co.id

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62_ _822-9998-0011/cs@izin.co.id_
";

}