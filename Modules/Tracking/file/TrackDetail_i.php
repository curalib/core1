<?php



//BREADCRUMB
//-------------------------------------------------
$cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL . $bcsplitKeys . "/0/0.html";

//BREADCRUMB TRACKING
//-------------------------------------------------
$linkTarget = "DtTrac_pg";
$cBrdCrumb = "SELECT tgl FROM `izin_sys_menu` WHERE `module` = 'Tracking' AND `page` = '" . $linkTarget . "'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bcTrack = BASEURL . $bcsplitKeys . "/0/0.html";


/*
//LINK TO TIMELINE!!!!
//-------------------------------------------------
$Timelinepage = '50140';
$cbtntimeline = "SELECT `id`, `id_parent`, `tgl` FROM `izin_sys_menu` WHERE `publish`='Y' AND `nama`='".$Timelinepage."'";
$rcbtntimeline = $dbs->getQuery($cbtntimeline);
$btntimeline = $dbs->getAssoc($rcbtntimeline);
$splitKeys = SplitKeys($btntimeline['tgl']);
$linksys = BASEURL.$splitKeys."/0/0.html";
$TimelineLink = $linksys;
//echo '<a href="'.$linksys.'" class="btn btn-warning ml-3">'.$lang_80411.'</a>';
//$buttTimeLink = '<a href="'.$linksys.'" class="btn btn-danger ml-3"><i class="material-icons mr-1">access_time</i>'.$lang_50140.'</a>';
*/


//BUTTON VIEW TIMELINE
//-------------------------------------------------
$act_timeline = "ShowTimeline_" . $_SESSION["butts_Enter"];
$forms = "ViewTimeline";
$name = $lang_50140;
$buttTimeLink = "";
$buttTimeLink .= "													";
$buttTimeLink .= "<a href=\"javascript:submitbutton('" . $act_timeline . "','" . $forms . "');\" id=\"" . $act_timeline . "\" class=\"btn btn-danger ml-3\"><i class=\"material-icons mr-1\">access_time</i>" . $name . "</a>";
$buttTimeLink .= "\r\n";

//GET SESSION ID TRACKING
//-------------------------------------------------
//$view_trackingid = $tracking_id;
if (isset($_POST['tracking_id'])) {
    $tracking_id = Purefy($_POST['tracking_id']);
    $_SESSION["showtrackingid"] = $tracking_id;
} else if (isset($_SESSION["showtrackingid"])) {
    $tracking_id = $_SESSION["showtrackingid"];
} else {
    //error or direct access !!!!
    $_SESSION["AlertMess"] = $lang_80457;
}


//if(!isset($_SESSION["AlertMess"])) {

//GET DIRUT DETAIL
//-------------------------------------------------
$cFindDirut = "SELECT `track`.`reminder_tax`,`track`.`reminder_haki`,`track`.`reminder_akta_in`,`track`.`reminder_akta`,`track`.`reminder_lkpm`,`track`.`id_trklist`,`track`.`tracking_id`,`track`.`trk_password`, `lead`.`client_id`,`lead`.`client_email`,`track`.`reminder_tax_0`, `track`.`reminder_tax_1`, `track`.`reminder_tax_2`, `track`.`reminder_tax_3`, `track`.`reminder_tax_4`, `track`.`reminder_tax_5`, ";
$cFindDirut .= "`track`.`ultah_perusahaan`,`track`.`ultah_direktur`,`track`.`company_name`,`lead`.`id_lead`,`lead`.`dirut`, `lead`.`tgl_lhr_dirut`,  ";
$cFindDirut .= "`track`.`reminder_haki_merek`, `track`.`reminder_haki_kelas`, `track`.`reminder_haki_link`, ";
$cFindDirut .= "CASE WHEN EXISTS (SELECT * FROM `track_auth_identities` WHERE `type` = 'email_password' AND `secret` = `lead`.`client_email`) then 1 ELSE 0 END AS isActivated ";
$cFindDirut .= "FROM `izin_apps_trklist` AS `track` ";
$cFindDirut .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
$cFindDirut .= "WHERE `track`.`tracking_id` = ?";
$rcFindDirut = $dbs->getQuery($cFindDirut, "Exec", ["s|$tracking_id"]);
$FindDirut = $dbs->getAssoc($rcFindDirut);

$trkList_id = $FindDirut["id_trklist"];
$cFindAttachments = "SELECT * FROM `izin_apps_trkfile` WHERE `trkfile_uploader_id` LIKE '{$trkList_id}%'";
$rcFindAttachments = $dbs->getQuery($cFindAttachments);
$FindAttachments = $dbs->getAll($rcFindAttachments);

$dirut_name = $FindDirut["dirut"];
$dirut_birthdate = $FindDirut["tgl_lhr_dirut"];

//$arr_dirut_name = explode(" ",$dirut_name);
//$fix_dirut_name = implode("",$arr_dirut_name);
//$dirut_name_strip = str_replace("'", "", $dirut_name);
//$fix_dirut_name = preg_replace("/[^A-Za-z0-9]/", "", $dirut_name);
$fix_dirut_name = preg_replace("/[^A-Za-z]/", "", $dirut_name ?? '');
//$fix_dirut_name = preg_replace("/[^\w]/", "", $dirut_name_strip);
$arr_dirut_birthdate = explode("-", $dirut_birthdate ?? '');
$fix_dirut_birthdate = implode("", $arr_dirut_birthdate);
$pwdtoshow = $fix_dirut_name . $fix_dirut_birthdate;


//POPULATE ITEM DROPDOWN
//-------------------------------------------------
/*
    $cLoadItems = "SELECT `pkgworks`.`id_work`, `pkgworks`.`kode`,`pkgworks`.`urutan` FROM `izin_apps_packagework` AS `pkgworks` ";
    $cLoadItems.= "WHERE `pkgworks`.`id_lead` = (";
    $cLoadItems .= "SELECT `track`.`lead_id` FROM `izin_apps_trklist` AS `track` ";
    $cLoadItems .= "WHERE `track`.`tracking_id` = '".$tracking_id."') ";
    $cLoadItems .= "ORDER BY `pkgworks`.`urutan` ASC";
    $rcLoadItems = $dbs->getQuery($cLoadItems);
/*
    $cLoadItems = "SELECT `pkgworks`.`id_work`, `pkgworks`.`kode`,`pkgworks`.`urutan` FROM `izin_apps_packagework` AS `pkgworks` ";
    $cLoadItems.= "WHERE `pkgworks`.`id_lead` = (";
    $cLoadItems .= "SELECT `lead`.`id_lead` FROM `izin_apps_trklist` AS `track` ";
    $cLoadItems .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
    $cLoadItems .= "WHERE `track`.`tracking_id` = '".$tracking_id."') ";
    $cLoadItems .= "ORDER BY `pkgworks`.`urutan` ASC";
    $rcLoadItems = $dbs->getQuery($cLoadItems);
*/

// ambil data item tersedia sesuai lead berdasarkan urutan
$cLeadItems = "SELECT `pkgworks`.`id_work`, `pkgworks`.`kode`,`pkgworks`.`urutan` FROM `izin_apps_packagework` AS `pkgworks` ";
$cLeadItems .= "WHERE `pkgworks`.`id_lead` = (";
$cLeadItems .= "SELECT `track`.`lead_id` FROM `izin_apps_trklist` AS `track` ";
$cLeadItems .= "WHERE `track`.`tracking_id` = ?) ";
$cLeadItems .= "ORDER BY `pkgworks`.`urutan` ASC";
$rcLeadItems = $dbs->getQuery($cLeadItems, "Exec", ["s|$tracking_id"]);
//echo $cLeadItems;
//exit;

$items_array = array();

while ($LeadItems = $dbs->getAssoc($rcLeadItems)) {


    // ambil data yang sudah di kerjakan status 0/new sudah ada
    //$cTrackItems = "SELECT `trkitm`.`id_trkitm`, `trkitm`.`works_id`,`trkitm`.`trkitm_status` FROM `izin_apps_trkitm` AS `trkitm` ";
    $cTrackItems = "SELECT `trkitm`.`works_id` FROM `izin_apps_trkitm` AS `trkitm` ";
    $cTrackItems .= "WHERE `trkitm`.`trklist_id` = (";
    $cTrackItems .= "SELECT `track`.`id_trklist` FROM `izin_apps_trklist` AS `track` ";
    $cTrackItems .= "WHERE `track`.`tracking_id` = ?) ";
    $cTrackItems .= "AND `trkitm`.`works_id` = ? ";
    //$cTrackItems .= "AND `trkitm`.`trkitm_status` = 0 ";
    $cTrackItems .= "GROUP BY `trkitm`.`works_id` ";
    //echo $cTrackItems;
    $rcTrackItems = $dbs->getQuery($cTrackItems, "Exec", ["s|$tracking_id", "s|" . $LeadItems['id_work']]);
    $TrackItems = $dbs->getAssoc($rcTrackItems);

    if (isset($TrackItems['works_id']) && !empty($TrackItems['works_id'])) {
        //kalau ada work yang di maksud, masukkan ke array
        $items_array[] = $TrackItems['works_id'];
    }
}
//exit;



$items_for_dropdown = implode("', '", $items_array);
//echo $items_for_dropdown;
//exit;

$cLoadItems = "SELECT `pkgworks`.`id_work`, `pkgworks`.`kode`,`pkgworks`.`urutan` FROM `izin_apps_packagework` AS `pkgworks` ";
$cLoadItems .= "WHERE `pkgworks`.`id_lead` = (";
$cLoadItems .= "SELECT `track`.`lead_id` FROM `izin_apps_trklist` AS `track` ";
$cLoadItems .= "WHERE `track`.`tracking_id` = '" . $tracking_id . "') ";
$cLoadItems .= "AND `pkgworks`.`id_work` IN ('" . $items_for_dropdown . "') ";
$cLoadItems .= "ORDER BY `pkgworks`.`urutan` ASC";
//echo $cLoadItems;
//exit;
$rcLoadItems = $dbs->getQuery($cLoadItems);



//POPULATE STAKEHOLDER DROPDOWN
//-------------------------------------------------
//$cLoadStkholder = "SELECT * FROM `izin_apps_stakeholder`";
//$rcLoadStkholder = $dbs->getQuery($cLoadStkholder);


//BUTTON UPDATE TRACKING
//-------------------------------------------------
$act_update = "UpdateTracking_" . $_SESSION["butts_Update"];
$form_update = "UpdateTrack";
$name = $lang_805; //813
$name = "Update Tracking"; //813
$buttUpdate = "";
$buttUpdate .= "													";
//$buttUpdate .= "<a href=\"javascript:submitbutton('".$act_update."','".$form_update."');\" id=\"".$act_update."\" class=\"btn btn-warning \"><i class=\"material-icons mr-1\">launch</i>".$name."</a>";
$buttUpdate .= "<a href=\"javascript:confirmation('Close tracking item!');\" id=\"" . $act_update . "\" class=\"btn btn-warning \"><i class=\"material-icons mr-1\">launch</i>" . $name . "</a>";
$buttUpdate .= "\r\n";

//BUTTON UPDATE ULTAH
//-------------------------------------------------
$act_updateULTAH = "SettingTracking_" . $_SESSION["SaveSettingTracking"];
$form_updateULTAH = "settingUltah";
//$name = $lang_805;//813
$nameULTAH = "Save Setting"; //813
$buttUpdateULTAH = "";
$buttUpdateULTAH .= "													";
//$buttUpdate .= "<a href=\"javascript:submitbutton('".$act_update."','".$form_update."');\" id=\"".$act_update."\" class=\"btn btn-warning \"><i class=\"material-icons mr-1\">launch</i>".$name."</a>";
$buttUpdateULTAH .= "<a href=\"javascript:confirmation('Save Setting');\" id=\"" . $act_updateULTAH . "\" class=\"btn btn-success \"><i class=\"material-icons mr-1\">check</i>" . $nameULTAH . "</a>";
$buttUpdateULTAH .= "\r\n";

// CLIENT DETAIL
//-------------------------------------------------
$cViewClient .= "SELECT `trklist`.`company_name`, `lead`.`client_id`, `lead`.`client_email`, `lead`.`client_name`, `trklist`.`tracking_id`, `lead`.`package_name` , `lead`.`package_id` ";
$cViewClient .= "FROM `izin_apps_lead` AS `lead` ";
$cViewClient .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `lead`.`id_lead`=`trklist`.`lead_id` ";
$cViewClient .= "WHERE `trklist`.`tracking_id` = ? ";
$rcViewClient = $dbs->getQuery($cViewClient, "Exec", ["s|$tracking_id"]);
$ViewClient = $dbs->getAssoc($rcViewClient);

// BNRI DETECTOR
//-------------------------------------------------
$cDetectBNRI = "SELECT COUNT(`pwork`.`id`) AS `detector` FROM `izin_apps_lead` AS `lead` ";
$cDetectBNRI .= "INNER JOIN `izin_apps_packagework` AS `pwork` ON `lead`.`id_lead`=`pwork`.`id_lead` ";
$cDetectBNRI .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `lead`.`id_lead`=`trklist`.`lead_id` ";
$cDetectBNRI .= "WHERE `trklist`.`tracking_id` = ? AND `pwork`.`kode` = ? ";
$rcDetectBNRI = $dbs->getQuery($cDetectBNRI, "Exec", ["s|$tracking_id", "s|BNRI"]);
$DetectBNRI = $dbs->getAssoc($rcDetectBNRI);

// POPULATE TRACKING
//-------------------------------------------------
$cViewTrack = "SELECT `track`.`id_trklist`, `track`.`tracking_id`, `track`.`company_name`, `lead`.`client_id`, `lead`.`client_name`, `lead`.`package_name`, ";
$cViewTrack .= "`item`.`id_trkitm`, `item`.`tgl_update_itm`, `item`.`reason_title`, `item`.`reason_note`, `item`.`trkitm_status`, `item`.`id_trkfile`, `item`.`trkfile_name_uniq`, ";
$cViewTrack .= "`item`.`kode` FROM `izin_apps_trklist` AS `track` ";
$cViewTrack .= "INNER JOIN `izin_apps_lead` AS `lead` ON `track`.`lead_id` = `lead`.`id_lead` ";
$cViewTrack .= "LEFT JOIN ( ";
$cViewTrack .= "SELECT `trkitm`.`id_trkitm`, `trkitm`.`trklist_id`, `trkitm`.`tgl_update_itm`, `trkitm`.`reason_title`, `trkitm`.`reason_note`, `trkitm`.`trkitm_status`, `trkfile`.`id_trkfile`, ";
$cViewTrack .= "`trkfile`.`trkfile_name_uniq`, `trkitm`.`stakeholder_id`, `works`.`id`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cViewTrack .= "LEFT JOIN `izin_apps_trkfile` AS `trkfile` ON `trkitm`.`trkfile_id`=`trkfile`.`id_trkfile` ";
$cViewTrack .= "INNER JOIN `izin_apps_works` AS `works` ON `works`.`id`=`trkitm`.`works_id` ";
$cViewTrack .= "WHERE `trkitm`.`trkitm_status` <> 0 ";
$cViewTrack .= ") AS `item` ON `item`.`trklist_id` = `track`.`id_trklist` ";
$cViewTrack .= "WHERE `lead`.`status` = ? AND `track`.`tracking_id` = ? ";
//$cViewTrack .= "AND `track`.`trk_status` <> 3 "; //DONE
$cViewTrack .= "ORDER BY `item`.`tgl_update_itm` DESC";
$rcViewTrack = $dbs->getQuery($cViewTrack, "Exec", ["s|CL", "s|$tracking_id"]);
//}
