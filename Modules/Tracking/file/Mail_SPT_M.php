<?php

if ($BAD_NmPT != 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "Pelaporan Pajak Bulanan $Company_Name";
    $Email_Body = "<DIV style='max-width: 500px'><P>Kepada Yth,</P>
        <P>Salam dari IZIN.co.id!</P>

        <P>Bersama ini kami informasikan bahwa proses Pelaporan Pajak Bulanan atas nama <b>$Company_Name</b> telah selesai dikerjakan.</P>

        <P>Untuk mengunduh soft file dokumen izin Anda, silakan klik <a href='https://tracking.izin.co.id'>traking.izin.co.id</a> dan masukkan Kode Tracking yang sudah dikirimkan di email sebelumnya beserta password Anda.</P>

        <p>Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan <a href='https://izin.co.id'>IZIN.co.id</a>. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan <a href='https://izin.co.id'>IZIN.co.id</a>.</p>

        <p>Silakan klik di sini untuk memberikan review Anda: <a href='http://search.google.com/local/writereview?placeid=ChIJFwAAAPnzaS4RvliqYwgBDT4'>http://search.google.com/local/writereview?placeid=ChIJFwAAAPnzaS4RvliqYwgBDT4</a>.</p>

        <p>Terima kasih atas kepercayaan Anda menggunakan layanan <a href='https://izin.co.id'>IZIN.co.id</a>. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.</p>

        <P>Salam Hangat, <BR/>
        <A HREF='https://izin.co.id'>IZIN.co.id</A></P>

        <BR/><HR>
        <P class='text-muted'><i>Pesan ini adalah pesan otomatis - Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut, Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
        </i></P>
        </DIV>
        ";
}
