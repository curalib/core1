<!--===========TRACKING=========================================================-->

<div class="container-fluid page__heading-container">
    <div class="page__heading d-flex align-items-center">
        <div class="flex">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="<?php echo $bchref ?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101
                                                                                                                            ?></a></li>
                    <li class="breadcrumb-item"><?php echo $lang_50010 ?></li>
                    <li class="breadcrumb-item"><a href="<?php echo $bcTrack ?>"><?php echo $lang_50100 ?></a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo $lang_50130 ?></li>
                </ol>
            </nav>
            <!--<h1 class="m-0"><?php echo $lang_50130 ?></h1>-->
        </div>
        <form action="" method="post" name="ViewTimeline" id="ViewTimeline">
            <input type="hidden" name="task" />
            <input type="hidden" name="trackingID" value="<?php echo $tracking_id ?>" />
        </form>
        <?php
        echo $buttTimeLink;
        ?>
        <button type="button" style="margin-right:7px;margin-left:7px;" class="btn btn-primary" data-toggle="modal" data-target="#modalTax">Setting TAX</button>
        <?php if ($ViewClient["package_id"] == "PHKI") { ?>
            <button type="button" style="margin-right:7px;margin-left:7px;" class="btn btn-primary" data-toggle="modal" data-target="#modalHaki">Setting HaKI</button>
        <?php } else { ?>
            <button type="button" style="margin-right:7px;margin-left:7px;" class="btn btn-primary" data-toggle="modal" data-target="#modalAkta">Setting AKTA</button>
            <button type="button" style="margin-right:7px;margin-left:7px;" class="btn btn-primary" data-toggle="modal" data-target="#modalLKPM">Setting LKPM</button>
        <?php } ?>
        <button type="button" style="margin-right:7px;margin-left:7px;" class="btn btn-primary" data-toggle="modal" data-target="#modalBast">Setting BAST</button>
    </div>
</div>

<!-- Modal Tax-->
<div style="position:absolute; z-index:99999;" class="modal fade" id="modalTax" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="top:70px; z-index:99999; padding-bottom: 450px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setting Reminder Tax</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="max-height: 400px; overflow-y: auto;">
                <div id="form_DL_Tax"></div>
                <form action="" method="post" name="settingReminderTAX" id="settingReminderTAX" enctype="multipart/form-data">
                    <input type="hidden" name="idtrack" value="<?= $FindDirut['id_trklist']; ?>">
                    <input type="hidden" name="email" value="<?= $FindDirut['client_email']; ?>">
                    <input type="hidden" name="nama" value="<?= $FindDirut['company_name']; ?>">
                    <div class="row">
                        <div class="container">
                            <?php foreach (TAX as $key => $itemH) : ?>
                                <!-- timeline item <?= $itemH ?> -->
                                <div class="row">
                                    <!-- timeline item <?= $itemH ?> left dot -->
                                    <div class="col-auto text-center flex-column d-none d-sm-flex">
                                        <div class="row h-50">
                                            <div class="col">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                        <h5 class="m-2">
                                            <span class="badge badge-pill bg-light border">&nbsp;</span>
                                        </h5>
                                        <div class="row h-50">
                                            <div class="col border-right">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                    </div>
                                    <!-- timeline item <?= $itemH ?> event content -->
                                    <div class="col py-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="float-right text-muted"></div>
                                                <h4 class="card-title text-muted"><?= $itemH; ?></h4>
                                                <?php if (!in_array($key, [0, 1, 2, 4])) : // skip EFIN, SPPKP, SK PP 55, & PAJAK TAHUNAN
                                                ?>
                                                    <div class="form-group mt-2">
                                                        <input type="date" class="form-control date-picker mb-2" style="width: 175px" name="reminder_tax_<?= $key ?>" value="<?= $FindDirut['reminder_tax_' . $key] ?>" />
                                                        <button type="submit" name="SAVEREMINDERTAX" value="<?= $key ?>" class="btn btn-primary btn-sm">Save Reminder Date</button>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (in_array($key, [4])) : // only PAJAK TAHUNAN
                                                ?>
                                                    <div class="form-group mt-2">
                                                        <select name="reminder_tax_<?= $key ?>">
                                                            <option value="">Please select...</option>
                                                            <?php for ($i = date('Y') - 2; $i <= date('Y'); $i++) : ?>
                                                                <option value="<?= $i ?>" <?= (substr($FindDirut['reminder_tax_' . $key], 0, 4) == $i) ? "selected" : "" ?>><?= $i ?></option>
                                                            <?php endfor; ?>
                                                        </select>
                                                        <button type="submit" name="SAVEREMINDERTAX" value="<?= $key ?>" class="btn btn-primary btn-sm">Save Year</button>
                                                    </div>
                                                <?php endif; ?>
                                                <div class="form-group">
                                                    <label for="attachment_tax_<?php echo $key ?>"><?php echo $lang_818 ?></label><br>
                                                    <div class="dropzone">
                                                        <div class="fallback">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input" name="attachment_tax_<?php echo $key ?>" id="attachment_tax_<?php echo $key ?>">
                                                                <label class="custom-file-label" for="attachment_tax_<?php echo $key ?>" id="label_attachment_tax_<?php echo $key ?>">
                                                                    <span data-content="Upload file...">
                                                                        <?php
                                                                        $foundTax = false;
                                                                        foreach ($FindAttachments as $attachment) {
                                                                            if ($attachment['trkfile_uploader_id'] == $FindDirut['id_trklist'] . '_attachment_tax_' . $key) {
                                                                                $foundTax = true;
                                                                                echo $attachment['trkfile_name_ori'];
                                                                                break;
                                                                            }
                                                                        }

                                                                        if (!$foundTax) {
                                                                            echo '...';
                                                                        }
                                                                        ?>
                                                                    </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" name="SAVEATTACHMENTTAX" value="<?= $key ?>" class="btn btn-secondary btn-sm">Save Attachment</button>
                                                <?php if ($foundTax) { ?>
                                                    <a style="cursor: pointer;" class="btn btn-info btn-sm btn_dl_tax" data-id="<?= $attachment['id_trkfile'] ?>">Download</a>
                                                    <?php if (!in_array($key, [0, 1, 2])) {
                                                        if (!in_array($FindDirut['reminder_tax_' . $key], [NULL, '0000-00-00'])) {
                                                            echo '<button type="submit" name="SAVESENDINGTAX" value="' . $key . '" class="btn btn-success btn-sm">Send Notifications</button>';
                                                        } else {
                                                            echo '<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Set Tanggal & Attachment terlebih dulu."><button type="button" class="btn btn-success btn-sm" style="pointer-events: none;" disabled>Send Notifications</button></span>';
                                                        }
                                                    } else {
                                                        echo '<button type="submit" name="SAVESENDINGTAX" value="' . $key . '" class="btn btn-success btn-sm">Send Notifications</button>';
                                                    } ?>
                                                <?php } else {
                                                    echo '<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Set Attachment terlebih dulu."><button type="button" class="btn btn-success btn-sm" style="pointer-events: none;" disabled>Send Notifications</button></span>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <!--/row-->
                        </div>
                        <!--container-->
                        <div class="col-md-12">
                            <br>
                        </div>
                        <br>
                        <div class="col-md-6" style="margin-top:20px;">

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<!-- Modal Haki -->
<div style="position:absolute; z-index:99999;" class="modal fade" id="modalHaki" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="top:70px; z-index:99999; padding-bottom: 450px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setting Reminder HAKI</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="form_DL_Haki"></div>
                <form action="" method="post" name="settingReminderHAKI" id="settingReminderHAKI" enctype="multipart/form-data">
                    <input type="hidden" name="idtrack" value="<?= $FindDirut['id_trklist']; ?>">
                    <input type="hidden" name="email" value="<?= $FindDirut['client_email']; ?>">
                    <div class="row">
                        <div class="container">
                            <div class="row">
                                <!-- header without left dot -->
                                <div class="col-auto text-center flex-column d-none d-sm-flex">
                                    <div class="row h-50">
                                        <div class="col">&nbsp;</div>
                                        <div class="col">&nbsp;</div>
                                    </div>
                                    <div class="row h-50">
                                        <div class="col">&nbsp;</div>
                                        <div class="col">&nbsp;</div>
                                    </div>
                                </div>
                                <!-- header event content -->
                                <div class="col py-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="float-right text-muted"></div>
                                            <h4 class="card-title text-muted">Form</h4>
                                            <div class="form-row">
                                                <div class="col-lg-8">
                                                    <input type="text" class="form-control" placeholder="Nama Merek" name="reminder_haki_merek" value="<?= $FindDirut['reminder_haki_merek'] ?>" maxlength="255">
                                                </div>
                                                <div class="col-lg-2">
                                                    <input type="number" class="form-control" placeholder="Kelas" name="reminder_haki_kelas" value="<?= $FindDirut['reminder_haki_kelas'] ?>" min="1">
                                                </div>
                                                <div class="col-lg-2">
                                                    <button type="submit" name="SAVESETTINGHAKI" value="header" class="btn btn-primary">Save</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- timeline item 1 -->
                            <?php foreach (HAKI as $key => $itemH) : ?>
                                <div class="row">
                                    <!-- timeline item 1 left dot -->
                                    <div class="col-auto text-center flex-column d-none d-sm-flex">
                                        <div class="row h-50">
                                            <div class="col">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                        <h5 class="m-2">
                                            <span class="badge badge-pill bg-light border">&nbsp;</span>
                                        </h5>
                                        <div class="row h-50">
                                            <div class="col border-right">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                    </div>
                                    <!-- timeline item 1 event content -->
                                    <div class="col py-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="float-right text-muted"></div>
                                                <h4 class="card-title text-muted"><?= $itemH; ?></h4>
                                                <?php if (in_array($itemH, ['Link Progress'])) : // URL
                                                ?>
                                                    <div class="form-group mt-2">
                                                        <input type="url" class="form-control mb-2" style="width: 255px" name="reminder_haki_link" value="<?= $FindDirut['reminder_haki_link'] ?>" maxlength="255" />
                                                        <button type="submit" name="SAVESETTINGHAKI" value="<?= $key ?>" class="btn btn-primary btn-sm">Save</button>
                                                    </div>
                                                <?php endif; ?>

                                                <?php if (in_array($itemH, ['Pengajuan Permohonan', 'Publikasi', 'Sertifikat Selesai'])) : // Attachment
                                                ?>
                                                    <div class="form-group mt-2">
                                                        <label for="attachment_haki_<?php echo $key ?>"><?php echo $lang_818 ?></label><br>
                                                        <div class="dropzone">
                                                            <div class="fallback">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="attachment_haki_<?php echo $key ?>" id="attachment_haki_<?php echo $key ?>">
                                                                    <label class="custom-file-label" for="attachment_haki_<?php echo $key ?>" id="label_attachment_haki_<?php echo $key ?>">
                                                                        <span data-content="Upload file...">
                                                                            <?php
                                                                            $found = false;
                                                                            foreach ($FindAttachments as $attachment) {
                                                                                if ($attachment['trkfile_uploader_id'] == $FindDirut['id_trklist'] . '_attachment_haki_' . $key) {
                                                                                    $found = true;
                                                                                    echo $attachment['trkfile_name_ori'];
                                                                                    break;
                                                                                }
                                                                            }

                                                                            if (!$found) {
                                                                                echo '...';
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" name="SAVEATTACHMENTHAKI" value="<?= $key ?>" class="btn btn-secondary btn-sm" style="margin-top:8px;">Save Attachment</button>
                                                    <?php if ($found) : ?>
                                                        <a style="cursor: pointer;margin-top:8px;" class="btn btn-info btn-sm btn_dl_haki" data-id="<?= $attachment['id_trkfile'] ?>">Download</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                                <?php
                                                if (in_array($itemH, ['Link Progress'])) {
                                                    if (!empty($FindDirut['reminder_haki_merek']) && !empty($FindDirut['reminder_haki_kelas']) && !empty($FindDirut['reminder_haki_link'])) {
                                                        echo '<button type="submit" name="SAVESENDINGHAKI" value="' . $key . '" class="btn btn-success btn-sm" style="margin-top:8px;">Send Notifications</button>';
                                                    } else {
                                                        echo '<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Set Nama Merek, Kelas, dan Link terlebih dulu."><button type="button" class="btn btn-success btn-sm" style="margin-top: 8px; pointer-events: none;" disabled>Send Notifications</button></span>';
                                                    }
                                                } elseif (in_array($itemH, ['Pengajuan Permohonan', 'Publikasi', 'Sertifikat Selesai'])) {
                                                    if (!empty($FindDirut['reminder_haki_merek']) && !empty($FindDirut['reminder_haki_kelas']) && $found) {
                                                        echo '<button type="submit" name="SAVESENDINGHAKI" value="' . $key . '" class="btn btn-success btn-sm" style="margin-top:8px;">Send Notifications</button>';
                                                    } else {
                                                        echo '<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Set Nama Merek, Kelas & Attachment terlebih dulu."><button type="button" class="btn btn-success btn-sm" style="margin-top: 8px; pointer-events: none;" disabled>Send Notifications</button></span>';
                                                    }
                                                } else {
                                                    if (!empty($FindDirut['reminder_haki_merek']) && !empty($FindDirut['reminder_haki_kelas'])) {
                                                        echo '<button type="submit" name="SAVESENDINGHAKI" value="' . $key . '" class="btn btn-success btn-sm" style="margin-top:8px;">Send Notifications</button>';
                                                    } else {
                                                        echo '<span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Set Nama Merek dan Kelas terlebih dulu."><button type="button" class="btn btn-success btn-sm" style="margin-top: 8px; pointer-events: none;" disabled>Send Notifications</button></span>';
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <!--/row-->
                        </div>
                        <!--container-->
                        <div class="col-md-12">
                            <br>
                        </div>
                        <br>
                        <div class="col-md-6" style="margin-top:20px;">

                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<!-- Modal Akta -->
<div style="top:170px; position:absolute; z-index:99999;" class="modal fade" id="modalAkta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="top:170px;  z-index:99999;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setting Reminder AKTA SK</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" name="settingReminderAKTA" id="settingReminderAKTA">
                    <input type="hidden" name="idtrack" value="<?= $FindDirut['id_trklist']; ?>">
                    <input type="hidden" name="email" value="<?= $FindDirut['client_email']; ?>">
                    <div class="row" style="padding:30px;">
                        <?php
                        $inYear = $FindDirut['reminder_akta_in'] ?? 0;
                        $date = strtotime($FindDirut['reminder_akta']);
                        $newdate = date('Y-m-d', strtotime("-$inYear year", $date));
                        ?>
                        <div class="col-sm-12 col-lg-4">
                            <div class="from-group">
                                <label for="aktaMulai">Reminder AKTA Tahun ke: </label>
                                <input type="number" class="form-control" id="aktaTahun" name="aktaTahun" value="<?= $inYear; ?>" min="0" max="5">
                            </div>
                        </div>
                        <br>
                        <div class="col-sm-12 col-lg-6">
                            <div class="from-group">
                                <label for="aktaMulai">Tanggal Mulai AKTA: </label>
                                <input type="date" class="form-control" id="aktaMulai" name="aktaMulai" placeholder="Tanggal Mulai" value="<?= $newdate; ?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <p>NOTE : <i>Reminder akan terkirim <?= $inYear; ?> tahun kemudian.</i></p>
                        </div>
                        <br>
                        <div class="col-md-6" style="margin-top:20px;">
                            <button type="submit" name="SAVESETTINGAKTA" class="btn btn-success" style="margin-bottom:40px;margin-top:8px;">Save Setting</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal LKPM -->
<div style="top:170px; position:absolute; z-index:99999;" class="modal fade" id="modalLKPM" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="top:170px;  z-index:99999;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setting Reminder LKPM</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" name="settingReminderLKPM" id="settingReminderLKPM">
                    <input type="hidden" name="idtrack" value="<?= $FindDirut['id_trklist']; ?>">
                    <input type="hidden" name="email" value="<?= $FindDirut['client_email']; ?>">
                    <div class="form-group">
                        <label for="lkpm">Pilih Triwulan</label>
                        <select name="lkpm" id="lkpm" class="form-control">
                            <option value="1" <?= $FindDirut['reminder_lkpm'] == '1' ? 'selected' : ''; ?>>Triwulan 1</option>
                            <option value="2" <?= $FindDirut['reminder_lkpm'] == '2' ? 'selected' : ''; ?>>Triwulan 2</option>
                            <option value="3" <?= $FindDirut['reminder_lkpm'] == '3' ? 'selected' : ''; ?>>Triwulan 3</option>
                            <option value="4" <?= $FindDirut['reminder_lkpm'] == '4' ? 'selected' : ''; ?>>Triwulan 4</option>
                        </select>
                    </div>
                    <button type="submit" name="SAVESETTINGLKPM" value="0" class="btn btn-success">Save Setting</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal BAST -->
<div style="position:absolute; z-index:99999;" class="modal fade" id="modalBast" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="false" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="top:70px; z-index:99999; padding-bottom: 450px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Setting Attachment BAST</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="max-height: 400px; overflow-y: auto;">
                <div id="form_DL_Bast"></div>
                <form action="" method="post" name="settingBAST" id="settingBAST" enctype="multipart/form-data">
                    <input type="hidden" name="idtrack" value="<?= $FindDirut['id_trklist']; ?>">
                    <input type="hidden" name="email" value="<?= $FindDirut['client_email']; ?>">
                    <div class="container">
                        <?php for ($key = 0; $key < 7; $key++) : ?>
                            <!-- timeline item <?= $key ?> -->
                            <div class="row">
                                <?php if ($key == 6) { ?>
                                    <!-- header without left dot -->
                                    <div class="col-auto text-center flex-column d-none d-sm-flex">
                                        <div class="row h-50">
                                            <div class="col">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                        <div class="row h-50">
                                            <div class="col">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <!-- timeline item <?= $key ?> left dot -->
                                    <div class="col-auto text-center flex-column d-none d-sm-flex">
                                        <div class="row h-50">
                                            <div class="col">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                        <h5 class="m-2">
                                            <span class="badge badge-pill bg-light border">&nbsp;</span>
                                        </h5>
                                        <div class="row h-50">
                                            <div class="col border-right">&nbsp;</div>
                                            <div class="col">&nbsp;</div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <!-- timeline item <?= $key ?> event content -->
                                <div class="col py-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="float-right text-muted"></div>
                                            <?php
                                            $tmp_bast_increment = $key + 1;
                                            switch ($key) {
                                                case '5':
                                                    $bast_label = "TAX Kit";
                                                    break;
                                                case '6':
                                                    $bast_label = "Legal Kit";
                                                    break;
                                                default:
                                                    $bast_label = "Attachment $tmp_bast_increment";
                                                    break;
                                            }
                                            ?>
                                            <h4 class="card-title text-muted"><?= $bast_label ?></h4>
                                            <div class="form-group">
                                                <label for="attachment_bast_<?php echo $key ?>"><?php echo $lang_818 ?></label><br>
                                                <div class="dropzone">
                                                    <div class="fallback">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" name="attachment_bast_<?php echo $key ?>" id="attachment_bast_<?php echo $key ?>">
                                                            <label class="custom-file-label" for="attachment_bast_<?php echo $key ?>" id="label_attachment_bast_<?php echo $key ?>">
                                                                <span data-content="Upload file...">
                                                                    <?php
                                                                    $foundBAST = false;
                                                                    foreach ($FindAttachments as $attachment) {
                                                                        if ($attachment['trkfile_uploader_id'] == $FindDirut['id_trklist'] . '_attachment_bast_' . $key) {
                                                                            $foundBAST = true;
                                                                            echo $attachment['trkfile_name_ori'];
                                                                            break;
                                                                        }
                                                                    }

                                                                    if (!$foundBAST) {
                                                                        echo '...';
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" name="SAVEATTACHMENTBAST" value="<?= $key ?>" class="btn btn-secondary btn-sm">Save Attachment</button>
                                            <?php if ($foundBAST) : ?>
                                                <a style="cursor: pointer;" class="btn btn-info btn-sm btn_dl_bast" data-id="<?= $attachment['id_trkfile'] ?>">Download</a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endfor; ?>
                        <!--/row-->
                    </div>
                    <!--container-->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid page__container">
    <?php
    if (isset($_SESSION["AlertMess"]) or !empty($_SESSION["AlertMess"])) {
        echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
        echo '<i class="material-icons mr-3">error_outline</i>';
        //echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
        echo '<div class="text-body"><strong>A l e r t</strong><br/> ' . $_SESSION["AlertMess"] . '</div>';
        echo '</div>';
        //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
        unset($_SESSION["AlertMess"]);
    }
    ?>
    <!-- -->


    <!-- Client Detail      style="width: 18rem;"  w-auto bg-light  d-flex flex-sm-row-->
    <div class="card card-form flex flex-sm-row">
        <div class="card-body card-form__body flex" style="padding-top:15px;padding-bottom:10px;">
            <!--<div class="card-form__body card-body-form-group flex bg-secondary flex-shrink-1 justify-content-end flex-column flex-row-reverse">-->
            <!--<div class="card-form__body card-body-form-group d-flex flex-column flex-shrink-1 ">-->
            <div class="row">
                <div class="col-sm-auto flex">
                    <label><?php echo $lang_50201 ?></label>
                    <h6><?php echo $tracking_id; //$ViewClient['client_id']
                        ?> </h6>
                </div>

                <div class="col-sm-auto flex">
                    <label><?php echo $lang_50202 ?></label>
                    <h6><?php echo $ViewClient['client_name'] ?> </h6>
                </div>

                <div class="col-sm-auto flex">
                    <label><?php echo $lang_50203 ?></label>
                    <h6><?php echo $ViewClient['package_name'] ?> </h6>
                </div>

                <div class="col-sm-auto flex">
                    <!--<div class="col-sm-auto">
                                                <div class="form-group">-->
                    <label><?php echo $lang_50204 ?></label>
                    <h6><?php echo $ViewClient['company_name'] ?> </h6>
                    <!--</div>
                                            </div>-->

                </div>

                <div class="col-sm-auto flex " style="margin-top:10px">
                    <form action="" method="post" name="AccessClient" id="AccessClient">
                        <input type="hidden" name="task" />
                        <input type="hidden" name="client_email" value="<?= $ViewClient["client_email"] ?>" />
                    </form>
                    <?php
                    if ($FindDirut["isActivated"] == 1) {
                        echo ButtonsCommon("commonbuttons", THEMES, "Edit_i", "Update Access", "UpdatePasswd_" . $_SESSION["butts_Save"], "AccessClient", "", "");
                    } else {
                        echo ButtonsCommon("commonbuttons", THEMES, "Edit_i", "Create Access", "CreatePasswd_" . $_SESSION["butts_Save"], "AccessClient", "", "");
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Client Detail -->
    <form action="" method="post" name="settingUltah" id="settingUltah">
        <div class="card card-form">
            <input type="hidden" name="idtrack" value="<?= $FindDirut['id_trklist']; ?>">
            <input type="hidden" name="idlead" value="<?= $FindDirut['id_lead']; ?>">
            <input type="hidden" name="email" value="<?= $FindDirut['client_email']; ?>">
            <div class="card-form__body ">
                <div class="row" style="padding:30px;">
                    <?php
                    $edtNamaPerusahaanEdit = false;
                    if (in_array($_SESSION['Jbtn'], ['Manager', 'Administrator'])) {
                        $edtNamaPerusahaanEdit = true;
                    }
                    ?>
                    <div class="col-md-6">
                        <div class="from-group">
                            <label for="namaPerusahaan">Company Name : </label>
                            <input type="text" class="form-control" id="namaPerusahaan" value="<?= $ViewClient['company_name']; ?>" name="namaPerusahaan" placeholder="Nama Perusahaan" <?= ($edtNamaPerusahaanEdit) ? '' : 'readonly' ?>>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="from-group">
                            <label for="ultahPerusahaan">Ulang Tahun Perusahaan : </label>
                            <input type="date" class="form-control" id="ultahPerusahaan" value="<?= $FindDirut['ultah_perusahaan']; ?>" name="ultahPerusahaan" placeholder="Tanggal Ulang Tahun">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="from-group">
                            <label for="ultahDirektur">Ulang Tahun Direktur : </label>
                            <input type="date" class="form-control" id="ultahDirektur" value="<?= $FindDirut['ultah_direktur']; ?>" name="ultahDirektur" placeholder="Tanggal Ulang Direktur">
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-top:20px;">
                        <button type="submit" name="SAVESETTING" class="btn btn-success" style="margin-top:8px;">Save Setting</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <form action="" method="post" name="UpdateTrack" id="UpdateTrack" enctype="multipart/form-data">
        <input type="hidden" name="task" />
        <!--<input type="hidden" name="clientID" value="<?php //echo $view_trackingid
                                                        ?>" />-->
        <input type="hidden" name="tracking_id" value="<?php echo $tracking_id ?>" />

        <div class="card card-form flex">
            <div class="card-form__body card-body-form-group d-flex " style="padding-top:15px;padding-bottom:10px;">
                <div class="col-sm-6">
                    <!-- style="background-color:pink;" -->
                    <div class="row-sm-auto">
                        <div class="form-group">
                            <label for="mstitem"><?php echo $lang_50132 ?></label><br>
                            <select name="mstitem" id="mstitem" class="custom-select js-example-theme-multiple" style="width: 150px;">
                                <!--<select name="mstitem" id="mstitem" data-toggle="select" class="custom-select" style="width: 150px;">js-example-basic-single js-states form-control
                                                  -->
                                <option value="all">Any</option>
                                <?php
                                while ($LoadItems = $dbs->getAssoc($rcLoadItems)) {
                                    if ($LoadItems['kode'] != "BNRI") {
                                ?>
                                        <option value="<?php echo $LoadItems['id_work'] ?>"><?php echo $LoadItems['kode'] ?></option>
                                <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row-sm-auto">
                        <div class="form-group">
                            <label for="trkbadge"><?php echo $lang_50113 ?></label><br>
                            <select name="trkbadge" id="trkbadge" class="custom-select" style="width: 150px;">
                                <option value="all">Any</option>
                                <?php
                                foreach (BADGE_STATUS as $badgekey => $badgevalue) {
                                    if ($badgevalue != "New") {
                                        echo '<option value="' . $badgekey . '">' . $badgevalue . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row-sm-auto">
                        <div class="form-group">
                            <!--<label for="filter_name"><?php echo $lang_813 ?></label>
                                                    <input name="upstatus" id="filter_name" type="text" class="form-control" placeholder="Input update" required>
                                                    -->
                            <label for="select01">Reason<?php //echo $lang_50132
                                                        ?></label><br>
                            <select name="reason_title" id="select01" class=" custom-select" style="width: 150px;">
                                <!--<select name="mstitem" id="mstitem" data-toggle="select" class="custom-select" style="width: 150px;">js-example-basic-single js-states form-control
                                                  -->
                                <?php
                                //<option value="all">Any</option>
                                //<option value="1">Tessss reason title</option>
                                foreach (REASON_TITLE as $key => $value) {
                                    //if($LoadItems['kode_mstitem']!="WAITING") {
                                ?>
                                    <option value="<?php echo $key ?>"><?php echo $value ?></option>
                                <?php
                                    //}
                                }
                                ?>
                                <?php
                                //while($LoadItems = $dbs->getAssoc($rcLoadItems)) {
                                //if($LoadItems['kode_mstitem']!="WAITING") {
                                ?>
                                <!--<option value="<?php //echo $LoadItems['id_work']
                                                    ?>"><?php //echo $LoadItems['kode']
                                                        ?></option>-->
                                <?php
                                //}
                                //}
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="row-sm-auto">
                        <div class="form-group">
                            <label for="customFileUpload"><?php echo $lang_818 ?></label><br>
                            <!--
                                                    <div class="dropzone dropzone-multiple w-100" data-toggle="dropzone" data-dropzone-multiple data-dropzone-url="http://" data-dropzone-files='["assets/images/avatar/blue.svg", "assets/images/avatar/demi.png", "assets/images/avatar/green.svg"]'>
                                                        <div class="fallback">
                                                    -->
                            <div class="dropzone">
                                <div class="fallback">
                                    <div class="custom-file">
                                        <!--<label class="custom-file-label" style="background-color: red;" for="customFileUpload"><?php //echo $lang_816
                                                                                                                                    ?>...<span id="company-logo" class="custom-file-control custom-file-name" data-content="Upload company logo..."></span></label>-->
                                        <input type="file" class="custom-file-input" name="izindocsup" id="customFileUpload">
                                        <label class="custom-file-label" for="customFileUpload" id="label_customFileUpload"><span data-content="Upload file...">...</span></label>
                                    </div>
                                </div>
                            </div>
                            <!--
                                                        </div>
                                                    </div>
                                                    -->
                        </div>
                    </div>
                </div><!-- <div class="col-sm-6"> -->

                <!--
                                              <div class="row-sm-auto">
                                                  <div class="form-group">
                                                      <label for="stkholder"><?php //echo $lang_50133
                                                                                ?></label><br>
                                                      <select name="stkholder" id="stkholder" class="custom-select" style="width: 150px;">
                                                          <option value="all">Any</option>
                                                          <?php
                                                            //while($LoadStkholder = $dbs->getAssoc($rcLoadStkholder)) {
                                                            ?>
                                                          <option value="<?php //echo $LoadStkholder['id_stakeholder']
                                                                            ?>"><?php //echo $LoadStkholder['stkholder_name']
                                                                                ?></option>
                                                          <?php
                                                            //}
                                                            ?>
                                                      </select>
                                                  </div>
                                              </div>
                                              <div class="row-sm-auto" style="margin-top:25px"><!-- d-flex justify-content-end style="margin-top:25px" -->
                <!--                                                  <div class="form-group">
                                                    <?php
                                                    //echo $buttUpdate;
                                                    ?>
                                                  </div>
                                              </div>
-->


                <div class="col-sm-6">
                    <!--  style="background-color:yellow;" -->
                    <div class="row-sm-auto">
                        <div class="form-group">
                            <label for="reason_note">Detail/Note<?php //echo $lang_50133
                                                                ?></label>
                            <textarea name="reason_note" id="reason_note" maxlength="1000" class="form-control" rows="8" cols="30" placeholder="Enter reason note here"></textarea>
                        </div>
                    </div>
                    <div class="row-sm-auto">
                        <!-- mx-auto -->
                        <div class="form-group float-sm-right mt-3">
                            <?php
                            echo $buttUpdate;
                            ?>
                        </div>
                    </div>
                </div><!-- <div class="col"> -->
            </div><!-- <div class="card-form__body card-body-form-group flex " style="margin-top:0px;padding-top:15px;padding-bottom:10px;"> -->
        </div>
        <!--<div class="card card-form flex flex-sm-row"> -->

    </form>



    <?php
    if ($DetectBNRI["detector"]) {
    ?>
        <!-- BNRI message -->
        <div class="card card-form flex flex-sm-row">
            <div class="card-body card-form__body flex" style="padding-top:7px;padding-bottom:5px;">
                <div class="row">
                    <div class="col-sm-auto flex">
                        <h6>* BNRI di selesaikan dalam waktu 365 hari</h6>
                    </div>
                </div>
            </div>
        </div>
        <!-- BNRI message -->
    <?php
    }
    ?>


    <?php
    $showdate = "";

    while ($ViewTrack = $dbs->getAssoc($rcViewTrack)) {
        $dt = new DateTime($ViewTrack['tgl_update_itm']);
        $date = $dt->format('l\, jS F');
        $time = $dt->format('h:i a');
        if ($showdate == $date) {
        } else {
            $showdate = $date;
            echo '<p class="text-dark-gray d-flex align-items-center mt-3">';
            echo '<i class="material-icons icon-muted mr-2">event</i>';
            echo '<strong>' . $showdate . '</strong>';
            echo '</p>';
        }
    ?>

        <div class="row align-items-center projects-item mb-1">
            <div class="col-sm-auto mb-1 mb-sm-0">
                <div class="text-dark-gray"><?php echo $time ?></div>
            </div>
            <div class="col-sm">
                <div class="card m-0">
                    <div class="px-4 py-3">
                        <div class="row align-items-center">
                            <div class="col" style="min-width: 300px">
                                <div class="d-flex align-items-center">
                                    <!--<a href="#" class="text-body"><strong class="text-15pt mr-2">Akta dan SK sudah selesai</strong></a>-->
                                    <a href="#" class="text-body"><?php echo $ViewTrack['kode'] ?>: &nbsp</a>
                                    <a href="#" class="text-body"><strong class="text-15pt mr-2"><?php echo $ViewTrack['reason_title'] ?></strong></a>
                                    <!--<span class="badge badge-success">DONE</span>-->
                                </div>
                                <div class="d-flex align-items-center">
                                    <small class="text-dark-gray mr-2">Detail/Note<?php //echo $lang_50133
                                                                                    ?></small>
                                    <!--<a href="#" class="d-flex align-items-middle">-->
                                    <!--<small class="ml-2"><?php //echo $ViewTrack['stkholder_name']
                                                            ?></small>-->
                                    <small class="ml-2"><?php echo $ViewTrack['reason_note'] ?></small>
                                    <!--</a>-->
                                </div>
                            </div>
                            <div class="col-auto d-flex align-items-center">
                                <?php
                                echo show_badge($ViewTrack['trkitm_status'], BADGE_STATUS);
                                ?>
                                <!--<a href="#" class="text-body"><?php //echo $ViewTrack['kode_mstitem']
                                                                    ?></a>-->
                            </div>
                            <div class="col-auto d-flex align-items-center" style="min-width: 140px;">
                                <?php
                                if ($ViewTrack['trkfile_name_uniq'] != NULL) {
                                ?>

                                    <form action="<?php echo BASEURL ?>getdl.html" method="post" name="DL_<?php echo $ViewTrack['trkfile_name_uniq']; ?>" id="DL_<?php echo $ViewTrack['trkfile_name_uniq']; ?>">
                                        <input type="hidden" name="task" />
                                        <input type="hidden" name="fids" value="<?php echo $ViewTrack['id_trkfile']; ?>">
                                        <input type="hidden" name="fdir" value="Tracking_Files">
                                    </form>
                                    <a href="javascript:{}" onclick="document.getElementById('DL_<?php echo $ViewTrack['trkfile_name_uniq'] ?>').submit(); return false;"><?php echo $lang_818 ?></a>

                                    <!--
                                                        <a href ="javascript:submitbutton('ShowDL_<?php //echo $_SESSION['butts_Src']
                                                                                                    ?>','DL_<?php //echo $ViewTrack['nama_file_uniq']
                                                                                                            ?>');"><?php echo $lang_818 ?></a>
                                                        <a href="javascript:{}" onclick="document.getElementById('the_form').submit(); return false;">submit</a>

                                                                                  <a href="../unggah/<?php //echo $ViewTrack['nama_file_uniq']
                                                                                                        ?>" class="text-dark-gray">Attachment</a> -->
                                    <i class="material-icons icon-muted icon-20pt ml-2">folder</i>
                                <?php
                                }
                                ?>
                                <!--
                                                          -->
                            </div>

                            <div class="col-auto d-flex align-items-center" style="min-width: 10px;">
                                <div class="dropdown ml-auto">
                                    <a href="" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
                                    <?php
                                    //echo show_badge($ViewTrack['trkitm_status'], BADGE_STATUS);
                                    if ($ViewTrack['trkitm_status'] >= "2") {
                                    ?>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <!-- <div class="dropdown-item-text dropdown-item-text--lh">
                                                                    <a class="dropdown-item" href="#">Action</a>
                                                          			 col-auto d-flex  div><strong><?php //echo $_SESSION['lgnNama']
                                                                                                    ?></strong></di>
                                                                </div> -->
                                            <form action="" method="post" name="SendManual_<?php echo $ViewTrack['id_trkitm']; ?>" id="SendManual_<?php echo $ViewTrack['id_trkitm']; ?>" enctype="multipart/form-data">
                                                <input type="hidden" name="task" />
                                                <input type="hidden" name="tracking_id" value="<?php echo $tracking_id; ?>">
                                                <input type="hidden" name="trkitm_id" value="<?php echo $ViewTrack['id_trkitm']; ?>">
                                                <!--<input type="hidden" name="works_id" value="<?php //echo $ViewTrack['kode'];
                                                                                                ?>">-->
                                                <?php
                                                //echo '<input id="lchart_range" name="lchart_range" value="" type="text" class="form-control flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';  class=\"btn btn-danger ml-3\"><i class=\"material-icons mr-1\">access_time</i>".$name."
                                                ?>
                                            </form>
                                            <a href="javascript:submitbutton('SendEmailWA_<?php echo $_SESSION["butts_Enter"]; ?>','SendManual_<?php echo $ViewTrack['id_trkitm']; ?>');" id="SendEmailWA_<?php echo $_SESSION["butts_Enter"]; ?>" class="dropdown-item"><small><b>Kirim Email & WA</b></small></a>
                                            <!--class="dropdown-item text-muted">-->
                                        </div>
                                    <?php
                                    } else {
                                        //echo "&nbsp;";
                                    }
                                    ?>

                                </div>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>


    <?php
        $chkbox++;
    }
    ?>
</div>

<script>
    $(document).ready(function() {

        $("input[type='file']").change(function(e) {
            var fileName = e.target.files[0].name;
            var id = $(this).attr('id');

            $("#label_" + id).html(fileName);
        });

        $(".btn_copy").on("click", function(element) {
            $("#password_generated").select();
            document.execCommand("copy");
        });

        $(".btn_dl_tax").on("click", function(element) {
            var id = $(this).attr("data-id"),
                baseUrl = "<?= BASEURL ?>";

            $('#modalTax .modal-body #form_DL_Tax').empty();
            $('<form action="getdl.html" target="_blank" method="post"> \
                <input type="hidden" name="task" /> \
                <input type="hidden" name="fids" value="' + id + '"> \
                <input type="hidden" name="fdir" value="Attachment_Tax"> \
            </form>')
                .appendTo('#modalTax .modal-body #form_DL_Tax').submit();
        });

        $(".btn_dl_haki").on("click", function(element) {
            var id = $(this).attr("data-id"),
                baseUrl = "<?= BASEURL ?>";

            $('#modalHaki .modal-body #form_DL_Haki').empty();
            $('<form action="getdl.html" target="_blank" method="post"> \
                <input type="hidden" name="task" /> \
                <input type="hidden" name="fids" value="' + id + '"> \
                <input type="hidden" name="fdir" value="Attachment_Haki"> \
            </form>')
                .appendTo('#modalHaki .modal-body #form_DL_Haki').submit();
        });

        $(".btn_dl_bast").on("click", function(element) {
            var id = $(this).attr("data-id"),
                baseUrl = "<?= BASEURL ?>";

            $('#modalBast .modal-body #form_DL_Bast').empty();
            $('<form action="getdl.html" target="_blank" method="post"> \
                <input type="hidden" name="task" /> \
                <input type="hidden" name="fids" value="' + id + '"> \
                <input type="hidden" name="fdir" value="Attachment_Bast"> \
            </form>')
                .appendTo('#modalBast .modal-body #form_DL_Bast').submit();
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
</script>

<script type="application/javascript">
    function confirmation(pesan) {
        var selector = document.getElementById('trkbadge');
        //var value = selector[selector.selectedIndex].value;
        var value = selector.selectedIndex;
        if (value == '3') {
            if (confirm(pesan)) {
                submitbutton("<?php echo $act_update ?>", "<?php echo $form_update ?>");
            } else {
                event.preventDefault();
            }
        } else {
            //index not 3/done
            submitbutton("<?php echo $act_update ?>", "<?php echo $form_update ?>");
        }
    }
</script>