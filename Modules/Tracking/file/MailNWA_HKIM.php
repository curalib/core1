<?php

$Email_Subject = "✅ Update Process | Sertifikat Merek - Selesai";
$Email_Body = "<DIV style='max-width: 500px'><P>Kepada Yth,</P>
<p>" . $Company_Name . "</p>
<P>Salam dari IZIN.co.id!</P>

<p>Melalui notifikasi ini, Kami informasikan bahwa Sertifikat untuk " . $Company_Name . " telah selesai dan sudah bisa diunduh.  Mohon untuk melakukan pengecekan kembali terhadap sertifikat yang telah terbit, apabila terdapat penyesuaian atau kesalahan data mohon untuk menginformasikan kepada kami maksimal 2 (dua) hari kerja setelah email ini diterima.</p>

<P>Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya beserta password Anda.</P>

<P>Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id.</P>

<p>Silakan klik di sini untuk memberikan review Anda: <a href='http://bit.ly/ReviewIZIN'>http://bit.ly/ReviewIZIN</a>.</p>

<p>Terima kasih telah mempercayai dan menggunakan layanan IZIN.co.id. Selamat menjalankan perusahaan Anda, kami doakan bisnis Anda berjalan lancar dan sukses.</p>

<P>Salam Hangat,<BR/>
<A HREF='https://izin.co.id'>IZIN.co.id</A></P>

<BR/><HR>
<P class='text-muted'><i>Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id</i></P>
</DIV>
";

$Email_Alt_Body = "Kepada Yth,
" . $Company_Name . "

Salam dari IZIN.co.id!

Melalui notifikasi ini, Kami informasikan bahwa Sertifikat untuk " . $Company_Name . " telah selesai dan sudah bisa diunduh.  Mohon untuk melakukan pengecekan kembali terhadap sertifikat yang telah terbit, apabila terdapat penyesuaian atau kesalahan data mohon untuk menginformasikan kepada kami maksimal 2 (dua) hari kerja setelah email ini diterima.

Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya beserta password Anda.

Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id.

Silakan klik di sini untuk memberikan review Anda: http://bit.ly/ReviewIZIN.

Terima kasih telah mempercayai dan menggunakan layanan IZIN.co.id. Selamat menjalankan perusahaan Anda, kami doakan bisnis Anda berjalan lancar dan sukses.


Salam Hangat,
Tim IZIN.co.id


______________________________________________________________________________________

Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
";

$WA_Message = "Kepada Yth,
 " . $Company_Name . "

Salam dari IZIN CO ID!

Melalui notifikasi ini, Kami informasikan bahwa Sertifikat untuk " . $Company_Name . " telah selesai dan sudah bisa diunduh.  Mohon untuk melakukan pengecekan kembali terhadap sertifikat yang telah terbit, apabila terdapat penyesuaian atau kesalahan data mohon untuk menginformasikan kepada kami maksimal 2 (dua) hari kerja setelah email ini diterima.

Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya beserta password Anda.

Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id.

Silakan klik di sini untuk memberikan review Anda: http://bit.ly/ReviewIZIN.

Terima kasih telah mempercayai dan menggunakan layanan IZIN.co.id. Selamat menjalankan perusahaan Anda, kami doakan bisnis Anda berjalan lancar dan sukses.

Salam Hangat,
*Tim IZIN.co.id*

__________________________________________________________________________

Pesan ini adalah pesan otomatis – Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
";
