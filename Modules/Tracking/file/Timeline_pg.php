
<!--===========TIMELINE=========================================================-->

                            <div class="container-fluid page__heading-container">
                                <div class="page__heading">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb mb-0">
                                            <li class="breadcrumb-item"><a href="<?php echo $bchref?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101?></a></li>
                                            <li class="breadcrumb-item"><?php echo $lang_50010?></li>
                                            <li class="breadcrumb-item"><a href="<?php echo $bcDtTrack?>"><?php echo $lang_50100?></a></li>

                                            <li class="breadcrumb-item">
                                            </li>
                                            <form action="" method="post" name="ViewTracking" id ="ViewTracking">
                                                <input type="hidden" name="task" />
                                                <input type="hidden" name="tracking_id" value="<?php echo $tracking_id; ?>">
                                            </form>

                                            <li class="breadcrumb-item">
                                              <a href ="javascript:submitbutton('Track_<?php echo $_SESSION["butts_Enter"]?>_Detail','ViewTracking');"><?php echo $lang_50130?></a>
                                            </li>

                                            <!--<li class="breadcrumb-item"><a href="<?php //echo $bcTrack?>"><?php //echo $lang_50130?></a></li>-->

                                            <li class="breadcrumb-item active" aria-current="page"><?php echo $lang_50140?></li>
                                        </ol>
                                    </nav>
                                    <!--<h1 class="m-0"><?php echo $lang_50140?></h1>-->
                                </div>
                            </div>

                            <div class="container-fluid page__container">

                              <?php
                              if(isset($_SESSION["AlertMess"]) OR !empty($_SESSION["AlertMess"])){
                                  echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
                                      echo '<i class="material-icons mr-3">error_outline</i>';
                                      echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
                                  echo '</div>';
                                  //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                                  unset($_SESSION["AlertMess"]);
                              }
                              ?><!-- -->

                                <!-- Client Detail   justify-content-between flex-fill   style="width: 18rem;"-->
                                <div class="card card-form w-auto bg-light ">
                                    <div class="card-body card-form__body flex " style="padding-top:15px;padding-bottom:10px;"><!--  w-auto d-flex -->
                                        <!--<div class="card-form__body card-body-form-group flex bg-secondary flex-shrink-1 justify-content-end flex-column flex-row-reverse">-->
                                        <!--<div class="card-form__body card-body-form-group d-flex flex-column flex-shrink-1 ">-->
                                        <div class="row"><!-- w-100 -->
                                            <div class="col-sm-auto flex "><!-- d-flex flex-column w-25 -->
                                                <label ><?php echo $lang_50201?></label>
                                                <h6><?php echo $tracking_id; //$ViewClient['client_id']?></h6><!--  class="col-sm-auto flex"  -->
                                            </div>

                                            <div class="col-sm-auto flex ">
                                                <label ><?php echo $lang_50202?></label>
                                                <h6><?php echo $ViewClient['client_name']?> </h6><!-- class="col-sm-auto flex" -->
                                            </div>

                                            <div class="col-sm-auto flex "><!-- -->
                                                <label ><?php echo $lang_50203?></label>
                                                <h6><?php echo $ViewClient['package_name']?> </h6>
                                            </div>

                                            <div class="col-sm-auto flex "><!-- -->
                                            <!--<div class="col-sm-auto">
                                                <div class="form-group">-->
                                                <label ><?php echo $lang_50204?></label>
                                                <h6><?php echo $ViewClient['company_name']?> </h6>
                                                <!--</div>
                                            </div>-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Client Detail -->


                                <div class="card card-form" >
                                    <div class="row no-gutters">
                                        <div class="col-lg-12 card-form__body">


                                            <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                <table class="table mb-0 thead-border-top-0">
                                                    <thead>
                                                        <tr>
                                                          <!--
                                                            <th style="width: 18px;">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input js-toggle-check-all" data-target="#staff" id="customCheckAll">
                                                                    <label class="custom-control-label" for="customCheckAll"><span class="text-hide">Toggle all</span></label>
                                                                </div>
                                                            </th>
                                                          -->
                                                            <th style="width: 10%;"><div class="wiz_ls-20">Item</div></th>
                                                            <th style="width: 13%;">Duration</th>
                                                            <!--elapsed-->
                                                            <th style="width: 37%;">Note</th>
                                                            <th style="width: 15%;">Business Day</th>
                                                            <th style="width: 15%;">Last Activity</th>
                                                            <th style="width: 10%;">Status</th>
                                                            <!--<th style="width: 24px;"></th>-->
                                                        </tr>
                                                    </thead>
                                                    <tbody class="list" id="staff">



          <?php
          while($LoadTrkItem = $dbs->getAssoc($rcLoadTrkItem)) {
          ?>
          <tr class="selected">
            <!--
              <td>
                  <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_1">
                      <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                  </div>
              </td>
            -->
              <td><div class="wiz_ls-20"><?php echo $LoadTrkItem["kode"]?></div></td>
              <td><?php echo $LoadTrkItem["wkt"]?></td>

<?php
/*
              $start_date_db = $LoadTrkItem["tgl_start"];
              $office_closinghour = 17;
              $duration = $LoadTrkItem["waktu_urus"];
              $holidays = get_indonesian_holidays("2019",$dbs);
              $expected_date = get_a_business_day_date($start_date_db, $duration, $office_closinghour, $holidays);
              echo $expected_date;
*/
?>
              <td><?php echo $LoadTrkItem["nama"]?></td>
              <td>
                  <small class="text-muted">
                    <?php
                    $LastActivityInDays = $HitDays->DayRange(convertDate($LoadTrkItem["tgl_update_itm"]), "now", false)->Result();
                    echo $LastActivityInDays[2];
                    ?>
                <?php
/*
                $start_date_db = $LoadTrkItem["tgl_update_itm"];
                $office_closinghour = 17;
                $holidays = get_indonesian_holidays($dbs);
                //$holidays = get_indonesian_holidays("2019",$dbs);
                $duration = $LoadTrkItem["wkt"];
//print_r($holidays);
//echo $start_date_db;
//exit(0);
//$start_date_db = '2019-03-24 11:47';
                //$start_date_db = '2019-03-24 11:47';
                //$print_business_days = get_duration_days($start_date_db, $office_closinghour, $holidays);
                $print_business_days = get_duration_days($start_date_db, $office_closinghour, $holidays, $duration);
                //$elapsed_days = get_duration_days($start_date_db, $office_closinghour, $holidays);
                //$print_working_days = get_duration_days_note($start_date_db);
                //$print_business_days = $elapsed_days;
                //$print_jumlah_waktu = $elapsed_days;
                //$print_keterangan_waktu = get_duration_days_note($start_date_db);
                //$print_working_days = $print_jumlah_waktu." ".$print_keterangan_waktu;
                //echo $elapsed_days;
                //exit(0);
                echo $print_business_days;
*/
/*
                if($elapsed_days > $LoadTrkItem["waktu_urus"]){
                    //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                    echo '<div class="text-danger">'.$print_working_days.'</div>';
                } else {
                    echo $print_working_days;
                    //echo $start_date_db;
                }
                //echo date("m-d");
*/
                ?>
                  </small>
              </td>



              <td>
                  <small class="text-muted"><span data-time-format="time-ago" data-time-value="<?php echo $LoadTrkItem['tgl_update_itm']?>"></span></small>
              </td>



              <td>
                <?php
                    echo show_badge($LoadTrkItem['trkitm_status'], BADGE_STATUS);
                ?>
                <!--<span class="badge badge-success">DONE</span>-->
              </td>
              <?php //echo $notif_badge[$LoadTrkItem['notif_badge']]?>
              <!--<td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>-->
          </tr>
          <?php
              $chkbox++;
          }
          ?>





<!--
                                                        <tr class="selected">

                                                            <td>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_1">
                                                                    <label class="custom-control-label" for="customCheck1_1"><span class="text-hide">Check</span></label>
                                                                </div>
                                                            </td>

                                                            <td>

                                                            Akta dan SK

                                                            </td>


                                                            <td>10</td>
                                                            <td>setelah tanda tangan </td>
                                                            <td><small class="text-muted">3 days ago</small></td>
                                                            <td><span class="badge badge-success">DONE</span></td>
                                                            <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                        </tr>
                                                        <tr>

                                                            <td>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck2_1">
                                                                    <label class="custom-control-label" for="customCheck2_1"><span class="text-hide">Check</span></label>
                                                                </div>
                                                            </td>

                                                            <td>

                                                            NPWP

                                                            </td>


                                                            <td>4</td>
                                                            <td>setelah Akta dan SK selesai</td>
                                                            <td><small class="text-muted">1 week ago</small></td>
                                                            <td><span class="badge badge-warning">ON GOING</span></td>
                                                            <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                        </tr>
                                                        <tr>

                                                            <td>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck3_1">
                                                                    <label class="custom-control-label" for="customCheck3_1"><span class="text-hide">Check</span></label>
                                                                </div>
                                                            </td>

                                                            <td>
                                                            SKDP Online

                                                            </td>


                                                            <td>10</td>
                                                            <td>setelah NPWP selesai (parallel)</td>
                                                            <td><small class="text-muted">1 week ago</small></td>
                                                            <td><span class="badge badge-warning">ON GOING</span></td>
                                                            <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                        </tr>

                                                        <tr class="selected">

                                                            <td>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck1_2">
                                                                    <label class="custom-control-label" for="customCheck1_2"><span class="text-hide">Check</span></label>
                                                                </div>
                                                            </td>

                                                            <td>

                                                            NIB

                                                            </td>


                                                            <td>5</td>
                                                            <td>setelah NPWP selesai (parallel)</td>
                                                            <td><small class="text-muted">3 days ago</small></td>
                                                            <td><span class="badge badge-secondary">WAITING</span></td>
                                                            <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                        </tr>
                                                        <tr>

                                                            <td>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input js-check-selected-row" id="customCheck2_2">
                                                                    <label class="custom-control-label" for="customCheck2_2"><span class="text-hide">Check</span></label>
                                                                </div>
                                                            </td>

                                                            <td>

                                                            SIUP

                                                            </td>


                                                            <td>5</td>
                                                            <td>setelah NPWP selesai (parallel)</td>
                                                            <td><small class="text-muted">1 week ago</small></td>
                                                            <td><span class="badge badge-secondary">WAITING</span></td>
                                                            <td><a href="" class="text-muted"><i class="material-icons">more_vert</i></a></td>
                                                        </tr>
-->




                                                    </tbody>
                                                </table>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>



                            <script>
                                $( document ).ready(function() {
                                    // time formatter
                                    $("[data-time-format]").each(function() {
                                        var el = $( this );
                                        switch(el.attr("data-time-format")) {
                                            case "time-ago":
                                              var timeValue = el.attr("data-time-value");
                                              var strTimeAgo = moment(timeValue).fromNow();
                                              el.text(strTimeAgo);
                                            break;
                                        }
                                    });//$( "[data-time-format]" ).each(function() {

                                    //$('#customCheckAll').click(function(event) {
                                    //  $(':checkbox').prop('checked', this.checked);
                                    //});

                                });//$( document ).ready(function() {

                            </script>


<!--===========TIMELINE=========================================================-->
