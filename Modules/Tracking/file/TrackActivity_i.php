<?php


if(isset($_SESSION["ActivityPage"])) {
 unset($_SESSION["ActivityPage"]);
}
//if(isset($_SESSION["showtrackingid"])) {
// unset($_SESSION["showtrackingid"]);
//}


//BREADCRUMB
$cBrdCrumb = "SELECT tgl FROM ".$tblp."sys_menu WHERE `module` = 'System' AND `page` = 'Dashboard'";
$rcBrdCrumb = $dbs->getQuery($cBrdCrumb);
$BrdCrumb = $dbs->getAssoc($rcBrdCrumb);
$bcsplitKeys = SplitKeys($BrdCrumb['tgl']);
$bchref = BASEURL.$bcsplitKeys."/0/0.html";

//echo "hhh1";
//exit;
//$Kode_Kantor = $_SESSION["Dash_KodeKantor"];


/******************************************
 * Track Activity
 ******************************************/

// RECENT ACTIVITY DATA
//-------------------------------------------------
/*
//ALL
$cRecActAllTrack = "SELECT `trkitm`.`updater_name`, `trkitm`.`tgl_update_itm`, `trkitm`.`reason_title`, `trkitm`.`trkitm_status`, `lgndetail`.`avatar`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cRecActAllTrack .= "INNER JOIN `izin_sys_lgndetail` AS `lgndetail` ON `trkitm`.`updater_id` = `lgndetail`.`id_login` ";
$cRecActAllTrack .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
//$cRecActAllLead .= "WHERE `KodeKantor` = '".$Kode_Kantor."' ";
$cRecActAllTrack .= "WHERE str_to_date(`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."' ";
$cRecActAllTrack .= "ORDER BY `tgl_update_itm` ASC ";
$rcRecActAllTrack = $dbs->getQuery($cRecActAllTrack);

//ONGOING
$cRecActOGTrack = "SELECT `trkitm`.`updater_name`, `trkitm`.`tgl_update_itm`, `trkitm`.`reason_title`, `trkitm`.`trkitm_status`, `lgndetail`.`avatar`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cRecActOGTrack .= "INNER JOIN `izin_sys_lgndetail` AS `lgndetail` ON `trkitm`.`updater_id` = `lgndetail`.`id_login` ";
$cRecActOGTrack .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cRecActOGTrack .= "WHERE str_to_date(`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."' ";
$cRecActOGTrack .= "AND `trkitm`.`trkitm_status` = 1 ";
$rcRecActOGTrack = $dbs->getQuery($cRecActOGTrack);

//WAITING
$cRecActWaitTrack = "SELECT `trkitm`.`updater_name`, `trkitm`.`tgl_update_itm`, `trkitm`.`reason_title`, `trkitm`.`trkitm_status`, `lgndetail`.`avatar`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cRecActWaitTrack .= "INNER JOIN `izin_sys_lgndetail` AS `lgndetail` ON `trkitm`.`updater_id` = `lgndetail`.`id_login` ";
$cRecActWaitTrack .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cRecActWaitTrack .= "WHERE str_to_date(`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."' ";
$cRecActWaitTrack .= "AND `trkitm`.`trkitm_status` = 2 ";
$rcRecActWaitTrack = $dbs->getQuery($cRecActWaitTrack);

//DONE
$cRecActDoneTrack = "SELECT `trkitm`.`updater_name`, `trkitm`.`tgl_update_itm`, `trkitm`.`reason_title`, `trkitm`.`trkitm_status`, `lgndetail`.`avatar`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cRecActDoneTrack .= "INNER JOIN `izin_sys_lgndetail` AS `lgndetail` ON `trkitm`.`updater_id` = `lgndetail`.`id_login` ";
$cRecActDoneTrack .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cRecActDoneTrack .= "WHERE str_to_date(`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."' ";
$cRecActDoneTrack .= "AND `trkitm`.`trkitm_status` = 3 ";
$rcRecActDoneTrack = $dbs->getQuery($cRecActDoneTrack);

//echo "sini";
//exit;

//CURRENT STATUS DONE
//$status_done = 3;
$cCurStatTrack = "SELECT `trklist`.`tracking_id`, `trklist`.`client_name`, `trklist`.`company_name`, `trklist`.`trk_status` FROM `izin_apps_trklist` AS `trklist` ";
$cCurStatTrack .= "WHERE `trklist`.`trk_status` = ".$track_status." ";
//$cCurStatTrack .= "AND `trklist`.`trk_status` = ".$status_done;
if(isset($filter_by_name) && !empty($filter_by_name)) {
    $cCurStatTrack .= "AND `trklist`.`client_name`  LIKE '%".$filter_by_name."%' ";
}
$cCurStatTrack .= "AND str_to_date(`tgl_start`, '%Y-%m-%d') BETWEEN '".$date_from."' AND '".$date_to."' ";
//echo "sini ".$cCurStatTrack;
//exit;
$rcCurStatTrack = $dbs->getQuery($cCurStatTrack);
*/
/******************************************
 * End of Track Activity
 ******************************************/


 /******************************************
  * Permit Today
  ******************************************/

//DONE ARRAY
$array_done = array();
$array_remove = array();
$array_waiting = array();
$array_ongoing = array();

//DROPDOWN DONE
$cWorksDone = "SELECT `works`.`id`, `works`.`kode`, `works`.`urutan` FROM `izin_apps_works` AS `works`  ";
$cWorksDone .= "WHERE `works`.`kode` <> 'BNRI' ";
$cWorksDone .= "ORDER BY `works`.`urutan` ";
$rcWorksDone = $dbs->getQuery($cWorksDone);

$done_i = 0;
/*
$cPerTodayDone2 = "SELECT COUNT(`trklist`.`tracking_id`) AS jumlah FROM `izin_apps_trkitm` AS `trkitm` ";
//$cPerTodayDone .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayDone2 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayDone2 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') = '".$date_now."' ";
if(isset($works_id) && !empty($works_id)  && $works_id != "All Item") {
  //$cPerTodayDone .= "AND `works`.`id` = '".$works_id."' ";
  $cPerTodayDone2 .= "AND `trkitm`.`works_id` = '".$works_id."' ";
}
$cPerTodayDone2 .= "AND `trkitm`.`trkitm_status` = 3 ";
//$cPerTodayDone .= "GROUP BY `works`.`kode` ";
//$cPerTodayDone .= "ORDER BY `works`.`urutan` ";

$cPerTodayDone = "SELECT `trklist`.`tracking_id`, `trklist`.`company_name`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayDone .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayDone .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayDone .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') = '".$date_now."' ";
if(isset($works_id) && !empty($works_id)  && $works_id != "All Item") {
  $cPerTodayDone .= "AND `works`.`id` = '".$works_id."' ";
}
$cPerTodayDone .= "AND `trkitm`.`trkitm_status` = 3 ";
//$cPerTodayDone .= "GROUP BY `works`.`kode` ";
$cPerTodayDone .= "ORDER BY `works`.`urutan` ";
//echo $cPerTodayDone;
//exit;
//$rcPerTodayDone = $dbs->getQuery($cPerTodayDone);

$d = new paging ($dbs,PAGE_ITEM,PAGE_LIST,-1,"LstPermitTodayDone");
//echo $cPerTodayDone2;
//exit;
$d->queryTracking($cPerTodayDone,$cPerTodayDone2);

//echo $cPerTodayDone;
//exit;
*/

// Semua item yang telah done untuk masing2 tracking id.
$cPerTodayDone3 = "SELECT `trkitm`.`id_trkitm`, `trklist`.`tracking_id`, `trklist`.`company_name`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayDone3 .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayDone3 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayDone3 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$ra_date_from."' AND '".$ra_date_to."' ";
//$cPerTodayDone3 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') = '".$date_now."' ";
if(isset($works_id_d) && !empty($works_id_d)  && $works_id_d != "All Item") {
  $cPerTodayDone3 .= "AND `works`.`id` = '".$works_id_d."' ";
}
$cPerTodayDone3 .= "AND `trkitm`.`trkitm_status` = 3 ";
$cPerTodayDone3 .= "ORDER BY `works`.`urutan` ";
$rcPerTodayDone3 = $dbs->getQuery($cPerTodayDone3);
while($PerTodayDone3 = $dbs->getAssoc($rcPerTodayDone3)) {
  $array_done[] = $PerTodayDone3["id_trkitm"];
  // DETECTOR UNTUK cek apabila waiting untuk works dan lead dimaksud telah done maka lewatkan waiting nya.
  $array_remove[] = $PerTodayDone3["tracking_id"].",".$PerTodayDone3["kode"];
}
$done_list = "'". implode("', '", $array_done) ."'";
//Hitung jumlah data
$cPerTodayDone2 = "SELECT COUNT(`trklist`.`tracking_id`) AS jumlah FROM `izin_apps_trkitm` AS `trkitm` ";
//$cPerTodayDone .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayDone2 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayDone2 .= "WHERE `trkitm`.`id_trkitm` IN (".$done_list.") ";
//Data untuk display
$cPerTodayDone = "SELECT `trkitm`.`tgl_update_itm`, `trklist`.`tracking_id`, `trklist`.`company_name`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayDone .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayDone .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayDone .= "WHERE `trkitm`.`id_trkitm` IN (".$done_list.") ";
//$cPerTodayDone .= "ORDER BY `works`.`urutan` ";
$cPerTodayDone .= "ORDER BY `trkitm`.`tgl_update_itm` ";
//Tampilkan dengan paging
$d = new paging ($dbs,PAGE_ITEM,PAGE_LIST,-1,"LstPermitTodayDone");
$d->queryTracking($cPerTodayDone,$cPerTodayDone2);


//DROPDOWN WAITING
$cWorksWait = "SELECT `works`.`id`, `works`.`kode`, `works`.`urutan` FROM `izin_apps_works` AS `works`  ";
$cWorksWait .= "WHERE `works`.`kode` <> 'BNRI' ";
$cWorksWait .= "ORDER BY `works`.`urutan` ";
$rcWorksWait = $dbs->getQuery($cWorksWait);

$waiting_i = 0;
// ada detector yang cek apabila waiting untuk works dan lead dimaksud telah done maka lewatkan waiting nya.
$cPerTodayWaiting3 = "SELECT `trkitm`.`id_trkitm`, `trklist`.`tracking_id`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayWaiting3 .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayWaiting3 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayWaiting3 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$ra_date_from."' AND '".$ra_date_to."' ";
//$cPerTodayWaiting3 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') = '".$date_now."' ";
if(isset($works_id_w) && !empty($works_id_w)  && $works_id_w != "All Item") {
  $cPerTodayWaiting3 .= "AND `works`.`id` = '".$works_id_w."' ";
}
$cPerTodayWaiting3 .= "AND `trkitm`.`trkitm_status` = 2 ";
$cPerTodayWaiting3 .= "ORDER BY `works`.`urutan` ";
$rcPerTodayWaiting3 = $dbs->getQuery($cPerTodayWaiting3);
while($PerTodayWaiting3 = $dbs->getAssoc($rcPerTodayWaiting3)) {
    $array_waiting[] = $PerTodayWaiting3["id_trkitm"];
    foreach ($array_remove as $arr_key => $arr_value) {
        $exp_done = explode(',',$arr_value);
        // Kalau waiting pada tracking dan kode item dimaksud sudah ada yang done maka hapus dari daftar waiting nya
        if($exp_done[0] == $PerTodayWaiting3["tracking_id"] && $exp_done[1] == $PerTodayWaiting3["kode"]) {
            // ambil key dari array waiting
            if (($key_wait = array_search($PerTodayWaiting3["id_trkitm"], $array_waiting)) !== false) {
                //hapus item nya
                unset($array_waiting[$key_wait]);
            }
        }
    }

}
$wait_list = "'". implode("', '", $array_waiting) ."'";

$cPerTodayWaiting2 = "SELECT COUNT(`trklist`.`tracking_id`) AS jumlah FROM `izin_apps_trkitm` AS `trkitm` ";
//$cPerTodayWaiting2 .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayWaiting2 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayWaiting2 .= "WHERE `trkitm`.`id_trkitm` IN (".$wait_list.") ";

$cPerTodayWaiting = "SELECT `trkitm`.`reason_title`, `trkitm`.`tgl_update_itm`, `trklist`.`tracking_id`, `trklist`.`company_name`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayWaiting .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayWaiting .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayWaiting .= "WHERE `trkitm`.`id_trkitm` IN (".$wait_list.") ";
$cPerTodayWaiting .= "ORDER BY `works`.`urutan` ";

$w = new paging ($dbs,PAGE_ITEM,PAGE_LIST,-1,"LstPermitTodayWait");
$w->queryTracking($cPerTodayWaiting,$cPerTodayWaiting2);








//DROPDOWN ONGOING
$cWorksOngoing = "SELECT `works`.`id`, `works`.`kode`, `works`.`urutan` FROM `izin_apps_works` AS `works`  ";
$cWorksOngoing .= "WHERE `works`.`kode` <> 'BNRI' ";
$cWorksOngoing .= "ORDER BY `works`.`urutan` ";
$rcWorksOngoing = $dbs->getQuery($cWorksOngoing);

$ongoing_i = 0;



$cPerTodayOngoing3 = "SELECT `trkitm`.`id_trkitm`, `trklist`.`tracking_id`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayOngoing3 .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayOngoing3 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayOngoing3 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') BETWEEN '".$ra_date_from."' AND '".$ra_date_to."' ";
//$cPerTodayOngoing3 .= "WHERE str_to_date(`trkitm`.`tgl_update_itm`, '%Y-%m-%d') = '".$date_now."' ";
if(isset($works_id_o) && !empty($works_id_o)  && $works_id_o != "All Item") {
  $cPerTodayOngoing3 .= "AND `works`.`id` = '".$works_id_o."' ";
}
$cPerTodayOngoing3 .= "AND `trkitm`.`trkitm_status` = 1 ";
$cPerTodayOngoing3 .= "ORDER BY `works`.`urutan` ";
$rcPerTodayOngoing3 = $dbs->getQuery($cPerTodayOngoing3);
while($PerTodayOngoing3 = $dbs->getAssoc($rcPerTodayOngoing3)) {
    $array_ongoing[] = $PerTodayOngoing3["id_trkitm"];
    foreach ($array_remove as $arr_key => $arr_value) {
        $exp_done = explode(',',$arr_value);
        // Kalau ongoing pada tracking dan kode item dimaksud sudah ada yang done maka hapus dari daftar ongoing nya
        if($exp_done[0] == $PerTodayOngoing3["tracking_id"] && $exp_done[1] == $PerTodayOngoing3["kode"]) {
            // ambil key dari array waiting
            if (($key_wait = array_search($PerTodayOngoing3["id_trkitm"], $array_ongoing)) !== false) {
                //hapus item nya
                unset($array_ongoing[$key_wait]);
            }
        }
    }

}
$ongoing_list = "'". implode("', '", $array_ongoing) ."'";

$cPerTodayOngoing2 = "SELECT COUNT(`trklist`.`tracking_id`) AS jumlah FROM `izin_apps_trkitm` AS `trkitm` ";
//$cPerTodayOngoing2 .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayOngoing2 .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayOngoing2 .= "WHERE `trkitm`.`id_trkitm` IN (".$ongoing_list.") ";

$cPerTodayOngoing = "SELECT `trkitm`.`reason_title`, `trkitm`.`tgl_update_itm`, `trklist`.`tracking_id`, `trklist`.`company_name`, `works`.`kode` FROM `izin_apps_trkitm` AS `trkitm` ";
$cPerTodayOngoing .= "INNER JOIN `izin_apps_works` AS `works` ON `trkitm`.`works_id` = `works`.`id` ";
$cPerTodayOngoing .= "INNER JOIN `izin_apps_trklist` AS `trklist` ON `trkitm`.`trklist_id` = `trklist`.`id_trklist` ";
$cPerTodayOngoing .= "WHERE `trkitm`.`id_trkitm` IN (".$ongoing_list.") ";
$cPerTodayOngoing .= "ORDER BY `works`.`urutan` ";

$o = new paging ($dbs,PAGE_ITEM,PAGE_LIST,-1,"LstPermitTodayOngoing");
$o->queryTracking($cPerTodayOngoing,$cPerTodayOngoing2);


//echo "done: ".$works_id_d."<br/>";
//echo "done: ".$works_id_w."<br/>";
//echo "done: ".$works_id_o."<br/>";
//exit;




  /******************************************
   * End of Permit Today
   ******************************************/



?>
