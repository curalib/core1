<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "NIB completed";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        Congratulation, all your company documents have been completed.
        <br><br>
        We attach your completed company NIB (Business Identification Number).
        <br><br>
        Standard Business Licensing for low risk business sectors is NIB, while for high risk business sectors requires a special permit issued by the relevant agency/ministry. For companies with medium and high risk business sectors, you’ll received standard company certificate.
        <br><br>
        For further information about document pick-up and delivery, please contact our customer service
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

$WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! NIB (Business Identification Number) has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Thank You.

Warm regards,
*Invest In Asia team*
";

} else {
    $Email_Subject = "Update Process - NIB  ".$Company_Name." Selesai";

    $Email_Body = "Kepada Yth,
    <br><br>
    Salam dari IZIN.co.id!
    <br><br>
    Melalui notifikasi ini, kami informasikan bahwa NIB (Nomor Induk Berusaha) untuk $Company_Name telah selesai dan sudah bisa diunduh.
    <br><br>
    NIB adalah nomor identitas pelaku usaha dalam pelaksanaan kegiatan usaha dan berfungsi sebagai pengganti TDP (Tanda Daftar Perusahaan).
    <br><br>
    Untuk mengunduh soft file dokumen izin Anda, silakan klik <a href='https://tracking.izin.co.id'>https://tracking.izin.co.id</a> dan masukkan Kode Tracking yang sudah dikirimkan di email sebelumnya beserta password Anda.
    <br><br>
    Dengan demikian seluruh proses perizinan telah selesai.
    <br><br>
    Pengambilan dokumen dapat diambil di kantor kami di :
    <br>
    Gedung Atria Tower @Sudirman Lt. 23, Jl. Jend. Sudirman Kav. 33 A, RT. 003 RW. 002
    Karet Tengsin, Tanah Abang, Jakarta Pusat, DKI Jakarta - 10220
    <br><br>
    Atau dapat menghubungi Customer Service kami melalui nomor +62 822-9998-0011 (hanya Whatsapp, buka Senin - Jumat pukul 09.00 - 17.00 WIB). Apabila ingin mengambil dokumen, mohon untuk menginformasikan kepada kami agar kami bisa mempersiapkan dokumennya.
    <br><br>
    Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id.
    <br><br>
    Silakan klik di sini untuk memberikan review Anda: <a href='http://search.google.com/local/writereview?placeid=ChIJFwAAAPnzaS4RvliqYwgBDT4'>http://search.google.com/local/writereview?placeid=ChIJFwAAAPnzaS4RvliqYwgBDT4</a>
    <br><br>
    Terima kasih telah mempercayai dan menggunakan layanan IZIN.co.id. Selamat menjalankan perusahaan Anda, kami doakan bisnis Anda berjalan lancar dan sukses.
    <br><br><br><br>
    Salam Hangat,<br>
    <strong>IZIN.co.id</strong>
    <br><br>";

$WA_Message = "Kepada Yth,

Salam dari IZIN.co.id!

Melalui notifikasi ini, kami informasikan bahwa NIB (Nomor Induk Berusaha) untuk $Company_Name telah selesai dan sudah bisa diunduh.

NIB adalah nomor identitas pelaku usaha dalam pelaksanaan kegiatan usaha dan berfungsi sebagai pengganti TDP (Tanda Daftar Perusahaan).

Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking yang sudah dikirimkan di email sebelumnya beserta password Anda.

Dengan demikian seluruh proses perizinan telah selesai.

Pengambilan dokumen dapat diambil di kantor kami di :
Gedung Atria Tower @Sudirman Lt. 23, Jl. Jend. Sudirman Kav. 33 A, RT. 003 RW. 002
Karet Tengsin, Tanah Abang, Jakarta Pusat, DKI Jakarta - 10220

Atau dapat menghubungi Customer Service kami melalui nomor +62 822-9998-0011 (hanya Whatsapp, buka Senin - Jumat pukul 09.00 - 17.00 WIB). Apabila ingin mengambil dokumen, mohon untuk menginformasikan kepada kami agar kami bisa mempersiapkan dokumennya.

Kami sangat berterima kasih jika Anda berkenan memberi review tentang layanan IZIN.co.id. Feedback Anda membantu kami untuk terus menjaga dan meningkatkan kualitas layanan IZIN.co.id.

Silakan klik di sini untuk memberikan review Anda: http://search.google.com/local/writereview?placeid=ChIJFwAAAPnzaS4RvliqYwgBDT4

Terima kasih telah mempercayai dan menggunakan layanan IZIN.co.id. Selamat menjalankan perusahaan Anda, kami doakan bisnis Anda berjalan lancar dan sukses.


Salam Hangat,
*IZIN.co.id*

__________________________________________________________________________

Pesan ini adalah pesan otomatis - Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
";

}