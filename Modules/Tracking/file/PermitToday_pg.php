
<!--===========TRACK ACTIVITY=========================================================-->


                    <div class="container-fluid page__heading-container">
                        <div class="page__heading d-flex align-items-center">
                            <div class="flex">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb mb-0">
                                        <li class="breadcrumb-item"><a href="<?php echo $bchref?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101?></a></li>
                                        <li class="breadcrumb-item"><?php echo $lang_50010?></li>
                                        <li class="breadcrumb-item active" aria-current="page"><?php echo $lang_50400?></li>
                                    </ol>
                                </nav>
                                <h1 class="m-0"><?php echo $lang_50400?></h1>
                            </div>
                        </div>
                    </div>



                    <div class="container-fluid page__container">


                        <?php

                        if(isset($_SESSION["AlertMess"]) OR !empty($_SESSION["AlertMess"])){
                            echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
                                echo '<i class="material-icons mr-3">error_outline</i>';
                                echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
                            echo '</div>';
                            //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                            unset($_SESSION["AlertMess"]);
                        }

                        ?><!-- -->




                                    <div class="row">
                                        <div class="col-lg">
                                            <div class="card">
                                                <div class="card-header card-header-large bg-white d-flex align-items-center">
                                                    <h4 class="card-header__title flex m-0"><?php echo date("l, d M Y") ?></h4>


            <form action="" method="post" name="UpdateDateRange" id ="UpdateDateRange" enctype="multipart/form-data">
                <input type="hidden" name="task" />
                <input type="hidden" name="lead_status" value="<?php echo $lead_status?>" />
                <!--<input type="hidden" name="ra_range" value="<?php //echo $ra_date_range?>" />-->

                <input type="hidden" name="selPermitDoneItem" value="<?php echo $works_id_d ?>"/>
                <input type="hidden" name="selPermitWaitItem" value="<?php echo $works_id_w ?>"/>
                <input type="hidden" name="selPermitOngoingItem" value="<?php echo $works_id_o ?>"/>

            <?php
            echo '<input id="ra_range" name="ra_range" value="" type="text" class="flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
            ?>
            </form>


                                                </div>
                                                <div class="form-group text-center">
                                    <div class="card-header card-header-tabs-basic nav justify-content-center " role="tablist">
                                        <a href="#activity_done" <?php echo $DoneTab==true ? 'class="active"' : ''?> data-toggle="tab" role="tab" aria-controls="activity_done" aria-selected="<?php echo $DoneTab==true ? 'true' : 'false'?>"> DONE</a>
                                        <a href="#activity_ongoing" <?php echo $OngoingTab==true ? 'class="active"' : ''?> data-toggle="tab" role="tab" aria-controls="activity_ongoing" aria-selected="<?php echo $OngoingTab==true ? 'true' : 'false'?>">ONGOING</a>
                                        <a href="#activity_waiting" <?php echo $WaitTab==true ? 'class="active"' : ''?> data-toggle="tab" role="tab" aria-controls="activity_waiting" aria-selected="<?php echo $WaitTab==true ? 'true' : 'false'?>">WAITING</a>
                                    </div>
                                    <div class="list-group tab-content list-group-flush">

                                       <div id="activity_done" class="tab-pane <?php echo $DoneTab==true ? 'active show fade' : ''?>">

                                            <div class="card">

                                                    <div class="card-header bg-white">

                                                        <div class="row">
                                                        <!--<form class="form-inline">-->
                                                            <div class="col-sm-8">
                                                            <form class="form-inline" action="" method="post" name="PermitDoneItem" id ="PermitDoneItem">
                                                                <input type="hidden" name="task" />
                                                                <input type="hidden" name="selPermitOngoingItem" value="<?php echo $works_id_o ?>"/>
                                                                <input type="hidden" name="selPermitWaitItem" value="<?php echo $works_id_w ?>"/>
                                                                <input type="hidden" name="ra_range" value="<?php echo $ra_date_range?>" />

                                                                <label class="mr-sm-2" for="inlineFormFilterBy">Filter by:</label>

                                                                <label class="sr-only" for="selPermitDoneItem">Role</label>
                                                                <!--<select id="inlineFormRole" class="custom-select mb-2 mr-sm-2 mb-sm-0">-->
                                                                <select id="selPermitDoneItem" name="selPermitDoneItem" class="custom-select mb-2 mr-sm-2 mb-sm-0">
                                                                    <option value="All Item">All Item</option>
                                                                  <?php
                                                                  while( $WorksDone = $dbs->getAssoc($rcWorksDone)) {
                                                                      if(isset($works_id_d) && !empty($works_id_d) && $works_id_d == $WorksDone["id"]) {
                                                                          echo '<option value="'.$WorksDone["id"].'" selected>'.$WorksDone["kode"].'</option>';
                                                                      } else {
                                                                          echo '<option value="'.$WorksDone["id"].'" >'.$WorksDone["kode"].'</option>';
                                                                      }
                                                                  }
                                                                  ?>
                                                                </select>
                                                            </form>
                                                            </div>
                                                            <!-- align-middle my-auto align-self-center align-items-center  style="background-color: red" -->
                                                            <div class="col-sm-4 align-self-center text-right">
                                                            <small class="text-muted"><?php echo $d->data_info(); ?></small>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <!--
                                                    <div class="card-header card-header-large bg-white text-left">
                                                        <h4 class="card-header__title">AKTA</h4>
                                                    </div>
                                                  -->
                                                    <div class="table-responsive border-bottom text-left" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                        <table class="table mb-0 thead-border-top-0">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 10%;">No</th>
                                                                    <th style="width: 20%;">Tracking ID</th>
                                                                    <th style="width: 35%;">Nama PT</th>
                                                                    <th style="width: 20%;">Permit</th>
                                                                    <th style="width: 15%;">Last Update</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="list" id="staff">
                                                              <?php
                                                              //while( $PerTodayDone = $dbs->getAssoc($rcPerTodayDone)) {
                                                              while ( $PerTodayDone = $d->result_assoc() ){
                                                                $done_i++;

                                                                //$array_done[$PerTodayDone["tracking_id"]] = $PerTodayDone["kode"];
                                                              ?>
                                                                <tr class="">
                                                                    <!--<td><?php //echo $done_i ?></td>-->
                                                                    <td><?php echo $d->show_num() ?></td>
                                                                    <td><?php echo $PerTodayDone["tracking_id"] ?></td>
                                                                    <td><?php echo $PerTodayDone["company_name"] ?></td>
                                                                    <td><?php echo $PerTodayDone["kode"] ?></td>
                                                                    <td><small class="text-muted">
									<?php 
//$LastActivityInDays = $HitDays->DayRange(convertDate($PerTodayDone["tgl_update_itm"]),"now", false)->Result();
//echo $LastActivityInDays[2];
echo date("d-m-Y", strtotime($PerTodayDone["tgl_update_itm"]));
									?></small>
								    </td>
                                                                </tr>
                                                              <?php
                                                              }
                                                              ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
<!--
                                                    <div class="card-header card-header-large bg-white text-left">
                                                        <h4 class="card-header__title">NPWP</h4>
                                                    </div>
                                                    <div class="table-responsive border-bottom text-left" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                        <table class="table mb-0 thead-border-top-0">
                                                            <thead>
                                                                <tr>



                                                                    <th>No</th>
                                                                    <th>Tracking ID</th>
                                                                    <th>Nama PT</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="list" id="staff">

                                                                <tr class="selected">
                                                                    <td>1</td>
                                                                    <td>0100</td>
                                                                    <td>PT ABC</td>
                                                                 </tr>

                                                                 <tr class="selected">
                                                                    <td>2</td>
                                                                    <td>0101</td>
                                                                    <td>PT ABA SEJATI UTAMA</td>
                                                                 </tr>



                                                            </tbody>
                                                        </table>
                                                    </div>
                                                  -->

                                                </div>
                                                <?php
                                                echo $d->print_page(NULL);
                                                echo $d->form_print_page();
                                                ?>



                                                </div>



                                          <div id="activity_ongoing" class="tab-pane <?php echo $OngoingTab==true ? 'active show fade' : ''?>">

                                                        <div class="card">

                                                   <div class="card-header bg-white">
                                                       <div class="row">
                                                       <!--<form class="form-inline">-->
                                                           <div class="col-sm-8">
                                                       <form class="form-inline" action="" method="post" name="PermitOngoingItem" id ="PermitOngoingItem">
                                                           <input type="hidden" name="task" />
                                                           <input type="hidden" name="selPermitDoneItem" value="<?php echo $works_id_d ?>"/>
                                                           <input type="hidden" name="selPermitWaitItem" value="<?php echo $works_id_w ?>"/>
                                                           <input type="hidden" name="ra_range" value="<?php echo $ra_date_range?>" />

                                                           <label class="mr-sm-2" for="inlineFormFilterBy">Filter by:</label>

                                                           <label class="sr-only" for="selPermitWaitItem">Role</label>
                                                           <select id="selPermitOngoingItem" name="selPermitOngoingItem" class="custom-select mb-2 mr-sm-2 mb-sm-0">
                                                               <option value="All Item">All Item</option>
                                                             <?php
                                                             while( $WorksOngoing = $dbs->getAssoc($rcWorksOngoing)) {
                                                               if(isset($works_id_o) && !empty($works_id_o) && $works_id_o == $WorksOngoing["id"]) {
                                                                   echo '<option value="'.$WorksOngoing["id"].'" selected>'.$WorksOngoing["kode"].'</option>';
                                                               } else {
                                                                   echo '<option value="'.$WorksOngoing["id"].'" >'.$WorksOngoing["kode"].'</option>';
                                                               }
                                                             }
                                                             ?>
                                                           </select>
                                                       </form>
                                                     </div>
                                                     <!-- align-middle my-auto align-self-center align-items-center  style="background-color: red" -->
                                                     <div class="col-sm-4 align-self-center text-right">
                                                     <small class="text-muted"><?php echo $o->data_info(); ?></small>
                                                     </div>
                                                 </div>
                                                   </div>

                                                   <!--
                                                   <div class="card-header card-header-large bg-white text-left">
                                                       <h4 class="card-header__title">WAITING AKTA</h4>
                                                   </div>
                                                 -->
                                                   <div class="table-responsive border-bottom text-left" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                       <table class="table mb-0 thead-border-top-0">
                                                           <thead>
                                                               <tr>
                                                                   <th style="width: 10%;">No</th>
                                                                   <th style="width: 15%;">Tracking ID</th>
                                                                   <th style="width: 30%;">Nama PT</th>
                                                                   <th style="width: 10%;">Permit</th>
                                                                   <th style="width: 20%;">Reason</th>
                                                                   <th style="width: 15%;">Last Update</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody class="list" id="staff">

                                                             <?php
                                                             //while( $PerTodayWaiting = $dbs->getAssoc($rcPerTodayWaiting)) {
                                                             while($PerTodayOngoing = $o->result_assoc()) {
                                                               $ongoing_i++;

                                                               //$array_done[$PerTodayDone["tracking_id"]] = $PerTodayDone["kode"];
                                                               //if(array_key_exists($PerTodayWaiting["tracking_id"],$array_done) && $array_done[$PerTodayWaiting["tracking_id"]] == $PerTodayWaiting["kode"]) {
                                                                 // kalau ada di done maka skip
                                                               //} else {
                                                             ?>
                                                               <tr class="">
                                                                   <!--<td><?php //echo $ongoing_i ?></td>-->
                                                                   <td><?php echo $o->show_num() ?></td>
                                                                   <td><?php echo $PerTodayOngoing["tracking_id"] ?></td>
                                                                   <td><?php echo $PerTodayOngoing["company_name"] ?></td>
                                                                   <td><?php echo $PerTodayOngoing["kode"] ?></td>
                                                                   <td><?php echo $PerTodayOngoing["reason_title"] ?></td>
                                                                   <td><small class="text-muted"><?php $start_date = strtotime($PerTodayOngoing["tgl_update_itm"]); echo date("d-m-Y", $start_date); ?></small></td>
                                                               </tr>
                                                             <?php
                                                               //}
                                                             }
                                                             ?>
                                                           </tbody>
                                                       </table>
                                                   </div>
                                               </div>
                                               <?php
                                               echo $o->print_page(NULL);
                                               echo $o->form_print_page();
                                               ?>
                                          </div>





                                          <div id="activity_waiting" class="tab-pane <?php echo $WaitTab==true ? 'active show fade' : ''?>">

                                                        <div class="card">

                                                   <div class="card-header bg-white">
                                                       <div class="row">
                                                       <!--<form class="form-inline">-->
                                                           <div class="col-sm-8">
                                                       <form class="form-inline" action="" method="post" name="PermitWaitingItem" id ="PermitWaitingItem">
                                                           <input type="hidden" name="task" />
                                                           <input type="hidden" name="selPermitOngoingItem" value="<?php echo $works_id_o ?>"/>
                                                           <input type="hidden" name="selPermitDoneItem" value="<?php echo $works_id_d ?>"/>
                                                           <input type="hidden" name="ra_range" value="<?php echo $ra_date_range?>" />

                                                           <label class="mr-sm-2" for="inlineFormFilterBy">Filter by:</label>

                                                           <label class="sr-only" for="selPermitWaitItem">Role</label>
                                                           <select id="selPermitWaitItem" name="selPermitWaitItem" class="custom-select mb-2 mr-sm-2 mb-sm-0">
                                                               <option value="All Item">All Item</option>
                                                             <?php
                                                             while( $WorksWait = $dbs->getAssoc($rcWorksWait)) {
                                                               if(isset($works_id_w) && !empty($works_id_w) && $works_id_w == $WorksWait["id"]) {
                                                                   echo '<option value="'.$WorksWait["id"].'" selected>'.$WorksWait["kode"].'</option>';
                                                               } else {
                                                                   echo '<option value="'.$WorksWait["id"].'" >'.$WorksWait["kode"].'</option>';
                                                               }
                                                             }
                                                             ?>
                                                           </select>
                                                       </form>
                                                     </div>
                                                     <!-- align-middle my-auto align-self-center align-items-center  style="background-color: red" -->
                                                     <div class="col-sm-4 align-self-center text-right">
                                                     <small class="text-muted"><?php echo $w->data_info(); ?></small>
                                                     </div>
                                                 </div>
                                                   </div>

                                                   <!--
                                                   <div class="card-header card-header-large bg-white text-left">
                                                       <h4 class="card-header__title">WAITING AKTA</h4>
                                                   </div>
                                                 -->
                                                   <div class="table-responsive border-bottom text-left" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                       <table class="table mb-0 thead-border-top-0">
                                                           <thead>
                                                               <tr>
                                                                   <th style="width: 10%;">No</th>
                                                                   <th style="width: 15%;">Tracking ID</th>
                                                                   <th style="width: 30%;">Nama PT</th>
                                                                   <th style="width: 10%;">Permit</th>
                                                                   <th style="width: 20%;">Reason</th>
                                                                   <th style="width: 15%;">Pending Date</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody class="list" id="staff">

                                                             <?php
                                                             //while( $PerTodayWaiting = $dbs->getAssoc($rcPerTodayWaiting)) {
                                                             while($PerTodayWaiting = $w->result_assoc()) {
                                                               $waiting_i++;

                                                               //$array_done[$PerTodayDone["tracking_id"]] = $PerTodayDone["kode"];
                                                               //if(array_key_exists($PerTodayWaiting["tracking_id"],$array_done) && $array_done[$PerTodayWaiting["tracking_id"]] == $PerTodayWaiting["kode"]) {
                                                                 // kalau ada di done maka skip
                                                               //} else {
                                                             ?>
                                                               <tr class="">
                                                                   <!--<td><?php //echo $waiting_i ?></td>-->
                                                                   <td><?php echo $w->show_num() ?></td>
                                                                   <td><?php echo $PerTodayWaiting["tracking_id"] ?></td>
                                                                   <td><?php echo $PerTodayWaiting["company_name"] ?></td>
                                                                   <td><?php echo $PerTodayWaiting["kode"] ?></td>
                                                                   <td><?php echo $PerTodayWaiting["reason_title"] ?></td>
                                                                   <td><small class="text-muted"><?php $start_date = strtotime($PerTodayWaiting["tgl_update_itm"]); echo date("d-m-Y", $start_date); ?></small></td>
                                                               </tr>
                                                             <?php
                                                               //}
                                                             }
                                                             ?>
                                                           </tbody>
                                                       </table>
                                                   </div>

<!--
                                                   <div class="card-header card-header-large bg-white text-left">
                                                       <h4 class="card-header__title">NPWP</h4>
                                                   </div>
                                                   <div class="table-responsive border-bottom text-left" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>

                                                       <table class="table mb-0 thead-border-top-0">
                                                           <thead>
                                                               <tr>



                                                                   <th>No</th>
                                                                   <th>Tracking ID</th>
                                                                   <th>Nama PT</th>
                                                                   <th>Permit</th>
                                                                   <th>Reason</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody class="list" id="staff">

                                                               <tr class="selected">
                                                                   <td>1</td>
                                                                   <td>0100</td>
                                                                   <td>PT ABC</td>
                                                                   <td></td>
                                                                   <td>Kendala System PTSP</td>
                                                                </tr>

                                                                <tr class="selected">
                                                                   <td>2</td>
                                                                   <td>0101</td>
                                                                   <td>PT ABA SEJATI UTAMA</td>
                                                                   <td></td>
                                                                   <td>Klien belum melengkapi dokumen</td>
                                                                </tr>



                                                           </tbody>
                                                       </table>
                                                   </div>
-->

                                               </div>

                                               <?php
                                               echo $w->print_page(NULL);
                                               echo $w->form_print_page();
                                               ?>



                                                         </div>


                                                    </div>


                                                </div>

                                        </div>
                                        </div>
                                      </div>

                        </div>


                      <script>
                      $(document).ready(function() {


                          var now_date = moment().format("DD/MM/YYYY");
                          var amonth_ago = moment().subtract(7, 'days').format("DD/MM/YYYY");
                          $("#ra_range").flatpickr({
                              mode: "range",
                              altInput: true,
                              altFormat: "d/m/Y",
                              dateFormat: "d/m/Y",
                              allowInput: false,
                              <?php
                              if(isset($ra_date_fromS) && !empty($ra_date_fromS)) {
                                  echo 'defaultDate: ["'.$ra_date_fromS.'", "'.$ra_date_toS.'"],';
                              } else {
                                  echo 'defaultDate: [amonth_ago, now_date],';
                              }
                              // onChange onClose onValueUpdate
                              ?>
                              onClose: function(selectedDates, dateStr, instance) {
                                  //alert("hello "+dateStr);
                                  //$(this).updateLineChart();


                          //alert($('#hidRAvalue').text());
                          //alert($('#hidRAvalue').val());

                                  $(this).updateReAct("UpdateDateRange");
                              }
                              //event.preventDefault(); UpdateRecentAct
                          });


/*
                          var now_date = moment().format("DD/MM/YYYY");
                          var amonth_ago = moment().subtract(10, 'days').format("DD/MM/YYYY");

                          $("#react_range").flatpickr({
                              mode: "range",
                              altInput: true,
                              altFormat: "d/m/Y",
                              dateFormat: "d/m/Y",
                              allowInput: false,
                              <?php /*
                              if(isset($date_fromS) && !empty($date_fromS)) {
                                  echo 'defaultDate: ["'.$date_fromS.'", "'.$date_toS.'"],';
                              } else {
                                  echo 'defaultDate: [amonth_ago, now_date],';
                              }
                              // onChange onClose onValueUpdate
                              */
                              ?>
                              onClose: function(selectedDates, dateStr, instance) {
                                  $(this).updateReAct("UpdateRecentActivity");
                              }
                              //event.preventDefault();
                          });
*/
                          $.fn.updateReAct = function(form_to_send) {
                              var act_upReAct = form_to_send+"_<?php echo $_SESSION["butts_Update"] ?>";
                              submitbutton(act_upReAct,form_to_send);
                          }
                          $("#selPermitDoneItem").on("change", function() {
//alert("hello");
                              $(this).updateReAct("PermitDoneItem");
                          });
                          $("#selPermitWaitItem").on("change", function() {
                              $(this).updateReAct("PermitWaitingItem");
                          });
                          $("#selPermitOngoingItem").on("change", function() {
                              $(this).updateReAct("PermitOngoingItem");
                          });
                          $('#filter_by_name').on('keypress', function (e) {
                                  if(e.which === 13){
                                     //Disable textbox to prevent multiple submit
//                                     $(this).attr("disabled", "disabled");
                                     $(this).updateReAct("Filter_By");
                                     //Enable the textbox again if needed.
//                                     $(this).removeAttr("disabled");
                                  }
                          });

                          $("[data-time-format]").each(function() {
                              var el = $( this );
                              switch(el.attr("data-time-format")) {
                                  case "time-ago":
                                    var timeValue = el.attr("data-time-value");
                                    var strTimeAgo = moment(timeValue).fromNow();
                                    el.text(strTimeAgo);
                                  break;
                              }
                          });//$( "[data-time-format]" ).each(function() {
                      });
                      </script>




<!--===========END of TRACK ACTIVIY=========================================================-->
