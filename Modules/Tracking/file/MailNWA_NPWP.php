<?php

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "NPWP updated";
    $Email_Body = "Dear Customer,
        <br><br>
        Greetings from Invest In Asia!
        <br><br>
        We attach a scan of your completed company NPWP (Taxpayer Identification Number). NPWP is a tax identification number for companies in Indonesia.
        <br><br>
        The next stage is the registration of your company's NIB (Business Identification Number).
        <br><br>
        If there is further information, we will convey it to you.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";

    $WA_Message = "Dear Customer,

Greetings from Invest In Asia!

Congratulation! NPWP (Taxpayer Identification Number) has been completed.

For further information, you can refer to this link https://tracking.izin.co.id.

Warm regards,
*Invest In Asia team*
";
} else {
    $Email_Subject = "✅ Update Process - NPWP  $Company_Name Selesai";
    $Email_Body = "<DIV style='max-width: 500px'><P>Kepada Yth,</P>
        <p>" . $Company_Name . "</p>
        <P>Salam dari IZIN CO ID!</P>

        <P>Melalui notifikasi ini, Kami informasikan bahwa NPWP (Nomor Pokok Wajib Pajak) untuk $Company_Name telah selesai dan sudah bisa diunduh.</P>

        <P>Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya.</P>

        <p>Proses selanjutnya adalah pengurusan NIB (Nomor Induk Berusaha). Silakan cek proses perizinan di sistem tracking kami, satu-satunya di Indonesia.</p>

        <p>Terima kasih atas kepercayaan Anda menggunakan layanan IZIN.co.id. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.</p>


        <P>Salam Hangat,<BR/>
        <A HREF='https://izin.co.id'>IZIN.co.id</A></P>

        <BR/><HR>
        <P class='text-muted'><i>Pesan ini adalah pesan otomatis - Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id</i></P>
        </DIV>
        ";

    $WA_Message = "Kepada Yth,
    " . $Company_Name . "

Salam dari IZIN CO ID!

Melalui notifikasi ini, Kami informasikan bahwa NPWP (Nomor Pokok Wajib Pajak) untuk $Company_Name telah selesai dan sudah bisa diunduh.

Untuk mengunduh soft file dokumen izin Anda, silakan klik https://tracking.izin.co.id dan masukkan Kode Tracking beserta password yang sudah dikirimkan di email sebelumnya.

Proses selanjutnya adalah pengurusan NIB (Nomor Induk Berusaha). Silakan cek proses perizinan di sistem tracking kami, satu-satunya di Indonesia.

Terima kasih atas kepercayaan Anda menggunakan layanan IZIN.co.id. Kami akan selalu meningkatkan pelayanan demi kenyamanan dan kemudahan berbisnis Anda.

Salam Hangat,
*Tim IZIN.co.id*

__________________________________________________________________________

Pesan ini adalah pesan otomatis - Mohon untuk tidak membalas email ini. Untuk informasi lebih lanjut , Anda bisa menghubungi kami melalui Whatsapp/e-mail +62 822 9998 0011 / cs@izin.co.id
";
}
