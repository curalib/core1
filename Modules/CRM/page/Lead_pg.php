<?php
require "system/PagingTrack.php";
require "page/Alert.php";
echo '<div class="container-fluid page__container">';
if (isset($_SESSION["AddItm"])) {
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Leads_Add.php";
} else {
	unset($_SESSION["AGREEMENT"]);
	unset($_SESSION["AGREEMENT_POST"]);
	unset($_SESSION['agreement_pdf']);
	if (isset($_SESSION["LeadDets"])) {
		$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Leads_Dets.php";
	} elseif (isset($_SESSION["LeadChStat"])) {
		$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Leads_Stats.php";
	} else {
		$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Leads_List.php";
	}
}
if (is_file($fl)) {
	require $fl;
} else {
	require $_SESSION["ROOT_DIR"] . "/Modules/System/page/PageNotFound.php";
}
echo '</div>';
