<?php
require "system/PagingTrack.php";
require "page/Alert.php";
echo '<div class="container-fluid page__container">';
if (isset($_SESSION["LeadDets"])) {
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Leads_Dets.php";
} else {
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Dtbase_List.php";
}
if (is_file($fl)) {
	require $fl;
} else {
	require $_SESSION["ROOT_DIR"] . "/Modules/System/page/PageNotFound.php";
}
echo '</div>';
