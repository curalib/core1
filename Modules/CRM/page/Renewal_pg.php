<?php
require "system/PagingTrack.php";
require "page/Alert.php";

if (isset($_SESSION["RenewalLeadDets"])) {
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Renewal_Leads_Dets.php";
} elseif (isset($_SESSION["LeadDets"])) {
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Leads_Dets.php";
} else {
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Renewal_List.php";
}
echo '<div class="container-fluid page__container">';
if (is_file($fl)) {
	require $fl;
} else {
	require $_SESSION["ROOT_DIR"] . "/Modules/System/page/PageNotFound.php";
}
echo '</div>';
