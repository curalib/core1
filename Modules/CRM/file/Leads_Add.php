<?php
echo '
<script>
';
if (is_file("Modules/" . $cmodule . "/autocomplete/JS_File.json")) {
	require "Modules/" . $cmodule . "/autocomplete/JS_File.json";
}
echo '
$(function() {
	$("#SrchClient").autocomplete({
		source: datasp,
		focus: 	function(event, ui) {
			event.preventDefault();
			$(this).val(ui.item.label);
		},
		select: function(event, ui) {
			event.preventDefault();
			$(this).val(ui.item.label);
			$("#SrcSerId").val(ui.item.value);
		},
		minLength: 2
	});
});

</script>
';
?>
<style>
	.ui-autocomplete.ui-widget {
		font-size: 13px;
	}

	.ui-state-active a,
	.ui-state-active a:link,
	.ui-state-active a:visited {
		color: #5b518b;
		/* any color you like */

	}

	.ui-widget-content .ui-state-active {
		color: #5b518b;
		/* any color you like */
	}
</style>
<?php if (!isset($_SESSION["ClietType"])) { ?>
	<div class="card card-form">
		<div class="row no-gutters">
			<div class="col-lg-12 card-form__body card-body">
				<form action="" method="post" name="SelClient" id="SelClient">
					<input type="hidden" name="task" />
					<div class="form-group">
						<label for="SrchClient"><?php echo $lang_2005 ?> *</label>
						<input id="SrchClient" name="SrchClient" type="text" class="form-control" placeholder="<?php echo $lang_2006 ?> ...">
						<input id="SrcSerId" type="hidden" name="SrcSerId" value="" />
					</div>
				</form>
				<div class="text-right mb-5">
					<?php
					echo
					ButtonsCommon("commonbuttons", THEMES, "Ok_i", $lang_2012, "Cl_" . $_SESSION["butts_Enter"], "SelClient", "", "left") . " " .
						ButtonsCommon("commonbuttons", THEMES, "Add_i", $lang_2013, "Cl_" . $_SESSION["butts_Add"], "SelClient", "", "left");
					?>
				</div>

			</div>
		</div>
	</div>

<?php
} elseif (isset($_SESSION["ClietType"])) {
	if ($_SESSION["ClietType"] != "AddNew") {
		$cDetClient = "Select kode, nama, email, tgl_lahir, no_wa from " . $tblp . "apps_client where id = '" . $_SESSION["ClietType"] . "'";
		$rcDetClient = $dbs->getArr($cDetClient);
		$readonly = "disabled";
	} else {
		$readonly = "";
		$rcDetClient['email'] = $ClMail;
		$rcDetClient['no_wa'] = $ClWa;
		$rcDetClient['tgl_lahir'] = $ClHbd;
	}

?>
	<script>
		$(document).ready(function() {
			$('#ClNm').keyup(function() {
				if ($('#ClNm').val().length > 2) {
					var checking_html = 'Checking...';
					$('#ClID').val(checking_html);
					getClientID();
				} else {
					$('#ClID').val('');
				}
			});

			function getClientID() {
				var ClientName = $('#ClNm').val().toUpperCase();
				var pg = 'getClientID';
				var amodul = '<?php echo $cmodule ?>';
				$.ajax({
					url: '<?php echo BASEURL ?>system/ajax.php',
					type: 'POST',
					dataType: 'html',
					data: {
						clientName: ClientName,
						pg: pg,
						amod: amodul
					},
					success: function(result) {
						if (result) {
							$('#ClID').val(result);
						}
					},
				});
			}
		});
	</script>
	<div class="card card-form">
		<div class="row no-gutters">
			<div class="col-lg-12 card-form__body card-body">
				<form action="" method="post" name="AddLead" id="AddLead">
					<input type="hidden" name="task" />
					<span class="navbar-brand">Client Detail </span>
					<br />
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="ClID"><?php echo $lang_2009 ?></label>
								<input id="ClID" name="ClID" type="text" class="form-control" readonly="readonly" style="width:100px;" value="<?php echo $rcDetClient['kode'] ?>">
							</div>
						</div>
						<div class="col">
							<?php
							if ($_SESSION["ClietType"] == "AddNew") {
								echo ButtonsCommon("commonbuttons", THEMES, "Restart_i", $lang_2050, "ResetAddNewLead_" . $_SESSION["butts_Reset"], "AddLead", "", "right");
							} else {
								echo ButtonsCommon("commonbuttons", THEMES, "Restart_i", $lang_2049, "ResetAddNewLead_" . $_SESSION["butts_Reset"], "AddLead", "", "right");
							}
							?>
						</div>
					</div>

					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="ClNm"><?php echo $lang_2006 ?> *</label>
								<input id="ClNm" name="ClNm" type="text" class="form-control" placeholder="<?php echo $lang_2006 ?> ..." value="<?php echo $rcDetClient['nama'] ?>" <?php echo $readonly ?> style="<?php echo $ClNm_St ?>">
								<input type="hidden" name="Nama" value="<?php echo $rcDetClient['nama'] ?>">
							</div>
						</div>

						<div class="col">
							<div class="form-group">
								<label for="ClHbd"><?php echo $lang_2007 ?></label>
								<input id="ClHbd" name="ClHbd" type="text" class="form-control" data-toggle="flatpickr" value="<?php echo convertDate($rcDetClient['tgl_lahir']) ?>">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="ClWa"><?php echo $lang_2008 ?></label>
								<input id="ClWa" name="ClWa" type="text" class="form-control" placeholder="<?php echo $lang_2008 ?> ..." value="<?php echo $rcDetClient['no_wa'] ?>">
							</div>
						</div>

						<div class="col">
							<div class="form-group">
								<label for="ClMail"><?php echo $lang_2010 ?> *</label>
								<input id="ClMail" name="ClMail" type="text" class="form-control" placeholder="<?php echo $lang_2010 ?> ..." value="<?php echo $rcDetClient['email'] ?>" style="<?php echo $ClMail_St ?>">
							</div>
						</div>
					</div>

					<div class="form-group batas">&nbsp;</div>
					<br /><span class="navbar-brand">Sales Detail</span><br />
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="Sales"><?php echo $lang_6038 ?> *</label>
								<select id="Sales" name="Sales" class="form-control" data-toggle="select" style="<?php echo $Sales_St ?>">
									<option value="">--</option>
									<?php
									if ($_SESSION['Jbtn'] == "Manager" || in_array($_SESSION['lgnPriv'], [0, 2])) { // Manager or lvl 0, 2
										$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where (priv = 4 or priv = 2) order by nama_asli asc";
									} else {
										$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where id_login = '" . $_SESSION['tblID'] . "' order by nama_asli asc";
									}
									$rcSales = $dbs->getQuery($cSales);
									while ($sls = $dbs->getAssoc($rcSales)) {
										if ($sls['id_login'] == $_SESSION['tblID']) {
											$sales_add_sel = "selected";
										} else {
											$sales_add_sel = "";
										}
										echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $sls['id'] . "' " . $sales_add_sel . ">" . $sls['nama_asli'] . "</option>";
									}
									?>
								</select>
							</div>
						</div>

						<div class="col">
							<div class="form-group">
								<label for="Source"><?php echo $lang_2011 ?> *</label>
								<select id="Source" name="Source" class="form-control" data-toggle="select" style="<?php echo $Source_St ?>">
									<option value="">--</option>
									<?php
									foreach (SRC_ITEM as $srcCode => $srcName) {
										if ($srcCode == $Source) {
											$source_add_sel = "selected";
										} else {
											$source_add_sel = "";
										}
										echo '<option value="' . $srcCode . '" ' . $source_add_sel . '>' . $srcName . '</option>';
									}
									?>
								</select>
							</div>
						</div>

						<!--div class="col">
					<div class="form-group">
                  		<label for="PilMans"><?php echo $lang_6035 ?>*</label>
						<select id="PilMans" name="PilMans" class="form-control" style="<?php echo $PilMans_St ?>">
								<option value=""> -- </option>
                                <?php
								foreach ($CompanyNums as $UP_Code => $UP_Nm) {
									if ($UP_Code == $PilMans) {
										$pilmans_add_sel = "selected";
									} else {
										$pilmans_add_sel = "";
									}
									echo '<option value="' . $UP_Code . '" ' . $pilmans_add_sel . '>' . $UP_Nm . '</option>';
								}
								?>
                            </select>
                    </div>
               </div-->

					</div>

					<?php
					$wish = '<label>' . $lang_6010 . ' **</label>';
					$cLWi = "Select id, kode, nama from " . $tblp . "apps_works order by urutan asc";
					$rcLWi = $dbs->getQuery($cLWi);
					$wiArr = array();
					while ($cwi = $dbs->getAssoc($rcLWi)) {
						$wish .= '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="' . $cwi['id'] . '" name="' . $cwi['id'] . '" value="1"><label class="custom-control-label" for="' . $cwi['id'] . '"><span>' . $cwi['kode'] . " - " . $cwi['nama'] . '</span></label></div>';
					}
					?>

					<script>
						$(document).ready(function() {
							$('#PackIn').change(function() {
								if (this.value == 'CUSTOM') {
									$('#WiSelItm').html('<?php echo $wish ?>');
								} else {
									$('#WiSelItm').html('');
								}
							});
						});
					</script>
					<div class="row">
						<div class="col">
							<div class="form-group">
								<label for="PackIn"><?php echo $lang_2014 ?> *</label>
								<select id="PackIn" name="PackIn" style="<?php echo $PackIn_St ?>" class="form-control" data-toggle="select">
									<option value="">--</option>
									<?php
									$cPack = "Select id, nama, kode from " . $tblp . "apps_package where publish = 'Y' order by nama";
									$rcPack = $dbs->getQuery($cPack);
									while ($cp = $dbs->getAssoc($rcPack)) {
										if ($cp['kode'] == $PackIn) {
											$packin_add_sel = "selected";
										} else {
											$packin_add_sel = "";
										}
										echo '<option value="' . $cp['kode'] . '" ' . $packin_add_sel . '>' . $cp['nama'] . '</option>';
									}
									?>
								</select>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="PrefLoc"><?php echo $lang_2015 ?></label>
								<select id="PrefLoc" name="PrefLoc" class="form-control" data-toggle="select">
									<option value="">--</option>
									<?php
									$cVolc = "Select id, lokasi from " . $tblp . "master_volocation where publish = 'Y'";
									$rcVolc = $dbs->getQuery($cVolc);
									while ($voc = $dbs->getAssoc($rcVolc)) {
										echo '<option value="' . $voc['id'] . '">' . $voc['lokasi'] . '</option>';
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group"><span id="WiSelItm"></span></div>
				</form>
				<div class="text-right mb-5">
					<?php
					if ($rdLead['status'] != 'CL') {
						echo ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_2048, "Lead_" . $_SESSION["butts_Save"], "AddLead", "", "left");
					}
					?>
				</div>
			</div>
		</div>
	</div>
<?php
}
?>