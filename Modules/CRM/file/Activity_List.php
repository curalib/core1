<?php
$srcque_user = "";
/*
if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
	$srcque_user = " AND sales_id = '" . $_SESSION['lgnDetID'] . "' ";
}
*/
?>
<div class="trello-container">
	<div class="trello-board container-fluid page__container">
		<div class="trello-board__tasks" data-toggle="dragula" data-dragula-containers='["#trello-tasks-1", "#trello-tasks-2", "#trello-tasks-3", "#trello-tasks-4"]'>
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'AC' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">About To Contact (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-1">
						<?php
						$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'AC' $srcque_user order by tgl_update desc";
						$rAC = $dbs->getQuery($cAC);
						while ($ac = $dbs->getAssoc($rAC)) {
							$acshow .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
						}
						echo $acshow;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="trello-board__tasks">
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'WR' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">Waiting For Response (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-2">
						<?php
						$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'WR' $srcque_user order by tgl_update desc";
						$rAC = $dbs->getQuery($cAC);
						while ($ac = $dbs->getAssoc($rAC)) {
							$wrshow .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
						}
						echo $wrshow;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="trello-board__tasks">
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'IN' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">Interacted (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-3">
						<?php
						$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'IN' $srcque_user order by tgl_update desc";
						$rAC = $dbs->getQuery($cAC);
						while ($ac = $dbs->getAssoc($rAC)) {
							$inshow .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
						}
						echo $inshow;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="trello-board__tasks">
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'PS' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">Proposal Sent (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-4">
						<?php
						$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'PS' $srcque_user order by tgl_update desc";
						$rAC = $dbs->getQuery($cAC);
						while ($ac = $dbs->getAssoc($rAC)) {
							$psshow .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
						}
						echo $psshow;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="trello-board__tasks">
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'IS' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">Invoice Sent (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-4">
						<?php
						$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'IS' $srcque_user order by tgl_update desc";
						$rAC = $dbs->getQuery($cAC);
						while ($ac = $dbs->getAssoc($rAC)) {
							$is_show .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
						}
						echo $is_show;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="trello-board__tasks">
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'CL' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">Closed (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-4">
						<?php
						$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'CL' $srcque_user order by tgl_update desc";
						$rAC = $dbs->getQuery($cAC);
						while ($ac = $dbs->getAssoc($rAC)) {
							$cl_show .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
						}
						echo $cl_show;
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="trello-board__tasks">
			<div class="card bg-light border">
				<div class="card-header card-header-sm bg-white">
					<?php
					$jAC = "Select count(*) as jum from " . $tblp . "apps_lead where status = 'LO' $srcque_user";
					$rJAC = $dbs->getArr($jAC);
					?>
					<h4 class="card-header__title">Lost (<?php echo $rJAC['jum'] ?>)</h4>
				</div>
				<div class="card-body p-2">
					<div class="trello-board__tasks-list card-list" id="trello-tasks-4">
						<div class="trello-board__tasks-item card shadow-none border">
							<?php
							$cAC = "select client_id, package_name, client_name from " . $tblp . "apps_lead where status = 'LO' $srcque_user order by tgl_update desc";
							$rAC = $dbs->getQuery($cAC);
							while ($ac = $dbs->getAssoc($rAC)) {
								$lo_show .= '
						<div class="trello-board__tasks-item card shadow-none border">
              			<div class="p-3">
                		<p class="m-0 d-flex align-items-center"> <strong>' . $ac['package_name'] . '</strong> </p>
                		<p class="d-flex align-items-center mb-2"> <i class="material-icons icon-16pt mr-2 text-muted">done</i> <span class="text-muted mr-3"><strong>ID</strong> : ' . $ac['client_id'] . '</span> <i class="material-icons icon-16pt mr-2 text-muted">account_circle</i> <span class="text-muted">' . $ac['client_name'] . '</span> </p>
                		<div class="media">
                	  	<div class="media-body"> </div>
                		</div>
              			</div>
            			</div>';
							}
							echo $lo_show;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--===========END of ACTIVITY==================================================-->