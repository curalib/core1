<?php

require_once "system/SendEmail.php";

$Email_Subject = "Urgent Payment verification Invoice No. $invnum - $packageName";

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Body = "Dear Finance Team,
        <br><br>
        Please confirm the payment for invoice no $invnum that was paid by client.
        <br><br>
        The payment proof already attached on ERP system within this detail:
        <br><br>
        Package : " . $packageName . "<br>
        Payment nominal : IDR " . c_curr($totalDue, ",", ".", $min_sign = "mark") . ",-<br>
        Bank Transfer : " . $BAD_NmBank . " " . $BAD_NmPT . "<br>
        Date : " . c_date($TglByr, "shortdate", "short", $list_month, $list_month_short) . "
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";
} else {
    $Email_Body = "Dear Finance Team,
        <br><br>
        Mohon dapat dilakukan verifikasi pembayaran terhadap Invoice No. $invnum yang telah dibayarkan oleh klien (bukti pembayaran terlampir pada sistem ERP) dengan detil sebagai berikut:
        <br><br>
        Nama Paket : " . $packageName . "<br>
        Nominal pembayaran : IDR " . c_curr($totalDue, ",", ".", $min_sign = "mark") . ",-<br>
        Bank Transfer : " . $BAD_NmBank . " " . $BAD_NmPT . "<br>
        Tanggal : " . c_date($TglByr, "shortdate", "short", $list_month, $list_month_short) . "
        <br><br><br><br>
        Salam,<br>
        <strong>IZIN.co.id</strong>
        <br><br>
        <hr>
        <em>This is an automated message please do not reply directly to this email/whatsapp. For further
        information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";
}

$mail->IsSMTP();
$mail->SMTPDebug = EMAIL_SMTPDEBUG;
$mail->Host = EMAIL_HOST;
$mail->SMTPAuth = EMAIL_SMTPAUTH;
$mail->SMTPSecure = EMAIL_SMTPSECURE;
$mail->Port = EMAIL_PORT;
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->isHTML(EMAIL_ISHTML);
$mail->Username = EMAIL_USERNAME;
$mail->Password = EMAIL_PASSWORD;
$mail->setFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
$ccsent = '';
foreach (FIN_USR as $FinUsrEm => $FinUsrNm) {
    $mail->addAddress($FinUsrEm, $FinUsrNm);
    $ccsent .= $FinUsrNm . " (" . $FinUsrEm . ")" . "| ";
}
foreach ($ccemail as $cce => $ccn) {
    $ccsent .= $ccn . " (" . $cce . ")" . "| ";
    $mail->addCC($cce, $ccn);
}
$mail->addBCC($_SESSION['lgnUid'], $_SESSION['lgnNama']);
$mail->Subject = $Email_Subject;
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
if ($mail->send()) {
    $MailSendStatus = 'Email sent successfully.';
} else {
    $MailSendStatus = 'Email not sent!';
}