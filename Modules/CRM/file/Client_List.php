<div class="card">
  <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
    <table class="table mb-0 thead-border-top-0">
      <thead>
        <tr>
          <th style="width: 2%;">No</th>
          <th style="width: 8%;">Client ID</th>
          <th style="width: 20%;">Full Name</th>
          <th style="width: 15%;">Mobile / WA</th>
          <th style="width: 25%;">Email</th>
          <th style="width: 15%;">Birth Date</th>
          <th style="width: 10%;">Leads</th>
        </tr>
      </thead>
      <tbody class="list" id="staff">
        <tr class="selected">
          <td>12</td>
          <td><div class="media align-items-center"> <a href="lead-info.php">Budi</a> <a href="#" class="rating-link"></a> </div></td>
          <td>PT Budi Sejahtera</td>
          <td>087172871827</td>
          <td>budi@budisejahera.com</td>
          <td><small class="text-muted">3 days ago</small></td>
          <td>Renewal</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="card-body text-right"> 15 <span class="text-muted">of 1,430</span> <a href="#" class="text-muted-light"><i class="material-icons ml-1">arrow_forward</i></a> </div>
</div>
