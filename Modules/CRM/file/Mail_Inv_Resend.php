<?php

require_once "system/SendEmail.php";

$Email_Subject = "Invoice No. $invnum - Resend update $packageName";

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Body = "Dear Client,
        <br><br>
        Greeting from Invest of Asia!
        <br><br>
        Within this email, we attached invoice for $packageName to be paid.
        <br><br>
        If you already made the payment, please confirm it with your consultant/sales by sending your payment proof to them so we can process your company's legal documents as soon as possible.
        <br><br>
        If there any invoice related to virtual office or additional package will be sent separately.
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";
} else {
    $Email_Body = "Dear Client,
        <br><br>
        Berikut ini kami kirimkan ulang Invoice terbaru dan terupdate untuk paket layanan yang dipilih. Apabila pembayaran telah dilakukan mohon segera melakukan konfirmasi pembayaran kepada konsultan/sales terkait dengan melampirkan bukti pembayaran agar pembuatan perusahaan/perizinan dapat segera dilakukan.
        <br><br>
        Untuk invoice sehubungan dengan virtual office atau paket tambahan lainnya akan dikirimkan terpisah (jika ada).
        <br><br>
        Terima kasih telah menggunakan layanan IZIN.co.id.
        <br><br><br><br>
        Salam,<br>
        <strong>IZIN.co.id</strong>
        <br><br>
        <hr>
        <em>This is an automated message please do not reply directly to this email/whatsapp. For further
        information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";
}

$mail->IsSMTP();
$mail->SMTPDebug = EMAIL_SMTPDEBUG;
$mail->Host = EMAIL_HOST;
$mail->SMTPAuth = EMAIL_SMTPAUTH;
$mail->SMTPSecure = EMAIL_SMTPSECURE;
$mail->Port = EMAIL_PORT;
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->IsHTML(EMAIL_ISHTML);
$mail->Username = EMAIL_USERNAME;
$mail->Password = EMAIL_PASSWORD;
$mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
$mail->AddCC(EMAIL_CC);
$mail->AddCC("contact@huqum.id");
$mail->AddAddress($invmail, $invpic);
$mail->Subject = $Email_Subject;
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
$mail->addAttachment($file);
if ($mail->send()) {
    $MailSendStatus = 'Email sent successfully to ' . $invmail;
} else {
    $MailSendStatus = 'Email not sent!';
}