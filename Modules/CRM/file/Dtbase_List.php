<?php
$srcque_user = " ";
if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
  //$srcque_user = " AND `lead`.`sales_id` = '" . $_SESSION['lgnDetID'] . "' ";
}

if (isset($_SESSION["LeadSearch"]) and $_SESSION["LeadSearch"] != '') {
  $srcque = "`lead`.`status` = 'CL' AND `lead`.`client_name` LIKE '%" . $_SESSION["LeadSearch"] . "%' $srcque_user ";
  $srcque .= "OR `lead`.`status` = 'CL' AND `lead`.`client_id` LIKE '%" . $_SESSION["LeadSearch"] . "%' $srcque_user ";
  $srcque .= "OR `lead`.`status` = 'CL' AND EXISTS (SELECT * FROM " . $tblp . "apps_agreement WHERE `id_lead` = `lead`.`id_lead` AND `client_company_name` LIKE '%" . $_SESSION["LeadSearch"] . "%' AND discard = 0) $srcque_user ";
  $srcque .= "OR `lead`.`status` = 'CL' AND `track`.`company_name` LIKE '%" . $_SESSION["LeadSearch"] . "%' $srcque_user ";
} else {
  $srcque = "`lead`.`status` = 'CL' $srcque_user ";
}

$cLsLead = "SELECT `lead`.`id_lead`, `lead`.`client_id`, `lead`.`client_name`, `lead`.`last_update`, `lead`.`status`, `lead`.`status_nm`, `lead`.`package_name`, `lead`.`sales_id`, `lead`.`sales_name`, `lead`.`client_email`, `lead`.`client_wa`, `lead`.`comp_nm1`, `lead`.`comp_nm2`, `lead`.`comp_nm3`, `lead`.`company_name` AS comp_name, ";
$cLsLead .= "(SELECT `client_company_name` FROM " . $tblp . "apps_agreement WHERE `id_lead` = `lead`.`id_lead` AND `discard` = 0 LIMIT 1) AS comp_agreement, ";
$cLsLead .= "`track`.`company_name` AS comp_tracking, `lead`.tgl_update ";
$cLsLead .= "FROM " . $tblp . "apps_lead AS `lead` ";
$cLsLead .= "LEFT JOIN " . $tblp . "apps_trklist AS `track` ON `track`.`lead_id` = `lead`.`id_lead` ";
$cLsLead .= "WHERE " . $srcque . " ORDER BY `lead`.`tgl_update` DESC ";

$cLsLead2 = "SELECT COUNT(`lead`.`id_lead`) AS jumlah FROM " . $tblp . "apps_lead AS `lead` LEFT JOIN " . $tblp . "apps_trklist AS `track` ON `track`.`lead_id` = `lead`.`id_lead` WHERE " . $srcque . " ";

$d = new paging($dbs, PAGE_ITEM, PAGE_LIST, -1, "LstLeads");
$d->queryTracking($cLsLead, $cLsLead2);
?>

<div class="card">
  <div class="card-header card-header-large bg-white">
    <small class="text-muted"><?php echo $d->data_info(); ?></small>
  </div>
  <div class="card-header">
    <form action="" method="post" name="SrcDt" onkeydown="if(event.keyCode === 13) { return false; }">
      <input type="hidden" name="task" />
      <div class="form-row align-items-center">
        <div class="form-group col-md-3">
          <label for="dtsrc">Search</label>
          <input id="dtsrc" name="dtsrc" type="text" class="form-control" placeholder="Enter Client / Agree / Tracking Name" value="<?php echo $_SESSION["LeadSearch"] ?>">
        </div>
        <div class="col-auto" style="margin-top:10px">
          <?= "<a href=\"javascript:submitbutton('DtSrc_" . $_SESSION["butts_Src"] . "','SrcDt');\" id=\"DtSrc_" . $_SESSION["butts_Src"] . "\" class=\"btn btn-warning float-left btn-lg\"><i class=\"material-icons mr-1\">search</i>Search</a>" ?>
        </div>
        <?php if (isset($_SESSION["LeadSearch"]) && $_SESSION["LeadSearch"] != '') : ?>
          <div class="col-auto" style="margin-top:10px">
            <?= "<a href=\"javascript:submitbutton('DtSrc_" . $_SESSION["butts_Reset"] . "','SrcDt');\" id=\"DtSrc_" . $_SESSION["butts_Reset"] . "\" class=\"btn btn-success float-left btn-lg\"><i class=\"material-icons mr-1\">clear_all</i>Clear Search</a>" ?>
          </div>
        <?php endif; ?>
      </div>
    </form>
  </div>
  <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
    <table class="table mb-0 thead-border-top-0">
      <thead>
        <tr>
          <th style="width: 2%;">No</th>
          <th style="width: 3%;">Client ID</th>
          <th style="width: 16%;">Client Name</th>
          <th style="width: 15%;">Nama PT Aggreement</th>
          <th style="width: 15%;">Nama PT Tracking</th>
          <th style="width: 14%;">Mobile Phone / WhatsApp No</th>
          <th style="width: 10%;">Email Address</th>
          <th style="width: 15%;">Closing Date</th>
          <th style="width: 10%;">Sales</th>
        </tr>
      </thead>
      <tbody class="list" id="staff">
        <?php
        while ($ll = $d->result_assoc()) {
          $llc++;
          if ($llc % 2 == 0) {
            $cls = "selected";
          } else {
            $cls = '';
          }

          $cClsDate = "select tgl_update from " . $tblp . "apps_lead_status where id_lead = '" . $ll['id_lead'] . "' order by tgl_update desc limit 1";
          $rcClsDate = $dbs->getArr($cClsDate);

          $cAv = "Select id, avatar, nama_asli from " . $tblp . "sys_lgndetail where id = '" . $ll['sales_id'] . "'";
          $rcAv = $dbs->getArr($cAv);
          if (empty($rcAv['id'])) {
            $styles = "color:red;";
            $namasales = $ll['sales_name'];
          } else {
            $styles = "";
            $namasales = $rcAv['nama_asli'];
          }

          echo '
  <form action="" name="LstLead' . $llc . '" id="LstLead' . $llc . '" method="post">
      <input type="hidden" name="task">
      <input type="hidden" name="Ldid" value="' . $ll['id_lead'] . '">
      </form>
  <tr class="' . $cls . '">
        <td>' . $d->show_num() . '</td>
        <td>' . $ll['client_id'] . '</td>
        <td><div class="media align-items-center">' . "<a href=\"javascript:submitbutton('LeadDet" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\">" . $ll['client_name'] . '</a></div></td>
        <td>' . $ll['comp_agreement'] . '</td>
        <td>' . $ll['comp_tracking'] . '</td>
        <td>' . $ll['client_wa'] . '</td>
        <td>' . $ll['client_email'] . '</td>
        <td><small class="text-muted">' . c_date($rcClsDate['tgl_update'], "longdate", "short", $list_month, $list_month_short) . '</small></td>
        <td>
    <div class="media align-items-center">
                <div class="avatar avatar-xs mr-2">' . "<img src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $rcAv["avatar"])) . "' class='avatar-img rounded-circle' alt='Avatar'>" . '</div>
                <div class="media-body"> <span class="js-lists-values-employee-name" style="' . $styles . '">' . $namasales . '</span> </div>
                </div>
  </td>
      </tr>';
        }
        ?>
      </tbody>
    </table>
  </div>
</div>
<div class="mt-4">
  <?php
  echo $d->print_page();
  echo $d->form_print_page();
  ?>
</div>