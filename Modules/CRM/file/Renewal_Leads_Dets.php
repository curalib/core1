<?php
$dLead = "SELECT a.*, b.locale, c.paid, d.tracking_id FROM " . $tblp . "apps_lead a ";
$dLead .= "LEFT JOIN " . $tblp . "apps_client b on b.kode = a.client_id ";
$dLead .= "LEFT JOIN " . $tblp . "apps_invdet c on c.id_lead = a.id_lead ";
$dLead .= "LEFT JOIN " . $tblp . "apps_trklist d on d.lead_id = a.id_lead ";
$dLead .= "WHERE a.id_lead = '" . $_SESSION["RenewalLeadDets"] . "'";
$rdLead = $dbs->getArr($dLead);
if ($rdLead['status'] == 'CL' || $rdLead['tracking_id'] != NULL) {
	$disabled = "disabled";
} else {
	$disabled = "";
}

?>

<div class="container-fluid page__container">
	<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="lead-client-detail-tab" data-toggle="pill" href="#lead-client-detail" role="tab" aria-controls="lead-client-detail" aria-selected="true">CLIENT & SALES</a>
		</li>
	</ul>
	<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show active" id="lead-client-detail" role="tabpanel" aria-labelledby="lead-client-detail-tab">
			<form action="" method="post" name="LeadClientDetail" id="LeadClientDetail">
				<input type="hidden" name="task" />
				<div class="card card-form">
					<div class="row no-gutters">
						<div class="col-lg-12 card-form__body card-body">
							<div class="form-group">
								<label for="ClID"><?php echo $lang_2009 ?></label>
								<input id="ClID" name="ClID" type="text" class="form-control" readonly="readonly" style="width:100px;" value="<?php echo $rdLead['client_id'] ?>">
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="ClNm"><?php echo $lang_2006 ?>*</label>
										<input id="ClNm" name="ClNm" type="text" class="form-control" placeholder="<?php echo $lang_2006 ?> ..." value="<?php echo $rdLead['client_name'] ?>" readonly="readonly">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="ClHbd"><?php echo $lang_2007 ?> *</label>
										<input id="ClHbd" name="ClHbd" type="text" class="form-control" value="<?= (!in_array($rdLead['client_tgllhr'], [NULL, '0000-00-00'])) ? convertDate($rdLead['client_tgllhr']) : '' ?>" readonly="readonly">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="ClWa"><?php echo $lang_2008 ?>*</label>
										<input id="ClWa" name="ClWa" type="text" class="form-control" placeholder="<?php echo $lang_2008 ?> ..." value="<?php echo $rdLead['client_wa'] ?>" readonly="readonly">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="ClMail"><?php echo $lang_2010 ?> *</label>
										<input id="ClMail" name="ClMail" type="text" class="form-control" placeholder="<?php echo $lang_2010 ?> ..." value="<?php echo $rdLead['client_email'] ?>" readonly="readonly">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form action="" method="post" name="RenewalLeadSalesDetail" id="RenewalLeadSalesDetail">
				<input type="hidden" name="task" />
				<div class="card card-form">
					<div class="row no-gutters">
						<div class="col-lg-12 card-form__body card-body">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="Sales"><?php echo $lang_6038 ?> *</label>
										<select id="Sales" name="Sales" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											if ($_SESSION['Jbtn'] == "Manager" || in_array($_SESSION['lgnPriv'], [0, 2])) { // Manager or lvl 0, 2
												$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where (priv = 4 or priv = 2) order by nama_asli asc";
											} else {
												$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where id = '" . $rdLead['sales_id'] . "' order by nama_asli asc";
											}
											$rcSales = $dbs->getQuery($cSales);
											$foundSales = false;
											while ($sls = $dbs->getAssoc($rcSales)) {
												if ($sls['id'] == $rdLead['sales_id']) {
													$sales_sel = "selected";
													$foundSales = true;
												} else {
													$sales_sel = '';
												}
												echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $sls['id'] . "' " . $sales_sel . ">" . $sls['nama_asli'] . "</option>";
											}
											if (!$foundSales) {
												echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $rdLead['sales_id'] . "' selected>" . $rdLead['sales_name'] . "</option>";
											}
											?>
										</select>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="Source"><?php echo $lang_2011 ?> *</label>
										<select id="Source" name="Source" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											foreach (SRC_ITEM as $srcCode => $srcName) {
												if ($srcCode == $rdLead['source_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $srcCode . '" ' . $src_sel . '>' . $srcName . '</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<?php
							# Get Package Work
							$lstPackWork = "select kode from " . $tblp . "apps_packagework where id_lead = '" . $_SESSION["LeadDets"] . "'";
							$rlstPackWork = $dbs->getQuery($lstPackWork);
							$lpwArray = array();
							while ($lpw = $dbs->getAssoc($rlstPackWork)) {
								array_push($lpwArray, $lpw['kode']);
							}

							$wish = '<label>' . $lang_6010 . ' **</label>';
							$cLWi = "Select id, kode, nama from " . $tblp . "apps_works order by urutan asc";
							$rcLWi = $dbs->getQuery($cLWi);
							//$wiArr = array();
							while ($cwi = $dbs->getAssoc($rcLWi)) {
								if (in_array($cwi['kode'], $lpwArray)) {
									$lpwsel = "checked";
								} else {
									$lpwsel = "";
								}
								$wish .= '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="' . $cwi['id'] . '" name="' . $cwi['id'] . '" value="1" ' . $lpwsel . ' ' . $disabled . '><label class="custom-control-label" for="' . $cwi['id'] . '"><span>' . $cwi['kode'] . " - " . $cwi['nama'] . '</span></label></div>';
							}
							?>
							<script>
								$(document).ready(function() {
									var packselected = $('#PackIn').find(":selected").val();
									if (packselected == 'CUSTOM') {
										$('#WiSelItm').html('<?php echo $wish ?>');
									}

									$('#PackIn').change(function() {
										if (this.value == 'CUSTOM') {
											$('#WiSelItm').html('<?php echo $wish ?>');
										} else {
											$('#WiSelItm').html('');
										}
									});
								});
							</script>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="PackIn"><?php echo $lang_2014 ?> *</label>
										<select id="PackIn" name="PackIn" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											$cPack = "Select id, nama, kode from " . $tblp . "apps_package where publish = 'Y' order by nama";
											$rcPack = $dbs->getQuery($cPack);
											while ($cp = $dbs->getAssoc($rcPack)) {
												if ($cp['kode'] == $rdLead['package_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $cp['kode'] . '" ' . $src_sel . '>' . $cp['nama'] . '</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="PrefLoc"><?php echo $lang_2015 ?></label>
										<select id="PrefLoc" name="PrefLoc" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											$cVolc = "Select id, lokasi from " . $tblp . "master_volocation where publish = 'Y'";
											$rcVolc = $dbs->getQuery($cVolc);
											while ($voc = $dbs->getAssoc($rcVolc)) {
												if ($voc['id'] == $rdLead['prefloc_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $voc['id'] . '" ' . $src_sel . '>' . $voc['lokasi'] . '</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group"><span id="WiSelItm"></span></div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="RenewalSales"><?php echo $lang_6038 ?> *</label>
										<select id="RenewalSales" name="RenewalSales" data-toggle="select" class="form-control">
											<option value="">--</option>
											<?php
											if ($_SESSION['Jbtn'] == "Manager" || in_array($_SESSION['lgnPriv'], [0, 2])) { // Manager or lvl 0, 2
												$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where (priv = 4 or priv = 2) order by nama_asli asc";
											} else {
												$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where id = '" . $rdLead['renew_sales_id'] . "' order by nama_asli asc";
											}
											$rcSales = $dbs->getQuery($cSales);
											$foundSales = false;
											while ($sls = $dbs->getAssoc($rcSales)) {
												if ($sls['id'] == $rdLead['renew_sales_id']) {
													$sales_sel = "selected";
													$foundSales = true;
												} else {
													$sales_sel = '';
												}
												echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $sls['id'] . "' " . $sales_sel . ">" . $sls['nama_asli'] . "</option>";
											}
											if (!$foundSales) {
												echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $rdLead['sales_id'] . "' selected>" . $rdLead['sales_name'] . "</option>";
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="text-right mb-5">
								<?= ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_2062, "RenewalLeadSales_" . $_SESSION["butts_Update"], "RenewalLeadSalesDetail", "", "left"); ?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>