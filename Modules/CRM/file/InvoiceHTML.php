<?php
$company = BANK_TO_COMPANY[$BnkAcc];
if ($vaNumber === NULL) {
  $_vaNumber = $BnkAcc;
} else {
  $_vaNumber = $vaNumber;
}

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
  $company['watermark'] = '<img src="' . convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/watermark-iia.png') . '" width="100%" height="100%">';
  $company['logo'] = '<img src="' . convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/invest_in_asia.png') . '" width="300px">';
} else {
  $company['watermark'] = '<img src="' . convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/watermark.jpg') . '" width="100%" height="100%">';
  $company['logo'] = '<img src="' . convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/logo-dark-backup.png.png') . '" width="200px">';
}

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
  require_once $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/_Invoice-IIA.php";
} else {
  require_once $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/_Invoice-default.php";
}
