<div class="card">
    <div class="card-header card-header-large bg-white">
        <small class="text-muted"><?php echo $d->data_info(); ?></small>
    </div>
    <div class="card-header">
        <form action="" method="post" name="SrcDt">
            <input type="hidden" name="task" />
            <div class="form-row align-items-center">
                <div class="form-group col-md-3">
                    <label for="filter_name">Search</label>
                    <input id="filter_name" name="filter_name" type="text" class="form-control" placeholder="Enter Client ID or Client Name" style="/** width:300px; **/" value="<?php echo $_SESSION["LeadSearch"] ?>">
                </div>
                <div class="form-group col-md-3">
                    <label for="filter_date">Expired <?php echo $lang_2031 ?></label>
                    <input id="filter_date" name="filter_date" type="text" class="form-control" placeholder="Select date ..." data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y" value="<?php echo $_SESSION["LeadSearchDateRange"] ?? $default_filter_date ?>">
                </div>
                <div class="col-auto" style="margin-top:10px">
                    <?= "<a href=\"javascript:submitbutton('ExpSrc_" . $_SESSION["butts_Src"] . "','SrcDt');\" id=\"ExpSrc_" . $_SESSION["butts_Src"] . "\" class=\"btn btn-warning float-left btn-lg\"><i class=\"material-icons mr-1\">search</i>Search</a>" ?>
                </div>
                <div class="col-auto" style="margin-top:10px">
                    <?= "<a href=\"javascript:submitbutton('ExpSrc_" . $_SESSION["butts_Reset"] . "','SrcDt');\" id=\"ExpSrc_" . $_SESSION["butts_Reset"] . "\" class=\"btn btn-success float-left btn-lg\"><i class=\"material-icons mr-1\">clear_all</i>Clear Search</a>" ?>
                </div>
            </div>
        </form>
    </div>
    <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
        <table class="table mb-0 thead-border-top-0">
            <?php

            echo ' <thead>';
            echo '<tr>
                        <th style="width: 2%;">No</th>
                        <th style="width: 3%;">Client ID</th>
                        <th style="width: 18%;">Client Name</th>
                        <th style="width: 20%;">Product</th>
                        <th style="width: 20%;">Nama PT Tracking</th>
                        <th style="width: 9%;">Tracking ID</th>
                        <th style="width: 10%;">Expired Date</th>
                        <th style="width: 18%;">Assigned To Sales</th>
                    </tr>
                </thead>
                <tbody class="list" id="staff">';

            while ($ll = $d->result_assoc()) :
                $llc++;
                if ($llc % 2 == 0) {
                    $cls = "selected";
                } else {
                    $cls = '';
                }

                $cAv = "Select id, avatar, nama_asli from " . $tblp . "sys_lgndetail where id = '" . $ll['sales_id'] . "'";
                $rcAv = $dbs->getArr($cAv);
                if (empty($rcAv['id'])) {
                    $styles = "color:red;";
                    $namasales = $ll['sales_name'];
                } else {
                    $styles = "";
                    $namasales = $rcAv['nama_asli'];
                }

                if (!empty($ll['renew_sales_id'])) {
                    $cAv2 = "Select id, avatar, nama_asli from " . $tblp . "sys_lgndetail where id = '" . $ll['renew_sales_id'] . "'";
                    $rcAv2 = $dbs->getArr($cAv2);
                    if (empty($rcAv2['id'])) {
                        $styles2 = "color:red;";
                        $namasales2 = $ll['renew_sales_name'];
                    } else {
                        $styles2 = "";
                        $namasales2 = $rcAv2['nama_asli'];
                    }
                }

                echo '
                <form action="" name="LstLead' . $llc . '" id="LstLead' . $llc . '" method="post">
					<input type="hidden" name="task">
					<input type="hidden" name="Ldid" value="' . $ll['id_lead'] . '">
				</form>
                <tr class="' . $cls . '">
                    <td>' . $d->show_num() . '</td>
                    <td>' . $ll['client_id'] . '</td>
                    <td><div class="media align-items-center">' . "<a href=\"javascript:submitbutton('LeadDet" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\">" . $ll['client_name'] . '</a></div></td>
                    <td>' . $ll['pck_nama'] . '</td>
                    <td>' . $ll['company_name'] . '</td>
                    <td>' . $ll['tracking_id'] . '</td>
                    <td><small class="text-muted">' . c_date($ll['reminder_akta'], "shortdate", "short", $list_month, $list_month_short) . '</small></td>
                    <td><div class="media align-items-center">
                        <div class="avatar avatar-xs mr-2">' . "<img src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $rcAv["avatar"])) . "' class='avatar-img rounded-circle' alt='Avatar'>" . '</div>
                        <div class="media-body"> <span class="js-lists-values-employee-name" style="' . $styles . '">';
                if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
                    echo $namasales;
                } else {
                    echo "<a style=\"$styles\" href=\"javascript:submitbutton('RenewalLeadDet_" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\">" . $namasales . "</a>";
                }
                echo '</span> </div></div>';
                if (!empty($ll['renew_sales_id'])) {
                    echo '
                          <div class="media align-items-center">
                        <div class="avatar avatar-xs mr-2">' . "<img src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $rcAv2["avatar"])) . "' class='avatar-img rounded-circle' alt='Avatar'>" . '</div>
                        <div class="media-body"> <span class="js-lists-values-employee-name" style="' . $styles2 . '">' . $namasales2 . '</span> </div>
                          </div>';
                }
                echo '
                    </td>
                </tr>
                '; ?>
            <?php endwhile; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="mt-4">
    <?php
    echo $d->print_page();
    echo $d->form_print_page();
    ?>
</div>