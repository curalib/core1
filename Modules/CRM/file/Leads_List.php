<?php
$srcque = "lead.id_lead != '' ";
if (isset($_SESSION["LeadSearch"]) && $_SESSION["LeadSearch"] != '') {
    $srcque .= "and lead.client_name like '%" . $_SESSION["LeadSearch"] . "%' ";
}

if (isset($_SESSION["LeadSearchSales"]) && $_SESSION["LeadSearchSales"] != '') {
    $srcque .= "and lead.sales_id = '" . $_SESSION["LeadSearchSales"] . "' ";
} /* else {
    if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
        $srcque .= " AND sales_id = '" . $_SESSION['lgnDetID'] . "' ";
    }
} */

if (isset($_SESSION["LeadProdSales"]) && $_SESSION["LeadProdSales"] != '') {
    $srcque .= "and lead.package_id = '" . $_SESSION["LeadProdSales"] . "' ";
}

if (isset($_SESSION["LeadSearchStatus"]) && $_SESSION["LeadSearchStatus"] != '') {
    $srcque .= "and lead.status = '" . $_SESSION["LeadSearchStatus"] . "' ";
} else {
    $srcque .= "and lead.status != 'CL' and lead.status != 'LO' ";
}

if (isset($_SESSION["LeadSearchDateRange"]) && $_SESSION["LeadSearchDateRange"] != '') {
    list($daterange01, $daterange02) = explode(" to ", $_SESSION["LeadSearchDateRange"]);
    if ($daterange02) {
        $srcque .= "and DATE(lead.tgl_update) >= '" . convertDate($daterange01, "/") . "' and DATE(lead.tgl_update) <= '" . convertDate($daterange02, "/") . "' ";
    } else {
        $srcque .= "and DATE(lead.tgl_update) = '" . convertDate($daterange01, "/") . "' ";
    }
}

if (isset($_SESSION["InvSearchStatus"]) && $_SESSION["InvSearchStatus"] != '') {
    $srcque .= "and inv.tipe = '" . $_SESSION["InvSearchStatus"] . "' ";
}

$cLsLead = "select lead.id_lead, lead.client_id, lead.client_name, lead.last_update, lead.status, lead.status_nm, lead.package_name, lead.sales_id, lead.sales_name, lead.source, inv.inv_num, inv.tipe ";
$cLsLead .= "FROM " . $tblp . "apps_lead as lead LEFT JOIN " . $tblp . "apps_invdet as inv ON inv.id_lead = lead.id_lead ";
$cLsLead .= "WHERE " . $srcque . " ORDER BY lead.tgl_update DESC";
$d = new paging($dbs, PAGE_ITEM, PAGE_LIST, -1, "LstLeads");
$d->query($cLsLead);
?>
<div class="card card-form d-flex flex-column flex-sm-row">
    <div class="card-form__body card-body-form-group flex">
        <form action="" method="post" name="LeadSrcFrm" id="LeadSrcFrm">
            <input type="hidden" name="task">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="SrcLead"><?php echo $lang_2030 ?></label>
                        <input id="SrcLead" name="SrcLead" type="text" class="form-control" placeholder="Enter keyword" value="<?php echo $_SESSION["LeadSearch"] ?>">
                    </div>
                    <div class="form-group">
                        <label for="SalesFilter"><?php echo $lang_6038 ?></label><br />
                        <select id="SalesFilter" name="SalesFilter" class="custom-select">
                            <option value="">Any</option>
                            <?php
                            $cLs = "Select id, nama_asli from " . $tblp . "sys_lgndetail where priv = '4' or jabatan = 'Manager' order by nama_asli";
                            $rcLs = $dbs->getQuery($cLs);
                            while ($sls = $dbs->getAssoc($rcLs)) {
                                if ($_SESSION["LeadSearchSales"] == $sls['id']) {
                                    $csls = "selected";
                                } else {
                                    $csls = "";
                                }
                                echo '<option value="' . $sls['id'] . '" ' . $csls . '>' . $sls['nama_asli'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ProdFilter">Product</label><br />
                        <select id="ProdFilter" name="ProdFilter" class="custom-select">
                            <option value="">Any</option>
                            <?php
                            $cPs = "Select id, kode, nama from " . $tblp . "apps_package where kode != '' order by nama";
                            $rcPs = $dbs->getQuery($cPs);
                            while ($sps = $dbs->getAssoc($rcPs)) {
                                if ($_SESSION["LeadProdSales"] == $sps['kode']) {
                                    $sps_sel = "selected";
                                } else {
                                    $sps_sel = "";
                                }
                                echo '<option value="' . $sps['kode'] . '" ' . $sps_sel . '>' . $sps['nama'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="LeadStatusFilter">Lead Status</label><br />
                        <select id="LeadStatusFilter" name="LeadStatusFilter" class="custom-select">
                            <option value="">Any</option>
                            <?php
                            foreach (LEAD_STATUS as $statusCode => $statusNm) {
                                if ($_SESSION["LeadSearchStatus"] == $statusCode) {
                                    $status_sel = "selected";
                                } else {
                                    $status_sel = "";
                                }
                                echo '<option value="' . $statusCode . '" ' . $status_sel . '>' . $statusNm . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="filter_date">Lead Update</label>
                        <input id="filter_date" name="filter_date" type="text" class="form-control" placeholder="Select date ..." data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y" value="<?php echo $_SESSION["LeadSearchDateRange"] ?>">
                    </div>
                    <div class="form-group">
                        <label for="InvStatusFilter">Invoice Status</label><br />
                        <select id="InvStatusFilter" name="InvStatusFilter" class="custom-select">
                            <option value="">Any</option>
                            <option value="PREVIEW" <?= ($_SESSION["InvSearchStatus"] != "PREVIEW") ? '' : 'selected' ?>>PREVIEW</option>
                            <option value="SENT" <?= ($_SESSION["InvSearchStatus"] != "SENT") ? '' : 'selected' ?>>SENT</option>
                            <option value="PAID" <?= ($_SESSION["InvSearchStatus"] != "PAID") ? '' : 'selected' ?>>PAID</option>
                        </select>
                    </div>
                </div>
                <div class="col" style="margin-top:25px;">
                    <!--div class="form-group"-->
                    <?php
                    echo ButtonsCommon("commonbuttons", THEMES, "Cari_i", $lang_2030, "LeadList_" . $_SESSION["butts_Src"], "LeadSrcFrm", "", "left");
                    if (isset($_SESSION["LeadSearch"]) || isset($_SESSION["LeadSearchSales"]) || isset($_SESSION["LeadProdSales"]) || isset($_SESSION["LeadSearchStatus"]) || isset($_SESSION["LeadSearchDateRange"]) || isset($_SESSION["InvSearchStatus"])) {
                        echo ButtonsCommon("commonbuttons", THEMES, "Reset_i", "Clear Search", "LeadList_" . $_SESSION["butts_Reset"], "LeadSrcFrm", "", "left");
                    }
                    echo ButtonsCommon("commonbuttons", THEMES, "Excell_i", $lang_2032, "Excel_" . $_SESSION["butts_Enter"], "LeadSrcFrm", "", "left");
                    //echo ButtonsCommon("commonbuttons",THEMES,"PDF_i","Export to PDF","PDF_".$_SESSION["butts_Enter"],"LeadSrcFrm","","left");
                    ?>
                    <!--/div-->
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
        <table class="table mb-0 thead-border-top-0">
            <?php

            echo ' <thead>';
            echo '<tr>';
            echo '<td colspan=6>';
            echo '<div class="card-body text-left">' . $d->data_info() . '</div>';
            echo '</td>';
            echo '</tr>';

            echo '<tr>
                          <th style="width: 60px;">Client ID</th>
                          <th style="width: 170px;">Client</th>
                          <th style="width: 150px;">Status</th>
                          <th style="width: 120px;">Last Update</th>
                          <th style="width: 200px;">Product</th>
                        <th style="width: 100px;">Source</th>
                          <th style="width: 250px;">Sales</th>
                    </tr>
                      </thead>
                      <tbody class="list" id="staff">';

            while ($ll = $d->result_assoc()) {
                $llc++;
                $LstUpdate = $HitDays->DayRange(convertDate($ll['last_update']), "now", false)->Result();

                $cAv = "Select id, avatar, nama_asli from " . $tblp . "sys_lgndetail where id = '" . $ll['sales_id'] . "'";
                $rcAv = $dbs->getArr($cAv);
                if (empty($rcAv['id'])) {
                    $styles = "color:red;";
                    $namasales = $ll['sales_name'];
                } else {
                    $styles = "";
                    $namasales = $rcAv['nama_asli'];
                }

                # Invoice Status
                $cInv = "select tipe, due_date from " . $tblp . "apps_invdet where id_lead = '" . $ll['id_lead'] . "'";
                $rcInv = $dbs->getArr($cInv);
                $InvStatus = '';
                $invstyles = '';
                switch ($rcInv['tipe']) {
                    case "SENT":
                        if (strtotime($rcInv['due_date']) < strtotime(date('Y-m-d', time()) . '00:00:00')) {
                            $InvStatus = "[OVER DUE]";
                            $invstyles = "color:red; font-weight: bold;";
                        } else {
                            if ($rcInv['unduh'] == "Y") {
                                $InvStatus = "[ACTIVATED]";
                            } else {
                                $InvStatus = "[SENT]";
                            }
                            $invstyles = "color:silver; font-weight: bold;";
                            $invstyles = '';
                        }
                        break;

                    case "PAID":
                        $InvStatus = "[PAID]";
                        $invstyles = "color:green; font-weight: bold;";
                        break;
                }

                echo '
                    <tr>
                    <td>
                    <form action="" name="LstLead' . $llc . '" id="LstLead' . $llc . '" method="post">
                    <input type="hidden" name="task">
                    <input type="hidden" name="Ldid" value="' . $ll['id_lead'] . '">
                    </form>' . $ll['client_id'] . '</td>
                    <td><div class="media align-items-center">' . "<a href=\"javascript:submitbutton('LeadDet" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\">" . $ll['client_name'] . '</a></div></td>
                    <td><div class="media align-items-center">' . "<a href=\"javascript:submitbutton('LeadChStat" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\" style=\"" . $invstyles . "\">" . $ll['status_nm'] . ' ' . $InvStatus . '</a></div></td>
                    <td><small class="text-muted">' . $LstUpdate[1] . '</small></td>
                    <td>' . $ll['package_name'] . '</td>
                    <td>' . $ll['source'] . '</td>
                    <td><div class="media align-items-center">
                        <div class="avatar avatar-xs mr-2">' . "<img src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $rcAv["avatar"])) . "' class='avatar-img rounded-circle' alt='Avatar'>" . '</div>
                        <div class="media-body"> <span class="js-lists-values-employee-name" style="' . $styles . '">' . $namasales . '</span> </div>
                          </div>
                    </td>
                  </tr>
                ';
            }
            ?>
            </tbody>
        </table>
    </div>
</div>
<div class="mt-4">
    <?php
    echo $d->print_page();
    echo $d->form_print_page();
    ?>
</div>