<?php
$dLead = "select * from " . $tblp . "apps_lead where id_lead ='" . $_SESSION["LeadChStat"] . "'";
$rdLead = $dbs->getArr($dLead);

$cTrack = "select id_trklist from " . $tblp . "apps_trklist where lead_id = '" . $_SESSION["LeadChStat"] . "'";
$rcTrack = $dbs->getArr($cTrack);
if ($rcTrack['id_trklist']) {
	$readonly = "disabled";
} else {
	$readonly = "";
}

# Cek Invoice
$CurrInv = "Select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl_create desc limit 1";
$rCurrInv = $dbs->getArr($CurrInv);

# Get Inv Detail
if ($rCurrInv['id'] == '') {
	# First Inv
	$cPackPrice = "select price from " . $tblp . "apps_package where kode = '" . $rdLead['package_id'] . "'";
	$rcPackPrice = $dbs->getArr($cPackPrice);

	$cWrk = "Select kode from " . $tblp . "apps_packagework where id_lead = '" . $_SESSION["LeadChStat"] . "' order by urutan asc";
	$rcWrk = $dbs->getQuery($cWrk);
	while ($rwrk = $dbs->getAssoc($rcWrk)) {
		$wrkc++;
		if ($wrkc == 1) {
			$lstwrk = $rwrk['kode'];
		} else {
			$lstwrk .= ", " . $rwrk['kode'];
		}
	}

	$invnum = "Auto Generated";
	$issuedate = date("d-m-Y");
	$duedate = date('d-m-Y', strtotime("+3 days"));
	$ammountdue = $rcPackPrice['price'];

	$name = "";
	if (!empty($rdLead['comp_nm1'])) {
		$name .= $rdLead['comp_nm1'];
	}
	if (!empty($rdLead['comp_nm2'])) {
		$name .= " / " . $rdLead['comp_nm2'];
	}
	if (!empty($rdLead['comp_nm3'])) {
		$name .= " / " . $rdLead['comp_nm3'];
	}
	if (empty($name)) {
		$name = "Company name has not been set yet";
	}

	$pic = $rdLead['client_name'];
	$email = $rdLead['client_email'];
	$phnumber = $rdLead['client_wa'];
	$itm_01 = $rdLead['package_name'];
	$itm_02 = $lstwrk;
	$price_01 = $rcPackPrice['price'];
	$price_05 = 0;
} else {
	# Last Inv
	$cLstInv = "select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl_create desc limit 1";
	$rli = $dbs->getArr($cLstInv);

	$invnum = $rli['inv_num'];
	$issuedate = convertDate($rli['issue_date']);
	$duedate = convertDate($rli['due_date']);
	$ammountdue = $rli['total_due'];
	$name = $rli['name'];
	$pic = $rli['pic'];
	$email = $rli['email'];
	$phnumber = $rli['phone_number'];

	$cid = "select item, price, no_urut from " . $tblp . "apps_invitem where id_inv = '" . $rli['id'] . "'";
	$rcid = $dbs->getQuery($cid);
	while ($citm = $dbs->getAssoc($rcid)) {
		${"itm_" . sprintf("%02d", $citm['no_urut'])} = $citm['item'];
		${"price_" . sprintf("%02d", $citm['no_urut'])} = $citm['price'];
	}
}

if (empty($TabStatus) and empty($TabInvoice) and empty($TabSending) and empty($TabBukti)) {
	$TabStatus = "active";
}

?>

<div class="container-fluid page__container">
	<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		<li class="nav-item"> <a class="nav-link <?php echo $TabStatus ?>" id="status-update-tab" data-toggle="pill" href="#status-update" role="tab" aria-controls="status-update" aria-selected="true">UPDATE STATUS</a> </li>
		<li class="nav-item"> <a class="nav-link <?php echo $TabInvoice ?>" id="status-invoice-tab" data-toggle="pill" href="#status-invoice" role="tab" aria-controls="status-invoice" aria-selected="false">GENERATE INVOICE</a> </li>
		<li class="nav-item"> <a class="nav-link <?php echo $TabSending ?>" id="status-sending-tab" data-toggle="pill" href="#status-sending" role="tab" aria-controls="status-sending" aria-selected="false">SEND INVOCE</a>
		<li class="nav-item"> <a class="nav-link <?php echo $TabBukti ?>" id="status-bukti-tab" data-toggle="pill" href="#status-bukti" role="tab" aria-controls="status-bukti" aria-selected="false">PAYMENT PROOF</a> </li>
	</ul>
	<div class="tab-content" id="pills-tabContent">
		<div class="tab-pane fade show <?php echo $TabBukti ?>" id="status-bukti" role="tabpanel" aria-labelledby="status-bukti-tab">
			<div class="card card-form">
				<div class="row no-gutters">
					<div class="col-lg-12 card-form__body card-body">
						<div class="form-group">
							<label for="UpFl"><?php echo $lang_2041 ?></label>
							<form action="" method="post" name="PayFrm" enctype="multipart/form-data">
								<input type="hidden" name="task" />
								<div class="custom-file">
									<label for="UpFl" class="custom-file-label"><?php echo "Select " . $lang_2041 ?></label>
									<input id="UpFl" name="UpFl" type="file" class="custom-file-input" />
								</div>
							</form>
							<script>
								$(".custom-file-input").on("change", function() {
									var fileName = $(this).val().split("\\").pop();
									var pjg = fileName.length;
									var ambil = 87;
									var mulai = pjg - ambil;
									if (pjg > ambil) {
										var shortText = '...' + jQuery.trim(fileName).substring(mulai);
									} else {
										var shortText = fileName;
									}
									$(this).siblings(".custom-file-label").addClass("selected").html(shortText);
								});
							</script>
						</div>
						<div class="text-right mb-5">
							<?php
							echo ButtonsCommon("commonbuttons", THEMES, "Upgrade_i", "Upload Payment Proof", "PayFile_" . $_SESSION["butts_Save"], "PayFrm", "", "left");
							?>
						</div>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="table-responsive border-bottom">
					<table class="table mb-0 thead-border-top-0">
						<thead>
							<tr>
								<th style="width: 5%;"><?php echo strtoupper('NO') ?></th>
								<th style="width: 45%;"><?php echo strtoupper('File Name') ?></th>
								<th style="width: 15%;"><?php echo strtoupper('Uploaded Date') ?></th>
								<th style="width: 15%"><?php echo strtoupper('Uploaded By') ?></th>
								<th style="width: 20%;"></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$lstF = "Select id, nmfile, nmfile_asli, upload_by, upload_by_id, tgl from " . $tblp . "apps_invbayar where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl desc";
							$rlstF = $dbs->getQuery($lstF);
							while ($lf = $dbs->getAssoc($rlstF)) {
								$cAv = "Select id, nama_asli from " . $tblp . "sys_lgndetail where id_login = '" . $lf['upload_by_id'] . "'";
								$rcAv = $dbs->getArr($cAv);
								if (empty($rcAv['id'])) {
									$styles = "color:red;";
									$namasales = $lf['upload_by'];
								} else {
									$styles = "";
									$namasales = $rcAv['nama_asli'];
								}

								$volc++;
								if ($volc % 2 == 0) {
									$wilsclass = "selected";
								} else {
									$wilsclass = "";
								}
								if (empty($vol['old_status_nm'])) {
									$vol['old_status_nm'] = "About to Contact";
								}
								echo '
										<form action = "" method="post" name="FlLstFrm_' . $volc . '" id="FlLstFrm_' . $volc . '">
										<input type="hidden" name="task">
										<input type="hidden" name="FlId" value = "' . $lf['id'] . '">
										</form>
										<tr class="' . $wilsclass . '">
										<td>' . $volc . '</td>
										<td>' . "<a href=\"javascript:void()\" onclick=\"window.open('../../pdf.html?x=payment&y=" . $lf['id'] . "', 'MsgWindow', 'toolbar=0,location=0,menubar=0,height=500,width=500');\">" . $lf['nmfile_asli'] . '</a></td>
                    					<td>' . c_date($lf['tgl'], "longdate", "short", $list_month, $list_month_short) . '</td>
                    					<td ' . $styles . '">' . $namasales . '</td>
										<td>
										' .
									"<a href=\"javascript:submitbutton('PayFile_" . $_SESSION["butts_Del"] . "','FlLstFrm_" . $volc . "');\" id=\"LstFile_" . $_SESSION["butts_Del"] . "\" onClick=\"return konfirmasi('File will be deleted. Continue?')\" class=\"btn btn-danger float-right btn-sm\"><i class=\"material-icons mr-1\">delete_forever</i>Delete</a>"
									. '
										</td></tr>
									';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="card card-form">
				<div class="row no-gutters">
					<div class="col-lg-12 card-form__body card-body">
						<form action="" method="post" name="FinNotif">
							<input type="hidden" name="task" />
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="TglByr">Payment Date</label>
										<input id="TglByr" name="TglByr" type="text" data-toggle="flatpickr" style="width:200px;">
									</div>
								</div>
								<div class="col">
									<!--div class="form-group">
                        		<label for="FinStaff">Finance Staff</label>
                                <select id="FinSt" name="FinSt" data-toggle="select" class="form-control" >
                      				<option value="">--</option>
                      				<option value="wawan@pacific.net.id|Verena">Verena - PT. Wahana Rahmat Nusa</option>
                                    <option value="barep@pacificlink.co.id|Neni">Neni - PT. Ijin Usaha Indonesia</option>
                    			</select>
                            </div-->
								</div>
							</div>
						</form>
						<div class="text-right mb-5">
							<?php
							echo ButtonsCommon("commonbuttons", THEMES, "PagingNext_i", "Send Notification to Finance", "FinNotif_" . $_SESSION["butts_Save"], "FinNotif", "Notification email will be sent to Finance Team. Make sure all payment proofs are right and uploaded. Continue?", "left");
							?>
						</div>
					</div>
				</div>
			</div>

			<div class="card">
				<div class="table-responsive border-bottom">
					<table class="table mb-0 thead-border-top-0">
						<thead>
							<tr>
								<th style="width: 5%;"><?php echo strtoupper('NO') ?></th>
								<!--th style="width: 60%;"><?php echo strtoupper('Sent To') ?></th-->
								<th style="width: 55%;"><?php echo strtoupper('Sent By') ?></th>
								<th style="width: 40%"><?php echo strtoupper('Sent Date') ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$volc = 0;
							$lstF = "Select tgl, fin_email, fin_name, notif_by from " . $tblp . "apps_invnotif where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl desc";
							$rlstF = $dbs->getQuery($lstF);
							while ($lf = $dbs->getAssoc($rlstF)) {
								$volc++;
								echo '
										<tr>
										<td>' . $volc . '</td>
										<!--td>' . $lf['fin_name'] . " (" . $lf['fin_email'] . ") " . '</td-->
										<td ' . $styles . '">' . $lf['notif_by'] . '</td>
                    					<td>' . c_date($lf['tgl'], "longdate", "short", $list_month, $list_month_short) . '</td>
										</tr>
									';
							}
							?>
						</tbody>
					</table>
				</div>
			</div>


		</div>
		<div class="tab-pane fade show <?php echo $TabStatus ?>" id="status-update" role="tabpanel" aria-labelledby="status-update-tab">
			<div class="card card-form">
				<div class="row no-gutters">
					<div class="col-lg-12 card-form__body card-body">
						<form action="" method="post" name="UpStatLead" id="UpStatLead">
							<input type="hidden" name="task" />
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="LeadStatus">Lead Status</label>
										<select id="LeadStatus" name="LeadStatus" data-toggle="select" class="form-control" style="width:200px;" <?php echo $readonly ?>>
											<option value="">--</option>
											<?php
											foreach (LEAD_STATUS as $InCode => $InName) {
												if ($InCode != 'IS') {
													if ($InCode == $rdLead['status']) {
														$src_sel = "selected";
													} else {
														$src_sel = '';
													}
													echo '<option value="' . $InCode . '" ' . $src_sel . '>' . $InName . '</option>';
												}
											}
											?>
										</select>
									</div>
								</DIV>
								<div class="col"> &nbsp; </div>
							</div>
							<div class="form-group">
								<?php

								$nl = preg_replace("#<br\s*/?>#i", "\n", $rdLead['status_reason']);

								?>
								<label for="alasan"><?php echo $lang_2034 ?></label>
								<textarea id="alasan" name="alasan" rows="4" class="form-control" placeholder="<?php echo $lang_2034 ?> ..." <?php echo $readonly ?>></textarea>
							</div>
						</form>
						<div class="text-right mb-5">
							<?php
							if (!$rcTrack['id_trklist']) {
								echo ButtonsCommon("commonbuttons", THEMES, "Upgrade_i", $lang_2033, "UpStat_" . $_SESSION["butts_Update"], "UpStatLead", "", "left");
							}
							?>
						</div>
					</div>
				</div>
				<div class="table-responsive border-bottom">
					<table class="table mb-0 thead-border-top-0">
						<thead>
							<tr>
								<th style="width: 30px;"><?php echo strtoupper('NO') ?></th>
								<th style="width: 250px;"><?php echo strtoupper('Status Change') ?></th>
								<th style="width: 150px;"><?php echo strtoupper('Changed Date') ?></th>
								<th style="width: 150px;"><?php echo strtoupper('Changed By') ?></th>
								<th style="width: 300px;"><?php echo strtoupper('Note/Reason') ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$lvo = "Select tgl_update, old_status, status, old_status_nm, status_nm, update_by_name, update_by_id, alasan  from " . $tblp . "apps_lead_status where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl_update desc";
							$rlvo = $dbs->getQuery($lvo);
							while ($vol = $dbs->getAssoc($rlvo)) {

								$cAv = "Select id, nama_asli from " . $tblp . "sys_lgndetail where id_login = '" . $vol['update_by_id'] . "'";
								$rcAv = $dbs->getArr($cAv);
								if (empty($rcAv['id'])) {
									$styles = "color:red;";
									$namasales = $vol['update_by_name'];
								} else {
									$styles = "";
									$namasales = $rcAv['nama_asli'];
								}

								$volc++;
								if ($volc % 2 == 0) {
									$wilsclass = "selected";
								} else {
									$wilsclass = "";
								}
								if (empty($vol['old_status_nm'])) {
									$vol['old_status_nm'] = "About to Contact";
								}
								echo '
					<tr class="' . $wilsclass . '">
					<td style="width: 30px;">' . $volc . '</td>
					<td style="width: 250px;">' . $vol['old_status_nm'] . " &rarr; " . $vol['status_nm'] . '</td>
                    <td style="width: 150px;">' . c_date($vol['tgl_update'], "longdate", "short", $list_month, $list_month_short) . '</td>
                    <td style="width: 150px; ' . $styles . '">' . $namasales . '</td>
                    <td style="width: 300px;">' . nl2br($vol['alasan']) . '</td>
					</tr>
					';
							}

							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane fade show <?php echo $TabInvoice ?>" id="status-invoice" role="tabpanel" aria-labelledby="status-invoice-tab">
			<script>
				/*
	Ini untuk ubah Ammount Due dengan mengubah price 01 s.d price 05
	Khusus price 05 adalah discount. Jadi pengurangan
	*/
				$(document).ready(function() {
					function calcall() {

						var invprc01 = $("#invprc01").val();
						var invprc02 = $("#invprc02").val();
						var invprc03 = $("#invprc03").val();
						var invprc04 = $("#invprc04").val();
						var invprc05 = $("#invprc05").val();
						var invprc06 = $("#invprc06").val();

						invprc01 = parseInt(invprc01.replace(/\D/g, ''));
						invprc02 = parseInt(invprc02.replace(/\D/g, ''));
						invprc03 = parseInt(invprc03.replace(/\D/g, ''));
						invprc04 = parseInt(invprc04.replace(/\D/g, ''));
						invprc05 = parseInt(invprc05.replace(/\D/g, ''));
						invprc06 = parseInt(invprc06.replace(/\D/g, ''));

						if (invprc01 !== invprc01) {
							invprc01 = 0;
						}
						if (invprc02 !== invprc02) {
							invprc02 = 0;
						}
						if (invprc03 !== invprc03) {
							invprc03 = 0;
						}
						if (invprc04 !== invprc04) {
							invprc04 = 0;
						}
						if (invprc05 !== invprc05) {
							invprc05 = 0;
						}
						if (invprc06 !== invprc06) {
							invprc06 = 0;
						}

						var hasilnya = 0;

						hasilnya = invprc01 + invprc02 + invprc03 + invprc04 - invprc05 + invprc06;
						hasilnya = hasilnya.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")
						return hasilnya;
					}

					$("#invprc01").change(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc01").keyup(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc02").change(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc02").keyup(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc03").change(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc03").keyup(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc04").change(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc04").keyup(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc05").change(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc05").keyup(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc06").change(function() {
						total = calcall();
						$("#ammdue").val(total);
					});

					$("#invprc06").keyup(function() {
						total = calcall();
						$("#ammdue").val(total);
					});
				});
			</script>
			<div class="card card-form">
				<div class="row no-gutters">
					<div class="col-lg-12 card-form__body card-body">
						<form action="" method="post" name="CrtInv" id="CrtInv">
							<input type="hidden" name="task" />
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="InvNum">Invoice Number</label>
										<input id="InvNum" name="InvNum" type="text" class="form-control" disabled value="<?php echo $invnum ?>">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="IssueDt">Issue Date</label>
										<input id="IssueDt" name="IssueDt" type="text" class="form-control" data-toggle="flatpickr" value="<?php echo $issuedate ?>">
									</div>
								</div>
								<?php
								#Find Next Due Date

								?>
								<div class="col">
									<div class="form-group">
										<label for="DueDt">Due Date</label>
										<input id="DueDt" name="DueDt" type="text" class="form-control" data-toggle="flatpickr" value="<?php echo $duedate; ?>">
									</div>
								</div>
								<div class="col">
									<div class="form-group"> <?php echo "<script>jQuery(function($){ $('#ammdue').autoNumeric('init', {aSep: '.', aDec: ',', mDec: '0'});});</script>"; ?>
										<label for="ammdue">Amount Due (IDR)</label>
										<input id="ammdue" name="ammdue" type="text" class="form-control" style="text-align:right; font-size:17px; font-weight:700; color:#FF0000;" value='<?php echo $ammountdue ?>' disabled="disabled">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="invname">Name</label>
								<input id="invname" name="invname" type="text" class="form-control" value="<?php echo $name ?>">
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invpic">PIC</label>
										<input id="invpic" name="invpic" type="text" class="form-control" value="<?php echo $pic ?>">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="invmail">Email</label>
										<input id="invmail" name="invmail" type="text" class="form-control" value="<?php echo $email ?>">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="invphnum">Phone Number</label>
										<input id="invphnum" name="invphnum" type="text" class="form-control" value="<?php echo $phnumber ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invitm01">Item 01</label>
										<input id="invitm01" name="invitm01" type="text" class="form-control" value="<?php echo $itm_01 ?>">
									</div>
								</div>
								<div class="col">
									<script>
										jQuery(function($) {
											$('#invprc01').autoNumeric('init', {
												aSep: '.',
												aDec: ',',
												mDec: '0'
											});
										});
									</script>
									<div class="form-group">
										<label for="invprc01">Price 01 (IDR)</label>
										<input id="invprc01" name="invprc01" type="text" class="form-control" value="<?php echo $price_01 ?>" style="text-align:right;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invitm02">Item 02</label>
										<input id="invitm02" name="invitm02" type="text" class="form-control" value="<?php echo $itm_02 ?>">
									</div>
								</div>
								<div class="col">
									<script>
										jQuery(function($) {
											$('#invprc02').autoNumeric('init', {
												aSep: '.',
												aDec: ',',
												mDec: '0'
											});
										});
									</script>
									<div class="form-group">
										<label for="invprc02">Price 02 (IDR)</label>
										<input id="invprc02" name="invprc02" type="text" class="form-control" value="<?php echo $price_02 ?>" style="text-align:right;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invitm03">Item 03</label>
										<input id="invitm03" name="invitm03" type="text" class="form-control" value="<?php echo $itm_03 ?>">
									</div>
								</div>
								<div class="col">
									<script>
										jQuery(function($) {
											$('#invprc03').autoNumeric('init', {
												aSep: '.',
												aDec: ',',
												mDec: '0'
											});
										});
									</script>
									<div class="form-group">
										<label for="invprc03">Price 03 (IDR)</label>
										<input id="invprc03" name="invprc03" type="text" class="form-control" value="<?php echo $price_03 ?>" style="text-align:right;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invitm04">Item 04</label>
										<input id="invitm04" name="invitm04" type="text" class="form-control" value="<?php echo $itm_04 ?>">
									</div>
								</div>
								<div class="col">
									<script>
										jQuery(function($) {
											$('#invprc04').autoNumeric('init', {
												aSep: '.',
												aDec: ',',
												mDec: '0'
											});
										});
									</script>
									<div class="form-group">
										<label for="invprc04">Price 04 (IDR)</label>
										<input id="invprc04" name="invprc04" type="text" class="form-control" value="<?php echo $price_04 ?>" style="text-align:right;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invitm06">Item 05</label>
										<input id="invitm06" name="invitm06" type="text" class="form-control" value="<?php echo $itm_06 ?>">
									</div>
								</div>
								<div class="col">
									<script>
										jQuery(function($) {
											$('#invprc06').autoNumeric('init', {
												aSep: '.',
												aDec: ',',
												mDec: '0'
											});
										});
									</script>
									<div class="form-group">
										<label for="invprc06">Price 05 (IDR)</label>
										<input id="invprc06" name="invprc06" type="text" class="form-control" value="<?php echo $price_06 ?>" style="text-align:right;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="invitm05">Discount</label>
										<input id="invitm05" name="invitm05" type="text" class="form-control" value="<?php echo $itm_05 ?>">
									</div>
								</div>
								<div class="col">
									<script>
										jQuery(function($) {
											$('#invprc05').autoNumeric('init', {
												aSep: '.',
												aDec: ',',
												mDec: '0'
											});
										});
									</script>
									<div class="form-group">
										<label for="invprc05">Amount (IDR)</label>
										<input id="invprc05" name="invprc05" type="text" class="form-control" value="<?php echo $price_05 ?>" style="text-align:right;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="BnkAcc">Bank Account</label>
										<select id="BnkAcc" name="BnkAcc" data-toggle="select" class="form-control" style="width:100%;">
											<option value="" disabled>--</option>
											<?php
											foreach (BANK_ACC as $AccCode => $AccName) {
												if ($AccCode == $rli['bank_acc']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												$AccNameDet = explode("|", $AccName);
												if ($AccNameDet[4] == 'Active') {
													echo '<option value="' . $AccCode . '" ' . $src_sel . '>' . $AccNameDet[1] . " - " . $AccNameDet[2] . '</option>';
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
						</form>
						<div class="text-right mb-5">
							<?php
							if (!$rcTrack['id_trklist']) {
								echo ButtonsCommon("Dashboard_commonbuttons", THEMES, "Inv_i", "Generate Invoice", "InPrev_" . $_SESSION["buttkey"], "CrtInv", "Please re-check your invoice details before proceeding to generate invoice. Continue?", "left");
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade show <?php echo $TabSending ?>" id="status-sending" role="tabpanel" aria-labelledby="status-sending-tab">
			<?php
			$cInvSkr = "select inv_num, name from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
			$rcInvSkr = $dbs->getArr($cInvSkr);

			$dir = "../../" . UPDIR . "/Invoices";
			$pdffile = $dir . "/" . $rcInvSkr['inv_num'] . ".pdf";
			$pdffilename = preg_replace("/\W/", "_", $rcInvSkr['name']);

			if (is_file($pdffile)) {
				echo '<iframe src="../../pdf.html" scrolling="no" align="middle" frameborder="0" width="100%" height="1200"></iframe>';
			} else {
				echo '
					<div class="text-center mb-5"><br><br><strong>Invoice cannot be found!</strong><br><br></div>
					';
			}
			?>
			<div class="card card-form">
				<div class="row no-gutters">
					<div class="col-lg-12 card-form__body card-body">
						<div class="row"><br /></div>
						<form action="" method="post" name="InvPreview">
							<input type="hidden" name="task" />
							<?php
							if ($rCurrInv['tipe'] == "SENT" and $rCurrInv['sending'] == "Y") {
								echo '
							<div class="form-group">
        						<label for="alasan_cancel">Reason of Cancelation</label>
            					<textarea id="alasan_cancel" name="alasan_cancel" rows="4" class="form-control" placeholder="Reason of cancelation ..."></textarea>
        					</div>
							';
							}
							?>
						</form>
						<div class="text-right mb-5">
							<?php
							if (!$rcTrack['id_trklist']) {
								if ($rCurrInv['tipe'] == "SENT" and ($rCurrInv['sending'] == "Y" or $rCurrInv['unduh'] == "Y")) {
									if ($rCurrInv['unduh'] == "Y") {
										echo ButtonsCommon("commonbuttons", THEMES, "Deny_i", "Cancel Invoice", "InvCncl_" . $_SESSION["butts_Reset"], "InvPreview", "This Invoice has been activated but not been paid yet. You are about to CANCEL THIS INVOICE and cancelation email wiil be sent automatically to the Client. Continue?", "left");
									} else {
										echo ButtonsCommon("commonbuttons", THEMES, "Deny_i", "Cancel Invoice", "InvCncl_" . $_SESSION["butts_Reset"], "InvPreview", "This Invoice has activated and been sent but not been paid yet. You are about to CANCEL THIS INVOICE and cancelation email wiil be sent automatically to the Client. Continue?", "left");
									}
								} elseif ($rCurrInv['tipe'] == "PREVIEW") {
									echo ButtonsCommon("commonbuttons", THEMES, "Del_i", "Delete Invoice", "InvDel_" . $_SESSION["butts_Del"], "InvPreview", "Invoice will be deleted. You can always re-generate new invoice from Generate Invoce Tab. Continue?", "left");
									echo ButtonsCommon("commonbuttons", THEMES, "PagingNext_i", "Send Invoice", "InvSend_" . $_SESSION["butts_Unlink"], "InvPreview", "Invoice will be send!. PLEASE CHECK AND RE-CHECK THE INVOICE. Continue?", "left");
									echo ButtonsCommon("commonbuttons", THEMES, "Downgrade_i", "Activated Invoice", "InvDld_" . $_SESSION["butts_Unlink"], "InvPreview", "Invoice will be activated and will NOT be sent to client!. PLEASE CHECK AND RE-CHECK THE INVOICE. Continue?", "left");
								} elseif ($rCurrInv['tipe'] == "PREVIEW" and $rCurrInv['sending'] == "Y") {
								} elseif ($rCurrInv['tipe'] == "PAID" and $rCurrInv['paid'] == "Y") {
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>