<?php
$dLead = "SELECT a.*, b.locale, c.paid, d.tracking_id FROM " . $tblp . "apps_lead a ";
$dLead .= "LEFT JOIN " . $tblp . "apps_client b on b.kode = a.client_id ";
$dLead .= "LEFT JOIN " . $tblp . "apps_invdet c on c.id_lead = a.id_lead ";
$dLead .= "LEFT JOIN " . $tblp . "apps_trklist d on d.lead_id = a.id_lead ";
$dLead .= "WHERE a.id_lead = '" . $_SESSION["LeadDets"] . "'";
$rdLead = $dbs->getArr($dLead);
if ($rdLead['status'] == 'CL' || $rdLead['tracking_id'] != NULL) {
	$disabled = "disabled";
} else {
	$disabled = "";
}

if (empty($TabClient) and empty($TabSales) and empty($TabCompany) and empty($TabBocBod) and empty($TabUpfile)) {
	$TabClient = "active";
}

?>

<div class="container-fluid page__container">
	<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
		<li class="nav-item">
			<a class="nav-link <?php echo $TabClient ?>" id="lead-client-detail-tab" data-toggle="pill" href="#lead-client-detail" role="tab" aria-controls="lead-client-detail" aria-selected="true">CLIENT & SALES</a>
		</li>
		<!--li class="nav-item">
        	<a class="nav-link <?php echo $TabSales ?>" id="lead-sales-detail-tab" data-toggle="pill" href="#lead-sales-detail" role="tab" aria-controls="lead-sales-detail" aria-selected="false">SALES DETAIL</a>
       	</li-->
		<li class="nav-item">
			<a class="nav-link <?php echo $TabCompany ?>" id="lead-company-detail-tab" data-toggle="pill" href="#lead-company-detail" role="tab" aria-controls="lead-company-detail" aria-selected="false">COMPANY DETAIL</a>
		</li>
		<!--li class="nav-item>">
        	<a class="nav-link <?php echo $TabBocBod ?>" id="lead-bocbod-detail-tab" data-toggle="pill" href="#lead-bocbod-detail" role="tab" aria-controls="lead-bocbod-detail" aria-selected="false">BOC & BOD DETAIL</a>
        </li-->
		<li class="nav-item">
			<a class="nav-link <?php echo $TabUpfile ?>" id="lead-file-detail-tab" data-toggle="pill" href="#lead-file-detail" role="tab" aria-controls="lead-file-detail" aria-selected="false">UPLOAD FILE</a>
		</li>
	</ul>
	<div class="tab-content" id="pills-tabContent">
		<!--div class="tab-pane fade show <?php echo $TabSales ?>" id="lead-sales-detail" role="tabpanel" aria-labelledby="lead-sales-detail-tab">
        <!-- DISATUKAN DI CLIENT DETAIL -->
		</div-->

		<div class="tab-pane fade show <?php echo $TabClient ?>" id="lead-client-detail" role="tabpanel" aria-labelledby="lead-client-detail-tab">
			<form action="" method="post" name="LeadClientDetail" id="LeadClientDetail">
				<input type="hidden" name="task" />
				<div class="card card-form">
					<div class="row no-gutters">
						<div class="col-lg-12 card-form__body card-body">
							<div class="form-group">
								<label for="ClID"><?php echo $lang_2009 ?></label>
								<input id="ClID" name="ClID" type="text" class="form-control" readonly="readonly" style="width:100px;" value="<?php echo $rdLead['client_id'] ?>">
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="ClNm"><?php echo $lang_2006 ?>*</label>
										<input id="ClNm" name="ClNm" type="text" class="form-control" placeholder="<?php echo $lang_2006 ?> ..." value="<?php echo $rdLead['client_name'] ?>" readonly="readonly">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="ClHbd"><?php echo $lang_2007 ?> *</label>
										<input id="ClHbd" name="ClHbd" type="text" class="form-control" data-toggle="flatpickr" value="<?= (!in_array($rdLead['client_tgllhr'], [NULL, '0000-00-00'])) ? convertDate($rdLead['client_tgllhr']) : '' ?>">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="ClWa"><?php echo $lang_2008 ?>*</label>
										<input id="ClWa" name="ClWa" type="text" class="form-control" placeholder="<?php echo $lang_2008 ?> ..." value="<?php echo $rdLead['client_wa'] ?>">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="ClMail"><?php echo $lang_2010 ?> *</label>
										<input id="ClMail" name="ClMail" type="text" class="form-control" placeholder="<?php echo $lang_2010 ?> ..." value="<?php echo $rdLead['client_email'] ?>">
									</div>
								</div>
							</div>
							<!--div class="row">
							<div class="col-6">
								<div class="form-group">
									<label for="ClLocale"><?php echo $lang_2060 ?> *</label>
									<select id="ClLocale" name="ClLocale" data-toggle="select" class="form-control" <?php echo $disabled ?> >
										<option value="">--</option>
										<option value="id" <?= ($rdLead['locale'] == 'id') ? 'selected' : '' ?>>Indonesian</option>
										<option value="en" <?= ($rdLead['locale'] == 'en') ? 'selected' : '' ?>>English</option>
									</select>
								</div>
							</div>
						</div-->
							<div class="text-right mb-5">
								<?php
								//if(!$disabled) {
								echo ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_2051, "LeadClient_" . $_SESSION["butts_Update"], "LeadClientDetail", "", "left");
								//}
								?>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form action="" method="post" name="LeadSalesDetail" id="LeadSalesDetail">
				<input type="hidden" name="task" />
				<div class="card card-form">
					<div class="row no-gutters">
						<div class="col-lg-12 card-form__body card-body">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="Sales"><?php echo $lang_6038 ?> *</label>
										<select id="Sales" name="Sales" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											if ($_SESSION['Jbtn'] == "Manager" || in_array($_SESSION['lgnPriv'], [0, 2])) { // Manager or lvl 0, 2
												$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where (priv = 4 or priv = 2) order by nama_asli asc";
											} else {
												$cSales = "Select id, id_login, nama_asli, avatar from " . $tblp . "sys_lgndetail where id = '" . $rdLead['sales_id'] . "' order by nama_asli asc";
											}
											$rcSales = $dbs->getQuery($cSales);
											$foundSales = false;
											while ($sls = $dbs->getAssoc($rcSales)) {
												if ($sls['id'] == $rdLead['sales_id']) {
													$sales_sel = "selected";
													$foundSales = true;
												} else {
													$sales_sel = '';
												}
												echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $sls['id'] . "' " . $sales_sel . ">" . $sls['nama_asli'] . "</option>";
											}
											if (!$foundSales) {
												echo "<option data-avatar-src='data:image/jpg;base64," . base64_encode(ShowImage("../../" . UPDIR . "/avatar/", $sls["avatar"])) . "' value='" . $rdLead['sales_id'] . "' selected>" . $rdLead['sales_name'] . "</option>";
											}
											?>
										</select>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="Source"><?php echo $lang_2011 ?> *</label>
										<select id="Source" name="Source" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											foreach (SRC_ITEM as $srcCode => $srcName) {
												if ($srcCode == $rdLead['source_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $srcCode . '" ' . $src_sel . '>' . $srcName . '</option>';
											}
											?>
										</select>
									</div>
								</div>
								<!--div class="col">
                				<div class="form-group">
                  					<label for="PilMans"><?php echo $lang_6035 ?>*</label>
                  					<select id="PilMans" name="PilMans" class="form-control" <?php echo $disabled ?>>
                    					<option value=""> -- </option>
                    					<?php
										foreach ($CompanyNums as $UP_Code => $UP_Nm) {
											if ($rdLead['KodeKantor'] == $UP_Code) {
												$kantorsel = "selected";
											} else {
												$kantorsel = '';
											}
											echo '<option value="' . $UP_Code . '" ' . $kantorsel . '>' . $UP_Nm . '</option>';
										}
										?>
                  					</select>
                				</div>
              				</div-->
							</div>
							<?php
							# Get Package Work
							$lstPackWork = "select kode from " . $tblp . "apps_packagework where id_lead = '" . $_SESSION["LeadDets"] . "'";
							$rlstPackWork = $dbs->getQuery($lstPackWork);
							$lpwArray = array();
							while ($lpw = $dbs->getAssoc($rlstPackWork)) {
								array_push($lpwArray, $lpw['kode']);
							}

							$wish = '<label>' . $lang_6010 . ' **</label>';
							$cLWi = "Select id, kode, nama from " . $tblp . "apps_works order by urutan asc";
							$rcLWi = $dbs->getQuery($cLWi);
							//$wiArr = array();
							while ($cwi = $dbs->getAssoc($rcLWi)) {
								if (in_array($cwi['kode'], $lpwArray)) {
									$lpwsel = "checked";
								} else {
									$lpwsel = "";
								}
								$wish .= '<div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input" id="' . $cwi['id'] . '" name="' . $cwi['id'] . '" value="1" ' . $lpwsel . ' ' . $disabled . '><label class="custom-control-label" for="' . $cwi['id'] . '"><span>' . $cwi['kode'] . " - " . $cwi['nama'] . '</span></label></div>';
							}
							?>
							<script>
								$(document).ready(function() {
									var packselected = $('#PackIn').find(":selected").val();
									if (packselected == 'CUSTOM') {
										$('#WiSelItm').html('<?php echo $wish ?>');
									}

									$('#PackIn').change(function() {
										if (this.value == 'CUSTOM') {
											$('#WiSelItm').html('<?php echo $wish ?>');
										} else {
											$('#WiSelItm').html('');
										}
									});
								});
							</script>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="PackIn"><?php echo $lang_2014 ?> *</label>
										<select id="PackIn" name="PackIn" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											$cPack = "Select id, nama, kode from " . $tblp . "apps_package where publish = 'Y' order by nama";
											$rcPack = $dbs->getQuery($cPack);
											while ($cp = $dbs->getAssoc($rcPack)) {
												if ($cp['kode'] == $rdLead['package_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $cp['kode'] . '" ' . $src_sel . '>' . $cp['nama'] . '</option>';
											}
											?>
										</select>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="PrefLoc"><?php echo $lang_2015 ?></label>
										<select id="PrefLoc" name="PrefLoc" data-toggle="select" class="form-control" <?php echo $disabled ?>>
											<option value="">--</option>
											<?php
											$cVolc = "Select id, lokasi from " . $tblp . "master_volocation where publish = 'Y'";
											$rcVolc = $dbs->getQuery($cVolc);
											while ($voc = $dbs->getAssoc($rcVolc)) {
												if ($voc['id'] == $rdLead['prefloc_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $voc['id'] . '" ' . $src_sel . '>' . $voc['lokasi'] . '</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group"><span id="WiSelItm"></span></div>
							<div class="text-right mb-5">
								<?php
								if (!$disabled) {
									echo ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_2052, "LeadSales_" . $_SESSION["butts_Update"], "LeadSalesDetail", "", "left");
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>

		<div class="tab-pane fade show <?php echo $TabCompany ?>" id="lead-company-detail" role="tabpanel" aria-labelledby="lead-company-detail-tab">
			<style>
				.ui-autocomplete.ui-widget {
					font-size: 13px;
				}

				.ui-state-active a,
				.ui-state-active a:link,
				.ui-state-active a:visited {
					color: #5b518b;
					/* any color you like */

				}

				.ui-widget-content .ui-state-active {
					color: #5b518b;
					/* any color you like */
				}
			</style>
			<?php
			echo '<script>';
			if (is_file("Modules/Setting/autocomplete/JS_File_KBLI.json")) {
				require "Modules/Setting/autocomplete/JS_File_KBLI.json";
				echo '
						$(function() {
							$("#Indus1Name").autocomplete({
								source: dataKBLI,
								focus: 	function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
									$("#Indus1").val(ui.item.value);
								},
								minLength: 2
							});

							$("#Indus2Name").autocomplete({
								source: dataKBLI,
								focus: 	function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
									$("#Indus2").val(ui.item.value);
								},
								minLength: 2
							});

							$("#Indus3Name").autocomplete({
								source: dataKBLI,
								focus: 	function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
									$("#Indus3").val(ui.item.value);
								},
								minLength: 2
							});

							$("#Indus4Name").autocomplete({
								source: dataKBLI,
								focus: 	function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
									$("#Indus4").val(ui.item.value);
								},
								minLength: 2
							});

							$("#Indus5Name").autocomplete({
								source: dataKBLI,
								focus: 	function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
									$("#Indus5").val(ui.item.value);
								},
								minLength: 2
							});

							$("#tdpName").autocomplete({
								source: dataKBLI,
								focus: 	function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
								},
								select: function(event, ui) {
									event.preventDefault();
									$(this).val(ui.item.label);
									$("#tdp").val(ui.item.value);
								},
								minLength: 2
							});

						});
					';
			}

			echo '</script>';
			?>
			<form action="" method="post" name="LeadCompanyDetail" id="LeadCompanyDetail">
				<input type="hidden" name="task" />
				<div class="card card-form">
					<div class="row no-gutters">
						<div class="col-lg-12 card-form__body card-body">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="CompNm1"><?php echo $lang_2017 ?> **</label>
										<input id="CompNm1" name="CompNm1" type="text" class="form-control" placeholder="<?php echo $lang_2017 ?> 1 ..." value="<?php echo $rdLead['comp_nm1'] ?>" />
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="CompNm2">&nbsp;</label>
										<input id="CompNm2" name="CompNm2" type="text" class="form-control" placeholder="<?php echo $lang_2017 ?> 2 ..." value="<?php echo $rdLead['comp_nm2'] ?>" />
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="CompNm3">&nbsp;</label>
										<input id="CompNm3" name="CompNm3" type="text" class="form-control" placeholder="<?php echo $lang_2017 ?> 3 ..." value="<?php echo $rdLead['comp_nm3'] ?>" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<?php $nl = preg_replace('#<br\s*/?>#i', "\n", $rdLead['comp_func']); ?>
										<label for="FgsUtm"><?php echo $lang_2018 ?> **</label>
										<textarea id="FgsUtm" name="FgsUtm" rows="4" class="form-control" placeholder="<?php echo $lang_2018 ?> ..."><?php echo $nl; ?></textarea>
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<?php $nl = preg_replace('#<br\s*/?>#i', "\n", $rdLead['comp_address']); ?>
										<label for="CompAddr"><?php echo $lang_2019 ?> **</label>
										<textarea id="CompAddr" name="CompAddr" rows="4" class="form-control" placeholder="<?php echo $lang_2019 ?> ..."><?php echo $nl; ?></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="Prov"><?php echo $lang_2021 ?> **</label>
										<select id="Prov" name="Prov" data-toggle="select" class="form-control" style="width: 100%">
											<option value="">--</option>
											<?php
											$cPack = "Select id_prov, nm_provinsi_lengkap from " . $tblp . "master_provinsi";
											$rcPack = $dbs->getQuery($cPack);
											while ($cp = $dbs->getAssoc($rcPack)) {
												if ($cp['id_prov'] == $rdLead['comp_province_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $cp['id_prov'] . '" ' . $src_sel . '>' . $cp['nm_provinsi_lengkap'] . '</option>';
											}
											?>
										</select>
									</div>
								</div>
								<script>
									<?php
									$cursel = '<select id="KabKota" name="KabKota" data-toggle="select" class="form-control" >';
									$cLSelKab = "Select nm_kabkota, id_kabkota from " . $tblp . "master_kabkota where id_prov = '" . $rdLead['comp_province_id'] . "'";
									$rcLSelKab = $dbs->getQuery($cLSelKab);
									while ($cs = $dbs->getAssoc($rcLSelKab)) {
										if ($rdLead['comp_city_id'] == $cs['id_kabkota']) {
											$kotasel = "selected";
										} else {
											$kotasel = '';
										}
										$cursel .= '<option value="' . $cs['id_kabkota'] . '" ' . $kotasel . '>' . $cs['nm_kabkota'] . '</option>';
									}
									$cursel .= '</select>';

									?>
									$(document).ready(function() {
										var cursel = '<?php echo $cursel ?>';
										$('#KabKotaOpts').html(cursel);
										$('#Prov').change(function() {
											if ($('#Prov').val() != '') {
												getKabKota();
											} else {
												var kabopt = '<select id=\"KabKota\" name=\"KabKota\" data-toggle=\"select\" class=\"form-control\"></select>';
												$('#KabKotaOpts').html(kabopt);
											}
										});

										function getKabKota() {
											var PropSel = $('#Prov').val();
											var KabSel = '<?php echo $rdLead['comp_city_id'] ?>';
											var pg = 'getKabKota';
											var amodul = '<?php echo $cmodule ?>';
											$.ajax({
												url: '<?php echo BASEURL ?>system/ajax.php',
												type: 'POST',
												dataType: 'html',
												data: {
													propSel: PropSel,
													kabSel: KabSel,
													pg: pg,
													amod: amodul
												},
												success: function(result) {
													if (result) {
														$('#KabKotaOpts').html(result);
														$('#KabKota').select2({
															theme: 'classic',
															width: 'resolve',
															placeholder: 'Please select',
															allowClear: true,
														});
													}
												},
											});
										}
									});
								</script>
								<div class="col">
									<div class="form-group">
										<label for="KabKota"><?php echo $lang_2022; ?> **</label>
										<span id="KabKotaOpts"></span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="csize"><?php echo $lang_2024 ?> **</label>
										<select id="csize" name="csize" data-toggle="select" class="form-control" style="width: 100%">
											<option value="">--</option>
											<?php
											foreach (JENIS_PER as $InCode => $InName) {
												if ($InCode == $rdLead['compsize_id']) {
													$src_sel = "selected";
												} else {
													$src_sel = '';
												}
												echo '<option value="' . $InCode . '" ' . $src_sel . '>' . $InName . '</option>';
											}
											?>
										</select>
									</div>
								</div>

								<div class="col">
									<div class="form-group">
										<label for="CompMail"><?php echo $lang_2025 ?> **</label>
										<input id="CompMail" name="CompMail" type="text" class="form-control" placeholder="<?php echo $lang_2025 ?> ..." value="<?php echo $rdLead['comp_email'] ?>" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="Indus1"><?php echo $lang_2020 ?> **</label>
								<input id="Indus1Name" name="Indus1Name" type="text" class="form-control" placeholder="<?php echo "Kode KBLI" ?> ..." value="<?php echo $rdLead['comp_industry1'] . " - " . $rdLead['comp_industry1_name'] ?>" />
								<input id="Indus1" type="hidden" name="Indus1" value="<?php echo $rdLead['comp_industry1'] ?>" />
							</div>
							<div class="form-group">
								<input id="Indus2Name" name="Indus2Name" type="text" class="form-control" placeholder="<?php echo "Kode KBLI" ?> ..." value="<?php echo $rdLead['comp_industry2'] . " - " . $rdLead['comp_industry2_name'] ?>" />
								<input id="Indus2" type="hidden" name="Indus2" value="<?php echo $rdLead['comp_industry2'] ?>" />
							</div>
							<div class="form-group">
								<input id="Indus3Name" name="Indus3Name" type="text" class="form-control" placeholder="<?php echo "Kode KBLI" ?> ..." value="<?php echo $rdLead['comp_industry3'] . " - " . $rdLead['comp_industry3_name'] ?>" />
								<input id="Indus3" type="hidden" name="Indus3" value="<?php echo $rdLead['comp_industry3'] ?>" />
							</div>
							<div class="form-group">
								<input id="Indus4Name" name="Indus4Name" type="text" class="form-control" placeholder="<?php echo "Kode KBLI" ?> ..." value="<?php echo $rdLead['comp_industry4'] . " - " . $rdLead['comp_industry4_name'] ?>" />
								<input id="Indus4" type="hidden" name="Indus4" value="<?php echo $rdLead['comp_industry4'] ?>" />
							</div>
							<div class="form-group">
								<input id="Indus5Name" name="Indus5Name" type="text" class="form-control" placeholder="<?php echo "Kode KBLI" ?> ..." value="<?php echo $rdLead['comp_industry5'] . " - " . $rdLead['comp_industry5_name'] ?>" />
								<input id="Indus5" type="hidden" name="Indus5" value="<?php echo $rdLead['comp_industry5'] ?>" />
							</div>

							<div class="form-group">
								<label for="tdp"><?php echo $lang_2023 ?> **</label>
								<input id="tdpName" name="tdpName" type="text" class="form-control" placeholder="<?php echo "Kode KBLI" ?> ..." value="<?php echo $rdLead['comp_tdp_industry'] . " - " . $rdLead['comp_tdp_industry_name'] ?>" />
								<input id="tdp" type="hidden" name="tdp" value="<?php echo $rdLead['comp_tdp_industry'] ?>" />
							</div>

							<div class="form-group">
								<?php $nl = preg_replace('#<br\s*/?>#i', "\n", $rdLead['catatan']); ?>
								<label for="Cttn"><?php echo $lang_2026 ?> **</label>
								<textarea id="Cttn" name="Cttn" rows="4" class="form-control" placeholder="<?php echo $lang_2026 ?> ..."><?php echo $nl ?></textarea>
							</div>
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="PengFullName"><?php echo $lang_2054 ?>*</label>
										<input id="PengFullName" name="PengFullName" type="text" class="form-control" placeholder="<?php echo $lang_2054 ?> ..." value="<?php echo $rdLead['dirut'] ?>" />
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="PengPos"><?php echo $lang_2055 ?></label>
										<select id="PengPos" name="PengPos" data-toggle="select" class="form-control" style="width: 100%">
											<option value="">--</option>
											<?php
											foreach (POS_PENGURUS as $InCode => $InName) {
												if ($InCode == $rdLead['posisi_id']) {
													$pengpos_sel = "selected";
												} else {
													$pengpos_sel = "";
												}
												echo '<option value="' . $InCode . '" ' . $pengpos_sel . '>' . $InName . '</option>';
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="text-right mb-5">
								<?php
								//if( $rdLead['status'] != 'CL' ) {
								echo ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_2053, "LeadComDets_" . $_SESSION["butts_Update"], "LeadCompanyDetail", "", "left");
								//}
								?>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="tab-pane fade show <?php echo $TabUpfile ?>" id="lead-file-detail" role="tabpanel" aria-labelledby="lead-file-detail-tab">
			<form action="" method="post" name="AddLeadFile" id="AddLeadFile" enctype="multipart/form-data">
				<input type="hidden" name="task" />
				<div class="card card-form">
					<div class="row no-gutters">
						<div class="col-lg-12 card-form__body card-body">
							<div class="row">
								<div class="col">
									<div class="form-group">
										<label for="FlTitle"><?php echo $lang_2039 ?>*</label>
										<input id="FlTitle" name="FlTitle" type="text" class="form-control" placeholder="<?php echo $lang_2039 ?> ...">
									</div>
								</div>
								<div class="col">
									<div class="form-group">
										<label for="FlDets"><?php echo $lang_2040 ?>*</label>
										<input id="FlDets" name="FlDets" type="text" class="form-control" placeholder="<?php echo $lang_2040 ?> ...">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="UpFl"><?php echo $lang_2041 ?></label>
								<div class="custom-file">
									<label for="UpFl" class="custom-file-label"><?php echo "Select " . $lang_2041 ?></label>
									<input id="UpFl" name="UpFl" type="file" class="custom-file-input" />
								</div>
								<script>
									$(".custom-file-input").on("change", function() {
										var fileName = $(this).val().split("\\").pop();
										var pjg = fileName.length;
										var ambil = 87;
										var mulai = pjg - ambil;
										if (pjg > ambil) {
											var shortText = '...' + jQuery.trim(fileName).substring(mulai);
										} else {
											var shortText = fileName;
										}
										$(this).siblings(".custom-file-label").addClass("selected").html(shortText);
									});
								</script>
							</div>
			</form>
			<div class="text-right mb-5">
				<?php
				//if( $rdLead['status'] != 'CL' ) {
				echo ButtonsCommon("commonbuttons", THEMES, "Upgrade_i", $lang_2042, "LeadFile_" . $_SESSION["butts_Save"], "AddLeadFile", "", "left");
				//}
				?>
			</div>
		</div>
	</div>
</div>
<div class="card">
	<div class="table-responsive border-bottom">
		<table class="table mb-0 thead-border-top-0">
			<thead>
				<tr>
					<th style="width: 30px;"><?php echo strtoupper('NO') ?></th>
					<th style="width: 150px;"><?php echo strtoupper('Title') ?></th>
					<th style="width: 250px;"><?php echo strtoupper('Description') ?></th>
					<th style="width: 150px;"><?php echo strtoupper('Uploaded Date') ?></th>
					<th style="width: 150px;"><?php echo strtoupper('Uploaded By') ?></th>
					<th style="width: 250px;"></th>
				</tr>
			</thead>
			<tbody>
				<?php
				$lstF = "Select id, judul, desk, nmfile, nmfile_asli, upload_by, upload_by_id, tgl_upload from " . $tblp . "apps_leadfile where id_lead = '" . $_SESSION["LeadDets"] . "'";
				$rlstF = $dbs->getQuery($lstF);
				while ($lf = $dbs->getAssoc($rlstF)) {
					$cAv = "Select id, nama_asli from " . $tblp . "sys_lgndetail where id_login = '" . $lf['upload_by_id'] . "'";
					$rcAv = $dbs->getArr($cAv);
					if (empty($rcAv['id'])) {
						$styles = "color:red;";
						$namasales = $lf['upload_by'];
					} else {
						$styles = "";
						$namasales = $rcAv['nama_asli'];
					}

					$volc++;
					if ($volc % 2 == 0) {
						$wilsclass = "selected";
					} else {
						$wilsclass = "";
					}
					if (empty($vol['old_status_nm'])) {
						$vol['old_status_nm'] = "About to Contact";
					}
					echo '
										<form action = "" method="post" name="FlLstFrm_' . $volc . '" id="FlLstFrm_' . $volc . '">
										<input type="hidden" name="task">
										<input type="hidden" name="FlId" value = "' . $lf['id'] . '">
										</form>
										<tr class="' . $wilsclass . '">
										<td style="width: 30px;">' . $volc . '</td>
										<td style="width: 150px;">' . $lf['judul'] . '</td>
                    					<td style="width: 250px;">' . $lf['desk'] . '</td>
                    					<td style="width: 150px;">' . c_date($lf['tgl_upload'], "longdate", "short", $list_month, $list_month_short) . '</td>
                    					<td style="width: 150px; ' . $styles . '">' . $namasales . '</td>
										<td style="width: 250px;">
										' .
						"<a href=\"javascript:submitbutton('LstFile_" . $_SESSION["butts_Del"] . "','FlLstFrm_" . $volc . "');\" id=\"LstFile_" . $_SESSION["butts_Del"] . "\" onClick=\"return konfirmasi('File will be deleted. Continue?')\" class=\"btn btn-danger float-right btn-sm\"><i class=\"material-icons mr-1\">delete_forever</i>Delete</a>" .
						"<a href=\"javascript:submitbutton('DLLstFile_" . $_SESSION["buttkey"] . "','FlLstFrm_" . $volc . "');\" id=\"LstFile_" . $_SESSION["Unlink"] . "\"  class=\"btn btn-dark float-right btn-sm\"><i class=\"material-icons mr-1\">arrow_downward</i>Download</a>"
						. '
										</td></tr>
									';
				}
				?>
			</tbody>
		</table>
	</div>
</div>
</div>
<!--
        <div class="tab-pane fade show <?php echo $TabBocBod ?>" id="lead-bocbod-detail" role="tabpanel" aria-labelledby="lead-bocbod-detail-tab">
    		<form action="" method="post" name="AddLeadBocBod" id="AddLeadBocBod">
          	<input type="hidden" name="task" />
  			<div class="card card-form">
    			<div class="row no-gutters">
      				<div class="col-lg-12 card-form__body card-body">

                        <div class="row">
            				<div class="col">
              					<div class="form-group">
                					<label for="PengFullName"><?php echo $lang_2054 ?>*</label>
                					<input id="PengFullName" name="PengFullName" type="text" class="form-control" placeholder="<?php echo $lang_2054 ?> ..." <?php echo $disabled ?> >
              					</div>
            				</div>
            				<div class="col">
              					<div class="form-group">
                					<label for="PengPos"><?php echo $lang_2055 ?></label>
            						<select id="PengPos" name="PengPos" data-toggle="select" class="form-control" <?php echo $disabled ?>>
              							<option value="">--</option>
              							<?php
											foreach (POS_PENGURUS as $InCode => $InName) {
												echo '<option value="' . $InCode . '">' . $InName . '</option>';
											}
											?>
            						</select>
              					</div>
            				</div>
          				</div>
        				</form>
                        <div class="text-right mb-5">
          					<?php
								if ($disabled != "disabled") {
									echo ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_2056, "LeadBocBod_" . $_SESSION["butts_Save"], "AddLeadBocBod", "", "left");
								}
								?>
        				</div>

                    </div>
				</div>
			</div>

            <div class="card">
    			<div class="table-responsive border-bottom">
    	  			<table class="table mb-0 thead-border-top-0">
        				<thead>
          					<tr>
            					<th style="width:3%;"><?php echo strtoupper('NO') ?></th>
            					<th style="width:50%;"><?php echo strtoupper($lang_2054) ?></th>
                        		<th style="width:30%;"><?php echo strtoupper($lang_2055) ?></th>
            					<th style="width:17%;"></th>
          					</tr>
        				</thead>
        				<tbody>
          					<?php
								$volc = 0;
								$lstF = "Select id, nama, posisi, posisi_id from " . $tblp . "apps_pengurus where id_lead = '" . $_SESSION["LeadDets"] . "' order by posisi_id desc";
								$rlstF = $dbs->getQuery($lstF);
								while ($lf = $dbs->getAssoc($rlstF)) {
									$volc++;
									if ($volc % 2 == 0) {
										$wilsclass = "selected";
									} else {
										$wilsclass = "";
									}
									if (empty($vol['old_status_nm'])) {
										$vol['old_status_nm'] = "About to Contact";
									}
									echo '
										<form action = "" method="post" name="NgurusFrm_' . $volc . '" id="NgurusFrm_' . $volc . '">
										<input type="hidden" name="task">
										<input type="hidden" name="FlId" value = "' . $lf['id'] . '">
										</form>
										<tr class="' . $wilsclass . '">
										<td>' . $volc . '</td>
										<td>' . $lf['nama'] . '</td>
                    					<td>' . $lf['posisi'] . '</td>
										<td>' .
										"<a href=\"javascript:submitbutton('LstPengurus_" . $_SESSION["butts_Del"] . "','NgurusFrm_" . $volc . "');\" id=\"LstPengurus_" . $_SESSION["butts_Del"] . "\" onClick=\"return konfirmasi('BOC/BOD will be deleted. Continue?')\" class=\"btn btn-danger ml-3 float-right btn-sm\"><i class=\"material-icons mr-1\">delete_forever</i>Delete</a>" .
										'</td></tr>
									';
								}
								?>
        				</tbody>
      				</table>
    			</div>
  			</div>
        </div>
         -->
</div>
</div>