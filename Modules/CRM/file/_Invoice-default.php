<?php

$BAD_KCP = $BnkAccDet[5] ?? '';

$invhtml = '
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>' . $judul_invoice . '</title>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,600;1,400&display=swap" rel="stylesheet">
<style type="text/css">
@page {
  margin: 0cm 0cm;
}

body {
  margin-top: 1.5cm;
  margin-bottom: 1cm;
  margin-left: 1.2cm;
  margin-right: 1.2cm;
  font-family: DejaVuSans;
}

#watermark {
  position: fixed;
  bottom: 0px;
  left: 0px;
  width: 21.8cm;
  height: 29.7cm;
  opacity: 0.4;
  z-index:  -1000;
}

.style4 {
  font-family: "Source Sans Pro", sans-serif;
  font-weight: bold;
  font-size: 17px;
  letter-spacing: -1px;
}
.style5 {
  font-family: "Source Sans Pro", sans-serif;
  font-size: 11.25px;
  color: #666666;
}
.style6 {
  font-size: 36px;
  font-family: "Source Sans Pro", sans-serif;
  font-weight: bolder;
  color: #666666;
  line-height: 0.9;
}
.style7 {
  font-family: "Source Sans Pro", sans-serif;
  font-size: 18px;
  font-weight: bold;
  color: #666666;
}
.style9 {
  font-family: "Source Sans Pro", sans-serif;
  font-size: 15px;
  font-weight: bold;
  color: #666666;
}
.style11 {
  font-size: 27px;
  font-weight: bold;
  font-family: Arial, Helvetica, sans-serif;
  color: #5cbb5c;
}
.style14 {
  font-size: 15px;
  color: #333333;
  font-family: Arial, Helvetica, sans-serif;
}
.style16 {
  font-size: 24px;
  font-family: Arial, Helvetica, sans-serif;
  color: #666666;
  letter-spacing: 2px;
  font-weight: bold;
}
.style20 {
  font-size: 15px;
  font-weight: bold;
  font-family: Arial, Helvetica, sans-serif;
  color:#666666;
}
.style21 {
  font-size: 24px;
  font-family: Arial, Helvetica, sans-serif;
  color: #666666;
  font-weight: bold;
}
.style22 {
  font-size: 17px;
  font-family: Arial, Helvetica, sans-serif;
  color: #666666;
  letter-spacing: 2px;
}
.style23 {
  font-size: 10px;
  font-weight: bold;
  color: #009900;
  font-family: Arial, Helvetica, sans-serif;
}
.style24 {
  font-size: 14px;
  color: #555;
  font-weight: bold;
  font-family: Arial, Helvetica, sans-serif;
}
</style>
</head>

<body>
<div id="watermark">
  ' . $company['watermark'] . '
</div>
  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="200" align="center" valign="middle">
        <table width="180" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>' . $company['logo'] . '</td>
          </tr>
          <tr>
            <td><span class="style4">' . $company['name'] . '</span></td>
          </tr>
        </table>
      </td>
      <td width="50">&nbsp;</td>
      <td>
        <table align="right" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="200" align="left" valign="top">
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div align="justify" class="style5">
                  ' . $company['address'] . '
                </div>
            </td>
          </tr>
          <tr>
            <td>
                <span class="style5">&nbsp;</span>
            </td>
          </tr>
          <tr>
            <td>
                <span class="style5">
                  <strong>NPWP: ' . $company['tax_id'] . '</strong>
                </span>
            </td>
        </tr>
        <tr>
            <td align="left">
                ' . $InvMark . '
            </td>
        </tr>
        </table>
    </td>
    <td width="50">&nbsp;</td>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><span class="style6">' . strtoupper($judul_invoice) . '</span></td>
        </tr>
        <tr>
          <td width="130"><span class="style7">Invoice Number</span></td>
          <td align="left" class="style7">' . $invnum . '</td>
        </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="15"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
            </tr>
          </table></td>
          </tr>
      </table>';

if ($judul_invoice == 'Invoice') {
  $invhtml .= '
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="20" align="left" valign="middle"><span class="style9">Payment Date</span></td>
        <td align="left" valign="middle" class="style9">' . $PayDtShow . '</td>
      </tr>
      <tr>
        <td height="20" align="left" valign="middle"><span class="style9">Amount Due</span></td>
        <td align="left" valign="middle" class="style9">IDR ' . c_curr($ammdue, ",", ".", $min_sign = "mark") . ',-</td>
      </tr>
    </table>';
} else {
  $invhtml .= '
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="130" height="20" align="left" valign="middle"><span class="style9">Issue Date</span></td>
      <td align="left" valign="middle" class="style9">' . $IssueDtShow . '</td>
    </tr>
    <tr>
      <td height="20" align="left" valign="middle"><span class="style9">Due Date</span></td>
      <td align="left" valign="middle" class="style9">' . $DueDtShow . '</td>
    </tr>
    <tr>
      <td height="20" align="left" valign="middle"><span class="style9">Amount Due</span></td>
      <td align="left" valign="middle" class="style9">IDR ' . c_curr($ammdue, ",", ".", $min_sign = "mark") . ',-</td>
    </tr>
  </table>';
}

$invhtml .= '</td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="15" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
          </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="28%" align="left" valign="middle"><span class="style11">Name</span></td>
          <td width="3%" align="left" valign="middle"><span class="style11">:</span></td>
          <td align="left" valign="middle"><span class="style14">' . $invname . '</span></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="28%" align="left" valign="middle"><span class="style11">PIC</span></td>
          <td width="3%" align="left" valign="middle"><span class="style11">:</span></td>
          <td align="left" valign="middle"><span class="style14">' . $invpic . '</span></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="28%" align="left" valign="middle"><span class="style11">Email</span></td>
          <td width="3%" align="left" valign="middle"><span class="style11">:</span></td>
          <td align="left" valign="middle"><span class="style14">' . $invmail . '</span></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="28%" align="left" valign="middle"><span class="style11">Phone Number</span></td>
          <td width="3%" align="left" valign="middle"><span class="style11">:</span></td>
          <td align="left" valign="middle"><span class="style14">' . $invphnum . '</span></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="15" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
          </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3" align="left" valign="top"><span class="style16">Invoice Details</span></td>
  </tr>
  <tr>
    <td width="70%" height="23" align="left" valign="middle"><span class="style20">' . $invitm01 . '</span></td>
    <td width="1%">&nbsp;</td>
    <td align="right" valign="middle"><span class="style20">' . $prc01show . '</span></td>
  </tr>
  <tr>
    <td width="70%" height="23" align="left" valign="middle"><span class="style20">' . $invitm02 . '</span></td>
    <td width="1%">&nbsp;</td>
    <td align="right" valign="middle"><span class="style20">' . $prc02show . '</span></td>
  </tr>
  <tr>
    <td width="70%" height="23" align="left" valign="middle"><span class="style20">' . $invitm03 . '</span></td>
    <td width="1%">&nbsp;</td>
    <td align="right" valign="middle"><span class="style20">' . $prc03show . '</span></td>
  </tr>
  <tr>
    <td width="70%" height="23" align="left" valign="middle"><span class="style20">' . $invitm04 . '</span></td>
    <td width="1%">&nbsp;</td>
    <td align="right" valign="middle"><span class="style20">' . $prc04show . '</span></td>
  </tr>
  <tr>
    <td width="70%" height="23" align="left" valign="middle"><span class="style20">' . $invitm06 . '</span> </td>
    <td width="1%">&nbsp;</td>
    <td align="right" valign="middle"><span class="style20">' . $prc06show . '</span></td>
  </tr>
  <tr>
    <td width="70%" height="23" align="left" valign="middle"><span class="style20">' . $invitm05 . '</span></td>
    <td width="1%">&nbsp;</td>
    <td align="right" valign="middle"><span class="style20">' . $prc05show . '</span></td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="7" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
          </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="200" align="left" valign="middle" class="style16">Total Due</td>
    <td align="right" valign="middle" class="style21">IDR ' . c_curr($ammdue, ",", ".", $min_sign = "mark") . ',-</td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="15" align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
          </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="middle"><div align="justify" class="style5" style="font-family: Arial, Helvetica, sans-serif; font-size: 10.25px; line-height: 1.2;">
        <b>Dengan dilakukannya pembayaran ini, klien mengerti dan memahami bahwa pembayaran yang sudah dilakukan tidak dapat diajukan refund
        dengan alasan apapun. Klien dianggap sudah mengerti dan memahami bahwa segala resiko pada saat pengurusan perizinan yang bukan
        disebabkan oleh kelalaian IZIN.co.id adalah diluar tanggung jawab IZIN.co.id.</b><br>
        <i>By making this payment, client understands that it is not refundable with any reasons. Client is expected to understand that the risks incurred during
        the licensing process which are not caused by IZIN.co.id is beyond IZIN.co.id\'s responsibility.</i>
      </div></td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="28%" align="left" valign="middle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
          </tr>
    </table>    </td>
    <td width="44%" align="center" valign="middle"><span class="style22">METHODS OF PAYMENT</span></td>
    <td width="28%" align="left" valign="middle">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td bgcolor="#666666"><img src="' . convert_img_to_base64($root_dir . '/templates/Default/images/ganjel.png') . '" width="1" height="1"></td>
          </tr>
    </table>    </td>
  </tr>
  <tr>
    <td align="left" valign="middle"><span class="style24">(For remitting in IDR)</span></td>
    <td align="center" valign="middle"><span class="style23">PAYMENT BY WAY OF TRANSFER / BANK IN</span></td>
    <td align="left" valign="middle">&nbsp;</td>
  </tr>
</table>';
if (XND_PYMT && $judul_invoice == 'Proforma Invoice') {
  $invhtml .= '<table>
  <tr><td>&nbsp;</td></tr>
  <tr><td><span class="style5" style="font-size: 13px;">Anda dapat melakukan pembayaran secara daring melalui tautan berikut:</span></td></tr>
  <tr><td><span class="style5" style="font-size: 13px;"><a href="' . PAYURL . '?id=' . $id_inv . '">' . PAYURL . '?id=' . $id_inv . '</a></span></td></tr>
  </table>';
} else {
  $invhtml .= ' <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3" align="left" valign="middle">&nbsp;</td>
    </tr>
    <tr>
      <td width="100" align="left" valign="middle" class="style24">Bank</td>
      <td width="20" align="center" valign="middle" class="style24">:</td>
      <td align="left" valign="middle" class="style24">' . $BAD_NmBank . '</td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="style24">Account Name</td>
      <td align="center" valign="middle" class="style24">:</td>
      <td align="left" valign="middle" class="style24">' . $BAD_NmPT . '</td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="style24">Account Number</td>
      <td align="center" valign="middle" class="style24">:</td>
      <td align="left" valign="middle" class="style24">' . $_vaNumber . '</td>
    </tr>
    <tr>
      <td align="left" valign="middle" class="style24">Swift Code</td>
      <td align="center" valign="middle" class="style24">:</td>
      <td align="left" valign="middle" class="style24">' . $BAD_SwfCode . '</td>
    </tr>
  </table>';
}
$invhtml .= '</body>
</html>
';
