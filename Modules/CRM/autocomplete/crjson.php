<?php
/*
Function Create json file
This function for autocomplete purposes.
This will create json file for client
*/
function CrJsonFile($jsonfile,$dbcon,$tblp,$list_month,$list_month_short){
	$jsonArr = "var datasp = [";
	$JsonPrep = "select id, kode, nama, tgl_lahir, email, no_wa, locale from ".$tblp."apps_client order by nama asc";
	$rJsonPrep = $dbcon->getQuery($JsonPrep);
	while( $jp = $dbcon->getAssoc($rJsonPrep)){
		$j++;
		$jp['nama'] = preg_replace("/\,/","",$jp['nama']);
		$jpTglLahir = c_date($jp['tgl_lahir'],"shortdate","long",$list_month,$list_month_short);
		$labels = $jp['nama']." - ".$jp['email']." - ".$jp['no_wa'];

		if ( $j == 1 ){
			$jsonArr .= "{ value: '".$jp['id']."', label: '".$labels."', tgllhr: '".$jpTglLahir."', nama: '".$jp['nama']."', sales_name: '".$jp['sales_name']."', wa: '".$jp['no_wa']."', locale: '".$jp['locale']."' }";
		}else{
			$jsonArr .= ",{ value: '".$jp['id']."', label: '".$labels."', tgllhr: '".$jpTglLahir."', nama: '".$jp['nama']."', sales_name: '".$jp['sales_name']."', wa: '".$jp['no_wa']."', locale: '".$jp['locale']."' }";
		}
	}
	$jsonArr .= "]";

	$handle = fopen($jsonfile, 'w');
	fwrite($handle, $jsonArr);
	fclose($handle);
}


/* ---------------------------------------------------- */
?>