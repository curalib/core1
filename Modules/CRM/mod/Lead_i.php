<?php
if ($task == $lang_2059 . "_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["LeadDets"]);
}

if ($task == $rcmdet['nama'] . "_" . $_SESSION["butts_Add"]) {
    $_SESSION["AddItm"] = "GasPol";
    unset($_SESSION["ClietType"]);
    unset($_SESSION["LeadDets"]);
}

if ($task == $rcmdet['nama'] . "_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["AddItm"]);
    unset($_SESSION["ClietType"]);
    unset($_SESSION["LeadChStat"]);
    unset($_SESSION["LeadDets"]);
}

if ($task == "LeadList_" . $_SESSION["butts_Src"]) {
    $SrcLead = Purefy($_POST['SrcLead']);
    $SalesFilter = Purefy($_POST['SalesFilter']);
    $filter_date = Purefy($_POST['filter_date']); #Output: 27/03/2019 to 31/03/2019 / 31/03/2019
    //$filter_date = preg_replace("/\//", "-", $filter_date); #Output: 27-03-2019 to 31-03-2019 / 31-03-2019
    $ProdFilter = Purefy($_POST['ProdFilter']);
    $LeadStatusFilter = Purefy($_POST['LeadStatusFilter']);
    $InvStatusFilter = Purefy($_POST['InvStatusFilter']);

    if (!empty($SrcLead)) {
        $_SESSION["LeadSearch"] = $SrcLead;
    } else {
        unset($_SESSION["LeadSearch"]);
    }
    if (!empty($SalesFilter)) {
        $_SESSION["LeadSearchSales"] = $SalesFilter;
    } else {
        unset($_SESSION["LeadSearchSales"]);
    }
    if (!empty($filter_date)) {
        $_SESSION["LeadSearchDateRange"] = $filter_date;
    } else {
        unset($_SESSION["LeadSearchDateRange"]);
    }
    if (!empty($LeadStatusFilter)) {
        $_SESSION["LeadSearchStatus"] = $LeadStatusFilter;
    } else {
        unset($_SESSION["LeadSearchStatus"]);
    }
    if (!empty($ProdFilter)) {
        $_SESSION["LeadProdSales"] = $ProdFilter;
    } else {
        unset($_SESSION["LeadProdSales"]);
    }
    if (!empty($InvStatusFilter)) {
        $_SESSION["InvSearchStatus"] = $InvStatusFilter;
    } else {
        unset($_SESSION["InvSearchStatus"]);
    }
}

if ($task == "LeadList_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["LeadSearch"]);
    unset($_SESSION["LeadSearchSales"]);
    unset($_SESSION["LeadSearchDateRange"]);
    unset($_SESSION["LeadProdSales"]);
    unset($_SESSION["LeadSearchStatus"]);
    unset($_SESSION["InvSearchStatus"]);
}

if ($task == "Cl_" . $_SESSION["butts_Add"]) {
    $_SESSION["ClietType"] = "AddNew";
}

if ($task == "Cl_" . $_SESSION["butts_Enter"]) {
    $SrcSerId = Purefy($_POST['SrcSerId']);
    if (!empty($SrcSerId)) {
        $_SESSION["ClietType"] = $SrcSerId;
    } else {
        //$AlertMess = $lang_2016;
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2016;
    }
}

if ($task == "Lead_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["ClietType"]);
}

if ($task == "ResetAddNewLead_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["ClietType"]);
}

if ($task == "Lead_" . $_SESSION["butts_Save"]) {
    $Sales = Purefy($_POST['Sales']);
    $Source = Purefy($_POST['Source']);
    $ClID = Purefy($_POST['ClID']);
    $ClNm = Purefy($_POST['ClNm']);
    $ClHbd = convertDate(Purefy($_POST['ClHbd']));
    $ClWa = Purefy($_POST['ClWa']);
    $ClMail = Purefy($_POST['ClMail']);
    $PackIn = Purefy($_POST['PackIn']);
    // print_r($_POST);
    // exit;
    // include "accurate/Customer.php";
    // $customer = new Customer();
    // $client = new Client();
    // $client->clientNo = $ClID;
    // $client->clientName = $_POST['Nama'];
    // $client->clientEmail = $ClMail;
    // $client->clientTelp = $ClWa;
    // $customer->Save($client);

    $PrefLoc = Purefy($_POST['PrefLoc']);
    $PilMans = Purefy($_POST['PilMans']);
    #Disabled Company
    $PilMans = "Disabled";
    if (($_SESSION["ClietType"] == "AddNew" and (empty($ClID) || empty($ClNm) || empty($ClMail) || empty($Sales) || empty($PackIn) || empty($Source) || empty($PilMans))) or
        ($_SESSION["ClietType"] != "AddNew" and (empty($Sales) || empty($PackIn) || empty($Source) || empty($PilMans)) || empty($ClMail))
    ) {

        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2028 . "|" . $lang_2027;

        /*
		if ( empty($ClNm) ){ $ClNm_St = "background-color:#ffe8b1;"; }
		if ( empty($ClMail) ){ $ClMail_St = "background-color:#ffe8b1;"; }
		if ( empty($Sales) ){ $Sales_St = "background-color:#ffe8b1;"; }
		if ( empty($PackIn) ){ $PackIn_St = "background-color:#ffe8b1;"; }
		if ( empty($Source) ){ $Source_St = "background-color:#ffe8b1;"; }
		if ( empty($PilMans) ){ $PilMans_St = "background-color:#ffe8b1;"; }
		*/
    } else {
        $pass = true;
        # Do Some Check  ----------------- #

        if ($_SESSION["ClietType"] == "AddNew") {
            $ClID_Chr = substr($ClID, 0, 3);
            $ClID_Num = substr($ClID, 3, 3) * 1;
            $cKCid = "Select count(*) as jum from " . $tblp . "apps_clientid where clcode = '" . $ClID_Chr . "' and clnum = " . $ClID_Num;
            $rcKCid = $dbs->getArr($cKCid);
            if ($rcKCid['jum'] > 0) {
                $AlertMess .= "Client ID already taken! |";
                $pass = false;
            }
        }

        $cSales = "Select nama_asli from " . $tblp . "sys_lgndetail where id = '" . $Sales . "' and (priv = 4 or priv = 2)";
        $rcSales = $dbs->getArr($cSales);
        if (empty($rcSales['nama_asli'])) {
            $AlertMess .= "Sales not found! |";
            $pass = false;
        }

        $cNPack = "Select nama from " . $tblp . "apps_package where kode = '" . $PackIn . "'";
        $rcNPack = $dbs->getArr($cNPack);
        if (empty($rcNPack['nama'])) {
            $AlertMess .= "Package not found! |";
            $pass = false;
        } else {
            if ($PackIn == "CUSTOM") {
                #Listing Work
                $PackWorkArr = array();
                $cDw = "Select id from " . $tblp . "apps_works";
                $rcDw = $dbs->getQuery($cDw);
                while ($dw = $dbs->getAssoc($rcDw)) {
                    if (Purefy($_POST[$dw['id']]) == 1) {
                        array_push($PackWorkArr, $dw['id']);
                    }
                }
            } else {
                $PckId = "Select id, works from " . $tblp . "apps_package where kode = '" . $PackIn . "'";
                $rPckId = $dbs->getArr($PckId);
                if (!empty($rPckId['id'])) {
                    $PackWorkArr = array();
                    $cPwi = "select mstitem_id from " . $tblp . "apps_pkgitm where package_id = '" . $rPckId['id'] . "'";
                    $rcPwi = $dbs->getQuery($cPwi);
                    while ($pwi = $dbs->getAssoc($rcPwi)) {
                        array_push($PackWorkArr, $pwi['mstitem_id']);
                    }
                }
            }

            if (count($PackWorkArr) < 1) {
                $AlertMess .= "Package work not found!";
                $pass = false;
            }
        }

        $nmSrc = SRC_ITEM[$Source];
        if (empty($nmSrc)) {
            $AlertMess .= "Source not valid! |";
            $pass = false;
        }

        if (!is_email($ClMail)) {
            $AlertMess .= "Invalid email format! |";
            $pass = false;
        }

        # EOF Some Check ----------------- #
        if ($pass === true) {

            # Secondary Processing
            $Id_Lead = md5(uniqids(microtime()));

            //if ( !is_email($ClMail) ){
            //	$AlertMess .= "Client email not valid! |";
            //	$ClMail = '';
            //}

            $PackWork = '';
            foreach ($PackWorkArr as $pwa) {
                $cWiDet = "select paralel, nama, wkt, cntdown, kode, urutan from " . $tblp . "apps_works where id = '" . $pwa . "'";
                $rcWiDet = $dbs->getArr($cWiDet);

                $PackWorkId = md5(uniqids(microtime()));
                $svlwi = "insert into " . $tblp . "apps_packagework set id = '" . $PackWorkId . "', id_lead = '" . $Id_Lead . "', id_work = '" . $pwa . "', work_name = '" . $rcWiDet['nama'] . "', paralel = '" . $rcWiDet['paralel'] . "', cntdown = '" . $rcWiDet['cntdown'] . "', wkt = '" . $rcWiDet['wkt'] . "', kode = '" . $rcWiDet['kode'] . "', urutan = '" . $rcWiDet['urutan'] . "'";
                $dbs->getQuery($svlwi);
                $PackWork .= $PackWorkId . ":";
            }

            $cPrefLoc = "select lokasi from " . $tblp . "master_volocation where publish = 'Y' and id = '" . $PrefLoc . "'";
            $rcPrefLoc = $dbs->getArr($cPrefLoc);

            $cCityNm = "select nm_kabkota from " . $tblp . "master_kabkota where id_kabkota = '" . $KabKota . "'";
            $rcCityNm = $dbs->getArr($cCityNm);

            $cProvNm = "select nm_provinsi_lengkap from " . $tblp . "master_provinsi where id_prov = '" . $Prov . "'";
            $rcProvNm = $dbs->getArr($cProvNm);

            $svlead = "insert into " . $tblp . "apps_lead set
						id_lead = ?,
						sales_id = ?,
						sales_name = ?,
						KodeKantor = ?,
						client_id = ?,
						client_name = ?,
						client_email = ?,
						client_tgllhr = ?,
						client_wa = ?,
						prefloc_id = ?,
						prefloc_name = ?,
						comp_industry1 = ?,
						comp_industry2 = ?,
						comp_industry3 = ?,
						comp_industry4 = ?,
						comp_industry5 = ?,
						comp_tdp_industry = ?,
						comp_industry1_name = ?,
						comp_industry2_name = ?,
						comp_industry3_name = ?,
						comp_industry4_name = ?,
						comp_industry5_name = ?,
						comp_tdp_industry_name = ?,
						comp_nm1 = ?,
						comp_nm2 = ?,
						comp_nm3 = ?,
						company_name = ?,
						comp_func = ?,
						comp_address = ?,
						comp_city = ?,
						comp_city_id = ?,
						comp_province = ?,
						comp_province_id = ?,
						comp_email = ?,
						package_id = ?,
						package_name = ?,
						package_work = ?,
						tgl_input = now(),
						source_id = ?,
						source = ?,
						catatan = ?,
						compsize_id = ?,
						compsize_nm = ?,
						tgl_update = now(),
						last_update = now(),
                        last_update_by = ?,
                        last_update_by_id = ?,
						status = ?,
						status_nm = ?
						";
            $bind = array(
                "s|" . $Id_Lead,
                "s|" . $Sales,
                "s|" . $rcSales['nama_asli'],
                "s|" . $PilMans,
                "s|" . $ClID,
                "s|" . $ClNm,
                "s|" . $ClMail,
                "s|" . $ClHbd,
                "s|" . $ClWa,
                "s|" . $PrefLoc,
                "s|" . $rcPrefLoc['lokasi'],
                "s|" . $Indus1,
                "s|" . $Indus2,
                "s|" . $Indus3,
                "s|" . $Indus4,
                "s|" . $Indus5,
                "s|" . $tdp,
                "s|" . INDUSTRY[$Indus1],
                "s|" . INDUSTRY[$Indus2],
                "s|" . INDUSTRY[$Indus3],
                "s|" . INDUSTRY[$Indus4],
                "s|" . INDUSTRY[$Indus5],
                "s|" . INDUSTRY[$tdp],
                "s|" . $CompNm1,
                "s|" . $CompNm2,
                "s|" . $CompNm3,
                "s|" . $CompNm,
                "s|" . $FgsUtm,
                "s|" . $CompAddr,
                "s|" . $rcCityNm['nm_kabkota'],
                "s|" . $KabKota,
                "s|" . $rcProvNm['nm_provinsi_lengkap'],
                "s|" . $Prov,
                "s|" . $CompMail,
                "s|" . $PackIn,
                "s|" . $rcNPack['nama'],
                "s|" . $PackWork,
                "s|" . $Source,
                "s|" . $nmSrc,
                "s|" . $Cttn,
                "s|" . $csize,
                "s|" . JENIS_PER[$csize],
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|AC",
                "s|" . LEAD_STATUS['AC']
            );
            $dbs->getQuery($svlead, "Exec", $bind);

            # Save client crucial data
            if ($_SESSION["ClietType"] != "AddNew") {
                $cCurrClient = "Select id, nama, kode, tgl_lahir, email from " . $tblp . "apps_client where kode = '" . $ClID . "'";
                $rcCurrClient = $dbs->getArr($cCurrClient);
                $ClID = $rcCurrClient['kode'];
                $ClNm = $rcCurrClient['nama'];
                $ClHbd = $rcCurrClient['tgl_lahir'];
            }

            $NewClient = "update " . $tblp . "apps_lead set client_id = ?, client_name = ?, client_tgllhr = ?, client_email = ? where id_lead = ?";
            $bind = array("s|" . $ClID, "s|" . $ClNm, "s|" . $ClHbd, "s|" . $ClMail, "s|" . $Id_Lead);
            $dbs->getQuery($NewClient, "Exec", $bind);

            if ($_SESSION["ClietType"] == "AddNew") {
                # Save clientID to DB
                $sv = "insert into " . $tblp . "apps_clientid set id = '" . md5(uniqids(microtime())) . "', clcode = '" . $ClID_Chr . "', clnum = '" . $ClID_Num . "'";
                $dbs->getQuery($sv);
            }

            # Copy to table client
            if (empty($rcCurrClient['id'])) {
                # Create sys_login
                # Ini nanti aja ngisiya pas edit client.
                # Jadi defaultnya client ga punya single login.

                //$pwdhash = password_hash($ClPass, PASSWORD_BCRYPT, $passhashoptions);
                //$idSysLogin = md5(uniqids(microtime()));
                //$cSysLogin = "insert into ".$tblp."sys_login set
                //			  id = ?, userid = ?, pwd = ?";
                //$bind = array(
                //	"s|".$idSysLogin,
                //	"s|".md5($ClID),
                //	"s|".$pwdhash
                //);
                //$dbs->getQuery($cSysLogin,"Exec",$bind);

                $CopyClient = "insert into " . $tblp . "apps_client set id = ?, kode = ?, nama = ?, email = ?, tgl_lahir = ?, no_wa = ?, id_login = ?";
                $binds = array(
                    "s|" . md5(uniqids(microtime())),
                    "s|" . $ClID,
                    "s|" . $ClNm,
                    "s|" . $ClMail,
                    "s|" . $ClHbd,
                    "s|" . $ClWa,
                    "s|" . $idSysLogin
                );
                $dbs->getQuery($CopyClient, "Exec", $binds);
            } else {
                $CopyClient = "update " . $tblp . "apps_client set email = ?, no_wa = ?, tgl_lahir = ? where id = ?";
                $binds = array(
                    "s|" . $ClMail,
                    "s|" . $ClWa,
                    "s|" . $ClHbd,
                    "s|" . $rcCurrClient['id']
                );
                $dbs->getQuery($CopyClient, "Exec", $binds);
            }

            # Create Json File for autocomplete
            require_once("Modules/" . $cmodule . "/autocomplete/crjson.php");
            CrJsonFile("Modules/" . $cmodule . "/autocomplete/JS_File.json", $dbs, $tblp, $list_month, $list_month_short);

            $AlertMess = $lang_2029;
            $AlertMess = 'alert-success#done_all#SUCCESS!#' . $lang_2029;

            unset($_SESSION["AddItm"]);
            unset($_SESSION["ClietType"]);
        } else {
            //echo $AlertMess = $lang_2028." | ".$AlertMess;
            $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2028 . " | " . $AlertMess;
        }
    }
}

if ($task == "LeadDet" . $_SESSION["butts_Enter"]) {
    $Ldid = Purefy($_POST['Ldid']);
    if (!empty($Ldid)) {
        $_SESSION["LeadDets"] = $Ldid;
    }
}

if ($task == "UpStat_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["LeadDets"]);
    unset($_SESSION["LeadChStat"]);
}

if ($task == "UpStat_" . $_SESSION["butts_Update"]) {
    $TabStatus = "active";
    $TabInvoice = "";
    $TabSending = "";
    $TabBukti = "";

    # Check Invoice Status
    $chInvStat = "Select tipe, sending, paid from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rcis = $dbs->getArr($chInvStat);

    $LeadStatus = Purefy($_POST['LeadStatus']);
    $alasan = Purefy($_POST['alasan']);

    #Mapping Karena adanya status SENT dan PAID di invoice
    switch ($rcis['tipe']) {
        case "SENT":
            $LeadStatus = '';
            $Reason = "Update status denied! Invoce has been sent and waiting for payment!";
            break;

        case "PAID":
            if ($LeadStatus != "CL") {
                $LeadStatus = '';
                $Reason = "Update status denied! Client has paid the invoice. Please close this lead.";
            }
            break;

        case "PREVIEW":
            if ($LeadStatus == "CL") {
                $LeadStatus = '';
                $Reason = "Update status denied! You cannot close unfinished lead. Please select Lost status if you want to end this lead.";
            }
            break;

        case "BAD":
            $LeadStatus = '';
            $Reason = "Update status denied! Invoice marked as Bad Debt. Please regenerate invoice first.";
            break;

        case '':
            if ($LeadStatus == "CL") {
                $LeadStatus = '';
                $Reason = "Update status denied! You cannot close unpaid lead. Please select Lost status if you want to end this lead.";
            }
            break;
    }

    if (!empty($LeadStatus)) {
        $cOldStat = "select status_nm, status from " . $tblp . "apps_lead_status where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl_update desc limit 1";
        $rcOldStat = $dbs->getArr($cOldStat);
        if (empty($rcOldStat['status'])) {
            $rcOldStat['status'] = "AC";
            $rcOldStat['status_nm'] = "About to Contact";
        }

        $in = "insert into " . $tblp . "apps_lead_status set
			   id = ?, id_lead = ?, tgl_update = now(), old_status = ?, old_status_nm = ?, status = ?, status_nm = ?, update_by_id = ?, update_by_name = ?, alasan = ?";
        $bind = array(
            "s|" . md5(uniqids(microtime())),
            "s|" . $_SESSION["LeadChStat"],
            "s|" . $rcOldStat['status'],
            "s|" . $rcOldStat['status_nm'],
            "s|" . $LeadStatus,
            "s|" . LEAD_STATUS[$LeadStatus],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["lgnNama"],
            "s|" . $alasan
        );
        $dbs->getQuery($in, "Exec", $bind);

        $up = "update " . $tblp . "apps_lead set status = ?, status_nm = ?, status_reason = ?, tgl_update = now(), last_update = now(), last_update_by = ?, last_update_by_id = ? where id_lead = ?";
        $bind = array(
            "s|" . $LeadStatus,
            "s|" . LEAD_STATUS[$LeadStatus],
            "s|" . $alasan,
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["LeadChStat"]
        );
        $dbs->getQuery($up, "Exec", $bind);
        $AlertMess = 'alert-success#done_all#SUCCESS!#' . $lang_2036;
    } else {
        $AlertMess = 'alert-warning#warning#WARNING!#' . $Reason;
    }
}

if ($task == "LeadChStat" . $_SESSION["butts_Enter"]) {
    $Ldid = Purefy($_POST['Ldid']);
    if (!empty($Ldid)) {
        $_SESSION["LeadChStat"] = $Ldid;
    }
}

# Update Client Detail
if ($task == "LeadClient_" . $_SESSION["butts_Update"]) {
    $TabClient = "active";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "";
    $TabUpfile = "";

    $ClID = Purefy($_POST['ClID']);
    $ClHbd = Purefy($_POST['ClHbd']);
    $ClWa = Purefy($_POST['ClWa']);
    $ClMail = Purefy($_POST['ClMail']);
    $ClLocale = Purefy($_POST['ClLocale']);
    if (empty($ClLocale)) {
        $ClLocale = 'id'; // default
    }

    #if ( !is_email($ClMail) || empty($ClHbd) || empty($ClID)  ){
    if (!is_email($ClMail) || empty($ClID) || empty($ClHbd) || empty($ClWa)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2045 . "|" . $lang_2027;
    } else {
        $ClHbd = convertDate($ClHbd);

        $NewClient = "update " . $tblp . "apps_lead set client_email = ?, client_wa = ?, client_tgllhr = ?, tgl_update = now(), last_update = now(), last_update_by = ?, last_update_by_id = ?  where id_lead = ?";
        $bind = array("s|" . $ClMail, "s|" . $ClWa, "s|" . $ClHbd, "s|" . $_SESSION["lgnNama"], "s|" . $_SESSION["tblID"], "s|" . $_SESSION["LeadDets"]);
        $dbs->getQuery($NewClient, "Exec", $bind);

        # Update Client Data
        $cCurrClient = "Select id from " . $tblp . "apps_client where kode = '" . $ClID . "'";
        $rcCurrClient = $dbs->getArr($cCurrClient);

        $CopyClient = "update " . $tblp . "apps_client set email = ?, no_wa = ?, tgl_lahir = ?, locale = ? where id = ?";
        $binds = array(
            "s|" . $ClMail,
            "s|" . $ClWa,
            "s|" . $ClHbd,
            "s|" . $ClLocale,
            "s|" . $rcCurrClient['id']
        );
        $dbs->getQuery($CopyClient, "Exec", $binds);

        # Create Json File for autocomplete
        require_once("Modules/" . $cmodule . "/autocomplete/crjson.php");
        CrJsonFile("Modules/" . $cmodule . "/autocomplete/JS_File.json", $dbs, $tblp, $list_month, $list_month_short);

        $AlertMess = 'alert-success#done_all#SUCCESS!#Client Saved! ' . $lang_2044;
    }
}

if ($task == "LeadSales_" . $_SESSION["butts_Update"]) {
    $TabClient = "active";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "";
    $TabUpfile = "";

    $Sales = Purefy($_POST['Sales']);
    $Source = Purefy($_POST['Source']);
    $PackIn = Purefy($_POST['PackIn']);
    $PrefLoc = Purefy($_POST['PrefLoc']);
    $PilMans = Purefy($_POST['PilMans']);
    $PilMans = "Disabled";

    if (empty($Sales) || empty($PackIn) || empty($Source) || empty($PilMans)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2045 . "|" . $lang_2027;
    } else {
        $pass = true;

        $cSales = "Select nama_asli from " . $tblp . "sys_lgndetail where id = '" . $Sales . "' "; // get full name
        $rcSales = $dbs->getArr($cSales);
        if (empty($rcSales['nama_asli'])) {
            $AlertMess .= "Sales not found! |";
            $pass = false;
        }

        $cNPack = "Select nama from " . $tblp . "apps_package where kode = '" . $PackIn . "'";
        $rcNPack = $dbs->getArr($cNPack);
        if (empty($rcNPack['nama'])) {
            $AlertMess .= "Package not found! |";
            $pass = false;
        } else {
            if ($PackIn == "CUSTOM") {
                #Listing Work
                $PackWorkArr = array();
                $cDw = "Select id from " . $tblp . "apps_works";
                $rcDw = $dbs->getQuery($cDw);
                while ($dw = $dbs->getAssoc($rcDw)) {
                    if (Purefy($_POST[$dw['id']]) == 1) {
                        array_push($PackWorkArr, $dw['id']);
                    }
                }
            } else {
                $PckId = "Select id, works from " . $tblp . "apps_package where kode = '" . $PackIn . "'";
                $rPckId = $dbs->getArr($PckId);
                if (!empty($rPckId['id'])) {
                    $PackWorkArr = array();
                    $cPwi = "select mstitem_id from " . $tblp . "apps_pkgitm where package_id = '" . $rPckId['id'] . "'";
                    $rcPwi = $dbs->getQuery($cPwi);
                    while ($pwi = $dbs->getAssoc($rcPwi)) {
                        array_push($PackWorkArr, $pwi['mstitem_id']);
                    }
                }
            }

            if (count($PackWorkArr) < 1) {
                $AlertMess .= "Package work not found!";
                $pass = false;
            }
        }

        $nmSrc = SRC_ITEM[$Source];
        if (empty($nmSrc)) {
            $AlertMess .= "Source not valid! !";
            $pass = false;
        }

        if ($pass === true) {

            # Save Package
            $delCurrPack = "delete from " . $tblp . "apps_packagework where id_lead = '" . $_SESSION["LeadDets"] . "'";
            $dbs->getQuery($delCurrPack);

            $PackWork = '';
            foreach ($PackWorkArr as $pwa) {
                $cWiDet = "select paralel, nama, wkt, cntdown, kode, urutan from " . $tblp . "apps_works where id = '" . $pwa . "'";
                $rcWiDet = $dbs->getArr($cWiDet);

                $PackWorkId = md5(uniqids(microtime()));
                $svlwi = "insert into " . $tblp . "apps_packagework set id = '" . $PackWorkId . "', id_lead = '" . $_SESSION["LeadDets"] . "', id_work = '" . $pwa . "', work_name = '" . $rcWiDet['nama'] . "', paralel = '" . $rcWiDet['paralel'] . "', cntdown = '" . $rcWiDet['cntdown'] . "', wkt = '" . $rcWiDet['wkt'] . "', kode = '" . $rcWiDet['kode'] . "', urutan = '" . $rcWiDet['urutan'] . "'";
                $dbs->getQuery($svlwi);
                $PackWork .= $PackWorkId . ":";
            }

            $cPrefLoc = "select lokasi from " . $tblp . "master_volocation where publish = 'Y' and id = '" . $PrefLoc . "'";
            $rcPrefLoc = $dbs->getArr($cPrefLoc);

            $svlead = "update " . $tblp . "apps_lead set
						sales_id = ?,
						sales_name = ?,
						KodeKantor = ?,
						prefloc_id = ?,
						prefloc_name = ?,
						package_id = ?,
						package_name = ?,
                        package_work = ?,
						last_update = now(),
						source_id = ?,
						source = ?,
						last_update = now(),
						tgl_update = now(),
						last_update_by = ?,
						last_update_by_id = ?
						where id_lead = ?
						";
            $bind = array(
                "s|" . $Sales,
                "s|" . $rcSales['nama_asli'],
                "s|" . $PilMans,
                "s|" . $PrefLoc,
                "s|" . $rcPrefLoc['lokasi'],
                "s|" . $PackIn,
                "s|" . $rcNPack['nama'],
                "s|" . $PackWork,
                "s|" . $Source,
                "s|" . $nmSrc,
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|" . $_SESSION["LeadDets"]
            );
            $dbs->getQuery($svlead, "Exec", $bind);
            // UPDATE LEAD STATUS
            $AlertMess = 'alert-success#done_all#SUCCESS!#Sales Detail Saved! ' . $lang_2044;
        } else {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $AlertMess . "|" . $lang_2045;
        }
    }
}

if ($task == "LeadComDets_" . $_SESSION["butts_Update"]) {
    $TabClient = "";
    $TabSales = "";
    $TabCompany = "active";
    $TabBocBod = "";
    $TabUpfile = "";

    $CompNm1 = Purefy($_POST['CompNm1']);
    $CompNm2 = Purefy($_POST['CompNm2']);
    $CompNm3 = Purefy($_POST['CompNm3']);
    $FgsUtm = nl2br(Purefy($_POST['FgsUtm']));
    $CompAddr = nl2br(Purefy($_POST['CompAddr']));
    $Prov = Purefy($_POST['Prov']);
    $KabKota = Purefy($_POST['KabKota']);
    $CompMail = Purefy($_POST['CompMail']);
    $Indus1 = Purefy($_POST['Indus1']);
    $Indus2 = Purefy($_POST['Indus2']);
    $Indus3 = Purefy($_POST['Indus3']);
    $Indus4 = Purefy($_POST['Indus4']);
    $Indus5 = Purefy($_POST['Indus5']);
    $tdp = Purefy($_POST['tdp']);
    $csize = Purefy($_POST['csize']);
    $Cttn = nl2br(Purefy($_POST['Cttn']));

    $cCityNm = "select nm_kabkota from " . $tblp . "master_kabkota where id_kabkota = '" . $KabKota . "'";
    $rcCityNm = $dbs->getArr($cCityNm);

    $cProvNm = "select nm_provinsi_lengkap from " . $tblp . "master_provinsi where id_prov = '" . $Prov . "'";
    $rcProvNm = $dbs->getArr($cProvNm);

    $arrkbli = array();
    $lstKBLI = "Select kode, judul from " . $tblp . "master_kbli";
    $rlstKBLI = $dbs->getQuery($lstKBLI);
    while ($KBLI = $dbs->getAssoc($rlstKBLI)) {
        $arrkbli[$KBLI['kode']] = $KBLI['judul'];
    }

    $PengFullName = Purefy($_POST['PengFullName']);
    $PengPos = Purefy($_POST['PengPos']);

    $svlead = "update " . $tblp . "apps_lead set
				last_update_by = ?,
				comp_industry1 = ?,
				comp_industry2 = ?,
				comp_industry3 = ?,
				comp_industry4 = ?,
				comp_industry5 = ?,
				comp_tdp_industry = ?,
				comp_industry1_name = ?,
				comp_industry2_name = ?,
				comp_industry3_name = ?,
				comp_industry4_name = ?,
				comp_industry5_name = ?,
				comp_tdp_industry_name = ?,
				comp_nm1 = ?,
				comp_nm2 = ?,
				comp_nm3 = ?,
				comp_func = ?,
				comp_address = ?,
				comp_city = ?,
				comp_city_id = ?,
				comp_province = ?,
				comp_province_id = ?,
				comp_email = ?,
				catatan = ?,
				compsize_id = ?,
				compsize_nm = ?,
				dirut = ?,
				posisi_id = ?,
				posisi = ?,
				last_update = now(),
				tgl_update = now(),
				last_update_by = ?,
				last_update_by_id = ?
				where id_lead = ?
				";
    $bind = array(
        "s|" . $_SESSION["lgnNama"],
        "s|" . $Indus1,
        "s|" . $Indus2,
        "s|" . $Indus3,
        "s|" . $Indus4,
        "s|" . $Indus5,
        "s|" . $tdp,
        "s|" . $arrkbli[$Indus1],
        "s|" . $arrkbli[$Indus2],
        "s|" . $arrkbli[$Indus3],
        "s|" . $arrkbli[$Indus4],
        "s|" . $arrkbli[$Indus5],
        "s|" . $arrkbli[$tdp],
        "s|" . $CompNm1,
        "s|" . $CompNm2,
        "s|" . $CompNm3,
        "s|" . $FgsUtm,
        "s|" . $CompAddr,
        "s|" . $rcCityNm['nm_kabkota'],
        "s|" . $KabKota,
        "s|" . $rcProvNm['nm_provinsi_lengkap'],
        "s|" . $Prov,
        "s|" . $CompMail,
        "s|" . $Cttn,
        "s|" . $csize,
        "s|" . JENIS_PER[$csize],
        "s|" . $PengFullName,
        "s|" . $PengPos,
        "s|" . POS_PENGURUS[$PengPos],
        "s|" . $_SESSION["lgnNama"],
        "s|" . $_SESSION["tblID"],
        "s|" . $_SESSION["LeadDets"]
    );
    $dbs->getQuery($svlead, "Exec", $bind);
    $AlertMess = 'alert-success#done_all#SUCCESS!#Company Detail Saved! ' . $lang_2044;
}

if ($task == "LeadFile_" . $_SESSION["butts_Save"]) {
    $TabClient = "";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "";
    $TabUpfile = "active";

    $FlTitle = Purefy($_POST['FlTitle']);
    $FlDets = Purefy($_POST['FlDets']);
    if (empty($FlTitle)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2046 . "|" . $lang_2027;
    } else {
        $UploadFile = $pm->File("UpFl", FILESIZES, "../../" . UPDIR . "/Lead_Files")->FileUpload()->Result();
        if ($UploadFile[0] == "FL_UPLOAD_SUCCESS" and !empty($UploadFile[1])) {
            $inFile = "insert into " . $tblp . "apps_leadfile set id = ?, id_lead = ?, nmfile = ?, nmfile_asli = ?, file_size = ?, tgl_upload = now(), upload_by = ?, upload_by_id = ?, judul = ?, desk = ?";
            $bind = array(
                "s|" . md5(uniqids(microtime())),
                "s|" . $_SESSION["LeadDets"],
                "s|" . $UploadFile[1],
                "s|" . $UploadFile[3],
                "i|" . $UploadFile[2],
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|" . $FlTitle,
                "s|" . $FlDets
            );
            $dbs->getQuery($inFile, "Exec", $bind);
            $AlertMess = 'alert-success#done_all#SUCCESS!#' . $FileManPesan[$UploadFile[0]];
        } else {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $FileManPesan[$UploadFile[0]];
        }
    }
}

if ($task == "LstFile_" . $_SESSION["butts_Del"]) {
    $TabClient = "";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "";
    $TabUpfile = "active";

    $FlId = Purefy($_POST['FlId']);
    if (!empty($FlId)) {
        $cFd = "Select nmfile from " . $tblp . "apps_leadfile where id = '" . $FlId . "'";
        $rcFd = $dbs->getArr($cFd);
        $ftd = "../../" . UPDIR . "/Lead_Files" . "/" . $rcFd['nmfile'];
        if (is_file($ftd)) {
            unlink($ftd);
        }

        $del = "delete from " . $tblp . "apps_leadfile where id = '" . $FlId . "'";
        $dbs->getQuery($del);
        $AlertMess = 'alert-success#done_all#SUCCESS!#File deleted.';
    }
}

if ($task == "DLLstFile_" . $_SESSION["buttkey"]) {
    $TabClient = "";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "";
    $TabUpfile = "active";

    $FlId = Purefy($_POST['FlId']);
    if (!empty($FlId)) {
        $cFd = "Select nmfile_asli, nmfile from " . $tblp . "apps_leadfile where id = '" . $FlId . "'";
        $rcFd = $dbs->getArr($cFd);
        $filename = "../../" . UPDIR . "/Lead_Files" . "/" . $rcFd['nmfile'];
        if (!empty($filename) || file_exists($filename)) {
            header('Content-Type: application/octet-stream');
            header('Content-Length: ' . filesize($filename));
            header('Content-Disposition: attachment; filename="' . $rcFd['nmfile_asli'] . '"');
            header('Content-Transfer-Encoding: binary');

            $file = @fopen($filename, "rb");
            if ($file) {
                while (!feof($file)) {
                    print(fread($file, 1024 * 8));
                    flush();
                    if (connection_status() != 0) {
                        @fclose($file);
                        die();
                    }
                }
                @fclose($file);
            }
        } else {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#File Not Found!';
        }
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#ID Not Found!';
    }
}

if ($task == "LeadBocBod_" . $_SESSION["butts_Save"]) {
    $TabClient = "";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "active";
    $TabUpfile = "";

    $PengFullName = Purefy($_POST['PengFullName']);
    $PengPos = Purefy($_POST['PengPos']);

    if (empty($PengFullName)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Add New BOC / BOD Failed. ' . $lang_202;
    } else {
        $in = "insert into " . $tblp . "apps_pengurus set
			   id = ?,
			   id_lead = ?,
			   nama = ?,
			   posisi_id = ?,
			   posisi = ?";
        $bind = array(
            "s|" . md5(uniqids(microtime())),
            "s|" . $_SESSION["LeadDets"],
            "s|" . $PengFullName,
            "s|" . $PengPos,
            "s|" . POS_PENGURUS[$PengPos]
        );

        $dbs->getQuery($in, "Exec", $bind);

        $svlead = "update " . $tblp . "apps_lead set
				last_update = now(),
				tgl_update = now(),
				last_update_by = ?,
				last_update_by_id = ?
				where id_lead = ?
				";
        $bind = array(
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["LeadDets"]
        );
        $dbs->getQuery($svlead, "Exec", $bind);
        $AlertMess = 'alert-success#done_all#SUCCESS!#BOC/BOD successfully added. ' . $lang_2044;
    }
}

if ($task == "LstPengurus_" . $_SESSION["butts_Del"]) {
    $TabClient = "";
    $TabSales = "";
    $TabCompany = "";
    $TabBocBod = "active";
    $TabUpfile = "";

    $FlId = Purefy($_POST['FlId']);
    if (!empty($FlId)) {
        $dell = "delete from " . $tblp . "apps_pengurus where id = '" . $FlId . "'";
        $dbs->getQuery($dell);
        $AlertMess = 'alert-success#done_all#SUCCESS!#BOC/BOD successfully deleted.';
    }
}

if ($task == "Excel_" . $_SESSION["butts_Enter"]) {
    require "system/CreateExcel.php";
    $filename = 'Listing Lead';

    $SrcLead = Purefy($_POST['SrcLead']);
    $SalesFilter = Purefy($_POST['SalesFilter']);
    $filter_date = Purefy($_POST['filter_date']);
    $ProdFilter = Purefy($_POST['ProdFilter']);
    $LeadStatusFilter = Purefy($_POST['LeadStatusFilter']);
    $InvStatusFilter = Purefy($_POST['InvStatusFilter']);

    if (!empty($SrcLead)) {
        $_SESSION["LeadSearch"] = $SrcLead;
    } else {
        unset($_SESSION["LeadSearch"]);
    }
    if (!empty($SalesFilter)) {
        $_SESSION["LeadSearchSales"] = $SalesFilter;
    } else {
        unset($_SESSION["LeadSearchSales"]);
    }
    if (!empty($filter_date)) {
        $_SESSION["LeadSearchDateRange"] = $filter_date;
    } else {
        unset($_SESSION["LeadSearchDateRange"]);
    }
    if (!empty($LeadStatusFilter)) {
        $_SESSION["LeadSearchStatus"] = $LeadStatusFilter;
    } else {
        unset($_SESSION["LeadSearchStatus"]);
    }
    if (!empty($ProdFilter)) {
        $_SESSION["LeadProdSales"] = $ProdFilter;
    } else {
        unset($_SESSION["LeadProdSales"]);
    }
    if (!empty($InvStatusFilter)) {
        $_SESSION["InvSearchStatus"] = $InvStatusFilter;
    } else {
        unset($_SESSION["InvSearchStatus"]);
    }

    $spreadsheet->getProperties()
        ->setCreator("CRM")
        ->setLastModifiedBy("CRM")
        ->setTitle("Listing Lead")
        ->setSubject("Listing Lead Generated File")
        ->setDescription(
            "Listing Lead. Generated from CRM Applications."
        )
        ->setKeywords("Listing Lead")
        ->setCategory("Generated File");

    $spreadsheet->setActiveSheetIndex(0);
    $activesheet = $spreadsheet->getActiveSheet();

    $PageSetup = $activesheet->getPageMargins();
    $PageSetup->setTop(1);
    $PageSetup->setRight(0.75);
    $PageSetup->setLeft(0.75);
    $PageSetup->setBottom(1);

    $activesheet->getPageSetup()
        ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT)
        ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
        ->setFitToWidth(0)
        ->setFitToHeight(0);


    $activesheet->getDefaultRowDimension()->setRowHeight(27);
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(8);
    $spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
    $spreadsheet->getDefaultStyle()->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $activesheet->setShowGridlines(false);

    $activesheet->getStyle('A1:P1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $activesheet->getRowDimension('1')->setRowHeight(25);

    $activesheet->getColumnDimension('A')->setWidth(10);
    $activesheet->setCellValue('A1', 'CLIENT ID')->getStyle('A1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('B')->setWidth(25);
    $activesheet->setCellValue('B1', 'CLIENT FULL NAME')->getStyle('B1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('C')->setWidth(40);
    $activesheet->setCellValue('C1', 'EMAIL')->getStyle('C1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('D')->setWidth(18);
    $activesheet->setCellValue('D1', 'WHATSAPP NO')->getStyle('D1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('E')->setWidth(15);
    $activesheet->setCellValue('E1', 'STATUS')->getStyle('E1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('F')->setWidth(15);
    $activesheet->setCellValue('F1', 'LAST UPDATE')->getStyle('F1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('G')->setWidth(18);
    $activesheet->setCellValue('G1', 'PRODUCT')->getStyle('G1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('H')->setWidth(15);
    $activesheet->setCellValue('H1', 'SOURCE')->getStyle('H1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('I')->setWidth(15);
    $activesheet->setCellValue('I1', 'SALES')->getStyle('I1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('J')->setWidth(15);
    $activesheet->setCellValue('J1', 'INV NUMBER')->getStyle('J1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('K')->setWidth(12);
    $activesheet->setCellValue('K1', 'INV STATUS')->getStyle('K1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('L')->setWidth(18);
    $activesheet->setCellValue('L1', 'TOTAL AMOUNT')->getStyle('L1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('M')->setWidth(15);
    $activesheet->setCellValue('M1', 'PAYMENT DATE')->getStyle('M1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('N')->setWidth(20);
    $activesheet->setCellValue('N1', 'BANK ACCOUNT')->getStyle('N1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('O')->setWidth(23);
    $activesheet->setCellValue('O1', 'AGREEMENT NAME')->getStyle('O1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('P')->setWidth(23);
    $activesheet->setCellValue('P1', 'TRACKING NAME')->getStyle('P1')->getFont()->setBold(true);

    $activesheet->getStyle('A1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('B1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('C1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('D1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('E1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('F1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('G1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('H1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('I1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('J1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('K1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('L1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('M1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('N1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('O1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('P1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
    $activesheet->getStyle('A1:P1')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
    $activesheet->getStyle('A1')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
    $activesheet->getStyle('A1:P1')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

    $srcque = "`lead`.`id_lead` != '' ";
    if (isset($_SESSION["LeadSearch"]) && $_SESSION["LeadSearch"] != '') {
        $srcque .= "and `lead`.`client_name` like '%" . $_SESSION["LeadSearch"] . "%' ";
    }

    if (isset($_SESSION["LeadSearchSales"]) && $_SESSION["LeadSearchSales"] != '') {
        $srcque .= "and `lead`.`sales_id` = '" . $_SESSION["LeadSearchSales"] . "' ";
    }

    if (isset($_SESSION["LeadProdSales"]) && $_SESSION["LeadProdSales"] != '') {
        $srcque .= "and `lead`.`package_id` = '" . $_SESSION["LeadProdSales"] . "' ";
    }

    if (isset($_SESSION["LeadSearchStatus"]) && $_SESSION["LeadSearchStatus"] != '') {
        $srcque .= "and `lead`.`status` = '" . $_SESSION["LeadSearchStatus"] . "' ";
    }

    if (isset($_SESSION["LeadSearchDateRange"]) && $_SESSION["LeadSearchDateRange"] != '') {
        list($daterange01, $daterange02) = explode(" to ", $_SESSION["LeadSearchDateRange"]);
        if ($daterange02) {
            $srcque .= "and DATE(`lead`.`tgl_update`) >= '" . convertDate($daterange01, "/") . "' and DATE(`lead`.`tgl_update`) <= '" . convertDate($daterange02, "/") . "' ";
        } else {
            $srcque .= "and DATE(`lead`.`tgl_update`) = '" . convertDate($daterange01, "/") . "' ";
        }
    }

    if (isset($_SESSION["InvSearchStatus"]) && $_SESSION["InvSearchStatus"] != '') {
        $srcque .= "and `inv`.`tipe` = '" . $_SESSION["InvSearchStatus"] . "' ";
    }

    $cLsLead = "SELECT `lead`.`id_lead`, `lead`.`client_id`, `lead`.`client_name`, `lead`.`last_update`, `lead`.`status`, `lead`.`status_nm`, `lead`.`package_name`, `lead`.`sales_id`, `lead`.`sales_name`, `lead`.`source`, `lead`.`client_email`, `lead`.`client_wa`, `inv`.`tipe`, `inv`.`paid`, `inv`.`inv_num`, `inv`.`total_due`, `inv`.`bank_acc`, `inv`.`tglbayar`, `inv`.`verified_date`, ";
    $cLsLead .= "(SELECT client_company_name FROM " . $tblp . "apps_agreement WHERE `discard` = 0 and `verified_by_id` IS NOT NULL and id_lead = `lead`.`id_lead` LIMIT 1) as agreement_name, ";
    $cLsLead .= "(SELECT company_name FROM " . $tblp . "apps_trklist WHERE lead_id = `lead`.`id_lead` LIMIT 1) as tracking_name ";
    $cLsLead .= "FROM " . $tblp . "apps_lead as `lead` LEFT JOIN  " . $tblp . "apps_invdet as `inv` ON `inv`.`id_lead` = `lead`.`id_lead` ";
    $cLsLead .= "WHERE " . $srcque . " ORDER BY `lead`.`tgl_update` DESC";
    $rcLsLead = $dbs->getQuery($cLsLead);
    $r = 1;
    while ($ll = $dbs->getAssoc($rcLsLead)) {
        $r++;
        $activesheet->setCellValue('A' . $r, $ll['client_id']);
        $activesheet->setCellValue('B' . $r, $ll['client_name']);
        $activesheet->setCellValue('C' . $r, $ll['client_email']);
        $activesheet->getCell('D' . $r)->setValueExplicit($ll["client_wa"], \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
        $activesheet->setCellValue('E' . $r, $ll['status_nm']);
        $activesheet->setCellValue('F' . $r, $ll['last_update']);
        $activesheet->setCellValue('G' . $r, $ll['package_name']);
        $activesheet->setCellValue('H' . $r, $ll['source']);
        $activesheet->setCellValue('I' . $r, $ll['sales_name']);
        $activesheet->setCellValue('J' . $r, $ll['inv_num']);
        $activesheet->setCellValue('K' . $r, $ll['tipe']);
        $activesheet->getStyle('L' . $r)->getNumberFormat()->setFormatCode('#,##0');
        $activesheet->setCellValue('L' . $r, $ll['total_due']);
        if ($ll['paid'] === 'Y' && $ll['tipe'] === 'PAID') {
            $activesheet->setCellValue('M' . $r, $ll['tglbayar']);
        }

        $t = BANK_ACC[$ll['bank_acc']];
        $tmp = explode("|", $t);
        $bank = $tmp[1];
        $activesheet->setCellValue('N' . $r, $bank);

        $activesheet->setCellValue('O' . $r, $ll['agreement_name']);
        $activesheet->setCellValue('P' . $r, $ll['tracking_name']);

        $activesheet->getStyle('A' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('A' . $r)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
        $activesheet->getStyle('B' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('C' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('D' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('E' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('F' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('G' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('H' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('I' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('J' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('J' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $activesheet->getStyle('K' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('K' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $activesheet->getStyle('L' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('L' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $activesheet->getStyle('M' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('N' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('O' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('P' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        $activesheet->getStyle('A' . $r . ':P' . $r)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    }

    $activesheet->getStyle('A' . $r . ':P' . $r)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

    /*$styleArray = array(
    	'borders' => array(
        	'outline' => array(
            	'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            	'color' => array('argb' => 'FFCCCCCC'),
        	),
    	),
	);

	$activesheet->getStyle('A0:G13')->applyFromArray($styleArray);
	*/

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
    header('Cache-Control: max-age=0');
    $writer->save('php://output');
}

if ($task == "InPrev_" . $_SESSION["buttkey"]) {
    $TabStatus = "";
    $TabInvoice = "active";
    $TabSending = "";
    $TabBukti = "";
    $GenErr = "";

    # FInd if this lead already has invoice
    $cLeadInv = "Select id, inv_num, tipe from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rcLeadInv = $dbs->getArr($cLeadInv);

    if (empty($rcLeadInv['id'])) {
        #Find Inv Num
        $fstinv = "Select count(*) as jum from " . $tblp . "apps_invnum";
        $rfstinv = $dbs->getArr($fstinv);

        if (empty($rfstinv['jum'])) {
            $invnum = date("y") . date("m") . "-" . sprintf("%03d", INVNUM);
            $svnum = "insert into " . $tblp . "apps_invnum set id = '" . md5(uniqids(microtime())) . "', tgl = now(), num = '" . INVNUM . "', inv_num = '" . $invnum . "'";
            $dbs->getQuery($svnum);
        } else {
            $InvNum = "select num, inv_num from " . $tblp . "apps_invnum where MONTH(tgl) = '" . date("m") . "' and YEAR(tgl) = '" . date("Y") . "' order by num desc limit 1";
            $rInvNum = $dbs->getArr($InvNum);

            if (empty($rInvNum['num'])) {
                $rInvNum['num'] = 1;
            } else {
                $rInvNum['num']++;
            }
            $invnum = date("y") . date("m") . "-" . sprintf("%03d", $rInvNum['num']);

            $svnum = "insert into " . $tblp . "apps_invnum set id = '" . md5(uniqids(microtime())) . "', tgl = now(), num = '" . $rInvNum['num'] . "', inv_num = '" . $invnum . "'";
            $dbs->getQuery($svnum);
        }

        $id_inv = md5(uniqids(microtime()));
    } else {
        if ($rcLeadInv['tipe'] == "SENT") {
            $GenErr = "SENT";
        }
        if ($rcLeadInv['tipe'] == "PAID") {
            $GenErr = "PAID";
        }
        if ($rcLeadInv['tipe'] == "BAD") {
            $GenErr = "BAD";
        }

        $invnum = $rcLeadInv['inv_num'];
        $id_inv = $rcLeadInv['id'];
    }

    if ($GenErr == "SENT") {
        $AlertMess = 'alert-warning#warning#WARNING!#Generating Invoice denied! | Invoice already sent!';
    } elseif ($GenErr == "PAID") {
        $AlertMess = 'alert-warning#warning#WARNING!#Generating Invoice denied! | Invoice already paid!';
    } elseif ($GenErr == "BAD") {
        $AlertMess = 'alert-warning#warning#WARNING!#Generating invoice denied! | Invoice marked as bad debt!';
    } else {
        $IssueDt = convertDate(Purefy($_POST['IssueDt']));
        $DueDt = convertDate(Purefy($_POST['DueDt']));
        $IssueDtShow = c_date($IssueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
        $DueDtShow = c_date($DueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
        $invname = Purefy($_POST['invname']);
        $invpic = Purefy($_POST['invpic']);
        $invmail = Purefy($_POST['invmail']);
        $invphnum = Purefy($_POST['invphnum']);

        $invitm01 = Purefy($_POST['invitm01']);
        $invprc01 = preg_replace("/\D/", "", Purefy($_POST['invprc01']));
        if (!empty($invprc01)) {
            $prc01show = 'IDR ' . c_curr($invprc01, ",", ".", $min_sign = "mark") . ',-';
        } else {
            $invprc01 = 0;
            $prc01show = '';
        }

        $invitm02 = Purefy($_POST['invitm02']);
        $invprc02 = preg_replace("/\D/", "", Purefy($_POST['invprc02']));
        if (!empty($invprc02)) {
            $prc02show = 'IDR ' . c_curr($invprc02, ",", ".", $min_sign = "mark") . ',-';
        } else {
            $invprc02 = 0;
            $prc02show = '';
        }

        $invitm03 = Purefy($_POST['invitm03']);
        $invprc03 = preg_replace("/\D/", "", Purefy($_POST['invprc03']));
        if (!empty($invprc03)) {
            $prc03show = 'IDR ' . c_curr($invprc03, ",", ".", $min_sign = "mark") . ',-';
        } else {
            $invprc03 = 0;
            $prc03show = '';
        }

        $invitm04 = Purefy($_POST['invitm04']);
        $invprc04 = preg_replace("/\D/", "", Purefy($_POST['invprc04']));
        if (!empty($invprc04)) {
            $prc04show = 'IDR ' . c_curr($invprc04, ",", ".", $min_sign = "mark") . ',-';
        } else {
            $invprc04 = 0;
            $prc04show = '';
        }

        $invitm05 = Purefy($_POST['invitm05']);
        $invprc05 = preg_replace("/\D/", "", Purefy($_POST['invprc05']));
        if (!empty($invprc05)) {
            $prc05show = 'IDR ' . c_curr($invprc05, ",", ".", $min_sign = "mark") . ',-';
        } else {
            $invprc05 = 0;
            $prc05show = '';
            $invitm05 = '';
        }

        $invitm06 = Purefy($_POST['invitm06']);
        $invprc06 = preg_replace("/\D/", "", Purefy($_POST['invprc06']));
        if (!empty($invprc06)) {
            $prc06show = 'IDR ' . c_curr($invprc06, ",", ".", $min_sign = "mark") . ',-';
        } else {
            $invprc06 = 0;
            $prc06show = '';
        }

        $ammdue = $invprc01 + $invprc02 + $invprc03 + $invprc04 - $invprc05 + $invprc06;

        $BnkAcc = Purefy($_POST['BnkAcc']);
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmBank = $BnkAccDet[0];
        $BAD_NmPT = $BnkAccDet[1];
        $BAD_NoRek = $BnkAccDet[2];
        $BAD_SwfCode = $BnkAccDet[3];

        $dir = "../../" . UPDIR . "/Invoices";
        if (!is_dir($dir)) {
            mkdir($dir, 0700);
        }
        $file = $dir . "/" . $invnum . ".pdf";

        $InvMark = '';
        if (VABANK[$BnkAcc] === "00000") {
            $va_num = NULL;
        } else {
            $va_num = VABANK[$BnkAcc] . str_replace("-", "", $invnum);
        }

        $vaNumber = $va_num;

        if (empty($rcLeadInv['id'])) {
            $sv = "insert into " . $tblp . "apps_invdet set id = ?, id_lead = ?, tgl_create = now(), create_by = ?, user_id = ?, inv_num = ?, issue_date = ?, due_date = ?, total_due =	 ?, name = ?, pic = ?, email = ?, phone_number = ?, bank_acc = ?, tipe = ?, va_number = ?";
            $bind = array(
                "s|" . $id_inv,
                "s|" . $_SESSION["LeadChStat"],
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|" . $invnum,
                "s|" . $IssueDt,
                "s|" . $DueDt,
                "i|" . $ammdue,
                "s|" . $invname,
                "s|" . $invpic,
                "s|" . $invmail,
                "s|" . $invphnum,
                "s|" . $BnkAcc,
                "s|PREVIEW",
                "s|" . $va_num,
            );
            $dbs->getQuery($sv, "Exec", $bind);

            for ($i = 1; $i <= 6; $i++) {
                $in = "insert into " . $tblp . "apps_invitem set
					   id = '" . md5(uniqids(microtime())) . "', id_inv = '" . $id_inv . "', item = '" . ${"invitm" . sprintf("%02d", $i)} . "', price = '" . ${"invprc" . sprintf("%02d", $i)} . "', 	no_urut = '" . $i . "'";
                $dbs->getQuery($in);
            }
        } else {
            $sv = "update " . $tblp . "apps_invdet set
                    tgl_create = now(),
                    create_by = ?,
                    user_id = ?,
                    issue_date = ?,
                    due_date = ?,
                    total_due = ?,
                    name = ?,
                    pic = ?,
                    email = ?,
                    phone_number = ?,
                    bank_acc = ?,
                    tipe = ?,
                    va_number = ?,
                    tglbayar = ?,
                    verified_by = ?,
                    verified_by_id = ?
                    where id = ?";

            $bind = array(
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|" . $IssueDt,
                "s|" . $DueDt,
                "i|" . $ammdue,
                "s|" . $invname,
                "s|" . $invpic,
                "s|" . $invmail,
                "s|" . $invphnum,
                "s|" . $BnkAcc,
                "s|PREVIEW",
                "s|" . $va_num,
                "s|0000-00-00 00:00:00",
                "s|",
                "s|",
                "s|" . $id_inv
            );
            $dbs->getQuery($sv, "Exec", $bind);

            $delInvItm = "delete from " . $tblp . "apps_invitem where id_inv = '" . $id_inv . "'";
            $dbs->getQuery($delInvItm);

            for ($i = 1; $i <= 6; $i++) {
                $in = "insert into " . $tblp . "apps_invitem set
					   id = '" . md5(uniqids(microtime())) . "', id_inv = '" . $id_inv . "', item = '" . ${"invitm" . sprintf("%02d", $i)} . "', price = '" . ${"invprc" . sprintf("%02d", $i)} . "', 	no_urut = '" . $i . "'";
                $dbs->getQuery($in);
            }
        }

        if (is_file($_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/InvoiceHTML.php")) {
            #Load Invoice HTML
            $root_dir = ROOT_DIR;
            $judul_invoice = "Proforma Invoice";
            require $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/InvoiceHTML.php";

            try {
                require "system/CreatePDF.php";
                $dompdf->loadHtml($invhtml);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $pdfoutput = $dompdf->output();
                if (is_dir($dir)) {
                    file_put_contents($file, $pdfoutput);
                    $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice number ' . $invnum . ' for preview saved!|Please check on Send Invoice Tab';
                } else {
                    $AlertMess = 'alert-danger#remove_circle#ERROR!#Saving directory is missing!';
                }
            } catch (Exception $e) {
                $AlertMess = 'alert-danger#remove_circle#ERROR!#Saving PDF file failed!';
            }
        } else {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#Generate Invoice Failed! Invoce template cannot be found!';
        }
    }
}

if ($task == "InvDel_" . $_SESSION["butts_Del"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "active";
    $TabBukti = "";

    $cDet = "select id, inv_num, tipe, sending from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rcDet = $dbs->getArr($cDet);

    $dir = "../../" . UPDIR . "/Invoices";
    $file = $dir . "/" . $rcDet['inv_num'] . ".pdf";

    if (empty($rcDet['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Delete invoice failed! | Invoice not found!';
    } elseif ($rcDet['tipe'] == "SENT") {
        $AlertMess = 'alert-warning#warning#WARNING!#Delete invoice denied! | Invoice has been sent!';
    } elseif ($rcDet['tipe'] == "PAID") {
        $AlertMess = 'alert-warning#warning#WARNING!#Delete invoice denied! | Invoice has been paid!';
    } elseif ($rcDet['tipe'] == "BAD") {
        $AlertMess = 'alert-warning#warning#WARNING!#Delete invoice denied! | Invoice marked as bad debt!';
    } else {
        if (!unlink($file)) {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#Delete invoice failed! | PDF file cannot be deleted!';
        } else {
            $del = "delete from " . $tblp . "apps_invitem where id_inv = ?";
            $dbs->getQuery($del, "Exec", ["s|" . $rcDet['id']]);

            $delinv = "delete from " . $tblp . "apps_invdet where id_lead = ?";
            $dbs->getQuery($delinv, "Exec", ["s|" . $_SESSION["LeadChStat"]]);

            $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice number ' . $rcDet['inv_num'] . ' Deleted!|Please generate new invoice on Generate Invoice Tab';
        }
    }
}

if ($task == "InvDld_" . $_SESSION["butts_Unlink"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "active";
    $TabBukti = "";

    $cDet = "select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rcDet = $dbs->getArr($cDet);
    if (empty($rcDet['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Activate invoice failed! | Invoice not found!';
    } elseif ($rcDet['tipe'] == "PAID") {
        $AlertMess = 'alert-warning#warning#WARNING!#Activate invoice denied! | Invoice has been paid!';
    } elseif ($rcDet['tipe'] == "SENT") {
        $AlertMess = 'alert-warning#warning#WARNING!#Activate invoice denied! | Invoice already being activated!';
    } elseif ($rcDet['tipe'] == "BAD") {
        $AlertMess = 'alert-warning#warning#WARNING!#Activate invoice denied! | Invoice already being activated!';
    } else {

        $up = "update " . $tblp . "apps_invdet set tipe = 'SENT', sending = 'Y', unduh = 'Y' where id_lead = '" . $_SESSION["LeadChStat"] . "'";
        $dbs->getQuery($up);

        $cdl = "Select client_name, client_email from " . $tblp . "apps_lead where id_lead = '" . $_SESSION["LeadChStat"] . "'";
        $rcdl = $dbs->getArr($cdl);

        $in = "insert into " . $tblp . "apps_invsent set
			   id = '" . md5(uniqids(microtime())) . "',
			   id_inv = '" . $rcDet['id'] . "',
			   id_lead = '" . $_SESSION["LeadChStat"] . "',
			   sent_date = now(),
			   status = '',
			   sent_by = '" . $_SESSION["lgnNama"] . "',
			   sent_user_id = '" . $_SESSION["tblID"] . "',
			   email_to = '" . $rcdl['client_email'] . "',
			   email_to_name = '" . $rcdl['client_name'] . "'";
        $dbs->getQuery($in);


        # Update Status
        $alasan = "
		Invoice Number: " . $rcDet['inv_num'] . "
		Activated Date: " . date("d-m-Y") . "
		PIC: " . $rcDet['pic'] . "
		Email: " . $rcDet['email'] . " ";

        $cOldStat = "select status_nm, status from " . $tblp . "apps_lead_status where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl_update desc limit 1";
        $rcOldStat = $dbs->getArr($cOldStat);
        if (empty($rcOldStat['status'])) {
            $rcOldStat['status'] = "AC";
            $rcOldStat['status_nm'] = "About to Contact";
        }

        $in = "insert into " . $tblp . "apps_lead_status set
			   id = ?, id_lead = ?, tgl_update = now(), old_status = ?, old_status_nm = ?, status = ?, status_nm = ?, update_by_id = ?, update_by_name = ?, alasan = ?";
        $bind = array(
            "s|" . md5(uniqids(microtime())),
            "s|" . $_SESSION["LeadChStat"],
            "s|" . $rcOldStat['status'],
            "s|" . $rcOldStat['status_nm'],
            "s|IS",
            "s|" . LEAD_STATUS['IS'],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["lgnNama"],
            "s|" . $alasan
        );
        $dbs->getQuery($in, "Exec", $bind);

        $up = "update " . $tblp . "apps_lead set status = ?, status_nm = ?, status_reason = ?, tgl_update = now(), last_update = now(), last_update_by = ?, last_update_by_id = ? where id_lead = ?";
        $bind = array(
            "s|IS",
            "s|" . LEAD_STATUS['IS'],
            "s|" . $alasan,
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["LeadChStat"]
        );
        $dbs->getQuery($up, "Exec", $bind);

        $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice Activated!|' . $MailSendStatus;
    }
}

if ($task == "InvSend_" . $_SESSION["butts_Unlink"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "active";
    $TabBukti = "";

    $cDet = "select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rcDet = $dbs->getArr($cDet);
    if (empty($rcDet['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Activate and send invoice failed! | Invoice not found!';
    } elseif ($rcDet['tipe'] == "PAID") {
        $AlertMess = 'alert-warning#warning#WARNING!#Activate and send invoice denied! | Invoice has been paid!';
    } elseif ($rcDet['tipe'] == "SENT") {
        $AlertMess = 'alert-warning#warning#WARNING!#Activate and send invoice denied! | Invoice already sent!';
    } elseif ($rcDet['tipe'] == "BAD") {
        $AlertMess = 'alert-warning#warning#WARNING!#Activate and send invoice denied! | Invoice already sent!';
    } else {

        $dir = "../../" . UPDIR . "/Invoices";
        $file = $dir . "/" . $rcDet['inv_num'] . ".pdf";

        $cPaket = "select item from " . $tblp . "apps_invitem where id_inv = '" . $rcDet['id'] . "' and no_urut = 1";
        $rcPaket = $dbs->getArr($cPaket);

        $invnum = $rcDet['inv_num'];
        $packageName = $rcPaket['item'];
        $invmail = $rcDet['email'];
        $invpic = $rcDet['pic'];

        $BnkAcc = $rcDet['bank_acc'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmPT = $BnkAccDet[1];

        if ($rcDet['sending'] == "Y") {
            /* -----------
            SEND EMAIL HERE
            -------------- */
            require_once "Modules/CRM/file/Mail_Inv_Resend.php";
        } else {
            /* -----------
            SEND EMAIL HERE
            -------------- */
            require_once "Modules/CRM/file/Mail_Inv_Send.php";
        }

        $up = "update " . $tblp . "apps_invdet set tipe = 'SENT', sending = 'Y' where id_lead = '" . $_SESSION["LeadChStat"] . "'";
        $dbs->getQuery($up);

        $cdl = "Select client_name, client_email from " . $tblp . "apps_lead where id_lead = '" . $_SESSION["LeadChStat"] . "'";
        $rcdl = $dbs->getArr($cdl);

        $invSendStatus = ($MailSendStatus == 'Email not sent!') ? 'FAILED' : 'SUCCESS';

        $in = "insert into " . $tblp . "apps_invsent set
			   id = '" . md5(uniqids(microtime())) . "',
			   id_inv = '" . $rcDet['id'] . "',
			   id_lead = '" . $_SESSION["LeadChStat"] . "',
			   sent_date = now(),
			   status = '" . $invSendStatus . "',
			   sent_by = '" . $_SESSION["lgnNama"] . "',
			   sent_user_id = '" . $_SESSION["tblID"] . "',
			   email_to = '" . $rcdl['client_email'] . "',
			   email_to_name = '" . $rcdl['client_name'] . "'";
        $dbs->getQuery($in);

        # Update Status
        $alasan = "
		Invoice Number: " . $invnum . "
		Send Date: " . date("d-m-Y") . "
		PIC: " . $invpic . "
		Email: " . $invmail . " ";

        $cOldStat = "select status_nm, status from " . $tblp . "apps_lead_status where id_lead = '" . $_SESSION["LeadChStat"] . "' order by tgl_update desc limit 1";
        $rcOldStat = $dbs->getArr($cOldStat);
        if (empty($rcOldStat['status'])) {
            $rcOldStat['status'] = "AC";
            $rcOldStat['status_nm'] = "About to Contact";
        }

        $in = "insert into " . $tblp . "apps_lead_status set
			   id = ?, id_lead = ?, tgl_update = now(), old_status = ?, old_status_nm = ?, status = ?, status_nm = ?, update_by_id = ?, update_by_name = ?, alasan = ?";
        $bind = array(
            "s|" . md5(uniqids(microtime())),
            "s|" . $_SESSION["LeadChStat"],
            "s|" . $rcOldStat['status'],
            "s|" . $rcOldStat['status_nm'],
            "s|IS",
            "s|" . LEAD_STATUS['IS'],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["lgnNama"],
            "s|" . $alasan
        );
        $dbs->getQuery($in, "Exec", $bind);

        $up = "update " . $tblp . "apps_lead set status = ?, status_nm = ?, status_reason = ?, tgl_update = now(), last_update = now(), last_update_by = ?, last_update_by_id = ? where id_lead = ?";
        $bind = array(
            "s|IS",
            "s|" . LEAD_STATUS['IS'],
            "s|" . $alasan,
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["LeadChStat"]
        );
        $dbs->getQuery($up, "Exec", $bind);

        $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice Activated!|' . $MailSendStatus;
    }
}

if ($task == "InvCncl_" . $_SESSION["butts_Reset"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "active";
    $TabBukti = "";

    $dtinv = "SELECT a.*, b.id as company_id, b.xendit_secret FROM `{$tblp}apps_invdet` a LEFT JOIN `{$tblp}master_company` b ON SUBSTRING(a.va_number,1,5) = b.bca_client_id WHERE a.id_lead = ? LIMIT 1 ";
    $rdtinv = $dbs->getArr($dtinv, ["s|" . $_SESSION["LeadChStat"]]);
    if (empty($rdtinv['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Send invoice failed! | Invoice not found!';
    } elseif ($rdtinv['tipe'] == "PAID") {
        $AlertMess = 'alert-warning#warning#WARNING!#Canceling invoice denied! | Invoice has been paid!';
    } elseif ($rdtinv['tipe'] == "PREVIEW") {
        $AlertMess = 'alert-warning#warning#WARNING!#Canceling invoice cannot be done! | Invoice has not been sent yet!';
    } else {

        #Create Cancelled Mark Invoice
        $InvMark = '<img src="' . convert_img_to_base64(ROOT_DIR . '/templates/Default/sysicons/Cancelled_i.png') . '" height="60">';

        $id_inv = $rdtinv['id'];
        $invnum = $rdtinv['inv_num'];
        $IssueDt = $rdtinv['issue_date'];
        $DueDt = $rdtinv['due_date'];
        $IssueDtShow = c_date($IssueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
        $DueDtShow = c_date($DueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
        $invname = $rdtinv['name'];
        $invpic = $rdtinv['pic'];
        $invmail = $rdtinv['email'];
        $invphnum = $rdtinv['phone_number'];

        $citminv = "SELECT item, price, no_urut FROM `{$tblp}apps_invitem` WHERE id_inv = ? ORDER BY no_urut ASC";
        $rcitminv = $dbs->getQuery($citminv, "Exec", ["s|" . $rdtinv['id']]);
        while ($cit = $dbs->getAssoc($rcitminv)) {
            ${"invitm0" . $cit['no_urut']} = $cit['item'];
            ${"invprc0" . $cit['no_urut']} = preg_replace("/\D/", "", $cit['price']);
            if (!empty(${"invprc0" . $cit['no_urut']})) {
                ${"prc0" . $cit['no_urut'] . "show"} = 'IDR ' . c_curr(${"invprc0" . $cit['no_urut']}, ",", ".", $min_sign = "mark") . ',-';
            } else {
                ${"prc0" . $cit['no_urut'] . "show"} = '';
            }
        }

        $ammdue = $rdtinv['total_due'];

        $BnkAcc = $rdtinv['bank_acc'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmBank = $BnkAccDet[0];
        $BAD_NmPT = $BnkAccDet[1];
        $BAD_NoRek = $BnkAccDet[2];
        $BAD_SwfCode = $BnkAccDet[3];

        $dir = "../../" . UPDIR . "/Invoices";
        if (!is_dir($dir)) {
            mkdir($dir, 0700);
        }
        $file = $dir . "/" . $invnum . ".pdf";

        if (is_file($_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/InvoiceHTML.php")) {
            $root_dir = ROOT_DIR;
            $judul_invoice = "Proforma Invoice";
            #Load Invoice HTML
            require $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/InvoiceHTML.php";
            try {
                require "system/CreatePDF.php";
                $dompdf->loadHtml($invhtml);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $pdfoutput = $dompdf->output();
                if (is_dir($dir)) {
                    file_put_contents($file, $pdfoutput);
                    $createpdf = "OK";
                } else {
                    $createpdf = "SHUT";
                }
            } catch (Exception $e) {
                $createpdf = "SHUT";
            }
        } else {
            $createpdf = "SHUT";
        }

        if ($createpdf != "OK") {
            $AlertMess = 'alert-warning#warning#WARNING!#Canceling invoice cannot be done! | Invoice file not found!';
        } else {
            # Revert Back The Status
            $alasan_cancel = Purefy($_POST['alasan_cancel']);
            $preStatus = "SELECT `status` FROM `{$tblp}apps_lead_status` WHERE id_lead = ? ORDER BY tgl_update DESC limit 1,1";
            $rpreStatus = $dbs->getArr($preStatus, ["s|" . $rdtinv['id_lead']]);
            if (empty($rpreStatus['status'])) {
                $rpreStatus['status'] = "AC";
                $rpreStatus['status_nm'] = "About to Contact";
            }

            $cOldStat = "SELECT status_nm, `status` FROM `{$tblp}apps_lead_status` WHERE id_lead = ? ORDER BY tgl_update DESC limit 1";
            $rcOldStat = $dbs->getArr($cOldStat, ["s|" . $rdtinv['id_lead']]);


            $in = "INSERT into `{$tblp}apps_lead_status` SET
                id = ?, id_lead = ?, tgl_update = now(), old_status = ?, old_status_nm = ?, status = ?, status_nm = ?, update_by_id = ?, update_by_name = ?, alasan = ?";
            $bind = array(
                "s|" . md5(uniqids(microtime())),
                "s|" . $rdtinv["id_lead"],
                "s|" . $rcOldStat['status'],
                "s|" . $rcOldStat['status_nm'],
                "s|" . $rpreStatus['status'],
                "s|" . LEAD_STATUS[$rpreStatus['status']],
                "s|" . $_SESSION["tblID"],
                "s|" . $_SESSION["lgnNama"],
                "s|" . $alasan_cancel
            );
            $dbs->getQuery($in, "Exec", $bind);

            $up = "UPDATE `{$tblp}apps_lead` SET
                status = ?, status_nm = ?, status_reason = ?, tgl_update = now(), last_update = now(), last_update_by = ?, last_update_by_id = ? where id_lead = ?";
            $bind = array(
                "s|" . $rpreStatus['status'],
                "s|" . LEAD_STATUS[$rpreStatus['status']],
                "s|" . $alasan_cancel,
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|" . $rdtinv["id_lead"]
            );
            $dbs->getQuery($up, "Exec", $bind);

            $up = "UPDATE `{$tblp}apps_invdet` SET
                tipe = 'PREVIEW', sending = 'Y', tglbayar = '0000-00-00 00:00:00', verified_by = '', verified_by_id = '' where id_lead = '" . $rdtinv["id_lead"] . "'";
            $dbs->getQuery($up);

            /* -----------
            SEND EMAIL HERE
            -------------- */
            if ($rdtinv['unduh'] != "Y") {
                $dir = "../../" . UPDIR . "/Invoices";
                $file = $dir . "/" . $invnum . ".pdf";

                $packageName = ${"invitm01"};

                require_once "Modules/CRM/file/Mail_Inv_Unreleased.php";
            }

            # Di sini bill atas inv_id harus di-cancel semua, kita cancel di local IZIN DB
            $cFind = "SELECT id FROM `{$tblp}xendit_bills` WHERE id_inv = ? AND status = ? ";
            $rcFind = $dbs->getQuery($cFind, "Exec", ["s|" . $rdtinv['id'], "s|PENDING"]);
            $result = $dbs->getAll($rcFind);

            // check bills if exist
            if (($rcFind instanceof PDOStatement && count($result) > 0)) {
                foreach ($result as $val) {
                    require_once 'vendor/autoload.php';

                    try {
                        \Xendit\Xendit::setApiKey($rdtinv['xendit_secret']);
                        $expireInvoice = \Xendit\Invoice::expireInvoice($val["token"]);
                        $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice Canceled!|Existing Xendit bills canceled!|' . $MailSendStatus;
                    } catch (\Xendit\Exceptions\ApiException $e) {
                        $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice Canceled!|' . $e->getMessage();
                    }
                }
            } else {
                $AlertMess = 'alert-success#done_all#SUCCESS!#Invoice Canceled!|' . $MailSendStatus;
            }
        }
    }
}

if ($task == "DtSrc_" . $_SESSION["butts_Src"]) {
    $dtsrc = Purefy($_POST['dtsrc']);
    if (!empty($dtsrc)) {
        $_SESSION["LeadSearch"] = $dtsrc;
    }
}

if ($task == "DtSrc_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["LeadSearch"]);
}

if ($task == "PayFile_" . $_SESSION["butts_Save"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "";
    $TabBukti = "active";

    $UploadFile = $pm->File("UpFl", FILESIZES, "../../" . UPDIR . "/Payment")->FileUpload()->Result();
    if ($UploadFile[0] == "FL_UPLOAD_SUCCESS" and !empty($UploadFile[1])) {

        $cIdIv = "Select id from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
        $rcIdIv = $dbs->getArr($cIdIv);

        $sv = "insert into " . $tblp . "apps_invbayar set id = ?, id_inv = ?, id_lead = ?, nmfile_asli = ?, nmfile = ?, file_size = ?, upload_by = ?, upload_by_id = ?, tgl = now()";
        $dbs->getQuery($sv, "Exec", [
            "s|" . md5(uniqids(microtime())),
            "s|" . $rcIdIv['id'],
            "s|" . $_SESSION["LeadChStat"],
            "s|" . $UploadFile[3],
            "s|" . $UploadFile[1],
            "i|" . $UploadFile[2],
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"]
        ]);

        $AlertMess = 'alert-success#done_all#SUCCESS!# Payment proof uploaded.';
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $FileManPesan[$UploadFile[0]];
    }
}

if ($task == "PayFile_" . $_SESSION["butts_Del"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "";
    $TabBukti = "active";

    $FlId = Purefy($_POST['FlId']);
    if (!empty($FlId)) {
        $cNmf = "Select nmfile from " . $tblp . "apps_invbayar where id = '" . $FlId . "'";
        $rcNmf = $dbs->getArr($cNmf);
        $file = "../../" . UPDIR . "/Payment/" . $rcNmf['nmfile'];
        if (is_file($file)) {
            if (unlink($file)) {
                $dd = "delete from " . $tblp . "apps_invbayar where id = '" . $FlId . "'";
                $dbs->getQuery($dd);
                $AlertMess = 'alert-success#done_all#SUCCESS!#File Deleted!.';
            } else {
                $AlertMess = 'alert-danger#remove_circle#ERROR!#File cannot be deleted';
            }
        } else {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#File Not Found!';
        }
    }
}

if ($task == "FinNotif_" . $_SESSION["butts_Save"]) {
    $TabStatus = "";
    $TabInvoice = "";
    $TabSending = "";
    $TabBukti = "active";

    // dont include sales
    //$salesemail = "Select sales_name, uid from " . $tblp . "apps_lead left join " . $tblp . "sys_lgndetail on (sales_id = id) where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    //$rsalesemail = $dbs->getArr($salesemail);

    $TglByr = convertDate(Purefy($_POST['TglByr']));
    //$finemail = explode("|", purefy($_POST['FinSt']));

    if (!empty($TglByr)) {

        $ccemail = array();
        $getCC = "Select nama_asli, uid from " . $tblp . "sys_lgndetail where priv = 2";
        $rgetCC = $dbs->getQuery($getCC);
        while ($cc = $dbs->getAssoc($rgetCC)) {
            $ccemail[$cc['uid']] = $cc['nama_asli'];
        }

        $cIdIv = "Select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
        $rcIdIv = $dbs->getArr($cIdIv);

        $cPaket = "select item from " . $tblp . "apps_invitem where id_inv = '" . $rcIdIv['id'] . "' and no_urut = 1";
        $rcPaket = $dbs->getArr($cPaket);

        $invnum = $rcIdIv['inv_num'];
        $packageName = $rcPaket['item'];
        $invmail = $rcIdIv['email'];
        $invpic = $rcIdIv['pic'];

        /* -----------
		SEND EMAIL HERE
		-------------- */
        $totalDue = $rcIdIv['total_due'];

        $BnkAcc = $rcIdIv['bank_acc'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmBank = $BnkAccDet[0];
        $BAD_NmPT = $BnkAccDet[1];

        require_once "Modules/CRM/file/Mail_Inv_Verification.php";

        $UpInvDet = "update " . $tblp . "apps_invdet set tglbayar = '" . $TglByr . "' where id = '" . $rcIdIv['id'] . "'";
        $dbs->getQuery($UpInvDet);

        $svNotif = "insert into " . $tblp . "apps_invnotif set
					id = '" . md5(uniqids(microtime())) . "',
					id_lead = '" . $_SESSION["LeadChStat"] . "',
					id_inv = '" . $rcIdIv['id'] . "',
					tgl = now(),
					fin_email = '" . $finemail[0] . "',
					fin_name = '" . $finemail[1] . "',
					notif_by = '" . $_SESSION["lgnNama"] . "',
					notif_by_id = '" . $_SESSION["tblID"] . "'";
        $dbs->getQuery($svNotif);

        $AlertMess = 'alert-success#done_all#SUCCESS!#Notification sent to; |' . $ccsent;
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Please set Payment Date!';
    }
}

/*
Ndak dipake.
Ini untuk percobaab generate pdf pake dompdf.

if ( $task == "PDF_".$_SESSION["butts_Enter"] ){

	require "system/CreatePDF.php";
	$invnum = "1905-095";
	$html = file_get_contents($_SESSION["ROOT_DIR"]."/agreement.html");
	$dompdf->loadHtml($html);

	$dompdf->setPaper('A4', 'portrait');
	$dompdf->render();

	//$dompdf->stream();
	$pdfoutput = $dompdf->output();

	$dir = "../../".UPDIR."/Agreement";
	if ( is_dir($dir) ){
		file_put_contents($dir."/".$invnum.'.pdf', $pdfoutput);
		$AlertMess = 'alert-success#done_all#SUCCESS!#Invoce number '.$invnum.' saved!';
	}else{
		$AlertMess = 'alert-danger#remove_circle#ERROR!#Saving directory is missing!';
	}

}
*/
