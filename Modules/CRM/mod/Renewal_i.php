<?php

if ($task == "ExpSrc_" . $_SESSION["butts_Src"]) {
    $filter_name = Purefy($_POST['filter_name']);
    $filter_date = Purefy($_POST['filter_date']);

    if (!empty($filter_name)) {
        $_SESSION["LeadSearch"] = $filter_name;
    }

    if (!empty($filter_date)) {
        $_SESSION["LeadSearchDateRange"] = $filter_date;
    } else {
        $default_filter_date = date('d/m/Y') . ' to ' . date_format(date_add(date_create(), date_interval_create_from_date_string("3 months")), "d/m/Y");
        $_SESSION["LeadSearchDateRange"] = $default_filter_date;
    }
}

if ($task == "ExpSrc_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["LeadSearch"]);

    $default_filter_date = date('d/m/Y') . ' to ' . date_format(date_add(date_create(), date_interval_create_from_date_string("3 months")), "d/m/Y");
    $_SESSION["LeadSearchDateRange"] = $default_filter_date;
}

if ($task == "RenewalLeadDet_" . $_SESSION["butts_Enter"]) {
    $Ldid = Purefy($_POST['Ldid']);
    if (!empty($Ldid)) {
        $_SESSION["RenewalLeadDets"] = $Ldid;
    }
}

if ($task == "LeadDet" . $_SESSION["butts_Enter"]) {
    $Ldid = Purefy($_POST['Ldid']);
    if (!empty($Ldid)) {
        $_SESSION["LeadDets"] = $Ldid;
    }
}

if ($task == $lang_2059 . "_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["RenewalLeadDets"]);
    unset($_SESSION["LeadDets"]);
}

if ($task == "RenewalLeadSales_" . $_SESSION["butts_Update"]) {
    $RenewalSales = Purefy($_POST['RenewalSales']);
    if (empty($RenewalSales)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_2045 . "|" . $lang_2027;
    } else {
        $pass = true;

        $cSales = "Select nama_asli from " . $tblp . "sys_lgndetail where id = ? "; // get full name
        $rcSales = $dbs->getArr($cSales, ["s|$RenewalSales"]);
        if (empty($rcSales['nama_asli'])) {
            $AlertMess .= "Sales not found! |";
            $pass = false;
        }

        if ($pass === true) {

            # Save Package
            $svlead = "update " . $tblp . "apps_lead set
						renew_sales_id = ?,
						renew_sales_name = ?,
						last_update = now(),
						tgl_update = now(),
						last_update_by = ?,
						last_update_by_id = ?
						where id_lead = ?
						";
            $bind = array(
                "s|$RenewalSales",
                "s|" . $rcSales['nama_asli'],
                "s|" . $_SESSION["lgnNama"],
                "s|" . $_SESSION["tblID"],
                "s|" . $_SESSION["RenewalLeadDets"]
            );
            $dbs->getQuery($svlead, "Exec", $bind);
            // UPDATE LEAD STATUS
            $AlertMess = 'alert-success#done_all#SUCCESS!#Sales Renewal Detail Saved!';
            unset($_SESSION["RenewalLeadDets"]);
        } else {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $AlertMess . "|" . $lang_2045;
        }
    }
}

$srcque_sales = "";
$srcque_sales_new = "";
if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
    $srcque_sales = " AND lead.sales_id = '" . $_SESSION['lgnDetID'] . "' ";
    $srcque_sales_new = " AND lead.renew_sales_id = '" . $_SESSION['lgnDetID'] . "' ";
}
$srcque_akta = "tracking.reminder_akta IS NOT NULL ";

if (!isset($_SESSION["LeadSearchDateRange"])) {
    $default_filter_date = date('d/m/Y') . ' to ' . date_format(date_add(date_create(), date_interval_create_from_date_string("3 months")), "d/m/Y");
    list($daterange01, $daterange02) = explode(" to ", $default_filter_date);
} else {
    list($daterange01, $daterange02) = explode(" to ", $_SESSION["LeadSearchDateRange"]);
}

if ($daterange02) {
    $srcque_akta .= "and DATE(tracking.reminder_akta) >= '" . convertDate($daterange01, "/") . "' and DATE(tracking.reminder_akta) <= '" . convertDate($daterange02, "/") . "' ";
} else {
    $srcque_akta .= "and DATE(tracking.reminder_akta) = '" . convertDate($daterange01, "/") . "' ";
}

if (isset($_SESSION["LeadSearch"])) {
    $srcque = "$srcque_akta and tracking.client_name like '%" . $_SESSION["LeadSearch"] . "%' $srcque_sales ";
    $srcque .= "OR $srcque_akta and tracking.client_id like '%" . $_SESSION["LeadSearch"] . "%' $srcque_sales ";
    $srcque .= "OR $srcque_akta and tracking.client_name like '%" . $_SESSION["LeadSearch"] . "%' $srcque_sales_new ";
    $srcque .= "OR $srcque_akta and tracking.client_id like '%" . $_SESSION["LeadSearch"] . "%' $srcque_sales_new ";
} else {
    $srcque = "$srcque_akta $srcque_sales OR $srcque_akta $srcque_sales_new ";
}

$cRenewal = "SELECT tracking.*, lead.id_lead, lead.sales_id, lead.sales_name, lead.renew_sales_id, lead.renew_sales_name ";
$cRenewal .= "FROM " . $tblp . "apps_trklist AS tracking ";
$cRenewal .= "INNER JOIN " . $tblp . "apps_lead AS lead ON lead.id_lead = tracking.lead_id ";
$cRenewal .= "WHERE $srcque ORDER BY tracking.reminder_akta ASC ";
//echo $cRenewal;
//exit(0);
$d = new paging($dbs, PAGE_ITEM, PAGE_LIST, -1, "LstLeads");
$d->query($cRenewal);
