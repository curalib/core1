<?php

require "system/CreateExcel.php";

$filename = 'Lead_Report';

$spreadsheet->getProperties()
	->setCreator("Bontot")
	->setLastModifiedBy("Bontot")
	->setTitle("Lead Report")
	->setSubject("Lead Report")
	->setDescription("Lead Report List for Office 2007 XLSX, generated using PHP classes.")
	->setKeywords("Lead Report Permit izin")
	->setCategory("Lead");

$spreadsheet->setActiveSheetIndex(0);
$activesheet = $spreadsheet->getActiveSheet();

$PageSetup = $activesheet->getPageMargins();
$PageSetup->setTop(0.95);
$PageSetup->setRight(0.75);
$PageSetup->setLeft(0.75);
$PageSetup->setBottom(0.95);

$activesheet->getPageSetup()
	->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT)
	->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
	->setFitToWidth(0)
	->setFitToHeight(0);

$activesheet->getDefaultRowDimension()->setRowHeight(27);
$spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
$spreadsheet->getDefaultStyle()->getFont()->setSize(9);
$spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
$spreadsheet->getDefaultStyle()->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

$activesheet->getPageSetup()->setHorizontalCentered(true);
$activesheet->setShowGridlines(false);

/*
$activesheet->mergeCells('C2:D2');
$activesheet->getStyle('C2:D2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$activesheet->setCellValue('C2', 'Finished Permit')->getStyle('A2')->getFont()->setBold(true);
$activesheet->getStyle('C2')->getFont()->setSize(16);
$activesheet->getStyle('C2')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
*/

$activesheet->getStyle('A1:F1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
$activesheet->getStyle('A')->getAlignment()->setHorizontal('center');
$activesheet->getStyle('C:F')->getAlignment()->setHorizontal('center');
$activesheet->getRowDimension('1')->setRowHeight(25);


$activesheet->getColumnDimension('A')->setWidth(10);
$activesheet->setCellValue('A1', 'Client ID')->getStyle('A1')->getFont()->setBold(true);
$activesheet->getColumnDimension('B')->setWidth(21);
$activesheet->setCellValue('B1', 'Client Name')->getStyle('B1')->getFont()->setBold(true);
$activesheet->getColumnDimension('C')->setWidth(18);
$activesheet->setCellValue('C1', 'WA')->getStyle('C1')->getFont()->setBold(true);
$activesheet->getColumnDimension('D')->setWidth(20);
$activesheet->setCellValue('D1', 'Email')->getStyle('D1')->getFont()->setBold(true);
$activesheet->getColumnDimension('E')->setWidth(12);
$activesheet->setCellValue('E1', 'Last Update')->getStyle('E1')->getFont()->setBold(true);
$activesheet->getColumnDimension('F')->setWidth(12);
$activesheet->setCellValue('F1', 'Status')->getStyle('F1')->getFont()->setBold(true);

$r = 2;
$activesheet->getRowDimension($r)->setRowHeight(3);
//	$activesheet->getRowDimension($r)->setRowHeight(300);

while ( $LeadReport = $dbs->getAssoc($rcLeadReport) ){
	$r++;
	/*
	if($r % 28 == 0){
		$activesheet->setCellValue('A'.$r, 'Client ID')->getStyle('A'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('B'.$r, 'Client Name')->getStyle('B'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('C'.$r, 'WA')->getStyle('C'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('D'.$r, 'Email')->getStyle('D'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('E'.$r, 'Last Update')->getStyle('E'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('F'.$r, 'Status')->getStyle('F'.$r)->getFont()->setBold(true);
		$r++;
		$activesheet->getRowDimension($r)->setRowHeight(3);
	} else {
	*/
	$activesheet->setCellValue('A'.$r, $LeadReport['client_id']);
	$activesheet->setCellValue('B'.$r, $LeadReport['client_name']);
	$activesheet->setCellValue('C'.$r, $LeadReport['client_wa']);
	$activesheet->setCellValue('D'.$r, $LeadReport['client_email']);
	$activesheet->setCellValue('E'.$r, date("d-m-Y", strtotime($LeadReport['tgl_update'])));
	$activesheet->setCellValue('F'.$r, $LeadReport['status_nm']);
	//		}
}

/*
while($r <= 100) {
	$r++;
	if($r % 28 == 0){
		$activesheet->setCellValue('A'.$r, 'Client ID')->getStyle('A'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('B'.$r, 'Client Name')->getStyle('B'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('C'.$r, 'WA')->getStyle('C'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('D'.$r, 'Email')->getStyle('D'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('E'.$r, 'Last Update')->getStyle('E'.$r)->getFont()->setBold(true);
		$activesheet->setCellValue('F'.$r, 'Status')->getStyle('F'.$r)->getFont()->setBold(true);
		$r++;
		$activesheet->getRowDimension($r)->setRowHeight(3);
	} else {
		$activesheet->setCellValue('A'.$r, "hello");
		$activesheet->setCellValue('B'.$r, "world");
		$activesheet->setCellValue('C'.$r, "world");
		$activesheet->setCellValue('D'.$r, "world");
		$activesheet->setCellValue('E'.$r, "world");
		$activesheet->setCellValue('F'.$r, "world");
	}
}
*/

$styleArray = array(
	'borders' => array(
		'allBorders' => array(
			'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
			'color' => array('argb' => 'FFCCCCCC'),
		),
	),
);

$activesheet->getStyle('A1:F'.$r)->applyFromArray($styleArray);
//    $activesheet->getStyle('A2:F'.$r)->getAlignment()->setWrapText(true);
	
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$filename.'.Xls"');
header('Cache-Control: max-age=0');

$writer->save('php://output');