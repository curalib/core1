
<!--===========Notification=========================================================-->

                            <div class="container-fluid page__heading-container">
                                <div class="page__heading d-flex align-items-center">
                                    <div class="flex">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb mb-0">
                                                <li class="breadcrumb-item"><a href="<?php echo $bchref?>"><i class="material-icons icon-20pt">home</i><?php //echo $lang_101?></a></li>
                                                <li class="breadcrumb-item active" aria-current="page">Notifications<?php //echo $lang_50130?></li>
                                            </ol>
                                        </nav>
                                        <h1 class="m-0">Notifications<?php //echo $lang_50130?></h1>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid page__container">
                                <?php
                                if(isset($_SESSION["AlertMess"]) OR !empty($_SESSION["AlertMess"])){
                                    echo '<div class="alert alert-soft-warning d-flex align-items-center" role="alert">';
                                        echo '<i class="material-icons mr-3">error_outline</i>';
                                        echo '<div class="text-body"><strong>Alert - </strong> '.$_SESSION["AlertMess"].'.</div>';
                                    echo '</div>';
                                    //echo '<div class="text-danger wiz_bs-25 wiz_ls-150">'.$_SESSION["AlertMess"].'</div>';
                                    unset($_SESSION["AlertMess"]);
                                }
                                ?><!-- -->
                                <div class="row">
                                    <div class="col-lg">



                                        <div class="card">

                                            <div class="card-header card-header-large bg-white d-flex align-items-center">
                                                <h4 class="card-header__title flex m-0">Notifications</h4>
                                                <form action="" method="post" name="UpdateNotif" id ="UpdateNotif" enctype="multipart/form-data">
                                                    <input type="hidden" name="task" />
                                                    <?php
                                                    echo '<input id="react_range" name="react_range" value="" type="text" class="flatpickr" data-toggle="flatpickr" data-flatpickr-date-format="Y-m-d">';
                                                    ?>
                                                </form>
                                                <small class="text-muted ml-5"><?php echo $n->data_info(); ?></small>
                                            </div>
                                            <div class="list-group tab-content list-group-flush">


                                              <?php
                                                //if(isset($_SESSION["Jbtn"]) && ($_SESSION["Jbtn"] == "Sales" || $_SESSION["Jbtn"] == "Manager" || $_SESSION["Jbtn"] == "Super Administrator")) {
                                                if(isset($_SESSION["lgnPriv"]) && ($_SESSION["lgnPriv"] == 4 || $_SESSION["lgnPriv"] == 2 || $_SESSION["lgnPriv"] == 1 || $_SESSION["lgnPriv"] == 0)) {
                                                  $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
    															                $day_now = $dt->format('w');
    				                                      // Hari rabu atau jumat
                                                  //if($day_now == 3 || $day_now == 5) {
    				                                      if($_SESSION["lgnPriv"] == 0 || ($day_now == 3 || $day_now == 5)) {
                                              ?>

                                                <div class="list-group-item list-group-item-action d-flex align-items-center ">
                                                    <div class="mr-3">
                                                      <form action="" method="post" name="NotifLeadPGToXLS" id ="NotifLeadPGToXLS">
                                                          <input type="hidden" name="task" />
                                                          <?php
                                                          if(isset($notif_date_range ) && !empty($notif_date_range)) {
                                                          echo '<input name="react_range" value="'.$notif_date_range.'" type="hidden"';
                                                          }
                                                          ?>
                                                      </form>
                                                        <a href="javascript:submitbutton('NotifExportToXLS_<?php echo $_SESSION['butts_Enter']?>','NotifLeadPGToXLS');">
                                                            <div class="avatar avatar-xs" style="width: 32px; height: 32px;">
                                                                <span class="avatar-title bg-purple rounded-circle"><i class="material-icons icon-16pt">cloud_download</i></span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="flex">
                                                        Follow up Lead <a href="javascript:submitbutton('NotifExportToXLS_<?php echo $_SESSION['butts_Enter']?>','NotifLeadPGToXLS');">Download</a>
                                                        <!--<small class="text-muted">1 hour ago</small>-->
                                                      </div>
                                                </div>


                                                <?php
                                                    }
                                                  }

                                                  //if(isset($_SESSION["Jbtn"]) && ($_SESSION["Jbtn"] == "Administrator" || $_SESSION["Jbtn"] == "Manager"  || $_SESSION["Jbtn"] == "Super Administrator")) {
                                                  if(isset($_SESSION["lgnPriv"]) && ($_SESSION["lgnPriv"] == 3 || $_SESSION["lgnPriv"] == 2 || $_SESSION["lgnPriv"] == 1 || $_SESSION["lgnPriv"] == 0)) {
                                                      //while($NotifDetailAll = $dbs->getAssoc($rcNotifDetailAll)) {
                                                      while($NotifDetailAll = $n->result_assoc()) {
                                                ?>
                                                <!--
                                                    <div class="mr-3">

                                                        <div class="avatar avatar-sm" style="width: 32px; height: 32px;">
                                                            <img src="<?php //echo BASEURL?>templates/Default/images/izin_img/256_daniel-gaffey-1060698-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">
                                                        </div>
                                                    </div>
                                                -->
                                                <div class="list-group-item list-group-item-action d-flex align-items-center ">
                                                    <div class="flex">
                                                        <!--<a href="">Ax.Demian</a> left a comment on-->
                                                        <form action="" method="post" name="View_<?php echo $NotifDetailAll['id_notif']; ?>" id ="View_<?php echo $NotifDetailAll['id_notif']; ?>">
                                                            <input type="hidden" name="task" />
                                                            <input type="hidden" name="tracking_id" value="<?php echo $NotifDetailAll['tracking_id']; ?>">
                                                        </form>
                                                        <a href=""><?php echo $NotifDetailAll['company_name']; ?></a> <?php echo BADGE_STATUS[$NotifDetailAll['trkitm_status']]; ?> <?php echo $NotifDetailAll['kode']; ?>
                                                      <div class="float-sm-right">
                                                        <a href="javascript:submitbutton('Track_<?php echo $_SESSION["butts_Enter"]?>_Notif','View_<?php echo $NotifDetailAll['id_notif']?>');">View<?php //echo $NotifDetail['tracking_id']?></a><br>
                                                      </div>
                                                      <div class="">
                                                        <small class="text-muted">
                                                          <?php
                                                          $LastActivityInDays = $HitDays->DayRange(convertDate($NotifDetailAll["tgl_update_itm"]), "now", false)->Result();
                                                          echo $LastActivityInDays[1];
                                                          ?>
                                                        </small>
                                                      </div>
                                                    </div>
                                                </div>
                                                <?php
                                                      }
                                                  }
                                                ?>
                                            </div>
                                        </div><!-- <div class="card"> -->

                                          <?php
                                          echo $n->print_page(NULL);
                                          echo $n->form_print_page();
                                          ?>





                                    </div>

                                </div>

                            </div>


                            <script>
                            $(document).ready(function() {
                                var now_date = moment().format("DD/MM/YYYY");
                                var amonth_ago = moment().subtract(10, 'days').format("DD/MM/YYYY");
                                $("#react_range").flatpickr({
                                    mode: "range",
                                    altInput: true,
                                    altFormat: "d/m/Y",
                                    dateFormat: "d/m/Y",
                                    allowInput: false,
                                    <?php
                                    if(isset($notif_date_fromS) && !empty($notif_date_fromS)) {
                                        echo 'defaultDate: ["'.$notif_date_fromS.'", "'.$notif_date_toS.'"],';
                                    } else {
                                        echo 'defaultDate: [amonth_ago, now_date],';
                                    }
                                    // onChange onClose onValueUpdate
                                    ?>
                                    onClose: function(selectedDates, dateStr, instance) {
                                        $(this).updateReAct("UpdateNotif");
                                    }
                                    //event.preventDefault();
                                });
                                $.fn.updateReAct = function(form_to_send) {
                                    var act_upReAct = form_to_send+"_<?php echo $_SESSION["butts_Update"] ?>";
                                    submitbutton(act_upReAct,form_to_send);
                                }
                            });
                            </script>

<!--===========End of Notification==================================================-->
