<?php
$udet = "select * from ".$tblp."sys_lgndetail where id_login = '".$_SESSION["tblID"]."'";
$rdt = $dbs->getArr($udet);

if ( $task == "EditUsr_".$_SESSION["butts_Save"] ){
	$FullName = Purefy($_POST['FullName']);
	$UBm = Purefy($_POST['UBm']);
	$PBm = Purefy($_POST['PBm']);
	
	if ( empty($FullName) ){
		$AlertMess = 'alert-danger#remove_circle#ERROR!#'.$lang_202;
	}else{
		$inLgnDet = "update ".$tblp."sys_lgndetail set 
					 nama_asli = ? 
					 where id = ?";
			$bind = array(
				"s|".$FullName, 
				"s|".$rdt['id']
			);
				
			$dbs->getQuery($inLgnDet,"Exec",$bind);
			
			//$AlertMess .= "Account Saved!";
			//$AlertMess = 'alert-success#done_all#SUCCESS!#'.$lang_406;
                        $AlertMess .= $lang_406;

                        if ( !empty($UBm) ){
				if ( $UBm != $PBm ){
					$AlertMess = 'alert-danger#remove_circle#ERROR!#Change Password Failed!.<br>'.$lang_203;
				}else{
					$pwdhash = password_hash($UBm, PASSWORD_BCRYPT, $passhashoptions);
					$inLgn = "update ".$tblp."sys_login set pwd = ? where id = ?";
					$bind = array(
								"s|".$pwdhash,
								"s|".$rdt['id_login']
							);
					$dbs->getQuery($inLgn,"Exec",$bind);
					$AlertMess .= "<br>".$lang_212;
				}
			}
			
			if ( Purefy($_POST['DA']) == 1 ){
				$cLastAv = "Select avatar from ".$tblp."sys_lgndetail where id = '".$rdt['id']."'";
				$rcLastAv = $dbs->getArr($cLastAv);
				$ftd = $_SESSION["ROOT_DIR"]."/"."../../".UPDIR."/avatar/".$rcLastAv['avatar'];
				if ( is_file($ftd) ){ unlink($ftd); }
				$upAv = "update ".$tblp."sys_lgndetail set avatar = '' where id = '".$rdt['id']."'";
				$dbs->getQuery($upAv);
				$AlertMess .= '<br>Avatar deleted!';
			}
			
			$AlertMess = 'alert-success#done_all#SUCCESS!#'.$AlertMess;
			
			$Hasil = $pm->File("UpAv",FILESIZES,"../../".UPDIR."/avatar")->Images(AVATAR_WD,0,"shrink")->Result();
			if ( $Hasil[0] == "FL_IM_SUCCESS" and $Hasil[1] != '' ){
				
				$cLastAv = "Select avatar from ".$tblp."sys_lgndetail where id = '".$rdt['id']."'";
				$rcLastAv = $dbs->getArr($cLastAv);
				$ftd = $_SESSION["ROOT_DIR"]."/"."../../".UPDIR."/avatar/".$rcLastAv['avatar'];
				if ( is_file($ftd) ){ unlink($ftd); }
				
				$upAv = "update ".$tblp."sys_lgndetail set avatar = '".$Hasil[1]."' where id = '".$rdt['id']."'";
				$dbs->getQuery($upAv);
				$AlertMess = $AlertMess."**".'alert-success#done_all#AVATAR SUCCESS!#'.$lang_6041;
				
			}else{
				$AlertMess = $AlertMess."**".'alert-warning#warning#AVATAR WARNING!#'.$FileManPesan[$Hasil[0]];
			}
			
	}
}
?>
