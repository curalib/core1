<?php

//echo "hhh1";
//exit;

$Kode_Kantor = $_SESSION["Dash_KodeKantor"];


// LINE CHART DATA
//-------------------------------------------------
$LC_dates = array();
$LC_leads = array();

$cLineChartData = "SELECT `leads`.`tgl_input`, COUNT(`leads`.`tgl_input`) AS `JumlahLead` FROM `izin_apps_lead` AS `leads` ";
$cLineChartData .= "WHERE str_to_date(`leads`.`tgl_input`, '%Y-%m-%d') BETWEEN '" . $lc_date_from . "' AND '" . $lc_date_to . "' ";
/*
    if($lead_status == "ALL") {
        //DO NOTHING
        //$cLineChartData .= "AND `status` = '".$lead_status."' ";
    } else {
        $cLineChartData .= "AND `status` = '".$lead_status."' ";
    }
    //$cLineChartData .= "AND `KodeKantor` = '".$Kode_Kantor."' ";
*/
if (isset($lead_status) && !empty($lead_status) && $lead_status != "ALL") {
    $cLineChartData .= "AND `leads`.`status` = '" . $lead_status . "' ";
}
$cLineChartData .= "GROUP BY str_to_date(`tgl_input`, '%Y-%m-%d') ";

//echo $cLineChartData;
//exit;

$rcLineChartData = $dbs->getQuery($cLineChartData);
while ($LineChartData = $dbs->getAssoc($rcLineChartData)) {
    $LC_leads[] = $LineChartData["JumlahLead"];
    $LC_dates[] = date("Y-m-d", strtotime($LineChartData["tgl_input"])); //
}

$leads_sum = array_sum($LC_leads);

$dates_to_js = json_encode($LC_dates);
$leads_to_js = json_encode($LC_leads);



// LEAD SOURCE PERCENTAGE
//-------------------------------------------------
//percentage data
$sl_TOTL = 0;
$src_sl_item_count_array = array();
//    $src_item_count_array = array_fill(0, sizeof(SRC_ITEM), 0);
//    $src_sl_item_count_array = array_fill_keys(SRC_ITEM, 0);
$src_sl_item_count_array = array_fill_keys(array_keys(SRC_ITEM), 0);

$cSourcePercentage = "SELECT `source_id`, `source` FROM `izin_apps_lead` ";
$cSourcePercentage .= "WHERE `status` <> 'LO' ";
$rcSourcePercentage = $dbs->getQuery($cSourcePercentage);
while ($SourcePercentage = $dbs->getAssoc($rcSourcePercentage)) {
    if (array_key_exists($SourcePercentage["source_id"], $src_sl_item_count_array)) {
        $src_sl_item_count_array[$SourcePercentage["source_id"]]++;
        $sl_TOTL++;
    }
} //while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)){
//percentage
$src_sl_item_percentage_array = array();
foreach ($src_sl_item_count_array as $key => $value) {
    $src_sl_item_percentage_array[$key] = round(($value / $sl_TOTL) * 100, 2);
}
$sl_pecentage_data = implode(",", $src_sl_item_percentage_array);

//bgcolor data
$i = 0;
$sizeofarray = sizeof(SRC_ITEM);
$sl_bgcolor_data_rgb = array();
while ($i < $sizeofarray) {
    $red_color = mt_rand(0, 255);
    $green_color = mt_rand(0, 255);
    $blue_color = mt_rand(0, 255);
    $sl_bgcolor_data_rgb[] = "rgb(" . $red_color . "," . $green_color . "," . $blue_color . ")";
    $i++;
}
$sl_bgcolor_data_raw = implode("', '", $sl_bgcolor_data_rgb);
$sl_bgcolor_data = "'" . $sl_bgcolor_data_raw . "'";

//labels data
$sl_labels_data_raw = implode("', '", SRC_ITEM);
$sl_labels_data = "'" . $sl_labels_data_raw . "'";






// PRODUCT CHART DATA
//-------------------------------------------------
$cProductChartData = "SELECT `package_name`, COUNT(`package_id`) AS PackageSum FROM `izin_apps_lead` AS `lead` ";
$cProductChartData .= "GROUP BY `package_name` ";
$rcProductChartData = $dbs->getQuery($cProductChartData);







// LOCATION PERCENTAGE
//-------------------------------------------------
//percentage data
$loc_TOTL = 0;


$src_lokasi = array();
$src_lokasi_id = array();
$cVoLocations = "SELECT `id`, `lokasi` FROM `izin_master_volocation` ";
$cVoLocations .= "WHERE `publish` = 'Y' ";
$cVoLocations .= "ORDER BY `lokasi` ASC";
$rcVoLocations = $dbs->getQuery($cVoLocations);
while ($VoLocations = $dbs->getAssoc($rcVoLocations)) {
    $src_lokasi_id[] = $VoLocations["id"];
    $src_lokasi[] = $VoLocations["lokasi"];
    //$src_lokasi[$VoLocations["id"]] = $VoLocations["lokasi"];
} //while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)){


//echo $src_lokasi;
//var_dump($src_lokasi);
//exit;


$src_loc_item_count_array = array();
$src_loc_item_count_array = array_fill_keys($src_lokasi_id, 0);
//    $src_loc_item_count_array = array_fill(0, sizeof($src_lokasi), 0);

//var_dump($src_loc_item_count_array);
//exit;

/*
define("LEAD_STATUS", array(
	'AC' => 'About to Contact',
	'WR' => 'Waiting for Response',
	'IN' => 'Interacted',
	'PS' => 'Proposal Sent',
	'IS' => 'Invoice Sent',
	'CL' => 'Closed',
	'LO' => 'Lost'
));

Pie chart lokasi
*/
$cLocationPercentage = "SELECT `prefloc_id`, `prefloc_name` FROM `izin_apps_lead` ";
$cLocationPercentage .= "WHERE `status` <> 'LO' ";
$rcLocationPercentage = $dbs->getQuery($cLocationPercentage);
while ($LocationPercentage = $dbs->getAssoc($rcLocationPercentage)) {
    if (array_key_exists($LocationPercentage["prefloc_id"], $src_loc_item_count_array)) {
        //            echo "<BR/> The : " . $src_loc_item_count_array[$LocationPercentage["prefloc_id"]];
        $src_loc_item_count_array[$LocationPercentage["prefloc_id"]]++;
        $loc_TOTL++;
    }
} //while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)){

//exit;

//percentage
$src_loc_item_percentage_array = array();
foreach ($src_loc_item_count_array as $key => $value) {
    $src_loc_item_percentage_array[$key] = round(($value / $loc_TOTL) * 100, 2);
}
$loc_pecentage_data = implode(",", $src_loc_item_percentage_array);


//var_dump($src_loc_item_count_array);
//var_dump($src_loc_item_percentage_array);
//exit;


//bgcolor data
$i = 0;
$loc_sizeofarray = sizeof($src_lokasi);
$loc_bgcolor_data_rgb = array();
while ($i < $loc_sizeofarray) {
    $red_color = mt_rand(0, 255);
    $green_color = mt_rand(0, 255);
    $blue_color = mt_rand(0, 255);
    $loc_bgcolor_data_rgb[] = "rgb(" . $red_color . "," . $green_color . "," . $blue_color . ")";
    $i++;
}
$loc_bgcolor_data_raw = implode("', '", $loc_bgcolor_data_rgb);
$loc_bgcolor_data = "'" . $loc_bgcolor_data_raw . "'";

//labels data
$loc_labels_data_raw = implode("', '", $src_lokasi);
$loc_labels_data = "'" . $loc_labels_data_raw . "'";






// KBLI PERCENTAGE
//-------------------------------------------------
//percentage data
$kbli_TOTL = 0;
$src_code_from = array();
$src_code_to = array();
$src_code_cat = array();
$src_code_desc = array();

$cKbli = "SELECT * FROM `izin_apps_kbli_category` ";
//$cKbli .= "WHERE `publish` = 'Y' ";
$cKbli .= "ORDER BY `code_cat` ASC";
$rcKbli = $dbs->getQuery($cKbli);
while ($Kbli = $dbs->getAssoc($rcKbli)) {
    $src_code_from[] = $Kbli["code_from"];
    $src_code_to[] = $Kbli["code_to"];
    $src_code_cat[] = $Kbli["code_cat"];
    $src_code_desc[$Kbli["code_cat"]] = $Kbli["code_desc"];
    //$src_lokasi[$VoLocations["id"]] = $VoLocations["lokasi"];
} //while( $SourcePercentage = $dbs->getAssoc($rcSourcePercentage)){

$src_kbli_item_count_array = array();
$src_kbli_item_count_array = array_fill_keys($src_code_cat, 0);
//$src_code_cat[] = "NA";
//$src_code_desc["NA"] = "Empty";
//$src_kbli_item_count_array["NA"] = 0;

$cKbliPercentage = "SELECT `comp_tdp_industry` FROM `izin_apps_lead` ";
$cKbliPercentage .= "WHERE `status` <> 'LO' ";
//$cKbliPercentage .= "AND `comp_tdp_industry` <> '' ";
//$cKbliPercentage .= "AND `comp_tdp_industry` <> NULL ";

$rcKbliPercentage = $dbs->getQuery($cKbliPercentage);
$na = true;
$kTOTL = 0;
$kempTOTL = 0;
while ($KbliPercentage = $dbs->getAssoc($rcKbliPercentage)) {
    $kTOTL++;
    for ($i = 0; $i <= 20; $i++) {
        //cat A to U = 21
        if ($KbliPercentage["comp_tdp_industry"] >= $src_code_from[$i] && $KbliPercentage["comp_tdp_industry"] <= $src_code_to[$i]) {
            $src_kbli_item_count_array[$src_code_cat[$i]]++;
            $kbli_TOTL++;
            $na = false;
            break 1;
        }
    }
    if ($na === true) {
        //  $src_kbli_item_count_array["NA"]++;
        $kempTOTL++;
    } else {
        $na = true;
    }
} //while( $KbliPercentage = $dbs->getAssoc($rcKbliPercentage))

//echo "total data: $kTOTL \r\n";
//echo "total empty: $kempTOTL \r\n";
//echo "total not empty: $kbli_TOTL \r\n";
//echo "\r\n";
//var_dump($src_kbli_item_count_array);
//exit;

//percentage
$src_kbli_item_percentage_array = array();
foreach ($src_kbli_item_count_array as $key => $value) {
    $src_kbli_item_percentage_array[$key] = round(($value / $kbli_TOTL) * 100, 2);
}
$kbli_pecentage_data = implode(",", $src_kbli_item_percentage_array);

//bgcolor data
$i = 0;
$sizeofarray = sizeof($src_code_cat);
$kbli_bgcolor_data_rgb = array();
while ($i < $sizeofarray) {
    $red_color = mt_rand(0, 255);
    $green_color = mt_rand(0, 255);
    $blue_color = mt_rand(0, 255);
    $kbli_bgcolor_data_rgb[] = "rgb(" . $red_color . "," . $green_color . "," . $blue_color . ")";
    $i++;
}
$kbli_bgcolor_data_raw = implode("', '", $kbli_bgcolor_data_rgb);
$kbli_bgcolor_data = "'" . $kbli_bgcolor_data_raw . "'";

//labels data
$kbli_labels_data_raw = implode("', '", $src_code_desc);
$kbli_labels_data = "'" . $kbli_labels_data_raw . "'";
//$labels_data = json_encode(SRC_ITEM);
//{"WA":"Whatsapp","EM":"Email","CC":"Call Center","RO":"Referral vOffice","RU":"Referral Unionspace","IN":"Instagram","FB":"Facebook","WI":"Walk In","LC":"Live Chat"}










// RECENT ACTIVITY DATA
//-------------------------------------------------

$RA_All = array();
$RA_Lead = array();
$RA_Invoice = array();
$RA_Tracking = array();

$cRecActAllLead = "SELECT `lead`.`client_id`, `lead`.`status`, `lead`.`tgl_update` FROM `izin_apps_lead` AS `lead` ";
$cRecActAllLead .= "WHERE str_to_date(`tgl_update`, '%Y-%m-%d') BETWEEN '" . $ra_date_from . "' AND '" . $ra_date_to . "' ";
$rcRecActAllLead = $dbs->getQuery($cRecActAllLead);
while ($RecActAllLead = $dbs->getAssoc($rcRecActAllLead)) {
    if ($RecActAllLead["status"] == "CL") {
        $RA_Invoice[$RecActAllLead["tgl_update"]] = array('inv', $RecActAllLead["client_id"]);
    } else {
        $RA_Lead[$RecActAllLead["tgl_update"]] = array('lead', $RecActAllLead["client_id"]);
    }
} //while( $RecActAllLead = $dbs->getAssoc($rcRecActAllLead)) {

$cRecActStrTrack = "SELECT `track`.`client_id`, `track`.`trk_status`, `track`.`tgl_start` FROM `izin_apps_trklist` AS `track` ";
$cRecActStrTrack .= "INNER JOIN `izin_apps_lead` AS `lead` ON `lead`.`id_lead` = `track`.`lead_id` ";
$cRecActStrTrack .= "WHERE `track`.`trk_status` = 0 ";
$cRecActStrTrack .= "AND str_to_date(`track`.`tgl_start`, '%Y-%m-%d') BETWEEN '" . $ra_date_from . "' AND '" . $ra_date_to . "' ";
$rcRecActStrTrack = $dbs->getQuery($cRecActStrTrack);
while ($RecActStrTrack = $dbs->getAssoc($rcRecActStrTrack)) {
    $RA_Tracking[$RecActStrTrack["tgl_start"]] = array('track', $RecActStrTrack["client_id"]);
} //while( $RecActAllLead = $dbs->getAssoc($rcRecActAllLead)) {


$cRecActClsTrack = "SELECT `track`.`client_id`, `track`.`trk_status`, `track`.`tgl_close` FROM `izin_apps_trklist` AS `track` ";
$cRecActClsTrack .= "INNER JOIN `izin_apps_lead` AS `lead` ON `lead`.`id_lead` = `track`.`lead_id` ";
$cRecActClsTrack .= "WHERE `track`.`trk_status` <> 0 ";
$cRecActClsTrack .= "AND str_to_date(`track`.`tgl_close`, '%Y-%m-%d') BETWEEN '" . $ra_date_from . "' AND '" . $ra_date_to . "' ";

$rcRecActClsTrack = $dbs->getQuery($cRecActClsTrack);
while ($RecActClsTrack = $dbs->getAssoc($rcRecActClsTrack)) {
    $RA_Tracking[$RecActClsTrack["tgl_close"]] = array('cltrack', $RecActClsTrack["client_id"]);
} //while( $RecActAllLead = $dbs->getAssoc($rcRecActAllLead)) {

//merge array
$RA_All = array_merge($RA_Lead, $RA_Invoice, $RA_Tracking);
krsort($RA_All);
$Count_RA_All = count($RA_All);
$Limit_lines = 0;

// CURRENT INVOICE DATA
//-------------------------------------------------
$cCurrentInv = "SELECT `lead`.`client_id`, `lead`.`client_name`, `lead`.`sales_name`, `lead`.`package_name`, `lead`.`last_update`, ";
$cCurrentInv .= "`lgndetail`.`avatar` FROM `izin_apps_lead` AS `lead` ";
$cCurrentInv .= "INNER JOIN `izin_sys_lgndetail` AS `lgndetail` ON `lead`.`sales_id`=`lgndetail`.`id` ";
$cCurrentInv .= "WHERE `lead`.`status` = 'IS' ";
$cCurrentInv .= "ORDER BY `lead`.`last_update` DESC";
$rcCurrentInv = $dbs->getQuery($cCurrentInv);
