<?php

//unset($_SESSION['AGREEMENT']);

if ($task == "DetInv" . $_SESSION["butts_Enter"]) {
    $Ldid = Purefy($_POST['Ldid']);
    if (!empty($Ldid)) {
        $_SESSION["Edit_ID"] = $Ldid;
    }
}

if ($task == "AGREEMENT" . $_SESSION["butts_Enter"]) {
    $Ldid = Purefy($_POST['Ldid']);
    if (!empty($Ldid)) {
        $_SESSION["AGREEMENT"] = $Ldid;

        // check if agreement exist
        $sql = "SELECT id, inv_num FROM " . $tblp . "apps_agreement WHERE `discard` = 0 AND inv_id = '$Ldid' LIMIT 1";
        $rcAgree = $dbs->getArr($sql);
        if (!empty($rcAgree['id'])) {
            $_SESSION["AGREEMENT_POST"] = $rcAgree['id'];
            $_SESSION["agreement_pdf"] = $rcAgree['inv_num'] . 'agreement.pdf';
        }
    }
}

if ($task == "3001_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["Edit_ID"]);
    unset($_SESSION["LeadChStat"]);
    unset($_SESSION["AGREEMENT"]);
    unset($_SESSION["AGREEMENT_POST"]);
    unset($_SESSION['agreement_pdf']);
}

if ($task == "InvPaid_" . $_SESSION["Restart_Dashboard"]) {
    $cDet = "select id, inv_num, tipe, sending from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rcDet = $dbs->getArr($cDet);
    if (empty($rcDet['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Send invoice failed! | Invoice not found!';
    } elseif ($rcDet['tipe'] == "PAID") {
        $AlertMess = 'alert-warning#warning#WARNING!#Payment confirmation invoice denied! | Invoice has been paid!';
    } elseif ($rcDet['tipe'] == "PREVIEW") {
        $AlertMess = 'alert-warning#warning#WARNING!#Payment confirmation cannot be done! | Invoice has not been sent yet!';
    } else {
        # Re-Save
        $TglByrFin = Purefy($_POST['TglByrFin']);
        $TtlPayFin = preg_replace("/\D/", "", Purefy($_POST['TtlPayFin']));
        if (empty($TglByrFin) || empty($TtlPayFin)) {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#Payment confirmation invoice denied! | Form incompleted!';
        } else {
            #Create Paid Invoice
            $InvMark = '<img src="' . convert_img_to_base64(ROOT_DIR . '/templates/Default/sysicons/Paid_i.png') . '" height="60">';

            $dtinv = "select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
            $rdtinv = $dbs->getArr($dtinv);

            $id_inv = $rdtinv['id'];
            $invnum = $rdtinv['inv_num'];
            $IssueDt = $rdtinv['issue_date'];
            $DueDt = $rdtinv['due_date'];
            //$PayDt = $rdtinv['tglbayar'];
            $PayDt = convertDate($TglByrFin);
            $IssueDtShow = c_date($IssueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
            $DueDtShow = c_date($DueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
            $PayDtShow = c_date($PayDt, "shortdate", "long", $list_month, $list_month_short, "Y");
            $invname = $rdtinv['name'];
            $invpic = $rdtinv['pic'];
            $invmail = $rdtinv['email'];
            $invphnum = $rdtinv['phone_number'];

            $citminv = "Select item, price, no_urut from " . $tblp . "apps_invitem where id_inv = '" . $rdtinv['id'] . "' order by no_urut asc";
            $rcitminv = $dbs->getQuery($citminv);
            while ($cit = $dbs->getAssoc($rcitminv)) {
                ${"invitm0" . $cit['no_urut']} = $cit['item'];
                ${"invprc0" . $cit['no_urut']} = preg_replace("/\D/", "", $cit['price']);
                if (!empty(${"invprc0" . $cit['no_urut']})) {
                    ${"prc0" . $cit['no_urut'] . "show"} = 'IDR ' . c_curr(${"invprc0" . $cit['no_urut']}, ",", ".", $min_sign = "mark") . ',-';
                } else {
                    ${"prc0" . $cit['no_urut'] . "show"} = '';
                }
            }

            $ammdue = $rdtinv['total_due'];

            $BnkAcc = $rdtinv['bank_acc'];
            $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
            $BAD_NmBank = $BnkAccDet[0];
            $BAD_NmPT = $BnkAccDet[1];
            $BAD_NoRek = $BnkAccDet[2];
            $BAD_SwfCode = $BnkAccDet[3];

            if (VABANK[$BnkAcc] === "00000") {
                $vaNumber = NULL;
            } else {
                $vaNumber = $rdtinv['va_number'];
            }

            $dir = "../../" . UPDIR . "/Invoices";
            if (!is_dir($dir)) {
                mkdir($dir, 0700);
            }
            $file = $dir . "/" . $invnum . ".pdf";

            if (is_file($_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php")) {
                #Load Invoice HTML
                $root_dir = ROOT_DIR;
                $judul_invoice = "Invoice";
                require $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php";
                try {
                    require "system/CreatePDF.php";
                    $dompdf->loadHtml($invhtml);
                    $dompdf->setPaper('A4', 'portrait');
                    $dompdf->render();
                    $pdfoutput = $dompdf->output();
                    if (is_dir($dir)) {
                        file_put_contents($file, $pdfoutput);
                        $createpdf = "OK";
                    } else {
                        $createpdf = "SHUT";
                    }
                } catch (Exception $e) {
                    $createpdf = "SHUT";
                }
            } else {
                $createpdf = "SHUT";
            }

            if ($createpdf == "OK") {
                $up = "update " . $tblp . "apps_invdet set tipe = 'PAID', paid = 'Y', verified_by = '" . $_SESSION["lgnNama"] . "', verified_by_id = '" . $_SESSION["tblID"] . "', verified_date	 = now(), tglbayar = '" . convertDate($TglByrFin) . "', total_payment = '" . $TtlPayFin . "' where id_lead = '" . $_SESSION["LeadChStat"] . "'";
                $dbs->getQuery($up);

                /* -----------
				SEND EMAIL HERE
				-------------- */
                require_once "Modules/Transaction/file/Mail_Inv_Paid.php"; // Pembayaran Invoice

                $AlertMess = 'alert-success#done_all#SUCCESS!#Payment confirmed!|' . $MailSendStatus;
            } else {
                $AlertMess = 'alert-danger#remove_circle#ERROR!#PDF file cannot be created!';
            }
        }
    }
}

if ($task == "InvBack_" . $_SESSION["Restart_Dashboard"]) {

    $dtinv = "SELECT * FROM `{$tblp}apps_invdet` WHERE `id_lead` = ?";
    $rdtinv = $dbs->getArr($dtinv, ["s|" . $_SESSION["LeadChStat"]]);

    if (empty($rdtinv['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Send invoice failed! | Invoice not found!';
    } else {
        $cTrk = "Select id_trklist from " . $tblp . "apps_trklist where lead_id = '" . $rdtinv['id_lead'] . "'";
        $rcTrk = $dbs->getArr($cTrk);

        if ($rdtinv['agree_status'] == "1") {
            $AlertMess = 'alert-warning#warning#WARNING!#Cancellation denied! | This lead already has an agreement!';
        } /* elseif ($rdtinv['verified_by'] == "System Xendit") {
            $AlertMess = 'alert-warning#warning#WARNING!#Cancellation denied! | This amount has been verified and settled by Xendit.';
        } */ elseif ($rdtinv['tipe'] == "SENT") {
            $AlertMess = 'alert-warning#warning#WARNING!#Cancellation denied! | Invoice has not been paid!';
        } elseif ($rdtinv['tipe'] == "PREVIEW") {
            $AlertMess = 'alert-warning#warning#WARNING!#Cancellation cannot be done! | Invoice has not been sent yet!';
        } elseif (!empty($rcTrk['id_trklist'])) {
            $AlertMess = 'alert-warning#warning#WARNING!#Cancellation denied! | This lead already on tracking system!';
        } else {
            #Create Invoice
            $InvMark = '<img src="' . convert_img_to_base64(ROOT_DIR . '/templates/Default/sysicons/PaidCancel_i.png') . '" height="60">';

            $id_inv = $rdtinv['id'];
            $invnum = $rdtinv['inv_num'];
            $IssueDt = $rdtinv['issue_date'];
            $DueDt = $rdtinv['due_date'];
            $PayDt = $rdtinv['tglbayar'];
            $IssueDtShow = c_date($IssueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
            $DueDtShow = c_date($DueDt, "shortdate", "long", $list_month, $list_month_short, "Y");
            $PayDtShow = c_date($PayDt, "shortdate", "long", $list_month, $list_month_short, "Y");
            $invname = $rdtinv['name'];
            $invpic = $rdtinv['pic'];
            $invmail = $rdtinv['email'];
            $invphnum = $rdtinv['phone_number'];

            $citminv = "Select item, price, no_urut from " . $tblp . "apps_invitem where id_inv = '" . $id_inv . "' order by no_urut asc";
            $rcitminv = $dbs->getQuery($citminv);
            while ($cit = $dbs->getAssoc($rcitminv)) {
                ${"invitm0" . $cit['no_urut']} = $cit['item'];
                ${"invprc0" . $cit['no_urut']} = preg_replace("/\D/", "", $cit['price']);
                if (!empty(${"invprc0" . $cit['no_urut']})) {
                    ${"prc0" . $cit['no_urut'] . "show"} = 'IDR ' . c_curr(${"invprc0" . $cit['no_urut']}, ",", ".", $min_sign = "mark") . ',-';
                } else {
                    ${"prc0" . $cit['no_urut'] . "show"} = '';
                }
            }

            $ammdue = $rdtinv['total_due'];

            $BnkAcc = $rdtinv['bank_acc'];
            $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
            $BAD_NmBank = $BnkAccDet[0];
            $BAD_NmPT = $BnkAccDet[1];
            $BAD_NoRek = $BnkAccDet[2];
            $BAD_SwfCode = $BnkAccDet[3];

            if (VABANK[$BnkAcc] === "00000") {
                $vaNumber = NULL;
            } else {
                $vaNumber = $rdtinv['va_number'];
            }

            $dir = "../../" . UPDIR . "/Invoices";
            if (!is_dir($dir)) {
                mkdir($dir, 0700);
            }
            $file = $dir . "/" . $invnum . ".pdf";

            if (is_file($_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php")) {
                #Load Invoice HTML
                $root_dir = ROOT_DIR;
                $judul_invoice = "Invoice";
                require $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php";
                try {
                    require "system/CreatePDF.php";
                    $dompdf->loadHtml($invhtml);
                    $dompdf->setPaper('A4', 'portrait');
                    $dompdf->render();
                    $pdfoutput = $dompdf->output();
                    if (is_dir($dir)) {
                        file_put_contents($file, $pdfoutput);
                        $createpdf = "OK";
                    } else {
                        $createpdf = "SHUT";
                    }
                } catch (Exception $e) {
                    $createpdf = "SHUT";
                }
            } else {
                $createpdf = "SHUT";
            }

            if ($createpdf == "OK") {
                $up = "UPDATE `{$tblp}apps_invdet` SET tipe = ?, paid = ?, tglbayar = ?, total_payment = ?, verified_by = ?, verified_by_id = ? WHERE id_lead = ? ";
                $dbs->getQuery($up, "Exec", array(
                    "s|SENT",
                    "s|N",
                    "s|0000-00-00 00:00:00",
                    "i|0",
                    "s|",
                    "s|",
                    "s|" . $rdtinv['id_lead']
                ));

                /* -----------
                SEND EMAIL HERE
                -------------- */
                require_once "Modules/Transaction/file/Mail_Inv_Cancel.php"; // Pembatalan Pembayaran Invoice

                $AlertMess = 'alert-success#done_all#SUCCESS!#Payment verification cancelled!';
            } else {
                $AlertMess = 'alert-danger#remove_circle#ERROR!#PDF file cannot be created!';
            }
        }

        #Recreate Original Invoice
        if (is_file($_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php")) {
            #Load Invoice HTML
            $root_dir = ROOT_DIR;
            $judul_invoice = "Proforma Invoice";
            $InvMark = '';
            require $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php";
            try {
                require "system/CreatePDF.php";
                $dompdf->loadHtml($invhtml);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $pdfoutput = $dompdf->output();
                if (is_dir($dir)) {
                    file_put_contents($file, $pdfoutput);
                    $createpdf = "OK";
                } else {
                    $createpdf = "SHUT";
                }
            } catch (Exception $e) {
                $createpdf = "SHUT";
            }
        } else {
            $createpdf = "SHUT";
        }
    }
}

if ($task == "InvBad_" . $_SESSION["Restart_Dashboard"]) {

    $dtinv = "select * from " . $tblp . "apps_invdet where id_lead = '" . $_SESSION["LeadChStat"] . "'";
    $rdtinv = $dbs->getArr($dtinv);

    if (empty($rdtinv['id'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Send invoice failed! | Invoice not found!';
    } else {
        $cTrk = "Select id_trklist from " . $tblp . "apps_trklist where lead_id = '" . $rdtinv['id_lead'] . "'";
        $rcTrk = $dbs->getArr($cTrk);

        if ($rdtinv['tipe'] == "PREVIEW") {
            $AlertMess = 'alert-warning#warning#WARNING!#Verification cannot be done! | Invoice has not been sent yet!';
        } elseif (!empty($rcTrk['id_trklist'])) {
            $AlertMess = 'alert-warning#warning#WARNING!#Verification denied! | This lead already on tracking system!';
        } else {
            #Create Bad Debt Invoice
            $InvMark = '<img src="' . convert_img_to_base64(ROOT_DIR . '/templates/Default/sysicons/Bad_i.png') . '" height="60">';

            $id_inv = $rdtinv['id'];
            $invnum = $rdtinv['inv_num'];
            $IssueDt = convertDate($rdtinv['issue_date']);
            $DueDt = convertDate($rdtinv['due_date']);
            $invname = $rdtinv['name'];
            $invpic = $rdtinv['pic'];
            $invmail = $rdtinv['email'];
            $invphnum = $rdtinv['phone_number'];

            $citminv = "Select item, price, no_urut from " . $tblp . "apps_invitem where id_inv = '" . $rdtinv['id'] . "' order by no_urut asc";
            $rcitminv = $dbs->getQuery($citminv);
            while ($cit = $dbs->getAssoc($rcitminv)) {
                ${"invitm0" . $cit['no_urut']} = $cit['item'];
                ${"invprc0" . $cit['no_urut']} = preg_replace("/\D/", "", $cit['price']);
                if (!empty(${"invprc0" . $cit['no_urut']})) {
                    ${"prc0" . $cit['no_urut'] . "show"} = 'IDR ' . c_curr(${"invprc0" . $cit['no_urut']}, ",", ".", $min_sign = "mark") . ',-';
                } else {
                    ${"prc0" . $cit['no_urut'] . "show"} = '';
                }
            }

            $ammdue = $rdtinv['total_due'];

            $BnkAcc = $rdtinv['bank_acc'];
            $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
            $BAD_NmBank = $BnkAccDet[0];
            $BAD_NmPT = $BnkAccDet[1];
            $BAD_NoRek = $BnkAccDet[2];
            $BAD_SwfCode = $BnkAccDet[3];

            if (VABANK[$BnkAcc] === "00000") {
                $vaNumber = NULL;
            } else {
                $vaNumber = $rdtinv['va_number'];
            }

            $dir = "../../" . UPDIR . "/Invoices";
            if (!is_dir($dir)) {
                mkdir($dir, 0700);
            }
            $file = $dir . "/" . $invnum . ".pdf";

            if (is_file($_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php")) {
                #Load Invoice HTML
                $root_dir = ROOT_DIR;
                $judul_invoice = "Invoice";
                require $_SESSION["ROOT_DIR"] . "/Modules/CRM/file/InvoiceHTML.php";
                try {
                    require "system/CreatePDF.php";
                    $dompdf->loadHtml($invhtml);
                    $dompdf->setPaper('A4', 'portrait');
                    $dompdf->render();
                    $pdfoutput = $dompdf->output();
                    if (is_dir($dir)) {
                        file_put_contents($file, $pdfoutput);
                        $createpdf = "OK";
                    } else {
                        $createpdf = "SHUT";
                    }
                } catch (Exception $e) {
                    $createpdf = "SHUT";
                }
            } else {
                $createpdf = "SHUT";
            }

            if ($createpdf == "OK") {
                $up = "update " . $tblp . "apps_invdet set tipe = 'BAD', paid = 'N', verified_by = '" . $_SESSION["lgnNama"] . "', verified_by_id = '" . $_SESSION["tblID"] . "', tglbayar = now() where id_lead = '" . $_SESSION["LeadChStat"] . "'";
                $dbs->getQuery($up);

                $AlertMess = 'alert-success#done_all#SUCCESS!#Success!';
            } else {
                $AlertMess = 'alert-danger#remove_circle#ERROR!#PDF file cannot be created!';
            }
        }
    }
}

if ($task == "UnsyncInv") {
    if ($_POST['invid'] != "" || !empty($_POST['invid'])) {
        $tmp = explode("#", $_POST['invid']);

        require "vendor/autoload.php";
        require "jurnal/Invoice.php";

        $errors = [];
        foreach ($tmp as $key => $item) {
            $invc = new Invoice();
            $res = $invc->delInvoice($item);
            if ($res <> "") $errors[] = $res;
            sleep(1);
        }

        $errors = array_unique($errors);
        if (count($errors) > 0) $AlertMess = 'alert-info#remove_circle#INFO!#' . implode(' <br>', $errors);
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Please select at least one invoice!';
    }
}

if ($task == "SyncInv") {
    if ($_POST['invid'] != "" || !empty($_POST['invid'])) {
        $tmp = explode("#", $_POST['invid']);

        require "vendor/autoload.php";
        require "jurnal/Invoice.php";

        $errors = [];
        foreach ($tmp as $key => $item) {
            $invc = new Invoice();
            $res = $invc->addInvoice($item);
            if ($res <> "") $errors[] = $res;
            sleep(1);
        }

        $errors = array_unique($errors);
        if (count($errors) > 0) $AlertMess = 'alert-info#remove_circle#INFO!#' . implode(' <br>', $errors);
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#Please select at least one invoice!';
    }
}

if ($task == "SrcInv_" . $_SESSION["butts_Src"]) {

    if (!empty($filter_name = Purefy($_POST['filter_name']))) {
        $_SESSION["InvSearch"] = $filter_name;
    }

    if (!empty($filter_issued_date = Purefy($_POST['filter_issued_date']))) {
        $_SESSION["InvSearchIssuedDateRange"] = $filter_issued_date;
    }

    if (!empty($filter_due_date = Purefy($_POST['filter_due_date']))) {
        $_SESSION["InvSearchDueDateRange"] = $filter_due_date;
    }

    if (!empty($filter_verified_date = Purefy($_POST['filter_verified_date']))) {
        $_SESSION["InvSearchVerifiedDateRange"] = $filter_verified_date;
    }
}

if ($task == "SrcInv_" . $_SESSION["butts_Reset"]) {
    unset($_SESSION["InvSearch"]);
    unset($_SESSION["InvSearchIssuedDateRange"]);
    unset($_SESSION["InvSearchDueDateRange"]);
    unset($_SESSION["InvSearchVerifiedDateRange"]);
}

if ($task == "ExcelInv_" . $_SESSION["buttkey"]) {
    require "system/CreateExcel.php";
    $filename = 'Listing Invoice';

    $spreadsheet->getProperties()
        ->setCreator("CRM")
        ->setLastModifiedBy("CRM")
        ->setTitle("Listing Lead")
        ->setSubject("Listing Lead Generated File")
        ->setDescription(
            "Listing Lead. Generated from CRM Applications."
        )
        ->setKeywords("Listing Lead")
        ->setCategory("Generated File");

    $spreadsheet->setActiveSheetIndex(0);
    $activesheet = $spreadsheet->getActiveSheet();

    $PageSetup = $activesheet->getPageMargins();
    $PageSetup->setTop(1);
    $PageSetup->setRight(0.5);
    $PageSetup->setLeft(0.5);
    $PageSetup->setBottom(1);

    $activesheet->getPageSetup()
        ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT)
        ->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4)
        ->setFitToWidth(0)
        ->setFitToHeight(0);


    $activesheet->getDefaultRowDimension()->setRowHeight(27);
    $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(8);
    $spreadsheet->getDefaultStyle()->getAlignment()->setWrapText(true);
    $spreadsheet->getDefaultStyle()->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
    $activesheet->setShowGridlines(false);

    $activesheet->getStyle('A1:M1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
    $activesheet->getRowDimension('1')->setRowHeight(35);

    $activesheet->getColumnDimension('A')->setWidth(13);
    $activesheet->setCellValue('A1', 'CLIENT ID')->getStyle('A1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('B')->setWidth(25);
    $activesheet->setCellValue('B1', 'CLIENT FULL NAME')->getStyle('B1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('C')->setWidth(25);
    $activesheet->setCellValue('C1', 'COMPANY NAME')->getStyle('C1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('D')->setWidth(15);
    $activesheet->setCellValue('D1', 'INVOICE')->getStyle('D1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('E')->setWidth(20);
    $activesheet->setCellValue('E1', 'ISSUED DATE')->getStyle('E1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('F')->setWidth(20);
    $activesheet->setCellValue('F1', 'DUE DATE')->getStyle('F1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('G')->setWidth(41);
    $activesheet->setCellValue('G1', 'INVOICE DETAIL')->getStyle('G1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('H')->setWidth(20);
    $activesheet->setCellValue('H1', 'TOTAL DUE')->getStyle('H1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('I')->setWidth(20);
    $activesheet->setCellValue('I1', 'PT')->getStyle('I1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('J')->setWidth(20);
    $activesheet->setCellValue('J1', 'PAYMENT DATE')->getStyle('J1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('K')->setWidth(20);
    $activesheet->setCellValue('K1', 'TOTAL PAYMENT')->getStyle('K1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('L')->setWidth(20);
    $activesheet->setCellValue('L1', 'VERIFICATION')->getStyle('L1')->getFont()->setBold(true);

    $activesheet->getColumnDimension('M')->setWidth(20);
    $activesheet->setCellValue('M1', 'SALES')->getStyle('M1')->getFont()->setBold(true);

    $activesheet->getStyle('A1')->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
    $activesheet->getStyle('A1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('B1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('C1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('D1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('E1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('F1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('G1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('H1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('I1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('J1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('K1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('L1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    $activesheet->getStyle('M1')->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

    $activesheet->getStyle('A1:M1')->getBorders()->getTop()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
    $activesheet->getStyle('A1:M1')->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

    if (!empty($filter_name = Purefy($_POST['filter_name']))) {
        $_SESSION["InvSearch"] = $filter_name;
    }

    if (!empty($filter_issued_date = Purefy($_POST['filter_issued_date']))) {
        $_SESSION["InvSearchIssuedDateRange"] = $filter_issued_date;
    }

    if (!empty($filter_due_date = Purefy($_POST['filter_due_date']))) {
        $_SESSION["InvSearchDueDateRange"] = $filter_due_date;
    }

    if (!empty($filter_verified_date = Purefy($_POST['filter_verified_date']))) {
        $_SESSION["InvSearchVerifiedDateRange"] = $filter_verified_date;
    }

    $r = 1;
    $srcque_user = "";
    $srcque_issued_date = "";
    $srcque_due_date = "";
    $srcque_verified_date = "";
    /*
    if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
        $srcque_user = " AND lead.sales_id = '" . $_SESSION['lgnDetID'] . "' ";
    }
    */

    if (isset($_SESSION["InvSearchIssuedDateRange"]) && $_SESSION["InvSearchIssuedDateRange"] != '') {
        list($issueddaterange01, $issueddaterange02) = explode(" to ", $_SESSION["InvSearchIssuedDateRange"]);
        if ($issueddaterange02) {
            $srcque_issued_date = "and DATE(issue_date) >= '" . convertDate($issueddaterange01, "/") . "' and DATE(issue_date) <= '" . convertDate($issueddaterange02, "/") . "' ";
        } else {
            $srcque_issued_date = "and DATE(issue_date) = '" . convertDate($issueddaterange01, "/") . "' ";
        }
    }

    if (isset($_SESSION["InvSearchDueDateRange"]) && $_SESSION["InvSearchDueDateRange"] != '') {
        list($duedaterange01, $duedaterange02) = explode(" to ", $_SESSION["InvSearchDueDateRange"]);
        if ($duedaterange02) {
            $srcque_due_date = "and DATE(due_date) >= '" . convertDate($duedaterange01, "/") . "' and DATE(due_date) <= '" . convertDate($duedaterange02, "/") . "' ";
        } else {
            $srcque_due_date = "and DATE(due_date) = '" . convertDate($duedaterange01, "/") . "' ";
        }
    }

    if (isset($_SESSION["InvSearchVerifiedDateRange"]) && $_SESSION["InvSearchVerifiedDateRange"] != '') {
        list($verifieddaterange01, $verifieddaterange02) = explode(" to ", $_SESSION["InvSearchVerifiedDateRange"]);
        if ($verifieddaterange02) {
            $srcque_verified_date = "and DATE(verified_date) >= '" . convertDate($verifieddaterange01, "/") . "' and DATE(verified_date) <= '" . convertDate($verifieddaterange02, "/") . "' ";
        } else {
            $srcque_verified_date = "and DATE(verified_date) = '" . convertDate($verifieddaterange01, "/") . "' ";
        }
    }

    if (isset($_SESSION["InvSearch"]) && $_SESSION["InvSearch"] != '') {
        $srcque .= "tipe != 'PREVIEW' and inv_num like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
        $srcque .= "or tipe != 'PREVIEW' and name like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
        $srcque .= "or tipe != 'PREVIEW' and client_id like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
        $srcque .= "or tipe != 'PREVIEW' and pic like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
    } else {
        $srcque = "tipe != 'PREVIEW' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
    }

    $cLsLead = "SELECT * from {$tblp}apps_invdet as inv INNER JOIN {$tblp}apps_lead AS lead ON inv.id_lead = lead.id_lead ";
    $cLsLead .= "WHERE $srcque ORDER BY due_date DESC";
    $rcLsLead = $dbs->getQuery($cLsLead);
    while ($ll = $dbs->getAssoc($rcLsLead)) {
        $r++;
        # Get Invoice Detail
        $invdetshow = '';
        $invdetshow01 = '';
        $invdetshow02 = '';
        $invdetshow03 = '';
        $invdetshow04 = '';
        $invdetshow05 = '';
        $invdetshow06 = '';
        $nu = 0;
        $cDetInv = "select item from " . $tblp . "apps_invitem where id_inv = '" . $ll['id'] . "' order by no_urut asc";
        $rcDetInv = $dbs->getQuery($cDetInv);
        while ($invdet = $dbs->getAssoc($rcDetInv)) {
            $nu++;
            if (!empty($invdet['item'])) {
                ${"invdetshow0" . $nu} = "\r " . $invdet['item'];
            }
        }

        $invdetshow = $invdetshow01 . $invdetshow02 . $invdetshow03 . $invdetshow04 . $invdetshow06 . $invdetshow05 . "\r ";

        $getSales = "select sales_name, client_id from " . $tblp . "apps_lead where id_lead = '" . $ll['id_lead'] . "'";
        $rgetSales = $dbs->getArr($getSales);

        $activesheet->getStyle('A' . $r . ':M' . $r)->getAlignment()->setIndent(1);
        $activesheet->getRowDimension($r)->setRowHeight(85);

        $t = BANK_ACC[$ll['bank_acc']];
        $tmp = explode("|", $t);
        $bank = $tmp[1];

        $activesheet->setCellValue('A' . $r, $rgetSales['client_id']);
        $activesheet->setCellValue('B' . $r, $ll['pic']);
        $activesheet->setCellValue('C' . $r, $ll['name']);
        $activesheet->setCellValue('D' . $r, $ll['inv_num']);
        $activesheet->setCellValue('E' . $r, c_date($ll['issue_date'], "shortdate", "long", $list_month, $list_month_short, "Y"));
        $activesheet->setCellValue('F' . $r, c_date($ll['due_date'], "shortdate", "long", $list_month, $list_month_short, "Y"));
        $activesheet->setCellValue('G' . $r, $invdetshow);
        $activesheet->getStyle('G' . $r)->getAlignment()->setWrapText(true);
        $activesheet->setCellValue('H' . $r, 'IDR ' . c_curr($ll['total_due'], ".", ",", $min_sign = "mark"));
        $activesheet->setCellValue('I' . $r, $bank);
        if ($ll['paid'] === 'Y') {
            $activesheet->setCellValue('J' . $r, c_date($ll['tglbayar'], "shortdate", "long", $list_month, $list_month_short, "Y"));
            $activesheet->setCellValue('K' . $r, 'IDR ' . c_curr($ll['total_payment'], ".", ",", $min_sign = "mark"));
            $activesheet->setCellValue('L' . $r, c_date($ll['verified_date'], "longdate", "short", $list_month, $list_month_short, "Y"));
        }
        $activesheet->setCellValue('M' . $r, $rgetSales['sales_name']);

        $activesheet->getStyle('A' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('A' . $r)->getBorders()->getLeft()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);
        $activesheet->getStyle('A' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $activesheet->getStyle('B' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('C' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('D' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('D' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $activesheet->getStyle('E' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('F' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('G' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('H' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('H' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $activesheet->getStyle('I' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('J' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('K' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('K' . $r)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $activesheet->getStyle('L' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $activesheet->getStyle('M' . $r)->getBorders()->getRight()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

        $activesheet->getStyle('A' . $r . ':M' . $r)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
    }
    $activesheet->getStyle('A' . $r . ':M' . $r)->getBorders()->getBottom()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . $filename . '.Xls"');
    header('Cache-Control: max-age=0');
    $writer->save('php://output');
}

if ($task == "DeleteAgreement_" . $_SESSION["butts_Save"]) {
    $sql = "SELECT COUNT(*) as jml FROM " . $tblp . "apps_agreement WHERE inv_id = '" . $_POST['inv_id'] . "' ";
    $koreksi = $dbs->getArr($sql);

    // 05.04.2022 -> cek limit revisi hanya 2x
    if ($koreksi['jml'] > 2) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3013;
        return;
    }

    $sql = "UPDATE " . $tblp . "apps_agreement SET `discard` = 1 WHERE `discard` = 0 AND id = '" . $_POST['agreement_id'] . "' ";
    $dbs->getQuery($sql, "Exec");

    $AlertMess = 'alert-success#done_all#SUCCESS!#Agreement canceled!';

    unset($_SESSION['AGREEMENT']);
    unset($_SESSION['AGREEMENT_POST']);
    unset($_SESSION['agreement_pdf']);
}

if ($task == "SaveAgreement_" . $_SESSION["butts_Save"]) {
    /** Check if Agreement already exist & active */
    $sql = "SELECT * FROM " . $tblp . "apps_agreement WHERE `discard` = 0 AND inv_id = '" . $_POST['inv_id'] . "' ";
    $result = $dbs->getArr($sql);

    if (!empty($result)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3010;
        return;
    }

    $izinSite = Purefy($_POST['izin_site']);
    $izinCity = Purefy($_POST['izin_city']);
    $promoName = Purefy($_POST['promo_name']);
    $detail = Purefy($_POST['detail']);
    $clientCompanyName = Purefy($_POST['client_company_name']);
    $clientCompanyAddress = Purefy($_POST['client_company_address']);
    $clientCompanyPhone = Purefy($_POST['client_company_phone']);
    $clientCompanyEmail = Purefy($_POST['client_company_email']);
    $clientPic = Purefy($_POST['client_pic']);
    $clientPosition = Purefy($_POST['client_position']);
    $additionalPlan = Purefy($_POST['additional_plan']);
    $additionalFee = Purefy($_POST['additional_fee']);
    $additionalDiscount = Purefy($_POST['additional_discount']);
    $additionalPromo = Purefy($_POST['additional_promo']);
    $additionalAmount = Purefy($_POST['additional_amount']);
    $additionalDetail = Purefy($_POST['additional_detail']);

    /** Validate Email */
    if (!filter_var($clientCompanyEmail, FILTER_VALIDATE_EMAIL)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_214;
        return;
    }

    /** Validate Phone Number */
    $filtered_phone_number = filter_var($clientCompanyPhone, FILTER_SANITIZE_NUMBER_INT); // Allow +, - and . in phone number
    $phone_to_check = str_replace("-", "", $filtered_phone_number); // Remove "-" from number
    // Check the lenght of number. This can be customized if you want phone number from a specific country
    if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_222;
        return;
    }

    if (!empty($izinCity) && !empty($detail) && !empty($clientCompanyName) && !empty($clientCompanyAddress) && !empty($clientCompanyEmail) && !empty($clientCompanyPhone) && !empty($clientPic) && !empty($clientPosition)) {
        $id = md5(uniqids(microtime()));
        $sql = "INSERT INTO " . $tblp . "apps_agreement SET id = ?,
                id_lead = ?,
                inv_num = ?,
                inv_id = ?,
                izin_site = ?,
                izin_city = ?,
                izin_company = ?,
                izin_address = ?,
                izin_account = ?,
                package_name = ?,
                promo_name = ?,
                total_payment = ?,
                detail = ?,
                client_company_name = ?,
                client_company_address = ?,
                client_company_phone = ?,
                client_company_email = ?,
                client_pic = ?,
                client_position = ?,
                additional_plan = ?,
                additional_fee = ?,
                additional_discount = ?,
                additional_promo = ?,
                additional_amount = ?,
                additional_detail = ?,
                created_at = ?";

        $binds = array(
            "s|" . $id,
            "s|" . $_POST['id_lead'],
            "s|" . $_POST['inv_num'],
            "s|" . $_POST['inv_id'],
            "s|" . $izinSite,
            "s|" . $izinCity,
            "s|" . $_POST['izin_company'],
            "s|" . $_POST['izin_address'],
            "s|" . $_POST['izin_account'],
            "s|" . $_POST['package_name'],
            "s|" . $_POST['promo_name'],
            "s|" . (int) $_POST['total_payment'],
            "s|" . $detail,
            "s|" . $clientCompanyName,
            "s|" . $clientCompanyAddress,
            "s|" . $clientCompanyPhone,
            "s|" . $clientCompanyEmail,
            "s|" . $clientPic,
            "s|" . $clientPosition,
            "s|" . $additionalPlan,
            "s|" . (int) $additionalFee,
            "s|" . (int) $additionalDiscount,
            "s|" . $additionalPromo,
            "s|" . (int) $additionalAmount,
            "s|" . $additionalDetail,
            "s|" . date('Y-m-d H:i:s')
        );
        $dbs->getQuery($sql, "Exec", $binds);

        // send Email
        $reminder = false;
        $Email_Address = $clientCompanyEmail;
        $invnum = $_POST['inv_num'];
        $url = BASEURL . 'agreement?id=' . $id;
        $BnkAcc = $_POST['izin_account'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmPT = $BnkAccDet[1];
        require_once "Modules/Transaction/file/MailNWA_Agreement.php";
        // send Whatsapp except IIA
        if ($BAD_NmPT != 'PT INVESTASI INDO ASIA') {
            $WaBlast_result = WaBlast($clientCompanyPhone, $WA_Message);
            $wa_message = WA_message($WaBlast_result);
            $AlertMess .= '<br/>' . $wa_message;
        }

        $_SESSION["AGREEMENT_POST"] = $id;
        $_SESSION["agreement_pdf"] = $_POST['inv_num'] . 'agreement.pdf';
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3028 . "|" . $lang_3007;
    }
}

if ($task == "ResendAgreement_" . $_SESSION["butts_Save"]) {
    /** Check if Agreement already exist, active, and not yet signed */
    $sql = "SELECT * FROM `{$tblp}apps_agreement` WHERE id = ? AND `discard` = 0 AND client_sign IS NULL ";
    $result = $dbs->getArr($sql, ["s|" . $_POST['agreement_id']]);

    if (empty($result)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3017;
        return;
    }

    if (!empty($result['correction_request'])) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3025;
        return;
    }

    $id = $result['id'];
    // Jika sudah 3 hari berselang, link expired. Agreement harus di reissued.
    $date1 = date_create(date('Y-m-d', strtotime($result['created_at'])));
    $date2 = date_create(date('Y-m-d'));
    $diff = date_diff($date1, $date2);
    if ($diff->format("%a") > 4) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3018;
        return;
    }

    // send Email
    $reminder = true;
    $clientPic = $result['client_pic'];
    $Email_Address = $result['client_company_email'];
    $invnum = $result['inv_num'];
    $url = BASEURL . 'agreement?id=' . $id;
    $BnkAcc = $result['izin_account'];
    $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
    $BAD_NmPT = $BnkAccDet[1];
    $clientCompanyPhone = $result['client_company_phone'];
    require_once "Modules/Transaction/file/MailNWA_Agreement.php";
    // send Whatsapp except IIA
    if ($BAD_NmPT != 'PT INVESTASI INDO ASIA') {
        $WaBlast_result = WaBlast($clientCompanyPhone, $WA_Message);
        $wa_message = WA_message($WaBlast_result);
        $AlertMess .= '<br/>' . $wa_message;
    }

    $_SESSION["AGREEMENT_POST"] = $id;
    $_SESSION["agreement_pdf"] = $result['inv_num'] . 'agreement.pdf';
}

if ($task == "VerifyAgreement_" . $_SESSION["butts_Save"]) {
    /** Check if Agreement already exist, active, and client signed */
    $sql = "SELECT a.*, b.total_due FROM " . $tblp . "apps_agreement a INNER JOIN " . $tblp . "apps_invdet b ON b.id = a.inv_id ";
    $sql .= "WHERE a.id = '" . $_POST['agreement_id'] . "' AND a.`discard` = 0 AND a.client_sign IS NOT NULL ";
    $result = $dbs->getArr($sql);

    if (empty($result)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3017;
        return;
    }

    $sql_sign = "SELECT * FROM " . $tblp . "apps_agreementsign WHERE id_agreement = ? AND id = ? LIMIT 1";
    $clientSign = $dbs->getArr($sql_sign, ["s|" . $result["id"], "s|" . $result["client_sign"]]);
    if ($clientSign != FALSE) {
        $fileName = $clientSign['id'] . $clientSign['file_ext'];
        $fullPath = "../../" . UPDIR . "/Signature/" . $fileName;
        if (file_exists($fullPath)) {
            $result['client_sign'] = [
                'file_name' => $fileName,
                'base64_img' => '<img src="' . convert_img_to_base64($fullPath) . '" style="width:210px;"/>',
                'signed_date' => $clientSign['signed_date']
            ];
        }
    }

    if ($result['izin_company'] === 'PT INVESTASI INDO ASIA') {
        $ttd = "./assets/images/TTD_IIA.png"; // TTD IIA
        if (!file_exists($ttd)) {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#File not found';
            return;
        }
        $base64_ttd = '<img src="' . convert_img_to_base64($ttd) . '" style="width:150px; height:"150px"/>';
    } else {
        $ttd = "./assets/images/TTD_PNG.png"; // TTD Izin Office
        if (!file_exists($ttd)) {
            $AlertMess = 'alert-danger#remove_circle#ERROR!#File not found';
            return;
        }
        $base64_ttd = '<img src="' . convert_img_to_base64($ttd) . '" style="width:210px;"/>';
    }

    $fileId = md5(uniqids(microtime()));
    $fileExt = pathinfo($ttd, PATHINFO_EXTENSION);
    $result['izin_sign'] = [
        'file_name' => $fileId . '.' . $fileExt,
        'base64_img' => $base64_ttd,
        'signed_date' => date('Y-m-d H:i:s')
    ];

    // Start pdf agreement
    #Load HTML
    $data = $result;
    $id = $result['id'];
    $clientSign = $result['client_sign'];
    $izinSign = $result['izin_sign'];

    try {
        require "system/CreatePDF.php";

        $_length = strlen($data['detail']) + strlen($data['additional_detail']);
        if ($_length < 200) {
            $spacing = "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
        } elseif ($_length > 200 && $_length < 350) {
            $spacing = "<tr><td colspan='5'></td></tr>";
        } else {
            $spacing = "";
        }

        $additional_fee = ($data['additional_fee'] == 0) ? "" : $data['additional_fee'];
        $additional_discount = ($data['additional_discount'] == 0) ? "" : $data['additional_discount'];
        $additional_amount = ($data['additional_amount'] == 0) ? "" : $data['additional_amount'];

        if (is_file($_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/AgreementHTML.php")) {
            require_once $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/AgreementHTML.php";

            $dompdf->loadHtml($file_html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            //$dompdf->stream("dompdf_out.pdf", array("Attachment" => false)); exit(0);
            $pdfoutput = $dompdf->output(['compress' => 1]);

            $invnum = $result['inv_num'];
            $dir = "../../" . UPDIR . "/Agreement";
            $file = $dir . "/" . $invnum . "agreement.pdf";

            if (is_dir($dir)) {
                file_put_contents($file, $pdfoutput);
                $createpdf = "OK";
            } else {
                $createpdf = "SHUT";
            }
        } else {
            $createpdf = "SHUT";
        }
    } catch (Exception $e) {
        $createpdf = "SHUT";
    }
    // End pdf agreement

    if ($createpdf == "OK") {
        // Start save Izin sign
        $sql = "INSERT INTO " . $tblp . "apps_agreementsign SET id = ?,
                file_ext = ?,
                file_size = ?,
                id_agreement = ?,
                signed_date = ?,
                signed_by = ?,
                signed_by_id = ?";

        $binds = array(
            "s|" . $fileId,
            "s|" . '.' . $fileExt,
            "i|" . filesize($ttd),
            "s|" . $result['id'],
            "s|" . $result['izin_sign']['signed_date'],
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"]
        );
        $dbs->getQuery($sql, "Exec", $binds);
        // End save Izin Sign

        // Start update agreement
        $sql = "UPDATE " . $tblp . "apps_agreement SET izin_sign = ?, verified_by = ?, verified_by_id = ?, verified_by_jabatan = ?, updated_at = ? ";
        $sql .= "WHERE `discard` = 0 AND id = '" . $_POST['agreement_id'] . "' ";
        $dbs->getQuery($sql, "Exec", array(
            "s|" . $fileId,
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["Jbtn"],
            "s|" . date('Y-m-d H:i:s'),
        ));
        // End update agreement

        // Start update agree_status invdet
        $sql = "UPDATE " . $tblp . "apps_invdet SET agree_status = ? WHERE id = '" . $_POST['inv_id'] . "' ";
        $dbs->getQuery($sql, "Exec", array(
            "s|1"
        ));
        // End update agree_status invdet

        if (!copy($ttd, "../../" . UPDIR . "/Signature/" . $result['izin_sign']['file_name'])) {
            //echo "failed to copy $ttd...\n";
        }

        $AlertMess = "alert-success#done_all#SUCCESS!#Agreement confirmed!";

        // send Agreement attachment
        $Email_Address = $result['client_company_email'];
        $clientPic = $result['client_pic'];
        $BnkAcc = $result['izin_account'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmPT = $BnkAccDet[1];
        $clientCompanyPhone = $result['client_company_phone'];
        require_once "Modules/Transaction/file/MailNWA_Agreement_Signed.php";
        // send Whatsapp except IIA
        if ($BAD_NmPT != 'PT INVESTASI INDO ASIA') {
            $WaBlast_result = WaBlast($clientCompanyPhone, $WA_Message);
            $wa_message = WA_message($WaBlast_result);
            $AlertMess .= '<br/>' . $wa_message;
        }
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#PDF file cannot be created!';
    }
}

if ($task == "UploadFile_" . $_SESSION["butts_Save"]) {
    /** Check if Agreement already exist, active, not yet signed */
    $sql = "SELECT * FROM " . $tblp . "apps_agreement WHERE id = '" . $_POST['agreement_id'] . "' AND `discard` = 0 AND client_sign IS NULL AND izin_sign IS NULL ";
    $result = $dbs->getArr($sql);

    if (empty($result)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3017;
        return;
    }

    $invnum = $result['inv_num'];
    $fileName = $invnum . 'agreement.pdf';
    $UploadFile = $pm->File("UpFl", FILESIZES, "../../" . UPDIR . "/Agreement", $fileName)->FileUpload()->Result();
    if ($UploadFile[0] == "FL_UPLOAD_SUCCESS" and !empty($UploadFile[1])) {
        // Start insert agreement file
        $inFile = "INSERT INTO " . $tblp . "apps_agreementfile SET id_agreementfile = ?, id_agreement = ?, agreementfile_name_uniq = ?, agreementfile_size = ?, agreementfile_name_ori = ?, agreementfile_upload_date = ?, agreementfile_uploader_id = ?, agreementfile_uploader_name = ?";
        $dbs->getQuery($inFile, "Exec", array(
            "s|" . md5(uniqids(microtime())),
            "s|" . $result["id"],
            "s|" . $UploadFile[1],
            "i|" . $UploadFile[2],
            "s|" . $UploadFile[3],
            "s|" . date('Y-m-d H:i:s'),
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["lgnNama"]
        ));
        // End insert agreement file

        // Start update agreement
        $sql = "UPDATE " . $tblp . "apps_agreement SET client_sign = ?, izin_sign = ?, verified_by = ?, verified_by_id = ?, verified_by_jabatan = ? ";
        $sql .= "WHERE `discard` = 0 AND id = '" . $_POST['agreement_id'] . "' ";
        $dbs->getQuery($sql, "Exec", array(
            "s|Y", // manual upload
            "s|Y", // manual upload
            "s|" . $_SESSION["lgnNama"],
            "s|" . $_SESSION["tblID"],
            "s|" . $_SESSION["Jbtn"]
        ));
        // End update agreement

        // Start update agree_status invdet
        $sql = "UPDATE " . $tblp . "apps_invdet SET agree_status = ? WHERE id = '" . $_POST['inv_id'] . "' ";
        $dbs->getQuery($sql, "Exec", array(
            "s|1"
        ));
        // End update agree_status invdet

        $AlertMess = 'alert-success#done_all#SUCCESS!#' . $FileManPesan[$UploadFile[0]];

        // send Agreement attachment
        $file = "../../" . UPDIR . "/Agreement/" . $fileName;
        $Email_Address = $result['client_company_email'];
        $clientPic = $result['client_pic'];
        $BnkAcc = $result['izin_account'];
        $BnkAccDet = explode("|", BANK_ACC[$BnkAcc]);
        $BAD_NmPT = $BnkAccDet[1];
        $clientCompanyPhone = $result['client_company_phone'];
        require_once "Modules/Transaction/file/MailNWA_Agreement_Signed.php";
        // send Whatsapp except IIA
        if ($BAD_NmPT != 'PT INVESTASI INDO ASIA') {
            $WaBlast_result = WaBlast($clientCompanyPhone, $WA_Message);
            $wa_message = WA_message($WaBlast_result);
            $AlertMess .= '<br/>' . $wa_message;
        }
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $FileManPesan[$UploadFile[0]];
    }
}

if ($task == "DownloadBlankAgreement_" . $_SESSION["butts_Save"]) {
    /** Check if Agreement already exist, active, and client !signed */
    $sql = "SELECT a.*, b.total_due FROM " . $tblp . "apps_agreement a INNER JOIN " . $tblp . "apps_invdet b ON b.id = a.inv_id ";
    $sql .= "WHERE a.id = '" . $_POST['agreement_id'] . "' AND a.`discard` = 0 AND a.client_sign IS NULL ";
    $result = $dbs->getArr($sql);

    if (empty($result)) {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#' . $lang_3017;
        return;
    }

    // Start pdf agreement
    #Load HTML
    $data = $result;
    $clientSign = $result['client_sign'];
    $izinSign = $result['izin_sign'];

    try {
        require "system/CreatePDF.php";

        $_length = strlen($data['detail']) + strlen($data['additional_detail']);
        if ($_length < 200) {
            $spacing = "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
            $spacing .= "<tr><td colspan='5'></td></tr>";
        } elseif ($_length > 200 && $_length < 350) {
            $spacing = "<tr><td colspan='5'></td></tr>";
        } else {
            $spacing = "";
        }

        $additional_fee = ($data['additional_fee'] == 0) ? "" : $data['additional_fee'];
        $additional_discount = ($data['additional_discount'] == 0) ? "" : $data['additional_discount'];
        $additional_amount = ($data['additional_amount'] == 0) ? "" : $data['additional_amount'];

        if (is_file($_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/AgreementHTML.php")) {
            require_once $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/AgreementHTML.php";

            $dompdf->loadHtml($file_html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $pdfoutput = $dompdf->output(['compress' => 1]);

            $invnum = $result['inv_num'];
            $dir = "../../" . UPDIR . "/Agreement";
            $file = $dir . "/" . $invnum . "agreement_form.pdf";

            if (is_dir($dir)) {
                file_put_contents($file, $pdfoutput);
                $createpdf = "OK";
            } else {
                $createpdf = "SHUT";
            }
        } else {
            $createpdf = "SHUT";
        }
    } catch (Exception $e) {
        $createpdf = "SHUT";
    }
    // End pdf agreement

    if ($createpdf == "OK") {
        $AlertMess = '';

        // Send Agreement attachment
        $Full_Name = $_SESSION['lgnNama'];
        $Email_Address = $_SESSION['lgnUid'];
        $clientPic = $result['client_pic'];
        require_once "Modules/Transaction/file/Mail_Agreement_Blank.php";
    } else {
        $AlertMess = 'alert-danger#remove_circle#ERROR!#PDF file cannot be created!';
    }
}
