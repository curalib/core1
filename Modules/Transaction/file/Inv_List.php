<?php
$srcque_user = "";
$srcque_issued_date = "";
$srcque_due_date = "";
$srcque_verified_date = "";
/*
if ($_SESSION['Jbtn'] == "Sales" || in_array($_SESSION['lgnPriv'], [4])) { // Sales
  $srcque_user = " AND lead.sales_id = '" . $_SESSION['lgnDetID'] . "' ";
}
*/

if (isset($_SESSION["InvSearchIssuedDateRange"]) && $_SESSION["InvSearchIssuedDateRange"] != '') {
  list($issueddaterange01, $issueddaterange02) = explode(" to ", $_SESSION["InvSearchIssuedDateRange"]);
  if ($issueddaterange02) {
    $srcque_issued_date = "and DATE(issue_date) >= '" . convertDate($issueddaterange01, "/") . "' and DATE(issue_date) <= '" . convertDate($issueddaterange02, "/") . "' ";
  } else {
    $srcque_issued_date = "and DATE(issue_date) = '" . convertDate($issueddaterange01, "/") . "' ";
  }
}

if (isset($_SESSION["InvSearchDueDateRange"]) && $_SESSION["InvSearchDueDateRange"] != '') {
  list($duedaterange01, $duedaterange02) = explode(" to ", $_SESSION["InvSearchDueDateRange"]);
  if ($duedaterange02) {
    $srcque_due_date = "and DATE(due_date) >= '" . convertDate($duedaterange01, "/") . "' and DATE(due_date) <= '" . convertDate($duedaterange02, "/") . "' ";
  } else {
    $srcque_due_date = "and DATE(due_date) = '" . convertDate($duedaterange01, "/") . "' ";
  }
}

if (isset($_SESSION["InvSearchVerifiedDateRange"]) && $_SESSION["InvSearchVerifiedDateRange"] != '') {
  list($verifieddaterange01, $verifieddaterange02) = explode(" to ", $_SESSION["InvSearchVerifiedDateRange"]);
  if ($verifieddaterange02) {
    $srcque_verified_date = "and DATE(verified_date) >= '" . convertDate($verifieddaterange01, "/") . "' and DATE(verified_date) <= '" . convertDate($verifieddaterange02, "/") . "' ";
  } else {
    $srcque_verified_date = "and DATE(verified_date) = '" . convertDate($verifieddaterange01, "/") . "' ";
  }
}

if (isset($_SESSION["InvSearch"]) && $_SESSION["InvSearch"] != '') {
  $srcque .= "tipe != 'PREVIEW' and inv_num like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
  $srcque .= "or tipe != 'PREVIEW' and name like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
  $srcque .= "or tipe != 'PREVIEW' and client_id like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
  $srcque .= "or tipe != 'PREVIEW' and pic like '%" . $_SESSION["InvSearch"] . "%' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
} else {
  $srcque = "tipe != 'PREVIEW' $srcque_issued_date $srcque_due_date $srcque_verified_date $srcque_user ";
}

$cLsLead = "SELECT * from {$tblp}apps_invdet as inv INNER JOIN {$tblp}apps_lead AS lead ON inv.id_lead = lead.id_lead ";
$cLsLead .= "WHERE $srcque ORDER BY due_date DESC";
$d = new paging($dbs, PAGE_ITEM, PAGE_LIST, -1, "LstInv");
$d->query($cLsLead);
?>

<div class="card">
  <div class="card-header card-header-large bg-white">
    <small class="text-muted"><?php echo $d->data_info(); ?></small>
  </div>
  <div class="card-header">
    <form action="" method="post" name="LeadSrcFrm" id="LeadSrcFrm">
      <input type="hidden" name="task" />
      <input type="hidden" name="invid" id="invid">
      <div class="form-row align-items-center">
        <div class="form-group col-md-3">
          <label for="filter_name"><?php echo $lang_2030 ?></label>
          <input id="filter_name" name="filter_name" type="text" class="form-control" placeholder="Enter keyword" value="<?php echo $_SESSION["InvSearch"] ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="filter_issued_date">Issued Date</label>
          <input id="filter_issued_date" name="filter_issued_date" type="text" class="form-control" placeholder="Select issued date range ..." data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y" value="<?php echo $_SESSION["InvSearchIssuedDateRange"] ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="filter_due_date">Due Date</label>
          <input id="filter_due_date" name="filter_due_date" type="text" class="form-control" placeholder="Select due date range ..." data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y" value="<?php echo $_SESSION["InvSearchDueDateRange"] ?>">
        </div>
        <div class="form-group col-md-3">
          <label for="filter_verified_date">Verification Date</label>
          <input id="filter_verified_date" name="filter_verified_date" type="text" class="form-control" placeholder="Select verification date range ..." data-toggle="flatpickr" data-flatpickr-mode="range" data-flatpickr-alt-format="d/m/Y" data-flatpickr-date-format="d/m/Y" value="<?php echo $_SESSION["InvSearchVerifiedDateRange"] ?>">
        </div>
      </div>
      <div class="form-row align-items-center">
        <div class="col-auto" style="margin-top:10px">
          <?= ButtonsCommon("commonbuttons", THEMES, "Cari_i", $lang_2030, "SrcInv_" . $_SESSION["butts_Src"], "LeadSrcFrm", "", "right") ?>
        </div>
        <?php if (
          (isset($_SESSION["InvSearch"]) && $_SESSION["InvSearch"] != '') ||
          (isset($_SESSION["InvSearchIssuedDateRange"]) && $_SESSION["InvSearchIssuedDateRange"] != '') ||
          (isset($_SESSION["InvSearchDueDateRange"]) && $_SESSION["InvSearchDueDateRange"] != '') ||
          (isset($_SESSION["InvSearchVerifiedDateRange"]) && $_SESSION["InvSearchVerifiedDateRange"] != '')
        ) : ?>
          <div class="col-auto" style="margin-top:10px">
            <?= ButtonsCommon("commonbuttons", THEMES, "Reset_i", "Clear Search", "SrcInv_" . $_SESSION["butts_Reset"], "LeadSrcFrm", "", "right") ?>
          </div>
        <?php endif; ?>
        <div class="col-auto" style="margin-top:10px">
          <?= ButtonsCommon("commonbuttons", THEMES, "Excell_i", "Export to Excel", "ExcelInv_" . $_SESSION["buttkey"], "LeadSrcFrm", "", "right") ?>
        </div>
        <div class="col-auto" style="margin-top:10px">
          <?= ButtonsCommon("commonbuttons", THEMES, "Ok_i", "Save Jurnal", "SyncInv", "LeadSrcFrm", "", "right") ?>
        </div>
        <div class="col-auto" style="margin-top:10px">
          <?= ButtonsCommon("commonbuttons", THEMES, "Back_i", "Undo Jurnal", "UnsyncInv", "LeadSrcFrm", "", "right") ?>
        </div>
      </div>
    </form>
  </div>
  <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values='["js-lists-values-employee-name"]'>
    <table class="table mb-0 thead-border-top-0" style="margin-right:-20px">
      <?php
      echo ' <thead>';
      echo '<tr>
                  <th  style="width: 5%;">
                  <label>
                    <input type="checkbox" class="check" id="checkAll"> Check All
                  </label> </th>
          				<th style="width: 8%;">Inv Num</th>
          				<th style="width: 7%;">Client ID</th>
          				<th style="width: 9%;">Due Date</th>
          				<th style="width: 20%">Invoice Name</th>
          				<th style="width: 11%;">PIC</th>
          				<th style="width: 12%; text-align:right;">Total Due</th>
                  <th style="width: 9%;">PT</th>
                  <th style="width: 9%;">Verification</th>
                  <th style="width: 10%;">Status</th>
        			</tr>
      				</thead>
      				<tbody class="list" id="staff">';

      while ($ll = $d->result_assoc()) {
        $llc++;

        # Determine Invoice Status
        switch ($ll['tipe']) {
          case "SENT":
            if (strtotime($ll['due_date']) < strtotime(date('Y-m-d', time()) . '00:00:00')) {
              $InvStatus = "OVER DUE";
              $styles = "color:red; font-weight: bold;";
            } else {
              if ($ll['unduh'] == "Y") {
                $InvStatus = "ACTIVATED";
                $styles = "color:silver; font-weight: bold;";
              } else {
                $InvStatus = "SENT";
                $styles = "color:silver; font-weight: bold;";
              }
            }
            break;

          case "PAID":
            $text = ($ll['agree_status'] == '0') ? "Agreement" : "<span style='color:green;'>Agreement Done</span>";
            $InvStatus = "PAID  <a href=\"javascript:submitbutton('AGREEMENT" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\">" . $text . "</a>";
            $styles = "color:green; font-weight: bold;";
            break;

          case "BAD":
            $InvStatus = "BAD DEBT";
            $styles = "color:red; font-weight: bold;";
            break;
        }
        $t = BANK_ACC[$ll['bank_acc']];
        $tmp = explode("|", $t);
        $bank = $tmp[1];

        if ($ll['syncstatus_jurnal'] == '2') {
          $InvStatus .= "<br> <span style='color:green;'>SYNC</span>";
        }

        echo '
					<tr>
            <td>
              <form action="" name="LstLead' . $llc . '" id="LstLead' . $llc . '" method="post">
                <input type="hidden" name="task">
                <input type="hidden" name="Ldid" value="' . $ll['id'] . '">
              </form>
              <input type="checkbox" class="chk" id="ind" name="sync[]" value="' . $ll['id'] . '">
            </td>
            <td><div class="media align-items-center">' . "<a href=\"javascript:submitbutton('DetInv" . $_SESSION["butts_Enter"] . "','LstLead" . $llc . "');\">" . $ll['inv_num'] . '</a></div></td>
            <td>' . $ll['client_id'] . '</td>
            <td>' . c_date($ll['due_date'], "shortdate", "short", $list_month, $list_month_short) . '</td>
            <td>' . $ll['name'] . '</td>
            <td>' . $ll['pic'] . '</td>
					  <td align="right">IDR ' . c_curr($ll['total_due'], ",", ".", $min_sign = "mark") . ',-</td>
            <td>' . $bank . '</td>
            <td><small class="text-muted">' . c_date($ll['verified_date'], "longdate", "short", $list_month, $list_month_short) . '</small></td>
					  <td style="' . $styles . '">' . $InvStatus . '</td>
          		</tr>
				';
      }
      ?>
      </tbody>
    </table>
  </div>
</div>


<div class="mt-4">
  <?php
  echo $d->print_page();
  echo $d->form_print_page();
  ?>
</div>

<script type="text/javascript">
  $("#staff").on("change", "#ind", function(event) {
    var tmp = $("#invid").val()
    if (tmp != "") {
      tmp = tmp.split("#");
    } else {
      tmp = []
    }
    if ($(this)[0].checked) {
      tmp.push($(this).val())
      $("#invid").val(tmp.join("#"));
    } else {
      var index = tmp.indexOf($(this).val());
      if (index !== -1) tmp.splice(index, 1);
      $("#invid").val(tmp.join("#"));
    }

    console.log($("#invid").val())

  });
  $("#checkAll").change(function() {
    if (this.checked) {
      // $(".chk").prop('checked', $(this).prop('checked'));
      $('.chk').each(function() {
        if ($(this).is(":disabled")) {
          console.log($(this));
        } else {
          var tmp = $("#invid").val()
          if (tmp != "") {
            tmp = tmp.split("#");
          } else {
            tmp = []
          }
          tmp.push($(this).val())
          $("#invid").val(tmp.join("#"));
          $(this).prop('checked', true);
        }
      });
    } else {
      $('.chk').each(function() {
        if ($(this).is(":disabled")) {
          console.log($(this));
        } else {
          var tmp = $("#invid").val()
          if (tmp != "") {
            tmp = tmp.split("#");
          } else {
            tmp = []
          }
          var index = tmp.indexOf($(this).val());
          if (index !== -1) tmp.splice(index, 1);
          $("#invid").val(tmp.join("#"));
          $(this).prop('checked', false);
        }
      });
    }
  });

  $("#aggrementBtn").on("click", function() {
    window.location.href = "/online/erp.izin.co.id/www/Modules/Transaction/file/Agrement.php"
  })
</script>