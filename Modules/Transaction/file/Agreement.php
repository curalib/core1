<?php

/**
 * New Agreement file, 2022, signed draw canvas
 */
?>

<div class="container-fluid page__container"></div>

<?php
$isNew = false;
$clientSigned = false;
$izinSigned = false;
$isPdf = false;

if (isset($_SESSION["AGREEMENT_POST"])) {
    $AgreementStatus = "PUBLISHED";
    $AgreementStyles = "color:black; font-weight: bold;";

    $sql = "SELECT * FROM " . $tblp . "apps_agreement WHERE `discard` = ? AND id = ? LIMIT 1";
    $data = $dbs->getArr($sql, ["s|0", "s|" . $_SESSION["AGREEMENT_POST"]]);

    if (!empty($data['verified_by_id'])) {
        // pdf created
        $dir = "../../" . UPDIR . "/Agreement";
        $pdffile = $dir . "/" . $data['inv_num'] . "agreement.pdf";
        if (is_file($pdffile)) {
            $isPdf = true;
        }
    }

    $izinCompany = $data['izin_company'];
    $izinAddress = $data['izin_address'];
    $izinAccount = $data['izin_account'];

    if ($data["client_sign"] === 'Y') // manual upload
    {
        $AgreementStatus = "AGREED";
        $AgreementStyles = "color:green; font-weight: bold;";
    } else {
        $sql = "SELECT * FROM " . $tblp . "apps_agreementsign WHERE id_agreement = ? AND id = ? LIMIT 1";
        $clientSign = $dbs->getArr($sql, ["s|" . $data['id'], "s|" . $data["client_sign"]]);
        if ($clientSign != FALSE) {
            $fileName = $clientSign['id'] . $clientSign['file_ext'];
            $fullPath = "../../" . UPDIR . "/Signature/" . $fileName;
            if (file_exists($fullPath)) {
                $clientSigned = true;
                $data['client_sign'] = [
                    'file_name' => $fileName,
                    'base64_img' => 'data:image/png;base64,' . base64_encode(file_get_contents($fullPath)),
                    'signed_date' => $clientSign['signed_date'],
                    'signed_by' => $clientSign['signed_by']
                ];

                $AgreementStatus = "AGREED";
                $AgreementStyles = "color:green; font-weight: bold;";
            }
        }
    }

    if ($data["izin_sign"] === 'Y') // manual upload
    {
        $sql = "SELECT * FROM " . $tblp . "apps_agreementfile WHERE id_agreement = ? LIMIT 1";
        $izinSign = $dbs->getArr($sql, ["s|" . $data['id']]);
        if ($izinSign != FALSE) {
            $izinSigned = true;
            $data['izin_sign'] = [
                'signed_date' => $izinSign['agreementfile_upload_date'],
                'signed_by' => $izinSign['agreementfile_uploader_name']
            ];
        }
    } else {
        $sql = "SELECT * FROM " . $tblp . "apps_agreementsign WHERE id_agreement = ? AND id = ? LIMIT 1";
        $izinSign = $dbs->getArr($sql, ["s|" . $data['id'], "s|" . $data["izin_sign"]]);
        if ($izinSign != FALSE) {
            $fileName = $izinSign['id'] . $izinSign['file_ext'];
            $fullPath = "../../" . UPDIR . "/Signature/" . $fileName;
            if (file_exists($fullPath)) {
                $izinSigned = true;
                $data['izin_sign'] = [
                    'file_name' => $fileName,
                    'base64_img' => 'data:image/png;base64,' . base64_encode(file_get_contents($fullPath)),
                    'signed_date' => $izinSign['signed_date'],
                    'signed_by' => $izinSign['signed_by']
                ];
            }
        }
    }
} else {
    require_once "jurnal/Invoice.php";
    $invoice = new Invoice();
    $data = $invoice->getInvoice($_SESSION['AGREEMENT']);

    $isNew = true;
    $isPdf = false;
    $cfgBank = explode("|", BANK_ACC[$data['bank_acc']]);
    $cfgCompany = BANK_TO_COMPANY[$data['bank_acc']];

    $izinCompany = $cfgBank[1];
    $izinAddress = $cfgCompany['address'];
    $izinAccount = $data['bank_acc'];
}
?>

<?php if (!$isNew) : ?>
    <div class="container-fluid page__container">
        <div class="card card-form flex flex-sm-row">
            <div class="card-body card-form__body flex" style="padding-top:15px;padding-bottom:10px;">
                <div class="row">
                    <div class="col-sm-auto flex">
                        <label>Agreement Status</label>
                        <h6 style="<?= $AgreementStyles ?>"><?= $AgreementStatus ?></h6>
                    </div>
                    <div class="col-sm-auto flex">
                        <label>Client Agreement Date</label>
                        <h6><?= ($clientSigned) ? c_date($data['client_sign']['signed_date'], "shortdate", "short", $list_month, $list_month_short) : '-' ?></h6>
                    </div>
                    <div class="col-sm-auto flex">
                        <label>Verification Date</label>
                        <h6><?= ($izinSigned) ? c_date($data['izin_sign']['signed_date'], "longdate", "short", $list_month, $list_month_short) : '-' ?></h6>
                    </div>
                    <div class="col-sm-auto flex">
                        <label>Verified By</label>
                        <h6><?= ($izinSigned) ? $data['izin_sign']['signed_by'] : '-' ?></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<style>
    .header .logo .logo_name {
        font-size: 30px;
        margin-left: 73%;
        width: 180px;
        border: 5px green solid;
    }

    .moto {
        margin-top: -30px;
        float: right;
    }

    .welcome .welcome_text {
        margin-top: -30px;
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        font-size: 24px;
    }

    .welcome .agreement-text {
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
        font-size: 30px;
        opacity: 0.3;
        margin-top: -45px;
    }

    .wrapper .container {
        display: inline-block;
        width: 49%;
    }

    .wrapper {
        display: inline-block;
    }

    .wrapper .container .title {
        height: 20px;
        border: none;
        border-radius: 10px 10px;
    }

    .title-text {
        color: white;
        margin-left: 10px;
    }

    .template {
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .agreement {
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }

    .table {
        width: 100%;
    }

    .agreement-box {
        margin-left: 30px;
        margin-right: 30px;
    }

    .agreement-box .table tbody tr td {
        padding: 5px;
    }

    .field .table .field-col-2 {
        border: none;
        border-bottom: 1px solid black;
        width: 13%;
        padding: 3px;
    }

    .field .table .field-col-4 {
        width: 23%;
        padding: 3px;
    }

    .field .table .field-col-6 {
        border: none;
        border-bottom: 1px solid black;
        width: 33%;
        padding: 3px;
    }

    .field .table .field-col-8 {
        border: none;
        border-bottom: 1px solid black;
        width: 43%;
        padding: 3px;
    }

    .wrapper .footer {
        margin-top: 20px;
    }

    .wrapper .footer .footer-text {
        font-size: 13px;
    }

    .wrapper .footer-box {
        margin-top: 20px;
    }

    .wrapper .referal-box .table-referal {
        width: 100%;
    }

    .wrapper .referal-box {
        margin-top: 20px;
        margin-left: 40%;
        margin-right: 50px;
        border: 1px black solid;
        border-radius: 10px 10px;
    }

    .wrapper .referal-box .table-referal td {
        padding: 3px;
    }

    .email-communication ol {
        font-size: 12px;
        margin-left: 30px;
        margin-right: 50px;
    }

    .end-page .table tr td {
        opacity: 0.5;
    }
</style>
<div class="container-fluid page__container">
    <?php if ($pdffile) {
        echo '<iframe src="../../pdf.html" scrolling="no" align="middle" frameborder="0" width="100%" height="1200"></iframe>';
    } else { ?>
        <!-- BEGIN Form -->
        <div class="container-fluid page_print" style="overflow:scroll">
            <div class="card card-form flex flex-sm-row">
                <div class="card-body card-form__body flex" style="padding-top:15px;padding-bottom:10px;">
                    <label>PAID Agreement</label>
                    <div class="table-responsive border-bottom">
                        <?php if ($isNew) { ?>
                            <form action="" method="POST" name="AddAgreement" enctype="multipart/form-data">
                                <input type="hidden" name="task" />
                                <input type="hidden" value="<?= $data['id_lead']; ?>" name="id_lead" />
                                <input type="hidden" value="<?= $data['idinv']; ?>" name="inv_id" />
                                <input type="hidden" value="<?= $data['inv_num']; ?>" name="inv_num" />
                            <?php } else { ?>
                                <form action="" method="POST" name="DeleteAgreement">
                                    <input type="hidden" name="task" />
                                    <input type="hidden" value="<?= $_SESSION['AGREEMENT_POST']; ?>" name="agreement_id" />
                                    <input type="hidden" value="<?= $data['inv_id']; ?>" name="inv_id" />
                                </form>
                                <form action="" method="POST" name="ResendAgreement">
                                    <input type="hidden" name="task" />
                                    <input type="hidden" value="<?= $_SESSION['AGREEMENT_POST']; ?>" name="agreement_id" />
                                </form>
                                <?php if ($clientSigned) { ?>
                                    <form action="" method="POST" name="VerifyAgreement">
                                        <input type="hidden" name="task" />
                                        <input type="hidden" value="<?= $_SESSION['AGREEMENT_POST']; ?>" name="agreement_id" />
                                        <input type="hidden" value="<?= $data['inv_id']; ?>" name="inv_id" />
                                    </form>
                                <?php } else { ?>
                                    <form action="" method="POST" name="DownloadAgreement">
                                        <input type="hidden" name="task" />
                                        <input type="hidden" value="<?= $_SESSION['AGREEMENT_POST']; ?>" name="agreement_id" />
                                        <input type="hidden" value="<?= $data['inv_id']; ?>" name="inv_id" />
                                    </form>
                                <?php } ?>
                            <?php } ?>
                            <?php
                            if ($izinCompany === 'PT INVESTASI INDO ASIA') {
                                $company['background'] = '../../assets/images/watermark-iia.png';
                                $company['logo'] = '../../templates/Default/images/invest_in_asia.png';
                                $company['service_name'] = 'InvestInAsia';
                                $company['tagline_name'] = '';
                                $company['pic_name'] = 'Endah Wahyuningsih';
                                $company['pic_position'] = 'Operation Manager';
                                $company['primary_color'] = '#223666';
                                $company['secondary_color'] = '#27afa7';
                                $company['contact_info'] = '+6221 5091 9500';
                                $company['email_info'] = 'hello@investinasia.id';
                                $company['address_info'] = 'District 8, Treasury Tower Lt. 6 <br>
                                Unit F, Senayan, Kebayoran Baru, <br>
                                Kota Administrasi Jakarta Selatan, <br>
                                DKI Jakarta <br>';
                            } else {
                                $company['background'] = '../../assets/images/watermark.jpg';
                                $company['logo'] = '../../assets/images/logo-dark-backup.png';
                                $company['service_name'] = 'IZIN.co.id';
                                $company['tagline_name'] = 'START YOUR BUSSINESS RIGHT';
                                $company['pic_name'] = 'Otty Yuniarty Y';
                                $company['pic_position'] = 'General Manager';
                                $company['primary_color'] = 'green';
                                $company['secondary_color'] = 'green';
                                $company['contact_info'] = '+6221 2955 3500';
                                $company['email_info'] = 'cs@izin.co.id';
                                $company['address_info'] = 'Centennial Tower, Lantai 29, Kav. 24-25 Unit D-F<br>
                                Jl. Jend. Gatot Subroto No. 27 RT 2/RW 2 <br>
                                Kel. Karet Semanggi, Kec. Setiabudi, Kota Administrasi <br>
                                Jakarta Selatan DKI Jakarta 12930 <br>';
                            }
                            ?>
                            <div class="template" style="background-image: url(<?= $company['background'] ?>)">
                                <!-- header -->
                                <nav class="header">
                                    <div class="logo">
                                        <img style="height: 50px; margin-left: 240mm;" src="<?= $company['logo'] ?>">
                                    </div>
                                    <br><br>
                                    <div class="moto">
                                        <p><?= $company['tagline_name'] ?></p>
                                    </div>
                                    <div class="welcome" style="color: <?= $company['secondary_color'] ?>;">
                                        <p class="welcome_text">Selamat Datang di <b><?= $company['service_name'] ?></b></p>
                                    </div>
                                    <div>
                                        <p>Perjanjian Layanan <?= $company['service_name'] ?> dengan Klien dibawah ini :</p>
                                    </div>
                                </nav>
                                <!-- header -->

                                <div class="wrapper">
                                    <!-- body main -->
                                    <div class="container">
                                        <div class="title" style="background-color: <?= $company['primary_color'] ?>;">
                                            <p class="title-text">1&nbsp;&nbsp;<?= $company['service_name'] ?></p>
                                        </div>
                                        <div class="field">
                                            <table class="table">
                                                <tr>
                                                    <td class="field-col-2">Kota</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input class="form-control" placeholder="City" type="text" name="izin_city" value="Jakarta" maxlength="50">
                                                        <?php } else { ?>
                                                            <?= $data['izin_city'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2"><?= $company['service_name'] ?></td>
                                                    <td class="field-col-6"><?= $izinCompany ?></td>
                                                    <?php if ($isNew) : ?>
                                                        <input type="hidden" type="text" name="izin_company" value="<?= $izinCompany ?>">
                                                    <?php endif; ?>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Situs Web</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input class="form-control" placeholder="Situs Web" type="text" name="izin_site" value="www.izin.co.id" maxlength="50">
                                                        <?php } else { ?>
                                                            <?= $data['izin_site'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Alamat</td>
                                                    <td class="field-col-6"><?= $izinAddress ?></td>
                                                    <?php if ($isNew) : ?>
                                                        <input type="hidden" type="text" name="izin_address" value="<?= $izinAddress ?>">
                                                    <?php endif; ?>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="title" style="background-color: <?= $company['primary_color'] ?>;">
                                            <p class="title-text">4&nbsp;&nbsp;Rincian Layanan</p>
                                        </div>
                                        <div class="field">
                                            <table class="table">
                                                <tr>
                                                    <td class="field-col-2">Layanan</td>
                                                    <td class="field-col-6"><?= $data['package_name'] ?></td>
                                                    <?php if ($isNew) : ?>
                                                        <input type="hidden" type="text" name="package_name" value="<?= $data['package_name'] ?>">
                                                    <?php endif; ?>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Harga</td>
                                                    <td class="field-col-6">Rp. <?= number_format($data['total_payment']) ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Potongan Harga</td>
                                                    <td class="field-col-6">-</td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Promo</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Promo" name="promo_name">
                                                        <?php } else { ?>
                                                            <input type="text" class="form-control" value="<?= ($data['promo_name'] == '') ? '-' : $data['promo_name'] ?>" readonly>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Total</td>
                                                    <td class="field-col-6">Rp. <?= number_format($data['total_payment']) ?></td>
                                                    <?php if ($isNew) : ?>
                                                        <input type="hidden" type="text" name="total_payment" value="<?= $data['total_payment'] ?>">
                                                    <?php endif; ?>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-8" colspan="2">
                                                        <?php if ($isNew) { ?>
                                                            <textarea name="detail" rows="8" cols="60" id="text">Rincian Layanan Yang Didapatkan &#13;- Akta Perubahan&#13;- SK Perubahan&#13;- SIUP&#13;- NIB</textarea>
                                                        <?php } else { ?>
                                                            <textarea rows="8" cols="60" readonly><?= $data['detail'] ?></textarea>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="title" style="background-color: <?= $company['primary_color'] ?>;">
                                            <p class="title-text">2&nbsp;&nbsp;Klien</p>
                                        </div>
                                        <div class="field">
                                            <table class="table">
                                                <tr>
                                                    <td class="field-col-2">Nama Perusahaan</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Company Name" name="client_company_name" maxlength="50">
                                                        <?php  } else { ?>
                                                            <?= $data['client_company_name'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Alamat Perusahaan</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Address Company" name="client_company_address" maxlength="200">
                                                        <?php } else { ?>
                                                            <?= $data['client_company_address'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Nama Perwakilan</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Nama Perwakilan" name="client_pic" maxlength="30">
                                                        <?php } else { ?>
                                                            <?= $data['client_pic'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Jabatan</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Jabatan" name="client_position" maxlength="20">
                                                        <?php } else { ?>
                                                            <?= $data['client_position'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Telp/Whatsapp</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Telp/Whatsapp" name="client_company_phone" maxlength="20">
                                                        <?php } else { ?>
                                                            <?= $data['client_company_phone'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Email</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Email" name="client_company_email" maxlength="50">
                                                        <?php } else { ?>
                                                            <?= $data['client_company_email'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="title" style="background-color: <?= $company['primary_color'] ?>;">
                                            <p class="title-text">5&nbsp;&nbsp;Layanan Tambahan</p>
                                        </div>
                                        <div class="field">
                                            <table class="table">
                                                <tr>
                                                    <td class="field-col-2">Layanan Tambahan</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <textarea placeholder="Additional Plan" name="additional_plan" rows="3" cols="43" maxlength="100"></textarea>
                                                        <?php } else { ?>
                                                            <textarea rows="3" cols="43" readonly><?= $data['additional_plan'] ?></textarea>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Harga</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="number" class="form-control" placeholder="Service Fee" name="additional_fee" min="0">
                                                        <?php } else { ?>
                                                            <?= ($data['additional_fee'] == '') ? '-' : $data['additional_fee'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Potongan Harga</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="number" class="form-control" placeholder="Discount" name="additional_discount" min="0">
                                                        <?php } else { ?>
                                                            <?= ($data['additional_discount'] == '') ? '-' : $data['additional_discount'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Promo</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="text" class="form-control" placeholder="Promo" name="additional_promo">
                                                        <?php } else { ?>
                                                            <?= ($data['additional_promo'] == '') ? '-' : $data['additional_promo'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Total</td>
                                                    <td class="field-col-6">
                                                        <?php if ($isNew) { ?>
                                                            <input type="number" class="form-control" placeholder="Paid Amount" name="additional_amount" min="0">
                                                        <?php } else { ?>
                                                            <?= ($data['additional_amount'] == '') ? '-' : $data['additional_amount'] ?>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-8" colspan="2">
                                                        <?php if ($isNew) { ?>
                                                            <textarea name="additional_detail" rows="8" cols="60">Rincian Layanan Yang Didapatkan &#13;- Akta Perubahan&#13;- SK Perubahan&#13;- SIUP&#13;- NIB</textarea>
                                                        <?php } else { ?>
                                                            <textarea rows="8" cols="60" readonly><?= $data['additional_detail'] ?></textarea>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="container">
                                        <div class="title" style="background-color: <?= $company['primary_color'] ?>;">
                                            <p class="title-text">3&nbsp;&nbsp;Bank</p>
                                        </div>
                                        <div class="field">
                                            <table class="table">
                                                <tr>
                                                    <td class="field-col-2">Bank</td>
                                                    <td class="field-col-2">Bank Central Asia</td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Pemilik Rekening</td>
                                                    <td class="field-col-2"><?= $izinCompany ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="field-col-2">Nomor Rekening</td>
                                                    <td class="field-col-2"><?= $izinAccount ?></td>
                                                    <?php if ($isNew) : ?>
                                                        <input type="hidden" type="text" name="izin_account" value="<?= $izinAccount ?>">
                                                    <?php endif; ?>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- body main -->

                                    <!-- footer -->
                                    <div class="footer">
                                        <p class="footer-text">
                                            Dengan telah menandatangani Perjanjian Layanan <?= $company['service_name'] ?>, Klien telah membaca, memahami dan menyetujui syarat dan ketentuan yang dilampirkan pada Perjanjian ini.
                                        </p>
                                    </div>
                                    <!-- footer -->

                                    <!-- footer box -->
                                    <div class="footer-box">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>Ditandatangani untuk dan atas nama klien</td>
                                                    <td style="padding-left: 100mm;">Ditandatangani untuk dan atas nama <?= $company['service_name'] ?></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <!-- client -->
                                                    <td>Nama : <?= ($isNew) ? '' : $data['client_pic'] ?></td>
                                                    <!-- izin -->
                                                    <td style="padding-left: 100mm;">Nama : <?= $company['pic_name'] ?></td>
                                                </tr>
                                                <tr>
                                                    <!-- client -->
                                                    <td>Jabatan : <?= ($isNew) ? '' : $data['client_position'] ?></td>
                                                    <!-- izin -->
                                                    <td style="padding-left: 100mm;">Jabatan : <?= $company['pic_position'] ?></td>
                                                </tr>
                                                <tr>
                                                    <!-- client -->
                                                    <td>Tanggal : <?= (!$isNew && $clientSigned) ? date('d M Y', strtotime($data['client_sign']['signed_date'])) : '' ?></td>
                                                    <!-- izin -->
                                                    <td style="padding-left: 100mm;">Tanggal : <?= (!$isNew && $izinSigned) ? date('d M Y', strtotime($data['izin_sign']['signed_date'])) : '' ?></td>
                                                </tr>
                                                <tr>
                                                    <!-- client -->
                                                    <td><?= (!$isNew && $clientSigned) ? "<img src='" . $data['client_sign']['base64_img'] . "' />" : "Tanda Tangan" ?></td>
                                                    <!-- izin -->
                                                    <td style="padding-left: 100mm;"><?= (!$isNew && $izinSigned) ? "<img src='" . $data['izin_sign']['base64_img'] . "' style='width:200px;'/>" : "Tanda Tangan" ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <!-- footer box -->

                                        <hr style="height: 8px; background-color: <?= $company['primary_color'] ?>;">

                                        <!-- end page -->
                                        <div class="end-page">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <td>A Business Of <b>IZIN.CO.ID</b></td>
                                                        <td style="padding-left: 134mm;"><?= explode("<br>", $company['address_info'])[0] ?></td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td style="padding-left: 134mm;"><?= explode("<br>", $company['address_info'])[1] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td style="padding-left: 134mm;"><?= explode("<br>", $company['address_info'])[2] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>T<span style='font-size:3px;'>&#9899;</span><?= $company['contact_info'] ?> E<span style='font-size:3px;'>&#9899;</span><?= $company['email_info'] ?></td>
                                                        <td style="padding-left: 134mm;"><?= explode("<br>", $company['address_info'])[3] ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- end page -->
                                    </div>
                                </div>
                            </div>
                            <?= ($isNew) ? '</form>' : '' ?>
                            <br>

                            <?php
                            if ($isNew) {
                                echo ButtonsCommon("commonbuttons", THEMES, "Save_i", $lang_3004, "SaveAgreement_" . $_SESSION["butts_Save"], "AddAgreement", $lang_3021);
                            } else {
                                echo ButtonsCommon("commonbuttons", THEMES, "Reset_i", $lang_3014, "ResendAgreement_" . $_SESSION["butts_Save"], "ResendAgreement");
                                echo ButtonsCommon("commonbuttons", THEMES, "Del_i", $lang_3011, "DeleteAgreement_" . $_SESSION["butts_Save"], "DeleteAgreement", $lang_3022);
                                if ($clientSigned && in_array($_SESSION['Jbtn'], ['Manager', 'Administrator'])) {
                                    echo ButtonsCommon("commonbuttons", THEMES, "Green_i", $lang_3019, "VerifyAgreement_" . $_SESSION["butts_Save"], "VerifyAgreement", $lang_3023);
                                } elseif (!$clientSigned) {
                                    echo ButtonsCommon("commonbuttons", THEMES, "PDF_i", $lang_3024, "DownloadBlankAgreement_" . $_SESSION["butts_Save"], "DownloadAgreement");
                                }
                            }
                            ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Form -->
    <?php } ?>
</div>

<?php if (!$isNew && !$clientSigned && !$izinSigned) : ?>
    <div class="container-fluid page__container">
        <div class="row no-gutters">
            <div class="col-lg-12 card-form__body card-body">
                <form action="" method="post" name="AddAgreementFile" id="AddAgreementFile" enctype="multipart/form-data">
                    <input type="hidden" name="task" />
                    <input type="hidden" value="<?= $_SESSION['AGREEMENT_POST']; ?>" name="agreement_id" />
                    <input type="hidden" value="<?= $data['inv_id']; ?>" name="inv_id" />
                    <input type="hidden" value="<?= $data['inv_num']; ?>" name="inv_num" />
                    <div class="form-group">
                        <label for="UpFl"><?= $lang_2041 ?></label>
                        <div class="custom-file">
                            <label for="UpFl" class="custom-file-label"><?php echo "Select " . $lang_2041 ?></label>
                            <input id="UpFl" name="UpFl" type="file" class="custom-file-input" accept="application/pdf" />
                        </div>
                        <script>
                            $(".custom-file-input").on("change", function(e) {
                                e.preventDefault;
                                var fileName = $(this).val().split("\\").pop(),
                                    pjg = fileName.length,
                                    ambil = 87,
                                    mulai = pjg - ambil;

                                if (pjg > ambil) {
                                    var shortText = '...' + jQuery.trim(fileName).substring(mulai);
                                } else {
                                    var shortText = fileName;
                                }
                                $(this).siblings(".custom-file-label").addClass("selected").html(shortText);
                            });
                        </script>
                    </div>
                </form>
                <div class="text-right mb-5">
                    <?= ButtonsCommon("commonbuttons", THEMES, "Upgrade_i", $lang_2042, "UploadFile_" . $_SESSION["butts_Save"], "AddAgreementFile", $lang_3020, "left") ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>