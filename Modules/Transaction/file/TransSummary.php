<?php
#$csent = "Select count(*) as jum, sum(total_due) as amm from ".$tblp."apps_invdet where tipe = 'SENT' and UNIX_TIMESTAMP(due_date) > ".time()."";
$csent = "Select count(*) as jum, sum(total_due) as amm from " . $tblp . "apps_invdet where tipe = 'SENT' and UNIX_TIMESTAMP(due_date) >= " . strtotime(date('Y-m-d', time()) . '00:00:00');
$rcsent = $dbs->getArr($csent);

$cover = "Select count(*) as jum, sum(total_due) as amm from " . $tblp . "apps_invdet where tipe = 'SENT' and UNIX_TIMESTAMP(due_date) < " . strtotime(date('Y-m-d', time()) . '00:00:00');
$rcover = $dbs->getArr($cover);

$cpaid = "Select count(*) as jum, sum(total_due) as amm from " . $tblp . "apps_invdet where tipe = 'PAID'";
$rcpaid = $dbs->getArr($cpaid);

$tinv = bcadd(bcadd($rcsent['jum'], $rcover['jum'], 0), $rcpaid['jum'], 0);
$tamm = bcadd(bcadd($rcsent['amm'], $rcover['amm'], 0), $rcpaid['amm'], 0);

?>
<div class="container-fluid page__container">
	<div class="row card-group-row">
		<div class="col-lg-12 col-md-12 ">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-md-2 flex">
							<h4 class="card-header__title"><strong>INVOICE STATUS</strong></h4>
						</div>
						<div class="col-md-5 flex">
							<h4 class="card-header__title flex m-0" style="float: right">NUMBER OF INVOICE</h4>
						</div>
						<div class="col-md-10 flex">
							<h4 class="card-header__title flex m-0" style="float: right">AMOUNT</h4>
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">
						<div class="col-md-2 flex">
							<h5 style="color:grey;">SENT</h5>
						</div>
						<div class="col-md-5 flex">
							<div style="float: right">
								<h5 style="color:grey;"><?php echo c_curr($rcsent['jum'], ",", ".", $min_sign = "mark") ?></h5>
							</div>
						</div>
						<div class="col-md-10 flex">
							<div style="float: right">
								<h5 style="color:grey;">IDR <?php echo c_curr($rcsent['amm'], ",", ".", $min_sign = "mark") ?></h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 flex">
							<h5 style="color:#CC0000;">OVER DUE</h5>
						</div>
						<div class="col-md-5 flex">
							<div style="float: right">
								<h5 style="color:#CC0000;"><?php echo c_curr($rcover['jum'], ",", ".", $min_sign = "mark") ?></h5>
							</div>
						</div>
						<div class="col-md-10 flex">
							<div style="float: right; color:#CC0000;">
								<h5 style="color:#CC0000;">IDR <?php echo c_curr($rcover['amm'], ",", ".", $min_sign = "mark") ?></h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 flex">
							<h5 style="color:#009900;">PAID</h5>
						</div>
						<div class="col-md-5 flex">
							<div style="float: right">
								<h5 style="color:#009900;"><?php echo  c_curr($rcpaid['jum'], ",", ".", $min_sign = "mark") ?></h5>
							</div>
						</div>
						<div class="col-md-10 flex">
							<div style="float: right; color:#CC0000;">
								<h5 style="color:#009900;">IDR <?php echo c_curr($rcpaid['amm'], ",", ".", $min_sign = "mark") ?></h5>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-auto flex">
							<hr style="border-top: 1px dotted #555 !important; margin-bottom:15px !important; margin-top:5px !important;" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-2 flex">
							<h4>TOTAL</h4>
						</div>
						<div class="col-md-5 flex">
							<div style="float: right">
								<h4><?php echo  c_curr($tinv, ",", ".", $min_sign = "mark") ?></h4>
							</div>
						</div>
						<div class="col-md-10 flex">
							<div style="float: right; color:#CC0000;">
								<h4>IDR <?php echo  c_curr($tamm, ",", ".", $min_sign = "mark") ?></h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>