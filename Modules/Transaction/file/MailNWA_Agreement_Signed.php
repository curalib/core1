<?php

require_once "system/SendEmail.php";

$Email_Subject = "Agreement Invoice No. $invnum";

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Body = "Dear Client,
        <br><br>
        We have confirmed your agreement and will proceed to process your company legal documents.
        <br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";
} else {
    $Email_Body = "Dear Valued Customer,
        <br><br>
        Terima kasih atas kepercayaan Bapak dan Ibu dalam menggunakan layanan IZIN.co.id.
        <br>
        Berikut ini kami lampirkan agreement layanan IZIN yang telah di tandatangani oleh Bapak/Ibu $clientPic.
        <br><br>
        Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk Anda.
        <br><br><br><br>
        Salam,<br>
        <strong>IZIN.co.id</strong>
        <br><br>
        <hr>
        <em>This is an automated message please do not reply directly to this email/whatsapp. For further information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";

    $WA_Message = "Dear Valued Customer,

Terima kasih atas kepercayaan Bapak dan Ibu dalam menggunakan layanan IZIN.co.id.

Berikut ini kami lampirkan agreement layanan IZIN yang telah di tandatangani oleh Bapak/Ibu $clientPic yang dapat dilihat pada email yang dikirimkan dari info@izin.co.id ke email Bapak/Ibu.

Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk Anda.

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62822-9998-0011_/_cs@izin.co.id_
";
}

$mail->IsSMTP();
$mail->SMTPDebug = EMAIL_SMTPDEBUG;
$mail->Host = EMAIL_HOST;
$mail->SMTPAuth = EMAIL_SMTPAUTH;
$mail->SMTPSecure = EMAIL_SMTPSECURE;
$mail->Port = EMAIL_PORT;
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->IsHTML(EMAIL_ISHTML);
$mail->Username = EMAIL_USERNAME;
$mail->Password = EMAIL_PASSWORD;
$mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
$mail->AddAddress($Email_Address);
$mail->AddCC(EMAIL_CC);
$mail->Subject = $Email_Subject;
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
$mail->addAttachment($file);
if ($mail->send()) {
    $AlertMess .= "<br> Email message sent (to: $Email_Address)";
} else {
    $AlertMess .= "<br> Fail sending email message (problem: " . $mail->ErrorInfo . ")";
}
