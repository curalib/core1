<?php

require_once "system/SendEmail.php";

if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
    $Email_Subject = "Payment Received Invoice No. $invnum";
    $Email_Body = "Dear Client,
        <br><br>
        Your payment has been confirmed. For the next step, we will process the legal document for your company as soon as possible.
        <br><br>
        Thank you for your trust in using Invest in Asia services. We will always improve our services for your convenience and ease of doing business.
        <br><br><br><br>
        Warm regards,<br>
        <strong>Invest In Asia team</strong>
        <br><br>";
} else {
    $Email_Subject = "Pembayaran Kami Terima Invoice No. $invnum";
    $Email_Body = "Dear Client,
        <br><br>
        Terima kasih atas pembayaran yang telah dilakukan. Pembayaran telah kami terima dengan baik. Untuk selanjutnya kami akan segera memproses pendirian perusahaan/pembuatan perijinan perusahaan anda.
        <br><br>
        Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk anda.
        <br><br><br><br>
        Salam,<br>
        <strong>IZIN.co.id</strong>
        <br><br>
        <hr>
        <em>This is an automated message please do not reply directly to this email/whatsapp. For further
        information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";
}

$mail->IsSMTP();
$mail->SMTPDebug = EMAIL_SMTPDEBUG;
$mail->Host = EMAIL_HOST;
$mail->SMTPAuth = EMAIL_SMTPAUTH;
$mail->SMTPSecure = EMAIL_SMTPSECURE;
$mail->Port = EMAIL_PORT;
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->IsHTML(EMAIL_ISHTML);
$mail->Username = EMAIL_USERNAME;
$mail->Password = EMAIL_PASSWORD;
$mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
$mail->AddCC(EMAIL_CC);
$mail->AddCC("contact@huqum.id");
$mail->AddAddress($invmail, $invpic);
$mail->Subject = $Email_Subject;
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
$mail->addAttachment($file);
if ($mail->send()) {
    $MailSendStatus = 'Email sent successfully to ' . $invmail;
} else {
    $MailSendStatus = 'Email not sent!';
}