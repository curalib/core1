<?php

if ($data['izin_company'] === 'PT INVESTASI INDO ASIA') {
    $company['background'] = convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/watermark-iia.png');
    $company['logo'] = '<img src="' . convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/agree-IIA.png') . '" height="60px">';
    $company['service_name'] = 'InvestInAsia';
    $company['tagline_name'] = '';
    $company['pic_name'] = 'Endah Wahyuningsih';
    $company['pic_position'] = 'Operation Manager';
    $company['primary_color'] = '#223666';
    $company['secondary_color'] = '#27afa7';
    $company['contact_info'] = '+6221 5091 9500';
    $company['email_info'] = 'hello@investinasia.id';
    $company['address_info'] = 'District 8, Treasury Tower Lt. 6 <br>
    Unit F, Senayan, Kebayoran Baru, <br>
    Kota Administrasi Jakarta Selatan, <br>
    DKI Jakarta <br>';
} else {
    $company['background'] = convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/watermark.jpg');
    $company['logo'] = '<img src="' . convert_img_to_base64($_SESSION["ROOT_DIR"] . '/templates/Default/images/agree-IZIN.png') . '" height="60px">';
    $company['service_name'] = 'IZIN.co.id';
    $company['tagline_name'] = 'START YOUR BUSSINESS RIGHT';
    $company['pic_name'] = 'Otty Yuniarty Y';
    $company['pic_position'] = 'General Manager';
    $company['primary_color'] = 'green';
    $company['secondary_color'] = 'green';
    $company['contact_info'] = '+6221 2955 3500';
    $company['email_info'] = 'cs@izin.co.id';
    $company['address_info'] = 'ATRIA @SUDIRMAN Lt. 23 <br>
    Jl. Jenderal Sudirman No.Kav. 33A, RT.3/RW.2, Karet <br>
    Tengsin, Kecamatan Tanah Abang, Kota Jakarta Pusat, <br>
    Daerah Khusus Ibukota Jakarta, 10220 <br>';
}

$file_html = "<!DOCTYPE html>
<html>
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<style>
    @page {
        margin: 0px;
    }

    .container {
        max-width: 960px;
    }

    body {
        padding: 25px;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-image: url('" . $company['background'] . "');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
    }

    .border-top {
        border-top: 1px solid #e5e5e5;
    }

    .border-bottom {
        border-bottom: 1px solid #e5e5e5;
    }

    .border-top-gray {
        border-top-color: #adb5bd;
    }

    .box-shadow {
        box-shadow: 0 .25rem .75rem rgba(0, 0, 0, .05);
    }

    .lh-condensed {
        line-height: 1.25;
    }

    .signature {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
        height: 80px;
    }

    .f14 {
        font-size: 14px;
    }

    .f13 {
        font-size: 13px;
    }

    .f12 {
        font-size: 12px;
    }

    .f11 {
        font-size: 11px;
    }

    .f10 {
        font-size: 10px;
    }

    .f8 {
        font-size: 8px;
    }

    .f-theme {
        color: #0dac50;
    }

    .b-theme {
        color: white;
        background-color: #0dac50;
    }

    .head-br {
        vertical-align: top;
        border-top-left-radius: 5px;
        border-bottom-right-radius: 5px;
        line-height: 15px;
        font-family: 'Tangerine';
    }

    .bg-gray {
        background-color: rgb(50, 50, 50, 0.1);
    }

    .f-gray {
        color: gray;
    }

    .border-btm td {
        border-bottom: 1px solid rgb(100, 100, 100, 0.5);
    }

    .wrapper {
        position: relative;
        width: 100%;
        height: 100%;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .id {
        font-style: normal;
        font-size: 10px;
    }

    .en {
        font-weight: lighter;
        font-style: italic;
        font-size: 9.3px;
        color: #696969;
    }

    .en-terms {
        font-style: italic;
    }

    ol.k {
        list-style-type: lower-alpha;
    }

    ol.c {
        list-style-type: decimal;
    }

    .page_break {
        page-break-before: always;
    }

    div .tos td {
        vertical-align: top;
        text-align: justify;
        text-justify: inter-word;
    }

    div #page1 {
        /* padding-bottom: 100px; */
    }
</style>
</head>
<body>
<div id='page1'>
    <table width='100%'>
        <tr>
            <td width='75%'>
                <span class='f-theme' style='font-size: 22px; color: " . $company['secondary_color'] . "'>Selamat Datang di <b>" . $company['service_name'] . "</b> / </span>
                <span class='f-theme en' style='font-size: 16px;'>Welcome to <b>" . $company['service_name'] . "</b></span>
                <br><br>
            </td>
            <td style='text-align: right;' width='25%' rowspan='2'>
                <div style='text-align: center;'>
                    " . $company['logo'] . "
                    <br>
                    <span class='f11' style='letter-spacing: -1 px;'><b>" . $company['tagline_name'] . "</b></span>
                </div>
            </td>
        </tr>
        <tr>
            <td style='line-height: 1em;'>
                <span class='f14 id'><b>Perjanjian Layanan " . $company['service_name'] . " dengan Klien di bawah ini:</b></span>
                <br><span class='f12 en'>This Service Agreement is made between " . $company['service_name'] . " and the Client below:</span>
            </td>
        </tr>
    </table>
    <table width='100%' class='f10'>
        <tr>
            <td width='45%' colspan='2'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . ";'>
                    &nbsp;&nbsp;&nbsp;<b>I. " . $company['service_name'] . "</b>
                </div>
            </td>
            <td width='3%'></td>
            <td width='52%' colspan='2'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . ";'>
                    &nbsp;&nbsp;&nbsp;<b>IV. RINCIAN LAYANAN /</b> <i>Service Detail</i>
                </div>
            </td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Kota</span>
                <br><span class='en'>City</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['izin_city'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Layanan</span>
                <br><span class='en'>Service</span>
            </td>
            <td class='bg-gray' width='37%'>" . $data['package_name'] . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>" . $company['service_name'] . "</td>
            <td class='bg-gray' width='30%'>" . $data['izin_company'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Harga</span>
                <br><span class='en'>Price</span>
            </td>
            <td class='bg-gray' width='37%'>Rp " . number_format($data['total_due']) . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Situs Web</span>
                <br><span class='en'>Website</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['izin_site'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Potongan Harga</span>
                <br><span class='en'>Discount</span>
            </td>
            <td class='bg-gray' width='37%'>-</td>
        </tr>
        <tr class='border-btm' valign='top'>
            <td width='15%' rowspan='3'>
                <span class='id'>Alamat</span>
                <br><span class='en'>Address</span>
            </td>
            <td class='bg-gray' rowspan='3' width='30%'>" . $data['izin_address'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Promo</span>
                <br><span class='en'>Promo</span>
            </td>
            <td class='bg-gray' width='37%'>" . $data['promo_name'] . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Total</span>
                <br><span class='en'>Total</span>
            </td>
            <td class='bg-gray' width='37%'>Rp " . number_format($data['total_payment']) . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <span class='id'><b>Rincian Layanan Yang Didapatkan</b></span> /
                <span class='en'>Details of services</span>
                <div class='bg-gray' valign='top'><small>" . $data['detail'] . "</small></div>
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . ";'>
                    &nbsp;&nbsp;&nbsp;<b>II. KLIEN /</b> <i>Client</i>
                </div>
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . ";'>
                    &nbsp;&nbsp;&nbsp;<b>V. LAYANAN TAMBAHAN /</b> <i>Additional Service</i>
                </div>
            </td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Nama Perusahaan</span>
                <br><span class='en'>Company Name</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['client_company_name'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Layanan Tambahan</span>
                <br><span class='en'>Additional Service</span>
            </td>
            <td class='bg-gray' width='37%'>" . $data['additional_plan'] . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%' valign='top'>
                <span class='id'>Alamat Perusahaan</span>
                <br><span class='en'>Company Address</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['client_company_address'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Harga</span>
                <br><span class='en'>Price</span>
            </td>
            <td class='bg-gray' width='37%'>" . $additional_fee . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Nama Perwakilan</span>
                <br><span class='en'>Representative Name</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['client_pic'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Potongan Harga</span>
                <br><span class='en'>Discount</span>
            </td>
            <td class='bg-gray' width='37%'>" . $additional_discount . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Jabatan</span>
                <br><span class='en'>Title</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['client_position'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Promo</span>
                <br><span class='en'>Promo</span>
            </td>
            <td class='bg-gray' width='37%'>" . $data['additional_promo'] . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Telp/Whatsapp</span>
                <br><span class='en'>Phone/Whatsapp Number</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['client_company_phone'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td width='15%'>
                <span class='id'>Total</span>
                <br><span class='en'>Total</span>
            </td>
            <td class='bg-gray' width='37%'>" . $additional_amount . "</td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Surel</span>
                <br><span class='en'>Email</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['client_company_email'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <span class='id'><b>Rincian Layanan Yang Didapatkan</b></span> /
                <span class='en'>Details of services</span>
                <div class='bg-gray' valign='top'><small>" . $data['additional_detail'] . "</small></div>
            </td>
        </tr>
        <tr>
            <td colspan='2'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . ";'>
                    &nbsp;&nbsp;&nbsp;<b>III. BANK /</b> <i>Bank</i>
                </div>
            </td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Bank " . $company['service_name'] . "</span>
                <br><span class='en'>" . $company['service_name'] . " Bank</span>
            </td>
            <td class='bg-gray' width='30%'>Bank Central Asia</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Pemilik Rekening</span>
                <br><span class='en'>Account Name</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['izin_company'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
        </tr>
        <tr class='border-btm'>
            <td width='15%'>
                <span class='id'>Nomor Rekening</span>
                <br><span class='en'>Account Number</span>
            </td>
            <td class='bg-gray' width='30%'>" . $data['izin_account'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
        </tr>
        <tr>
            <td colspan='5'>&nbsp;</td>
        </tr>
        <tr>
            <td colspan='5'>
                <span class='id'>Dengan telah menandatangani Perjanjian Layanan " . $company['service_name'] . ", Klien telah membaca, memahami dan menyetujui syarat dan ketentuan yang dilampirkan pada Perjanjian ini.</span>
                <br><span class='en'>Upon signing " . $company['service_name'] . " Service Agreement, Client has read, understood and agreed to the terms and conditions stated in this Agreement.</span>
            </td>
        </tr>
        <tr>
            <td colspan='5'>
                <table width='100%'>
                    <tr>
                        <td colspan='2'>
                            <span class='id'><b>Ditandatangani untuk dan atas nama Klien</b></span>
                            <br><span class='en'>Signed for and on behalf of the Client</span>
                        </td>
                        <td width='3%' style='border-bottom: 0px solid transparent;'></td>
                        <td colspan='2'>
                            <span class='id'><b>Ditandatangani untuk dan atas nama " . $company['service_name'] . "</b></span>
                            <br><span class='en'>Signed for and on behalf of " . $company['service_name'] . "</span>
                        </td>
                    </tr>
                    <tr>
                        <td width='12%'>
                            <span class='id'>Nama</span>
                            <br><span class='en'>Name</span>
                        </td>
                        <td class='bg-gray' width='35%'>" . $data['client_pic'] . "</td>
                        <td width='3%' style='border-bottom: 0px solid transparent;'></td>
                        <td width='12%'>
                            <span class='id'>Nama</span>
                            <br><span class='en'>Name</span>
                        </td>
                        <td class='bg-gray' width='35%'>" . $company['pic_name'] . "</td>
                    </tr>
                    <tr>
                        <td width='12%'>
                            <span class='id'>Jabatan</span>
                            <br><span class='en'>Position</span>
                        </td>
                        <td class='bg-gray' width='35%'>" . $data['client_position'] . "</td>
                        <td width='3%' style='border-bottom: 0px solid transparent;'></td>
                        <td width='12%'>
                            <span class='id'>Jabatan</span>
                            <br><span class='en'>Position</span>
                        </td>
                        <td class='bg-gray' width='35%'>" . $company['pic_position'] . "</td>
                    </tr>
                    <tr>
                        <td width='12%'>
                            <span class='id'>Tanggal</span>
                            <br><span class='en'>Date</span>
                        </td>
                        <td class='bg-gray' width='35%'>" . (is_null($clientSign) ? '' : date('d M Y', strtotime($clientSign['signed_date']))) . "</td>
                        <td width='3%' style='border-bottom: 0px solid transparent;'></td>
                        <td width='12%'>
                            <span class='id'>Tanggal</span>
                            <br><span class='en'>Date</span>
                        </td>
                        <td class='bg-gray' width='35%'>" . (is_null($izinSign) ? '' : date('d M Y', strtotime($izinSign['signed_date']))) . "</td>
                    </tr>
                    <tr>
                        <td colspan='5'>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width='12%'>
                            <span class='id'>Tanda Tangan</span>
                            <br><span class='en'>Signature</span>
                        </td>
                        <td width='35%' class='signature'>" . (is_null($clientSign) ? '' : $clientSign['base64_img']) . "</td>
                        <td width='3%' style='border-bottom: 0px solid transparent;'></td>
                        <td width='12%'>
                            <span class='id'>Tanda Tangan</span>
                            <br><span class='en'>Signature</span>
                        </td>
                        <td width='35%' class='signature'>" . (is_null($izinSign) ? '' : $izinSign['base64_img']) . "</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan='5'>
                &nbsp;<br>
                <table width='100%'>
                    <tr>
                        <td width='5%'>&nbsp; </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width='5%'>&nbsp; </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr> " . $spacing . "
        <tr>
            <td colspan='5'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . ";'><b> &nbsp;</b></div>
            </td>
        </tr>
        <tr class='f-gray'>
            <td colspan='2'>" . (($company['tagline_name'] == '') ? '' : 'A Business of IZIN.CO.ID') . "<br><br>T • " . $company['contact_info'] . " E • " . $company['email_info'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <div style='right: 50px; position: absolute;'>
                    " . explode("<br>", $company['address_info'])[0] . "<br>
                    " . explode("<br>", $company['address_info'])[1] . "<br>
                    " . explode("<br>", $company['address_info'])[2] . "<br>
                    " . explode("<br>", $company['address_info'])[3] . "
                </div>
            </td>
        </tr>
    </table>
</div>
<div class='page_break'></div>
<div id='page2'>
    <table width='100%'>
        <tr>
            <td width='75%'>
                <span class='f-theme' style='font-size: 22px; color: " . $company['secondary_color'] . ";'>Perjanjian Layanan <b>" . $company['service_name'] . "</b></span>
                <br><span class='f-theme en' style='font-size: 16px;'>Terms of Use Agreement <b>" . $company['service_name'] . "</b></span>
                <br>
            </td>
            <td style='text-align: right;' width='25%'>
                <div style='text-align: center;'>
                    " . $company['logo'] . "
                    <br>
                    <span class='f11' style='letter-spacing: -1 px;'><b>" . $company['tagline_name'] . "</b></span>
                </div>
            </td>
        </tr>
    </table>
    <table width='100%' class='f10 tos'>
        <tr>
            <td colspan='2'><b>" . $company['service_name'] . " dan Klien telah setuju dan sepakat mengenai syarat dan ketentuan dalam perjanjian ini yang meliputi:</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>" . $company['service_name'] . " and the Client have agreed with the terms and conditions of this agreement which include:</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.0 JAM OPERASIONAL</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.0 OPERATING HOURS</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>Jam operasional " . $company['service_name'] . " mulai pukul 09:00 sampai dengan pukul 18:00, hari Senin sampai dengan Jumat dan mengikuti hari libur nasional sesuai dengan hukum Indonesia yang berlaku.</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>The operating hours of " . $company['service_name'] . " start from 09:00 to 18:00, Monday to Friday and will be closed on National holidays, in accordance with the Indonesian Law that applies.</td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.1 KOMUNIKASI</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.1 COMMUNICATION</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>
                Saluran komunikasi utama " . $company['service_name'] . " adalah melalui email. Email tersebut akan dikirimkan melalui <i>corporate email server</i> " . $company['service_name'] . " kepada alamat email yang telah diberikan oleh Klien dalam formulir pendaftaran. Klien wajib memberitahukan apabila terdapat perubahan email dikemudian hari.
                <br><br>
                Dalam hal Klien melakukan pergantian/perubahan alamat email, Klien wajib memberitahukan selambat-lambatnya 5 (lima) hari kerja sejak pergantian/perubahan dengan cara mengirimkan informasi secara tertulis ke email " . $company['email_info'] . ".
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                " . $company['service_name'] . " primary communication channel are through email. The email will be sent through " . $company['service_name'] . " corporate email server to the email address that has been provided by the Client on the registration form. Client is obligated to inform " . $company['service_name'] . " if there will be any changes in the email address.
                <br><br>
                In the event that the Client makes any change/change of email address, the Client is obliged to notify no later than 5 (five) working days from the change, by sending written information to email " . $company['email_info'] . ".
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.2 LINGKUP PEKERJAAN</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.2 SCOPE OF WORK</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>
                " . $company['service_name'] . " memberikan layanan pengurusan legalitas, pendaftaran Hak atas Kekayaan Intelektual (HaKI), layanan perpajakan dan/atau layanan perizinan dan pendaftaran sesuai dengan yang disepakati dengan Klien (\"Jasa\") sebagaimana tercantum dalam Rincian Layanan.
                <br><br>
                " . $company['service_name'] . " tidak bertanggung jawab atas tertundanya pengurusan dan pengerjaan jasa yang diberikan akibat keterlambatan Klien dalam memenuhi persyaratan informasi ataupun dokumen, adanya perubahan peraturan pemerintah, gangguan pada sistem layanan pemerintah atau hal-hal lain yang diluar dari kekuasaan " . $company['service_name'] . ".
                <br><br>
                Layanan " . $company['service_name'] . " dikerjakan oleh konsultan yang berpengalaman serta profesional dibidangnya sesuai dengan data yang diberikan oleh Klien.
                <br><br>
                IZIN memiliki layanan <i>Tracking & Secure Cloud Storage</i> untuk memudahkan Klien mengakses progres pengurusan Jasa. Untuk menikmati layanan <i>Tracking & Secure Cloud Storage</i> " . $company['service_name'] . ", harap diperhatikan ketentuan-ketentuan sebagai berikut:
                <ol class=\"k\">
                    <li>Klien memahami bahwa penggunaan layanan <i>Tracking & Secure Cloud Storage</i> hanya diperuntukkan pada klien " . $company['service_name'] . " yang sudah melakukan kesepakatan dan pembayaran layanan.</li>
                    <li>Progres pengerjaan Layanan di dalam <i>Tracking & Secure Cloud Storage</i> akan diperbaharui sesuai dengan proses pengurusan perizinan.</li>
                    <li>Akses <i>Tracking & Secure Cloud Storage</i> hanya untuk diberikan pada pihak atau PIC yang berwenang atas perusahaan yang diurus perizinannya oleh tim " . $company['service_name'] . ".</li>
                    <li>Klien memahami bahwa akses <i>Tracking & Secure Cloud Storage</i> adalah data yang rahasia dan wajib dijaga dengan baik. Klien untuk tidak memberikannya kepada pihak lain yang dirasa tidak memiliki kepentingan yang cukup atas perusahaan Klien.</li>
                    <li>Dalam hal Klien membutuhkan perubahan atau reset password pada akses <i>Tracking & Secure Cloud Storage</i>, Klien dapat mengajukan permohonan tertulis melalui email " . $company['email_info'] . " dengan menyertakan identitas dan kredensial yang dibutuhkan.</li>
                    <li>Kata Sandi akan diperbaharui secara otomatis dari sistem apabila Klien menggunakan kembali layanan pengurusan perizinan " . $company['service_name'] . " untuk mendirikan perusahaan baru.</li>
                    <li>" . $company['service_name'] . " tidak bertanggungjawab terhadap kelalaian Klien dalam menjaga kerahasiaan akses <i>Tracking & Secure Cloud Storage</i>.</li>
                </ol>
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                " . $company['service_name'] . " provides legal services, registration of Intellectual Property Rights (HaKI), tax services and/or licensing and registration services as agreed with the Client (\"Services\") as stated in the Service Detail.
                <br><br>
                " . $company['service_name'] . " is not responsible for any delays in the processing and execution of services provided, due to Client delays in fulfilling information or document requirements, changes in government regulations, disruptions to the government service system or other things that are beyond the control of " . $company['service_name'] . ".
                <br><br>
                " . $company['service_name'] . " Services are carried out by consultants who are experienced and professional in their field in accordance with the data provided by the Client.
                <br><br>
                IZIN has Tracking & Secure Cloud Storage services to make it easier for Clients to access the progress of the Service. To enjoy " . $company['service_name'] . " Tracking & Secure Cloud Storage services, please pay attention to the following conditions:
                <ol class=\"k\">
                    <li>The Client understands that the use of the Tracking & Secure Cloud Storage service is only intended for " . $company['service_name'] . " Clients who have signed the Agreement and paid for the Service.</li>
                    <li>Any kind of updates regarding the Services in Tracking & Secure Cloud Storage will be updated according to the licensing process or working progress.</li>
                    <li>Tracking & Secure Cloud Storage access is only to be given to authorized parties or appointed PICs of the Client's company.</li>
                    <li>The Client understands that Tracking and Secure Cloud Storage access is confidential data and must be safeguarded properly. Clients should not give it to any other parties who they feel do not have sufficient interest in the Client's company.</li>
                    <li>In the event that the Client requires a change or reset of the password for Tracking & Secure Cloud Storage access, the Client can submit a written request via email " . $company['email_info'] . " by including the required identity and credentials.</li>
                    <li>The password will be updated automatically from the system if the Client uses the " . $company['service_name'] . " licensing service again to establish a new company.</li>
                    <li>" . $company['service_name'] . " is not responsible for the Client's negligence in maintaining the confidentiality of Tracking & Secure Cloud Storage access.</li>
                </ol>
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.3 PEMBAYARAN</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.3 PAYMENT</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2' colspan='2'>
                Klien sepakat untuk melakukan pelunasan pembayaran sebelum dimulainya pengurusan atau pengerjaan Jasa. Jasa baru dapat diproses setelah pembayaran Jasa diterima oleh " . $company['service_name'] . " dan Klien telah menyerahkan dokumen persyaratan secara lengkap dan jelas. Biaya yang telah dibayarkan oleh Klien menjadi hak milik " . $company['service_name'] . ". Klien tidak dapat mengajukan permohonan pengembalian pembayaran apabila pengakhiran atau pembatalan dilakukan secara sukarela ataupun disebabkan hal lain yang bukan tanggung jawab ataupun di luar kekuasaan " . $company['service_name'] . ".
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                The client agrees to make payment in full before the start of processing or carrying out the Services. New services can be processed after payment for services is received by " . $company['service_name'] . " and the client has submitted the required documents completely and clearly. Fees that have been paid by the Client become the property of " . $company['service_name'] . ". Clients cannot submit a request for a refund if the termination or cancellation is done voluntarily or due to other reasons that are not the responsibility or beyond the control of " . $company['service_name'] . ".
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.4 PEMBATASAN TANGGUNG JAWAB</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.4 LIMITATION OF LIABILITY</b></td>
        </tr>
        <tr class=''>
            <td colspan='2'>
                " . $company['service_name'] . " bekerjasama dengan konsultan dan/atau pengemban profesi yang berhubungan dengan layanan Jasa yang berpengalaman di bidangnya dan dapat membantu memberikan rekomendasi dan/atau saran terbaik sesuai dengan peraturan yang berlaku berdasarkan informasi yang diberikan oleh Klien. " . $company['service_name'] . " dapat bertindak sebagai perantara yang membantu memberikan edukasi kepada Klien terkait Jasa dan proses yang berhubungan dengan itu.
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                " . $company['service_name'] . " works with consultants and/or professional providers related to services who are experienced in their fields and can help provide the best recommendations and/or suggestions in accordance with applicable regulations based on information provided by the Client. " . $company['service_name'] . " can act as an intermediary to help provide education to Clients regarding Services and related processes.
            </td>
        </tr>
        <tr>
            <td colspan='5'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . " ;'><b> &nbsp;</b></div>
            </td>
        </tr>
        <tr class='f-gray'>
            <td colspan='2'>" . (($company['tagline_name'] == '') ? '' : 'A Business of IZIN.CO.ID') . "<br><br>T • " . $company['contact_info'] . " E • " . $company['email_info'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <div style='right: 50px; position: absolute;'>
                    " . explode("<br>", $company['address_info'])[0] . "<br>
                    " . explode("<br>", $company['address_info'])[1] . "<br>
                    " . explode("<br>", $company['address_info'])[2] . "<br>
                    " . explode("<br>", $company['address_info'])[3] . "
                </div>
            </td>
        </tr>
    </table>
</div>
<div class='page_break'></div>
<div id='page3'>
    <table width='100%'>
        <tr>
            <td width='75%'>
                <span class='f-theme' style='font-size: 22px; color: " . $company['secondary_color'] . ";'>Perjanjian Layanan <b>" . $company['service_name'] . "</b></span>
                <br><span class='f-theme en' style='font-size: 16px;'>Terms of Use Agreement <b>" . $company['service_name'] . "</b></span>
                <br>
            </td>
            <td style='text-align: right;' width='25%'>
                <div style='text-align: center;'>
                    " . $company['logo'] . "
                    <br>
                    <span class='f11' style='letter-spacing: -1 px;'><b>" . $company['tagline_name'] . "</b></span>
                </div>
            </td>
        </tr>
    </table>
    <table width='100%' class='f10 tos'>
        <tr class='border-btm'>
            <td colspan='2'>
                Segala keputusan dan permintaan Klien sehubungan layanan Jasa yang digunakan diluar dari kewenangan dan tanggung jawab " . $company['service_name'] . ".
                <br><br>
                " . $company['service_name'] . " hanya bertanggung jawab sampai dengan terpenuhinya pengurusan dan pengerjaan Jasa dengan detail sebagaimana tercantum pada Rincian Layanan dan Layanan Tambahan. Apabila pengurusan dan pengerjaan Jasa tersebut telah terpenuhi, maka berakhir pula keberlakuan Perjanjian ini.
                <br><br>
                " . $company['service_name'] . " tidak bertanggung jawab sebagai penjamin baik secara lisan ataupun tulisan kepada Klien ataupun pihak ketiga atas keaslian dokumen yang diberikan oleh Klien.
                <br><br>
                Klien dilarang untuk memberikan keterangan yang tidak sebenarnya, seperti termasuk namun tidak terbatas dalam hal: memberikan data identitas palsu, menggunakan data pribadi pihak lain yang diperoleh secara tidak sah (diperoleh dengan cara-cara yang tidak sesuai dengan hukum atau dari sumber lain yang tidak diperbolehkan), dan/atau memalsukan tandatangan baik berupa tanda tangan elektronik ataupun tanda tangan basah, serta hal-hal lain yang tidak dalam kapasitas dan kewenangan dari " . $company['service_name'] . " untuk melakukan verifikasi atas kebenaran atau keaslian data yang diberikan. Klien menjamin untuk membebaskan " . $company['service_name'] . " dari segala akibat hukum yang timbul berupa tuntunan/klaim dari pihak lain/ ketiga sehubungan dengan pelanggaran dari ketentuan ini.
                <br><br>
                " . $company['service_name'] . " tidak bertanggung jawab atas hal-hal yang diluar kekuasaan dan wewenang " . $company['service_name'] . " termasuk kesalahan pelaporan dan/atau perhitungan pajak yang berasal dari kekeliruan data/dokumen/keterangan lisan ataupun tulisan yang diberikan oleh Klien, tunggakan pajak, pemeriksaan laporan keuangan dan pajak, perubahan kebijakan pemerintah/instansi yang terkait, serta pelanggaran hukum baik secara perdata maupun pidana yang dilakukan oleh Klien.
                <br><br>
                " . $company['service_name'] . " hanya bertanggung jawab sehubungan dengan pelaksanaan pemberian Jasa berdasarkan informasi dan data yang diberikan oleh Klien untuk memenuhi kewajiban dalam Perjanjian ini dan tidak terikat dengan aktivitas usaha ataupun kegiatan Klien yang tidak berhubungan dengan Perjanjian ini.
                <br><br>
                Sehubungan dengan layanan Jasa berhubungan dengan pelaporan pajak, Klien berkewajiban untuk menyerahkan dokumen yang diperlukan dengan batas waktu sebagai berikut:
                <ol class=\"c\">
                    <li>Untuk perhitungan guna pembayaran dan pelaporan PPH, dokumen wajib diserahkan paling lambat sebelum tanggal 1 pada bulan berikutnya.</li>
                    <li>Untuk perhitungan guna pembayaran dan pelaporan pajak PPN, dokumen wajib diserahkan paling lambat sebelum tanggal 10 pada bulan berikutnya.</li>
                    <li>Untuk perhitungan guna pembayaran dan pelaporan pajak SPT orang pribadi tahunan, Klien wajib menyerahkan dokumen sebelum tanggal 1 Maret tahun berikutnya.</li>
                    <li>Untuk perhitungan guna pembayaran dan pelaporan pajak SPT Perusahaan tahunan, Klien wajib menyerahkan dokumen sebelum tanggal 1 April tahun berikutnya.</li>
                </ol>
                " . $company['service_name'] . " tidak bertanggung jawab atas hal-hal di luar kekuasaan dan wewenang " . $company['service_name'] . " termasuk perubahan kebijakan pemerintah/instansi terkait serta pelanggaran perdata dan pidana yang dilakukan oleh Klien. " . $company['service_name'] . " hanya bertanggung jawab untuk memenuhi kebutuhan Klien sebatas jasa yang disediakan sesuai dengan Perjanjian ini.
                <br><br>
                Klien dilarang untuk mengalihkan, memindahkan atau menjaminkan segala hak atau kewajiban yang timbul berkaitan dengan Perjanjian ini kepada pihak manapun dengan atau tanpa sepengetahuan " . $company['service_name'] . ".
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                All Client's decisions and requests regarding the Services are outside the authority and responsibility of " . $company['service_name'] . ".
                <br><br>
                " . $company['service_name'] . "is only responsible for completing the Services in detail as stated in the Service Details and Additional Services. If the process of the Services has been fulfilled, the validity of this Agreement will also end.
                <br><br>
                " . $company['service_name'] . " is not responsible as a guarantor either verbally or in writing to the Client or third parties for the authenticity of the documents provided by the Client.
                <br><br>
                The Client is prohibited from providing false information, including but not limited to: furnishing fake identity data, using third-party personal data obtained unlawfully (acquired through illegal means or from unauthorized sources), and/or forging signatures, whether electronic or handwritten, as well as engaging in activities beyond " . $company['service_name'] . "'s capacity and authorization to verify the accuracy or authenticity of the provided data. The Client undertakes to indemnify " . $company['service_name'] . " against any legal consequences arising from claims by third parties regarding breaches of these provisions.
                <br><br>
                " . $company['service_name'] . " is not responsible for matters beyond the power and authority of " . $company['service_name'] . " including reporting errors and/or tax calculations originating from errors in data/documents/oral or written information provided by the Client, tax arrears, examination of financial and tax reports, changes in government policies/related agencies, as well as violations of law both civil and criminal committed by the Client.
                <br><br>
                " . $company['service_name'] . " is only responsible in connection with the implementation of the provision of Services based on information and data provided by the Client to fulfill the obligations
                in this Agreement and is not bound by the Client's business activities that are not related to " . $company['service_name'] . ".
                <br><br>
                In connection with Services related to tax reporting, the Client is obliged to submit the required documents within the following deadlines:
                <ol>
                    <li>For calculations for payment and reporting of PPH, documents must be submitted no later than the 1st day on the next month.</li>
                    <li>For calculations for payment and reporting of VAT tax, documents must be submitted no later than the 10th day on the next month.</li>
                    <li>For calculations for payment and reporting of annual personal tax returns, Clients are required to submit documents before March 1st of the next year.</li>
                    <li>For calculations for payment and reporting of annual Company Tax Returns, Clients are required to submit documents before April 1st of the next year.</li>
                </ol>
                " . $company['service_name'] . " shall not be liable for any matters outside of the powers " . $company['service_name'] . " including changes in government policies / related institutions as well as civil and criminal violations committed by the Client. " . $company['service_name'] . " is solely responsible for meeting the Client's needs to the extent of the services provided in accordance with this Agreement.
                <br><br>
                The Client is prohibited from assigning, transferring or guaranteeing any rights or obligations arising in connection with this Agreement to any party with or without " . $company['service_name'] . "'s knowledge.
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.5 PENGHENTIAN SEMENTARA ATAU PENGAKHIRAN</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.5 SUSPENSION OR TERMINATION</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>
                Klien memahami bahwa " . $company['service_name'] . ", atas kebijakannya sendiri dapat menghentikan sementara/mengakhiri jasa sewaktu-waktu dengan atau tanpa pemberitahuan apabila terjadi pelanggaran dalam hal pembayaran tagihan oleh Klien dan/atau terjadi pelanggaran dalam Perjanjian ini.
                <br><br>
                Untuk hal-hal yang menyebabkan pengakhiran pada Perjanjian ini, " . $company['service_name'] . " dan Klien sepakat untuk melepaskan atau mengesampingkan keberlakuan ketentuanketentuan dalam Pasal 1266 Kitab Undang-Undang Hukum Perdata Indonesia (KUH Perdata), yang mensyaratkan dalam keadaan pembatalan perjanjian harus dimintakan kepada pengadilan.
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                Client understands that " . $company['service_name'] . ", on its sole discretion may suspend/terminate services at any time, with or without notice, in case of a default in payment of bills by Client and/or violation of this Agreement.
                <br><br>
                For matters that cause the termination of this Agreement, " . $company['service_name'] . " and Client agree to waive or waive the validity of the provisions in Article 1266 of the Indonesian Civil Code (Kitab Undang-Undang Hukum Perdata Indonesia or KUH Perdata), which requires for terminate the agreement must be submitted to the court.
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.6 JAMINAN PERLINDUNGAN</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.6 INDEMNITY</b></td>
        </tr>
        <tr class=''>
            <td colspan='2'>
                Klien menjamin dan sepakat membebaskan " . $company['service_name'] . " termasuk rekan dan pegawainya, dari dan atas segala tindakan, tuntutan, persidangan, dan klaim atas kerugian, kerusakan, kesalahan, kehilangan, perubahan kebijakan pemerintah/instansi terkait, keterlambatan pendistribusian dokumen maupun biaya yang timbul sehubungan dengan penggunaan jasa " . $company['service_name'] . ".
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                The Client guarantees and agrees to release " . $company['service_name'] . ", including its partners and employees, from and for all actions, demands, trials and claims for loss, damage, errors, losses, changes in government/related agency policies, delays in document distribution and costs incurred in connection with the use of " . $company['service_name'] . " services.
            </td>
        </tr>
        <tr>
            <td colspan='5'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . " ;'><b> &nbsp;</b></div>
            </td>
        </tr>
        <tr class='f-gray'>
            <td colspan='2'>" . (($company['tagline_name'] == '') ? '' : 'A Business of IZIN.CO.ID') . "<br><br>T • " . $company['contact_info'] . " E • " . $company['email_info'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <div style='right: 50px; position: absolute;'>
                    " . explode("<br>", $company['address_info'])[0] . "<br>
                    " . explode("<br>", $company['address_info'])[1] . "<br>
                    " . explode("<br>", $company['address_info'])[2] . "<br>
                    " . explode("<br>", $company['address_info'])[3] . "
                </div>
            </td>
        </tr>
    </table>
</div>
<div class='page_break'></div>
<div id='page4'>
    <table width='100%'>
        <tr>
            <td width='75%'>
                <span class='f-theme' style='font-size: 22px; color: " . $company['secondary_color'] . ";'>Perjanjian Layanan <b>" . $company['service_name'] . "</b></span>
                <br><span class='f-theme en' style='font-size: 16px;'>Terms of Use Agreement <b>" . $company['service_name'] . "</b></span>
                <br>
            </td>
            <td style='text-align: right;' width='25%'>
                <div style='text-align: center;'>
                    " . $company['logo'] . "
                    <br>
                    <span class='f11' style='letter-spacing: -1 px;'><b>" . $company['tagline_name'] . "</b></span>
                </div>
            </td>
        </tr>
    </table>
    <table width='100%' class='f10 tos'>
        <tr class='border-btm'>
            <td colspan='2'>
                Klien mengerti dan memahami bahwa " . $company['service_name'] . " semata membantu Klien sehubungan dengan layanan Jasa. Oleh karenanya, apabila timbul biaya tambahan akibat perbedaan persepsi Klien dengan informasi edukasi yang diberikan oleh " . $company['service_name'] . ", maka biaya tersebut akan menjadi beban bagi Klien. Tambahan biaya sebagaimana dimaksud tidak termasuk dalam biaya Jasa yang telah dibayarkan sebelumnya.
                <br><br>
                Dalam hal terdapat ancaman, ucapan kasar, pelecehan, dan kekerasan dalam segala bentuk, maka " . $company['service_name'] . " dapat melaporkan hal ini kepada pihak yang berwajib dan menghentikan layanan yang diberikan oleh " . $company['service_name'] . " secara sepihak apabila telah mengganggu aktivitas operasional " . $company['service_name'] . ".
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                The Client understands that " . $company['service_name'] . " only helps the Client in connection with the Services. Therefore, if additional costs arise due to differences in the Client's perception with the educational information provided by " . $company['service_name'] . ", then these costs will be a burden for the Client. The additional costs referred to are not included in the service fees that have been previously paid.
                <br><br>
                In the event that there are threats, harsh words, harassment and violence in all forms, then " . $company['service_name'] . " can report this to the authorities and terminate the services provided by " . $company['service_name'] . " unilaterally if it has disrupted " . $company['service_name'] . "'s operational activities.
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.7 PERLINDUNGAN DATA & INFORMASI RAHASIA</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.7 PROTECTION OF CONFIDENTIAL DATA & INFORMATION</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>
                Bahwa dalam rangka pelaksanaan layanan " . $company['service_name'] . ", Klien dapat diminta untuk mengungkapkan Informasi rahasia seperti:
                <ul>
                    <li>Informasi dan/atau data pribadi baik secara langsung maupun tidak langsung berkaitan dengan layanan " . $company['service_name'] . ", yang dikemukakan melalui lisan, tertulis, elektronik atau bentuk lain yang secara langsung maupun tidak langsung disampaikan oleh Klien kepada " . $company['service_name'] . "; atau</li>
                    <li>Segala komunikasi antara para pihak, baik secara lisan maupun tulisan yang diketahui atau semestinya diketahui oleh para pihak untuk menjadi rahasia.</li>
                </ul>
                Untuk kepentingan layanan " . $company['service_name'] . ", yang dimaksud dengan Informasi Yang Tidak Dilindungi adalah sebagai berikut:
                <ul>
                    <li>Informasi yang pada saat pengungkapannya sudah berada pada kepemilikan yang sah atau tersedia pada " . $company['service_name'] . " yang diperoleh dengan cara-cara yang sesuai dengan hukum dan dari sumber lain yang diperbolehkan untuk mengungkapkannya.</li>
                    <li>Informasi yang telah atau akan menjadi tersedia untuk umum, yang tersedia bukan dari pelanggaran " . $company['service_name'] . ".</li>
                </ul>
                " . $company['service_name'] . " berkomitmen untuk tidak mengungkapkan dan akan mengambil seluruh tindakan yang diperlukan untuk melindungi kerahasiaan Informasi Rahasia, serta menghindari pengungkapan atau penyalahgunaan Informasi Rahasia dan berjanji hanya menggunakannya untuk kepentingan layanan " . $company['service_name'] . ".
                <br><br>
                Klien menyetujui penggunaan Informasi terbatas pada nama pribadi/perusahaan, email dan nomor telepon Klien dapat digunakan " . $company['service_name'] . " untuk kepentingan pemberitahuan dan pembaharuan informasi mengenai " . $company['service_name'] . " beserta grup perusahaannya seperti informasi perihal edukasi, produk dan layanan.
                <br><br>
                " . $company['service_name'] . " memiliki kewajiban untuk mengungkapkan Informasi Rahasia apabila memang hal tersebut disyaratkan oleh undang-undang yang berlaku di Indonesia, seperti dimintakan oleh instansi yang berwenang termasuk namun tidak terbatas pada OJK, Komisi Pemberantasan Korupsi (KPK), Kepolisian Republik Indonesia dan pengadilan.
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                Whereas in the context of implementing " . $company['service_name'] . " services, Clients may be asked to disclose confidential Information such as:
                <ul>
                    <li>Personal information and/or data, either directly or indirectly related to " . $company['service_name'] . " services, submitted verbally, in writing, electronically or in other forms that are directly or indirectly conveyed by the Client to " . $company['service_name'] . "; or</li>
                    <li>All communications between the parties, both orally and in writing, which are known or should be known by the parties, are kept confidential.</li>
                </ul>
                For the benefit of " . $company['service_name'] . " services, what is meant by Unprotected Information is as follows:
                <ul>
                    <li>Information that at the time of disclosure was in legal possession or available in " . $company['service_name'] . " obtained by means in accordance with the law and from other sources that are permitted to disclose it.</li>
                    <li>Information that has been or will become publicly available, that is made available not from " . $company['service_name'] . " violations.</li>
                </ul>
                " . $company['service_name'] . " is committed not to disclose and will take all necessary measures to protect the confidentiality of Confidential Information, and to avoid disclosure or misuse of
                Confidential Information and promise to only use it for the benefit of " . $company['service_name'] . " services.
                <br><br>
                The Client agrees to use Information limited to personal/company names, email and telephone numbers. Clients can use " . $company['service_name'] . " for the purposes of notification and updating of information about " . $company['service_name'] . " and its group of companies, such as information regarding education, products and services.
                <br><br>
                " . $company['service_name'] . " has the obligation to disclose Confidential Information if indeed this is required by law in force in Indonesia, as requested by the competent authorities including but not limited to the OJK, the Corruption Eradication Commission (KPK), the Republic of Indonesia Police and the courts.
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.8 BAHASA</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.8 LANGUAGE</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>Perjanjian ini ditandatangani dalam bahasa Indonesia dan bahasa Inggris. Apabila terdapat suatu perbedaan antara versi bahasa Indonesia dan bahasa Inggris, maka versi bahasa Indonesia yang akan berlaku.</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>This agreement is executed in the Indonesian language and English language. In the event of any difference between the English and the Indonesian language versions, the Indonesian language version shall prevail.</td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.9 SALINAN DAN TANDA TANGAN ELEKTRONIK</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.9 COPIES AND ELECTRONIC SIGNATURE</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>
                Perjanjian dapat dibuat dalam 2 (dua) rangkap dan masing-masing berkekuatan hukum tetap.
                <br><br>
                Tanda tangan perjanjian dapat dilakukan dengan tanda tangan elektronik atau tanda tangan basah. Apabila " . $company['service_name'] . " ataupun Klien menandatangani perjanjian dengan tanda tangan elektronik, tandatangan elektronik tersebut dianggap asli, sah dan memiliki kekuatan hukum yang sama dengan tandatangan basah.
            </td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>
                The agreement can be made in two copies and both are legally binding.
                <br><br>
                Signature on agreement can be done with an electronic or wet signature. If " . $company['service_name'] . " or client signed an agreement with electronic signatures, it is considered genuine, legitimate and have the same legal force with a wet signature.
            </td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.10 KEADAAN KAHAR</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.10 FORCE MAJEURE</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>" . $company['service_name'] . " tidak bertanggung jawab atas pelanggaran syarat dan ketentuan perjanjian yang disebabkan karena keadaan diuar kekuasaan kami namun tidak terbatas pada keadaan kahar termasuk bencana alam (gempa bumi, banjir, taufan, tanah longsor), pandemi, huruhara, perang pemberontakan, pemogokan, kebakaran, perubahan kebijakan pemerintah.</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>" . $company['service_name'] . " is not responsible for any infringement on the terms and conditions of the agreement due to circumstances that are not within our control but not limited to force majeure including natural disasters (earthquakes, floods, typhoons, landslides), pandemic, riots, wars, strikes, fire, and changes in government policy.</td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'><b>1.11 YURISDIKSI</b></td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'><b>1.11 JURISDICTIONS</b></td>
        </tr>
        <tr class='border-btm'>
            <td colspan='2'>Setiap permasalahan yang timbul sehubungan dengan perjanjian disepakati Para Pihak untuk diselesaikan secara musyawarah untuk mencapai mufakat. Apabila kesepakatan tidak tercapai dalam 60 (enam puluh) hari sejak timbulnya perselisihan, maka " . $company['service_name'] . " dan Klien sepakat untuk menyelesaikan permasalahan secara hukum melalui Pengadilan Negeri Jakarta Barat.</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2' class='en-terms'>Any issues arising in connection with the Agreement shall be resolved through discussion to reach consensus. In the event where discussion cannot be settled within 60 (sixty) days since the issue arises, " . $company['service_name'] . " and Client agree to resolve the issues legally through the West Jakarta District Court.</td>
        </tr>
        <tr>
            <td colspan='5'>
                <div class='b-theme head-br' width='100%' style='background-color: " . $company['primary_color'] . " ;'><b> &nbsp;</b></div>
            </td>
        </tr>
        <tr class='f-gray'>
            <td colspan='2'>" . (($company['tagline_name'] == '') ? '' : 'A Business of IZIN.CO.ID') . "<br><br>T • " . $company['contact_info'] . " E • " . $company['email_info'] . "</td>
            <td width='3%' style='border-bottom: 0px solid transparent;'></td>
            <td colspan='2'>
                <div style='right: 50px; position: absolute;'>
                    " . explode("<br>", $company['address_info'])[0] . "<br>
                    " . explode("<br>", $company['address_info'])[1] . "<br>
                    " . explode("<br>", $company['address_info'])[2] . "<br>
                    " . explode("<br>", $company['address_info'])[3] . "
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>";
