<?php

require_once "system/SendEmail.php";

$Email_Subject = "Agreement Invoice No. $invnum";

$Email_Body = "Dear $Full_Name,
            <br><br>
            Berikut ini kami lampirkan form agreement layanan IZIN untuk di tandatangani oleh Bapak/Ibu $clientPic.
            <br><br><br><br>
            Salam,<br>
            <strong>IZIN.co.id</strong>
            <br><br>
            <hr>
            <em>This is an automated message please do not reply directly to this email/whatsapp. For further information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";

$mail->IsSMTP();
$mail->SMTPDebug = EMAIL_SMTPDEBUG;
$mail->Host = EMAIL_HOST;
$mail->SMTPAuth = EMAIL_SMTPAUTH;
$mail->SMTPSecure = EMAIL_SMTPSECURE;
$mail->Port = EMAIL_PORT;
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->IsHTML(EMAIL_ISHTML);
$mail->Username = EMAIL_USERNAME;
$mail->Password = EMAIL_PASSWORD;
$mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
$mail->AddAddress($Email_Address);
$mail->AddCC(EMAIL_CC);
$mail->Subject = $Email_Subject;
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
$mail->addAttachment($file);
if ($mail->send())
{
    $AlertMess .= "<br> Email message sent (to: $Email_Address)";
}
else
{
    $AlertMess .= "<br> Fail sending email message (problem: " . $mail->ErrorInfo . ")";
}