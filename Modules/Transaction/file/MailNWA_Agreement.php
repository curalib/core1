<?php

require_once "system/SendEmail.php";

if ($reminder) {
    $Email_Subject = "Reminder Agreement Invoice No. $invnum";

    if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
        $Email_Body = "Dear Client,
            <br><br>
            Greetings from Invest In Asia!
            <br><br>
            Thank you for using our service!
            <br><br>
            We want to remind you that we send a permit and lease agreement to be signed <a href='" . $url . "'>here</a>.
            <br><br>
            For your information, date on the lease agreement is started from the payment date of your virtual office and need to be activated within 60 (sixty) days since vOffice confirmed your payment.
            <br><br>
            If you didn't send the signed lease agreement for virtual office within those periods, the virtual office will be forced activate by system.
            <br><br>
            Thank You.
            <br><br><br><br>
            Warm regards,<br>
            <strong>Invest In Asia team</strong>
            <br><br>";
    } else {
        $Email_Body = "Dear Valued Customer,
            <br><br>
            Kindly Reminder :
            <br><br>
            Kami lampirkan agreement layanan IZIN untuk dapat di tandatangani oleh Bapak/Ibu $clientPic. Mohon agreement untuk dapat ditandatangani pada bagian tandatangan sesuai tautan <a href='" . $url . "'>di sini</a>.
            <br><br>
            Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk Anda.
            <br><br><br><br>
            Salam,<br>
            <strong>IZIN.co.id</strong>
            <br><br>
            <hr>
            <em>This is an automated message please do not reply directly to this email/whatsapp. For further information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";

        $WA_Message = "Dear Valued Customer,

Kindly Reminder :

Kami lampirkan agreement layanan IZIN untuk dapat di tandatangani oleh Bapak/Ibu $clientPic

Mohon agreement untuk dapat ditandatangani pada bagian tandatangan sesuai tautan berikut: $url

Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk Anda.

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62822-9998-0011_/_cs@izin.co.id_
";
    }
} else {
    $Email_Subject = "Agreement Invoice No. $invnum";

    if ($BAD_NmPT === 'PT INVESTASI INDO ASIA') {
        $Email_Body = "Dear Client,
            <br><br>
            Greetings from Invest In Asia!
            <br><br>
            Thank you for using our service!
            <br><br>
            Within this email, we attached your permit and lease agreement.
            <br><br>
            Please sign on <a href='" . $url . "'>this page</a> as soon as possible so we can activate your lease.
            <br><br>
            Thank You.
            <br><br><br><br>
            Warm regards,<br>
            <strong>Invest In Asia team</strong>
            <br><br>";
    } else {
        $Email_Body = "Dear Valued Customer,
            <br><br>
            Kami lampirkan agreement layanan IZIN untuk dapat di tandatangani oleh Bapak/Ibu $clientPic. Mohon agreement untuk dapat ditandatangani pada bagian tandatangan sesuai tautan <a href='" . $url . "'>di sini</a>.
            <br><br>
            Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk Anda.
            <br><br><br><br>
            Salam,<br>
            <strong>IZIN.co.id</strong>
            <br><br>
            <hr>
            <em>This is an automated message please do not reply directly to this email/whatsapp. For further information you can contact us through our whatsapp/email+62 822-9998-0011 / cs@izin.co.id</em>";

        $WA_Message = "Dear Valued Customer,

Kami lampirkan agreement layanan IZIN untuk dapat di tandatangani oleh Bapak/Ibu $clientPic

Mohon agreement untuk dapat ditandatangani pada bagian tandatangan sesuai tautan berikut: $url

Kami akan berusaha senantiasa memberikan layanan yang terbaik untuk Anda.

Sincerely,
*IZIN.co.id*

__________________________________________________________________________

_This_ _is_ _an_ _automated_ _message_ _please_ _do_ _not_ _reply_ _directly_ _to_ _this_ _whatsapp_ _number._ _For_ _further_ _information_ _you_ _can_ _contact_ _us_ _through_ _our_ _whatsapp/email_ _+62822-9998-0011_/_cs@izin.co.id_
";
    }
}

$mail->IsSMTP();
$mail->SMTPDebug = EMAIL_SMTPDEBUG;
$mail->Host = EMAIL_HOST;
$mail->SMTPAuth = EMAIL_SMTPAUTH;
$mail->SMTPSecure = EMAIL_SMTPSECURE;
$mail->Port = EMAIL_PORT;
$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);
$mail->IsHTML(EMAIL_ISHTML);
$mail->Username = EMAIL_USERNAME;
$mail->Password = EMAIL_PASSWORD;
$mail->SetFrom(EMAIL_SETFROM_MAIL, EMAIL_SETFROM_NAME);
$mail->AddAddress($Email_Address);
$mail->Subject = $Email_Subject;
$mail->Body = $Email_Body;
$mail->AltBody = strip_tags($Email_Body);
if ($mail->send()) {
    $_lang = ($reminder) ? $lang_3015 : $lang_3009;
    $AlertMess = "alert-success#done_all#SUCCESS!#$_lang";
} else {
    $_lang = ($reminder) ? $lang_3016 : $lang_3012;
    $AlertMess = "alert-danger#remove_circle#ERROR!#$_lang";
}
