<?php
require "system/PagingTrack.php";
require "page/Alert.php";

echo '<div class="container-fluid page__container">';
if (!isset($_SESSION["Edit_ID"])) {
	if (!isset($_SESSION["AGREEMENT"])) {
		$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Inv_List.php";
	} else {
		$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Agreement.php";
	}
} else {
	unset($_SESSION["AGREEMENT"]);
	unset($_SESSION["AGREEMENT_POST"]);
	unset($_SESSION['agreement_pdf']);
	$fl = $_SESSION["ROOT_DIR"] . "/Modules/" . $cmodule . "/file/Inv_Det.php";
}

if (is_file($fl)) {
	require $fl;
} else {
	require $_SESSION["ROOT_DIR"] . "/Modules/System/page/PageNotFound.php";
}
echo '</div>';
