<?php
if ( CPTCH === true and empty($LoginErrs) ){
	if ( UrlExist(BASEURL.'Cptch.php') === false ){
		$LoginErrs = 'CAPT_ERROR';
	}
}
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
/*
<html>
<html lang="en">

*/
echo '
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
<!-- new -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Prevent the demo from appearing in search engines -->
<meta name="robots" content="noindex">
<!-- old
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
-->

<title>'.APPS_TITLE.'</title>

<link rel="shortcut icon" href="'.BASEURL.'templates/Default/images/izin.png" type="image/gif/png">

<!-- old
<link rel="shortcut icon" href="img/izin.png" type="image/gif/png">
<link rel="icon" href="'.BASEURL.APPS_ICON.'" type="image/x-icon"/>
<link href="'.BASEURL.'css/Front.css" rel="stylesheet" type="text/css" />
-->


<!-- new -->



<!-- App CSS -->
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/app.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/app.rtl.css" rel="stylesheet">

<!-- Material Design Icons -->
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-material-icons.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-material-icons.rtl.css" rel="stylesheet">


<!-- Font Awesome FREE Icons -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-fontawesome-free.rtl.css" rel="stylesheet">


<!-- Simplebar
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-simplebar.min.css" rel="stylesheet">
-->

<!-- Flatpickr
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-flatpickr.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-flatpickr.rtl.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-flatpickr-airbnb.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-flatpickr-airbnb.rtl.css" rel="stylesheet">
-->

<!-- Vector Maps
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-jqvmap.min.css" rel="stylesheet">
-->


<!-- Dragula
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-dragula.min.css" rel="stylesheet">
-->
	<!-- Quill Theme -->
<!--
	<link type="text/css" href="'.BASEURL.'assets/css/izin_css/css/vendor-quill.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-quill.rtl.css" rel="stylesheet">
-->

<!-- Dropzone
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-dropzone.css" rel="stylesheet">
<link type="text/css" href="'.BASEURL.'assets/css/izin_css/vendor-dropzone.rtl.css" rel="stylesheet">
-->

</head>





<!-- Old
<body>
-->
<body class="layout-login-centered-boxed">
<!-- New -->
<!--=======================================Modal Form========================================-->

<!--=======================================End Of Modal Form========================================-->
';



echo '<div class="layout-login-centered-boxed__form card">';
echo '
<div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
	<a href="index.html" class="navbar-brand flex-column mb-2 align-items-center mr-0" style="min-width: 0">
		<img class="navbar-brand-icon" src="'.BASEURL.'templates/Default/images/logo-dark-backup.png.png" width="120" alt="Izin">
	</a>
</div>
<div class="page-separator">
	<div class="page-separator__text">ERP IZIN</div>
</div>
<form action="" method="post" name="LoginApp" id="LoginApp">';

if (empty($LoginErrs)) {
echo '
<div class="form-group">
	<label class="text-label" for="email_2">'.$lang_30.':</label>
	<div class="input-group input-group-merge">
		<input name="fuid" id="fuid" type="email" required="" class="form-control form-control-prepended" placeholder="john@doe.com">
		<div class="input-group-prepend">
			<div class="input-group-text">
				<span class="far fa-envelope"></span>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="text-label" for="password_2">'.$lang_7.':</label>
	<div class="input-group input-group-merge">
		<input name="fpwd" id="fpwd" type="password" required="" class="form-control form-control-prepended" placeholder="Enter your password">
		<div class="input-group-prepend">
			<div class="input-group-text">
				<span class="fa fa-key"></span>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<button name="Login" type="submit" class="btn btn-block btn-primary" value="Enter">'.$lang_31.'</button>
</div>';
}else{
echo '
<div class="page-separator">
	<div class="page-separator__text text-danger align-items-center">'.$LoginErrsNote[$LoginErrs].' '.$LoginErrsMess.'</div>
</div>
<div class="form-group">
	<button name="Login" type="submit" class="btn btn-block btn-primary" value="'.$lang_5.'">'.$lang_5.'</button>
</div>
';
}
echo '
</form>
</div>';

echo '
    <!-- jQuery -->
<!--
    <script src="'.BASEURL.'assets/js/izin_vendor/jquery.min.js"></script>
-->
    <!-- Bootstrap -->
<!--
    <script src="'.BASEURL.'assets/js/izin_vendor/popper.min.js"></script>
    <script src="'.BASEURL.'assets/js/izin_vendor/bootstrap.min.js"></script>
-->
    <!-- Simplebar -->
<!--
    <script src="'.BASEURL.'assets/js/izin_vendor/simplebar.min.js"></script>
-->
    <!-- DOM Factory -->
<!--
    <script src="'.BASEURL.'assets/js/izin_vendor/dom-factory.js"></script>
-->
    <!-- MDK -->
<!--
    <script src="'.BASEURL.'assets/js/izin_vendor/material-design-kit.js"></script>
-->
    <!-- App -->
<!--
    <script src="'.BASEURL.'assets/js/izin_vendor/toggle-check-all.js"></script>
    <script src="'.BASEURL.'assets/js/izin_vendor/check-selected-row.js"></script>
-->


		<!-- App Settings (safe to remove) -->
<!--
    <script src="'.BASEURL.'assets/js/izin_js/app-settings.js"></script>
-->

';






/*
<?php echo BASEURL ?>
*/


echo '
</body>
</html>
';
