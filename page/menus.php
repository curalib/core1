<?php
echo '
    <menus>
        <div id="stuck_container" class="stuck_container">
            <nav class="nav">
                <div class="container">
                    <div class="MenusPart">
                        '.$mnshow.'
                    </div>
                    <div class="UsersPart">
                        <div class="MyAccLogo">
                            '.$sysmnsh.'
                        </div>
                        <div class="MyAccProp"> <strong>'.strtoupper($_SESSION['lgnNama']).'</strong><br>
                            '.${"lang_".$_SESSION['lgnPriv']}.'
		                    </div>
                    </div>
                </div>
            </nav>
        </div>
    </menus>
';
?>
