<?php
if ( $AlertMess ){
	$ListAlert = explode("**",$AlertMess);
	foreach( $ListAlert as $LstAl ){
		list( $AlCss,$AlIcon,$AlTitle,$AlMess ) = explode("#", $LstAl);
		$AlMess = preg_replace("/\|/","<br>",$AlMess);
		echo '
		<div class="container-fluid page__container">	
			<div class="alert '.$AlCss.' alert-dismissible fade show" role="alert">
				<div class="row">
   					<div class="col-auto align-self-center"><span class="material-icons" style="font-size: 38px">'.$AlIcon.'</span></div>
        				<div class="col">
        					<h6 class="alert-heading">'.$AlTitle.'</h6>
            				'.$AlMess.'
         				</div>
         				<div class="row align-self-start">
         				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    						<span aria-hidden="true">&times;</span>
  						</button>
        				</div>
    				</div>
				</div>
			</div>
		';
	}
}
?>