<div class="mdk-drawer  js-mdk-drawer" id="default-drawer" data-align="start">
	<div class="mdk-drawer__content">
    	<div class="sidebar sidebar-light sidebar-left simplebar" data-simplebar>
      		<div class="d-flex align-items-center sidebar-p-a border-bottom">
            	<span class="avatar mr-3">
                	<?php
					echo "<img src='data:image/jpg;base64,".base64_encode(ShowImage("../../".UPDIR."/avatar/",$_SESSION["Avatar"]))."' class='avatar-img rounded-circle' alt='Avatar'>";
					?>
        		</span> 
                <span class="flex d-flex flex-column">
                	<strong><?php echo $_SESSION['lgnNama'] ?></strong> 
                    <small class="text-muted text-uppercase"><?php echo $_SESSION["Jbtn"] ?></small>
				</span>
        		<div class="dropdown ml-auto">
                	<a href="" data-toggle="dropdown" data-caret="false" class="text-muted"><i class="material-icons">more_vert</i></a>
          			<div class="dropdown-menu dropdown-menu-right">
            			<div class="dropdown-item-text dropdown-item-text--lh">
              				<!--div><strong><?php echo $_SESSION['lgnNama'] ?></strong></div-->
            			</div>
             			<?php echo $sysshow ?>
          			</div>
        		</div>
			</div>
      		<div class="sidebar-heading sidebar-m-t">Menu</div>
      		<ul class="sidebar-menu">
				<?php echo $mmshow ?>
      		</ul>
            <!-- 
            	Ini musti ikut, Kalu ga scroll bar ga muncul
             -->
            <div class="sidebar-heading">&nbsp;</div>
      		<div class="sidebar-block p-0">&nbsp;</div>
      		<div class="sidebar-p-a">&nbsp;</div>
			<div class="sidebar-empty-space">&nbsp;</div>
    	</div>
	</div>
</div>