
	<div id="header" class="mdk-header js-mdk-header m-0" data-fixed>
		<div class="mdk-header__content">
			<div class="navbar navbar-expand-sm navbar-main navbar-dark bg-c-green pr-0" id="navbar" data-primary><!-- bg-c-green bg-izin  -->
				<div class="container-fluid p-0">
					<button class="navbar-toggler navbar-toggler-right d-block d-md-none" type="button" data-toggle="sidebar"><span class="navbar-toggler-icon"></span></button>
                    <a href="<?php echo BASEURL?>" class="navbar-brand"><img class="navbar-brand-icon" src="<?php echo BASEURL?>assets/images/logo-dark-backup.png" width="100" alt="izin"></a>
                        <!-- SEARCH BAR -->
<!--
                        <form class="izin-bg-search search-form d-none d-sm-flex flex" action="index.html">
                           <button class="btn" type="submit" role="button"><i class="material-icons">search</i></button>
                           <input type="text" class="form-control" placeholder="<?php //echo $lang_102?>">
                        </form>
-->
                        <ul class="nav navbar-nav ml-auto d-none d-md-flex">
                            <!-- GET HELP -->
                            <li class="nav-item">
                                <a href="" class="nav-link">
<!-- !!!!!!!!! Masukkan fungsi/link Get Help di sini -->
                                    <i class="material-icons">help_outline</i> <?php echo $lang_103?>
                                </a>
                            </li>


                            <!-- NOTIFICATONS -->
                            <li class="nav-item dropdown">
                                <a href="#notifications_menu" class="nav-link dropdown-toggle" data-toggle="dropdown" data-caret="false">
                                    <i class="material-icons nav-icon navbar-notifications-indicator">notifications</i>
                                </a>
                                <div id="notifications_menu" class="dropdown-menu dropdown-menu-right navbar-notifications-menu">
                                    <div class="dropdown-item d-flex align-items-center py-2">
                                        <span class="flex navbar-notifications-menu__title m-0">Notifications<?php //echo $lang_104?></span>
<!-- !!!!!!!!! Masukkan fungsi notifikasi clearall di sini -->
                                        <!--<a href="javascript:void(0)" class="text-muted"><small>Clear all</small></a>-->

                                    </div>
                                    <div class="navbar-notifications-menu__content" data-simplebar>
                                        <div class="py-2">



	<?php
	if(isset($_SESSION["lgnPriv"]) && ($_SESSION["lgnPriv"] == 3 || $_SESSION["lgnPriv"] == 2  || $_SESSION["lgnPriv"] == 1 || $_SESSION["lgnPriv"] == 0) ) {
	//if(isset($_SESSION["Jbtn"]) && ($_SESSION["Jbtn"] == "Administrator" || $_SESSION["Jbtn"] == "Manager"  || $_SESSION["Jbtn"] == "Super Administrator")) {
 	    while($NotifDetail = $dbs->getAssoc($rcNotifDetail)) {
        ?>

																					<div class="dropdown-item d-flex">
<!--
																							<div class="mr-3">
																									<div class="avatar avatar-sm" style="width: 32px; height: 32px;">
																											<img src="<?php //echo BASEURL?>templates/Default/images/izin_img/256_daniel-gaffey-1060698-unsplash.jpg" alt="Avatar" class="avatar-img rounded-circle">
																									</div>
																							</div>
-->
																							<div class="flex">
																									<!--<a href="">Ax.Demian</a> left a comment on-->
																									<form action="<?php echo $trTracking ?>" method="post" name="ViewHead_<?php echo $NotifDetail['id_notif']; ?>" id ="ViewHead_<?php echo $NotifDetail['id_notif']; ?>">
							                                        <input type="hidden" name="task" />
							                                        <input type="hidden" name="tracking_id" value="<?php echo $NotifDetail['tracking_id']; ?>">
							                                    </form>
																									<a href=""><?php echo $NotifDetail['company_name']; ?></a> <?php echo BADGE_STATUS[$NotifDetail['trkitm_status']]; ?> <?php echo $NotifDetail['kode']; ?>

																									<div class="float-sm-right">
							                                    <a href="javascript:submitbutton('Track_<?php echo $_SESSION["butts_Enter"]?>_Notif','ViewHead_<?php echo $NotifDetail['id_notif']?>');">View<?php //echo $NotifDetail['tracking_id']?></a><br>
																									</div>

																									<div class="">
																									<small class="text-muted">
																										<?php
																                    $LastActivityInDays = $HitDays->DayRange(convertDate($NotifDetail["tgl_update_itm"]), "now", false)->Result();
																                    echo $LastActivityInDays[1];
																                    ?>
																									</small>
																									</div>
																							</div>
																					</div>
	<?php
		}
	 }

	//if(isset($_SESSION["Jbtn"]) && ($_SESSION["Jbtn"] == "Sales" || $_SESSION["Jbtn"] == "Manager" || $_SESSION["Jbtn"] == "Super Administrator")) {
	if(isset($_SESSION["lgnPriv"]) && ($_SESSION["lgnPriv"] == 4 || $_SESSION["lgnPriv"] == 2  || $_SESSION["lgnPriv"] == 1 || $_SESSION["lgnPriv"] == 0)) {
	    $dt = new DateTime('now', new DateTimezone('Asia/Jakarta'));
            $day_now = $dt->format('w');
            // Hari rabu atau jumat
            if(($day_now == 3 || $day_now == 5) || $_SESSION["lgnPriv"]== 0) {
	?>


																					<div class="list-group-item list-group-item-action d-flex align-items-center ">
																													<div class="mr-3">
																															<form action="" method="post" name="NotifLeadToXLS" id ="NotifLeadToXLS">
							                                                    <input type="hidden" name="task" />
							                                                </form>
																															<a href="javascript:submitbutton('NotifExportToXLS_<?php echo $_SESSION['butts_Enter']?>','NotifLeadToXLS');">
																																	<div class="avatar avatar-xs" style="width: 32px; height: 32px;">
																																			<span class="avatar-title bg-purple rounded-circle"><i class="material-icons icon-16pt">cloud_download</i></span>
																																	</div>
																															</a>
																													</div>
																													<div class="flex">
																															Follow up Lead <a href="javascript:submitbutton('NotifExportToXLS_<?php echo $_SESSION['butts_Enter']?>','NotifLeadToXLS');">Download</a>
																															<!--<small class="text-muted">1 hour ago</small>-->
																													</div>
																							<!--<i class="material-icons icon-muted ml-3">arrow_forward</i>   "  onclick="  -->
																					</div>
                                          <?php
                                            		}
                                            }
																					?>
                                        </div>
                                    </div>
<!-- !!!!!!!!! Masukkan fungsi notifikasi view all di sini -->
																		<!--<a href="javascript:void(0);" class="dropdown-item text-center navbar-notifications-menu__footer">View All</a>-->
                                    <a href="<?php echo $ntNotif ?>" class="dropdown-item text-center navbar-notifications-menu__footer">View All</a>
                                </div>
                            </li>

                        </ul>



<!-- Container Fluid-->
<!--
</div>
-->
                    </div>  <!-- Container Fluid-->
                </div>  <!-- Navbar-->
            </div>  <!-- mdk-header__content-->
        </div>  <!-- mdk-header -->
