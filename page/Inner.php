<?php
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo APPS_TITLE ?></title>
	<link rel="shortcut icon" href="<?php echo BASEURL ?>templates/Default/images/izin.png" type="image/gif/png">
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-jquery.min.js"></script>
	<script src="<?php echo BASEURL ?>assets/js/jquery-ui.min.js"></script>

	<link href="<?php echo BASEURL ?>assets/css/jquery-ui.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/jquery-ui.theme.min.css" type="text/css" rel="stylesheet">

	<link href="<?php echo BASEURL ?>assets/css/izin_css/app.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/barep.css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-dropzone.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-dropzone.rtl.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-fontawesome-free.rtl.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-simplebar.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-flatpickr.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-flatpickr-airbnb.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/izin_css/vendor-select2.min.css" type="text/css" rel="stylesheet">
	<link href="<?php echo BASEURL ?>assets/css/xxTambahanku.css" rel="stylesheet" />
	<link href="<?php echo BASEURL ?>assets/css/blitzer/jquery-ui.css" type="text/css" rel="stylesheet">
	<script src="<?php echo BASEURL ?>assets/js/pus.js"></script>
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment.min.js"></script>
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-moment-range.js"></script>
	<script src="<?php echo BASEURL ?>assets/js/autoNumeric-1.9.41.js"></script>
	<script>
		(function() {
			'use strict';
			window['moment-range'].extendMoment(moment);
		})()
	</script>
</head>

<body class="layout-default">
	<div class="mdk-header-layout js-mdk-header-layout">
		<?php
		require "Modules/System/mod/Notif_i.php";
		require "page/header.php";
		?>
		<div class="mdk-header-layout__content">
			<div class="mdk-drawer-layout js-mdk-drawer-layout" data-push data-responsive-width="754px">
				<div class="mdk-drawer-layout__content page">
					<div class="inner">
						<div id="Page">
							<?php
							if (!is_file("Modules/" . $cmodule . "/page/" . $cpage)) {
								$cmodule = 'System';
								$cpage = "PageNotFound.php";
							}
							require_once "Modules/" . $cmodule . "/page/" . $cpage;
							?>
						</div>
					</div>
				</div> <!-- // END mdk-drawer-layout__content -->
				<?php include "page/sidebar.php"; ?>
			</div> <!-- // END mdk-drawer-layout -->
		</div> <!-- // END mdk-header-layout__content -->
	</div> <!-- // END mdk-header-layout -->




	<!-- Popper --> <!-- Sidebar Popup Menu-->
	<!--
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	-->
	<!-- Popper --> <!-- Sidebar Popup Menu--> <!-- Penempatan Harus before Bootstrap-->
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-popper.min.js"></script>

	<!-- Bootstrap --> <!-- Menu, Notifications-->
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-bootstrap.min.js"></script>

	<!-- Flatpickr --> <!-- Dropdown Tanggal -->
	<script src="<?php echo BASEURL ?>assets/js/izin_js/flatpickr.min.js"></script>
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-flatpickr.js"></script>



	<!-- App -->
	<!--
		<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-toggle-check-all.js"></script>
		<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-check-selected-row.js"></script>
	-->
	<!-- App Settings (safe to remove) -->
	<!--
		<script src="<?php echo BASEURL ?>assets/js/izin_js/app-settings.js"></script>
	-->




	<!-- SCRIPT untuk SIDEBAR (Tidak perlu yang lain??) -->

	<!-- Simplebar --> <!-- Sidebar -->
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-simplebar.min.js"></script>
	<!-- DOM Factory --> <!-- Sidebar -->
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-dom-factory.js"></script>
	<!-- MDK --> <!-- Sidebar -->
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-material-design-kit.js"></script>

	<script>
		(function() {
			'use strict';
			// Self Initialize DOM Factory Components
			domFactory.handler.autoInit()

			// Connect button(s) to drawer(s)
			var sidebarToggle = document.querySelectorAll('[data-toggle="sidebar"]')
			sidebarToggle = Array.prototype.slice.call(sidebarToggle)

			sidebarToggle.forEach(function(toggle) {
				toggle.addEventListener('click', function(e) {
					var selector = e.currentTarget.getAttribute('data-target') || '#default-drawer'
					var drawer = document.querySelector(selector)
					if (drawer) {
						drawer.mdkDrawer.toggle()
					}
				})
			})

			let drawers = document.querySelectorAll('.mdk-drawer')
			drawers = Array.prototype.slice.call(drawers)
			drawers.forEach((drawer) => {
				drawer.addEventListener('mdk-drawer-change', (e) => {
					if (!e.target.mdkDrawer) {
						return
					}
					document.querySelector('body').classList[e.target.mdkDrawer.opened ? 'add' : 'remove']('has-drawer-opened')
					let button = document.querySelector('[data-target="#' + e.target.id + '"]')
					if (button) {
						button.classList[e.target.mdkDrawer.opened ? 'add' : 'remove']('active')
					}
				})
			})

			// SIDEBAR COLLAPSE MENUS
			$('.sidebar .collapse').on('show.bs.collapse', function(e) {
				e.stopPropagation()
				var parent = $(this).parents('.sidebar-submenu').get(0) || $(this).parents('.sidebar-menu').get(0)
				$(parent).find('.open').find('.collapse').collapse('hide');
				$(this).closest('li').addClass('open');
			});
			$('.sidebar .collapse').on('hidden.bs.collapse', function(e) {
				e.stopPropagation()
				$(this).closest('li').removeClass('open');
			});

			// ENABLE TOOLTIPS
			$('[data-toggle="tooltip"]').tooltip()

			// PRELOADER
			window.addEventListener('load', function() {
				$('.preloader').fadeOut()
				domFactory.handler.upgradeAll()
			})

		})()
	</script>
	<!-- SCRIPT untuk SIDEBAR (Tidak perlu yang lain??) -->

	<script src="<?php echo BASEURL ?>assets/js/izin_vendor/select2/select2.min.js"></script>
	<script src="<?php echo BASEURL ?>assets/js/izin_js/vendor-select2.js"></script>

	<?php
	if (!empty($AlertMess)) {
		require "system/pesan.php";
	}
	?>




</body>

</html>